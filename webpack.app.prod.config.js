const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const isProduction = process.env.NODE_ENV === 'production';
const APP_DIR = path.resolve(__dirname, 'src');
const STATIC_DIR = path.resolve(__dirname, 'static', 'app' );
const BUILD_DIR = path.resolve(__dirname, 'dist', 'app');
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  mode: 'production',
  entry: {
    main: `${APP_DIR}/app.tsx`,
    vendor: [
      'babel-polyfill',
      'react',
      'react-dom',
      'react-redux',
      'react-router',
      'redux'
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].[chunkhash].js',
    publicPath: '/',
    globalObject: 'this'
  },
  target: 'web',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'awesome-typescript-loader?module=es6',
            options: {
              configFileName: 'tsconfig.json',
            },
          },
        ],
        include: path.join(__dirname, 'src'),
      },
      {
        test: /(\.scss|\.css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              url: false,
            }
          },
          {
            loader: 'sass-loader',
            options: {
              url: false
            }
          }
        ],
      },
      { test: /\.html$/, use: 'html-loader' },
      { test: /\.png$/, use: 'url-loader?limit=10000' },
      { test: /\.jpg$|\.woff2?$|\.gif$|\.ttf$|\.eot$|\.svg$/, use: 'file-loader' },
    ]
  },
  optimization: {
    runtimeChunk: {
      name:'vendor',
    },
    minimizer: [new UglifyJsPlugin()],
  },
  plugins: [
    new webpack.optimize.AggressiveMergingPlugin(),
    new HtmlWebpackPlugin({
      title: 'acela',
      template: `${STATIC_DIR}/index.html`
    }),
    new CopyWebpackPlugin(
      [
        {from: `${STATIC_DIR}/assets`, to: `${BUILD_DIR}/assets`},
      ]
    ),
    new webpack.DefinePlugin({
      'process.env': {
        SENTRY_KEY: JSON.stringify(process.env.SENTRY_KEY),
        API_URL: JSON.stringify(process.env.API_URL),
      },
  }),
  new MiniCssExtractPlugin(),
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    mainFields: ['browser', 'main']
  }
};