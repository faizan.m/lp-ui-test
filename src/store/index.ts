import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, compose, createStore as _createStore } from 'redux';
import { reducers as combinedReducers } from '../reducers';

import ApiMiddleware from '../middleware/ApiMiddleware';
import {
  LoginMiddleware,
  LogOutMiddleware,
} from '../middleware/AuthMiddleware';

export const history = createBrowserHistory();

const reduxRouterMiddleware = routerMiddleware(history);
const middleware = [
  reduxRouterMiddleware,
  LoginMiddleware,
  LogOutMiddleware,
  ApiMiddleware,
];

const store = _createStore(
  combinedReducers,
  compose(
    applyMiddleware(...middleware),
    (window as any).devToolsExtension
      ? (window as any).__REDUX_DEVTOOLS_EXTENSION__()
      : f => f
  )
);

export default store;
