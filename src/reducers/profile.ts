import {
  FETCH_CUST_USERS_FAILURE,
  FETCH_CUST_USERS_REQUEST,
  FETCH_CUST_USERS_SUCCESS,
  FETCH_PROFILE_FAILURE,
  FETCH_PROFILE_REQUEST,
  FETCH_PROFILE_SUCCESS,
  PROFILE_EDIT_FAILURE,
  PROFILE_EDIT_REQUEST,
  PROFILE_EDIT_SUCCESS,
  PROFILE_UPLOAD_FAILURE,
  PROFILE_UPLOAD_REQUEST,
  PROFILE_UPLOAD_SUCCESS,
} from '../actions/profile';

const initialState: IProfilestore = {
  user: null,
  isFetching: false,
  customerUsers: [],
  isUsersFetching: false,
};

export default function customer(state: any = initialState, action: any): any {
  switch (action.type) {
    case FETCH_PROFILE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        user: action.response,
        isFetching: false,
      });

    case FETCH_PROFILE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    case PROFILE_EDIT_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case PROFILE_EDIT_SUCCESS:
      return Object.assign({}, state, {
        user: action.response,
        isFetching: false,
      });

    case PROFILE_EDIT_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        errorList: action.errorList,
        isFetching: false,
      });

    case PROFILE_UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case PROFILE_UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case PROFILE_UPLOAD_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    case FETCH_CUST_USERS_REQUEST: {
      return Object.assign({}, state, {
        isUsersFetching: true,
      });
    }
    case FETCH_CUST_USERS_SUCCESS: {
      return Object.assign({}, state, {
        customerUsers: action.response,
        isUsersFetching: false,
      });
    }

    case FETCH_CUST_USERS_FAILURE: {
      return Object.assign({}, state, {
        isUsersFetching: false,
      });
    }

    default:
      return state;
  }
}
