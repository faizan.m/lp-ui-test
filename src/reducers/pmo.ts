import {
  LAST_VISITED_PMO_SITE,
  ADDITIONAL_SETTING_REQUEST,
  ADDITIONAL_SETTING_SUCCESS,
  ADDITIONAL_SETTING_FAILURE,
  SAVE_PMO_DASHBOARD_FILTERS,
  SAVE_PROJECT_DETAILS_FILTER,
} from "../actions/pmo";

const initialState: IPMOStore = {
  lastVisitedSite: "/ProjectManagement",
  pmoDashboardFilters: null,
  projectDetailsFilters: null,
  additionalSetting: null,
  isFetching: false,
};

export default function customer(state: any = initialState, action: any): any {
  switch (action.type) {
    case ADDITIONAL_SETTING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case ADDITIONAL_SETTING_SUCCESS:
      return Object.assign({}, state, {
        additionalSetting: action.response,
        isFetching: false,
      });

    case ADDITIONAL_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case SAVE_PROJECT_DETAILS_FILTER:
      return Object.assign({}, state, {
        projectDetailsFilters: action.response,
      });

    case SAVE_PMO_DASHBOARD_FILTERS:
      return Object.assign({}, state, {
        pmoDashboardFilters: action.response,
      });

    case LAST_VISITED_PMO_SITE:
      return Object.assign({}, state, {
        lastVisitedSite: action.response,
      });

    default:
      return state;
  }
}
