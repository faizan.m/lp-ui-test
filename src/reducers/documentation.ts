import {
  CREATE_CIRCUIT_TYPE_FAILURE,
  CREATE_CIRCUIT_TYPE_REQUEST,
  CREATE_CIRCUIT_TYPE_SUCCESS,
  DOWNLOAD_FILE_FAILURE,
  DOWNLOAD_FILE_REQUEST,
  DOWNLOAD_FILE_SUCCESS,
  FETCH_ALL_CUST_USERS_FAILURE,
  FETCH_ALL_CUST_USERS_REQUEST,
  FETCH_ALL_CUST_USERS_SUCCESS,
  FETCH_CIRCUIT_INFO_FAILURE,
  FETCH_CIRCUIT_INFO_REQUEST,
  FETCH_CIRCUIT_INFO_SUCCESS,
  FETCH_CIRCUIT_TYPE_FAILURE,
  FETCH_CIRCUIT_TYPE_REQUEST,
  FETCH_CIRCUIT_TYPE_SUCCESS,
  FETCH_CUSTOMER_ESCALATION_FAILURE,
  FETCH_CUSTOMER_ESCALATION_REQUEST,
  FETCH_CUSTOMER_ESCALATION_SUCCESS,
  FETCH_CUSTOMER_NOTES_FAILURE,
  FETCH_CUSTOMER_NOTES_REQUEST,
  FETCH_CUSTOMER_NOTES_SUCCESS,
  FETCH_DEVICES_SHORT_FAILURE,
  FETCH_DEVICES_SHORT_REQUEST,
  FETCH_DEVICES_SHORT_SUCCESS,
  FETCH_ESCALATION_FIELDS_FAILURE,
  FETCH_ESCALATION_FIELDS_REQUEST,
  FETCH_ESCALATION_FIELDS_SUCCESS,
  FETCH_NETWORK_NOTES_FAILURE,
  FETCH_NETWORK_NOTES_REQUEST,
  FETCH_NETWORK_NOTES_SUCCESS,
  FETCH_NOTE_TYPES_FAILURE,
  FETCH_NOTE_TYPES_REQUEST,
  FETCH_NOTE_TYPES_SUCCESS,
  FETCH_PATH_LISTING_FAILURE,
  FETCH_PATH_LISTING_REQUEST,
  FETCH_PATH_LISTING_SUCCESS,
  ADD_ESCALATION_REQUEST,
  ADD_ESCALATION_SUCCESS,
  ADD_ESCALATION_FAILURE,
} from '../actions/documentation';

const initialState: IDocumentationstore = {
  customerNotes: null,
  networkNote: null,
  noteType: null,
  isFetching: false,
  customerEscalations: null,
  escalationFields: null,
  distributionList: {
    alerts_and_notifications: [],
    monitoring_alerts: [],
    scheduled_reports: [],
    staffing_changes: [],
  },
  users: [],
  circuitInfoList: [],
  circuitTypes: [],
  documentsList: [],
  deviceList: [],
  isFetchingDevices: false,
  isFetchingTypes: false,
};

export default function customer(state: any = initialState, action: any): any {
  switch (action.type) {
    case FETCH_NOTE_TYPES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_NOTE_TYPES_SUCCESS:
      return Object.assign({}, state, {
        noteType: action.response,
        isFetching: false,
      });

    case FETCH_NOTE_TYPES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        noteType: null,
        isFetching: false,
      });

    case FETCH_CIRCUIT_INFO_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_CIRCUIT_INFO_SUCCESS:
      return Object.assign({}, state, {
        circuitInfoList: action.response,
        isFetching: false,
      });

    case FETCH_CIRCUIT_INFO_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        noteType: null,
        isFetching: false,
      });
    case FETCH_CIRCUIT_TYPE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_CIRCUIT_TYPE_SUCCESS:
      return Object.assign({}, state, {
        circuitTypes: action.response,
        isFetching: false,
      });

    case FETCH_CIRCUIT_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        noteType: null,
        isFetching: false,
      });
    case FETCH_CUSTOMER_NOTES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_CUSTOMER_NOTES_SUCCESS:
      return Object.assign({}, state, {
        customerNotes: action.response,
        isFetching: false,
      });

    case FETCH_CUSTOMER_NOTES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        customerNotes: null,
        isFetching: false,
      });

    case FETCH_NETWORK_NOTES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_NETWORK_NOTES_SUCCESS:
      return Object.assign({}, state, {
        networkNote: action.response && action.response.network_note,
        distributionList:
          action.response && action.response.customer_distribution,
        isFetching: false,
      });

    case FETCH_NETWORK_NOTES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        ne: null,
        isFetching: false,
      });

    case FETCH_CUSTOMER_ESCALATION_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_CUSTOMER_ESCALATION_SUCCESS:
      return Object.assign({}, state, {
        customerEscalations: action.response,
        isFetching: false,
      });

    case FETCH_CUSTOMER_ESCALATION_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        customerEscalations: null,
        isFetching: false,
      });

    case FETCH_ESCALATION_FIELDS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_ESCALATION_FIELDS_SUCCESS:
      return Object.assign({}, state, {
        escalationFields: action.response,
        isFetching: false,
      });

    case FETCH_ESCALATION_FIELDS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        escalationFields: null,
        isFetching: false,
      });

    case FETCH_ALL_CUST_USERS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_ALL_CUST_USERS_SUCCESS:
      return Object.assign({}, state, {
        users: action.response,
        isFetching: false,
      });

    case FETCH_ALL_CUST_USERS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        users: null,
        isFetching: false,
      });
    case FETCH_PATH_LISTING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_PATH_LISTING_SUCCESS:
      return Object.assign({}, state, {
        documentsList: action.response,
        isFetching: false,
      });

    case FETCH_PATH_LISTING_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        documentsList: null,
        isFetching: false,
      });
    case DOWNLOAD_FILE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case DOWNLOAD_FILE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case DOWNLOAD_FILE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        documentsList: null,
        isFetching: false,
      });

    case FETCH_DEVICES_SHORT_REQUEST:
      return Object.assign({}, state, {
        isFetchingDevices: true,
      });

    case FETCH_DEVICES_SHORT_SUCCESS:
      return Object.assign({}, state, {
        deviceList: action.response,
        isFetchingDevices: false,
      });

    case FETCH_DEVICES_SHORT_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingDevices: false,
      });

      case CREATE_CIRCUIT_TYPE_REQUEST:
        return Object.assign({}, state, {
          isFetchingDevices: true,
        });
  
      case CREATE_CIRCUIT_TYPE_SUCCESS:
        return Object.assign({}, state, {
          isFetchingDevices: false,
        });
  
      case CREATE_CIRCUIT_TYPE_FAILURE:
        return Object.assign({}, state, {
          error: 'some error',
          isFetchingDevices: false,
        });
        case ADD_ESCALATION_REQUEST:
          return Object.assign({}, state, {
            isFetching: true,
          });
    
        case ADD_ESCALATION_SUCCESS:
          return Object.assign({}, state, {
            isFetching: false,
          });
    
        case ADD_ESCALATION_FAILURE:
          return Object.assign({}, state, {
            error: 'some error',
            isFetching: false,
          });

    default:
      return state;
  }
}
