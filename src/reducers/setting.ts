import {
  EDIT_DOC_SETTING_FAILURE,
  EDIT_DOC_SETTING_REQUEST,
  EDIT_DOC_SETTING_SUCCESS,
  EDIT_SERVICE_CATALOG_FAILURE,
  EDIT_SERVICE_CATALOG_REQUEST,
  EDIT_SERVICE_CATALOG_SUCCESS,
  EDIT_SERVICE_TYPE_FAILURE,
  EDIT_SERVICE_TYPE_REQUEST,
  EDIT_SERVICE_TYPE_SUCCESS,
  FETCH_DOC_SETTING_FAILURE,
  FETCH_DOC_SETTING_REQUEST,
  FETCH_DOC_SETTING_SUCCESS,
  FETCH_SERVICE_CATALOG_CAT_FAILURE,
  FETCH_SERVICE_CATALOG_CAT_REQUEST,
  FETCH_SERVICE_CATALOG_CAT_SUCCESS,
  FETCH_SERVICE_TYPE_FAILURE,
  FETCH_SERVICE_TYPE_REQUEST,
  FETCH_SERVICE_TYPE_SUCCESS,
  GET_SMART_NET_ATTACH_REQUEST,
  GET_SMART_NET_ATTACH_SUCCESS,
  GET_SMART_NET_ATTACH_FAILURE,
  GET_SMART_NET_SETTING_REQUEST,
  GET_SMART_NET_SETTING_SUCCESS,
  GET_SMART_NET_SETTING_FAILURE,
  GET_SMART_NET_ATTACH_LIST_REQUEST,
  GET_SMART_NET_ATTACH_LIST_SUCCESS,
  GET_SMART_NET_ATTACH_LIST_FAILURE,
  GET_INGRAM_CRED_REQUEST,
  GET_INGRAM_CRED_SUCCESS,
  GET_INGRAM_CRED_FAILURE,
  GET_P_O_SETTING_FAILURE,
  GET_P_O_SETTING_REQUEST,
  GET_P_O_SETTING_SUCCESS,
  GET_SHIPMENTS_REQUEST,
  GET_SHIPMENTS_SUCCESS,
  GET_SHIPMENTS_FAILURE,
  FETCH_AGREEMENT_MARGIN_REQUEST,
  FETCH_AGREEMENT_MARGIN_SUCCESS,
  FETCH_AGREEMENT_MARGIN_FAILURE,
  GET_RECIEVING_STATUS_REQUEST,
  GET_RECIEVING_STATUS_SUCCESS,
  GET_RECIEVING_STATUS_FAILURE,
  GET_BILLING_STATUSES_REQUEST,
  GET_BILLING_STATUSES_SUCCESS,
  GET_BILLING_STATUSES_FAILURE,
  GET_PAX8_CRED_REQUEST,
  GET_PAX8_CRED_SUCCESS,
  GET_PAX8_CRED_FAILURE,
  GET_CUSTOMER_TYPES_REQUEST,
  GET_CUSTOMER_TYPES_SUCCESS,
  GET_CUSTOMER_TYPES_FAILURE,
  GET_CUSTOMER_STATUSES_REQUEST,
  GET_CUSTOMER_STATUSES_SUCCESS,
  GET_CUSTOMER_STATUSES_FAILURE,
  GET_CW_TEAM_ROLES_REQUEST,
  GET_CW_TEAM_ROLES_SUCCESS,
  GET_CW_TEAM_ROLES_FAILURE,
} from "../actions/setting";

const initialState: ISettingsStore = {
  serviceCatalogCategories: null,
  error: null,
  isFetching: false,
  isFetchingType: false,
  isFetchingCWTeamRoles: false,
  isFetchingPAX8Credentials: false,
  isFetchingCustomerTypes: false,
  isFetchingCustomerStatuses: false,
  isFetchingShipments: false,
  serviceType: null,
  isFetchingDocSet: false,
  docSetting: null,
  smartNetSetting: null,
  purchaseOrderSetting: null,
  agreementMargin: null,
  receivedStatusList: [],
  attachmentsList: [],
  cwTeamRoles: [],
  customerTypes: [],
  customerStatuses: [],
  ingramCredentials: {
    client_id: "",
    client_secret: "",
    ingram_customer_id: "",
  },
  pax8Credentials: {
    client_id: "",
    client_secret: "",
    vendors: [],
  },
  shipments: [],
  billingStatusOptions: [],
};

export default function setting(state: any = initialState, action: any): any {
  switch (action.type) {
    case FETCH_SERVICE_TYPE_REQUEST:
      return Object.assign({}, state, {
        isFetchingType: true,
      });

    case FETCH_SERVICE_TYPE_SUCCESS:
      return Object.assign({}, state, {
        serviceType: action.response,
        isFetchingType: false,
      });

    case FETCH_SERVICE_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingType: false,
      });

    case FETCH_AGREEMENT_MARGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_AGREEMENT_MARGIN_SUCCESS:
      return Object.assign({}, state, {
        agreementMargin: action.response,
        isFetching: false,
      });

    case FETCH_AGREEMENT_MARGIN_FAILURE:
      return Object.assign({}, state, {
        error: "Unable to fetch agreement margin settings",
        isFetching: false,
      });

    case EDIT_SERVICE_TYPE_REQUEST:
      return Object.assign({}, state, {
        isFetchingType: true,
      });

    case EDIT_SERVICE_TYPE_SUCCESS:
      return Object.assign({}, state, {
        //serviceType: action.response,
        isFetchingType: false,
      });

    case EDIT_SERVICE_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingType: false,
      });

    case FETCH_DOC_SETTING_REQUEST:
      return Object.assign({}, state, {
        isFetchingDocSet: true,
      });

    case FETCH_DOC_SETTING_SUCCESS:
      return Object.assign({}, state, {
        docSetting: action.response,
        isFetchingDocSet: false,
      });

    case FETCH_DOC_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingDocSet: false,
      });

    case EDIT_DOC_SETTING_REQUEST:
      return Object.assign({}, state, {
        isFetchingDocSet: true,
      });

    case EDIT_DOC_SETTING_SUCCESS:
      return Object.assign({}, state, {
        docSetting: action.response,
        isFetchingDocSet: false,
      });

    case EDIT_DOC_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingDocSet: false,
      });

    case FETCH_SERVICE_CATALOG_CAT_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_SERVICE_CATALOG_CAT_SUCCESS:
      return Object.assign({}, state, {
        serviceCatalogCategories: action.response,
        isFetching: false,
      });

    case FETCH_SERVICE_CATALOG_CAT_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case EDIT_SERVICE_CATALOG_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case EDIT_SERVICE_CATALOG_SUCCESS:
      return Object.assign({}, state, {
        //serviceCatalog: action.response.results,
        isFetching: false,
      });

    case EDIT_SERVICE_CATALOG_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        user: null,
        isFetching: false,
      });

    case GET_SMART_NET_ATTACH_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_SMART_NET_ATTACH_SUCCESS:
      return Object.assign({}, state, {
        //serviceCatalog: action.response.results,
        isFetching: false,
      });

    case GET_SMART_NET_ATTACH_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case GET_SMART_NET_ATTACH_LIST_REQUEST:
      return Object.assign({}, state, {
        isFetchingType: true,
      });

    case GET_SMART_NET_ATTACH_LIST_SUCCESS:
      return Object.assign({}, state, {
        attachmentsList: action.response,
        isFetchingType: false,
      });

    case GET_SMART_NET_ATTACH_LIST_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingType: false,
      });

    case GET_RECIEVING_STATUS_REQUEST:
      return state;

    case GET_RECIEVING_STATUS_SUCCESS:
      return Object.assign({}, state, {
        receivedStatusList: action.response.map((c) => ({
          value: c.id,
          label: c.status,
        })),
      });

    case GET_RECIEVING_STATUS_FAILURE:
      return Object.assign({}, state, {
        error: "Unable to fetch Receiving status",
      });

    case GET_BILLING_STATUSES_REQUEST:
      return state;

    case GET_BILLING_STATUSES_SUCCESS:
      return Object.assign({}, state, {
        billingStatusOptions: action.response.map((c) => ({
          value: c.billing_status_crm_id,
          label: c.billing_status_name,
        })),
      });

    case GET_BILLING_STATUSES_FAILURE:
      return Object.assign({}, state, {
        error: "Unable to fetch Billing status",
      });

    case GET_CUSTOMER_TYPES_REQUEST:
      return Object.assign({}, state, {
        customerTypes: [],
        isFetchingCustomerTypes: true,
      });

    case GET_CUSTOMER_TYPES_SUCCESS:
      return Object.assign({}, state, {
        isFetchingCustomerTypes: false,
        customerTypes: action.response.map((c) => ({
          value: c.customer_type_id,
          label: c.customer_type_name,
        })),
      });

    case GET_CUSTOMER_TYPES_FAILURE:
      return Object.assign({}, state, {
        isFetchingCustomerTypes: false,
        error: "Unable to fetch customer types",
      });

    case GET_CW_TEAM_ROLES_REQUEST:
      return Object.assign({}, state, {
        cwTeamRoles: [],
        isFetchingCWTeamRoles: true,
      });

    case GET_CW_TEAM_ROLES_SUCCESS:
      return Object.assign({}, state, {
        isFetchingCWTeamRoles: false,
        cwTeamRoles: action.response.map((c) => ({
          value: c.role_crm_id,
          label: c.role_name,
        })),
      });

    case GET_CW_TEAM_ROLES_FAILURE:
      return Object.assign({}, state, {
        isFetchingCWTeamRoles: false,
        error: "Unable to fetch CW team roles",
      });

    case GET_CUSTOMER_STATUSES_REQUEST:
      return Object.assign({}, state, {
        customerStatuses: [],
        isFetchingCustomerStatuses: true,
      });

    case GET_CUSTOMER_STATUSES_SUCCESS:
      return Object.assign({}, state, {
        isFetchingCustomerStatuses: false,
        customerStatuses: action.response.map((c) => ({
          value: c.customer_status_crm_id,
          label: c.customer_status_name,
        })),
      });

    case GET_CUSTOMER_STATUSES_FAILURE:
      return Object.assign({}, state, {
        isFetchingCustomerStatuses: false,
        error: "Unable to fetch customer statuses",
      });

    case GET_SMART_NET_SETTING_REQUEST:
      return Object.assign({}, state, {
        v: true,
      });

    case GET_SMART_NET_SETTING_SUCCESS:
      return Object.assign({}, state, {
        smartNetSetting: action.response,
        isFetching: false,
      });

    case GET_SMART_NET_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case GET_INGRAM_CRED_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_INGRAM_CRED_SUCCESS:
      return Object.assign({}, state, {
        ingramCredentials: action.response,
        isFetching: false,
      });

    case GET_INGRAM_CRED_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case GET_PAX8_CRED_REQUEST:
      return Object.assign({}, state, {
        isFetchingPAX8Credentials: true,
      });

    case GET_PAX8_CRED_SUCCESS:
      return Object.assign({}, state, {
        pax8Credentials: action.response,
        isFetchingPAX8Credentials: false,
      });

    case GET_PAX8_CRED_FAILURE:
      return Object.assign({}, state, {
        error: "Error fetching PAX8 Credentials!",
        isFetchingPAX8Credentials: false,
      });

    case GET_P_O_SETTING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_P_O_SETTING_SUCCESS:
      return Object.assign({}, state, {
        purchaseOrderSetting: action.response,
        isFetching: false,
      });

    case GET_P_O_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case GET_SHIPMENTS_REQUEST:
      return Object.assign({}, state, {
        isFetchingShipments: true,
      });

    case GET_SHIPMENTS_SUCCESS:
      return Object.assign({}, state, {
        shipments: action.response.map((c) => ({
          value: c.id,
          label: c.name,
        })),
        isFetchingShipments: false
      });

    case GET_SHIPMENTS_FAILURE:
      return Object.assign({}, state, {
        error: "Error fetching shipments",
        isFetchingShipments: false,
      });

    default:
      return state;
  }
}
