import {
  TEST_INTEGRATION_FAILURE,
  TEST_INTEGRATION_REQUEST,
  TEST_INTEGRATION_SUCCESS,
} from '../../actions/integration';
import {
  CREATE_PROVIDER_INTEGRATIONS_FAILURE,
  CREATE_PROVIDER_INTEGRATIONS_REQUEST,
  CREATE_PROVIDER_INTEGRATIONS_SUCCESS,
  FETCH_ALL_PROVIDER_INTEGRATIONS_FAILURE,
  FETCH_ALL_PROVIDER_INTEGRATIONS_REQUEST,
  FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS,
  FETCH_PROVIDER_BOARD_STATUS_FAILURE,
  FETCH_PROVIDER_BOARD_STATUS_REQUEST,
  FETCH_PROVIDER_BOARD_STATUS_SUCCESS,
  FETCH_PROVIDER_CONFIG_STATUS_FAILURE,
  FETCH_PROVIDER_CONFIG_STATUS_REQUEST,
  FETCH_PROVIDER_CONFIG_STATUS_SUCCESS,
  FETCH_PROVIDER_EXTRA_CONFIGS_FAILURE,
  FETCH_PROVIDER_EXTRA_CONFIGS_REQUEST,
  FETCH_PROVIDER_EXTRA_CONFIGS_SUCCESS,
  FETCH_PROVIDER_INTEGRATIONS_FAILURE,
  FETCH_PROVIDER_INTEGRATIONS_REQUEST,
  FETCH_PROVIDER_INTEGRATIONS_SUCCESS,
  FETCH_PROVIDER_MANAGED_BOARD_STATUS_FAILURE,
  FETCH_PROVIDER_MANAGED_BOARD_STATUS_REQUEST,
  FETCH_PROVIDER_MANAGED_BOARD_STATUS_SUCCESS,
} from '../../actions/provider/integration';

const initialState: IProviderIntegrationStore = {
  configStatus: null,
  providerIntegration: null,
  providerExtraConfigs: null,
  allProviderIntegrations: null,
  boardStatusList: null,
  boardManagedStatusList: null,
  isFetching: 0,
  isFetchingExtraConfig: false,
};

export default function customer(
  state: IProviderIntegrationStore = initialState,
  action: any
): IProviderIntegrationStore {
  switch (action.type) {
    case FETCH_PROVIDER_CONFIG_STATUS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_PROVIDER_CONFIG_STATUS_SUCCESS:
      return Object.assign({}, state, {
        configStatus: action.response,
        isFetching: false,
      });

    case FETCH_PROVIDER_CONFIG_STATUS_FAILURE:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case CREATE_PROVIDER_INTEGRATIONS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case CREATE_PROVIDER_INTEGRATIONS_SUCCESS:
      return Object.assign({}, state, {
        providerIntegration: action.response,
        isFetching: false,
      });

    case CREATE_PROVIDER_INTEGRATIONS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case FETCH_PROVIDER_INTEGRATIONS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_PROVIDER_INTEGRATIONS_SUCCESS:
      return Object.assign({}, state, {
        providerIntegration: action.response,
        isFetching: false,
      });
    case FETCH_PROVIDER_INTEGRATIONS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });
    case FETCH_ALL_PROVIDER_INTEGRATIONS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS:
      return Object.assign({}, state, {
        allProviderIntegrations: action.response,
        isFetching: false,
      });
    case FETCH_ALL_PROVIDER_INTEGRATIONS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case FETCH_PROVIDER_EXTRA_CONFIGS_REQUEST:
      return Object.assign({}, state, {
        providerExtraConfigs: action.response,
        isFetchingExtraConfig: true,
      });

    case FETCH_PROVIDER_EXTRA_CONFIGS_SUCCESS:
      return Object.assign({}, state, {
        providerExtraConfigs: action.response,
        isFetchingExtraConfig: false,
      });

    case FETCH_PROVIDER_EXTRA_CONFIGS_FAILURE:
      return Object.assign({}, state, {
        providerExtraConfigs: action.response,
        isFetchingExtraConfig: false,
      });

    case FETCH_PROVIDER_MANAGED_BOARD_STATUS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_PROVIDER_MANAGED_BOARD_STATUS_SUCCESS:
      return Object.assign({}, state, {
        boardManagedStatusList: action.response,
        isFetching: false,
      });

    case FETCH_PROVIDER_MANAGED_BOARD_STATUS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case FETCH_PROVIDER_BOARD_STATUS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_PROVIDER_BOARD_STATUS_SUCCESS:
      return Object.assign({}, state, {
        boardStatusList: action.response,
        isFetching: false,
      });
    case FETCH_PROVIDER_BOARD_STATUS_FAILURE:
      return Object.assign({}, state, {
        error: 'error',
        isFetching: false,
      });

    case TEST_INTEGRATION_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case TEST_INTEGRATION_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });
    case TEST_INTEGRATION_FAILURE:
      return Object.assign({}, state, {
        error: 'error',
        isFetching: false,
      });
    default:
      return state;
  }
}
