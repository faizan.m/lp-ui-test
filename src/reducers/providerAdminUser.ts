import {
  GET_ACCESS_FEATURES_LIST_SUCCESS,
  FETCH_PROVIDER_ADMIN_FAILURE,
  FETCH_PROVIDER_ADMIN_REQUEST,
  FETCH_PROVIDER_ADMIN_SUCCESS,
  PROVIDER_ADMIN_ADD_FAILURE,
  PROVIDER_ADMIN_ADD_SUCCESS,
  PROVIDER_ADMIN_DELETE_FAILURE,
  PROVIDER_ADMIN_DELETE_REQUEST,
  PROVIDER_ADMIN_DELETE_SUCCESS,
  PROVIDER_ADMIN_EDIT_FAILURE,
  PROVIDER_ADMIN_EDIT_SUCCESS,
} from "../actions/providerAdminUser";
const initialState: IProviderAdminUserStore = {
  providerAdminUser: null,
  providerAdminUsers: null,
  error: null,
  isFetchingAdmins: false,
  isAction: false,
  accessFeatures: null,
};

export default function providerAdminUser(
  state: IProviderAdminUserStore = initialState,
  action: any
): IProviderAdminUserStore {
  switch (action.type) {
    case FETCH_PROVIDER_ADMIN_REQUEST:
      return Object.assign({}, state, {
        isFetchingAdmins: true,
      });

    case FETCH_PROVIDER_ADMIN_SUCCESS:
      return Object.assign({}, state, {
        providerAdminUsers: action.response,
        isFetchingAdmins: false,
      });

    case FETCH_PROVIDER_ADMIN_FAILURE:
      return Object.assign({}, state, {
        providerAdminUsers: null,
        error: "some error",
        isFetchingAdmins: false,
      });

    case PROVIDER_ADMIN_ADD_SUCCESS:
      return Object.assign({}, state, {
        providerAdminUser: action.response,
      });

    case PROVIDER_ADMIN_ADD_FAILURE:
      return Object.assign({}, state, {
        providerAdminUser: null,
        error: "some error",
        errorList: action.response,
      });
    case PROVIDER_ADMIN_EDIT_SUCCESS:
      return Object.assign({}, state, {
        providerAdminUser: action.response,
      });

    case PROVIDER_ADMIN_EDIT_FAILURE:
      return Object.assign({}, state, {
        providerAdminUser: null,
        error: "some error",
        errorList: action.response,
      });

    case PROVIDER_ADMIN_DELETE_REQUEST:
      return Object.assign({}, state, {
        isAction: true,
      });
    case PROVIDER_ADMIN_DELETE_SUCCESS:
      return Object.assign({}, state, {
        isAction: false,
      });

    case PROVIDER_ADMIN_DELETE_FAILURE:
      return Object.assign({}, state, {
        isAction: false,
        error: "some error",
        errorList: action.response,
      });

    case GET_ACCESS_FEATURES_LIST_SUCCESS:
      return Object.assign({}, state, {
        accessFeatures: action.response,
      });
      
    default:
      return state;
  }
}
