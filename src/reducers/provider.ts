import {
  EDIT_PROVIDER_PROFILE_FAILURE,
  EDIT_PROVIDER_PROFILE_SUCCESS,
  FETCH_LOGO_FAILURE,
  FETCH_LOGO_REQUEST,
  FETCH_LOGO_SUCCESS,
  FETCH_PROVIDER_FAILURE,
  FETCH_PROVIDER_PROFILE_SUCCESS,
  FETCH_PROVIDER_REQUEST,
  FETCH_PROVIDER_SUCCESS,
  FETCH_PROVIDERS_FAILURE,
  FETCH_PROVIDERS_REQUEST,
  FETCH_PROVIDERS_SUCCESS,
  PROVIDER_CREATE_FAILURE,
  PROVIDER_CREATE_SUCCESS,
  PROVIDER_EDIT_FAILURE,
  PROVIDER_EDIT_SUCCESS,
  FETCH_TIMEZONES_REQUEST,
  FETCH_TIMEZONES_SUCCESS,
  FETCH_TIMEZONES_FAILURE,
} from '../actions/provider';
const initialState: IProviderStore = {
  provider: null,
  providers: null,
  error: null,
  isProvidersFetching: false,
  isProviderFetching: false,
  themeColor: '',
  logo_url: '',
  updated_on: '',
  customer_instance_id: '',
  company_url: '',
  enable_url: false,
  timezones:[]
};

export default function provider(
  state: IProviderStore = initialState,
  action: any
): IProviderStore {
  switch (action.type) {
    case FETCH_PROVIDER_REQUEST:
      return Object.assign({}, state, {
        isProviderFetching: true,
      });

    case FETCH_PROVIDER_SUCCESS:
      return Object.assign({}, state, {
        provider: action.response,
        isProviderFetching: false,
      });

    case FETCH_PROVIDER_FAILURE:
      return Object.assign({}, state, {
        providers: null,
        error: 'some error',
        isProviderFetching: false,
      });

    case FETCH_PROVIDERS_REQUEST:
      return Object.assign({}, state, {
        isProvidersFetching: true,
      });

    case FETCH_PROVIDERS_SUCCESS:
      return Object.assign({}, state, {
        providers: action.response,
        isProvidersFetching: false,
      });

    case FETCH_PROVIDERS_FAILURE:
      return Object.assign({}, state, {
        providers: null,
        error: 'some error',
        isProvidersFetching: false,
      });

    case PROVIDER_CREATE_SUCCESS:
      return Object.assign({}, state, {
        provider: action.response,
      });

    case PROVIDER_CREATE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        errorList: action.errorList,
      });

    case PROVIDER_EDIT_SUCCESS:
      return Object.assign({}, state, {
        provider: action.response,
      });

    case PROVIDER_EDIT_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        errorList: action.errorList,
      });

    case FETCH_PROVIDER_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        themeColor: action.response.colour_code,
        company_url: action.response.company_url,
        enable_url: action.response.enable_url,
        logo_url: action.response.logo,
        updated_on: action.response.updated_on,
        customer_instance_id: action.response.customer_instance_id,
      });

    case EDIT_PROVIDER_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        company_url: action.response.company_url,
        enable_url: action.response.enable_url,
        themeColor: action.response.colour_code,
        logo_url: action.response.logo,
        updated_on: action.response.updated_on,
      });

    case EDIT_PROVIDER_PROFILE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        errorList: action.errorList,
      });

    case FETCH_LOGO_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_LOGO_SUCCESS:
      return Object.assign({}, state, {
        themeColor: action.response.colour_code,
        logo_url: action.response.logo,
        updated_on: new Date(),
      });

    case FETCH_LOGO_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        user: null,
        isFetching: false,
      });
      case FETCH_TIMEZONES_REQUEST:
        return Object.assign({}, state, {
          isFetching: true,
        });
  
      case FETCH_TIMEZONES_SUCCESS:
        return Object.assign({}, state, {
          timezones: action.response,
          isFetching: false,
        });
  
      case FETCH_TIMEZONES_FAILURE:
        return Object.assign({}, state, {
          error: 'some error',
          isFetching: false,
        });

    default:
      return state;
  }
}
