import {
  DOWNLOAD_RENEWAL_SUCCESS,
  FETCH_COMPLETED_DEVICES_FAILURE,
  FETCH_COMPLETED_DEVICES_REQUEST,
  FETCH_COMPLETED_DEVICES_SUCCESS,
  FETCH_NEW_RENEWALS_FAILURE,
  FETCH_NEW_RENEWALS_REQUEST,
  FETCH_NEW_RENEWALS_SUCCESS,
  FETCH_PENDING_DEVICES_FAILURE,
  FETCH_PENDING_DEVICES_REQUEST,
  FETCH_PENDING_DEVICES_SUCCESS,
  FETCH_RENEWAL_TYPE_FAILURE,
  FETCH_RENEWAL_TYPE_SUCCESS,
  FETCH_SERVICE_LEVEL_FAILURE,
  FETCH_SERVICE_LEVEL_SUCCESS,
  POST_RENEWAL_FAILURE,
  POST_RENEWAL_SUCCESS,
  SET_RENEWAL_CUSTOMER_ID,
} from '../actions/renewal';

import {
  PROVIDER_RENEWAL_FETCH_FAILURE,
  PROVIDER_RENEWAL_FETCH_REQUEST,
  PROVIDER_RENEWAL_FETCH_SUCCESS,
} from '../actions/provider';

const initialState: IRenewalStore = {
  newRenewals: null,
  pending: null,
  completed: null,
  renewalTypes: null,
  serviceLevelTypes: null,
  requestResponce: null,
  renewalCustomerList: null,
  renewalCustomerId: null,
  isProviderRenwalFetching: false,
  isCompletedFetching: false,
  isPendingFetching: false,
  isNewDevicesFetching: false,
};

export default function customer(
  state: IRenewalStore = initialState,
  action: any
): IRenewalStore {
  switch (action.type) {
    case FETCH_NEW_RENEWALS_REQUEST:
      return Object.assign({}, state, {
        isNewDevicesFetching: true,
      });

    case FETCH_NEW_RENEWALS_SUCCESS:
      return Object.assign({}, state, {
        newRenewals: action.response,
        isNewDevicesFetching: false,
      });

    case FETCH_NEW_RENEWALS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isNewDevicesFetching: false,
      });

    case FETCH_RENEWAL_TYPE_SUCCESS:
      return Object.assign({}, state, {
        renewalTypes: action.response,
      });

    case FETCH_RENEWAL_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
      });

    case FETCH_SERVICE_LEVEL_SUCCESS:
      return Object.assign({}, state, {
        serviceLevelTypes: action.response,
      });

    case FETCH_PENDING_DEVICES_REQUEST:
      return Object.assign({}, state, {
        isPendingFetching: true,
      });

    case FETCH_PENDING_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        pending: action.response,
        isPendingFetching: false,
      });

    case FETCH_PENDING_DEVICES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isPendingFetching: false,
      });

    case FETCH_COMPLETED_DEVICES_REQUEST:
      return Object.assign({}, state, {
        isCompletedFetching: true,
      });

    case FETCH_COMPLETED_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        completed: action.response,
        isCompletedFetching: false,
      });

    case FETCH_COMPLETED_DEVICES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isCompletedFetching: false,
      });

    case FETCH_SERVICE_LEVEL_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
      });

    case POST_RENEWAL_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
      });

    case POST_RENEWAL_SUCCESS:
      return Object.assign({}, state, {
        requestResponce: action.response,
      });

    case PROVIDER_RENEWAL_FETCH_REQUEST:
      return Object.assign({}, state, {
        isProviderRenwalFetching: true,
      });

    case PROVIDER_RENEWAL_FETCH_SUCCESS:
      return Object.assign({}, state, {
        isProviderRenwalFetching: false,
        renewalCustomerList: action.response,
      });

    case PROVIDER_RENEWAL_FETCH_FAILURE:
      return Object.assign({}, state, {
        isProviderRenwalFetching: false,
      });
    case SET_RENEWAL_CUSTOMER_ID:
      return Object.assign({}, state, {
        renewalCustomerId: action.id,
      });

    case DOWNLOAD_RENEWAL_SUCCESS:
      const url = window.URL.createObjectURL(new Blob([action.response]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'renewal-pending-report.csv');
      document.body.appendChild(link);
      link.click();

      return Object.assign({}, state, {
        error: 'some error',
      });
    default:
      return state;
  }
}
