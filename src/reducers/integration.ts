import {
  FETCH_DEFAULT_MAPPING_FAILURE,
  FETCH_DEFAULT_MAPPING_REQUEST,
  FETCH_DEFAULT_MAPPING_SUCCESS,
  FETCH_GROUPS_CAT_REQUEST,
  FETCH_GROUPS_CAT_SUCCESS,
  FETCH_GROUPS_REQUEST,
  FETCH_GROUPS_SUCCESS,
  FETCH_INTEGRATIONS_CAT_SUCCESS,
  FETCH_INTEGRATIONS_CISCO_SUCCESS,
  FETCH_INTEGRATIONS_SUCCESS,
  TEST_INTEGRATION_SUCCESS,
} from '../actions/integration';
import {
  FETCH_TER_MEMBERS_FAILURE,
  FETCH_TER_MEMBERS_REQUEST,
  FETCH_TER_MEMBERS_SUCCESS,
  FETCH_TERRITORIES_FAILURE,
  FETCH_TERRITORIES_REQUEST,
  FETCH_TERRITORIES_SUCCESS,
  SAVE_MANAGER_FAILURE,
  SAVE_MANAGER_REQUEST,
  SAVE_MANAGER_SUCCESS,
} from '../actions/provider/integration';

const initialState: IIntegrationStore = {
  integrations: null,
  integrationsCatgories: null,
  integrationVerified: false,
  manufacturerIntegrations: null,
  isFetchingGroupList: false,
  defaultKeyMapping: null,
  terretories: null,
  isFetchingTerretories: false,
  terretoryMembers: null,
  isFetchingMembers: false,
};

export default function integration(
  state: IIntegrationStore = initialState,
  action: any
): IIntegrationStore {
  switch (action.type) {
    case FETCH_INTEGRATIONS_SUCCESS:
      return Object.assign({}, state, {
        integrations: action.response,
      });

    case FETCH_INTEGRATIONS_CISCO_SUCCESS:
      return Object.assign({}, state, {
        manufacturerIntegrations: action.response,
      });

    case FETCH_INTEGRATIONS_CAT_SUCCESS:
      return Object.assign({}, state, {
        integrationsCatgories: action.response,
      });

    case TEST_INTEGRATION_SUCCESS:
      return Object.assign({}, state, {
        integrationVerified: action.response.verified,
      });

    case FETCH_GROUPS_REQUEST:
      return Object.assign({}, state, {
        isFetchingGroupList: true,
      });

    case FETCH_GROUPS_CAT_REQUEST:
      return Object.assign({}, state, {
        isFetchingGroupList: true,
      });
    case FETCH_GROUPS_SUCCESS:
      return Object.assign({}, state, {
        isFetchingGroupList: false,
      });

    case FETCH_GROUPS_CAT_SUCCESS:
      return Object.assign({}, state, {
        isFetchingGroupList: false,
      });
    case FETCH_DEFAULT_MAPPING_REQUEST:
      return Object.assign({}, state, {
        isFetchingGroupList: true,
      });
    case FETCH_DEFAULT_MAPPING_SUCCESS:
      return Object.assign({}, state, {
        isFetchingGroupList: false,
        defaultKeyMapping: action.response,
      });

    case FETCH_DEFAULT_MAPPING_FAILURE:
      return Object.assign({}, state, {
        isFetchingGroupList: false,
      });

    case FETCH_TERRITORIES_REQUEST:
      return Object.assign({}, state, {
        isFetchingTerretories: true,
      });
    case FETCH_TERRITORIES_SUCCESS:
      return Object.assign({}, state, {
        isFetchingTerretories: false,
        terretories: action.response,
      });

    case FETCH_TERRITORIES_FAILURE:
      return Object.assign({}, state, {
        isFetchingTerretories: false,
      });
    case FETCH_TER_MEMBERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingMembers: true,
      });
    case FETCH_TER_MEMBERS_SUCCESS:
      return Object.assign({}, state, {
        isFetchingMembers: false,
        terretoryMembers: action.response,
      });

    case FETCH_TER_MEMBERS_FAILURE:
      return Object.assign({}, state, {
        isFetchingMembers: false,
      });

    case SAVE_MANAGER_REQUEST:
      return Object.assign({}, state, {
        isFetchingTerretories: true,
      });
    case SAVE_MANAGER_SUCCESS:
      return Object.assign({}, state, {
        isFetchingTerretories: false,
      });

    case SAVE_MANAGER_FAILURE:
      return Object.assign({}, state, {
        isFetchingTerretories: false,
      });
    default:
      return state;
  }
}
