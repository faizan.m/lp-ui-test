import {
  FETCH_COLLECTORS_FAILURE,
  FETCH_COLLECTORS_REQUEST,
  FETCH_COLLECTORS_SUCCESS,
  FETCH_CUS_COLLTS_FAILURE,
  FETCH_CUS_COLLTS_REQUEST,
  FETCH_CUS_COLLTS_SUCCESS,
  UPDATE_CUS_COLLTS_FAILURE,
  UPDATE_CUS_COLLTS_REQUEST,
  UPDATE_CUS_COLLTS_SUCCESS,
  FETCH_C_SERVICE_TYPE_REQUEST,
  FETCH_C_SERVICE_TYPE_SUCCESS,
  FETCH_C_SERVICE_TYPE_FAILURE,
  SAVE_COLL_MAPPING_REQUEST,
  SAVE_COLL_MAPPING_SUCCESS,
  SAVE_COLL_MAPPING_FAILURE,
  UPDATE_CL_MAPPING_REQUEST,
  UPDATE_CL_MAPPING_SUCCESS,
  UPDATE_CL_MAPPING_FAILURE,
  FETCH_D_CL_MAPPING_REQUEST,
  FETCH_D_CL_MAPPING_SUCCESS,
  FETCH_D_CL_MAPPING_FAILURE,
  FETCH_COLLECTOR_REQUEST,
  FETCH_COLLECTOR_SUCCESS,
  FETCH_COLLECTOR_FAILURE,
  FETCH_QUERY_REQUEST,
  FETCH_QUERY_SUCCESS,
  FETCH_QUERY_FAILURE,
  FETCH_QUERIES_REQUEST,
  FETCH_QUERIES_SUCCESS,
  FETCH_QUERIES_FAILURE,
  GET_URL_REQUEST,
  GET_URL_SUCCESS,
  GET_URL_FAILURE,
  DELETE_COLLECTOR_REQUEST,
  DELETE_COLLECTOR_SUCCESS,
  DELETE_COLLECTOR_FAILURE,
  REPORT_COMPLIANCE_REQUEST,
  REPORT_COMPLIANCE_SUCCESS,
  REPORT_COMPLIANCE_FAILURE,
} from '../actions/collector';
import { isEqual } from 'lodash';
const initialState: ICollectorStore = {
  collectors: [],
  customerCollectors: [],
  customerCollector: null,
  isFetchingCollectors: false,
  isFetchingCustomerCollectors: false,
  isFetchingCustomerCollector: false,
  isFetching: false,
  serviceTypes: [],
  isFetchingServiceTypes: false,
  settings: null,
  queryList: [],
  query: null,
  isFetchingQueryList: false,
  isFetchingQuery: false,
  isFetchingURL: false,
  isReportDownloading: false,
  downloadURL: ''
};

export default function customer(
  state: ICollectorStore = initialState,
  action: any
): ICollectorStore {
  switch (action.type) {
    case FETCH_CUS_COLLTS_REQUEST:
      return Object.assign({}, state, {
        isFetchingCustomerCollectors: true,
      });

    case FETCH_CUS_COLLTS_SUCCESS:
      return Object.assign({}, state, {
        customerCollectors: action.response,
        isFetchingCustomerCollectors: false,
      });

    case FETCH_CUS_COLLTS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCustomerCollectors: false,
      });

    case FETCH_COLLECTOR_REQUEST:
      return Object.assign({}, state, {
        customerCollector: null,
        isFetchingCustomerCollector: true,
      });

    case FETCH_COLLECTOR_SUCCESS:
      return Object.assign({}, state, {
        customerCollector: action.response,
        isFetchingCustomerCollector: false,
      });

    case FETCH_COLLECTOR_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCustomerCollector: false,
      });

    case FETCH_COLLECTORS_REQUEST:
      return Object.assign({}, state, {
        isFetchingCollectors: true,
      });

    case FETCH_COLLECTORS_SUCCESS:
      return Object.assign({}, state, {
        collectors: action.response,
        isFetchingCollectors: false,
      });

    case FETCH_COLLECTORS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCollectors: false,
      });

    case FETCH_C_SERVICE_TYPE_REQUEST:
      return Object.assign({}, state, {
        isFetchingServiceTypes: true,
      });

    case FETCH_C_SERVICE_TYPE_SUCCESS:
      return Object.assign({}, state, {
        serviceTypes: action.response,
        isFetchingServiceTypes: false,
      });

    case FETCH_C_SERVICE_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingServiceTypes: false,
      });


    case SAVE_COLL_MAPPING_REQUEST:
      return Object.assign({}, state, {
        isFetchingServiceTypes: true,
      });

    case SAVE_COLL_MAPPING_SUCCESS:
      return Object.assign({}, state, {
        isFetchingServiceTypes: false,
      });

    case SAVE_COLL_MAPPING_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingServiceTypes: false,
      });

    case UPDATE_CUS_COLLTS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case UPDATE_CUS_COLLTS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case UPDATE_CUS_COLLTS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    case UPDATE_CL_MAPPING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case UPDATE_CL_MAPPING_SUCCESS:
      return Object.assign({}, state, {
        settings: action.response.settings,
        isFetching: false,
      });

    case UPDATE_CL_MAPPING_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    case FETCH_D_CL_MAPPING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_D_CL_MAPPING_SUCCESS:
      return Object.assign({}, state, {
        settings: action.response.settings && !isEqual(action.response.settings, {}) ? action.response.settings : { "solarwinds": { "device_mappings": { null: { "cw_mnf_id": '', "cw_mnf_name": "", "solarwinds_vendor": "" } } } },
        isFetching: false,
      });

    case FETCH_D_CL_MAPPING_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
      case FETCH_QUERY_REQUEST:
        return Object.assign({}, state, {
          isFetchingQuery: true,
        });
  
      case FETCH_QUERY_SUCCESS:
        return Object.assign({}, state, {
          query: action.response,
          isFetchingQuery: false,
        });
  
      case FETCH_QUERY_FAILURE:
        return Object.assign({}, state, {
          error: 'some error',
          isFetchingQuery: false,
        });
  
      case FETCH_QUERIES_REQUEST:
        return Object.assign({}, state, {
          isFetchingQueryList: true,
        });
  
      case FETCH_QUERIES_SUCCESS:
        return Object.assign({}, state, {
          queryList: action.response,
          isFetchingQueryList: false,
        });
  
      case FETCH_QUERIES_FAILURE:
        return Object.assign({}, state, {
          error: 'some error',
          isFetchingQueryList: false,
        });

        case GET_URL_REQUEST:
          return Object.assign({}, state, {
            isFetchingURL: true,
          });
    
        case GET_URL_SUCCESS:
          return Object.assign({}, state, {
            downloadURL: action.response,
            isFetchingURL: false,
          });
    
        case GET_URL_FAILURE:
          return Object.assign({}, state, {
            error: 'some error',
            isFetchingURL: false,
          });
          
      case DELETE_COLLECTOR_REQUEST:
        return Object.assign({}, state, {
          isFetchingURL: true,
        });
  
      case DELETE_COLLECTOR_SUCCESS:
        return Object.assign({}, state, {
          downloadURL: action.response,
          isFetchingURL: false,
        });
  
      case DELETE_COLLECTOR_FAILURE:
        return Object.assign({}, state, {
          error: 'some error',
          isFetchingURL: false,
        });

        case REPORT_COMPLIANCE_REQUEST:
          return Object.assign({}, state, {
            isReportDownloading: true,
          });
    
        case REPORT_COMPLIANCE_SUCCESS:
          return Object.assign({}, state, {
            isReportDownloading: false,
          });
    
        case REPORT_COMPLIANCE_FAILURE:
          return Object.assign({}, state, {
            error: 'some error',
            isReportDownloading: false,
          });
    default:
      return state;
  }
}
