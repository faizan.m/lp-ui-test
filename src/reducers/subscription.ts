import {
  FETCH_PRODUCT_MAPPINGS_FAILURE,
  FETCH_PRODUCT_MAPPINGS_REQUEST,
  FETCH_PRODUCT_MAPPINGS_SUCCESS,
  FETCH_SUBSCRIPTIONS_FAILURE,
  FETCH_SUBSCRIPTIONS_REQUEST,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_MODEL_MAPPINGS_REQUEST,
  FETCH_MODEL_MAPPINGS_SUCCESS,
  FETCH_MODEL_MAPPINGS_FAILURE,
  UPDATE_MODELS_REQUEST,
  UPDATE_MODELS_SUCCESS,
  UPDATE_MODELS_FAILURE,
} from '../actions/subscription';

const initialState: ISubscriptionStore = {
  modelMappings: null,
  productMappings: null,
  isFetching: false,
  isFetchingModelList: false,
  isFetchingSubscriptions: false,
  isFetchingSubscription: false,
  isLoadingModel: false,
  subscription: null,
  subscriptions: [],
};

export default function subscription(
  state: ISubscriptionStore = initialState,
  action: any
): ISubscriptionStore {
  switch (action.type) {
    case FETCH_PRODUCT_MAPPINGS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_PRODUCT_MAPPINGS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        productMappings: action.response,
      });

    case FETCH_PRODUCT_MAPPINGS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });

      case FETCH_MODEL_MAPPINGS_REQUEST:
        return Object.assign({}, state, {
          isFetchingModelList: true,
        });
      case FETCH_MODEL_MAPPINGS_SUCCESS:
        return Object.assign({}, state, {
          isFetchingModelList: false,
          modelMappings: action.response,
        });
  
      case FETCH_MODEL_MAPPINGS_FAILURE:
        return Object.assign({}, state, {
          isFetchingModelList: false,
        });

        
      case UPDATE_MODELS_REQUEST:
        return Object.assign({}, state, {
          isLoadingModel: true,
        });
      case UPDATE_MODELS_SUCCESS:
        return Object.assign({}, state, {
          isLoadingModel: false,
        });
  
      case UPDATE_MODELS_FAILURE:
        return Object.assign({}, state, {
          isLoadingModel: false,
        });

    case FETCH_SUBSCRIPTIONS_REQUEST:
      return Object.assign({}, state, {
        isFetchingSubscriptions: true,
      });
    case FETCH_SUBSCRIPTIONS_SUCCESS:
      return Object.assign({}, state, {
        subscriptions: action.response,
        isFetchingSubscriptions: false,
      });

    case FETCH_SUBSCRIPTIONS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSubscriptions: false,
      });
    default:
      return state;
  }
}
