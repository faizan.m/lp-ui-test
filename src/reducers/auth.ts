import { cloneDeep } from 'lodash';

import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REFRESH_TOKEN_FAILURE,
  REFRESH_TOKEN_REQUEST,
  REFRESH_TOKEN_SUCCESS,
  RESET_PASSWORD_FAILURE,
} from '../actions/auth';

const initialState: IAuthStore = {
  isLoggingIn: false,
  isAuthenticated: false,
  isaccelaAdmin: false,
  isProductAdmin: false,
  isSiteAdmin: false,
  userProfile: null,
  error: null,
  fetchingToken: false,
};

export default function auth(
  state: IAuthStore = initialState,
  action: any
): IAuthStore {
  switch (action.type) {
    case LOGIN_REQUEST:
      return Object.assign({}, state, {
        isLoggingIn: true,
        isAuthenticated: false,
        isaccelaAdmin: false,
        isProductAdmin: false,
        isSiteAdmin: false,
        userProfile: null,
        error: null,
      });

    case LOGIN_SUCCESS:
      const isaccelaAdmin = false;
      const isSiteAdmin = false;
      const isProductAdmin = false;

      return Object.assign({}, state, {
        isLoggingIn: false,
        isAuthenticated: true,
        isaccelaAdmin,
        isSiteAdmin,
        isProductAdmin,
        userProfile: action.userProfile,
        error: null,
        fetchingToken: false,
      });

    case LOGIN_FAILURE:
      return Object.assign({}, state, {
        isLoggingIn: false,
        isAuthenticated: false,
        isaccelaAdmin: false,
        isProductAdmin: false,
        isSiteAdmin: false,
        userProfile: null,
        error: action.error,
      });

    case REFRESH_TOKEN_REQUEST:
      return Object.assign({}, state, {
        fetchingToken: true,
      });

    case REFRESH_TOKEN_SUCCESS:
      return Object.assign({}, state, {
        fetchingToken: false,
      });

    case REFRESH_TOKEN_FAILURE:
      return Object.assign({}, state, {
        fetchingToken: false,
      });

    case LOGOUT_SUCCESS:
      return initialState;

    case RESET_PASSWORD_FAILURE:
      const newState = cloneDeep(state);
      newState.error = 'Error in reset password';

      return newState;

    default:
      return state;
  }
}
