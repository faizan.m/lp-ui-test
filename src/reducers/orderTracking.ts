import {
  GET_OT_TABLE_FAILURE,
  GET_OT_TABLE_REQUEST,
  GET_OT_TABLE_SUCCESS,
  GET_SERVICE_TICKETS_REQUEST,
  GET_SERVICE_TICKETS_SUCCESS,
  GET_SERVICE_TICKETS_FAILURE,
  GET_UNMAPPED_SERVICE_TICKETS_REQUEST,
  GET_UNMAPPED_SERVICE_TICKETS_SUCCESS,
  GET_UNMAPPED_SERVICE_TICKETS_FAILURE,
  GET_SALES_ORDERS_REQUEST,
  GET_SALES_ORDERS_SUCCESS,
  GET_SALES_ORDERS_FAILURE,
  GET_PURCHASE_ORDERS_REQUEST,
  GET_PURCHASE_ORDERS_SUCCESS,
  GET_PURCHASE_ORDERS_FAILURE,
  GET_LINKED_PO_REQUEST,
  GET_LINKED_PO_SUCCESS,
  GET_LINKED_PO_FAILURE,
} from "../actions/orderTracking";

const initialState: IOrderTrackingStore = {
  orderTrackingDashboardData: [],
  serviceTickets: [],
  unmappedServiceTickets: [],
  isTicketsFetching: false,
  isFetching: false,
  salesOrders: [],
  purchaseOrders: [],
  isFetchingSalesOrders: false,
  isFetchingPurchaseOrders: false,
  linkedPOs: [],
  isFetchingLinkedPOs: false
};

export default function orderTracking(
  state: IOrderTrackingStore = initialState,
  action: any
): IOrderTrackingStore {
  switch (action.type) {
    case GET_OT_TABLE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        orderTrackingDashboardData: [],
      });
    case GET_OT_TABLE_SUCCESS:
      return Object.assign({}, state, {
        orderTrackingDashboardData: action.response,
        isFetching: false,
      });
    case GET_OT_TABLE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });
    case GET_SERVICE_TICKETS_REQUEST:
      return Object.assign({}, state, {
        isTicketsFetching: true,
        serviceTickets: [],
      });
    case GET_SERVICE_TICKETS_SUCCESS:
      return Object.assign({}, state, {
        serviceTickets: action.response,
        isTicketsFetching: false,
      });
    case GET_SERVICE_TICKETS_FAILURE:
      return Object.assign({}, state, {
        error: "Error retrieving service tickets list",
        isTicketsFetching: false,
      });
    case GET_UNMAPPED_SERVICE_TICKETS_REQUEST:
      return Object.assign({}, state, {
        isTicketsFetching: true,
        serviceTickets: [],
      });
    case GET_UNMAPPED_SERVICE_TICKETS_SUCCESS:
      return Object.assign({}, state, {
        unmappedServiceTickets: action.response,
        isTicketsFetching: false,
      });
    case GET_UNMAPPED_SERVICE_TICKETS_FAILURE:
      return Object.assign({}, state, {
        error: "Error retrieving service tickets list",
        isTicketsFetching: false,
      });
    case GET_SALES_ORDERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingSalesOrders: true,
        salesOrders: [],
      });
    case GET_SALES_ORDERS_SUCCESS:
      return Object.assign({}, state, {
        salesOrders: action.response.unlinked_sales_orders.map(
          (sales_order_crm_id: number) => ({
            value: sales_order_crm_id,
            label: sales_order_crm_id,
          })
        ),
        isFetchingSalesOrders: false,
      });
    case GET_SALES_ORDERS_FAILURE:
      return Object.assign({}, state, {
        error: "Error retrieving sales orders list",
        isFetchingSalesOrders: false,
      });
    case GET_PURCHASE_ORDERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingPurchaseOrders: true,
        purchaseOrders: [],
      });
    case GET_PURCHASE_ORDERS_SUCCESS:
      return Object.assign({}, state, {
        purchaseOrders: action.response.map((el) => ({
          value: el.crm_id,
          label: el.po_number,
        })),
        isFetchingPurchaseOrders: false,
      });
    case GET_PURCHASE_ORDERS_FAILURE:
      return Object.assign({}, state, {
        error: "Error retrieving purchase orders list",
        isFetchingPurchaseOrders: false,
      });
    case GET_LINKED_PO_REQUEST:
      return Object.assign({}, state, {
        isFetchingLinkedPOs: true,
        linkedPOs: [],
      });
    case GET_LINKED_PO_SUCCESS:
      return Object.assign({}, state, {
        linkedPOs: action.response.map((el) => ({
          value: el.crm_id,
          label: el.PO_number,
        })),
        isFetchingLinkedPOs: false,
      });
    case GET_LINKED_PO_FAILURE:
      return Object.assign({}, state, {
        error: "Error retrieving purchase orders list",
        isFetchingLinkedPOs: false,
      });
    default:
      return state;
  }
}
