import axios from 'axios';
import { ERROR, FETCHING_COMPLETE, IS_FETCHING } from '../actions/appState';
import { getIdToken, getTempToken, isTokeExpired } from '../utils/AuthUtil';
const ID_TOKEN = 'id_token';
const PROFILE_EXPIRY = 'profile_expiry';
const PROFILE = 'profile';
let API_BASE_URL;
export interface UseAPIConfig {
  endpoint?: string;
  // Type any is intentional here
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  dependencies?: any[];
  options?: IncomingOptions;
}

// TODO
// This can be removed
export const setAPIBaseUrl = (hostname: string) => {
  API_BASE_URL = hostname;
};

export const CALL_API = Symbol('Call API');

export default store => next => action => {
  const callAPI = action[CALL_API];

  // So the middleware doesn't get applied to every single action
  if (typeof callAPI === 'undefined') {
    return next(action);
  }
  let wait = false;
  const {
    endpoint,
    method,
    params,
    body,
    headers,
    types,
    authenticated,
    hideLoadingWidget,
    tempAuthenticated,
    mockAPI,
  } = callAPI;

  // TODO
  // Use the ES6 format while implementing retries.
  // const [requestType, successType, errorType] = types;
  const requestType = types[0];
  const successType = types[1];
  const errorType = types[2];

  store.dispatch({ type: requestType });
  store.dispatch({ type: IS_FETCHING, hideLoadingWidget });

  /* Passing the authenticated boolean back in our data will let us
   * distinguish between normal and secret quotes
   */

  if (isTokeExpired() && authenticated) {
    wait = true;

    return sleep(3000).then(() => {
      return callApi(
        endpoint,
        method,
        params,
        body,
        headers,
        authenticated,
        tempAuthenticated,
        mockAPI
      ).then(
        (response) => {
          store.dispatch({ type: FETCHING_COMPLETE, hideLoadingWidget });

          return next({
            response,
            authenticated,
            type: successType,
          });
        },
        (error) => {
          store.dispatch({
            type: ERROR,
            errorMessage: error.message || "There was an error.",
            hideLoadingWidget,
            errorDetails: error.response,
          });

          return next({
            error: error.message || "There was an error.",
            type: errorType,
            errorList: error.response,
          });
        }
      );
    });
  }
  if (!wait) {
    return callApi(
      endpoint,
      method,
      params,
      body,
      headers,
      authenticated,
      tempAuthenticated,
      mockAPI
    ).then(
      (response) => {
        store.dispatch({ type: FETCHING_COMPLETE, hideLoadingWidget });

        return next({
          response,
          authenticated,
          type: successType,
        });
      },
      (error) => {
        store.dispatch({
          type: ERROR,
          errorMessage: error.message || "There was an error.",
          hideLoadingWidget,
          errorDetails: error.response,
        });

        return next({
          error: error.message || "There was an error.",
          type: errorType,
          errorList: error.response,
        });
      }
    );
  }
};

export function callApi(
  endpoint: string,
  method: any,
  params: any,
  body: any,
  headers: any,
  authenticated: boolean,
  tempAuthenticated: boolean,
  mockAPI?: string,
) {
  const token = getIdToken() || null;
  const TempToken = getTempToken() || null;
  const mandatoryHeaders = {
    'Content-Type': 'application/json',
    'Client-Url': window.location,
  };

  const requestHeaders = Object.assign({}, mandatoryHeaders, headers);
  const config = {
    method,
    params,
    headers: requestHeaders,
    data: body,
    url: mockAPI? mockAPI : API_BASE_URL + endpoint,
  };

  if (authenticated) {
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } else {
      console.info('No token saved!');
      // return Promise.resolve("cancel");
    }
  }

  if (tempAuthenticated) {
    if (TempToken) {
      config.headers['multi-fa-token'] = `Auth ${TempToken}`;
    } else {
      console.info('No token saved!');
      // return Promise.resolve("cancel");
    }
  }

  return axios
    .request(config)
    .then((response: any) => {
      // TODO
      // Get the expected response status from the action.
      if (
        response.status !== 200 &&
        response.status !== 201 &&
        response.status !== 202 &&
        response.status !== 204
      ) {
        throw Error(response.statusText);
      }

      return response.data;
    })
    .catch(error => {
      if (error && error.response && error.response.status === 401) {
        localStorage.removeItem(ID_TOKEN);
        localStorage.removeItem(PROFILE);
        localStorage.removeItem(PROFILE_EXPIRY);
        window.location.reload();
      }
      throw error;
    });
}
function sleep(ms: any) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


import useFetch, { UseFetch, CachePolicies, IncomingOptions } from "use-http";


export const useApi = <TData = any>(config: UseAPIConfig = {}): UseFetch<TData> => {
  const { endpoint, options, dependencies } = config;
  const token = getIdToken() || null;
  const mandatoryHeaders = {
    "Content-Type": "application/json",
    "Client-Url": window.location.hostname,
    "Authorization": `Bearer ${token}`
  };
  const requestHeaders = Object.assign({}, mandatoryHeaders);

  
  const requestConfig: IncomingOptions = {
    cachePolicy: CachePolicies.CACHE_FIRST,
    cacheLife: 60 * 60 * 1000 * 1, // 1 hour
    persist: true,
    headers: { ...requestHeaders },
    ...options,
  };

  const serverUrl = `${API_BASE_URL}${endpoint || ""}`;

  return useFetch<TData>(serverUrl, requestConfig, dependencies);
};

