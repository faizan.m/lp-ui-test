import * as React from 'react';
import ReactSelect from 'react-select';
import 'react-select/dist/react-select.css';

import './style.scss';

interface ISelectInputProps {
  name: string;
  value: any;
  clearable?: boolean;
  options: Array<{ value: any; label: string; disabled?: boolean }>;
  onChange: TButtonOnClick;
  onBlur?: TButtonOnClick;
  disabled?: boolean;
  placeholder?: string;
  required?: boolean;
  multi?: boolean;
  searchable?: boolean;
  isOptionDisabled?: any;
  loading?: any;
  onInputChange?: any;
  enableInternalFiltering?: boolean;
  optionComponent?: any;
  valueComponent?: any;
}

export default class SelectInput extends React.Component<
  ISelectInputProps,
  {}
> {
  public static defaultProps = {
    name: '',
    value: '',
    clearable: false,
    onChange: null,
    onBlur: null,
    placeholder: 'Select Option',
    options: null,
    multi: false,
    disabled: false,
    searchable: true,
    loading: false,
  };

  onChange = e => {
    if (this.props.multi) {
      const newValue = e.map(option => option.value);
      this.props.onChange({
        target: { name: this.props.name, value: newValue, label: e && e.label },
      });
    } else {
      this.props.onChange({
        target: { name: this.props.name, value: e && e.value || null, label: e && e.label,e},
      });
    }
  };

  onBlur = e => {
    this.props.onBlur && this.props.onBlur(e);
  };

  onInputChange = e => {
    this.props.onInputChange && this.props.onInputChange(e);
  };

  render() {
    const {
      name,
      options,
      value,
      clearable,
      placeholder,
      multi,
      disabled,
      searchable,
      loading,
      enableInternalFiltering=true
    } = this.props;

    return (
      <ReactSelect
        value={value}
        name={name}
        options={options}
        disabled={disabled}
        isOptionDisabled={option => option.disabled === true}
        clearable={clearable}
        onChange={e => this.onChange(e)}
        onBlur={e => this.onBlur(e)}
        placeholder={placeholder}
        searchable={searchable}
        isLoading={loading}
        multi={multi}
        menuPortalTarget={document.body}
        onInputChange={(e) => this.onInputChange(e)}
        filterOptions= {enableInternalFiltering}
        optionComponent={this.props.optionComponent}
        valueComponent={this.props.valueComponent}
      />
    );
  }
}
