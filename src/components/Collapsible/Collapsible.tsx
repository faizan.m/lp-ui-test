import React, { useEffect, useState } from 'react';
import './Collapsible.scss';
import { Collapse } from 'react-bootstrap';

interface ICollapsibleProps {
  label: any;
  children: any;
  isOpen?: boolean;
  statusClass?: string
  parentClass?: string
}

const Collapsible: React.FC<ICollapsibleProps> = ({ label, ...props }) => {
  const [isOpen, setIsOpen] = useState(props.isOpen);
  useEffect(() => {
    setIsOpen(props.isOpen);
  }, [props.isOpen]);
  return (
    <div className={`collapsable-component ${props.parentClass || ''}`}>
      <div
        className={`label
        ${props.statusClass}
        ${isOpen ? "field--not-collapsed" : ""}`}
        onClick={() => setIsOpen(!isOpen)}
        aria-controls={`collapsable-item-${label}`}
        aria-expanded={isOpen}
      >
        <div className={`collapsable-heading `}>{label}</div>
        <div className="action-collapse">
          {!isOpen ? '+' : '-'}
        </div>
      </div>
      <Collapse in={isOpen}>
        <div id={`collapsable-item-${label}`}>{props.children}</div>
      </Collapse>
    </div>
  );
};

export default Collapsible;
