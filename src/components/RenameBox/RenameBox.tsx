import React from "react";

import SquareButton from "../Button/button";
import ModalBase from "../ModalBase/modalBase";

import Input from "../Input/input";
import "./style.scss";

interface IConfirmProps {
  show: boolean;
  onClose: () => void;
  onSubmit: (name: string) => void;
  isLoading: boolean;
  name: string;
  title?: string;
  okText?: string;
  cancelText?: string;
  labelName?: string;
}

export default class RenameBox extends React.Component<
  IConfirmProps,
  { name: string }
> {
  constructor(props: IConfirmProps) {
    super(props);
    this.state = {
      name: "",
    };
  }
  onSubmit = () => {
    this.props.onSubmit(this.state.name);
    this.props.onClose();
  };

  componentDidUpdate(prevProps: { name: string;}) {
    if (this.props.name && this.props.name !== prevProps.name) {
      this.setState({ name: this.props.name });
    }
  }
  getBody = () => {
    return (
      <div
        className={`rename_body
        ${this.props.isLoading ? `loading` : ""}`}
      >
        <Input
          field={{
            label: this.props.labelName ? this.props.labelName : "Name",
            type: InputFieldType.TEXT,
            value: this.state.name,
            isRequired: true,
          }}
          width={12}
          placeholder={`Enter ${
            this.props.labelName ? this.props.labelName : "Name"
          }`}
          name="name"
          onChange={this.handleChange}
        />
      </div>
    );
  };
  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      [targetName]: targetValue,
    }));
  };
  getFooter = () => {
    return (
      <div
        className={`confirm__actions
        ${this.props.isLoading ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content={this.props.cancelText ? this.props.cancelText : "Cancel"}
          bsStyle={ButtonStyle.DEFAULT}
          disabled={this.props.isLoading}
        />
        <SquareButton
          onClick={this.onSubmit}
          content={this.props.okText ? this.props.okText : "Download"}
          bsStyle={ButtonStyle.PRIMARY}
          disabled={this.props.isLoading || !this.state.name}
        />
      </div>
    );
  };

  render() {
    const title = this.props.title ? this.props.title : "Document Name ";

    return (
      <ModalBase
        hideCloseButton={this.props.isLoading}
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="rename"
      />
    );
  }
}
