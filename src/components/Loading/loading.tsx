import * as React from 'react';
import { Modal } from 'react-bootstrap';

import './style.scss';

interface ILoadingProps {
  show: boolean;
  location: any;
}

export default class Loading extends React.Component<ILoadingProps, {}> {
  render() {
    const disabledPaths = [
      '/dashboard',
      '/inventory-management',
      '/customer-renewals',
      '/renewal',
      '/inventory-management/field-mapping',
      '/providers',
      '/service-requests',
      '/accelavar-users',
      '/profile',
      '/customer-users',
      '/reports',
      '/customer-configs',
      '/provider-users',
      '/setting/api-credentials',
    ];
    let isDisabledPage = false;
    disabledPaths.map(path => {
      if (this.props.location.pathname.includes(path)) {
        isDisabledPage = true;

        return;
      }
    });

    const exactPaths = ['/', '/setting/alias', '/setting/erp'];
    // check exact path
    exactPaths.map(path => {
      if (this.props.location.pathname === path) {
        isDisabledPage = true;

        return;
      }
    });

    return (
      <Modal
        className="loading-overlay"
        show={this.props.show && !isDisabledPage}
        onHide={null}
        backdrop="static"
        // keyboard={false}
      >
        <img src="/assets/icons/loading.gif" />
      </Modal>
    );
  }
}
