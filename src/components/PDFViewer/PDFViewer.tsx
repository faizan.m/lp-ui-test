import * as React from 'react';
import { Modal } from 'react-bootstrap';

import './style.scss';

interface IModalBaseProps {
  show: boolean;
  onClose: (e: any) => void;
  closeOnEscape?: boolean;
  closeOnOverlayClick?: boolean;
  titleElement?: any;
  previewHTML?: any;
  footerElement?: any;
  className?: string;
}

export default class PDFViewer extends React.Component<IModalBaseProps, {}> {
  public static defaultProps: IModalBaseProps = {
    show: false,
    onClose: null,
    closeOnEscape: true,
    closeOnOverlayClick: false,
    titleElement: 'REPLACE TITLE',
    previewHTML: 'REPLACE BODY',
    footerElement: 'REPLACE FOOTER',
  };
  renderPreview = () => {
    if (!this.props.previewHTML) {
      return false;
    }

    return (
      <div className="pdf-preview-embed">
        <embed
          className="pdfobject"
          src={this.props.previewHTML.file_path}
          type="application/pdf"
          style={{
            overflow: 'auto',
            width: '947px',
            height: '93vh',
          }}
        />{' '}
      </div>
    );
  };
  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.onClose}
        backdrop={this.props.closeOnOverlayClick ? true : 'static'}
        keyboard={this.props.closeOnEscape}
        className={`${this.props.className} view-pdf-preview`}
      >
        <Modal.Header className="view-pdf-header" closeButton>{this.props.titleElement}</Modal.Header>
        <Modal.Body>{this.renderPreview()}</Modal.Body>
      </Modal>
    );
  }
}
