import React, { useState } from 'react';
import './ToggleButton.scss';

const ToggleButton: React.FC<any> = (props) => {
  const [enabled, setEnabled] = useState(props.enabled);

  const onClickCheckBox = (e) => {
    if (!props.disabled) {
      props.onChange(!enabled);
      setEnabled(!enabled);
    }
  };
  return (
    <div className={`c-switch-container ${props.disabled ? 'disabled' : ''}`}
      title={`${props.disabled ? 'Not allowed to Disable' : 'Click to toggle'}`}>
      <label className={`switch ${enabled ? '' : 'disabled-switch'}`}>
        <input type="checkbox" checked={enabled} onClick={onClickCheckBox} />
        <div>
          <span />
        </div>
      </label>
    </div>
  );
};

export default ToggleButton;
