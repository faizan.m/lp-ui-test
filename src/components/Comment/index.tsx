import React from "react";
import moment from "moment";
import { fromISOStringToDateTimeString } from "../../utils/CalendarUtil";
import "./style.scss";

interface ICommentProps {
  comment: IComment;
  showType: boolean;
}

const Comment: React.FC<ICommentProps> = (props) => {
  const {
    created_on,
    text,
    created_by,
    alternate_created_by,
    is_internal_note,
  } = props.comment;

  return (
    <div className="comment">
      <div className="comment__header">
        <h5>{created_by ? created_by : alternate_created_by}</h5>
        {props.showType && (
          <p className="comment-type">{`Comment Type: ${
            is_internal_note ? "Internal" : "Discussion"
          }`}</p>
        )}
        <p
          className="comment-create-time"
          title={fromISOStringToDateTimeString(created_on)}
        >
          {moment(created_on).fromNow()}
        </p>
      </div>
      <div className="comment__body">
        <p>{text}</p>
      </div>
    </div>
  );
};

export default Comment;
