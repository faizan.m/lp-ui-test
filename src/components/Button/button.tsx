import React from 'react';
import { Button } from 'react-bootstrap';

import './style.scss';

interface IButtonProps {
  content?: any;
  onClick?: TButtonOnClick;
  bsStyle?: ButtonStyle;
  className?: string;
  disabled?: boolean;
  title?: string;
}

interface IButtonState {}

export default class SquareButton extends React.Component<
  IButtonProps,
  IButtonState
> {
  render() {
    const {
      title,
      content,
      className,
      onClick,
      bsStyle,
      disabled,
    } = this.props;

    return (
      <Button
        onClick={event => onClick(event)}
        bsStyle={bsStyle}
        className={`square-btn ${className ? className : ''}`}
        disabled={disabled}
        title={title ? title : ''}
      >
        {content}
      </Button>
    );
  }
}
