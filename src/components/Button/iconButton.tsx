import React from "react";

import "./style.scss";

interface IEditButtonProps {
  onClick: TButtonOnClick;
  icon: string;
  newIcon?: boolean;
  className?: string;
  title?: string;
}

interface IEditButtonState {}

export default class IconButton extends React.Component<
  IEditButtonProps,
  IEditButtonState
> {
  render() {
    const { onClick, icon, title } = this.props;
    const styles = {
      background: `url(/assets/${
        this.props.newIcon ? "new-icons" : "icons"
      }/${icon}) center no-repeat`,
    };

    return (
      <button
        title={title ? title : ""}
        className={`btn icon-btn ${this.props.className}`}
        style={styles}
        onClick={(event) => onClick(event)}
      />
    );
  }
}
