import React, { Component } from 'react';
import { withRouter } from 'react-router';

interface IBackButtonProps extends ICommonProps {
  path: string;
  name?: string;
}

class BackButton extends Component<IBackButtonProps> {
  goBack = () => {
    this.props.history.push(this.props.path);
  };

  render() {
    let linkName = 'Back';
    if (this.props.name) {
      linkName = this.props.name;
    }

    return (
      <h4 className="d-pointer" onClick={this.goBack}>
        &lt;&nbsp;
        {linkName}
      </h4>
    );
  }
}

export default withRouter(BackButton);
