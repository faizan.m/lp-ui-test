import React, { useLayoutEffect, useState } from "react";
import "./style.scss";

export interface RangeSliderProps {
  min: number;
  max: number;
  minVal: number;
  maxVal: number;
  onChange: any;
  width?: number;
  thumbsize?: number;
}

export const RangeSlider: React.SFC<RangeSliderProps> = ({
  min,
  max,
  minVal,
  maxVal,
  onChange,
  width = 200,
  thumbsize = 14,
}) => {

  const [avg, setAvg] = useState((min + max) / 2);

  const minWidth =
    thumbsize + ((avg - min) / (max - min)) * (width - 2 * thumbsize);
  const minPercent = ((minVal - min) / (avg - min)) * 100;
  const maxPercent = ((maxVal - avg) / (max - avg)) * 100;

  const styles = {
    min: {
      width: minWidth,
      left: 0,
      "--minRangePercent": `${minPercent}%`,
    },
    max: {
      width: thumbsize + ((max - avg) / (max - min)) * (width - 2 * thumbsize),
      left: minWidth,
      "--maxRangePercent": `${maxPercent}%`,
    },
  };

  useLayoutEffect(() => {
    setAvg((maxVal + minVal) / 2);
    onChange({
      maxVal: Math.round(maxVal),
      minVal: Math.round(minVal),
      min,
      max,
      maxPercent
    })
  }, [minVal, maxVal]);

  return (
    <div className="slider-container">
      <div className="min-label-value">{`${Math.round(minVal)}%`}</div>
      <div
        className="min-max-slider"
        data-legendnum="2"
        data-rangemin={min}
        data-rangemax={max}
        data-thumbsize={thumbsize}
        data-rangewidth={width}
      >
        <label htmlFor="min">Minimum price</label>
        <input
          id="min"
          className="min"
          style={styles.min}
          name="min"
          type="range"
          step="1"
          min={min}
          max={avg}
          value={minVal}
          onChange={({ target }) => 
            onChange({
              maxVal: Math.round(maxVal),
              minVal: Number(target.value),
              min,
              max,
              maxPercent
            })
          }
        />
        <label htmlFor="max">Maximum price</label>
        <input
          id="max"
          className="max"
          style={styles.max}
          name="max"
          type="range"
          step="1"
          min={avg}
          max={max}
          value={maxVal}
          onChange={({ target }) =>
            onChange({
              maxVal: Number(target.value),
              minVal: Math.round(minVal),
              min,
              max,
              maxPercent
            })
          }
        />
      </div>
      <div className="max-label-value">{`${Math.round(maxVal)}%`}</div>
    </div>
  );
};
