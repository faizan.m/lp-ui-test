import React from "react";
import "./style.scss";

export interface MaxSliderProps {
  min: number;
  max: number;
  maxVal: number;
  onChange: any;
  width?: number;
  thumbsize?: number;
  disabled?: boolean;
}

const MaxSlider: React.FC<MaxSliderProps> = ({
  min,
  max,
  maxVal,
  onChange,
  width = 200,
  thumbsize = 14,
  disabled = false,
}) => {
  const styles = {
    max: {
      left: 0,
      "--maxRangePercent": `${((maxVal - min) / (max - min)) * 100}%`,
    },
  };

  return (
    <div className="slider-container">
      <div className="min-label-value">{`${Math.round(min)}%`}</div>
      <div
        className="min-max-slider"
        data-legendnum="2"
        data-rangemin={min}
        data-rangemax={max}
        data-thumbsize={thumbsize}
        data-rangewidth={width}
      >
        <label htmlFor="max">Maximum price</label>
        <input
          id="max"
          className="max"
          style={styles.max}
          name="max"
          type="range"
          step="1"
          min={min}
          max={max}
          value={maxVal}
          onChange={
            disabled
              ? null
              : ({ target }) =>
                  onChange({
                    maxVal: Number(target.value),
                    min,
                    max,
                  })
          }
        />
      </div>
      <div id="fix-max-percent" className="max-label-value">{`${Math.round(
        maxVal
      )}%`}</div>
    </div>
  );
};

export default MaxSlider;
