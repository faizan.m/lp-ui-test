import React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap/lib';
import './style.scss';

const ColorPicker: React.FC<any> = (props) => {
  const colors = props.colors || ['#176f07', '#db1643', '#f8af03', '#007ba8', '#7412e6'];

  const onSelectColor = (c) => {
    props.onSelectColor(c);
    document.body.click();
  }
  const confirm = (
    <Tooltip placement="right" className={`show in`} id="color-piker-box">
      <div className="color-box-panel">
        {
          colors.map((c,index) => (
            <div onClick={e => onSelectColor(c)} key={index}
              className={`color-box`} style={{ backgroundColor: c }} >
              {c === props.color ? <span className="selected">&#10003;</span> : ''}
            </div>
          ))
        }
      </div>
    </Tooltip>
  );


  return (

    <OverlayTrigger trigger='focus' overlay={confirm} placement="bottom">
      <div tabIndex={0} style={{ backgroundColor: props.color }} className={`color-box-value ${props.className}`}>
      </div>
    </OverlayTrigger>
  );
};

export default ColorPicker;