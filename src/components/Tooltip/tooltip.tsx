import React, { useState } from 'react';
import { Row } from 'react-bootstrap';
import './style.scss';

const TooltipCustom: React.FC<any> = (props) => {
  const[show, setShow] = useState(false)
  const tooltip = (
    <>
      {props.children || 
      <div className="tooltip-data-rules">
      <div>  Inline variables can be used to add rule specific variables using the following syntax</div>
      <div>  1. Please enter # to show list of inline Variables & select variable</div>
      <div>  2. Variables with OR condition 
              <br/> &nbsp; &nbsp;Syntax: #VAR_OPTION_OR(var1, var2, var3) 
              <br/> &nbsp; &nbsp;Example:#VAR_OPTION_OR(1,2,3) </div>  
      <div>  3. Variables with AND condition 
          <br/> &nbsp; &nbsp;Syntax: #VAR_OPTION_AND(var1, var2, var3) 
          <br/> &nbsp; &nbsp;Example: #VAR_OPTION_AND(1,2,3)</div>
      <div>  4. Variables in a Range of integers with an OR condition 
                  <br/> &nbsp; &nbsp;Syntax: #NUM_RANGE(min-max)
                  <br/> &nbsp; &nbsp;Example: #NUM_RANGE(1-10)</div>
      <div>  5. For wildcard matches use * 
                  <br/> &nbsp; &nbsp;Syntax: *
                  <br/> &nbsp; &nbsp;Example: enable secret 4 *</div>
    </div>}
    </>
  );


  return (
    <>
      <div
        className={
          "tooltip-help-icon" + (props.className ? ` ${props.className}` : "")
        }
      >
        <img
          alt=""
          className={"d-pointer rule-info"}
          src={`/assets/new-icons/${props.icon_url? props.icon_url:'info.svg'}`}
          onClick={()=>{setShow(!show)}}
        />
             {show && 
        <Row className="tooltip-body">
         {tooltip}
        </Row>
        }
      </div>
 
    </>
  );
};

export default TooltipCustom;