import React, { Component } from "react";
import ReactQuill, { Quill } from "react-quill"; // ES6
import "react-quill/dist/quill.snow.css";
import "quill-mention";
import { commonFunctions } from "../../utils/commonFunctions";
import TableModule from "quill1-table";
import ResizeModule from "@botom/quill-resize-module";

const Font = Quill.import("formats/font");
Font.whitelist = ["arial", "comic-sans", "courier-new"];
Quill.register(Font, true);
Quill.register("modules/table", TableModule);
Quill.register("modules/resize", ResizeModule);
var BaseImageFormat = Quill.import("formats/image");
const ImageFormatAttributesList = ["alt", "height", "width", "style"];
import "./styles.scss";

var Size = Quill.import("attributors/style/size");
Size.whitelist = ["10px", "12px", "14px", "16px", "18px", "24px"];
Quill.register(Size, true);

interface IQuillEditorState {
  fullscreen: boolean;
}

interface IIQuillEditorProps {
  value: any;
  onChange: (html: string, source: string) => void;
  className?: string;
  label?: string;
  error?: IFieldValidation;
  wrapperClass?: string;
  isRequired?: boolean;
  isDisabled?: boolean;
  scrollingContainer?: string;
  customToolbar?: any[];
  hideFullscreen?: boolean;
  hideTable?: boolean;
}

interface IPassedProps {
  meeting?: boolean;
  mailTemplateData?: any;
  setData?: any;
  markDownVariables?: IMarkDownVariables[];
}

export class QuillEditorAcela extends Component<
  IPassedProps & IIQuillEditorProps,
  IQuillEditorState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  ref: any;
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = {
      fullscreen: false,
    };
    this.ref = React.createRef();
    this.escFunction = this.escFunction.bind(this);
  }
  escFunction(event) {
    if (event.key === "Escape") {
      this.setState({ fullscreen: !this.state.fullscreen });
    }
  }
  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values: IMarkDownVariables[] = [];

      if (mentionChar === "@") {
        values =
          this.props.markDownVariables || commonFunctions.markdownVariables;
      }

      renderList(values, searchTerm);
    },
  };

  handleChange = (content, delta, source, editor) => {
    this.props.onChange(content, source);
  };

  render() {
    return (
      <div
        className={`col-md-12 react-quill ${
          this.props.wrapperClass ? this.props.wrapperClass : ""
        }`}
        onKeyDown={this.props.hideFullscreen ? null : this.escFunction}
      >
        {this.props.label && (
          <label className="field__label-label">
            {this.props.label}
            {this.props.isRequired && (
              <span className="field__label-required">*</span>
            )}
          </label>
        )}
        {!this.props.hideFullscreen && !this.props.isDisabled && (
          <img
            className={
              this.state.fullscreen
                ? " fullscreen-btn-in-full"
                : "fullscreen-btn"
            }
            alt={this.state.fullscreen ? "Exit Fullscreen" : "Fullscreen"}
            src={
              this.state.fullscreen
                ? "/assets/icons/full-exit.png"
                : "/assets/icons/fullscreen.png"
            }
            title={
              this.state.fullscreen
                ? "Exit Fullscreen (Press ESC to toggle fullscreen)"
                : "Fullscreen (Press ESC to toggle fullscreen)"
            }
            onClick={(e) => {
              this.setState({ fullscreen: !this.state.fullscreen });
            }}
          />
        )}
        <ReactQuill
          ref={(e) => (this.ref = e)}
          readOnly={Boolean(this.props.isDisabled)}
          value={this.props.value || ""}
          onChange={(a, b, c, d) => this.handleChange(a, b, c, d)}
          scrollingContainer={
            (this.state.fullscreen ? ".ql-container " : "") +
            this.props.scrollingContainer
          }
          className={
            "react-quill-main" +
            (this.props.className ? " " + this.props.className : "") +
            (this.props.error &&
            this.props.error.errorState === IValidationState.ERROR
              ? " quill-error-border"
              : "") +
            (this.props.isDisabled ? " react-quill-disabled" : "") +
            (this.state.fullscreen ? " react-quill-fullscreen" : "")
          }
          modules={{
            mention: this.mentionModule,
            toolbar: this.props.customToolbar
              ? this.props.customToolbar
              : [
                  ["bold", "italic", "underline", "strike"], // toggled buttons
                  ["blockquote", "code-block", "image", "video"],
                  [{ list: "ordered" }, { list: "bullet" }],
                  [{ script: "sub" }, { script: "super" }], // superscript/subscript
                  [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
                  [{ direction: "rtl" }], // text direction
                  [{ color: [] }, { background: [] }], // dropdown with defaults from theme
                  [{ font: ["arial", "comic-sans", "courier-new"] }],
                  [{ align: [] }],
                  // ["clean"], // remove formatting button
                  [
                    {
                      table: TableModule.tableOptions(),
                    },
                    {
                      table: [
                        "insert",
                        "append-row-above",
                        "append-row-below",
                        "append-col-before",
                        "append-col-after",
                        "remove-col",
                        "remove-row",
                        "remove-table",
                        "split-cell",
                        "merge-selection",
                        "remove-cell",
                        "remove-selection",
                        "undo",
                        "redo",
                      ],
                    },
                  ],
                  [{ header: [1, 2, 3, 4, 5, 6, false] }],
                  [{ size: ["10px", "12px", "14px", "16px", "18px", "24px"] }],
                ],
            table: !this.props.hideTable,
            // imageResize: {
            //   parchment: Quill.import("parchment"),
            //   modules: ["Resize", "DisplaySize","Toolbar"],
            // },
            resize: {},
            history: {
              delay: 2000,
              maxStack: 500,
              userOnly: true,
            },
          }}
          theme="snow"
        />
        {this.props.error &&
          this.props.error.errorState === IValidationState.ERROR && (
            <span className="custom-quill-error">
              {this.props.error.errorMessage
                ? this.props.error.errorMessage
                : "Required field"}
            </span>
          )}
      </div>
    );
  }
}

class ImageFormat extends BaseImageFormat {
  static formats(domNode) {
    return ImageFormatAttributesList.reduce(function(formats, attribute) {
      if (domNode.hasAttribute(attribute)) {
        formats[attribute] = domNode.getAttribute(attribute);
      }
      return formats;
    }, {});
  }
  format(name, value) {
    if (ImageFormatAttributesList.indexOf(name) > -1) {
      if (value) {
        this.domNode.setAttribute(name, value);
      } else {
        this.domNode.removeAttribute(name);
      }
    } else {
      super.format(name, value);
    }
  }
}
Quill.register(ImageFormat, true);
