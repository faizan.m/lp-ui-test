declare interface IHash {
  [key: string]: any;
}

declare interface ICommonProps {
  history: any;
  match: any;
  location: any;
}

declare type TButtonOnClick = ((event: any) => void);

declare interface ISearchItem {
  name: string;
  value: any;
}
declare interface ISearchOptions {
  title?: string;
  items: ISearchItem[];
}

declare const enum RadioState {
  CHECKED = 'CHECKED',
  UNCHECKED = 'UNCHECKED',
}

declare const enum ButtonStyle {
  PRIMARY = 'primary',
  OUTLINE = 'link',
  WARNING = 'warning',
  DANGER = 'danger',
  DEFAULT = 'default',
  SUCCESS = 'success',
  INFO = 'info',
}

declare interface IInputField {
  label: any;
  value: any;
  isRequired?: boolean;
  is_required?: boolean;
  type: InputFieldType;
  options?: Array<IPickListOptions | string>;
  disabled?: string;
  id?: string;
  help_text?: string;
}

declare const enum InputFieldType {
  DATE = 'DATE',
  SEARCH = 'SEARCH',
  TEXT = 'TEXT',
  NUMBER = 'NUMBER',
  TEXTAREA = 'TEXTAREA',
  PICKLIST = 'PICKLIST',
  PASSWORD = 'PASSWORD',
  RADIO = 'RADIO',
  FILE = 'FILE',
  COLOR = 'COLOR',
  CUSTOM = 'CUSTOM',
}

declare interface IFieldValidation {
  errorState: IValidationState;
  errorMessage: string;
}

declare const enum IValidationState {
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error',
}

declare interface   ISideMenuOption {
  name: string;
  path: string;
  campare: string;
  icon?: string;
  childs?: ISideMenuOption[];
  showChild? : boolean;
  isAllowed?:any;
  accessLevel?: any;
  search?: any;
  isAllow?: boolean;
  condition?:any;
  isAvailable?: string;
  defaultHide?: boolean;
}

declare interface ITableColumn {
  id?: string;
  accessor: string;
  Header: any;
  sortable?: boolean;
  Cell?: (row: any) => any;
  show?: boolean;
  width?: number,
  resizable?: boolean;
}

declare const enum IIntegrationsCatgories {
  CRM = 'CRM',
  DEVICE_INFO = 'DEVICE_INFO',
  DEVICE_MONITOR = 'DEVICE_MONITOR',
  STORAGE_SERVICE = 'STORAGE_SERVICE',
}

declare const enum ISetingsType {
  CISCO = 'CISCO',
  CONNECTWISE = 'CONNECTWISE',
}
