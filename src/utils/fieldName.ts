export const getFieldNames = (id: any) => {
  let name = '';
  const is = fieldNames.filter(f => id.includes(f.code))
  name = is && is[0] && is[0].name;
  return name ? name : id.replace(/[^\w\s]/gi, ' ').replace('_', ' ');
};
export const fieldNames = [
  {
    code: `root['applies_to']`,
    name: 'Applies To',
  },
  {
    code: `root['applies_to_manufacturers']`,
    name: 'Applies To Manufacturers',
  },
  {
    code: `root['classification_id']`,
    name: 'Classification',
  },
  {
    code: `root['detail'`,
    name: 'Details',
  },
  {
    code: `root['level'`,
    name: 'Level',
  },
  {
    code: `root['name']`,
    name: 'Name',
  },
  {
    code: `root['description']`,
    name: 'Description',
  },
  {
    code: `root['is_enabled']`,
    name: 'Is Enable',
  },
  {
    code: `root['type']`,
    name: 'Type',
  },
  {
    code: `root['option']`,
    name: 'Option',
  },
  {
    code: `root['variable_values']`,
    name: 'variable values',
  },
  {
    code: `root['json_config']['project_details']['task_deliverables']['value_markdown']`,
    name: 'Task Deliverables: Value',
  },
  {
    code: `root['json_config']['project_details']['task_deliverables']['label']`,
    name: 'Task Deliverables: Label',
  },
  {
    code: `root['json_config']['project_details']['project_assumptions']['label']`,
    name: 'Project Assumptions: Label',
  },
  {
    code: `root['json_config']['project_details']['project_assumptions']['value_markdown']`,
    name: 'Project Assumptions: Value',
  },
  {
    code: `root['json_config']['project_details']['request_description']['value']`,
    name: 'Request Description',
  },
  {
    code: `root['json_config']['project_details']['out_of_scope']['value_markdown']`,
    name: 'Out Of scope: Value',
  },
  {
    code: `root['json_config']['project_details']['out_of_scope']['label']`,
    name: 'Out Of scope: Label',
  },
  {
    code: `root['json_config']['service_cost']['project_management_hours']`,
    name: 'Project Management Hours (Estimate)',
  },
  {
    code: `root['json_config']['service_cost']['after_hours_rate']`,
    name: 'After Hours Rate (Sat, Sun, and M-F after 6pm)(In $)',
  },
  {
    code: `root['json_config']['service_cost']['engineering_hours']`,
    name: 'Engineering Hours (Estimate)',
  },
  {
    code: `root['json_config']['service_cost']['engineering_hourly_rate']`,
    name: 'Engineering Hourly Rate(In $)',
  },
  {
    code: `root['json_config']['service_cost']['project_management_hourly_rate']`,
    name: 'Project Managment Hourly Rate(In $)',
  },
  {
    code: `root['json_config']['service_cost']['notesMD']`,
    name: 'Notes',
  },
  {
    code: `root['json_config']['service_cost']['after_hours']`,
    name: 'After Hours (Sat, Sun, and M-F after 6pm)',
  },
  {
    code: `root['json_config']['service_cost']['internal_cost_fixed_fee']`,
    name: 'Internal Cost Fixed Fee(Cost in $)',
  },
  {
    code: `root['json_config']['service_cost']['internal_cost_t_and_m_fee']`,
    name: 'Internal Cost T&M(Cost in $)',
  },
  {
    code: `root['json_config']['service_cost']['customer_cost_fixed_fee']`,
    name: 'Customer Cost Fixed Fee(In $)',
  },
  {
    code: `root['json_config']['service_cost']['customer_cost_t_and_m_fee']`,
    name: 'Customer Cost T&M(Cost in $)',
  },
  {
    code: `root['json_config']['service_cost']['contractors'][0]`,
    name: 'Contractors 1',
  },
  {
    code: `root['json_config']['service_cost']['contractors'][1]`,
    name: 'Contractors 2',
  },
  {
    code: `root['json_config']['service_cost']['travels'][1]`,
    name: 'Travels 2',
  },
  {
    code: `root['json_config']['service_cost']['travels'][0]`,
    name: 'Travels 1',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][0]['delivery_model']['value']`,
    name: 'Implementation Logistics 1 Delivery Model',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][1]['delivery_model']['value']`,
    name: 'Implementation Logistics 2  Delivery Model',
  },

  {
    code: `root['json_config']['implementation_logistics']['sections'][2]['delivery_model']['value']`,
    name: 'Implementation Logistics 3  Delivery Model',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][3]['delivery_model']['value']`,
    name: 'Implementation Logistics 4  Delivery Model',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][0]['detail']['value']`,
    name: 'Implementation Logistics 1 Detail',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][1]['detail']['value']`,
    name: 'Implementation Logistics 2 Detail',
  },

  {
    code: `root['json_config']['implementation_logistics']['sections'][2]['detail']['value']`,
    name: 'Implementation Logistics 3 Detail',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][3]['detail']['value']`,
    name: 'Implementation Logistics 4 Detail',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][0]['column1']['value']`,
    name: 'Implementation Logistics 1 Column',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][1]['column2']['value']`,
    name: 'Implementation Logistics 2 Column',
  },

  {
    code: `root['json_config']['implementation_logistics']['sections'][2]['column3']['value']`,
    name: 'Implementation Logistics 3 Column',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][3['column4']['value']]`,
    name: 'Implementation Logistics 4 Column',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][0]`,
    name: 'Implementation Logistics 1',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][1]`,
    name: 'Implementation Logistics 2',
  },

  {
    code: `root['json_config']['implementation_logistics']['sections'][2]`,
    name: 'Implementation Logistics 3',
  },
  {
    code: `root['json_config']['implementation_logistics']['sections'][3]`,
    name: 'Implementation Logistics 4',
  },
  {
    code: `root['json_config']['terms_fixed_fee']['terms']['label']`,
    name: 'Terms Section: Label',
  },
  {
    code: `root['json_config']['terms_fixed_fee']['terms']['value']`,
    name: 'Terms Section: Value',
  },
  {
    code: `root['json_config']['terms_fixed_fee']['terms']['value_markdown']`,
    name: 'Terms Section: Value',
  },
  {
    code: `root['json_config']['project_management_fixed_fee']['project_management']['label']`,
    name: 'Project Management: Label',
  },
  {
    code: `root['json_config']['project_management_fixed_fee']['project_management']['value_markdown']`,
    name: 'Project Management: Value',
  },
  {
    code: `root['json_config']['project_management_fixed_fee']['project_management']['value']`,
    name: 'Project Management: Value',
  },
  {
    code: `root['json_config']['project_outcome']['project_outcome']['label']`,
    name: 'Project Outcome: Label',
  },
  {
    code: `root['json_config']['project_outcome']['project_outcome']['value_markdown']`,
    name: 'Project Outcome: Value',
  },
  {
    code: `root['doc_type']`,
    name: 'Doc Type',
  },
  {
    code: `root['category_id']`,
    name: 'Category',
  },
  {
    code: `root['json_config']['project_management_t_and_m']['project_management']['label']`,
    name: 'Project Management T&M: Label',
  },
  {
    code: `root['json_config']['project_management_t_and_m']['project_management']['value_markdown']`,
    name: 'Project Management T&M: Value',
  },
  {
    code: `root['json_config']['terms_t_and_m']['terms']['value_markdown']`,
    name: 'Terms T&M: Value',
  },
  {
    code: `root['json_config']['terms_t_and_m']['terms']['label']`,
    name: 'Terms T&M: Label',
  },
  {
    code: `root['service_catalog_category_id']`,
    name: 'Service Category',
  },
  {
    code: `root['customers_not_assigned']`,
    name: 'Rule Assigned/ UnAssigned',
  },
  {
    code: `root['is_disabled']`,
    name: 'Is Disabled',
  },
  {
    code: `root['json_config']['alternates']['alternates']['value_markdown'`,
    name: 'Alternates to consider',
  },
  {
    code: `root['json_config']['change_description']['change_description']['value_markdown']`,
    name: 'Describe Change',
  },
  {
    code: `root['json_config']['change_impact']['change_impact']['value_markdown']`,
    name: 'Change Impact',
  },
  {
    code: `root['json_config']['reason']['reason']['value_markdown']`,
    name: 'Reason for Change',
  },
  {
    code: `root['json_config']['technical_changes']['technical_changes']['value_markdown']`,
    name: 'Technical Changes',
  },
  {
    code: `root['json_config']['change_affects']['change_affects']`,
    name: 'change_affects',
  },
];