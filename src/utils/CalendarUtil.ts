import moment from "moment";
import { countryCodes } from "./countryCodes";

export const toInputDateTime = (d: string): string => {
  if (d) {
    const local: Date = new Date(d);
    local.setMinutes(local.getMinutes() - local.getTimezoneOffset());

    return local.toISOString().slice(0, 16);
  }

  return "";
};

export const fromInputDateTime = (d: string) => {
  if (d) {
    const local: Date = new Date(d);

    return local.toISOString();
  }

  return "";
};

export const fromISOStringToDate = (str: string) => {
  if (str) {
    return str.slice(0, 10);
  }

  return "";
};

export const fromISOStringToDateTimeString = (ISOString: string) => {
  if (ISOString) {
    return moment(ISOString).format("MM/DD/YYYY h:mm A");
  }

  return "";
};

export const utcToLocalInLongFormat = (date: string | Date) => {
  if (date) {
    // UTC time
    const utcTime = date;
    // First way
    const offset = moment().utcOffset();
    const localText = moment
      .utc(utcTime)
      .utcOffset(offset)
      .format("MM/DD/YYYY hh:mm A");

    return localText;
  }

  return "";
};

export const getDateTimeWithFormat = (
  utcSeconds,
  dateFormat = "MM/DD/YYYY h:mm A"
) => {
  const showDate = utcSeconds ? utcToLocalInLongFormat(utcSeconds) : null; // The 0 there is the key, which sets the date to the epoch
  return showDate ? showDate : "N.A.";
};
export const fromISOStringToFormattedDateString = (
  ISOString: string | Date
) => {
  if (ISOString && moment(ISOString, moment.ISO_8601).isValid()) {
    return moment.utc(ISOString).format("MMMM D, YYYY");
  }

  return "";
};

export const fromISOStringToFormattedDateCW = (
  date: string,
  format: string = "MM/DD/YYYY"
) => {
  if (date && date !== "-") {
    // UTC time
    const formattedDate = moment.utc(date.split('T')[0]).format(format)
    return formattedDate || "-";
   }
  return "";
};

export const fromISOStringToFormattedDate = (
  date: string | Date,
  format: string = "MM/DD/YYYY"
) => {
  if (date && date !== "-") {
    // UTC time
    const utcTime = date;
    // First way
    const offset = moment().utcOffset();
    const localText = moment
      .utc(utcTime)
      .utcOffset(offset)
      .format(format);

    return localText || "-";
  }
  return "";
};

export const toTimezone = (
  date: string | Date,
  timezone: string,
  format: string
) => {
  if (date && date !== "-") {
    const converted = moment
      .utc(date)
      .tz(timezone)
      .format(format);

    return converted || "-";
  }
  return "";
};

export const toFormattedDate = (
  date: string | Date,
  format: string = "MM/DD/YYYY"
) => {
  if (date && date !== "-") {
    const localText = moment.utc(date).format(format);

    return localText || "-";
  }
  return "";
};

export const dateSetTimeZero = (date: string | Date) => {
  if (date && date !== "-") {
    const localText = moment(date).set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    });

    return localText || "-";
  }
  return "";
};

export const getStartTimeFromDate = (date: string | Date) => {
  if (date && date !== "-") {
    const localText =moment(date)
    .format('HH:mm');

    return localText || "-";
  }
  return "";
};

export const fromSecondsToReadable = (seconds: number) => {
  const numyears = Math.floor(seconds / 31536000);
  const numdays = Math.floor((seconds % 31536000) / 86400);
  const numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
  const numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
  const numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;
  let time = "";
  if (numyears) {
    time += numyears + " years ";
  }
  if (numdays) {
    time += numdays + " days ";
  }
  if (numhours) {
    time += numhours + " hours ";
  }
  if (numminutes) {
    time += numminutes + " minutes ";
  }
  if (numseconds) {
    time += numseconds + " seconds";
  }

  return time;
};

export const phoneNumberInFormat = (code: string, phone: any) => {
  let phoneNumber = "";
  if (phone && phone.length > 8) {
    phoneNumber = `${code ? `+${code}` : ""} ${phone.slice(0, 3)}-${phone.slice(
      3,
      6
    )}-${phone.slice(6, 10)}`;
  }
  if (phone && phone.includes("-")) {
    phoneNumber = `${code ? `+${code}` : "+1"} ${phone}`;
  }
  return phoneNumber ? phoneNumber : "N.A.";
};

export const setDateOnly = (time, date) => {
  let myDate = moment.utc(date);
  let selectedDateTime = moment.utc(time);

  selectedDateTime.date(myDate.date());
  selectedDateTime.month(myDate.month());
  selectedDateTime.year(myDate.year());
  return selectedDateTime;
};

export const getCountryCodes = () => {
  if (countryCodes && countryCodes.length > 0) {
    return countryCodes.map(country => ({
      value: country.code,
      label: `${country.code} (${country.name})`,
    }));
  } else {
    return [];
  }
};