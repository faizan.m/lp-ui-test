import _ from "lodash";

export const  searchInFields = (row, search, fields) => {
  const searched = fields.map(f => _.get(row, f, '') && _.get(row, f, '').toLowerCase().indexOf(search) !== -1)
  return searched.includes(true);
}