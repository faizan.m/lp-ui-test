import jwtDecode from 'jwt-decode';
import moment from 'moment';

const PROFILE = 'profile';
const ID_TOKEN = 'id_token';
const PROFILE_EXPIRY = 'profile_expiry';
const REFRESH_TOKEN = 'refresh';
const REFRESH_EXPIRY = 'refresh_expiry';
const LAST_ACTIVITY = 'lastActivity';
const INACTIVITY = 'inactivity';
const LAST_ACTIVITY1 = 'LAST_ACTIVITY1';
const refreshTokenTime = 'refreshTokenTime';
const TokenTime = 'TokenTime';

export const storeUserProfile = (jwtToken: any) => {
  const userProfile = jwtDecode(jwtToken.token);
  localStorage.setItem(ID_TOKEN, jwtToken.token);
  localStorage.setItem(PROFILE, JSON.stringify(userProfile));
  localStorage.setItem(PROFILE_EXPIRY, userProfile.exp);
  if (jwtToken && jwtToken.refresh) {
    const refreshToken = jwtDecode(jwtToken.refresh);
    localStorage.setItem(REFRESH_TOKEN, jwtToken.refresh);
    localStorage.setItem(REFRESH_EXPIRY, refreshToken.exp);
    localStorage.setItem(INACTIVITY, refreshToken.inactivity);
  }
  if(userProfile.scopes.type === 'CUSTOMER'){
    localStorage.removeItem('customerId');
  }
  const TokenTime = new Date(0);
  TokenTime.setUTCSeconds(Number(userProfile.exp));
  localStorage.setItem('TokenTime', TokenTime.toString());
  const expiry = localStorage.getItem(REFRESH_EXPIRY);
  const refreshTokenTime = new Date(0);
  refreshTokenTime.setUTCSeconds(Number(expiry));
  localStorage.setItem('refreshTokenTime', refreshTokenTime.toString());

  return userProfile;
};

export const userLogOut = () => {
  localStorage.removeItem(ID_TOKEN);
  localStorage.removeItem(PROFILE);
  localStorage.removeItem(PROFILE_EXPIRY);
  localStorage.removeItem(REFRESH_TOKEN);
  localStorage.removeItem(REFRESH_EXPIRY);
  localStorage.removeItem(LAST_ACTIVITY);
  localStorage.removeItem(INACTIVITY);
  localStorage.removeItem(TokenTime);
  localStorage.removeItem(refreshTokenTime);
  localStorage.removeItem(LAST_ACTIVITY1);
  // window.location.reload();
};

export const isLoggedIn = () => {
  const expiry = localStorage.getItem(REFRESH_EXPIRY);
  const userProfile = localStorage.getItem(PROFILE);

  if (expiry !== null && userProfile !== null) {
    const expiryDate = new Date(0);
    expiryDate.setUTCSeconds(Number(expiry));

    if (new Date() < expiryDate) {
      return true;
    }
  }

  return false;
};
export const isTokeExpiring = () => {
  const expiry = localStorage.getItem(PROFILE_EXPIRY);
  const userProfile = localStorage.getItem(PROFILE);

  if (expiry !== null && userProfile !== null) {
    let lastActivity = null;

    const refreshTokenTime = new Date(0);
    refreshTokenTime.setUTCSeconds(Number(expiry) - 60);

    if (new Date() < refreshTokenTime) {
      lastActivity = new Date();
      localStorage.setItem(LAST_ACTIVITY, lastActivity.toString());
      localStorage.setItem('LAST_ACTIVITY1', lastActivity.toString());

      return false;
    }
    const checkActivity = localStorage.getItem(LAST_ACTIVITY);

    if (checkActivity) {
      const end = moment(new Date());
      const duration = moment.duration(end.diff(checkActivity));
      const userInActivityInSeconds = duration.asSeconds();
      localStorage.setItem(
        'userInActivityInSeconds',
        userInActivityInSeconds.toString()
      );
      const maxActivityInSeconds = getInactivityTime();
      if (userInActivityInSeconds > maxActivityInSeconds) {
        userLogOut();
      }
    }
  }

  return true;
};

export const isTokeExpired = () => {
  const expiry = localStorage.getItem(PROFILE_EXPIRY);
  const userProfile = localStorage.getItem(PROFILE);

  if (expiry !== null && userProfile !== null) {
    const refreshTokenTime = new Date(0);
    refreshTokenTime.setUTCSeconds(Number(expiry));

    if (new Date() < refreshTokenTime) {
      return false;
    }
  }

  return true;
};
export const getUserProfile = (): IUserProfile => {
  return JSON.parse(localStorage.getItem(PROFILE));
};

export const getIdToken = () => {
  return localStorage.getItem(ID_TOKEN);
};

export const getLocalRefreshToken = () => {
  return localStorage.getItem(REFRESH_TOKEN);
};
export const getInactivityTime = () => {
  const inactivity = localStorage.getItem(INACTIVITY);

  return inactivity === 'undefined' ? 28800 : inactivity;
};

export const getTempToken = () => {
  return sessionStorage.getItem('eyJ0eX1QiLCJ2fa');
};
