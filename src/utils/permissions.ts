export const allowPermission = id => {
  let isAllow = false;
  const profile: any = JSON.parse(localStorage.getItem('profile'));
  permissions.map(permission => {
    if (permission.id === id && profile && profile.scopes) {
      permission.types.map((type, i) => {
        if (
          type.key === profile.scopes.type &&
          type.roles.indexOf(profile.scopes.role) > -1
        ) {
          isAllow = true;
        }
      });
    }
  });

  return isAllow;
};


export const allowFeaturePermission = (userId: string, feature: string, accessList: IFeatureAccess[]) => {
  let isAllow = false;
  for (const el of accessList) {
    if(el.user === userId) {
      isAllow = true;
      break;
    }
  }
  return isAllow;
};


const ALL_USER_ROLES =  [USER_ROLE.OWNER, USER_ROLE.ADMIN, USER_ROLE.STAFF];

const permissions = [
  {
    id: 'create_provider',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: [USER_ROLE.OWNER] }],
  },
  {
    id: 'view_provider',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: ALL_USER_ROLES }],
  },
  {
    id: 'edit_provider',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: [USER_ROLE.OWNER, USER_ROLE.ADMIN] }],
  },
  {
    id: 'add_admin_user',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: [USER_ROLE.OWNER, USER_ROLE.ADMIN] }],
  },
  {
    id: 'view_admin_user',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: ALL_USER_ROLES }],
  },
  {
    id: 'add_acelavar_user',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: ALL_USER_ROLES }],
  },
  {
    id: 'search_acelavar_user',
    types: [{ key: USER_TYPE.ACCELAVAR, roles: ALL_USER_ROLES }],
  },
  {
    id: 'add_user',
    types: [{ key: USER_TYPE.PROVIDER, roles: [USER_ROLE.OWNER, USER_ROLE.ADMIN] }],
  },
  {
    id: 'add_customer_user',
    types: [{ key: USER_TYPE.CUSTOMER, roles: [USER_ROLE.OWNER, USER_ROLE.ADMIN] }],
  },
  {
    id: 'activate_user',
    types: [{ key: USER_TYPE.PROVIDER, roles: [USER_ROLE.OWNER] }],
  },
  {
    id: 'renewal_actions',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: [USER_ROLE.OWNER, USER_ROLE.ADMIN] },
    ],
  },

  {
    id: 'edit_device',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'view_device',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'search_device',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'add_device',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'filters',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'import',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'export',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'update_contract',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'update_status',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'renewal_enable_disable',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },
  {
    id: 'collector_enable_disable',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },
  {
    id: 'edit_customer_user',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },
  {
    id: 'resend_email_for_customer_user',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },
  {
    id: 'edit_customer_details',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },

  {
    id: 'upload_renewal_data',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'reject_renewal_action',
    types: [
      { key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES },
      { key: USER_TYPE.CUSTOMER, roles: ALL_USER_ROLES },
    ],
  },
  {
    id: 'search_provider_user',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },
  {
    id: 'customer_user_activate',
    types: [{ key: USER_TYPE.CUSTOMER, roles: [USER_ROLE.OWNER] }],
  },
  {
    id: 'customer_config_user_activate',
    types: [{ key: USER_TYPE.PROVIDER, roles: ALL_USER_ROLES }],
  },
  {
    id: 'finance_report',
    types: [{ key: USER_TYPE.PROVIDER, roles: [USER_ROLE.OWNER] }],
  },
];
