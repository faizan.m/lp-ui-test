export function formatPhone(phone: string) {
  if (phone) {
    let formattedPhone = '';

    if (phone.length > 10) {
      const isPlusSymbol = phone.charAt(0) === '+' ? true : false;
      if (isPlusSymbol) {
        formattedPhone = `${phone.slice(0, 2)}-${phone.slice(
          2,
          5
        )}-${phone.slice(5)}`;
      } else {
        formattedPhone = `+${phone.slice(0, 1)}-${phone.slice(
          1,
          4
        )}-${phone.slice(4)}`;
      }

      return formattedPhone;
    }

    if (phone.length === 10) {
      formattedPhone = `(${phone.slice(0, 3)}) ${phone.slice(
        3,
        6
      )}-${phone.slice(6)}`;

      return formattedPhone;
    }

    if (phone.length < 10) {
      formattedPhone = `${phone.slice(0, 3)}-${phone.slice(3)}`;

      return formattedPhone;
    }

    return formattedPhone;
  } else {
    return `N/A`;
  }
}
