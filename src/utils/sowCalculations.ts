import { round } from "lodash";
import { commonFunctions } from "./commonFunctions";

export const getRecommendedHours = (hours: number) => {
  let recommendedHours = 5;
  if (hours > 40) {
    recommendedHours = (hours - 40) * 0.22 + 5;
  }

  return recommendedHours.toFixed(2);
};

export const getCustomerCost = (contractor: IContractor) => {
  const partnerMargin = contractor.margin_percentage / 100;
  const customerCost = contractor.partner_cost / (1 - partnerMargin);

  return customerCost && customerCost > 0
    ? parseFloat(customerCost.toFixed(2))
    : 0;
};

export const getCalculatedHourlyResource = (
  resource: IHourlyResource
): IHourlyResource => {
  const customer_cost = round(resource.hours * resource.hourly_rate, 2);
  const internal_cost = round(resource.hours * resource.hourly_cost, 2);
  const margin = customer_cost - internal_cost;
  const margin_percentage =
    customer_cost !== 0 ? round((margin / customer_cost) * 100, 1) : 0;
  return {
    ...resource,
    margin,
    customer_cost,
    internal_cost,
    margin_percentage,
  };
};

export const getFixedFeeServiceCostSub = (
  jsonConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let serviceCost = 0;
  const engineeringHours = jsonConfig.service_cost.engineering_hours;
  const engineeringHourlyRate = jsonConfig.service_cost.engineering_hourly_rate;
  const PMHours = jsonConfig.service_cost.project_management_hours;
  const PMHourlyRate = jsonConfig.service_cost.project_management_hourly_rate;
  const afterHours = jsonConfig.service_cost.after_hours;
  const afterHourlyRate = jsonConfig.service_cost.after_hours_rate;

  serviceCost =
    (engineeringHours * engineeringHourlyRate +
      PMHours * PMHourlyRate +
      afterHours * afterHourlyRate) *
    (1 + docSetting.fixed_fee_variable);

  return serviceCost || 0;
};

export const getFixedFeeInternalCostSub = (
  jsonConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let InternalCost = 0;
  const engineeringHours = jsonConfig.service_cost.engineering_hours;

  const PMHours = jsonConfig.service_cost.project_management_hours;
  const afterHours = jsonConfig.service_cost.after_hours || 0;

  InternalCost =
    engineeringHours * docSetting.engineering_hourly_cost +
      PMHours * docSetting.pm_hourly_cost +
      afterHours * docSetting.after_hours_cost || 0;

  return InternalCost;
};

export const getCustomerCostFixedFee = (
  jsonConfig: IJSONConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let customerCost = 0;
  const travelCost = Boolean(
    jsonConfig.service_cost.travels && jsonConfig.service_cost.travels.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.travels.map((el) => el.cost || 0)
        )
      )
    : 0;
  const contractorCost = Boolean(
    jsonConfig.service_cost.contractors &&
      jsonConfig.service_cost.contractors.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.contractors.map((el) => el.customer_cost || 0)
        )
      )
    : 0;
  customerCost =
    travelCost +
    contractorCost +
    getFixedFeeServiceCostSub(jsonConfig, docSetting);

  return roudingOffNext(customerCost);
};

export const getFixedFeeInternalCost = (
  jsonConfig: IJSONConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let customerCost = 0;
  const travelCost = Boolean(
    jsonConfig.service_cost.travels && jsonConfig.service_cost.travels.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.travels.map((el) => el.cost || 0)
        )
      )
    : 0;
  const contractorCost = Boolean(
    jsonConfig.service_cost.contractors &&
      jsonConfig.service_cost.contractors.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.contractors.map((el) => el.partner_cost || 0)
        )
      )
    : 0;
  customerCost =
    travelCost +
    contractorCost +
    getFixedFeeInternalCostSub(jsonConfig, docSetting);
  return roudingOffNext(customerCost);
};

export const getCustomerCostTM = (jsonConfig) => {
  let InternalCost = 0;
  const engineeringHours = jsonConfig.service_cost.engineering_hours;
  const engineeringHourlyRate = jsonConfig.service_cost.engineering_hourly_rate;
  const PMHourlyRate = jsonConfig.service_cost.project_management_hourly_rate;

  const PMHours = jsonConfig.service_cost.project_management_hours;
  const afterHours = jsonConfig.service_cost.after_hours;
  const afterHourlyRate = jsonConfig.service_cost.after_hours_rate;
  InternalCost =
    engineeringHours * engineeringHourlyRate +
    PMHours * PMHourlyRate +
    afterHours * afterHourlyRate;

  return roudingOffNext(InternalCost);
};

export const getInternalCostTM = (jsonConfig, docSetting: IDOCSetting) => {
  let InternalCost = 0;
  if (!docSetting) {
    return 0;
  }
  const engineeringHours = jsonConfig.service_cost.engineering_hours;
  const PMHours = jsonConfig.service_cost.project_management_hours;
  const afterHours = jsonConfig.service_cost.after_hours;
  InternalCost =
    engineeringHours * docSetting.engineering_hourly_cost +
    PMHours * docSetting.pm_hourly_cost +
    afterHours * docSetting.after_hours_cost;

  return roudingOffNext(InternalCost);
};

export const getSowCalculationFields = (
  serviceCost: IServiceCost,
  docSetting: IDOCSetting,
  docType: SowDocType
): ISoWCalculationFields => {
  let roundToDigit = (num: number, digit: number = 0) =>
    Number(num.toFixed(digit));
  let totalCustomerCost: number = 0,
    totalInternalCost: number = 0,
    totalMargin: number = 0,
    totalMarginPercent: number = 0,
    riskBudgetCustomerCost: number = 0,
    riskBudgetInternalCost: number = 0,
    riskBudgetMargin: number = 0,
    riskBudgetMarginPercent: number = 0,
    contractorCustomerCost: number = 0,
    contractorInternalCost: number = 0,
    contractorMargin: number = 0,
    contractorMarginPercent: number = 0,
    proSerInternalCost: number = 0,
    proSerCustomerCost: number = 0,
    proSerMargin: number = 0,
    proSerMarginPercent: number = 0,
    travelCost: number = 0;

  // Engineering hours calculation
  let engineeringHoursInternalCost = roundToDigit(
    serviceCost.engineering_hours * docSetting.engineering_hourly_cost
  );
  let engineeringHoursCustomerCost = roundToDigit(
    serviceCost.engineering_hours * serviceCost.engineering_hourly_rate
  );
  let engineeringHoursMargin =
    engineeringHoursCustomerCost - engineeringHoursInternalCost;
  let engineeringHoursMarginPercent =
    engineeringHoursCustomerCost !== 0
      ? roundToDigit(
          (engineeringHoursMargin / engineeringHoursCustomerCost) * 100,
          1
        )
      : 0;

  // Engineering after hours calculation
  let afterHoursInternalCost = roundToDigit(
    serviceCost.after_hours * docSetting.after_hours_cost
  );
  let afterHoursCustomerCost = roundToDigit(
    serviceCost.after_hours * serviceCost.after_hours_rate
  );
  let afterHoursMargin = afterHoursCustomerCost - afterHoursInternalCost;
  let afterHoursMarginPercent =
    afterHoursCustomerCost !== 0
      ? roundToDigit((afterHoursMargin / afterHoursCustomerCost) * 100, 1)
      : 0;

  // Integration Technician calculation
  let integrationTechnicianInternalCost = roundToDigit(
    serviceCost.integration_technician_hours *
      docSetting.integration_technician_hourly_cost
  );
  let integrationTechnicianCustomerCost = roundToDigit(
    serviceCost.integration_technician_hours *
      serviceCost.integration_technician_hourly_rate
  );
  let integrationTechnicianMargin =
    integrationTechnicianCustomerCost - integrationTechnicianInternalCost;
  let integrationTechnicianMarginPercent =
    integrationTechnicianCustomerCost !== 0
      ? roundToDigit(
          (integrationTechnicianMargin / integrationTechnicianCustomerCost) *
            100,
          1
        )
      : 0;

  // Project management hours calculation
  let pmHoursInternalCost = roundToDigit(
    serviceCost.project_management_hours * docSetting.pm_hourly_cost
  );
  let pmHoursCustomerCost = roundToDigit(
    serviceCost.project_management_hours *
      serviceCost.project_management_hourly_rate
  );
  let pmHoursMargin = pmHoursCustomerCost - pmHoursInternalCost;
  let pmHoursMarginPercent =
    pmHoursCustomerCost !== 0
      ? roundToDigit((pmHoursMargin / pmHoursCustomerCost) * 100, 1)
      : 0;

  // Hourly Contract Labor Calculations
  let hourlyLaborCustomerCost: number = 0,
    hourlyLaborInternalCost: number = 0,
    hourlyLaborMargin: number = 0,
    hourlyLaborMarginPercent: number = 0;
  if (serviceCost.hourly_resources) {
    hourlyLaborInternalCost = roundToDigit(
      commonFunctions.arrSum(
        serviceCost.hourly_resources.map((el) => el.internal_cost)
      )
    );
    hourlyLaborCustomerCost = roundToDigit(
      commonFunctions.arrSum(
        serviceCost.hourly_resources.map((el) => el.customer_cost)
      )
    );
    hourlyLaborMargin = hourlyLaborCustomerCost - hourlyLaborInternalCost;
    hourlyLaborMarginPercent =
      hourlyLaborCustomerCost !== 0
        ? roundToDigit((hourlyLaborMargin / hourlyLaborCustomerCost) * 100, 1)
        : 0;
  }

  totalInternalCost =
    engineeringHoursInternalCost +
    afterHoursInternalCost +
    pmHoursInternalCost +
    integrationTechnicianInternalCost +
    hourlyLaborInternalCost;
  totalCustomerCost =
    engineeringHoursCustomerCost +
    afterHoursCustomerCost +
    pmHoursCustomerCost +
    integrationTechnicianCustomerCost +
    hourlyLaborCustomerCost;

  // Professional Services (All Cost combined)
  proSerCustomerCost =
    engineeringHoursCustomerCost +
    afterHoursCustomerCost +
    pmHoursCustomerCost +
    integrationTechnicianCustomerCost +
    hourlyLaborCustomerCost;
  proSerInternalCost =
    engineeringHoursInternalCost +
    afterHoursInternalCost +
    pmHoursInternalCost +
    integrationTechnicianInternalCost +
    hourlyLaborInternalCost;
  proSerMargin = proSerCustomerCost - proSerInternalCost;
  proSerMarginPercent =
    proSerCustomerCost !== 0
      ? roundToDigit((proSerMargin / proSerCustomerCost) * 100, 1)
      : 0;

  // Currently Risk Budget Calculations is for both Fixed Fee and
  // Change Request type documents change that
  if (docType !== SowDocType.T_AND_M) {
    riskBudgetCustomerCost = roundToDigit(
      (engineeringHoursCustomerCost +
        afterHoursCustomerCost +
        pmHoursCustomerCost +
        integrationTechnicianCustomerCost +
        hourlyLaborCustomerCost) *
        docSetting.fixed_fee_variable
    );

    riskBudgetInternalCost = roundToDigit(
      (engineeringHoursInternalCost +
        afterHoursInternalCost +
        pmHoursInternalCost +
        integrationTechnicianInternalCost +
        hourlyLaborInternalCost) *
        docSetting.fixed_fee_variable
    );

    riskBudgetMargin = roundToDigit(
      riskBudgetCustomerCost - riskBudgetInternalCost
    );

    riskBudgetMarginPercent =
      riskBudgetCustomerCost !== 0
        ? roundToDigit((riskBudgetMargin / riskBudgetCustomerCost) * 100, 1)
        : 0;

    contractorInternalCost = Boolean(
      serviceCost.contractors && serviceCost.contractors.length
    )
      ? roundToDigit(
          commonFunctions.arrSum(
            serviceCost.contractors.map((el) => el.partner_cost || 0)
          )
        )
      : 0;
    contractorCustomerCost = Boolean(
      serviceCost.contractors && serviceCost.contractors.length
    )
      ? roundToDigit(
          commonFunctions.arrSum(
            serviceCost.contractors.map((el) => el.customer_cost || 0)
          )
        )
      : 0;
    contractorMargin = contractorCustomerCost - contractorInternalCost;
    contractorMarginPercent =
      contractorCustomerCost !== 0
        ? roundToDigit((contractorMargin / contractorCustomerCost) * 100, 1)
        : 0;

    travelCost = Boolean(serviceCost.travels && serviceCost.travels.length)
      ? roundToDigit(
          commonFunctions.arrSum(serviceCost.travels.map((el) => el.cost || 0))
        )
      : 0;

    totalCustomerCost +=
      riskBudgetCustomerCost + contractorCustomerCost + travelCost;
    totalInternalCost += riskBudgetInternalCost + contractorInternalCost;
  }

  totalMargin = totalCustomerCost - totalInternalCost;
  totalMarginPercent =
    totalCustomerCost !== 0
      ? roundToDigit((totalMargin / totalCustomerCost) * 100, 1)
      : 0;

  return {
    engineeringHoursInternalCost,
    engineeringHoursCustomerCost,
    engineeringHoursMargin,
    engineeringHoursMarginPercent,
    afterHoursInternalCost,
    afterHoursCustomerCost,
    afterHoursMargin,
    afterHoursMarginPercent,
    integrationTechnicianInternalCost,
    integrationTechnicianCustomerCost,
    integrationTechnicianMargin,
    integrationTechnicianMarginPercent,
    pmHoursInternalCost,
    pmHoursCustomerCost,
    pmHoursMargin,
    pmHoursMarginPercent,
    riskBudgetInternalCost,
    riskBudgetCustomerCost,
    riskBudgetMargin,
    riskBudgetMarginPercent,
    contractorInternalCost,
    contractorCustomerCost,
    contractorMargin,
    contractorMarginPercent,
    proSerCustomerCost,
    proSerInternalCost,
    proSerMargin,
    proSerMarginPercent,
    hourlyLaborCustomerCost,
    hourlyLaborInternalCost,
    hourlyLaborMargin,
    hourlyLaborMarginPercent,
    totalCustomerCost,
    totalInternalCost,
    totalMargin,
    totalMarginPercent,
    travelCost,
  };
};

export const roudingOffNext = (value: number) => {
  let roundingValue = 0;
  roundingValue = value && value > 0 ? Math.ceil(value / 10) * 10 : 0;
  return roundingValue.toFixed(2);
};
