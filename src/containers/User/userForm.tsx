import cloneDeep from 'lodash/cloneDeep';
import React from 'react';
import validator from 'validator';
import Select from '../../components/Input/Select/select';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { getUserProfile } from '../../utils/AuthUtil';
import Validator from '../../utils/validator';

import './style.scss';
import { getCountryCodes } from '../../utils/CalendarUtil';

interface IUserFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (user: ISuperUser) => void;
  userRoles: Array<{ value: number; label: string }>;
  user?: ISuperUser;
  userType: string;
  errorList?: any; //TODO
  isProvideAdmin?: boolean;
  userRoleInitial?: {
    disabled: boolean;
    initialValue: number;
  };
  isLoading?: boolean;
  customers?: ICustomerShort[];
  fetchUserForCustomer?: any;
  customerUsers?: ISuperUser[];
  isUsersFetching?: boolean;
  loggenInUser?: ISuperUser;
  customer_instance_id?: string;
  fetchTerritoryMembers?:any;
  terretoryMembers?:any;
  getRoleRateMappingList?:any;
}

interface IUserFormState {
  user: ISuperUser;
  isFormValid: boolean;
  error: {
    first_name: IFieldValidation;
    last_name: IFieldValidation;
    email: IFieldValidation;
    role: IFieldValidation;
    department: IFieldValidation;
    office_phone: IFieldValidation;
    title: IFieldValidation;
    cell_phone_number: IFieldValidation;
    phone_number: IFieldValidation;
    twitter_profile_url: IFieldValidation;
    linkedin_profile_url: IFieldValidation;
    country_code: IFieldValidation;
    office_phone_country_code: IFieldValidation;
    system_member_crm_id: IFieldValidation;
    cco_id: IFieldValidation;
  };
  isEdit: boolean;
  statusList?:  any[];
  loading?: boolean;
}

export default class UserForm extends React.Component<
  IUserFormProps,
  IUserFormState
> {
  static validator = new Validator();
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };

  static emptyState: IUserFormState = {
    user: {
      id: '',
      email: '',
      first_name: '',
      last_name: '',
      phone_number: '',
      user_role: null,
      is_active: false,
      profile: {
        country_code: '1',
        department: '',
        title: '',
        office_phone: '',
        cell_phone_number: '',
        twitter_profile_url: '',
        linkedin_profile_url: '',
        cco_id: "",
        cco_id_acknowledged: false
      },
    },
    isFormValid: true,
    error: {
      first_name: {
        ...UserForm.emptyErrorState,
      },
      last_name: {
        ...UserForm.emptyErrorState,
      },
      email: {
        ...UserForm.emptyErrorState,
      },
      system_member_crm_id: {
        ...UserForm.emptyErrorState,
      },
      role: {
        ...UserForm.emptyErrorState,
      },
      department: {
        ...UserForm.emptyErrorState,
      },
      title: {
        ...UserForm.emptyErrorState,
      },
      office_phone: {
        ...UserForm.emptyErrorState,
      },
      cell_phone_number: {
        ...UserForm.emptyErrorState,
      },
      twitter_profile_url: {
        ...UserForm.emptyErrorState,
      },
      linkedin_profile_url: {
        ...UserForm.emptyErrorState,
      },
      phone_number: {
        ...UserForm.emptyErrorState,
      },
      country_code: {
        ...UserForm.emptyErrorState,
      },
      office_phone_country_code: {
        ...UserForm.emptyErrorState,
      },
      cco_id: {
        ...UserForm.emptyErrorState,
      },
    },
    isEdit: false,
    statusList: [],
    loading: false,
  };

  constructor(props: IUserFormProps) {
    super(props);

    this.state = cloneDeep(UserForm.emptyState);
  }

  componentDidMount(){

    if (this.props.customer_instance_id) {
      this.props.fetchUserForCustomer(this.props.customer_instance_id);
    }

    if(this.props.fetchTerritoryMembers){
      this.props.fetchTerritoryMembers();
    }
    if(this.props.getRoleRateMappingList){
      this.getRoleRateMapping();
    }

    if (this.props.show && this.props.user) {
      this.setState({
        ...cloneDeep(UserForm.emptyState),
        user: this.props.user,
        isEdit: true,
      });
    } else if (this.props.show) {
      this.setState(cloneDeep(UserForm.emptyState));
    }

    if (this.props.userRoleInitial) {
      this.setState(prevState => ({
        user: {
          ...prevState.user,
          user_role: this.props.userRoleInitial.initialValue,
        },
      }));
    }
  }
  
  componentDidUpdate(prevProps: IUserFormProps) {
    if (this.props.show !== prevProps.show && this.props.user) {
      this.setState({
        ...cloneDeep(UserForm.emptyState),
        user: this.props.user,
        isEdit: true,
      });
    } else if (this.props.show !== prevProps.show && !this.props.user) {
      this.setState(cloneDeep(UserForm.emptyState));
    }
    if (
      this.props.show !== prevProps.show &&
      this.props.userRoleInitial
    ) {
      this.setState(prevState => ({
        user: {
          ...prevState.user,
          user_role: this.props.userRoleInitial.initialValue,
        },
      }));
    }
    if (this.props.errorList && this.props.errorList !== prevProps.errorList) {
      this.setValidationErrors(this.props.errorList);
    }
    if (
      this.props.show &&
      this.props.customer_instance_id &&
      this.props.customer_instance_id !== prevProps.customer_instance_id
    ) {
      this.props.fetchUserForCustomer(this.props.customer_instance_id);
    }
  }
  
  getRoleRateMapping = () => {
    this.setState({ loading: true });
    this.props.getRoleRateMappingList().then((action) => {
      this.setState({ loading: false, statusList: action.response });
    });
  };
  setValidationErrors = errorList => {
    const newState: IUserFormState = cloneDeep(this.state);

    Object.keys(errorList).map(key => {
      if (key === 'profile') {
        Object.keys(errorList[key]).map(childKey => {
          newState.error[childKey].errorState = IValidationState.ERROR;
          newState.error[childKey].errorMessage = errorList[key][childKey];
        });
      } else if (key !== 'detail') {
        newState.error[key].errorState = IValidationState.ERROR;
        newState.error[key].errorMessage = errorList[key];
      }
    });

    newState.isFormValid = false;
    this.setState(newState);
  };

  clearValidationStatus = () => {
    const cleanState = cloneDeep(UserForm.emptyState.error);
    const newState: IUserFormState = cloneDeep(this.state);
    newState.error = cleanState;

    this.setState(newState);
  };

  validateForm = () => {
    this.clearValidationStatus();
    const newState: IUserFormState = cloneDeep(this.state);
    newState.error = cloneDeep(UserForm.emptyState.error);

    let isValid = true;

    if (!this.state.user.first_name) {
      newState.error.first_name.errorState = IValidationState.ERROR;
      newState.error.first_name.errorMessage = 'First Name cannot be empty';

      isValid = false;
    } else if (
      this.state.user.first_name &&
      this.state.user.first_name.length > 300
    ) {
      newState.error.first_name.errorState = IValidationState.ERROR;
      newState.error.first_name.errorMessage =
        'First Name should be less than 300 chars.';

      isValid = false;
    }
    if (!Validator.isAlphaWithSpaces(this.state.user.first_name)) {
      newState.error.first_name.errorState = IValidationState.ERROR;
      newState.error.first_name.errorMessage = 'Not valid name';

      isValid = false;
    }
    if (!this.state.user.last_name) {
      newState.error.last_name.errorState = IValidationState.ERROR;
      newState.error.last_name.errorMessage = 'Last Name cannot be empty';

      isValid = false;
    } else if (
      this.state.user.last_name &&
      this.state.user.last_name.length > 300
    ) {
      newState.error.last_name.errorState = IValidationState.ERROR;
      newState.error.last_name.errorMessage =
        'First Name should be less than 300 chars.';

      isValid = false;
    }
    if (!Validator.isAlphaWithSpaces(this.state.user.last_name)) {
      newState.error.last_name.errorState = IValidationState.ERROR;
      newState.error.last_name.errorMessage = 'Not valid name';

      isValid = false;
    }
    if (!this.state.user.email) {
      newState.error.email.errorState = IValidationState.ERROR;
      newState.error.email.errorMessage = 'Email cannot be empty';

      isValid = false;
    }

    if (this.state.user.email && !validator.isEmail(this.state.user.email)) {
      newState.error.email.errorState = IValidationState.ERROR;
      newState.error.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }

    if (!this.state.user.user_role) {
      newState.error.role.errorState = IValidationState.ERROR;
      newState.error.role.errorMessage = 'Role cannot be empty';

      isValid = false;
    }

    if (
      this.state.user.phone_number &&
      !UserForm.validator.isValidPhoneNumber(this.state.user.phone_number)
    ) {
      newState.error.phone_number.errorState = IValidationState.ERROR;
      newState.error.phone_number.errorMessage = 'Enter a valid phone number';

      isValid = false;
    }

    if (
      this.state.user.profile.cell_phone_number &&
      !UserForm.validator.isValidPhoneNumber(
        this.state.user.profile.cell_phone_number
      )
    ) {
      newState.error.cell_phone_number.errorState = IValidationState.ERROR;
      newState.error.cell_phone_number.errorMessage =
        'Enter a valid phone number';

      isValid = false;
    }

    // if (!this.state.user.profile.country_code) {
    //   newState.error.country_code.errorState = IValidationState.ERROR;
    //   newState.error.country_code.errorMessage = 'Select country code';

    //   isValid = false;
    // }

    if (
      this.state.user.profile.office_phone &&
      !UserForm.validator.isValidPhoneNumber(
        this.state.user.profile.office_phone
      )
    ) {
      newState.error.office_phone.errorState = IValidationState.ERROR;
      newState.error.office_phone.errorMessage = 'Enter a valid phone number';

      isValid = false;
    }

    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  };

  getCustomerOptions = () => {
    return this.props.customers
      ? this.props.customers.map((cust, index) => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];
  };

  getCustomerUserOptions = () => {
    return this.props.customerUsers
      ? this.props.customerUsers.map((user, index) => ({
          value: user.crm_id,
          label: `${user.first_name} ${user.last_name}`,
        }))
      : [];
  };

  getTerretoryMembersOptions = () => {
    return this.props.terretoryMembers
      ? this.props.terretoryMembers.map((user, index) => ({
          value: user.id,
          label: `${user.first_name} ${user.last_name}`,
        }))
      : [];
  };

  handleCustomerChange = (e: any) => {
    const newState = cloneDeep(this.state);
    const targetName = e.target.name;
    const targetValue = e.target.value;
    newState.user.profile.customer_instance_id = targetValue;
    newState.user.profile.customer_user_instance_id = 0;
    this.setState(newState);

    if (targetName === 'customer_instance_id') {
      this.props.fetchUserForCustomer(targetValue);
    }
  };

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.user[event.target.name] = event.target.value;

    this.setState(newState);
  };
  //dont allow user to self disable
  handleIsActiveChange = (event: any) => {
    const { user_id } = getUserProfile();
    if (user_id !== this.state.user.id) {
      const targetValue = event.target.value === 'Enable' ? true : false;
      const newState = cloneDeep(this.state);
      newState.user.is_active = targetValue;

      this.setState(newState);
    }
  };

  handleProfileChange = (event: any) => {
    const newState = cloneDeep(this.state);
    const targetName = event.target.name;
    const targetValue = event.target.value;
    newState.user.profile[targetName] = targetValue;

    this.setState(newState);
  };

  onClose = e => {
    // this.setState(cloneDeep(UserForm.emptyState));
    this.clearValidationStatus();
    this.props.onClose(e);
  };

  onSubmit = () => {
    if (this.validateForm()) {
      // this.setState(cloneDeep(UserForm.emptyState));
      this.props.onSubmit(this.state.user);
    }
  };

  getTitle = () => {
    const { userType } = this.props;
    const { isEdit } = this.state;

    return `${isEdit ? 'Edit' : 'Add'} ${userType} User`;
  };

  getBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`user-modal__body ${
            this.props.isLoading ? `loading` : ''
          }`}
        >
          <Input
            field={{
              label: 'First Name',
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.user.first_name,
            }}
            error={this.state.error.first_name}
            width={6}
            placeholder="Enter First Name"
            name="first_name"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: 'Last Name',
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.user.last_name,
            }}
            error={this.state.error.last_name}
            width={6}
            placeholder="Enter Last Name"
            name="last_name"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: 'Email Address',
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.user.email,
            }}
            error={this.state.error.email}
            width={6}
            placeholder="Enter Email"
            name="email"
            onChange={this.handleChange}
            disabled={this.state.isEdit}
          />
          <Input
            field={{
              label: 'Department',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.department,
            }}
            error={this.state.error.department}
            width={6}
            placeholder="Enter Department"
            name="department"
            onChange={this.handleProfileChange}
          />
          <Input
            field={{
              label: 'Office Country code',
              isRequired: false,
              type: InputFieldType.PICKLIST,
              options: getCountryCodes(),
              value: this.state.user.profile.office_phone_country_code,
            }}
            error={this.state.error.office_phone_country_code}
            width={6}
            placeholder="code"
            name="office_phone_country_code"
            onChange={this.handleProfileChange}
            className="country-code"
          />
          <Input
            field={{
              label: 'Office Phone Number',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.office_phone,
            }}
            error={this.state.error.office_phone}
            width={6}
            placeholder="Enter Office Phone Number"
            name="office_phone"
            onChange={this.handleProfileChange}
          />
          <Input
            field={{
              label: 'Country code',
              isRequired: false,
              type: InputFieldType.PICKLIST,
              options: getCountryCodes(),
              value: this.state.user.profile.country_code,
            }}
            error={this.state.error.country_code}
            width={6}
            placeholder="code"
            name="country_code"
            onChange={this.handleProfileChange}
            className="country-code"
          />
          <Input
            field={{
              label: 'Cell Phone Number',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.cell_phone_number,
            }}
            error={this.state.error.cell_phone_number}
            width={6}
            placeholder="Enter Cell Phone Number"
            name="cell_phone_number"
            onChange={this.handleProfileChange}
          />
          <Input
            field={{
              label: 'Twitter Profile URL',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.twitter_profile_url,
            }}
            error={this.state.error.twitter_profile_url}
            width={6}
            placeholder="Enter Twitter Profile URL"
            name="twitter_profile_url"
            onChange={this.handleProfileChange}
          />
          <Input
            field={{
              label: 'Linkedin Profile URL',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.linkedin_profile_url,
            }}
            error={this.state.error.linkedin_profile_url}
            width={6}
            placeholder="Enter Linkedin Profile URL"
            name="linkedin_profile_url"
            onChange={this.handleProfileChange}
          />
          <Input
            field={{
              label: 'Title',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.title,
            }}
            error={this.state.error.title}
            width={6}
            placeholder="Enter Title"
            name="title"
            onChange={this.handleProfileChange}
          />
          <Input
            field={{
              value: this.state.user.is_active ? 'Enable' : 'Disable',
              label: 'Status',
              type: InputFieldType.RADIO,
              isRequired: false,
              options: [
                { value: 'Enable', label: 'Enable' },
                { value: 'Disable', label: 'Disable' },
              ],
            }}
            disabled={!this.state.isEdit || !this.state.user.is_password_set}
            className={!this.state.isEdit || !this.state.user.is_password_set ?  'disabled-status-edit' : ''}
            width={6}
            name="is_active"
            onChange={this.handleIsActiveChange}
          />
          <Input
            field={{
              value: this.state.user.user_role,
              label: 'Role',
              type: InputFieldType.PICKLIST,
              isRequired: true,
              options: this.props.userRoles,
            }}
            disabled={
              this.props.userRoleInitial && this.props.userRoleInitial.disabled
            }
            width={6}
            name="user_role"
            onChange={this.handleChange}
            placeholder="Select Role"
            error={this.state.error.role}
          />
          <Input
            field={{
              label: 'CCO ID',
              type: InputFieldType.TEXT,
              isRequired: false,
              value: this.state.user.profile.cco_id,
            }}
            error={this.state.error.cco_id}
            width={6}
            placeholder="Enter CCO ID"
            name="cco_id"
            onChange={this.handleProfileChange}
          />
           { this.props.getRoleRateMappingList && 

                    <Input
            field={{
              value: this.state.user.project_rate_role,
              label: 'Project Role',
              type: InputFieldType.PICKLIST,
              isRequired: false,
              options: this.state.statusList.map((d) => ({
                value: d.id,
                label: d.role_name,
                disabled: false,
              })),
            }}
            width={6}
            name="project_rate_role"
            onChange={this.handleChange}
            placeholder="Select Role"
            error={this.state.error.role}
          />
  }
          {/* {this.props.user &&
            this.props.user.type &&
            this.props.user.type === 'provider' &&
            this.props.loggenInUser &&
            this.props.loggenInUser.role &&
            this.props.loggenInUser.role === 'owner' && (
              <div
                className="field-section
                 col-md-6 col-xs-6 report-mandatory"
              >
                <div className="field__label">
                  <label className="field__label-label">Customer</label>
                  <div className="field__input">
                    <Select
                      name="customer_instance_id"
                      value={this.state.user.profile.customer_instance_id}
                      onChange={e => this.handleCustomerChange(e)}
                      options={this.getCustomerOptions()}
                      searchable={true}
                      multi={false}
                      placeholder="Select Customer"
                    />
                  </div>
                </div>
              </div>
            )} */}
          {
            this.props.loggenInUser &&
            this.props.loggenInUser.role &&
            this.props.loggenInUser.role === 'owner' && (
              <div
                className="field-section
                 col-md-6 col-xs-6 report-mandatory"
              >
                <div className="field__label">
                  <label className="field__label-label">Customer User</label>
                  <div className="field__input">
                    <Select
                      name="customer_user_instance_id"
                      value={this.state.user.profile.customer_user_instance_id}
                      onChange={e => this.handleProfileChange(e)}
                      options={this.getCustomerUserOptions()}
                      searchable={true}
                      multi={false}
                      placeholder="Select User"
                    />
                  </div>
                </div>
              </div>
            )}
            {
            this.props.loggenInUser &&
            this.props.loggenInUser.role &&
            this.props.loggenInUser.role === 'owner' && (
              <div
                className="field-section
                 col-md-6 col-xs-6 report-mandatory"
              >
                <div className="field__label">
                  <label className="field__label-label">System Member Mapping</label>
                  <div className="field__input">
                    <Select
                      name="system_member_crm_id"
                      value={this.state.user.profile.system_member_crm_id}
                      onChange={e => this.handleProfileChange(e)}
                      options={this.getTerretoryMembersOptions()}
                      searchable={true}
                      multi={false}
                      placeholder="Select User"
                    />
                  </div>
                  {this.state.error.system_member_crm_id.errorState ===
                    IValidationState.ERROR && (
                    <span className="field__error">
                      {this.state.error.system_member_crm_id.errorMessage}
                    </span>
                  )}
                </div>
              </div>
            )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`user-modal__footer
      ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content={`${this.state.isEdit ? 'Save' : 'Add'}`}
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="user-modal"
      />
    );
  }
}
