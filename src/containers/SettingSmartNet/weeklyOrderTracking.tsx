import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  uploadImage,
  UPLOAD_IMAGE_FAILURE,
  UPLOAD_IMAGE_SUCCESS,
} from "../../actions/sow";
import { fetchAllProviderUsers } from "../../actions/provider/user";
import {
  addErrorMessage,
  addInfoMessage,
  addSuccessMessage,
  addWarningMessage,
} from "../../actions/appState";
import {
  weeklyTrackingCRU,
  fetchOrderTrackingSettings,
  sendWeeklyTrackingEmail,
  getWeeklyTrackingEmailPreview,
  WEEKLY_TRACKING_SUCCESS,
  WEEKLY_TRACKING_FAILURE,
  SEND_WEEKLY_TRACKING_SUCCESS,
  GET_WEEKLY_TRACKING_PREVIEW_SUCCESS,
  FETCH_ORDER_TRACKING_SETTING_SUCCESS,
} from "../../actions/setting";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import Checkbox from "../../components/Checkbox/checkbox";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import HTMLViewer from "../../components/HTMLViewer/HTMLViewer";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import { commonFunctions } from "../../utils/commonFunctions";
import {
  CONFIG_TASK_STATUS_SUCCESS,
  fetchTaskStatusConfig,
} from "../../actions/configuration";

interface WeeklyOrderTrackingProps {
  customers: ICustomerShort[];
  providerUsers: ISuperUser[];
  fetchAllProviderUsers: () => void;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  addWarningMessage: TShowWarningMessage;
  fetchTaskStatus: (id: number) => Promise<any>;
  fetchOrderTrackingSettings: () => Promise<any>;
  getWeeklyTrackingEmailPreview: () => Promise<any>;
  uploadImage: (file: any, name: string) => Promise<any>;
  sendWeeklyTrackingEmail: (
    crm_ids: number[],
    all_customers: boolean
  ) => Promise<any>;
  weeklyTrackingCRU: (
    request: HTTPMethods,
    data?: IWeeklyOrderTrackingSettings
  ) => Promise<any>;
}

const DayOptions: string[] = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

const SuccessState: IFieldValidation = {
  errorState: IValidationState.SUCCESS,
  errorMessage: "",
};

const WeeklyOrderTracking: React.FC<WeeklyOrderTrackingProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<IErrorValidation>({});
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [users, setUsers] = useState<number[]>([]);
  const [sendToAllCustomers, setSendAllCustomers] = useState<boolean>(false);
  const [previewHTML, setPreviewHTML] = useState<string>("");
  const [openPreview, setOpenPreview] = useState<boolean>(false);
  const [sendingEmail, setSendingEmail] = useState<boolean>(false);
  const [showSendModal, setShowSendModal] = useState<boolean>(false);
  const [contactTypes, setContactTypes] = useState<IPickListOptions[]>([]);
  const [isFetchingContactTypes, setIsFetchingContactType] = useState<boolean>(
    false
  );
  const [settings, setSettings] = useState<IWeeklyOrderTrackingSettings>({
    receiver_email_ids: [],
    email_subject: "",
    email_body_text: "",
    contact_type_crm_id: null,
    weekly_schedule: [],
    sender: "",
    is_enabled: true,
  });

  useEffect(() => {
    fetchSettings();
    fetchContactTypeOptions();
    props.fetchAllProviderUsers();
  }, []);

  const CustomerOptions: IPickListOptions[] = useMemo(
    () =>
      props.customers
        ? props.customers.map((customer) => ({
            value: customer.crm_id,
            label: customer.name,
          }))
        : [],
    [props.customers]
  );

  const providerUsersOptions: IPickListOptions[] = useMemo(
    () =>
      props.providerUsers
        ? props.providerUsers.map((author: ISuperUser) => ({
            value: author.id,
            label: `${author.first_name} ${author.last_name}`,
          }))
        : [],
    [props.providerUsers]
  );

  const providerUserEmailOptions: IPickListOptions[] = useMemo(
    () =>
      props.providerUsers
        ? props.providerUsers.map((author: ISuperUser) => ({
            value: author.email,
            label: `${author.first_name} ${author.last_name}`,
          }))
        : [],
    [props.providerUsers]
  );

  const fetchSettings = () => {
    setLoading(true);
    props
      .weeklyTrackingCRU(HTTPMethods.GET)
      .then((action) => {
        if (action.type === WEEKLY_TRACKING_SUCCESS) {
          setSettings(action.response);
        } else if (action.type === WEEKLY_TRACKING_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const fetchContactTypeOptions = () => {
    setIsFetchingContactType(true);
    props
      .fetchOrderTrackingSettings()
      .then((action) => {
        if (action.type === FETCH_ORDER_TRACKING_SETTING_SUCCESS) {
          const contactTypeOptions: IPickListOptions[] = action.response.map(
            (data) => ({
              value: data.id,
              label: `${data.description}`,
            })
          );

          setContactTypes(contactTypeOptions);
        }
      })
      .finally(() => setIsFetchingContactType(false));
  };

  const closePreview = () => {
    setOpenPreview(false);
    setPreviewHTML("");
  };

  const onCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUsers([]);
    setSendAllCustomers(e.target.checked);
  };

  const closeSendModal = () => {
    setShowSendModal(false);
    setUsers([]);
    setSendAllCustomers(false);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSettings((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleChangeEmailBody = (html: string) => {
    setSettings((prevState) => ({
      ...prevState,
      email_body_text: html,
    }));
  };

  const handleChangeUser = (e: { target: { value: number[] } }) => {
    setUsers(e.target.value);
  };

  const sendEmail = () => {
    setSendingEmail(true);
    props
      .sendWeeklyTrackingEmail(users, sendToAllCustomers)
      .then((action) => {
        if (action.type === SEND_WEEKLY_TRACKING_SUCCESS) {
          const taskId = action.response.task_id;
          fetchTaskStatus(taskId, users, sendToAllCustomers);
          closeSendModal();
          props.addInfoMessage("Sending Emails...");
        }
      })
      .finally(() => setSendingEmail(false));
  };

  const previewEmail = () => {
    setLoading(true);
    props
      .getWeeklyTrackingEmailPreview()
      .then((action) => {
        if (action.type === GET_WEEKLY_TRACKING_PREVIEW_SUCCESS) {
          setPreviewHTML(action.response.html);
          setOpenPreview(true);
        } else {
          props.addErrorMessage("Error fetching collections preview");
        }
      })
      .finally(() => setLoading(false));
  };

  const onImageUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    setLoading(true);
    if (
      attachment &&
      attachment.type &&
      ["image/png", "image/jpg", "image/jpeg"].includes(attachment.type)
    ) {
      const data = new FormData();
      if (attachment) {
        data.append("filename", attachment);
        props
          .uploadImage(data, attachment)
          .then((action) => {
            if (action.type === UPLOAD_IMAGE_SUCCESS) {
              props.addSuccessMessage("Image Uploaded Successfully!");
              let imageString = `<p><img src="${action.response.file_path}" style="max-width: 100%"></p>`;
              setSettings((prevState) => ({
                ...prevState,
                email_body_text: prevState.email_body_text + imageString,
              }));
            }
            if (action.type === UPLOAD_IMAGE_FAILURE) {
              props.addErrorMessage("Image Upload Failed!");
            }
          })
          .finally(() => setLoading(false));
      }
    } else {
      props.addErrorMessage("Invalid image format.");
    }
  };

  const fetchTaskStatus = (
    taskId: number,
    users: number[],
    allCustomers: boolean
  ) => {
    if (taskId) {
      props.fetchTaskStatus(taskId).then((action) => {
        if (action.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (action.response.status === "SUCCESS") {
            const res = action.response.result;
            if (res && res.operation_status !== "SUCCESS") {
              props.addErrorMessage(`An error occurred while sending emails!`);
            }
            const skipped_customers: {
              customer_name: string;
              customer_crm_id: number;
              reason: string;
            }[] =
              res && res.payload && res.payload.skipped_customer_details
                ? res.payload.skipped_customer_details
                : [];
            if (skipped_customers.length) {
              let names: string = skipped_customers
                .map((el) => el.customer_name)
                .join(", ");
              if (allCustomers) {
                props.addSuccessMessage(
                  "Weekly Order Tracking Report sent to all the customers with open Sales Orders!"
                );
              } else {
                if (skipped_customers.length === users.length) {
                  props.addWarningMessage(
                    `Weekly Order Tracking Report not sent to any customer(${names}) as not a single selected customer had an open Sales Order!`
                  );
                } else {
                  props.addWarningMessage(
                    `Weekly Order Tracking Report not sent to the following customers as No Open Sales Orders was found: ${names}!`
                  );
                  props.addSuccessMessage(
                    "Weekly Order Tracking Report sent to all the remaining customers with open Sales Order(s)!"
                  );
                }
              }
            } else {
              props.addSuccessMessage(
                `Weekly Order Tracking Report sent to all the selected customers!`
              );
            }
          }
          if (action.response.status === "PENDING") {
            setTimeout(
              () => fetchTaskStatus(taskId, users, allCustomers),
              3000
            );
          }
          if (action.response.status === "FAILURE") {
            props.addErrorMessage(`An error occurred while sending emails!`);
          }
        }
      });
    }
  };

  const validateForm = (): boolean => {
    let isValid = true;
    const errors = cloneDeep(error);
    let arrayFields = ["receiver_email_ids", "weekly_schedule"];
    let simpleFields = ["email_subject", "contact_type_crm_id", "sender"];

    arrayFields.forEach((key) => {
      if (settings[key].length === 0) {
        errors[key] = {
          errorState: IValidationState.ERROR,
          errorMessage: "This field is required",
        };
        isValid = false;
      } else {
        errors[key] = { ...SuccessState };
      }
    });
    simpleFields.forEach((key) => {
      if (!settings[key]) {
        errors[key] = {
          errorState: IValidationState.ERROR,
          errorMessage: "This field is required",
        };
        isValid = false;
      } else errors[key] = { ...SuccessState };
    });
    if (commonFunctions.isEditorEmpty(settings.email_body_text)) {
      errors.email_body_text = {
        errorState: IValidationState.ERROR,
        errorMessage: "This field is required",
      };
      isValid = false;
    } else errors.email_body_text = { ...SuccessState };
    if (!isValid) setError(errors);
    return isValid;
  };

  const onSubmit = () => {
    if (validateForm()) {
      setLoading(true);
      props
        .weeklyTrackingCRU(
          firstSave ? HTTPMethods.POST : HTTPMethods.PUT,
          settings
        )
        .then((action) => {
          if (action.type === WEEKLY_TRACKING_SUCCESS) {
            props.addSuccessMessage(
              `Weekly Order Tracking Settings ${
                firstSave ? "Saved" : "Updated"
              } Successfully!`
            );
            setFirstSave(false);
          } else if (action.type === WEEKLY_TRACKING_FAILURE) {
            let errors = action.errorList.data;
            let validationErrors = cloneDeep(error);
            if (!Array.isArray(errors)) {
              Object.keys(errors).forEach((key) => {
                (validationErrors[key] as IFieldValidation) = {
                  errorState: IValidationState.ERROR,
                  errorMessage: errors[key],
                };
              });
              setError(validationErrors);
            } else {
              props.addErrorMessage("An error occurred.");
            }
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const renderSendEmailModal = () => (
    <ModalBase
      show={true}
      onClose={closeSendModal}
      titleElement={"Send Tracking Report Email"}
      bodyElement={
        <div className="collection-notice-main">
          <Spinner className="send-collections-loader" show={sendingEmail} />
          <Checkbox
            isChecked={sendToAllCustomers}
            name="send-to-all"
            onChange={onCheckboxChange}
            className="send-to-all-checkbox"
          >
            Send to all customers
          </Checkbox>
          {!sendToAllCustomers && (
            <Input
              field={{
                label: "Customer",
                type: InputFieldType.PICKLIST,
                value: users,
                options: CustomerOptions,
                isRequired: true,
              }}
              className="customer-user-select"
              width={12}
              multi={true}
              name="users"
              onChange={handleChangeUser}
              placeholder={`Select Customer(s)`}
            />
          )}
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={ButtonStyle.DEFAULT}
            onClick={closeSendModal}
            className="cn-btn"
          />
          <SquareButton
            content={`Send Email`}
            bsStyle={ButtonStyle.PRIMARY}
            onClick={sendEmail}
            className="cn-btn"
            disabled={!sendToAllCustomers && users.length === 0}
          />
        </div>
      }
    />
  );

  return (
    <div className="collections-main-container">
      {showSendModal && renderSendEmailModal()}
      <Spinner className="collections-loader" show={loading} />
      <HTMLViewer
        show={openPreview}
        onClose={closePreview}
        titleElement={`View Weekly Tracking Email Preview`}
        previewHTML={previewHTML}
      />
      <div className="collections-tr-btns">
        {!firstSave && (
          <>
            <SquareButton
              onClick={previewEmail}
              content={"Preview"}
              bsStyle={ButtonStyle.PRIMARY}
              className="collections-preview-btn"
              disabled={loading}
            />
            <SquareButton
              onClick={() => setShowSendModal(true)}
              content="Send Tracking Report"
              bsStyle={ButtonStyle.PRIMARY}
              disabled={loading}
            />
          </>
        )}
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Send from User Email",
            type: InputFieldType.PICKLIST,
            value: settings.sender,
            options: providerUsersOptions,
            isRequired: true,
          }}
          multi={false}
          width={6}
          placeholder="Select User"
          error={!settings.sender ? error.sender : SuccessState}
          name="sender"
          onChange={handleChange}
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Send to Users",
            type: InputFieldType.PICKLIST,
            value: settings.receiver_email_ids,
            options: providerUserEmailOptions,
            isRequired: true,
          }}
          width={8}
          multi={true}
          name="receiver_email_ids"
          onChange={handleChange}
          error={
            settings.receiver_email_ids.length === 0
              ? error.receiver_email_ids
              : SuccessState
          }
          placeholder={`Select User(s)`}
        />
      </div>

      <div className="collections-row">
        <Input
          field={{
            label: "Send Schedule",
            type: InputFieldType.PICKLIST,
            value: settings.weekly_schedule,
            options: DayOptions,
            isRequired: true,
          }}
          width={6}
          multi={true}
          name="weekly_schedule"
          onChange={handleChange}
          error={
            settings.weekly_schedule.length === 0
              ? error.weekly_schedule
              : SuccessState
          }
          placeholder={`Select Day(s)`}
        />
        <Input
          field={{
            label: "Order Tracking Contact Type",
            type: InputFieldType.PICKLIST,
            value: settings.contact_type_crm_id,
            options: contactTypes,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="contact_type_crm_id"
          onChange={handleChange}
          placeholder={`Select Contact Type`}
          loading={isFetchingContactTypes}
          error={
            !settings.contact_type_crm_id
              ? error.contact_type_crm_id
              : SuccessState
          }
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Subject",
            type: InputFieldType.TEXT,
            value: settings.email_subject,
            isRequired: true,
          }}
          width={8}
          name="email_subject"
          onChange={handleChange}
          error={
            !settings.email_subject.trim() ? error.email_subject : SuccessState
          }
          placeholder={`Enter subject for email`}
        />
      </div>
      <div className="collections-row">
        <label className="btn square-btn btn-default upload-btn-container">
          <img
            className="icon-upload"
            src="/assets/icons/photo-camera.svg"
            title="Upload image"
          />
          <input
            type="file"
            name=""
            accept="image/jpg, image/jpeg, image/png"
            className="custom-file-input"
            onChange={(e) => onImageUpload(e)}
            onClick={(event: any) => {
              event.target.value = null;
            }}
          />
        </label>

        <QuillEditorAcela
          onChange={handleChangeEmailBody}
          label={"Email Body Template"}
          value={settings.email_body_text}
          scrollingContainer=".app-body"
          wrapperClass={"collections-email-template"}
          isRequired={true}
          hideTable={true}
          customToolbar={[
            ["bold", "italic", "underline", "strike", "custom-image"], // toggled buttons
            [{ color: [] }, { background: [] }],
            [{ list: "ordered" }, { list: "bullet" }],
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
          ]}
          error={
            commonFunctions.isEditorEmpty(settings.email_body_text)
              ? error.email_body_text
              : SuccessState
          }
        />
      </div>
      <SquareButton
        onClick={onSubmit}
        content={firstSave ? "Save" : "Update"}
        className={"collections-save-btn"}
        bsStyle={ButtonStyle.PRIMARY}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
  providerUsers: state.providerUser.providerUsersAll,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchAllProviderUsers: () => dispatch(fetchAllProviderUsers()),
  weeklyTrackingCRU: (
    request: HTTPMethods,
    data?: IWeeklyOrderTrackingSettings
  ) => dispatch(weeklyTrackingCRU(request, data)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  sendWeeklyTrackingEmail: (crm_ids: number[], all_customers: boolean) =>
    dispatch(sendWeeklyTrackingEmail(crm_ids, all_customers)),
  getWeeklyTrackingEmailPreview: () =>
    dispatch(getWeeklyTrackingEmailPreview()),
  fetchOrderTrackingSettings: () => dispatch(fetchOrderTrackingSettings()),
  uploadImage: (file: any, name: string) => dispatch(uploadImage(file, name)),
  addInfoMessage: (str: string) => dispatch(addInfoMessage(str)),
  addErrorMessage: (str: string) => dispatch(addErrorMessage(str)),
  addSuccessMessage: (str: string) => dispatch(addSuccessMessage(str)),
  addWarningMessage: (str: string) => dispatch(addWarningMessage(str)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WeeklyOrderTracking);
