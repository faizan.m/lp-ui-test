import React from "react";
import { cloneDeep, debounce } from "lodash";
import SquareButton from "../../components/Button/button";
import DeleteButton from "../../components/Button/deleteButton";
import Input from "../../components/Input/input";

interface ICarrierTrackingUrlProps {
  UrlList: number[];
  onSubmit: (UrlList: any) => void;
  shipments: any;
  loadingShipments: boolean;
}

interface ICarrierTrackingUrlState {
  mappingList: any[];
  open: boolean;
}

export default class CarrierTrackingUrl extends React.Component<
  ICarrierTrackingUrlProps,
  ICarrierTrackingUrlState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  private debouncedFetch;

  constructor(props: ICarrierTrackingUrlProps) {
    super(props);

    this.state = {
      mappingList: [],
      open: true,
    };
    this.debouncedFetch = debounce(this.passObjectToParent, 1000);
  }

  componentDidUpdate(prevProps: ICarrierTrackingUrlProps) {
    if (this.props.UrlList && this.props.UrlList !== prevProps.UrlList) {
      const mapping = this.props.UrlList && Object.keys(this.props.UrlList);
      const mappingList = [];
      if (mapping) {
        mapping.map((data) => {
          mappingList.push({
            tracking_url: this.props.UrlList[data].tracking_url,
            connectwise_shipper: this.props.UrlList[data].connectwise_shipper,
            name: data,
          });
        });
      }
      this.setState({
        mappingList,
        open: true,
      });
    }
  }
  
  addNew = () => {
    const newState = cloneDeep(this.state);
    newState.mappingList.unshift({
      tracking_url: "",
      connectwise_shipper: "",
      name: "",
    });

    this.setState(newState);
  };

  onDeleteRowClick = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.mappingList.splice(index, 1);
    this.setState(newState);
    this.passObjectToParent(newState.mappingList);
  };

  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.mappingList[index][e.target.name] = e.target.value;
    this.setState(newState);
    this.debouncedFetch(newState.mappingList);
  };

  passObjectToParent = (list) => {
    const mappedObject = {};
    list.map((data, i) => {
      mappedObject[data.name] = data;
    });
    this.props.onSubmit(mappedObject);
  };
  isValid = () => {
    const error = {
      subscription_category_id: { ...CarrierTrackingUrl.emptyErrorState },
      device_category_row: { ...CarrierTrackingUrl.emptyErrorState },
    };
    let isValid = true;

    if (this.state.mappingList && this.state.mappingList.length > 0) {
      this.state.mappingList.map((e) => {
        if (!e.connectwise_shipper || !e.name) {
          error.device_category_row.errorState = IValidationState.ERROR;
          // tslint:disable-next-line:max-line-length
          error.device_category_row.errorMessage = `Please enter required fields.`;
          isValid = false;
        }
      });
    }

    return isValid;
  };

  renderCategoryManufaturerMapping = () => {
    const mappingList = this.state.mappingList;

    return (
      <div className="row">
        <div
          className={`col-md-12 ${
            this.state.open ? "field--not-collapsed" : ""
          }`}
        >
          <div
            className="heading  collapsable-heading"
            onClick={() =>
              this.setState({
                open: !this.state.open,
              })
            }
          >
            {" "}
            carrier tracking url Mapping
            <div className="action-collapse">
              {!this.state.open ? "+" : "-"}
            </div>
          </div>
        </div>
        {this.state.open && (
          <div className="col-md-12 mapping-row">
            <div className="field-section  col-md-4 col-xs-4" />
            <SquareButton
              content="+ Add"
              onClick={this.addNew}
              bsStyle={ButtonStyle.PRIMARY}
              className="add-new-mapping  col-md-2 col-xs-2"
            />
            <div className="field-section  col-md-1 col-xs-1" />
          </div>
        )}
        {this.state.open &&
          mappingList &&
          mappingList.map((row, index) => (
            <div className="col-md-12 mapping-row" key={index}>
              <Input
                field={{
                  label: "Name",
                  type: InputFieldType.TEXT,
                  value: row.name,
                  isRequired: true,
                }}
                width={3}
                multi={true}
                name="name"
                onChange={(e) => this.handleChangeList(e, index)}
                placeholder={`Enter Name`}
              />
              <Input
                field={{
                  label: "Tracking URL",
                  type: InputFieldType.TEXT,
                  value: row.tracking_url,
                  isRequired: false,
                }}
                width={5}
                labelIcon={'info'}
                labelTitle={<div className="tooltip-data-rules">
                <div>  To include tracking number use place holder  %tracknum%  </div>
                <div>   
                        Example: 
                        <br/> http://www.xyz.com/WebTracking?InquiryNumber=<b>%tracknum%</b>&track=0</div>
              </div>}
                multi={true}
                name="tracking_url"
                onChange={(e) => this.handleChangeList(e, index)}
                placeholder={`Enter URL`}
              />
              <Input
                field={{
                  label: "Connectwise Shipper",
                  type: InputFieldType.PICKLIST,
                  value: this.state.mappingList[index].connectwise_shipper,
                  isRequired: true,
                  options: this.props.shipments,
                }}
                width={3}
                loading={this.props.loadingShipments}
                multi={false}
                name="connectwise_shipper"
                onChange={(e) => this.handleChangeList(e, index)}
                placeholder={`Select connectwise_shipper`}
              />
              <DeleteButton onClick={(e) => this.onDeleteRowClick(e, index)} />
            </div>
          ))}
      </div>
    );
  };

  render() {
    return (
      <div className="carrier-tracking-main collapsable-section">
        {this.renderCategoryManufaturerMapping()}
      </div>
    );
  }
}
