import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { isPostalCode } from "validator";
import {
  saveVendor,
  saveVendorPrimaryContact,
  SAVE_VENDOR_SUCCESS,
  SAVE_VENDOR_CONTACT_SUCCESS,
  getVendorContactTypes,
  SAVE_VENDOR_FAILURE,
  SAVE_VENDOR_CONTACT_FAILURE,
} from "../../../actions/sow";
import {
  vendorSettingsCRU,
  GET_VENDOR_SETTINGS_SUCCESS,
  GET_VENDOR_SETTINGS_FAILURE,
} from "../../../actions/setting";
import { addErrorMessage } from "../../../actions/appState";
import { fetchCountriesAndStates } from "../../../actions/inventory";
import Input from "../../../components/Input/input";
import AppValidators from "../../../utils/validator";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";

interface IAddVendorProps {
  show: boolean;
  countries: any;
  isCountriesFetching: boolean;
  contactTypes: IPickListOptions[];
  onClose: () => void;
  onSubmit: () => void;
  addErrorMessage: TShowErrorMessage;
  fetchCountries: () => Promise<any>;
  fetchContactTypes: () => Promise<any>;
  fetchVendorSettings: () => Promise<any>;
  saveVendor: (data: IVendorDetails) => Promise<any>;
  saveVendorContact: (data: IVendorContact) => Promise<any>;
}

interface ErrorInteface {
  [errorKey: string]: IFieldValidation;
}

const EmptyVendorDetails: IVendorDetails = {
  customer_identifier: "",
  vendor_name: "",
  address_line_1: "",
  address_line_2: "",
  city: "",
  state: "",
  zip: "",
  website: "",
  customer_site_name: "",
  territory_crm_id: null,
};

const EmptyPrimaryContact: IVendorContact = {
  first_name: "",
  last_name: "",
  title: "",
  office_phone_number: "",
  email: "",
  contact_type_crm_id: null,
  customer_crm_id: null,
};

const getErrorField = (
  msg: string = "This field is required",
  error: boolean = true
): IFieldValidation => ({
  errorState: error ? IValidationState.ERROR : IValidationState.SUCCESS,
  errorMessage: msg,
});

const EmptyErrors: ErrorInteface = {
  customer_identifier: { ...getErrorField("", false) },
  customer_site_name: { ...getErrorField("", false) },
  name: { ...getErrorField("", false) },
  address_line_1: { ...getErrorField("", false) },
  city: { ...getErrorField("", false) },
  state: { ...getErrorField("", false) },
  zip: { ...getErrorField("", false) },
  website: { ...getErrorField("", false) },
  first_name: { ...getErrorField("", false) },
  last_name: { ...getErrorField("", false) },
  office_phone_number: { ...getErrorField("", false) },
  email: { ...getErrorField("", false) },
  contact_type_crm_id: { ...getErrorField("", false) },
  territory_crm_id: { ...getErrorField("", false) },
};

const AddVendor: React.FC<IAddVendorProps> = (props) => {
  const [saving, setSaving] = useState<boolean>(false);
  const [vendorSaved, setVendorSaved] = useState<boolean>(false);
  const [vendorDetails, setVendorDetails] = useState<IVendorDetails>({
    ...EmptyVendorDetails,
  });
  const [contactDetails, setContactDetails] = useState<IVendorContact>({
    ...EmptyPrimaryContact,
  });
  const [errors, setErrors] = useState<ErrorInteface>({ ...EmptyErrors });
  const [territoryOptions, setTerritoryOptions] = useState<IPickListOptions[]>(
    []
  );

  const states: IPickListOptions[] = useMemo(
    () =>
      props.countries
        ? props.countries
            .find((el) => el.country === "United States")
            .states.map((el) => ({ value: el.state, label: el.state }))
        : [],
    [props.countries]
  );

  useEffect(() => {
    props.fetchCountries();
    props.fetchContactTypes();
    fetchTerritoryOptions();
  }, []);

  const fetchTerritoryOptions = () => {
    props.fetchVendorSettings().then((action) => {
      if (action.type === GET_VENDOR_SETTINGS_SUCCESS) {
        let settings: IVendorSettings = action.response;
        let options: IPickListOptions[] = settings.territories.map((el) => ({
          value: el.territory_crm_id,
          label: el.territory_name,
        }));
        setTerritoryOptions(options);
      } else if (action.type === GET_VENDOR_SETTINGS_FAILURE) {
        props.addErrorMessage("Vendor Settings are not configured!");
        onClose();
      }
    });
  };

  const setEmptyState = () => {
    setContactDetails({ ...EmptyPrimaryContact });
    setVendorDetails({ ...EmptyVendorDetails });
    setErrors({ ...EmptyErrors });
    setVendorSaved(false);
  };

  const handleChangeVendor = (event: React.ChangeEvent<HTMLInputElement>) => {
    setVendorDetails((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleChangeContact = (event: React.ChangeEvent<HTMLInputElement>) => {
    setContactDetails((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const onClose = () => {
    setEmptyState();
    props.onClose();
  };

  const onSubmit = () => {
    if (vendorSaved) {
      if (!validateContactForm()) {
        setSaving(true);
        props
          .saveVendorContact(contactDetails)
          .then((action) => {
            if (action.type === SAVE_VENDOR_CONTACT_SUCCESS) {
              setEmptyState();
              props.onSubmit();
            } else if (action.type === SAVE_VENDOR_CONTACT_FAILURE) {
              Object.keys(action.errorList.data[0]).forEach((key) => {
                props.addErrorMessage(action.errorList.data[0][key]);
              });
            }
          })
          .finally(() => setSaving(false));
      }
    } else {
      if (!validateVendorForm()) {
        setSaving(true);
        props
          .saveVendor(vendorDetails)
          .then((action) => {
            if (action.type === SAVE_VENDOR_SUCCESS) {
              let response = action.response;
              setVendorDetails({ ...EmptyVendorDetails });
              setContactDetails((prevState) => ({
                ...prevState,
                customer_crm_id: response.id,
              }));
              setVendorSaved(true);
            } else if (action.type === SAVE_VENDOR_FAILURE) {
              Object.keys(action.errorList.data[0]).forEach((key) => {
                const errorMessage: string = action.errorList.data[0][key];
                if (key === "identifier") {
                  setErrors((prevState) => ({
                    ...prevState,
                    customer_identifier: getErrorField(errorMessage),
                  }));
                } else props.addErrorMessage(errorMessage);
              });
            }
          })
          .finally(() => setSaving(false));
      }
    }
  };

  const getTitle = () => {
    return vendorSaved ? "Add Primary Contact" : "Create Vendor";
  };

  const validateVendorForm = (): boolean => {
    let errorFound = false;
    const newErrors: ErrorInteface = { ...errors };
    const vendorFields: string[] = [
      "vendor_name",
      "customer_identifier",
      "customer_site_name",
      "territory_crm_id",
      "address_line_1",
      "city",
      "state",
      "zip",
    ];
    vendorFields.forEach((field) => {
      if (!vendorDetails[field]) {
        errorFound = true;
        newErrors[field] = getErrorField();
      } else {
        newErrors[field] = getErrorField("", false);
      }
    });
    if (vendorDetails.customer_identifier.length > 15) {
      errorFound = true;
      newErrors.customer_identifier = getErrorField(
        "Maximum 15 characters allowed"
      );
    }
    if (!isPostalCode(vendorDetails.zip, "US")) {
      errorFound = true;
      newErrors.zip = getErrorField("Please enter a valid Zip Code.");
    }
    if (
      vendorDetails.website &&
      !AppValidators.isValidUrl(vendorDetails.website)
    ) {
      errorFound = true;
      newErrors.website = getErrorField("Enter a valid website name!");
    } else newErrors.website = getErrorField("", false);
    setErrors(newErrors);
    return errorFound;
  };

  const validateContactForm = (): boolean => {
    let errorFound = false;
    const newErrors: ErrorInteface = { ...errors };
    const contactFields: string[] = [
      "email",
      "last_name",
      "first_name",
      "office_phone_number",
      "contact_type_crm_id",
    ];
    contactFields.forEach((field) => {
      if (!contactDetails[field]) {
        errorFound = true;
        newErrors[field] = getErrorField();
      } else {
        newErrors[field] = getErrorField("", false);
      }
    });
    if (!AppValidators.isValidEmail(contactDetails.email)) {
      errorFound = true;
      newErrors.email = getErrorField("Enter a valid email!");
    }
    if (!AppValidators.isPhoneNumber(contactDetails.office_phone_number)) {
      errorFound = true;
      newErrors.office_phone_number = getErrorField(
        "Enter a valid phone number"
      );
    }
    setErrors(newErrors);
    return errorFound;
  };

  const getVendorFormBody = () => {
    return (
      <div className="vendor-form-body">
        <Spinner show={saving} className="vendor-modal-spinner" />
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Vendor Name",
              type: InputFieldType.TEXT,
              value: vendorDetails.vendor_name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Vendor Name"
            name="vendor_name"
            onChange={handleChangeVendor}
            error={errors.vendor_name}
          />
          <Input
            field={{
              label: "Vendor ID",
              type: InputFieldType.TEXT,
              value: vendorDetails.customer_identifier,
              isRequired: true,
            }}
            width={6}
            labelIcon="info"
            labelTitle="This is a unique ID required for vendor"
            placeholder="Enter Vendor ID"
            name="customer_identifier"
            onChange={handleChangeVendor}
            error={errors.customer_identifier}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Site Name",
              type: InputFieldType.TEXT,
              value: vendorDetails.customer_site_name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Site Name"
            labelIcon="info"
            labelTitle="Site Name is required for creating a vendor"
            name="customer_site_name"
            onChange={handleChangeVendor}
            error={errors.customer_site_name}
          />
          <Input
            field={{
              label: "Website",
              type: InputFieldType.TEXT,
              value: vendorDetails.website,
              isRequired: false,
            }}
            width={6}
            placeholder="Enter Website"
            name="website"
            onChange={handleChangeVendor}
            error={errors.website}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Vendor Territory",
              type: InputFieldType.PICKLIST,
              value: vendorDetails.territory_crm_id,
              options: territoryOptions,
              isRequired: true,
            }}
            width={6}
            name="territory_crm_id"
            labelIcon="info"
            labelTitle="This is the territory associated with the vendors. The territories list can be configured in Vendor Settings."
            loading={territoryOptions.length === 0}
            placeholder="Select Territory"
            onChange={handleChangeVendor}
            error={errors.territory_crm_id}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Address Line 1",
              type: InputFieldType.TEXT,
              value: vendorDetails.address_line_1,
              isRequired: true,
            }}
            width={8}
            placeholder="Enter Address Line 1"
            name="address_line_1"
            onChange={handleChangeVendor}
            error={errors.address_line_1}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Address Line 2",
              type: InputFieldType.TEXT,
              value: vendorDetails.address_line_2,
              isRequired: false,
            }}
            width={8}
            placeholder="Enter Address Line 2"
            name="address_line_2"
            onChange={handleChangeVendor}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "State",
              type: InputFieldType.PICKLIST,
              options: states,
              value: vendorDetails.state,
              isRequired: true,
            }}
            width={4}
            placeholder="Select State"
            name="state"
            onChange={handleChangeVendor}
            loading={props.isCountriesFetching}
            error={errors.state}
          />
          <Input
            field={{
              label: "City",
              type: InputFieldType.TEXT,
              value: vendorDetails.city,
              isRequired: true,
            }}
            width={4}
            placeholder="Enter City"
            name="city"
            onChange={handleChangeVendor}
            error={errors.city}
          />
          <Input
            field={{
              label: "Zip",
              type: InputFieldType.TEXT,
              value: vendorDetails.zip,
              isRequired: true,
            }}
            width={4}
            placeholder="Enter Zip Code"
            name="zip"
            onChange={handleChangeVendor}
            error={errors.zip}
          />
        </div>
      </div>
    );
  };

  const getPrimaryContactFormBody = () => {
    return (
      <div className="vendor-form-body">
        <Spinner show={saving} className="vendor-modal-spinner" />
        <div className="col-md-12 row">
          <Input
            field={{
              label: "First Name",
              type: InputFieldType.TEXT,
              value: contactDetails.first_name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter First Name"
            name="first_name"
            onChange={handleChangeContact}
            error={errors.first_name}
          />
          <Input
            field={{
              label: "Last Name",
              type: InputFieldType.TEXT,
              value: contactDetails.last_name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Last Name"
            name="last_name"
            onChange={handleChangeContact}
            error={errors.last_name}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Title",
              type: InputFieldType.TEXT,
              value: contactDetails.title,
              isRequired: false,
            }}
            width={6}
            placeholder="Enter Title"
            name="title"
            onChange={handleChangeContact}
            error={errors.title}
          />
          <Input
            field={{
              label: "Phone",
              type: InputFieldType.TEXT,
              value: contactDetails.office_phone_number,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Phone No."
            name="office_phone_number"
            onChange={handleChangeContact}
            error={errors.office_phone_number}
          />
        </div>
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Email",
              type: InputFieldType.TEXT,
              value: contactDetails.email,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Email"
            name="email"
            onChange={handleChangeContact}
            error={errors.email}
          />
          <Input
            field={{
              label: "Contact Type",
              type: InputFieldType.PICKLIST,
              value: contactDetails.contact_type_crm_id,
              options: props.contactTypes,
              isRequired: true,
            }}
            width={6}
            labelIcon="info"
            labelTitle="The Primary Contact created will have the following type."
            loading={props.contactTypes.length === 0}
            placeholder="Select Contact Type"
            name="contact_type_crm_id"
            onChange={handleChangeContact}
            error={errors.contact_type_crm_id}
          />
        </div>
      </div>
    );
  };

  const getFooter = () => {
    return (
      <div>
        {!vendorSaved && (
          <SquareButton
            onClick={onClose}
            content="Cancel"
            bsStyle={ButtonStyle.DEFAULT}
            disabled={saving}
          />
        )}
        <SquareButton
          onClick={onSubmit}
          content={vendorSaved ? "Save Primary Contact" : "Save Vendor"}
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  return (
    <ModalBase
      show={props.show}
      onClose={onClose}
      titleElement={getTitle()}
      bodyElement={
        vendorSaved ? getPrimaryContactFormBody() : getVendorFormBody()
      }
      footerElement={getFooter()}
      className="add-vendor-modal"
      hideCloseButton={saving || vendorSaved}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  countries: state.inventory.countries,
  contactTypes: state.sow.contactTypes,
  isCountriesFetching: state.inventory.isCountriesFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchCountries: () => dispatch(fetchCountriesAndStates()),
  fetchContactTypes: () => dispatch(getVendorContactTypes()),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  saveVendor: (data: IVendorDetails) => dispatch(saveVendor(data)),
  saveVendorContact: (data: IVendorContact) =>
    dispatch(saveVendorPrimaryContact(data)),
  fetchVendorSettings: (request = "get" as "get" | "put" | "post") =>
    dispatch(vendorSettingsCRU(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddVendor);
