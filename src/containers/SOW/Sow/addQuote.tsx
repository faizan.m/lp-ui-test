import { cloneDeep } from 'lodash';
import React from 'react';
import SquareButton from '../../../components/Button/button';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import Spinner from '../../../components/Spinner';
import { commonFunctions } from '../../../utils/commonFunctions';

interface IAddQuoteProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newDevice: any) => void;
  stages: any[];
  types: any[];
  isLoading: boolean;
  errorList: any;
  quote: any;
}

interface IAddQuoteState {
  types: any[];
  quote: any;
  error: {
    stage_id: IFieldValidation;
    type_id: IFieldValidation;
    name: IFieldValidation;
    customer_id: IFieldValidation;
  };
}

export default class AddQuote extends React.Component<
  IAddQuoteProps,
  IAddQuoteState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };

  constructor(props: IAddQuoteProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    quote: {
      stage_id: null,
      type_id: null,
      name: null,
    },
    error: {
      stage_id: { ...AddQuote.emptyErrorState },
      type_id: { ...AddQuote.emptyErrorState },
      name: { ...AddQuote.emptyErrorState },
      customer_id: { ...AddQuote.emptyErrorState },
    },
    types: [],
  });

  // Check if getDerivedStateFromProps can be used (in future)
  componentDidUpdate(prevProps: IAddQuoteProps) {
    if (this.props.errorList !== prevProps.errorList) {
      this.setValidationErrors(this.props.errorList);
    }
    if (this.props.quote !== prevProps.quote && this.props.quote) {
      this.setState({ quote: this.props.quote });
    }
  }

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const newState = cloneDeep(this.state);
    (newState.quote[targetName] as any) = targetValue;
    this.setState(newState);
  };

  onClose = e => {
    this.props.onClose(e);
    const newState = this.getEmptyState();
    this.setState(newState);
  };

  setValidationErrors = errorList => {
    const newState: IAddQuoteState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };
  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.quote.name || this.state.quote.name.trim().length === 0) {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage = 'Enter a valid opportunity name';

      isValid = false;
    } else if (this.state.quote.name.length > 300) {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage =
        'Opportunity name should be less than 300 chars.';

      isValid = false;
    }

    if (this.state.quote && !this.state.quote.stage_id) {
      error.stage_id.errorState = IValidationState.ERROR;
      error.stage_id.errorMessage = 'Please select stage';

      isValid = false;
    }
    if (this.state.quote && !this.state.quote.type_id) {
      error.type_id.errorState = IValidationState.ERROR;
      error.type_id.errorMessage = 'Please select type';

      isValid = false;
    }

    if (this.state.quote && this.state.quote.stage_id < 0) {
      error.stage_id.errorState = IValidationState.ERROR;
      error.stage_id.errorMessage = 'Please select stage';

      isValid = false;
    }
    if (this.state.quote && this.state.quote.type_id < 0) {
      error.type_id.errorState = IValidationState.ERROR;
      error.type_id.errorMessage = 'Please select type';

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = e => {
    if (this.isValid()) {
      this.props.onSubmit(this.state.quote);
      const newState = this.getEmptyState();
      this.setState(newState);
    }
  };

  getTitle = () => {
    return 'Add New Opportunity';
  };

  getBody = () => {
    const types = this.props.types
      ? this.props.types.map(site => ({
          value: site.id,
          label: site.label,
        }))
      : [];

    return (
      <div className="add-quote">
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`add-quote__body ${this.props.isLoading ? `loading` : ''}`}
        >
          <Input
            field={{
              label: 'Name',
              type: InputFieldType.TEXT,
              value: this.state.quote.name,
              isRequired: true,
            }}
            width={12}
            placeholder="Enter name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />

          <div
            className="select-manufacturer
           field-section  col-md-12 col-xs-12"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.type_id.errorMessage ? `error-input` : ''
              }`}
            >
              <SelectInput
                name="type_id"
                value={this.state.quote.type_id}
                onChange={this.handleChange}
                options={types}
                searchable={true}
                placeholder="Select type"
                clearable={false}
              />
            </div>
            {this.state.error.type_id.errorMessage && (
              <div className="field-error">
                {this.state.error.type_id.errorMessage}
              </div>
            )}
          </div>
          <div
            className="
          select-manufacturer field-section  col-md-12 col-xs-12"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Stage
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.stage_id.errorMessage ? `error-input` : ''
              }`}
            >
              <SelectInput
                name="stage_id"
                value={this.state.quote.stage_id}
                onChange={this.handleChange}
                options={this.props.stages}
                searchable={true}
                placeholder="Select stage"
                clearable={false}
              />
            </div>
            {this.state.error.stage_id.errorMessage && (
              <div className="field-error">
                {this.state.error.stage_id.errorMessage}
              </div>
            )}
          </div>
          <div>
            {this.state.error.customer_id.errorMessage && (
              <div className="field-error">
                {this.state.error.customer_id.errorMessage}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-quote__footer
      ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Add"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-quote"
      />
    );
  }
}
