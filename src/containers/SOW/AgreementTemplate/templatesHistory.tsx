import React from "react";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import InfiniteListing from "../../../components/InfiniteList/infiniteList";

interface TemplatesHistoryProps {
  show: boolean;
  onClose: (e: any) => void;
  history: any;
}

interface TemplatesHistoryState {}

export default class TemplatesHistory extends React.Component<
  TemplatesHistoryProps,
  TemplatesHistoryState
> {
  constructor(props: TemplatesHistoryProps) {
    super(props);
    this.state = {};
  }

  getTitle = () => {
    return (
      <div className="text-center">Agreement Templates Overall History</div>
    );
  };

  getBody = () => {
    const getTimestamp = (timestamp: string) => {
      const date = new Date(timestamp);
      return (
        date.getDate() +
        "/" +
        date.toLocaleString("default", { month: "short" }) +
        "/" +
        date
          .getFullYear()
          .toString()
          .substring(2) +
        " " +
        date
          .toLocaleString("default", {
            hour: "numeric",
            minute: "numeric",
            hour12: true,
          })
          .toUpperCase()
      );
    };

    return (
      <InfiniteListing
        id="templatesOverallHistory"
        url={"providers/sales/agreement-templates/history"}
        filterFunction={(element) =>
          element.changes_str !== "is_current_version: True → False"
        }
        columns={[
          {
            name: "Date",
            ordering: "timestamp",
            id: "timestamp",
            Cell: (cell) => getTimestamp(cell.timestamp),
          },
          {
            name: "Author",
            id: "actor",
          },
          {
            name: "Action",
            id: "action",
          },
          {
            name: "Version",
            id: "version",
            Cell: (cell) => cell.additional_data.version,
          },
          {
            name: "Template Name",
            id: "object_repr",
          },
        ]}
        className="templates-overall-history-component"
        showSearch={true}
        height={"100%"}
      />
    );
  };

  getFooter = () => {
    return <div className={`footer`}></div>;
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="version-history"
      />
    );
  }
}
