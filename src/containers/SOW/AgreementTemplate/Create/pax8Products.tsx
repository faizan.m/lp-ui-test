import React, { useState } from "react";
import { isNil } from "lodash";
import { connect } from "react-redux";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  fetchPAX8ProductDetails,
  FETCH_PAX8_PRODUCT_SUCCESS,
} from "../../../../actions/agreement";
import Input from "../../../../components/Input/input";
import IconButton from "../../../../components/Button/iconButton";
import ModalBase from "../../../../components/ModalBase/modalBase";
import InfiniteList from "../../../../components/InfiniteList/infiniteList";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../../components/Spinner";
import Checkbox from "../../../../components/Checkbox/checkbox";

interface IPAX8ProductProps {
  pax8Name: string;
  disabled: boolean;
  products: IPAX8Product[];
  noNameError: IFieldValidation;
  noProductsError: IFieldValidation;
  handleChangeName: (name: string) => void;
  fetchPAX8ProductDetails: (id: string) => Promise<any>;
  handleChangeProducts: (products: IPAX8Product[]) => void;
  addToQuotedProducts: (product: IPAX8ProductDetail) => void;
}

const PAX8Products: React.FC<IPAX8ProductProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [viewMode, setViewMode] = useState<boolean>(false);
  const [showAllPricing, setShowAllPricing] = useState<boolean>(true);
  const [currentProduct, setCurrentProduct] = useState<IPAX8ProductDetail>();

  const formatCurrency = (
    value: number,
    noDecimal: boolean = false
  ): string => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: noDecimal ? 0 : 2,
    });
    return formatter.format(value);
  };

  const getContractLength = (
    commitment_term: string,
    commitment_term_in_months: number
  ): string => {
    let textInMonths = !isNil(commitment_term_in_months)
      ? commitment_term_in_months +
        " month" +
        (commitment_term_in_months > 1 ? "s" : "")
      : "";
    return commitment_term || commitment_term_in_months
      ? commitment_term
        ? commitment_term + " (" + textInMonths + ")"
        : textInMonths
      : "N/A";
  };

  const closeModal = () => {
    setViewMode(false);
    setShowAllPricing(false);
    setCurrentProduct(undefined);
  };

  const removeProduct = (idx: number) => {
    props.handleChangeProducts(
      props.products.filter((el, index) => idx !== index)
    );
  };

  const onClickView = (product: IPAX8Product) => {
    setLoading(true);
    props
      .fetchPAX8ProductDetails(product.product_crm_id)
      .then((action) => {
        if (action.type === FETCH_PAX8_PRODUCT_SUCCESS) {
          const productInfo: IPAX8ProductDetail = action.response;
          setCurrentProduct(productInfo);
          setViewMode(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const renderPAX8ProductContainer = () => {
    return (
      <section
        className={
          "pax8-products-container" +
          (props.disabled ? " pax8-container-disabled" : "")
        }
      >
        <div className="pax8-catalog-section">
          <h3>PAX8 Products Catalog</h3>
          <InfiniteList
            showSearch={false}
            showIncludeExclude={true}
            height="auto"
            id="pax8-products-catalog"
            className="pax8-infinite-list"
            url={`providers/sales/agreement/pax8/products`}
            columns={
              [
                {
                  name: "",
                  className: "width-10 align-center",
                  Cell: (pax8_product: IPAX8ProductDetail) => (
                    <IconButton
                      onClick={() => props.addToQuotedProducts(pax8_product)}
                      icon="plus.svg"
                      title="Add to Quoted Products"
                      className="add-to-quoted-list"
                      newIcon={true}
                    />
                  ),
                },
                {
                  id: "sku",
                  name: "Product SKU",
                  className: "pax8-sku-col",
                },
                {
                  id: "product_name",
                  name: "Name",
                  className: "pax8-name-col",
                },
                {
                  id: "short_description",
                  name: "Description",
                  className: "pax8-desc-col text-ellipsis",
                },
                {
                  id: "vendor_name",
                  name: "Vendor",
                  className: "pax8-vendor-col",
                },
              ] as IColumnInfinite<IPAX8ProductDetail>[]
            }
          />
        </div>
        {props.products.length !== 0 ? (
          <div className="pax8-quoted-products-section">
            <Spinner show={loading} className="pax8-agr-temp-spinner" />
            <h3>PAX8 Quoted Products</h3>
            <Input
              field={{
                value: props.pax8Name,
                label: "",
                type: InputFieldType.TEXT,
              }}
              width={4}
              name="pax8Name"
              onChange={(e) => props.handleChangeName(e.target.value)}
              placeholder="Enter Section Name"
              className="pax8-name"
              disabled={props.disabled}
              error={!props.pax8Name.trim() ? props.noNameError : undefined}
            />

            <div className="quoted-products-list infinite-list-component">
              <div className="header">
                <div className="cell width-10 pax8-btns" />
                <div className="cell pax8-sku-col">Product SKU</div>
                <div className="cell pax8-name-col">Name</div>
                <div className="cell pax8-desc-col">Description</div>
                <div className="cell pax8-vendor-col">Vendor</div>
              </div>
              <div className="quoted-products-rows">
                {props.products.map((product, idx) => (
                  <div className="row-panel" key={idx}>
                    <div className="cell width-10 pax8-btns">
                      <IconButton
                        title="View Product"
                        onClick={() => onClickView(product)}
                        className="view-btn"
                        newIcon={true}
                        icon={"eye-fill.svg"}
                      />
                      <SmallConfirmationBox
                        className="remove"
                        showButton={true}
                        onClickOk={() => removeProduct(idx)}
                        text={"Product"}
                        title="Remove PAX8 Product"
                      />
                      <CopyToClipboard
                        text={product.sku + "\n" + product.short_description}
                        onCopy={() => {}}
                      >
                        <IconButton
                          icon="copy.svg"
                          onClick={() => null}
                          newIcon={true}
                          title="Copy line item"
                          className="product-copy"
                        />
                      </CopyToClipboard>
                    </div>
                    <div
                      className="cell pax8-sku-col text-ellipsis"
                      title={product.sku}
                    >
                      {product.sku}
                    </div>
                    <div
                      className="cell pax8-name-col text-ellipsis"
                      title={product.product_name}
                    >
                      {product.product_name}
                    </div>
                    <div
                      className="cell pax8-desc-col text-ellipsis"
                      title={product.short_description}
                    >
                      {product.short_description}
                    </div>
                    <div
                      className="cell pax8-vendor-col text-ellipsis"
                      title={product.vendor_name}
                    >
                      {product.vendor_name}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          props.noProductsError.errorState === IValidationState.ERROR && (
            <div id="pax8-products-error">
              {props.noProductsError.errorMessage}
            </div>
          )
        )}
      </section>
    );
  };

  const renderViewProductModal = () => {
    const renderLabelWithDetails = (
      width: number,
      label: string,
      detail: string
    ) => {
      return (
        <div className={`field-section col-xs-${width}`}>
          <div className="field__label row">
            <div className="field__label-label">{label}</div>
          </div>
          <div className="detail">{detail}</div>
        </div>
      );
    };

    const allEmptyContracts: boolean =
      currentProduct.pricing_data.filter(
        (el) =>
          el.commitment_term === null && el.commitment_term_in_months === null
      ).length === currentProduct.pricing_data.length;

    return (
      <ModalBase
        show={viewMode}
        onClose={closeModal}
        titleElement={"Preview " + currentProduct.sku}
        bodyElement={
          <div className="pax8-product-edit-container">
            <div className="pax8-modal-row">
              {renderLabelWithDetails(6, "Name", currentProduct.product_name)}
              {renderLabelWithDetails(6, "Vendor", currentProduct.vendor_name)}
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(
                12,
                "Description",
                currentProduct.short_description
              )}
            </div>
            <div className="pax8-product-pricing">
              <h4>
                Pricing Information
                {!allEmptyContracts && (
                  <Checkbox
                    isChecked={showAllPricing}
                    name="showAllPricing"
                    onChange={() => {
                      setShowAllPricing((prev) => !prev);
                    }}
                    className="pax8-all-price"
                  >
                    View pricing data with no contracts
                  </Checkbox>
                )}
              </h4>
              <div className="pax8-pricing-table">
                <div className="pax8-pricing-header">
                  <div className="right-headers">
                    <div className="col-headers">Quantity Range</div>
                    <div className="col-headers">Partner Buy Rate</div>
                    <div className="col-headers">Suggested Price</div>
                    <div className="col-headers">Charge Type</div>
                  </div>
                </div>
                {currentProduct.pricing_data.map(
                  (pricing: IPricingData, idx: number) => (
                    <div
                      style={{
                        display:
                          allEmptyContracts ||
                          showAllPricing ||
                          pricing.commitment_term !== null ||
                          pricing.commitment_term_in_months !== null
                            ? "flex"
                            : "none",
                      }}
                      className="pax8-pricing-row"
                      key={idx}
                    >
                      <div className="pricing-left-col">
                        <div className="left-col-details">
                          <label>Billing Term:</label>
                          <span>{pricing.billing_term}</span>
                        </div>
                        <div className="left-col-details">
                          <label>Contract Length:</label>
                          <span>
                            {getContractLength(
                              pricing.commitment_term,
                              pricing.commitment_term_in_months
                            )}
                          </span>
                        </div>
                        <div className="left-col-details">
                          <label>Pricing Type:</label>
                          <span>
                            {pricing.pricing_type
                              ? pricing.pricing_type
                              : "N/A"}
                          </span>
                        </div>
                        <div className="left-col-details">
                          <label>Unit of Measurement:</label>
                          <span>
                            {pricing.unit_of_measurement
                              ? pricing.unit_of_measurement
                              : "N/A"}
                          </span>
                        </div>
                      </div>
                      <div className="pricing-right-col">
                        {pricing.pricing_rates
                          .sort(
                            (a, b) =>
                              a.start_quantity_range - b.start_quantity_range
                          )
                          .map((rates: IPricingRates, idx: number) => (
                            <div className="rates-row" key={idx}>
                              <div className="rates-row-info">
                                {rates.start_quantity_range +
                                  (rates.end_quantity_range !== null
                                    ? " - " + rates.end_quantity_range
                                    : " & more")}
                              </div>
                              <div className="rates-row-info">
                                {formatCurrency(rates.partner_buy_rate)}
                              </div>
                              <div className="rates-row-info">
                                {formatCurrency(rates.suggested_retail_price)}
                              </div>
                              <div className="rates-row-info">
                                {rates.charge_type ? rates.charge_type : "N/A"}
                              </div>
                            </div>
                          ))}
                      </div>
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
        }
        footerElement={null}
        className={`pax8-product-edit-modal`}
      />
    );
  };

  return (
    <>
      {renderPAX8ProductContainer()}
      {viewMode && renderViewProductModal()}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPAX8ProductDetails: (id: string) =>
    dispatch(fetchPAX8ProductDetails(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PAX8Products);
