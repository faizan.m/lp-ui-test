import React, { useEffect, useState } from "react";
import { cloneDeep, every } from "lodash";
import { Row } from "react-bootstrap";
import { DraggableArea } from "react-draggable-tags";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import { getCustomerCost } from "../../../../utils/agreementCalculations";
import Checkbox from "../../../../components/Checkbox/checkbox";

const initialSingleProductDetails: IProductBundle = {
  is_bundle: false,
  billing_option: AgreementBillingTerm.MONTHLY,
  product_type: AgreementProductType.SERVICE,
  is_checked: true,
  selected_product: 0,
  is_required: true,
  comment: null,
  products: [
    {
      name: "",
      description: "",
      internal_cost: 0,
      customer_cost: 0,
      margin: 0,
      quantity: 1,
      error: {
        errorState: IValidationState.SUCCESS,
        errorMessage: "",
      },
    },
  ],
};

interface IProductsProps {
  products: IProductBundle[];
  setProductsFn: any;
  disabled?: boolean;
}

const Products: React.FC<IProductsProps> = ({
  products,
  setProductsFn,
  disabled = false,
}) => {
  const [editable, setEditable] = useState<boolean>(false);
  const [collapsed, setCollapsed] = useState<boolean[]>(
    Array(products.length).fill(true)
  );

  useEffect(() => setEditable(!disabled), [disabled]);

  const toggleCollapsedState = (e: any, productBundleIdx: number) => {
    e.stopPropagation();
    const newCollapsed = [...collapsed];
    newCollapsed[productBundleIdx] = !collapsed[productBundleIdx];
    setCollapsed(newCollapsed);
    setEditable(!disabled && !every(newCollapsed));
  };

  const handleDraggableChange = (tags: IProductBundle[]) => {
    setCollapsed(Array(products.length).fill(true));
    setProductsFn(tags);
  };

  const handleAddSingleProduct = (idx: number) => {
    const newProducts = cloneDeep(products);
    newProducts[idx].products.push({
      name: "",
      description: "",
      internal_cost: 0,
      customer_cost: 0,
      margin: 0,
      quantity: 1,
    });
    setProductsFn(newProducts);
  };

  const handleAddProductBundle = () => {
    const newProducts = cloneDeep(products);
    newProducts.push({
      ...initialSingleProductDetails,
      id: Math.round(Math.random() * 100000000),
    });
    setCollapsed(Array(newProducts.length).fill(true));
    setProductsFn(newProducts);
  };

  const handleRemoveProductBundle = (idx: number) => {
    const newProducts = cloneDeep(products);
    newProducts.splice(idx, 1);
    setCollapsed(Array(newProducts.length).fill(true));
    setProductsFn(newProducts);
  };

  const handleRemoveSingleProduct = (
    productBundleIdx: number,
    singleProductIdx: number
  ) => {
    const newProducts = cloneDeep(products);
    newProducts[productBundleIdx].products.splice(singleProductIdx, 1);
    setProductsFn(newProducts);
  };

  const handleProductDetailsChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    productBundleIdx: number,
    singleProductIdx: number
  ) => {
    const newProducts = cloneDeep(products);
    const product_attribute = e.target.name;
    if (product_attribute.startsWith("billing_option"))
      newProducts[productBundleIdx].billing_option = e.target
        .value as AgreementBillingTerm;
    else if (product_attribute.startsWith("product_type"))
      newProducts[productBundleIdx].product_type = e.target
        .value as AgreementProductType;
    else {
      newProducts[productBundleIdx].products[singleProductIdx][
        product_attribute
      ] =
        product_attribute === "name" || product_attribute === "description"
          ? e.target.value
          : Number(e.target.value);
    }
    setProductsFn(newProducts);
  };

  const handleChangeCheckBox = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx: number
  ) => {
    const newProducts = cloneDeep(products);
    if (e.target.name === "product-selected") {
      newProducts[idx].selected_product = e.target.checked ? 0 : null;
      newProducts[idx].is_checked = e.target.checked;
    } else {
      newProducts[idx].is_required = e.target.checked;
    }
    setProductsFn(newProducts);
  };

  const renderProductBundle = (
    productBundle: IProductBundle,
    externalIdx: number
  ) => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    return (
      <div
        key={externalIdx}
        className={
          "product-collapsible-block" +
          (collapsed[externalIdx] ? " collapsed" : " not-collapsed")
        }
      >
        <h4
          onClick={(e) => toggleCollapsedState(e, externalIdx)}
          className={
            "product-bundle-title" +
            (productBundle.hasError ? " agr-error-border" : "") +
            (editable ? " agr-cursor" : "")
          }
        >
          {productBundle.products[0].name}
          <div className="collapse-icon">
            {collapsed[externalIdx] ? "+" : "-"}
          </div>
        </h4>
        <div
          className={
            "product-bundle-main" +
            (collapsed[externalIdx] ? " product-display-none" : "")
          }
        >
          {productBundle &&
            productBundle.products &&
            productBundle.products.map(
              (singleProduct: IProduct, internalIdx: number) => (
                <div
                  className="single-product-details col-md-9 col-xs-12"
                  key={internalIdx}
                >
                  <Row className="single-product-row-1">
                    <Input
                      field={{
                        label: "Name",
                        type: InputFieldType.TEXT,
                        value: `${singleProduct.name}`,
                        isRequired: true,
                      }}
                      width={4}
                      name="name"
                      placeholder="Enter Name of Product"
                      onChange={(e) =>
                        handleProductDetailsChange(e, externalIdx, internalIdx)
                      }
                      disabled={!editable}
                      showErrorOnDisabled={true}
                      className="product-name"
                      error={
                        !singleProduct.name.trim()
                          ? singleProduct.error
                          : undefined
                      }
                    />
                    <Input
                      field={{
                        label: "Internal Cost(In $)",
                        type: InputFieldType.NUMBER,
                        value: `${singleProduct.internal_cost}`,
                      }}
                      width={3}
                      name="internal_cost"
                      placeholder={"Enter Internal Cost"}
                      disabled={!editable}
                      className="product-internal-cost"
                      onChange={(e) =>
                        handleProductDetailsChange(e, externalIdx, internalIdx)
                      }
                      error={
                        singleProduct.internal_cost === 0
                          ? {
                              ...singleProduct.error,
                              errorMessage: "Positive no. required",
                            }
                          : undefined
                      }
                    />
                    <Input
                      field={{
                        label: "Margin %",
                        type: InputFieldType.NUMBER,
                        value: `${singleProduct.margin}`,
                      }}
                      width={2}
                      name="margin"
                      placeholder="Enter Margin in %"
                      defaultValue={0}
                      minimumValue="0"
                      maximumValue="100"
                      disabled={!editable}
                      className="product-margin"
                      onChange={(e) =>
                        handleProductDetailsChange(e, externalIdx, internalIdx)
                      }
                    />
                    <Input
                      field={{
                        label: "Customer Cost($)",
                        type: InputFieldType.TEXT,
                        value: `${formatter.format(
                          Number(getCustomerCost(singleProduct))
                        )}`,
                      }}
                      width={2}
                      className="product-customer-cost"
                      labelTitle="This is the one time or monthly cost associated with this product."
                      labelIcon="info"
                      name="customer_cost"
                      defaultValue={"0"}
                      disabled={true}
                      onChange={(e) =>
                        handleProductDetailsChange(e, externalIdx, internalIdx)
                      }
                    />
                    {internalIdx !== 0 && editable && (
                      <SmallConfirmationBox
                        className="delete-product-confirmation"
                        onClickOk={() =>
                          handleRemoveSingleProduct(externalIdx, internalIdx)
                        }
                        text={"Product"}
                      />
                    )}
                  </Row>
                  <Row className="single-product-row-2">
                    <div className="custom-label">
                      Description
                      <span>*</span>
                    </div>
                    <Input
                      field={{
                        label: null,
                        type: InputFieldType.TEXT,
                        value: `${singleProduct.description}`,
                      }}
                      width={10}
                      name="description"
                      placeholder="Enter Product Description"
                      onChange={(e) =>
                        handleProductDetailsChange(e, externalIdx, internalIdx)
                      }
                      disabled={!editable}
                      error={
                        !singleProduct.description.trim()
                          ? singleProduct.error
                          : undefined
                      }
                      showErrorOnDisabled={true}
                    />
                  </Row>
                </div>
              )
            )}
          <div className="product-bundle-options">
            <div className="product-billing-options col-md-6 col-xs-6">
              <div className="custom-label">Billing options</div>
              <Input
                field={{
                  options: [
                    AgreementBillingTerm.MONTHLY,
                    AgreementBillingTerm.ANNUALLY,
                    AgreementBillingTerm.ONE_TIME,
                  ],
                  label: null,
                  type: InputFieldType.RADIO,
                  value: `${productBundle.billing_option}`,
                }}
                name={`billing_option_${externalIdx}`}
                width={12}
                onChange={(e) => handleProductDetailsChange(e, externalIdx, -1)}
                disabled={!editable}
              />
            </div>
            <div className="product-type-options col-md-6 col-xs-6">
              <div className="custom-label">Select One</div>
              <Input
                field={{
                  options: [
                    AgreementProductType.SERVICE,
                    AgreementProductType.HARDWARE,
                  ],
                  label: null,
                  type: InputFieldType.RADIO,
                  value: `${productBundle.product_type}`,
                }}
                name={`product_type_${externalIdx}`}
                width={12}
                onChange={(e) => handleProductDetailsChange(e, externalIdx, -1)}
                disabled={!editable}
              />
            </div>
          </div>
          {editable && (
            <div className="bundle-btn-container col-md-12">
              <div className="agr-temp-product-checkbox">
                <Checkbox
                  isChecked={productBundle.selected_product === 0}
                  name="product-selected"
                  onChange={(e) => handleChangeCheckBox(e, externalIdx)}
                >
                  Selected By Default
                </Checkbox>
                <Checkbox
                  isChecked={productBundle.is_required}
                  name="product-required"
                  onChange={(e) => handleChangeCheckBox(e, externalIdx)}
                >
                  Required Product
                </Checkbox>
              </div>
              <div className="agr-temp-product-btn">
                <SquareButton
                  onClick={() => handleAddSingleProduct(externalIdx)}
                  content={"\uFF0B Option"}
                  bsStyle={ButtonStyle.DEFAULT}
                  className="add-bundle-btn"
                />
                {products.length > 1 && (
                  <SquareButton
                    content={
                      <>
                        <span>Delete</span>
                        <SmallConfirmationBox
                          className="delete-bundle-confirmation"
                          onClickOk={() =>
                            handleRemoveProductBundle(externalIdx)
                          }
                          text={"Option"}
                        />
                      </>
                    }
                    bsStyle={ButtonStyle.DANGER}
                    className="remove-bundle-btn"
                    onClick={() => {}}
                  />
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };

  return (
    <section className="product-main-container">
      <h3 className="heading-three">Products</h3>
      {!editable && !disabled ? (
        <DraggableArea
          isList
          tags={products}
          render={({ tag, index }) => renderProductBundle(tag, index)}
          onChange={(tags) => handleDraggableChange(tags)}
        />
      ) : (
        products.map((bundle, idx) => renderProductBundle(bundle, idx))
      )}
      <SquareButton
        onClick={handleAddProductBundle}
        content={"Add Product"}
        bsStyle={ButtonStyle.PRIMARY}
        className="add-product-btn"
        disabled={disabled}
      />
    </section>
  );
};

export default Products;
