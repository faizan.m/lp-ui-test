import React from 'react';
import { Line } from 'react-chartjs-2';
import { connect } from 'react-redux';

import {
  exportRawData,
  getRawData,
  getServiceActivity,
  getServiceHistoryExpected,
  getServiceHostoryWon,
} from '../../../actions/sowDashboard';
import SquareButton from '../../../components/Button/button';
import ModalBase from '../../../components/ModalBase/modalBase';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';
import PDFViewer from '../../../components/PDFViewer/PDFViewer';
import './style.scss';

interface IDashboardProps extends ICommonProps {
  service_activity: ISOWDashboardField[];
  serviceHistoryCreated: any;
  isFetchingActivity: boolean;
  isFetchingSOWCreated: boolean;
  isFetchingExpected: boolean;
  isFetchingSRawData: boolean;
  getServiceActivity: any;
  getServiceHostoryWon: any;
  getServiceHistoryExpected: any;
  rawData: any;
  sowCreated: any;
  expectedWon: any;
  getRawData: any;
  exportRawData: any;
}

interface IDashboardState {
  service_activity: ISOWDashboardField[];
  showDetails: boolean;
  documents: any[];
  downloadingPDFIds: number[];
  openPreview: boolean;
  previewHTML: any;
}

class SOWDashBoard extends React.Component<IDashboardProps, IDashboardState> {
  constructor(props: IDashboardProps) {
    super(props);
    this.state = {
      service_activity: [],
      showDetails: false,
      documents: [],
      downloadingPDFIds: [],
      openPreview: false,
      previewHTML: null,
    };
  }

  componentDidMount() {
    if (this.props.service_activity) {
      this.setState({
        service_activity: this.props.service_activity,
      });
    } else {
      this.props.getServiceActivity();
    }
    this.props.getServiceHostoryWon();
    this.props.getServiceHistoryExpected();
    this.props.getRawData();
  }

  componentDidUpdate(prevProps: IDashboardProps) {
    if (this.props.service_activity && prevProps.service_activity !== this.props.service_activity) {
      this.setState({
        service_activity: this.props.service_activity,
      });
    }
  }

  getBody = docs => {
    const docsAsso =
      docs &&
      docs.map(item => {
        return `${item.circuit_id}`;
      });
    const columnsPopUp: any = [
      {
        accessor: 'name',
        Header: 'Name',
      },
      {
        accessor: 'customer',
        Header: 'Customer',
      },
      {
        accessor: 'quote',
        Header: 'Quote',
      },
      {
        accessor: 'author_name',
        Header: 'Author',
      },
      {
        accessor: 'hours',
        Header: 'Hours',
        Cell: c => <div>{c.value? c.value.toFixed(2) : '-'}</div>,
      },
      {
        accessor: 'created_on',
        Header: 'Created On',
        id: 'created_on',
        width: 110,
        sortable: true,
        Cell: cell => (
          <div>
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : ' N.A.'
              }`}
          </div>
        ),
      }
    ];
    const message = (
      <div style={{ width: '100%' }}>
        <Table
          columns={columnsPopUp}
          customTopBar={null}
          rows={docs || []}
          className={``}
          loading={this.props.isFetchingSRawData}
        />
      </div>
    );

    return docsAsso && docsAsso.length > 0 && message ? message : '';
  };

  toggleShowDetails = () => {
    this.setState({
      showDetails: false,
      documents: [],
    });
  };
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  openShowDetails = activity => {
    if (activity.documents && activity.documents.length > 0) {
      this.setState({
        showDetails: true,
        documents: activity.documents,
      });
    }
  };

  render() {
    const options2 = {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        text: `Expected vs Won Hours`,
        fontSize: 20,
        fontStyle: 800,
      },
      legend: {
        display: true,
        labels: {
          fontStyle: 800,
        },
        position: 'bottom',
      },
    };
    const options = {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        text: `SoW's Created / Won`,
        fontSize: 20,
        fontStyle: 800,
      },
      legend: {
        display: true,
        labels: {
          fontStyle: 800,
        },
        position: 'bottom',
      },
    };

    const columns: any = [
      {
        accessor: 'date',
        Header: 'Date',
      },
      {
        accessor: 'sow_created',
        Header: 'SoW’s Created',
      },
      {
        accessor: 'sow_won',
        Header: 'SoW’s Won',
      },
      {
        accessor: 'expected_hours',
        Header: 'Expected Hours',
      },
      {
        accessor: 'won_hours',
        Header: 'Won Hours',
      },
    ];

    return (
      <div className="sow-dashboard col-md-12 row ">
        <div className="header">
          <h3>Service Analytics</h3>
        </div>
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingActivity ||
              this.props.isFetchingSOWCreated ||
              this.props.isFetchingExpected
            }
          />
        </div>
        {this.state.service_activity &&
          Object.keys(this.state.service_activity).map((section, index) => (
            <div key={index} className="sow-dashboard-row col-md-12 ">
              <div className="heading"> {section} </div>
              <div className="body col-md-12 ">
                {this.state.service_activity &&
                  this.state.service_activity[section].map((activity, i) => (
                    <div key={i} className="block col-md-3 col-xs-6">
                      <div className="block-label">{activity.label}</div>
                      <div
                        className={` ${
                          activity.documents && activity.documents.length > 0
                            ? 'pop-up-available'
                            : ''
                          } box`}
                        style={{
                          backgroundColor: activity.bg_color,
                          color: activity.text_color,
                          borderLeft: `5px solid ${activity.text_color}`
                        }}
                        title={activity.hover_text}
                        onClick={e => this.openShowDetails(activity)}
                      >
                        {activity.value}
                        {activity.unit}
                        {activity.documents &&
                          activity.documents.length > 0 && (
                            <img
                              className="view-details-icon"
                              src={`/assets/new-icons/info.svg`}
                              title={'view details'}
                            />
                          )}
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          ))}
        <div className="sow-dashboard-row  col-md-12 ">
          <div className="heading">Service History</div>
          <div className="body graph-box col-md-6">
            {this.props.sowCreated && (
              <Line data={this.props.sowCreated} options={options} />
            )}
          </div>
          <div className="body graph-box col-md-6">
            {this.props.expectedWon && (
              <Line data={this.props.expectedWon} options={options2} />
            )}
          </div>
        </div>
        <div className="sow-dashboard-row  col-md-12 ">
          <div className="heading raw-data-heading">
            {' '}
            <span>Raw Data</span>{' '}
            <SquareButton
              content="Export"
              bsStyle={ButtonStyle.PRIMARY}
              onClick={e => this.props.exportRawData()}
            />
          </div>
          <div className={true ? 'loader' : ''}>
            <Spinner show={this.props.isFetchingSRawData} />
          </div>
          <div className="body col-md-12">
            <Table
              columns={columns}
              customTopBar={null}
              rows={this.props.rawData || []}
              className={`${this.props.isFetchingSRawData ? 'loading' : ''}`}
            />
          </div>
          <ModalBase
            show={this.state.showDetails}
            onClose={this.toggleShowDetails}
            titleElement={'Documents'}
            bodyElement={this.getBody(this.state.documents)}
            footerElement={
              <div className={`add-configuration__footer`}>
                <SquareButton
                  onClick={this.toggleShowDetails}
                  content="Close"
                  bsStyle={ButtonStyle.PRIMARY}
                />
              </div>
            }
            className="add-configuration configuration-view"
          />
        </div>
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={ButtonStyle.DEFAULT}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetchingActivity: state.sowDashboard.isFetchingActivity,
  isFetchingSOWCreated: state.sowDashboard.isFetchingSOWCreated,
  isFetchingExpected: state.sowDashboard.isFetchingExpected,
  isFetchingSRawData: state.sowDashboard.isFetchingSRawData,
  service_activity: state.sowDashboard.service_activity,
  rawData: state.sowDashboard.rawData,
  sowCreated: state.sowDashboard.sowCreated,
  expectedWon: state.sowDashboard.expectedWon,
});

const mapDispatchToProps = (dispatch: any) => ({
  getServiceActivity: () => dispatch(getServiceActivity()),
  getServiceHistoryExpected: () => dispatch(getServiceHistoryExpected()),
  getServiceHostoryWon: () => dispatch(getServiceHostoryWon()),
  getRawData: () => dispatch(getRawData()),
  exportRawData: () => dispatch(exportRawData())
});

export default connect(mapStateToProps, mapDispatchToProps)(SOWDashBoard);
