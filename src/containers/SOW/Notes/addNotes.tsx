import React from "react";
import "quill-mention";
import ReactQuill from "react-quill"; // ES6
import { connect } from "react-redux";
import { get, cloneDeep, debounce, DebouncedFunc } from "lodash";
import { Mention, MentionsInput } from "react-mentions";
import {
  createNotes,
  updateNotes,
  getNotesList,
  sowActionTypes,
  getNotesListAll,
} from "../../../actions/sow";
import { fetchQuoteDashboardListingPU } from "../../../actions/dashboard";
import { fetchSitesProvidersCustomersUser } from "../../../actions/inventory";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import CustomerUserNew from "../Sow/addUser";
import CreateSite from "../../CreateNew/createSite";
import CreateActivity from "../../CreateNew/createActivity";
import CreateTechnology from "../../CreateNew/createTechnology";
import CreateOpportunity from "../../CreateNew/createOpportunity";
import {
  getValuesForQuill,
  getValuesForMension,
  getTechnologyTypesForQuiell,
  getTechnologyTypesForQuiellMension,
} from "./utilsNotes";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import "./style.scss";

export enum PageType {
  Notes = "Note",
  Search = "All Notes",
}

interface INoteProps extends ICommonProps {
  sites: ISite[];
  show: boolean;
  customerId: number;
  defaultTab: string;
  isFetching: boolean;
  qStageList: IQuote[];
  contacts: ISuperUser[];
  salesNote?: ISalesNote;
  serviceType: ICategoryList[];
  customersShort: ICustomerShort[];
  onClose: () => void;
  fetchData: () => void;
  getNotesList: () => Promise<any>;
  getNotesListAll: () => Promise<any>;
  fetchServiceTypeSTT?: () => Promise<any>;
  fetchCustomerUsers: (id: number) => Promise<any>;
  fetchQuoteDashboardListing: (id: number) => Promise<any>;
  fetchSitesProvidersCustomersUser: (id: number) => Promise<any>;
  createNotes: (customerId: number, note: ISalesNote) => Promise<any>;
  updateNotes: (customerId: number, note: ISalesNote) => Promise<any>;
}
interface INoteState {
  sites: ISite[];
  hide: boolean;
  saved: boolean;
  customer?: any;
  textObject: any;
  pureText: string;
  currentPage: string;
  markDownText: string;
  searchString: string;
  qStageList: IQuote[];
  isCreateSite: boolean;
  salesNote: ISalesNote;
  filterCustomer?: any[];
  contacts: ISuperUser[];
  notesList: ISalesNote[];
  isCreateContact: boolean;
  isCreateActivity: boolean;
  isCreateTechnology: boolean;
  serviceType: ICategoryList[];
  isCreateOpportunity: boolean;
  customerOptions: IPickListOptions[];
  error: {
    body: IFieldValidation;
  };
}

class AddNote extends React.Component<INoteProps, INoteState> {
  myRef: React.LegacyRef<ReactQuill>;
  private debouncedFetch: DebouncedFunc<(close?: boolean) => void>;
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: INoteProps) {
    super(props);

    this.state = {
      salesNote: {
        body: null,
        description: null,
        opportunity_ids: [],
        contact_ids: [],
        site_ids: [],
        technology_ids: [],
        markDownText: null,
        notes_object: null,
      },
      error: {
        body: { ...AddNote.emptyErrorState },
      },
      sites: [],
      hide: true,
      pureText: "",
      saved: false,
      contacts: [],
      notesList: [],
      customer: null,
      qStageList: [],
      serviceType: [],
      textObject: null,
      searchString: "",
      markDownText: null,
      filterCustomer: [],
      customerOptions: [],
      isCreateSite: false,
      isCreateContact: false,
      isCreateActivity: false,
      isCreateTechnology: false,
      isCreateOpportunity: false,
      currentPage: this.props.defaultTab,
    };
    this.myRef = React.createRef();
    this.debouncedFetch = debounce(this.onSubmitNote, 3000);
  }

  componentDidMount() {
    if (this.props.salesNote) {
      this.setState({
        salesNote: this.props.salesNote,
        customer: this.props.salesNote.customer,
        filterCustomer: [this.props.salesNote.customer],
        pureText: this.props.salesNote.body,
        markDownText: this.props.salesNote.notes_object,
      });
      if (this.props.salesNote.customer) {
        const customerId = Number(this.props.salesNote.customer);
        this.props.fetchCustomerUsers(customerId);
        this.props.fetchQuoteDashboardListing(customerId);
        this.props.fetchSitesProvidersCustomersUser(customerId);
      }
    }
    if (this.props.customersShort && this.props.customersShort.length) {
      this.setState({
        customerOptions: this.props.customersShort.map((role) => ({
          value: role.id,
          label: role.name,
        })),
      });
    }
    this.props.getNotesListAll().then((a) => {
      if (a.type === sowActionTypes.GET_ALL_NOTES_SUCCESS) {
        this.setState({ notesList: a.response });
      }
    });
  }

  // Can also be done with getDerivedStateFromProps (in future)
  componentDidUpdate(prevProps: INoteProps) {
    const { sites, contacts, qStageList, customersShort } = this.props;
    if (sites && sites !== prevProps.sites) {
      this.setState({ sites });
    }
    if (contacts && contacts !== prevProps.contacts) {
      this.setState({ contacts });
    }
    if (qStageList && qStageList !== prevProps.qStageList) {
      this.setState({ qStageList });
    }
    if (customersShort && customersShort !== prevProps.customersShort) {
      this.setState({
        customerOptions: this.props.customersShort.map((role) => ({
          value: role.id,
          label: role.name,
        })),
      });
    }
  }

  onClose = () => {
    this.props.onClose();
    if (this.state.saved) {
      this.props.fetchData();
    }
  };

  getIdsFromObject = (symbol: string, textObject) => {
    let mentionObj = [];
    mentionObj = textObject.ops.filter((t) =>
      t.insert.hasOwnProperty("mention")
    );
    const filteredIds = mentionObj
      .filter((object) => object.insert.mention.denotationChar === symbol)
      .map((object) => object.insert.mention.id);

    return filteredIds;
  };

  saveBtnClick = (e) => {
    this.onSubmitNote(true);
  };

  onSubmitNote = (close?: boolean) => {
    const salesNote = {
      id: this.state.salesNote.id,
      body: this.state.pureText,
      description: this.state.salesNote.description,
      notes_object: this.state.markDownText,
      opportunity_ids: this.getIdsFromObject("$", this.state.textObject),
      contact_ids: this.getIdsFromObject("@", this.state.textObject),
      site_ids: this.getIdsFromObject("%", this.state.textObject),
      technology_ids: this.getIdsFromObject("#", this.state.textObject),
    };
    if (!this.state.salesNote.id) {
      this.props.createNotes(this.state.customer, salesNote).then((action) => {
        if (action.type === sowActionTypes.CREATE_NOTE_SUCCESS) {
          // this.props.onClose(event);
          this.props.getNotesList();
          const newState = cloneDeep(this.state);
          (newState.salesNote.id as number) = action.response.id;
          (newState.saved as boolean) = true;
          this.setState(newState);
        }
      });
    } else {
      this.props.updateNotes(this.state.customer, salesNote).then((action) => {
        if (action.type === sowActionTypes.UPDATE_NOTE_SUCCESS) {
          // this.props.onClose(event);
          const newState = cloneDeep(this.state);
          (newState.saved as boolean) = true;
          this.setState(newState);
          if (close) {
            this.onClose();
          }
        }
      });
    }
  };

  getTitle = () => {
    return (
      <div className="notes-details">
        Notes
        <SquareButton
          content="Open in New Window"
          bsStyle={ButtonStyle.PRIMARY}
          className="open-new-tab"
          onClick={() => {
            const win = window.open(
              `/sow/notes?openNew=${(this.props.salesNote &&
                this.props.salesNote.id) ||
                "new"}`,
              "",
              'fullscreen="yes"'
            );
            win.focus();
            this.onClose();
          }}
        />
      </div>
    );
  };

  handlePageChange = (page: PageType): void => {
    const newState = cloneDeep(this.state);
    (newState.currentPage as PageType) = page;
    this.setState(newState);
  };

  renderTopBar = () => {
    return (
      <div className="top-bar-header">
        {Object.keys(PageType).map((page, index) => {
          return (
            <div
              key={index}
              className={`header-link ${
                this.state.currentPage === PageType[page] ? "--active" : ""
              }`}
              onClick={() => this.handlePageChange(PageType[page])}
            >
              {PageType[page]}
            </div>
          );
        })}
      </div>
    );
  };

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values;

      if (mentionChar === "@") {
        values =
          this.state.contacts &&
          this.state.contacts.map((c) => ({
            id: c.crm_id,
            value: `${c.first_name}-${c.last_name} (${c.email})`,
          }));
      } else if (mentionChar === "#") {
        values = getTechnologyTypesForQuiell(this.props.serviceType);
      } else if (mentionChar === "$") {
        values = getValuesForQuill(this.state.qStageList, "id", "name");
      } else {
        values = getValuesForQuill(this.state.sites, "site_id", "name");
      }

      if (searchTerm.length === 0) {
        renderList(values, searchTerm);
      } else {
        const matches = [];
        for (let i = 0; i < values.length; i++)
          if (~values[i].value.toLowerCase().indexOf(searchTerm.toLowerCase()))
            matches.push(values[i]);
        renderList(matches, searchTerm);
      }
    },
  };

  listingInfo = () => {
    return (
      <div className="symbols">
        {this.props.serviceType && this.props.serviceType.length > 0 && (
          <div data-denotation-char="#" className="char-field">
            {" "}
            <span>#</span> - Technology list.
          </div>
        )}
        {this.state.sites && this.state.sites.length > 0 && (
          <div data-denotation-char="%" className="char-field">
            {" "}
            <span>%</span> - Site list.
          </div>
        )}

        {this.state.contacts && this.state.contacts.length > 0 && (
          <div data-denotation-char="@" className="char-field">
            {" "}
            <span>@</span> - Contact list.
          </div>
        )}
        {this.state.qStageList && this.state.qStageList.length > 0 && (
          <div data-denotation-char="$" className="char-field">
            {" "}
            <span>$</span> - Opportunity list.
          </div>
        )}
      </div>
    );
  };

  handleChange = (content: string, delta, source: string, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.pureText as string) = this.getContentFromObject(object);
    (newState.markDownText as string) = content;
    (newState.textObject as any) = object;
    (newState.saved as boolean) = false;
    this.setState(newState);
    if (
      (this.props.salesNote && this.props.salesNote.notes_object) !== content &&
      this.state.salesNote.description
    ) {
      this.debouncedFetch();
    }
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangeDetails = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    (newState.searchString as string) = e.target.value.trim();
    this.setState(newState);
  };

  handleChangeCustomer = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      customer: event.target.value,
      filterCustomer: [event.target.value],
    });
    if (event.target.value) {
      const customerId = Number(event.target.value);
      this.props.fetchCustomerUsers(customerId);
      this.props.fetchQuoteDashboardListing(customerId);
      this.props.fetchSitesProvidersCustomersUser(customerId);
    }
  };

  handleChangeDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    (newState.salesNote.description as string) = event.target.value;
    this.setState(newState);
  };

  renderCreateOptions() {
    return (
      <div className="create-new-stoc">
        <div title="Create New" className="heading">
          {" "}
          Create :{" "}
        </div>
        <div
          data-denotation-char="#"
          title="Create Technology"
          className="create-buttons"
          onClick={(e) => this.toggleCreateTechnologyModal()}
        >
          #{" "}
        </div>
        {this.state.customer && (
          <>
            <div
              title="Create Site"
              data-denotation-char="%"
              className="create-buttons"
              onClick={(e) => this.toggleCreateSiteModal()}
            >
              %
            </div>
            <div
              title="Create Contact"
              data-denotation-char="@"
              className="create-buttons"
              onClick={(e) => this.toggleCreateUserModal()}
            >
              @
            </div>
            <div
              title="Create Opportunity"
              data-denotation-char="$"
              className="create-buttons"
              onClick={(e) => this.toggleCreateOpportunityModal()}
            >
              $
            </div>
            <div
              title="Create Activity"
              className="create-buttons"
              onClick={(e) => this.toggleCreateActivityModal()}
            >
              <img
                src={`/assets/icons/running.png`}
                alt=""
                className="status-heartbit-images"
              />
            </div>
          </>
        )}
      </div>
    );
  }

  getBody = () => {
    const matchingCount = this.state.notesList
      .filter((note) =>
        this.state.filterCustomer.length > 0
          ? this.state.filterCustomer.includes(note.customer)
            ? true
            : false
          : true
      )
      .filter((note) =>
        note.notes_object
          .toLowerCase()
          .includes(this.state.searchString.toLowerCase())
      ).length;
    return (
      <div className="note-body">
        {this.renderTopBar()}
        {this.state.currentPage === PageType.Notes && (
          <>
            <div className="select-customer-note row">
              <Input
                field={{
                  label: "Customer",
                  type: InputFieldType.PICKLIST,
                  value: this.state.customer,
                  options: this.state.customerOptions,
                  isRequired: true,
                }}
                width={5}
                name="serviceType"
                onChange={this.handleChangeCustomer}
                placeholder={`Select Customer`}
                error={
                  !this.state.customer
                    ? {
                        errorState: IValidationState.ERROR,
                        errorMessage: "Please Select Customer",
                      }
                    : {
                        errorState: IValidationState.SUCCESS,
                        errorMessage: "",
                      }
                }
              />
              <Input
                field={{
                  label: "Description",
                  type: InputFieldType.TEXT,
                  value: this.state.salesNote.description,
                  isRequired: true,
                }}
                width={7}
                name="description"
                onChange={(e) => this.handleChangeDescription(e)}
                placeholder={`Enter description`}
                error={
                  !this.state.salesNote.description
                    ? {
                        errorState: IValidationState.ERROR,
                        errorMessage: "Please Enter Description",
                      }
                    : {
                        errorState: IValidationState.SUCCESS,
                        errorMessage: "",
                      }
                }
              />
            </div>
            {this.renderCreateOptions()}
            <ReactQuill
              ref={this.myRef}
              value={this.state.markDownText}
              onChange={this.handleChange}
              modules={{ mention: this.mentionModule }}
            />
            {this.state.saved && !this.props.isFetching ? (
              <div className="saving-save"> saved</div>
            ) : (
              ""
            )}
            {!this.state.saved && this.props.isFetching ? (
              <div className="saving-save"> saving...</div>
            ) : (
              ""
            )}
          </>
        )}
        {this.state.currentPage === PageType.Notes && (
          <div className="help-text-setion col-md-12">
            <div className="first-line">
              Please enter following symbols to view list
            </div>
            {this.listingInfo()}
          </div>
        )}
        {this.state.currentPage === PageType.Notes && (
          <div className="actions">
            <SquareButton
              content="Save"
              bsStyle={ButtonStyle.PRIMARY}
              onClick={this.saveBtnClick}
            />
          </div>
        )}

        {this.state.currentPage === PageType.Search && (
          <>
            <div className="all-note-header">
              <MentionsInput
                value={this.state.searchString}
                onChange={this.handleChangeDetails}
                className={"outer input-search"}
              >
                <Mention
                  trigger="@"
                  data={
                    this.state.contacts &&
                    this.state.contacts.map((c) => ({
                      id: c.crm_id,
                      display: ` ${c.first_name}-${c.last_name} (${c.email})`,
                    }))
                  }
                  className={"inner-drop"}
                  markup=" __display__"
                />
                <Mention
                  trigger="#"
                  data={getTechnologyTypesForQuiellMension(
                    this.props.serviceType
                  )}
                  className={"inner-drop"}
                  markup=" __display__"
                />
                <Mention
                  trigger="$"
                  data={getValuesForMension(
                    this.state.qStageList,
                    "id",
                    "name"
                  )}
                  className={"inner-drop"}
                  markup=" __display__"
                />
                <Mention
                  trigger="%"
                  data={getValuesForMension(
                    this.state.sites,
                    "site_id",
                    "name"
                  )}
                  className={"inner-drop"}
                  markup=" __display__"
                />
              </MentionsInput>
              <Input
                field={{
                  label: null,
                  type: InputFieldType.PICKLIST,
                  value: this.state.filterCustomer,
                  options: this.state.customerOptions,
                  isRequired: false,
                }}
                multi={true}
                width={4}
                name="filterCustomer"
                className="customer-filter-input"
                onChange={this.filterByCustomer}
                placeholder={`Filter by Customer`}
              />
              <Checkbox
                isChecked={this.state.hide}
                name="hide"
                onChange={(e) => this.setState({ hide: e.target.checked })}
                className="hide-notes"
              >
                Show matching notes only
              </Checkbox>
            </div>
            {(this.state.searchString ||
              this.state.filterCustomer.length > 0) && (
              <div className="count-matching">
                {matchingCount} matching {matchingCount > 1 ? "notes" : "note"}{" "}
                found
              </div>
            )}
            {this.listingInfo()}
            <div id="inputText " ref="Progress1">
              {this.state.notesList
                .filter((note) =>
                  this.state.filterCustomer.length > 0
                    ? this.state.filterCustomer.includes(note.customer)
                      ? true
                      : false
                    : true
                )
                .filter((note) =>
                  this.state.hide
                    ? note.notes_object
                        .toLowerCase()
                        .includes(this.state.searchString.toLowerCase())
                    : true
                )
                .map((note: any, index) => {
                  return (
                    <div
                      key={index}
                      className={`${
                        this.state.searchString &&
                        note.notes_object
                          .toLowerCase()
                          .includes(this.state.searchString.toLowerCase())
                          ? "found-border"
                          : "not-found-border"
                      } note-section-single`}
                    >
                      <div className="left">
                        <div className="description">
                          Description : {note.description}
                        </div>
                        <div
                          className={`${
                            note.notes_object
                              .toLowerCase()
                              .includes(this.state.searchString.toLowerCase())
                              ? "found"
                              : "not-found"
                          } single-notes`}
                          dangerouslySetInnerHTML={{
                            __html: this.state.searchString
                              ? note.notes_object.replaceAll(
                                  this.state.searchString,
                                  (match) => {
                                    return `<b style='background: #ffeb3b !important;'>${match}</b>`;
                                  }
                                )
                              : note.notes_object,
                          }}
                        />
                      </div>

                      <div className="note-details">
                        <div className="box">
                          Customer :{" "}
                          <b>
                            {" "}
                            {
                              this.props.customersShort.find(
                                (x) => x.id === note.customer
                              ).name
                            }{" "}
                          </b>
                        </div>
                        <div className="box">
                          Created on :{" "}
                          <b>{fromISOStringToFormattedDate(note.created_on)}</b>
                        </div>
                        <div className="box">
                          Last Updated by <b> {note.last_updated_by} </b>
                          <br />
                          on :{" "}
                          <b>
                            {fromISOStringToFormattedDate(note.updated_on)}{" "}
                          </b>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </>
        )}
        {this.state.isCreateContact && (
          <CustomerUserNew
            isVisible={this.state.isCreateContact}
            close={this.closeUserModal}
            customerId={this.state.customer}
          />
        )}
        {this.state.isCreateTechnology && (
          <CreateTechnology
            isVisible={this.state.isCreateTechnology}
            close={this.closeTechnologyModal}
            customerId={this.state.customer}
          />
        )}
        {this.state.isCreateSite && (
          <CreateSite
            isVisible={this.state.isCreateSite}
            close={this.closeSiteModal}
            customerId={this.state.customer}
          />
        )}
        {this.state.isCreateOpportunity && (
          <CreateOpportunity
            isVisible={this.state.isCreateOpportunity}
            close={() => this.closeOpportunityModal(true)}
            customerId={this.state.customer}
          />
        )}
        {this.state.isCreateActivity && (
          <CreateActivity
            isVisible={this.state.isCreateActivity}
            close={this.closeActivityModal}
            customer={this.state.customer}
            description={this.state.salesNote.description}
            {...this.props}
          />
        )}
      </div>
    );
  };

  toggleCreateUserModal = () => {
    this.setState((prevState) => ({
      isCreateContact: !prevState.isCreateContact,
    }));
  };

  closeUserModal = () => {
    this.setState((prevState) => ({
      isCreateContact: !prevState.isCreateContact,
    }));
    this.props.fetchCustomerUsers(this.state.customer);
  };

  toggleCreateTechnologyModal = () => {
    this.setState((prevState) => ({
      isCreateTechnology: !prevState.isCreateTechnology,
    }));
  };

  closeTechnologyModal = (reloadData: boolean) => {
    this.setState((prevState) => ({
      isCreateTechnology: !prevState.isCreateTechnology,
    }));
    if (reloadData === true) {
      this.props.fetchServiceTypeSTT();
    }
  };

  toggleCreateSiteModal = () => {
    this.setState((prevState) => ({
      isCreateSite: !prevState.isCreateSite,
    }));
  };

  closeSiteModal = () => {
    this.setState((prevState) => ({
      isCreateSite: !prevState.isCreateSite,
    }));
  };

  toggleCreateOpportunityModal = () => {
    this.setState((prevState) => ({
      isCreateOpportunity: !prevState.isCreateOpportunity,
    }));
  };
  toggleCreateActivityModal = () => {
    this.setState((prevState) => ({
      isCreateActivity: !prevState.isCreateActivity,
    }));
  };

  closeActivityModal = () => {
    this.setState((prevState) => ({
      isCreateActivity: !prevState.isCreateActivity,
    }));
  };

  closeOpportunityModal = (reloadData: boolean) => {
    this.setState((prevState) => ({
      isCreateOpportunity: !prevState.isCreateOpportunity,
    }));
    // if (reloadData === true) {
    //   this.props.fetchQuoteDashboardListing(this.state.customer);
    // }
  };

  filterByCustomer = (event: any) => {
    this.setState({ filterCustomer: event.target.value });
    if (event.target.value.length > 0) {
      const customerId = event.target.value[0];
      this.props.fetchCustomerUsers(customerId);
    } else {
      this.setState({
        sites: [],
        contacts: [],
        qStageList: [],
      });
    }
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={null}
        className="sales-notes"
      />
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customerId: state.customer.customerId,
  isFetching: state.sow.isFetching,
  customersShort: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  createNotes: (customerId: number, note: ISalesNote) =>
    dispatch(createNotes(customerId, note)),
  updateNotes: (customerId: number, note: ISalesNote) =>
    dispatch(updateNotes(customerId, note)),
  getNotesList: () => dispatch(getNotesList()),
  getNotesListAll: () => dispatch(getNotesListAll({ pagination: false })),
  fetchQuoteDashboardListing: (id: number) =>
    dispatch(fetchQuoteDashboardListingPU(id)),
  fetchSitesProvidersCustomersUser: (id: number) =>
    dispatch(fetchSitesProvidersCustomersUser(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNote);
