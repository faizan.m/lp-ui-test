import originalMoment from "moment";
import { extendMoment } from "moment-range";
import React from "react";

import SquareButton from "../../../components/Button/button";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import { getTechnologyTypes } from "./utilsNotes";
import DateRangePicker from "react-daterange-picker";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import "react-daterange-picker/dist/css/react-calendar.css";
import "./style.scss";

const moment = extendMoment(originalMoment);
const today = moment();

interface INotesFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: INotesFilters) => void;
  prevFilters?: INotesFilters;
  serviceType: any;
  customersShort: any;
  singleCustomer: boolean;
}
interface INotesFilterState {
  filters: INotesFilters;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
  isDatePickerEnabled: boolean;
  value: any;
}

export default class NotesFilter extends React.Component<
  INotesFilterProps,
  INotesFilterState
> {
  constructor(props: INotesFilterProps) {
    super(props);

    this.state = {
      filters: {},
      isDatePickerSelected: false,
      isDateValueSelected: false,
      isDatePickerEnabled: false,
      value: moment.range(today.clone().subtract(7, "days"), today.clone()),
    };
  }

  componentDidUpdate(prevProps: INotesFilterProps) {
    if (this.props.show !== prevProps.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }

  onClose = (e) => {
    this.props.onClose(e);
  };

  onSubmit = (e) => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  onSelect = (value, states) => {
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        updated_on_0: fromISOStringToFormattedDate(
          value.start._d,
          "YYYY-MM-DD"
        ),
        updated_on_1: fromISOStringToFormattedDate(value.end._d, "YYYY-MM-DD"),
        dateValue: value,
      },
      value,
      isDatePickerSelected: false,
      isDateValueSelected: true,
    }));
  };

  resetDate = () => {
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        updated_on_0: "",
        updated_on_1: "",
        dateValue: null,
      },
      value: null,
      isDatePickerSelected: false,
      isDateValueSelected: false,
    }));
  };

  showDateRangePicker = () => {
    this.setState({ isDatePickerSelected: !this.state.isDatePickerSelected });
    this.setState({ isDateValueSelected: false });
  };
  getBody = () => {
    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Customer</label>
            <div className="field__input">
              <SelectInput
                name="customer_ids"
                value={filters.customer_ids}
                onChange={this.onFilterChange}
                options={
                  this.props.customersShort &&
                  this.props.customersShort.map((c) => ({
                    value: c.id,
                    label: c.name,
                  }))
                }
                multi={true}
                placeholder="Select Customer"
                disabled={this.props.singleCustomer}
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Technology</label>
            <div className="field__input">
              <SelectInput
                name="technology_ids"
                value={filters.technology_ids}
                onChange={this.onFilterChange}
                options={getTechnologyTypes(this.props.serviceType)}
                multi={true}
                placeholder="Select Technology"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <label className="field__label-label">Date Range</label>
          <div className="date-range-box">
            <SquareButton
              onClick={this.showDateRangePicker}
              content=""
              bsStyle={ButtonStyle.DEFAULT}
              className="date-button"
            />

            {this.state.isDatePickerSelected &&
              !this.state.isDateValueSelected && (
                <DateRangePicker
                  value={this.state.value}
                  onSelect={this.onSelect}
                  singleDateRange={true}
                  minimumDate={new Date("1990/01/01")}
                  maximumDate={new Date()}
                  numberOfCalendars={1}
                  showLegend={true}
                  clearAriaLabel={"reset"}
                  ClearIcon={"reset"}
                />
              )}
            {!this.state.isDatePickerSelected &&
              this.state.isDateValueSelected && (
                <div className="date-value" title={"MM-DD-YYYY"}>
                  {fromISOStringToFormattedDate(
                    this.state.filters.updated_on_0,
                    "MM-DD-YYYY"
                  )}
                  {" - "}
                  {fromISOStringToFormattedDate(
                    this.state.filters.updated_on_1,
                    "MM-DD-YYYY"
                  )}
                </div>
              )}
            {!this.state.isDatePickerSelected &&
              this.state.isDateValueSelected && (
                <div onClick={(e) => this.resetDate()} className="reset-button">
                  x
                </div>
              )}
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={ButtonStyle.PRIMARY}
          disabled={this.state.isDatePickerSelected}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
