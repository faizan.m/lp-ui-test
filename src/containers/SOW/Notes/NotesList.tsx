import React from "react";
import { DebouncedFunc, debounce } from "lodash";
import { connect } from "react-redux";
import {
  deleteNotes,
  getNote,
  getNotesList,
  sowActionTypes,
} from "../../../actions/sow";
import { LAST_VISITED_CLIENT_360_TAB, fetchCustomerUsers } from "../../../actions/customer";
import { fetchServiceTypeSTT } from "../../../actions/setting";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import Select from "../../../components/Input/Select/select";
import EditButton from "../../../components/Button/editButton";
import DeleteButton from "../../../components/Button/deleteButton";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import NotesDetails from "./details";
import NotesFilter from "./filterNotes";
import AddNotes, { PageType } from "./addNotes";
import { getValuesForSelect } from "./utilsNotes";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import "react-daterange-picker/dist/css/react-calendar.css";
import "../../../commonStyles/filterModal.scss";
import store from "../../../store";
import "./style.scss";

interface INotesListProps extends ICommonProps {
  sites: any;
  users: any;
  customerId?: number;
  isFetching: boolean;
  qStageList: IQuote[];
  isFetchingSCList: boolean;
  serviceType: ICategoryList[];
  customersShort: ICustomerShort[];
  getNote: (id: number) => Promise<any>;
  fetchCustomerUsers: () => Promise<any>;
  fetchServiceTypeSTT: () => Promise<any>;
  deleteNotes: (id: number) => Promise<any>;
  NotesList: IPaginatedDefault & { results: ISalesNote[] };
  getNotesList: (params?: IServerPaginationParams) => Promise<any>;
}

interface INotesListState {
  rows: INoteRow[];
  id: string;
  detailRow: any;
  reset: boolean;
  defaultTab: string;
  searchString: string;
  isopenConfirm: boolean;
  filters: INotesFilters;
  isNotesPosting: boolean;
  showOlderNotes: boolean;
  salesNote?: ISalesNote;
  downloadingIds: number[];
  isDetailVisible: boolean;
  isFilterModalOpen: boolean;
  isAddNotesModalOpen: boolean;
  authorLabelIds: { [id: number]: string };
  customerLabelIds: { [id: number]: string };
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
}

interface INoteRow {
  body: string;
  description: string;
  created_on: string;
  updated_on: string;
  salesNote: ISalesNote;
  last_updated_by: string;
  id: number;
  customer: number;
  index: number;
}

class NotesListing extends React.Component<INotesListProps, INotesListState> {
  private debouncedFetch: DebouncedFunc<
    (params?: IServerPaginationParams) => void
  >;

  constructor(props: INotesListProps) {
    super(props);

    this.state = {
      id: "",
      rows: [],
      reset: false,
      detailRow: null,
      searchString: "",
      downloadingIds: [],
      authorLabelIds: {},
      isopenConfirm: false,
      customerLabelIds: {},
      isNotesPosting: false,
      showOlderNotes: false,
      isDetailVisible: false,
      isFilterModalOpen: false,
      isAddNotesModalOpen: false,
      defaultTab: PageType.Notes,
      filters: {
        technology_ids: [],
        contact_ids: [],
        customer_ids: [],
        site_ids: [],
        opportunity_ids: [],
      },
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
    };

    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  componentDidMount() {
    if (this.props.location.pathname === "/client-360") {
      store.dispatch({
        type: LAST_VISITED_CLIENT_360_TAB,
        response: "Notes",
      });
    }

    if (this.props.customerId) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            customer_id: this.props.customerId,
          },
        },
        filters: {
          ...prevState.filters,
          customer_ids: [this.props.customerId],
        },
      }));
    }
    this.props.fetchServiceTypeSTT();
    const query = new URLSearchParams(this.props.location.search);
    const openNew = query.get("openNew");
    if (openNew) {
      if (openNew !== "new") {
        this.props.getNote(Number(openNew)).then((action) => {
          if (action.type === sowActionTypes.CREATE_NOTE_SUCCESS) {
            this.setState({
              id: openNew,
              isAddNotesModalOpen: true,
              salesNote: action.response,
            });
          }
        });
      } else {
        this.setState({ isAddNotesModalOpen: true });
      }
    }
  }

  componentDidUpdate(prevProps: INotesListProps) {
    if (
      this.props.customerId &&
      prevProps.customerId !== this.props.customerId
    ) {
      this.setState(
        (prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              customer_id: this.props.customerId,
            },
          },
          filters: {
            ...prevState.filters,
            customer_ids: [this.props.customerId],
          },
        }),
        () => this.onFiltersUpdate(this.state.filters)
      );
    }
    if (this.props.NotesList && this.props.NotesList !== prevProps.NotesList) {
      this.setRows(this.props);
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params?: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getNotesList(newParams);
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  getDataAfterUpdate = () => {
    this.fetchData({
      page: 1,
    });
  };

  setRows = (nextProps: INotesListProps) => {
    const customersResponse = nextProps.NotesList;
    const NotesList: ISalesNote[] = customersResponse.results;
    const rows: INoteRow[] = NotesList.map((salesNote, index) => ({
      body: salesNote.body,
      description: salesNote.description,
      created_on: salesNote.created_on,
      updated_on: salesNote.updated_on,
      salesNote: salesNote,
      last_updated_by: salesNote.last_updated_by,
      id: salesNote.id,
      customer: salesNote.customer,
      index,
    }));

    this.setState((prevState) => ({
      reset: false,
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onEditRowClick = (data: { original: INoteRow }) => {
    this.setState({
      id: String(data.original.id),
      isAddNotesModalOpen: true,
      salesNote: data.original.salesNote,
    });
  };

  onRowClick = (rowInfo: { original: INoteRow }) => {
    this.setState({
      id: String(rowInfo.original.id),
      isAddNotesModalOpen: true,
      salesNote: rowInfo.original.salesNote,
    });
  };

  getNotesDetails = () => {
    const catalog: any = this.props.NotesList.results[this.state.detailRow];
    const types = this.getAllTechnoType(true);
    const techNoTypes = [];
    catalog.technology_ids.map((t) =>
      techNoTypes.push(types && types.filter((ty) => t === ty.value))
    );

    const typeNames = [];
    techNoTypes.map((techType, i) =>
      typeNames.push(techType && techType[0] && techType[0].label)
    );
    catalog.technology_ids_names = typeNames;

    return catalog as INotes;
  };

  toggleAddNotesModalSearch = () => {
    this.setState((prevState) => ({
      isAddNotesModalOpen: !prevState.isAddNotesModalOpen,
      salesNote: null,
      defaultTab: PageType.Search,
      id: "",
    }));
  };

  renderTopBar = () => {
    return (
      <div className="notes-listing__actions">
        <div className="row-action header-panel">
          <div className="left">
            <Input
              field={{
                value: this.state.pagination.params.search,
                label: "",
                type: InputFieldType.SEARCH,
              }}
              width={11}
              name="searchString"
              onChange={this.onSearchStringChange}
              placeholder="Search"
              className="search"
            />
          </div>
          <div className="field-section actions-right">
            {this.props.customerId && (
              <SquareButton
                content={
                  <>
                    <span className="add-plus">+</span>
                    <span className="add-text">Add Note</span>
                  </>
                }
                onClick={this.toggleAddNotesModal}
                className="add-btn"
                bsStyle={ButtonStyle.OUTLINE}
              />
            )}
            <SquareButton
              onClick={() => this.toggleFilterModal(true)}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.NotesList &&
                this.props.NotesList.results.length === 0
              }
              bsStyle={ButtonStyle.PRIMARY}
            />
            <SquareButton
              onClick={this.toggleAddNotesModalSearch}
              content={"All Notes View"}
              disabled={
                this.props.NotesList &&
                this.props.NotesList.results.length === 0
              }
              className="all-notes-btn"
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.setState((prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              contact_ids: prevState.filters.contact_ids.join(),
              customer_id: prevState.filters.customer_ids.join(),
              site_ids: prevState.filters.site_ids.join(),
              technology_ids: prevState.filters.technology_ids.join(),
              opportunity_ids: prevState.filters.opportunity_ids.join(),
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  getAllTechnoType = (short: boolean = false) => {
    const types1 = [];

    if (this.props.serviceType) {
      this.props.serviceType.map(
        (cat) =>
          cat.technology_types &&
          cat.technology_types.map((t) =>
            types1.push({
              value: t.id,
              label: short ? `${t.name}` : `${cat.name}: ${t.name}`,
              disabled: false,
            })
          )
      );
    }
    return types1;
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      (!this.props.customerId && filters.customer_ids.length > 0) ||
      filters.contact_ids.length > 0 ||
      filters.site_ids.length > 0 ||
      filters.dateValue ||
      filters.technology_ids.length > 0
        ? true
        : false;

    return shouldRenderFilters ? (
      <div className="notes-filters-listing">
        <label>Applied Filters: </label>

        {!this.props.customerId && filters.customer_ids.length > 0 && (
          <div className="section-show-filters">
            <label>Customer: </label>
            <Select
              name="customer_ids"
              value={filters.customer_ids}
              onChange={this.onFilterChange}
              options={
                this.props.customersShort &&
                this.props.customersShort.map((role) => ({
                  value: role.id,
                  label: role.name,
                }))
              }
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.contact_ids.length > 0 && (
          <div className="section-show-filters">
            <label>Contact: </label>
            <Select
              name="contact_ids"
              value={filters.contact_ids}
              onChange={this.onFilterChange}
              options={
                this.props.users &&
                this.props.users.map((c) => ({
                  value: c.crm_id,
                  label: ` ${c.first_name}-${c.last_name} (${c.email})`,
                }))
              }
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.technology_ids.length > 0 && (
          <div className="section-show-filters ">
            <label>Technology: </label>
            <Select
              name="technology_ids"
              value={filters.technology_ids}
              onChange={this.onFilterChange}
              options={this.getAllTechnoType()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.opportunity_ids.length > 0 && (
          <div className="section-show-filters">
            <label>Technology: </label>
            <Select
              name="opportunity_ids"
              value={filters.opportunity_ids}
              onChange={this.onFilterChange}
              options={getValuesForSelect(this.props.qStageList, "id", "name")}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.site_ids.length > 0 && (
          <div className="section-show-filters ">
            <label>Site: </label>
            <Select
              name="site_ids"
              value={filters.site_ids}
              onChange={this.onFilterChange}
              options={getValuesForSelect(this.props.sites, "site_id", "name")}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.dateValue && (
          <div className="section-show-filters">
            <label>Date Range: </label>
            <div className="date-value" title={"MM-DD-YYYY"}>
              {fromISOStringToFormattedDate(
                this.state.filters.updated_on_0,
                "MM-DD-YYYY"
              )}
              {" to "}
              {fromISOStringToFormattedDate(
                this.state.filters.updated_on_1,
                "MM-DD-YYYY"
              )}
            </div>
          </div>
        )}
      </div>
    ) : null;
  };

  onDeleteRowClick(original: any) {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteNotes(Number(this.state.id)).then((action) => {
      if (action.type === sowActionTypes.CREATE_NOTE_SUCCESS) {
        this.setState({
          reset: true,
        });
        this.debouncedFetch({
          page: 1,
        });
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };

  toggleFilterModal = (show: boolean) => {
    this.setState({
      isFilterModalOpen: show,
    });
  };

  onFiltersUpdate = (filters: INotesFilters) => {
    if (filters) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            contact_ids: filters.contact_ids.join(),
            customer_id: filters.customer_ids.join(),
            site_ids: filters.site_ids.join(),
            technology_ids: filters.technology_ids.join(),
            opportunity_ids: filters.opportunity_ids.join(),
            updated_on_0: filters.updated_on_0,
            updated_on_1: filters.updated_on_1,
            dateValue: filters.dateValue,
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal(false);
    }
  };

  toggleAddNotesModal = () => {
    this.setState((prevState) => ({
      isAddNotesModalOpen: !prevState.isAddNotesModalOpen,
      salesNote: this.props.customerId
        ? {
            body: "",
            description: "",
            opportunity_ids: [],
            contact_ids: [],
            site_ids: [],
            technology_ids: [],
            markDownText: "",
            notes_object: "",
            customer: this.props.customerId,
          }
        : null,
      defaultTab: PageType.Notes,
      id: "",
    }));
  };

  closeDetailModal = () => {
    this.setState({ isDetailVisible: false, detailRow: null });
  };

  onCreateSOWClick = (id: string) => {
    this.props.history.push(`/sow/sow/0?template=${id}`);
  };

  render() {
    const columns: ITableColumn[] = [
      {
        accessor: "description",
        Header: "Description",
        id: "description",
        sortable: true,
        Cell: (c) => (
          <div title={c.original.body}>
            {(c.value && c.value.slice(0, 44)) || "N.A."}
          </div>
        ),
      },
      {
        accessor: "last_updated_by",
        Header: "Last Updated By",
        id: "last_updated_by",
        width: 200,
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "customer",
        Header: "Customer",
        id: "customer",
        width: 200,
        sortable: true,
        Cell: (cell) => (
          <div>
            {cell.value
              ? this.props.customersShort &&
                this.props.customersShort.find((c) => c.id === cell.value).name
              : " N.A."}
          </div>
        ),
      },
      {
        accessor: "created_on",
        Header: "Created On",
        id: "created_on",
        width: 140,
        sortable: true,
        Cell: (cell) => (
          <div>
            {cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."}
          </div>
        ),
      },
      {
        accessor: "updated_on",
        Header: "Updated on",
        id: "updated_on",
        width: 140,
        sortable: true,
        Cell: (cell) => (
          <div>
            {cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        width: 140,
        sortable: false,
        Cell: (cell) => (
          <div>
            <EditButton onClick={() => this.onEditRowClick(cell)} />
            <DeleteButton onClick={() => this.onDeleteRowClick(cell)} />
            {cell.original.linked_template && (
              <DeleteButton
                type="create"
                title="Create SOW"
                onClick={(e) =>
                  this.onCreateSOWClick(cell.original.linked_template)
                }
              />
            )}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: null,
    };

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.debouncedFetch,
      reset: this.state.reset,
    };

    // Remove customer column, for a single customer
    if (this.props.customerId) columns.splice(2, 1);

    return (
      <div className="notes-listing">
        {!this.props.customerId && (
          <div className="header">
            <h3>Notes</h3>
            <SquareButton
              content="Add Note"
              onClick={this.toggleAddNotesModal}
              className="save-mapping"
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetchingSCList} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetchingSCList ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingSCList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
        />
        <NotesFilter
          show={this.state.isFilterModalOpen}
          onClose={() => this.toggleFilterModal(false)}
          onSubmit={this.onFiltersUpdate}
          serviceType={this.props.serviceType}
          customersShort={this.props.customersShort}
          prevFilters={this.state.filters}
          singleCustomer={Boolean(this.props.customerId)}
        />
        {this.state.isAddNotesModalOpen && (
          <AddNotes
            show={this.state.isAddNotesModalOpen}
            onClose={this.toggleAddNotesModal}
            defaultTab={this.state.defaultTab || PageType.Notes}
            salesNote={this.state.salesNote}
            qStageList={this.props.qStageList}
            serviceType={this.props.serviceType}
            sites={this.props.sites}
            contacts={this.props.users}
            fetchData={this.getDataAfterUpdate}
            {...this.props}
          />
        )}
        {this.state.isDetailVisible && (
          <NotesDetails
            isVisible={this.state.isDetailVisible}
            close={this.closeDetailModal}
            Notes={this.getNotesDetails()}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  NotesList: state.sow.NotesList,
  isFetchingSCList: state.sow.isFetchingSCList,
  isFetching: state.sow.isFetching,
  qStageList: state.dashboard.quoteList,
  serviceType: state.setting.serviceType,
  sites: state.inventory.sites,
  users: state.customerUser.users,
  customersShort: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  getNotesList: (params?: IServerPaginationParams) =>
    dispatch(getNotesList(params)),
  deleteNotes: (id: number) => dispatch(deleteNotes(id)),
  getNote: (id: number) => dispatch(getNote(id)),
  fetchServiceTypeSTT: () => dispatch(fetchServiceTypeSTT()),
  fetchCustomerUsers: (id: number) =>
    dispatch(fetchCustomerUsers(id, { pagination: false })),
});

export default connect(mapStateToProps, mapDispatchToProps)(NotesListing);
