import React from "react";
import saveAs from "file-saver";
import "chartjs-plugin-datalabels";
import { connect } from "react-redux";
import { cloneDeep, debounce } from "lodash";
import * as XLSX from 'xlsx';
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Input from "../../../../components/Input/input";
import Table from "../../../../components/Table/table";
import {
  getSOWDocumentsLastUpdated,
  FETCH_SOW_DOC_LAST_UPDATED_SUCCESS,
  getSOWAuthors,
  getSOWByTechnologies,
  FETCH_SOW_BY_TECH_SUCCESS,
  FETCH_SOW_AUTHORS_SUCCESS,
  getSOWList,
  getTabledataSOWMetrics,
  getMarginData,
  FETCH_SOW_MARGIN_SUCCESS,
  downloadSOWDocumnet,
  DOWNLOAD_SOW_SUCCESS,
  EDIT_SOW_SUCCESS,
  sendEmailToAccountManager,
  GET_FULL_TABLE_SUCCESS,
  getFullSOWMetricsTable
} from "../../../../actions/sow";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import moment from "moment";
import { rawDoughnutChart } from "./DoughnutChart";
import EditButton from "../../../../components/Button/editButton";
import DeleteButton from "../../../../components/Button/deleteButton";
import IconButton from "../../../../components/Button/iconButton";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import SquareButton from "../../../../components/Button/button";
import htmlDocx from "html-docx-js/dist/html-docx";
import RenameBox from "../../../../components/RenameBox/RenameBox";
import "../../../../commonStyles/doughnut_chart.scss";
import "./style.scss";

enum PageType {
  FailedTickets,
  PurchaseOrders,
}
interface IDashboardProps extends ICommonProps {
  customerId: number;
  user: ISuperUser;
  addErrorMessage: any;
  addSuccessMessage: any;
  getTabledata: any;
  getFullTableData: any;
  isFetching: boolean;
  getSOWDocumentsLastUpdated: any;
  getSOWAuthors: any;
  getSOWByTechnologies: any;
  getSOWList: any;
  SOWList: any;
  isFetchingSowList: any;
  SOWMetricsTable: any;
  getMarginData: any;
  sendEmailToAccountManager: any;
  downloadSOWDocumnet: any;
}

interface IDashboardState {
  searchString: string;
  sowByTech: any[];
  sowDocumentsLastUpdated: any[];
  sowAuthors: any;
  marginData: any;
  loadingOpenPO: boolean;
  loadingTWO: boolean;
  loadingTHREE: boolean;
  loadingFOUR: boolean;
  loading: boolean;
  loadingExcelData: boolean;
  rows: any;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  loggedInUserOnly: boolean;
  currentPage: {
    pageType: PageType;
  };
  loadingSOWDocLastUpdated: boolean;
  loadingSOWAuthors: boolean;
  loadingSOWByTech: boolean;
  showOlderSOW: boolean;
  closed: boolean;
  showMarginOpen: string;
  downloadingIds: number[];
  downloadingPDFIds: number[];
  previewPDFIds: number[];
  emailSending: number[];
  openPreview: boolean;
  previewHTML: any;
  id: any;
  docType: string;
  documentName: string;
  document: any;
  isopenRename: boolean;
}

class SOWMetrics extends React.Component<IDashboardProps, IDashboardState> {
  constructor(props: IDashboardProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }
  private debouncedFetch;

  priorityColorMap = [
    "#4ba2c1",
    "#e55b7a",
    "#fac64d",
    "#5b9950",
    "#ba89f2",
    "#cdd5e1",
  ];

  estShipppedDatePriorityColorMap = [
    "#5b9950",
    "#fac64d",
    "#4ba2c1",
    "#e55b7a",
    "#ba89f2",
    "#cdd5e1",
  ];

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.PurchaseOrders,
    },
    rows: [],
    searchString: "",
    sowByTech: [{ label: "", value: 0, filter: false }],
    sowDocumentsLastUpdated: [],
    sowAuthors: [],
    marginData: [],
    loadingOpenPO: false,
    loadingTWO: false,
    loadingTHREE: false,
    loadingFOUR: false,
    loading: false,
    loadingExcelData: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: { page_size: 25 },
    },
    reset: false,
    loggedInUserOnly: false,
    loadingSOWDocLastUpdated: false,
    loadingSOWAuthors: false,
    loadingSOWByTech: false,
    showOlderSOW: false,
    closed: false,
    showMarginOpen: "Open",
    downloadingIds: [],
    downloadingPDFIds: [],
    previewPDFIds: [],
    emailSending: [],
    openPreview: false,
    previewHTML: null,
    id: "",
    docType: "",
    documentName: "",
    document: null,
    isopenRename: false,
  });
  componentDidMount() {
    this.loadGraphsData();
  }

  loadGraphsData = () => {
    this.getSOWDocumentsLastUpdated();
    this.getSOWAuthors();
    this.getSOWByTechnologies();
    this.getMarginGraphData();
  };

  componentDidUpdate(prevProps: IDashboardProps) {
    if (
      this.props.SOWMetricsTable &&
      prevProps.SOWMetricsTable !== this.props.SOWMetricsTable
    ) {
      this.setRows(this.props);
    }
  }

  getSOWDocumentsLastUpdated = () => {
    this.setState({ loadingSOWDocLastUpdated: true });
    const params = this.filterForAll();
    this.props.getSOWDocumentsLastUpdated(params).then((action) => {
      if (action.type === FETCH_SOW_DOC_LAST_UPDATED_SUCCESS) {
        const sowDocumentsLastUpdated = action.response.map((data) => ({
          value: data.count,
          label: `${data.days_range}`,
          filter: this.getFilterValueByName(
            this.state.sowDocumentsLastUpdated,
            data.days_range
          ),
        }));
        this.setState({ sowDocumentsLastUpdated });
      }
      this.setState({ loadingSOWDocLastUpdated: false });
    });
  };

  getSOWAuthors = () => {
    this.setState({ loadingSOWAuthors: true });
    const params = this.filterForAll();
    this.props.getSOWAuthors(params).then((action) => {
      if (action.type === FETCH_SOW_AUTHORS_SUCCESS) {
        const sowAuthors = action.response.map((data) => ({
          value: data.count,
          label: `${data.author__name}`,
          filter: this.getFilterValueByName(
            this.state.sowAuthors,
            data.author__name
          ),
          id: data.author_id,
        }));
        this.setState({ sowAuthors });
      }
      this.setState({ loadingSOWAuthors: false });
    });
  };

  getSOWByTechnologies = () => {
    const params = this.filterForAll();
    this.setState({ loadingSOWByTech: true });
    this.props.getSOWByTechnologies(params).then((action) => {
      if (action.type === FETCH_SOW_BY_TECH_SUCCESS) {
        const sowByTech = action.response.map((data) => ({
          value: data.margin,
          count: data.count,
          label: data.category,
          filter: this.getFilterValueByName(
            this.state.sowByTech,
            data.category
          ),
        }));
        this.setState({ sowByTech });
      }
      this.setState({ loadingSOWByTech: false });
    });
  };

  getMarginGraphData = () => {
    const params = this.filterForAll();
    this.setState({ loadingTHREE: true });
    this.props.getMarginData(params).then((action) => {
      if (action.type === FETCH_SOW_MARGIN_SUCCESS) {
        const res = action.response;
        const marginData = [];
        Object.keys(res).map((name) => {
          const t = res[name];
          Object.keys(t).map((y) => {
            if (this.state.showMarginOpen === name) {
              marginData.push({
                label: y,
                value: t[y],
                filter: this.getFilterValueByName(this.state.marginData, y),
                type: name,
              });
            }
          });
        });
        this.setState({ marginData });
      }
      this.setState({ loadingTHREE: false });
    });
  };

  getFilterValueByName = (list, value) => {
    const filterValue = list.find((x) => x.label === value);
    return filterValue ? filterValue.filter : false;
  };
  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.

    let filterParams = this.createFilterParams();
    const list = this.state.sowByTech.filter((x) => x.filter);
    const sowByTech = list && list[0] && { technology_type: list[0].label };

    const list1 = this.state.sowAuthors.filter((x) => x.filter);
    const sowAuthors = list1 && list1[0] && { author_id: list1[0].id };

    const newParams = {
      ...prevParams,
      ...params,
      ...filterParams,
      ...sowByTech,
      ...sowAuthors,
      // ...marginData,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getTabledata(newParams);
  };
  filterForAll = () => {
    let filterParams = this.createFilterParams();
    const list = this.state.sowByTech.filter((x) => x.filter);
    const sowByTech = list && list[0] && { technology_type: list[0].label };
    const list1 = this.state.sowAuthors.filter((x) => x.filter);
    const sowAuthors = list1 && list1[0] && { author_id: list1[0].id };

    return {
      ...filterParams,
      ...sowByTech,
      ...sowAuthors,
    };
  };
  getFilterParams = (list) => {
    const data = list.filter((x) => x.filter);
    const derivedData = data[0] && this.calculateDate(data[0].label);
    return derivedData;
  };

  createFilterParams = () => {
    const sowDocumentsLastUpdated = this.getFilterParams(
      this.state.sowDocumentsLastUpdated
    );
    const marginData = this.getFilterParams(this.state.marginData);
    const docStaus = {};
    if (marginData) {
      marginData["doc-status"] = this.state.showMarginOpen;
    } else {
      docStaus["doc-status"] = this.state.showMarginOpen;
    }
    return {
      ...sowDocumentsLastUpdated,
      ...marginData,
      ...docStaus,
    };
  };

  getDateBySubtract = (days, duration) => {
    const date = moment()
      .subtract(days, duration)
      .format("YYYY-MM-DD");
    return date;
  };

  calculateDate = (key) => {
    let to;
    let from;
    switch (key) {
      case "SOW Document updated within last 7 days":
        from = this.getDateBySubtract(7, "days");
        return {
          updated_after: from,
          updated_before: "",
        };

      case "SOW Document not updated over 7 days":
        from = this.getDateBySubtract(15, "days");
        to = this.getDateBySubtract(8, "days");
        return {
          updated_after: from,
          updated_before: to,
        };

      case "SOW Document not updated over 15 days":
        from = this.getDateBySubtract(45, "days");
        to = this.getDateBySubtract(16, "days");
        return {
          updated_after: from,
          updated_before: to,
        };

      case "SOW Document not updated over 45 days":
        to = this.getDateBySubtract(46, "days");
        return {
          updated_after: "",
          updated_before: to,
        };

      case "Last Quarter":
        from = this.getDateBySubtract(6, "months");
        to = moment()
          .subtract(3, "months")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
        return {
          updated_after: from,
          updated_before: to,
        };

      case "This Month":
        from = this.getDateBySubtract(1, "months");
        return {
          updated_after: from,
          updated_before: "",
        };

      case "This Quarter":
        from = this.getDateBySubtract(3, "months");
        return {
          updated_after: from,
          updated_before: "",
        };

      default:
        return { to, from };
    }
  };
  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
      page_size: this.state.pagination.params.page_size,
    });
  };

  setRows = (nextProps: IDashboardProps) => {
    const sowData = nextProps.SOWMetricsTable;
    const data: any[] = sowData && sowData.results;
    const rows: any[] = data;

    this.setState((prevState) => ({
      reset: false,
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: sowData.count,
        totalPages: Math.ceil(
          sowData.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };
  appliedFilterLength = () => {
    return (
      this.state.sowByTech.filter((x) => x.filter).length +
      this.state.sowAuthors.filter((x) => x.filter).length +
      this.state.marginData.filter((x) => x.filter).length +
      this.state.sowDocumentsLastUpdated.filter((x) => x.filter).length
    );
  };

  renderTopBar = () => {
    return (
      <div className={"dashboard-sow-metrics__table-top"}>
        <Input
          field={{
            label: "",
            type: InputFieldType.SEARCH,
            value: this.state.pagination.params.search,
            isRequired: false,
          }}
          width={12}
          placeholder="Search"
          name="searchString"
          onChange={this.onSearchStringChange}
          className="dashboard-sow-metrics__search"
        />
        <div className="total-count">
          <span>Total : </span>
          {this.props.SOWMetricsTable.count || 0}
          <span> | </span>
        </div>

        <div className="filters">
          <span>
            <img alt="" className="filter-img" src="/assets/icons/filter.png" />
            Applied Filters:
          </span>
          <div className="tiles">
            {this.filtersTiles(this.state.sowByTech, "sowByTech")}
            {this.filtersTiles(this.state.sowAuthors, "sowAuthors")}
            {this.filtersTiles(this.state.marginData, "marginData")}
            {this.filtersTiles(
              this.state.sowDocumentsLastUpdated,
              "sowDocumentsLastUpdated"
            )}
            {this.appliedFilterLength() === 0 && (
              <div className="no-filter-applied">No Filter Applied</div>
            )}
            {this.appliedFilterLength() > 1 && (
              <div
                className="filter-tile clear-filter-applied"
                onClick={(e) => {
                  const newstate = cloneDeep(this.state);
                  newstate.sowByTech.map((x) => (x.filter = false));
                  newstate.marginData.map((x) => (x.filter = false));
                  newstate.sowAuthors.map((x) => (x.filter = false));
                  // newstate.orderEstimatedShipDate.map(
                  //   (x) => (x.filter = false)
                  // );
                  newstate.sowDocumentsLastUpdated.map(
                    (x) => (x.filter = false)
                  );
                  const pagination = this.getEmptyState().pagination;
                  (newstate.pagination as any) = pagination;
                  this.setState(newstate, () => {
                    this.debouncedFetch({
                      page: 1,
                      page_size: this.state.pagination.params.page_size,
                    });
                    this.loadGraphsData();
                  });
                }}
              >
                Clear all
              </div>
            )}
          </div>
        </div>
        <div className="right-section">
          <SquareButton
            onClick={this.handleExportClick}
            className={"sow-export-btn"}
            content={
              <span>
                <img
                  src={
                    this.state.loadingExcelData
                      ? "/assets/icons/loading.gif"
                      : "/assets/icons/export.png"
                  }
                  alt="Downloading File"
                />
                Export
              </span>
            }
            bsStyle={ButtonStyle.PRIMARY}
            disabled={this.props.isFetching || this.state.loadingExcelData}
          />
        </div>
      </div>
    );
  };

  filtersTiles = (list, name) => {
    return (
      <>
        {list.map((x, ind) => {
          if (!x.filter) {
            return false;
          }
          return (
            <div className="filter-tile" title={x.count || x.value}>
              {x.label}{" "}
              <img
                className={"d-pointer icon-remove"}
                alt=""
                src={"/assets/icons/cross-sign.svg"}
                onClick={(e) => {
                  const newstate = cloneDeep(this.state);
                  const data = newstate[name];
                  data[ind].filter = false;
                  (newstate[name] as any) = data;
                  const pagination = this.getEmptyState().pagination;
                  pagination.params.page_size = this.state.pagination.params.page_size;
                  (newstate.pagination as any) = pagination;
                  this.setState(newstate, () => {
                    this.debouncedFetch({
                      page: 1,
                      page_size: this.state.pagination.params.page_size,
                    });
                    this.loadGraphsData();
                  });
                }}
              />
            </div>
          );
        })}
      </>
    );
  };

  onRowsToggle = (selectedRows) => {};
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  toggleOpenRename = () => {
    this.setState({
      isopenRename: !this.state.isopenRename,
    });
  };
  OnRenameAndDownload = (name: string) => {
    if (this.state.docType === "pdf") {
      this.downloadPDFReport(name, this.state.id);
      this.setState({ docType: "" });
    } else {
      saveAs(this.state.document, `${name}.docx`);
      this.setState({ docType: "" });
    }
  };

  onEditRowClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/sow/${id}`);
  };
  onCloneClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/sow/${id}?cloned=true`);
  };
  downloadPDFReport = (name: string, docId: any) => {
    this.setState({
      downloadingPDFIds: [...this.state.downloadingPDFIds, docId],
    });
    this.props.downloadSOWDocumnet(docId, "pdf", name).then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          isopenRename: false,
          id: "",
          docType: "",
          downloadingPDFIds: this.state.downloadingPDFIds.filter(
            (id) => docId !== id
          ),
        });
        const url = action.response.file_path;
        const link = document.createElement("a");
        link.href = url;
        link.target = "_blank";
        link.setAttribute("download", action.response.file_name);
        document.body.appendChild(link);
        link.click();
      }
    });
  };
  getStream = (doc: any) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, doc.id],
    });
    this.props.downloadSOWDocumnet(doc.id, "doc").then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        const sourceHTML = action.response;
        const converted = htmlDocx.asBlob(sourceHTML, {
          orientation: "portrait",
          margins: {
            top: 300,
            right: 1300,
            left: 1300,
            header: 400,
            footer: 200,
            bottom: 0,
          },
        });
        let updatedDate: string = moment(doc.updated_on).format("YYYY-MM-DD");
        let name = `${doc.customer.name} - ${doc.name} - Sow v${doc.version.replace(".", "-")} - ${updatedDate}.doc`;
        this.setState({
          documentName: name,
          document: converted,
          isopenRename: true,
          docType: "doc",
        });
        this.setState({
          downloadingIds: this.state.downloadingIds.filter(
            (id) => doc.id !== id
          ),
        });
      }
    });
  };

  onPDFPreviewClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, original.id],
    });
    this.props.downloadSOWDocumnet(original.id, "pdf", name).then((a) => {
      if (a.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          id: "",
          docType: "",
          previewPDFIds: this.state.previewPDFIds.filter(
            (id) => original.id !== id
          ),
        });
      }
    });
  }
  onEmailSending(id: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      emailSending: [...this.state.emailSending, id],
    });
    this.props.sendEmailToAccountManager(id).then((action) => {
      if (action.type === EDIT_SOW_SUCCESS) {
        this.setState({
          emailSending: this.state.emailSending.filter((i) => i !== id),
        });
        this.props.addSuccessMessage("Sending Email to account manager.");
      }
    });
  }

  handleExportClick = () => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 0
    });

    this.setState({ loadingExcelData: true });
    this.props
      .getFullTableData({
        ...this.state.pagination.params,
        page: 1,
        page_size: this.state.pagination.totalRows,
      })
      .then((action) => {
        if (action.type === GET_FULL_TABLE_SUCCESS) {
          const raw_data = action.response.results;
          const rows = raw_data.map((row) => ({
            customer: row.customer.name,
            name: row.name,
            author: row.author_name,
            technology: row.category_name,
            opportunity: row.quote ? row.quote.name : "",
            status: row.quote ? row.quote.status_name : "",
            margin: row.margin ? formatter.format(row.margin.toFixed()) : "",
            budget_hours: row.budget_hours ? row.budget_hours : "",
          }));
          const worksheet = XLSX.utils.json_to_sheet(rows);
          const workbook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(workbook, worksheet, "SOW Metrics");

          /* fix headers */
          XLSX.utils.sheet_add_aoa(
            worksheet,
            [
              [
                "Customer",
                "Name",
                "Author",
                "Technology",
                "Opportunity",
                "Status",
                "Margin",
                "Budget Hours",
              ],
            ],
            { origin: "A1" }
          );

          /* calculate each column width */
          const max_width_arr = rows.reduce(
            (w, r) => [
              Math.max(w[0], r.customer.length),
              Math.max(w[1], r.name.length),
              Math.max(w[2], r.author.length),
              Math.max(w[3], r.technology.length),
              Math.max(w[4], r.opportunity.length),
            ],
            [10, 10, 10, 10, 10]
          );

          worksheet["!cols"] = max_width_arr.map((el) => ({
            wch: el,
          }));
          XLSX.writeFile(
            workbook,
            `sow_metrics_${new Date().toISOString()}.xlsx`
          );
          this.setState({ loadingExcelData: false });
        } else this.setState({ loadingExcelData: false });
      })
      .catch(() => this.setState({ loadingExcelData: false }));
  };

  onPDFClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    let updatedDate: string = moment(original.updated_on).format("YYYY-MM-DD");
    let name = `${original.customer.name} - ${original.name} - Sow v${original.version.replace(".", "-")} - ${updatedDate}.pdf`;
    this.setState({
      isopenRename: !this.state.isopenRename,
      documentName: name,
      id: original.id,
      docType: "pdf",
    });
  }
  listingDashboard = () => {
    const getDOCXDownloadMarkUp = (cell) => {
      if (this.state.downloadingIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="docx_download"
            title="DOC Download"
            onClick={(e) => this.getStream(cell.original)}
          />
        );
      }
    };
    const getPDFPrevieMarkUp = (cell) => {
      if (this.state.previewPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_preview"
            title="Preview"
            onClick={(e) => this.onPDFPreviewClick(cell.original, e)}
          />
        );
      }
    };
    const getEmailsendingMarkUp = (cell) => {
      if (this.state.emailSending.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <IconButton
            icon="mail.svg"
            onClick={(e) => {
              this.onEmailSending(cell.original.id, e);
            }}
            className="version-btn"
            title={"Send email to account manager"}
          />
        );
      }
    };
    const getPDFDownloadMarkUp = (cell) => {
      if (this.state.downloadingPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_download"
            title="PDF Download"
            onClick={(e) => this.onPDFClick(cell.original, e)}
          />
        );
      }
    };

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 0
    });

    const columns: ITableColumn[] = [
      {
        accessor: "po_number",
        Header: "Customer",
        id: "name",
        sortable: false,
        Cell: (c) => (
          <div className="po-underline" title={c.original.customer.name}>
            {c.original.customer.name}
          </div>
        ),
      },
      {
        accessor: "Customer",
        Header: "Name",
        id: "name",
        sortable: true,
        Cell: (c) => <div>{c.original.name}</div>,
      },
      {
        accessor: "author_name",
        Header: "Author",
        id: "author_name",
        sortable: false,
        Cell: (c) => <div>{c.original.author_name}</div>,
      },
      {
        accessor: "category_name",
        Header: "Technology",
        id: "category_name",
        sortable: false,
        Cell: (c) => <div>{c.original.category_name}</div>,
      },
      {
        accessor: "po",
        Header: "Opportunity",
        id: "quote__name",
        sortable: false,
        Cell: (c) => <div>{c.original.quote && c.original.quote.name}</div>,
      },
      {
        accessor: "po",
        Header: "Updated On",
        id: "updated_on",
        width: 140,
        sortable: true,
        Cell: (c) => (
          <div>
            {fromISOStringToFormattedDate(
              c.original.updated_on,
              "MM/DD/YYYY h:mm A"
            )}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: "Budget Hours",
        id: "budget_hours",
        width: 90,
        sortable: true,
        Cell: (c) => (
          <div>
            {c.original.budget_hours}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: "Status",
        id: "eta",
        width: 130,
        sortable: false,
        Cell: (c) => (
          <div>{c.original.quote && c.original.quote.status_name}</div>
        ),
      },
      {
        accessor: "po",
        Header: "Margin",
        id: "margin",
        sortable: true,
        Cell: (c) => (
          <div title={c.original.margin}>
            {c.original.margin ? formatter.format(c.original.margin.toFixed()) : ""}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        width: 131,
        sortable: false,
        Cell: (cell) => (
          <div className="icons-template">
            <EditButton onClick={(e) => this.onEditRowClick(cell.value, e)} />
            <DeleteButton
              type="cloned"
              title="Clone document"
              onClick={(e) => this.onCloneClick(cell.value, e)}
            />
            {cell.original.quote !== null && getPDFPrevieMarkUp(cell)}
            {getEmailsendingMarkUp(cell)}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Download",
        sortable: false,
        width: 80,
        Cell: (cell) => (
          <div className="icons-template">
            {cell.original.quote === null && (
              <img
                title={"Please add Opportunity for this SOW"}
                className="quote"
                alt=""
                src="/assets/icons/warning.png"
              />
            )}
            {cell.original.doc_type !== "Change Request" &&
              cell.original.quote !== null &&
              getDOCXDownloadMarkUp(cell)}
            {cell.original.quote !== null && getPDFDownloadMarkUp(cell)}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: this.onRowsToggle,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
      defaultPageSize: 25,
    };
    return (
      <div className="sow-metrics-listing col-md-12">
        <Table
          columns={columns}
          rows={this.state.rows || []}
          manualProps={manualProps}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`rules-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          onRowClick={() => {}}
          defaultSorted={[
            {
              id: "",
              desc: true,
            },
          ]}
          loading={this.props.isFetching}
        />
      </div>
    );
  };

  tabs = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="failed-tickets__header">
        {
          <div
            className={`failed-tickets__header-link ${
              currentPage.pageType === PageType.PurchaseOrders ? "--active" : ""
            }`}
            onClick={() => this.changePage(PageType.PurchaseOrders)}
          >
            SOW's
          </div>
        }
      </div>
    );
  };
  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };
  handleChangeMargin = (e) => {
    const targetValue = e.target.checked;
    const newState = cloneDeep(this.state);
    (newState.showMarginOpen as any) = !targetValue ? "Open" : "Won";
    this.setState(newState, () => {
      this.debouncedFetch({
        page: 1,
        page_size: this.state.pagination.params.page_size,
      });
      this.loadGraphsData();
    });
  };
  render() {
    const currentPage = this.state.currentPage;
    return (
      <div className="sow-metrics">
        <div className="open-won-switch">
          Open{" "}
          <input
            className="switch"
            type="checkbox"
            onChange={(e) => this.handleChangeMargin(e)}
          />{" "}
          Won
        </div>
        {
          <div className="charts-container">
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.sowByTech,
                "sowByTech",
                this.state.loadingSOWByTech,
                "SoW's by Technology",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.sowAuthors,
                "sowAuthors",
                this.state.loadingSOWAuthors,
                "SoW's Author",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.marginData,
                "marginData",
                this.state.loadingTHREE,
                "Margin",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.sowDocumentsLastUpdated,
                "sowDocumentsLastUpdated",
                this.state.loadingSOWDocLastUpdated,
                "SOW Last Update",
                this.priorityColorMap
              )}
            </div>
          </div>
        }
        {this.tabs()}
        {currentPage.pageType === PageType.PurchaseOrders &&
          this.listingDashboard()}
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={ButtonStyle.DEFAULT}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
        <RenameBox
          show={this.state.isopenRename}
          onClose={this.toggleOpenRename}
          onSubmit={this.OnRenameAndDownload}
          name={this.state.documentName}
          isLoading={this.props.isFetching}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customerId: state.customer.customerId,
  SOWMetricsTable: state.sow.SOWMetricsTable,
  isFetching: state.sow.isFetching,
  SOWList: state.sow.sowList,
  isFetchingSowList: state.sow.isFetchingSowList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getFullTableData: (params?: IServerPaginationParams) =>
    dispatch(getFullSOWMetricsTable(params)),
  getTabledata: (params?: IServerPaginationParams) =>
    dispatch(getTabledataSOWMetrics(params)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  getSOWDocumentsLastUpdated: (params: any) =>
    dispatch(getSOWDocumentsLastUpdated(params)),
  getSOWAuthors: (params: any) => dispatch(getSOWAuthors(params)),
  getSOWByTechnologies: (params: any) => dispatch(getSOWByTechnologies(params)),
  getMarginData: (params: any) => dispatch(getMarginData(params)),
  getSOWList: (
    show: boolean,
    closed: boolean,
    params?: IServerPaginationParams
  ) => dispatch(getSOWList(show, closed, params)),
  sendEmailToAccountManager: (id: any) =>
    dispatch(sendEmailToAccountManager(id)),
  downloadSOWDocumnet: (id: any, type: string, name: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SOWMetrics);
