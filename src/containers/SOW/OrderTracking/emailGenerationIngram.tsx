import { cloneDeep } from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import "rc-color-picker/assets/index.css";
import Spinner from "../../../components/Spinner";

import {
  FETCH_POTICKETS_SUCCESS,
  getEmailTemplateByPOID,
  getListPurchaseOrdersAll,
  GET_P_O_ALL_SUCCESS,
} from "../../../actions/inventory";
import Input from "../../../components/Input/input";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import SquareButton from "../../../components/Button/button";
import { downloadfile } from "../../../utils/download";
import Checkbox from "../../../components/Checkbox/checkbox";
import "./style.scss";

enum PageType {
  Email,
  Receiving,
}
interface IEmailGenerationIngramState {
  currentPage: {
    pageType: PageType;
  };
  purchaseOrder: any[];
  htmlData: string;
  tablesCSVs: any[];
  loading: boolean;
  htmlContextData: any;
  showShippedQty: boolean;
  isContentCopied: boolean;
  isCopyStateActive: boolean;
}

interface IEmailGenerationIngramProps extends ICommonProps {
  customerId: any;
  getListPurchaseOrdersAll: any;
  isFetching: boolean;
  purchaseOrdersAll: any[];
  getEmailTemplateByPOID: any;
  addSuccessMessage: any;
  addErrorMessage: any;
}

class EmailGenerationIngram extends Component<
  IEmailGenerationIngramProps,
  IEmailGenerationIngramState
> {
  constructor(props: IEmailGenerationIngramProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Email,
    },
    purchaseOrder: [],
    loading: false,
    htmlData: ``,
    tablesCSVs: null,
    htmlContextData: {},
    showShippedQty: false,
    isContentCopied: false,
    isCopyStateActive: false,
  });

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);

    this.props.getListPurchaseOrdersAll().then((a) => {
      if (a.type === GET_P_O_ALL_SUCCESS) {
        const purchaseOrderId = query.get("po-number");
        if (purchaseOrderId) {
          this.handleChange("purchaseOrder", [purchaseOrderId]);
        }
      }
    });
    const tab = query.get("tab");
    if (tab) {
      this.changePage(parseInt(tab));
    }
  }
  changePage = (pageType: PageType) => {
    const newstate = this.getEmptyState();
    newstate.currentPage.pageType = pageType;
    this.setState(newstate);
    this.props.history.push(`?tab=${pageType}`);
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="report__header">
        <div
          className={`report__header-link ${
            currentPage.pageType === PageType.Email
              ? "report__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Email)}
        >
          Email Generation
        </div>
        <div
          className={`report__header-link ${
            currentPage.pageType === PageType.Receiving
              ? "report__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Receiving)}
        >
          Receiving
        </div>
      </div>
    );
  };

  renderOrderInfo = (hardware_products, licences, settings) => {
    return (
      <div className="hardware-products-info">
        {hardware_products &&
          hardware_products.map((instance, idx) => (
            <div id="hardware-content" key={idx}>
              <p>
                <u>Shipping Destination</u>:<br></br>
              </p>
              <p>{instance.ship_to_address.addressline1}</p>
              <p>{instance.ship_to_address.addressline2}</p>
              <p>{instance.ship_to_address.addressline3}</p>
              <p>
                {instance.ship_to_address.city},{" "}
                {instance.ship_to_address.state}{" "}
                {instance.ship_to_address.postalcode}
              </p>
              <br></br>
              {(instance.line_items.length > 0 || licences[0].licences > 0) && (
                <table>
                  <tbody>
                    <tr>
                      <td style={{ width: "25% " }}>
                        <p
                          style={{
                            textAlign: "center",
                          }}
                        >
                          <strong>Part Number</strong>
                        </p>
                      </td>
                      <td>
                        <p>
                          <strong>Qty.</strong>
                        </p>
                      </td>
                      {this.state.showShippedQty && (
                        <td>
                          <p>
                            <strong>Shipped Qty.</strong>
                          </p>
                        </td>
                      )}
                      <td>
                        <p>
                          <strong>Tracking Number</strong>
                        </p>
                      </td>
                      <td>
                        <p>
                          <strong>Carrier</strong>
                        </p>
                      </td>
                      <td>
                        <p>
                          <strong>Status</strong>
                        </p>
                      </td>
                    </tr>
                    {instance.line_items &&
                      instance.line_items.map((product, idx) => (
                        <tr key={idx}>
                          <td>
                            <p>{product.part_number}</p>
                          </td>
                          {product.shipped_quantity <
                          product.purchased_quantity ? (
                            <>
                              <td style={{ background: "#ff000033" }}>
                                <p>{product.purchased_quantity}</p>
                              </td>
                              {this.state.showShippedQty && (
                                <td style={{ background: "#ff000033" }}>
                                  <p>{product.shipped_quantity}</p>
                                </td>
                              )}
                            </>
                          ) : (
                            <>
                              <td>
                                <p>{product.purchased_quantity}</p>
                              </td>
                              {this.state.showShippedQty && (
                                <td>
                                  <p>{product.shipped_quantity}</p>
                                </td>
                              )}
                            </>
                          )}
                          <td>
                            {product.tracking_numbers ? (
                              product.tracking_numbers
                                .split(", ")
                                .map((tracking_number: string, idx: number) => (
                                  <p
                                    key={idx}
                                    dangerouslySetInnerHTML={{
                                      __html: tracking_number,
                                    }}
                                  />
                                ))
                            ) : (
                              <p />
                            )}
                          </td>
                          <td>
                            <p>{product.carriers}</p>
                          </td>
                          <td>
                            <p>{product.ship_date}</p>
                          </td>
                          {/* <td>
                                                    <p>{product.status}</p>
                                                </td> */}
                        </tr>
                      ))}
                    {licences[0].licences &&
                      licences[0].licences.map((product, idx) => (
                        <tr key={idx}>
                          <td>
                            <p>{product.part_number}</p>
                          </td>
                          {product.shipped_quantity <
                          product.purchased_quantity ? (
                            <>
                              <td style={{ background: "#ff000033" }}>
                                <p>{product.purchased_quantity}</p>
                              </td>
                              {this.state.showShippedQty && (
                                <td style={{ background: "#ff000033" }}>
                                  <p>{product.shipped_quantity}</p>
                                </td>
                              )}
                            </>
                          ) : (
                            <>
                              <td>
                                <p>{product.purchased_quantity}</p>
                              </td>
                              {this.state.showShippedQty && (
                                <td>
                                  <p>{product.shipped_quantity}</p>
                                </td>
                              )}
                            </>
                          )}
                          <td>
                            {product.tracking_numbers ? (
                              product.tracking_numbers
                                .split(", ")
                                .map((tracking_number: string, idx: number) => (
                                  <p
                                    key={idx}
                                    dangerouslySetInnerHTML={{
                                      __html: tracking_number,
                                    }}
                                  />
                                ))
                            ) : (
                              <p />
                            )}
                          </td>
                          <td>
                            <p>{product.carriers}</p>
                          </td>
                          <td>
                            <p>{product.ship_date}</p>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              )}
              <br></br>
              {instance.line_items.length > 0 && !this.state.isCopyStateActive && (
                <div id="hardware-action-buttons" className="action-row-table">
                  <SquareButton
                    onClick={(e) => downloadfile(this.state.tablesCSVs[0])}
                    content={`Download Table`}
                    bsStyle={ButtonStyle.PRIMARY}
                    title={`Download File`}
                  />
                  {!this.state.isCopyStateActive && (
                    <Checkbox
                      isChecked={this.state.showShippedQty}
                      name="showShippedQty"
                      className="show-shipped-checkbox"
                      onChange={(e) =>
                        this.setState({ showShippedQty: e.target.checked })
                      }
                    >
                      <div className="bulk-detail-title">
                        Show Shipped Qty column
                      </div>
                    </Checkbox>
                  )}
                </div>
              )}
              {((licences[0].contracts && licences[0].contracts.length > 0) ||
                (instance.contracts && instance.contracts.length > 0)) && (
                <>
                  <div id="contract-content" className="contract-content-info">
                    <div
                      dangerouslySetInnerHTML={{
                        __html: settings.email_settings.smartnet_intro,
                      }}
                    />
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <p>
                              <strong>Part Number</strong>
                            </p>
                          </td>

                          <td>
                            <p>
                              <strong>Status </strong>
                            </p>
                          </td>
                        </tr>
                        {instance.contracts &&
                          instance.contracts.map((contract, idx) => (
                            <tr key={idx}>
                              <td>
                                <p>{contract.part_number}</p>
                              </td>

                              <td>
                                <p>{contract.status}</p>
                              </td>
                            </tr>
                          ))}
                        {licences[0].contracts &&
                          licences[0].contracts.map((contract, idx) => (
                            <tr key={idx}>
                              <td>
                                <p>{contract.part_number}</p>
                              </td>

                              <td>
                                <p>{contract.status}</p>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                    <br></br>
                  </div>
                  {!this.state.isCopyStateActive && (
                    <SquareButton
                      onClick={(e) => downloadfile(this.state.tablesCSVs[1])}
                      content={`Download Table`}
                      bsStyle={ButtonStyle.PRIMARY}
                      className="download-button"
                      title={`Download File`}
                    />
                  )}
                </>
              )}
            </div>
          ))}
      </div>
    );
  };

  renderLicensesInfo = (licences, settings) => {
    return (
      <div className="licneses-info">
        {licences &&
          licences.map(
            (licence) =>
              licence.licences &&
              licence.licences.length > 0 && (
                <>
                  <div id="license-content">
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <p>
                              <strong>Part Number</strong>
                            </p>
                          </td>
                          <td>
                            <p>
                              <strong>Purchased Qty.</strong>
                            </p>
                          </td>
                          <td>
                            <p>
                              <strong>Shipped Qty.</strong>
                            </p>
                          </td>
                          <td>
                            <p>
                              <strong>Status</strong>
                            </p>
                          </td>
                        </tr>
                        {licence.licences.map((each_licence) => (
                          <tr>
                            <td>
                              <p>{each_licence.part_number}</p>
                            </td>
                            {each_licence.shipped_quantity <
                            each_licence.purchased_quantity ? (
                              <>
                                <td>
                                  <p style={{ background: "indianred" }}>
                                    {each_licence.purchased_quantity}
                                  </p>
                                </td>
                                <td>
                                  <p style={{ background: "indianred" }}>
                                    {each_licence.shipped_quantity}
                                  </p>
                                </td>
                              </>
                            ) : (
                              <>
                                <td>
                                  <p>{each_licence.purchased_quantity}</p>
                                </td>
                                <td>
                                  <p>{each_licence.shipped_quantity}</p>
                                </td>
                                <td>
                                  <p>{each_licence.pak_number}</p>
                                </td>
                              </>
                            )}
                          </tr>
                        ))}
                      </tbody>
                    </table>
                    <br></br>
                  </div>
                  {!this.state.isCopyStateActive && (
                    <SquareButton
                      onClick={(e) => downloadfile(this.state.tablesCSVs[1])}
                      content={`Download Table`}
                      bsStyle={ButtonStyle.PRIMARY}
                      className="download-button"
                      title={`Download File`}
                    />
                  )}
                </>
              )
          )}
      </div>
    );
  };

  renderEmailGenerationContent = () => {
    const {
      cc_contacts,
      hardware_products,
      licences,
      name,
      settings,
      to_contacts_emails,
    } = this.state.htmlContextData;

    return (
      <div className="html-section col-md-12">
        {this.state.htmlData && (
          <SquareButton
            onClick={(e) =>
              this.copyContentToClipboard("email-generation-content")
            }
            content={`${
              this.state.isContentCopied ? "Copied!" : "Copy to clipboard"
            }`}
            bsStyle={ButtonStyle.PRIMARY}
            className="copy-to-clipboard"
            title={`Copy to clipboard`}
          />
        )}
        {this.state.htmlData ? (
          <div id="email-generation-content" className={`html-data`}>
            <div id="introduction-content">
              <p>{`Hello ${name}`},</p>
              <div
                className={``}
                dangerouslySetInnerHTML={{
                  __html: settings.email_settings.email_intro,
                }}
              />
              <p>
                <b>
                  <u>
                    <span style={{ fontSize: "14.0pt", color: "#3399ff" }}>
                      ORDER INFORMATION
                    </span>
                  </u>
                </b>
              </p>
            </div>
            {this.renderOrderInfo(hardware_products, licences, settings)}
            {/* {this.renderLicensesInfo(licences, settings)} */}
            <br></br>
            <div
              className={``}
              dangerouslySetInnerHTML={{
                __html: settings.email_settings.ending_paragraph,
              }}
            />
            <div
              id="to-content"
              style={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "centerwrap",
              }}
            >
              TO: &nbsp;
              {to_contacts_emails &&
                to_contacts_emails.map((email, idx) => (
                  <div
                    key={idx}
                    style={{
                      padding: "3px",
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    <div style={{ marginLeft: "5px", color: "#000000" }}>
                      {email}
                    </div>
                  </div>
                ))}
            </div>
            <div
              id="cc-content"
              style={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "centerwrap",
              }}
            >
              CC: &nbsp;
              {cc_contacts &&
                cc_contacts.map((contact) => (
                  <div
                    style={{
                      padding: "3px",
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    <div style={{ marginLeft: "5px", color: "#000000" }}>
                      {contact.email}
                    </div>
                  </div>
                ))}
            </div>
          </div>
        ) : (
          <div className="no-data">
            {this.state.loading ? (
              "loading..."
            ) : this.state.purchaseOrder.length === 0 ? (
              <p>
                <img
                  src={`/assets/icons/l-HIGH.svg`}
                  alt=""
                  className="status-images"
                  title={``}
                />
                Please select Purchase Order
              </p>
            ) : (
              "No data"
            )}
          </div>
        )}
      </div>
    );
  };

  renderEmailGeneration = () => {
    return (
      <div className="email-generation-body col-md-12">
        <Input
          field={{
            label: "Purchase Order",
            type: InputFieldType.PICKLIST,
            value: this.state.purchaseOrder,
            isRequired: true,
            options: this.props.purchaseOrdersAll.map((d) => {
              return {
                label: `${d.poNumber} (${(d.customerCompany &&
                  d.customerCompany.name) ||
                  ""})`,
                value: d.poNumber,
              };
            }),
          }}
          loading={this.props.isFetching}
          width={6}
          placeholder="Select Purchase Order"
          name="purchaseOrder"
          className="purchaseOrder-select"
          onChange={(e) => this.handleChange(e.target.name, e.target.value)}
          multi={true}
        />
        {this.renderEmailGenerationContent()}
      </div>
    );
  };

  handleChange = (name: any, value: any): void => {
    const newState = cloneDeep(this.state);
    newState[name] = value;
    (newState.loading as boolean) = true;
    (newState.htmlData as any) = "";
    (newState.tablesCSVs as any) = null;
    this.setState(newState);
    if (value.length > 0) {
      const poData = this.props.purchaseOrdersAll.find(
        (c) => c.poNumber === value[0]
      );
      const customerContact = poData && poData.customerContact.name;
      const poCustomerId = poData && poData.customerCompany.id;
      const toContactId = poData && poData.customerContact.id;
      const poIds = value.map((x) => {
        const all = this.props.purchaseOrdersAll.find((c) => c.poNumber === x);
        return all && all.id;
      });

      this.props
        .getEmailTemplateByPOID(
          value,
          poIds,
          customerContact,
          poCustomerId,
          toContactId
        )
        .then((action) => {
          if (action.type === FETCH_POTICKETS_SUCCESS) {
            this.setState({
              htmlContextData: action.response.template_data,
              htmlData: action.response.template_html,
              tablesCSVs: action.response.table_csvs,
              loading: false,
            });
          } else {
            this.setState({ loading: false });
            this.props.addErrorMessage(
              "No data found for selected Purchase orders"
            );
          }
        });
    } else {
      this.setState({ loading: false });
    }
  };

  copyContentToClipboard = (containerid = "email-generation-content") => {
    this.setState({ isCopyStateActive: true });

    setTimeout(() => {
      if (window.getSelection) {
        let selection = window.getSelection();
        var range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        selection.removeAllRanges();
        selection.addRange(range);
        document.execCommand("copy");

        this.setState({
          isContentCopied: true,
        });

        setTimeout(
          () =>
            this.setState({
              isContentCopied: false,
              isCopyStateActive: false,
            }),
          2000
        );
      }
    }, 750);
  };

  render() {
    return (
      <div className="cr-container report">
        <div className="loader">
          <Spinner show={this.state.loading} />
        </div>
        {this.renderEmailGeneration()}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  purchaseOrdersAll: state.inventory.purchaseOrdersAll,
  customerId: state.customer.customerId,
  isFetching: state.inventory.isPostingBatch,
});

const mapDispatchToProps = (dispatch: any) => ({
  getListPurchaseOrdersAll: () => dispatch(getListPurchaseOrdersAll()),
  getEmailTemplateByPOID: (
    poNumbers: number[],
    ids: number[],
    customerContact: any,
    customerId: string,
    toContactId: number
  ) =>
    dispatch(
      getEmailTemplateByPOID(
        poNumbers,
        ids,
        customerContact,
        customerId,
        toContactId
      )
    ),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmailGenerationIngram);
