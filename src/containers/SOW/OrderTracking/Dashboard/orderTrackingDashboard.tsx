import React from "react";
import { Doughnut } from "react-chartjs-2";
import "chartjs-plugin-datalabels";
import { connect } from "react-redux";
import { cloneDeep, debounce, DebouncedFunc } from "lodash";
import Input from "../../../../components/Input/input";
import Spinner from "../../../../components/Spinner";
import Table from "../../../../components/Table/table";
import {
  getTabledata,
  getOrderEstimatedShipDate,
  getLastCustomerUpdatedData,
  getOpenPOByPOBYDistributor,
  getOrderShippedNotReceived,
  FETCH_OPEN_PO_SUCCESS,
  FETCH_SHIPPED_SUCCESS,
  FETCH_ESTIMATED_SUCCESS,
  FETCH_LAST_CUST_DATA_SUCCESS,
} from "../../../../actions/orderTracking";
import { fromISOStringToFormattedDate } from "./../../../../utils/CalendarUtil";
import moment from "moment";
import Checkbox from "../../../../components/Checkbox/checkbox";
import { rawDoughnutChart } from "./DoughnutChart";
import TooltipCustom from "../../../../components/Tooltip/tooltip";
import "../../../../commonStyles/doughnut_chart.scss";
import "./style.scss";

interface IDashboardProps extends ICommonProps {
  getOpenPOByPOBYDistributor: any;
  isFetchingComplianceDashboardData: boolean;
  complianceDashboardData: {} | any;
  customerId: number;
  user: ISuperUser;
  fetchCustomerComplianceDashboardData: any;
  getTabledata: any;
  orderTrackingDashboardData: any;
  getLastCustomerUpdatedData: any;
  getOrderShippedNotReceived: any;
  getOrderEstimatedShipDate: any;
  isFetching: boolean;
}

interface IDashboardState {
  searchString: string;
  openPOsDistributors: any[];
  orderEstimatedShipDate: any[];
  lastCustomerUpdate: any[];
  orderShippedNotReceived: any[];
  loadingOpenPO: boolean;
  loadingTWO: boolean;
  loadingTHREE: boolean;
  loadingFOUR: boolean;
  loading: boolean;
  rows: any;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  loggedInUserOnly: boolean;
}

class OrderTRDashboard extends React.Component<
  IDashboardProps,
  IDashboardState
> {
  private debouncedFetch: DebouncedFunc<
    (params: IServerPaginationParams) => void
  >;

  constructor(props: IDashboardProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  priorityColorMap = [
    "#4ba2c1",
    "#e55b7a",
    "#fac64d",
    "#5b9950",
    "#ba89f2",
    "#cdd5e1",
  ];

  estShipppedDatePriorityColorMap = [
    "#5b9950",
    "#fac64d",
    "#4ba2c1",
    "#e55b7a",
    "#ba89f2",
    "#cdd5e1",
  ];

  getEmptyState = () => ({
    rows: [],
    searchString: "",
    openPOsDistributors: [{ label: "", value: 0, filter: false }],
    orderEstimatedShipDate: [],
    lastCustomerUpdate: [],
    orderShippedNotReceived: [],
    loadingOpenPO: false,
    loadingTWO: false,
    loadingTHREE: false,
    loadingFOUR: false,
    loading: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: { page_size: 25 },
    },
    reset: false,
    loggedInUserOnly: false,
  });

  componentDidMount() {
    this.loadGraphsData();
  }

  loadGraphsData = () => {
    this.getOpenPOByPOBYDistributor();
    this.getLastCustomerUpdatedData();
    this.getOrderShippedNotReceived();
    this.getOrderEstimatedShipDate();
  };

  componentDidUpdate(prevProps: IDashboardProps) {
    if (
      this.props.orderTrackingDashboardData &&
      prevProps.orderTrackingDashboardData !==
        this.props.orderTrackingDashboardData
    ) {
      this.setRows(this.props);
    }
  }

  getOpenPOByPOBYDistributor = () => {
    this.setState({ loadingOpenPO: true });
    this.props
      .getOpenPOByPOBYDistributor(this.state.loggedInUserOnly)
      .then((action) => {
        if (action.type === FETCH_OPEN_PO_SUCCESS) {
          const openPOsDistributors = action.response.map((data) => ({
            value: data.count,
            label: `${data.vendor_company_name}`,
            filter: false,
          }));
          this.setState({ openPOsDistributors });
        }
        this.setState({ loadingOpenPO: false });
      });
  };

  getLastCustomerUpdatedData = () => {
    this.setState({ loadingFOUR: true });
    this.props
      .getLastCustomerUpdatedData(this.state.loggedInUserOnly)
      .then((action) => {
        if (action.type === FETCH_LAST_CUST_DATA_SUCCESS) {
          const lastCustomerUpdate = action.response.map((data) => ({
            value: data.count,
            label: `${data.day_range}`,
            filter: false,
          }));
          this.setState({ lastCustomerUpdate });
        }
        this.setState({ loadingFOUR: false });
      });
  };

  getOrderShippedNotReceived = () => {
    this.setState({ loadingTWO: true });
    this.props
      .getOrderShippedNotReceived(this.state.loggedInUserOnly)
      .then((action) => {
        if (action.type === FETCH_SHIPPED_SUCCESS) {
          const orderShippedNotReceived = action.response.map((data) => ({
            value: data.shipped_but_not_received_count,
            label: `Shipped but not received count`,
            filter: false,
          }));
          this.setState({ orderShippedNotReceived });
        }
        this.setState({ loadingTWO: false });
      });
  };

  getOrderEstimatedShipDate = () => {
    this.setState({ loadingTHREE: true });
    this.props
      .getOrderEstimatedShipDate(this.state.loggedInUserOnly)
      .then((action) => {
        if (action.type === FETCH_ESTIMATED_SUCCESS) {
          const orderEstimatedShipDate = action.response.map((data) => ({
            value: data.count,
            label: `${data.shipped_range}`,
            filter: false,
          }));
          this.setState({ orderEstimatedShipDate });
        }
        this.setState({ loadingTHREE: false });
      });
  };

  getTotal = (array: number[]) => {
    let total = 0;
    total = array.length && array.reduce((a, b) => (a || 0) + (b || 0));
    return total ? total : null;
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.

    let filterParams = this.createFilterParams();
    const list = this.state.openPOsDistributors.filter((x) => x.filter);
    const openPOsDistributors = list &&
      list[0] && { vendor_company_name: list[0].label };
    const newParams = {
      ...prevParams,
      ...params,
      ...filterParams,
      ...openPOsDistributors,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getTabledata(this.state.loggedInUserOnly, newParams);
  };

  getFilterParams = (list) => {
    const data = list.filter((x) => x.filter);
    const derivedData = data[0] && this.calculateDate(data[0].label);
    return derivedData;
  };

  createFilterParams = () => {
    const orderShippedNotReceived = this.getFilterParams(
      this.state.orderShippedNotReceived
    );
    const orderEstimatedShipDate = this.getFilterParams(
      this.state.orderEstimatedShipDate
    );
    const lastCustomerUpdate = this.getFilterParams(
      this.state.lastCustomerUpdate
    );

    return {
      ...orderShippedNotReceived,
      ...orderEstimatedShipDate,
      ...lastCustomerUpdate,
    };
  };

  getDateBySubtract = (days) => {
    const date = moment()
      .subtract(days, "days")
      .format("YYYY-MM-DD");
    return date;
  };

  getDateByAdd = (days) => {
    const date = moment()
      .add(days, "days")
      .format("YYYY-MM-DD");
    return date;
  };

  calculateDate = (key) => {
    let to;
    let from;
    // after = from
    // before = to
    switch (key) {
      case "Updated within 7 days":
        from = this.getDateBySubtract(7);
        to = this.getDateBySubtract(0);
        //connectwise_last_data_update__after = last_7_days
        return {
          connectwise_last_data_update_after: from,
          connectwise_last_data_update_before: to,
        };

      case "Tickets not updated over 7 days":
        from = this.getDateBySubtract(14);
        to = this.getDateBySubtract(7);
        //connectwise_last_data_update__before=last_7_days and connectwise_last_data_update__after=last_14_days
        return {
          connectwise_last_data_update_after: from,
          connectwise_last_data_update_before: to,
        };

      case "Tickets not updated over 14 days":
        from = this.getDateBySubtract(30);
        to = this.getDateBySubtract(14);
        //connectwise_last_data_update__before=last_14_days and connectwise_last_data_update__after=last_30_days
        return {
          connectwise_last_data_update_after: from,
          connectwise_last_data_update_before: to,
        };
      case "Tickets not updated over 30 days":
        to = this.getDateBySubtract(30);
        return {
          connectwise_last_data_update_after: "",
          connectwise_last_data_update_before: to,
          //connectwise_last_data_update__before=last_30_days
        };
      case "30 Days":
        from = this.getDateByAdd(0);
        to = this.getDateByAdd(30);
        return {
          ingram_eta_after: from,
          ingram_eta_before: to,
          ingram_eta_is_null: undefined,
        };

      case "60 Days":
        from = this.getDateByAdd(30);
        to = this.getDateByAdd(60);
        return {
          ingram_eta_after: from,
          ingram_eta_before: to,
          ingram_eta_is_null: undefined,
        };

      case "90 Days":
        from = this.getDateByAdd(60);
        to = this.getDateByAdd(90);
        return {
          ingram_eta_after: from,
          ingram_eta_before: to,
          ingram_eta_is_null: undefined,
        };

      case "Over 90 days":
        from = this.getDateByAdd(90);
        to = "";
        return {
          ingram_eta_after: from,
          ingram_eta_before: to,
          ingram_eta_is_null: undefined,
        };
      case "Current Week":
        from = moment()
          .clone()
          .startOf("isoWeek")
          .format("YYYY-MM-DD");
        to = moment()
          .clone()
          .endOf("isoWeek")
          .format("YYYY-MM-DD");
        return {
          ingram_eta_after: from,
          ingram_eta_before: to,
          ingram_eta_is_null: undefined,
        };
      case "No ETA Available":
        return {
          ingram_eta_after: undefined,
          ingram_eta_before: undefined,
          ingram_eta_is_null: "True",
        };

      case "Shipped but not received count":
        return { "shipped-not-received": true };

      default:
        return { to, from };
    }
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: IDashboardProps) => {
    const customersResponse = nextProps.orderTrackingDashboardData;
    const rows: any[] = customersResponse.results;

    this.setState((prevState) => ({
      reset: false,
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  appliedFilterLength = () => {
    return (
      this.state.openPOsDistributors.filter((x) => x.filter).length +
      this.state.orderEstimatedShipDate.filter((x) => x.filter).length +
      this.state.orderShippedNotReceived.filter((x) => x.filter).length +
      this.state.lastCustomerUpdate.filter((x) => x.filter).length
    );
  };

  renderTopBar = () => {
    return (
      <div className={"dashboard-order-tracking__table-top"}>
        <Input
          field={{
            label: "",
            type: InputFieldType.SEARCH,
            value: this.state.pagination.params.search,
            isRequired: false,
          }}
          width={12}
          placeholder="Search"
          name="searchString"
          onChange={this.onSearchStringChange}
          className="dashboard-order-tracking__search"
        />
        <div className="total-count">
          <span>Total : </span>
          {this.props.orderTrackingDashboardData.count || 0}
          <span> | </span>
        </div>

        <div className="filters">
          <span className="filter__label">
            <img alt="" className="filter-img" src="/assets/icons/filter.png" />
            Applied Filters:
          </span>
          <div className="tiles">
            {this.filtersTiles(
              this.state.openPOsDistributors,
              "openPOsDistributors"
            )}
            {this.filtersTiles(
              this.state.orderEstimatedShipDate,
              "orderEstimatedShipDate"
            )}
            {this.filtersTiles(
              this.state.lastCustomerUpdate,
              "lastCustomerUpdate"
            )}
            {this.filtersTiles(
              this.state.orderShippedNotReceived,
              "orderShippedNotReceived"
            )}
            {this.appliedFilterLength() === 0 && (
              <div className="no-filter-applied">No Filter Applied</div>
            )}
            {this.appliedFilterLength() > 1 && (
              <div
                className="filter-tile clear-filter-applied"
                onClick={(e) => {
                  const newstate = cloneDeep(this.state);
                  newstate.openPOsDistributors.map((x) => (x.filter = false));
                  newstate.orderShippedNotReceived.map(
                    (x) => (x.filter = false)
                  );
                  newstate.orderEstimatedShipDate.map(
                    (x) => (x.filter = false)
                  );
                  newstate.lastCustomerUpdate.map((x) => (x.filter = false));
                  const pagination = this.getEmptyState().pagination;
                  (newstate.pagination as any) = pagination;
                  this.setState(newstate, () => {
                    this.debouncedFetch({
                      page: 1,
                    });
                  });
                }}
              >
                Clear all
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  filtersTiles = (list, name: string) => {
    return (
      <>
        {list.map((x, ind) => {
          if (!x.filter) {
            return false;
          }
          return (
            <div className="filter-tile">
              {x.label}{" "}
              <img
                className={"d-pointer icon-remove"}
                alt=""
                src={"/assets/icons/cross-sign.svg"}
                onClick={(e) => {
                  const newstate = cloneDeep(this.state);
                  const data = newstate[name];
                  data[ind].filter = false;
                  (newstate[name] as any) = data;
                  const pagination = this.getEmptyState().pagination;
                  (newstate.pagination as any) = pagination;
                  this.setState(newstate, () => {
                    this.debouncedFetch({
                      page: 1,
                    });
                  });
                }}
              />
            </div>
          );
        })}
      </>
    );
  };

  listingDashboard = () => {
    const IngramTooltip = (
      <TooltipCustom icon_url="i.png" className="ingram-tooltip">
        Ingram Data
      </TooltipCustom>
    );
    const columns: ITableColumn[] = [
      {
        accessor: "po_number",
        Header: "PO",
        id: "po_number__po_number",
        sortable: true,
        width: 130,
        Cell: (c) => (
          <div className="po-underline" title={c.original.internal_notes}>
            {c.original.po_number}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: "Ticket Updated",
        id: "connectwise_last_data_update",
        sortable: true,
        width: 130,
        Cell: (c) => (
          <div>
            {fromISOStringToFormattedDate(
              c.original.ticket.connectwise_last_data_update,
              "MM/DD/YYYY h:mm A"
            )}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: "Summary",
        id: "summary",
        sortable: false,
        Cell: (c) => (
          <div title={c.original.ticket.summary}>
            {" "}
            {c.original.ticket.summary}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: "Ticket Note",
        id: "last_ticket_note",
        sortable: false,
        Cell: (c) => (
          <div title={c.original.ticket.last_ticket_note}>
            {" "}
            {c.original.ticket.last_ticket_note}
          </div>
        ),
      },
      {
        accessor: "Customer",
        Header: "Company",
        id: "company_name",
        sortable: true,
        Cell: (c) => <div>{c.original.ticket.company_name}</div>,
      },
      {
        accessor: "po",
        Header: "Vendor Company Name",
        id: "vendor_company_name",
        width: 130,
        sortable: true,
        Cell: (c) => <div>{c.original.vendor_company_name}</div>,
      },

      {
        accessor: "po",
        Header: "Status",
        id: "status_name",
        width: 130,
        sortable: true,
        Cell: (c) => <div>{c.original.ticket.status_name}</div>,
      },
      {
        accessor: "shipped_date",
        Header: (
          <div className="d-flex-ingram-tooltip">
            {" "}
            Shipped On{IngramTooltip}
          </div>
        ),
        id: "shipped_date",
        sortable: true,
        width: 110,
        Cell: (c) => (
          <div className="ingram-data-fieds" title="Ingram Data">
            {fromISOStringToFormattedDate(
              c.original.shipped_date,
              "MM/DD/YYYY"
            )}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: (
          <div className="d-flex-ingram-tooltip">
            {" "}
            Est.Delivery{IngramTooltip}
          </div>
        ),
        id: "eta",
        width: 110,
        sortable: true,
        Cell: (c) => (
          <div className="ingram-data-fieds" title="Ingram Data">
            {fromISOStringToFormattedDate(c.original.eta)}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: () => null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
      defaultPageSize: 50,
    };
    return (
      <div className="order-tracking-listing col-md-12">
        <Table
          columns={columns}
          rows={this.state.rows || []}
          manualProps={manualProps}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`rules-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          defaultSorted={[
            {
              id: "",
              desc: true,
            },
          ]}
          loading={this.props.isFetching}
        />
      </div>
    );
  };

  onRowClick = (rowInfo) => {
    const { crm_id, ticket } = rowInfo.original;
    this.props.history.push(
      `/operations/order-tracking-v2?po-number=${crm_id}&company=${ticket.company_crm_id}`
    );
  };

  toggleLoggedInUserOnly = (event: any) => {
    const targetValue = event.target.checked;
    const newstate = cloneDeep(this.state);
    newstate.openPOsDistributors.map((x) => (x.filter = false));
    newstate.orderEstimatedShipDate.map((x) => (x.filter = false));
    newstate.lastCustomerUpdate.map((x) => (x.filter = false));
    const pagination = this.getEmptyState().pagination;
    pagination.params.page_size = this.state.pagination.params.page_size;
    (newstate.pagination as any) = pagination;
    (newstate.loggedInUserOnly as any) = targetValue;
    this.setState(newstate, () => {
      this.loadGraphsData();
      this.debouncedFetch({
        page: 1,
      });
    });
  };

  render() {
    const __this = this;
    return (
      <div className="order-track">
        {
          <div className="charts-container">
            <div className="graph-page-header">
              <Checkbox
                isChecked={this.state.loggedInUserOnly}
                name="loggedInUserOnly"
                className="searchbox_container"
                onChange={(e) => this.toggleLoggedInUserOnly(e)}
              >
                Show only logged In user data
              </Checkbox>
            </div>

            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.openPOsDistributors,
                "openPOsDistributors",
                this.state.loadingOpenPO,
                "Open PO's by Distributor",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              <div
                className={`graph-heading  ${
                  this.state.loadingTWO ? "loading" : ""
                }`}
              >
                Order Shipped but not received
              </div>
              <div className={this.state.loadingTWO ? "loader" : ""}>
                <Spinner show={this.state.loadingTWO} />
              </div>
              <div
                className={`dashboard-graph-img ${
                  this.state.loadingTWO ? "loading" : ""
                }`}
              >
                {true ? (
                  <>
                    <Doughnut
                      data={{
                        labels: ["", ""],
                        datasets: [
                          {
                            data: this.state.orderShippedNotReceived.map(
                              (x) => x.value
                            ),
                            backgroundColor: [this.priorityColorMap[4]],
                            borderWidth: 1,
                            hoverOffset: 10,
                          },
                        ],
                      }}
                      options={{
                        events: ["click"],
                        maintainAspectRatio: false,
                        plugins: {
                          datalabels: {
                            display: false,
                          },
                        },
                        legend: {
                          display: false,
                          position: "bottom",
                          align: "start",
                          labels: {
                            usePointStyle: true,
                            boxWidth: 7,
                            fontSize: 11,
                            boxRadius: 50,
                          },
                        },
                        layout: {
                          padding: {
                            left: 20,
                            right: 20,
                            top: 20,
                            bottom: 90,
                          },
                        },
                        cutoutPercentage: 75,
                        onClick: function(evt, element, a) {
                          if (element.length > 0) {
                            var ind = element[0]._index;
                            const newstate = cloneDeep(__this.state);
                            const data = newstate.orderShippedNotReceived;
                            data.map((x) => (x.filter = false));
                            data[ind].filter = true;
                            (newstate.orderShippedNotReceived as any) = data;
                            __this.setState(newstate, () => {
                              __this.debouncedFetch({
                                page: 1,
                              });
                            });
                          }
                        },
                      }}
                    />
                    <div className="donut-inner">
                      <span>
                        {this.getTotal(
                          this.state.orderShippedNotReceived.map((x) => x.value)
                        )}
                      </span>
                    </div>
                  </>
                ) : (
                  <div className="dashboard-graph-no-data">NO DATA</div>
                )}
              </div>
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.orderEstimatedShipDate,
                "orderEstimatedShipDate",
                this.state.loadingTHREE,
                "Order by Estimated Ship Date",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.lastCustomerUpdate,
                "lastCustomerUpdate",
                this.state.loadingFOUR,
                "Last Customer Update",
                this.priorityColorMap
              )}
            </div>
          </div>
        }
        {this.listingDashboard()}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customerId: state.customer.customerId,
  orderTrackingDashboardData: state.orderTracking.orderTrackingDashboardData,
  isFetching: state.orderTracking.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  getLastCustomerUpdatedData: (showLoggedInUserData: boolean) =>
    dispatch(getLastCustomerUpdatedData(showLoggedInUserData)),
  getOpenPOByPOBYDistributor: (showLoggedInUserData: boolean) =>
    dispatch(getOpenPOByPOBYDistributor(showLoggedInUserData)),
  getOrderShippedNotReceived: (showLoggedInUserData: boolean) =>
    dispatch(getOrderShippedNotReceived(showLoggedInUserData)),
  getOrderEstimatedShipDate: (showLoggedInUserData: boolean) =>
    dispatch(getOrderEstimatedShipDate(showLoggedInUserData)),
  getTabledata: (
    showLoggedInUserData: boolean,
    params?: IServerPaginationParams
  ) => dispatch(getTabledata(showLoggedInUserData, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTRDashboard);
