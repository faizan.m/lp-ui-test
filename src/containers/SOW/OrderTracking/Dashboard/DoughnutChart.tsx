import { cloneDeep } from "lodash";
import React from "react";
import { Doughnut } from "react-chartjs-2";
import Spinner from "../../../../components/Spinner";

export const rawDoughnutChart = (_this, list, name, loading, heading,priorityColorMap) => {
    return (
      <div className={`graph-heading ${loading ? "loading" : ""}`}>
        {heading}
        <div className={loading ? "loader" : ""}>
          <Spinner show={loading} />
          <div className={`dashboard-graph-img ${loading ? "loading" : ""}`}>
            {list.length > 0 ? (
              <>
                <Doughnut
                  data={{
                    labels: list.map((x) => x.label),
                    datasets: [
                      {
                        data: list.map((x) => x.value),
                        backgroundColor: priorityColorMap,
                        hoverBackgroundColor: priorityColorMap,
                        borderWidth: 1,
                        hoverBorderWidth: 5,
                        hoverBorderColor: priorityColorMap,
                      },
                    ],
                  }}
                  options={{
                    events: ["click"],
                    maintainAspectRatio: false,
                    plugins: {
                      datalabels: {
                        display: true,
                        formatter: (value, ctx) => {
                          return value && value.toFixed(0);
                        },
                        color: "#fff",
                      },
                    },
                    legend: {
                      position: "bottom",
                      align: "start",
                      labels: {
                        usePointStyle: true,
                        boxWidth: 7,
                        fontSize: 11,
                        boxRadius: 50,
                      },
                    },
                    layout: {
                      padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10,
                      },
                    },
                    cutoutPercentage: 70,
                    onClick: function(evt, element, a) {
                      if (element.length > 0) {
                        var ind = element[0]._index;
                        const newstate = cloneDeep(_this.state);
                        const data = newstate[name];
                        data.map((x) => (x.filter = false));
                        data[ind].filter = true;
                        (newstate[name] as any) = data;
                        _this.setState(newstate, () => {
                          _this.debouncedFetch({
                            page: 1,
                          });
                        });
                      }
                    },
                  }}
                />
              </>
            ) : (
              <div className="dashboard-graph-no-data">NO DATA</div>
            )}
          </div>
        </div>
      </div>
    );
  };