import React from "react";
import { connect } from "react-redux";
import { startCase } from "lodash";
import { withRouter } from "react-router-dom";
import { diff as jsonDiff } from "json-diff";

import Spinner from "../../../components/Spinner";
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import {
  getAgreementHistory,
  GET_AGREEMENT_HISTORY_SUCCESS,
} from "../../../actions/agreement";
import DiffBox from "../../../components/DiffBox/diffBox";
import {
  getCustomerCost,
  getBillingMultiplier,
} from "../../../utils/agreementCalculations";
import SquareButton from "../../../components/Button/button";
import "../../../commonStyles/versionHistory.scss";
import "./style.scss";

interface IViewAgreementHistoryProps extends ICommonProps {
  id: any;
  show: boolean;
  onClose: (e: any) => void;
  getAgreementHistory: any;
}

interface IViewAgreementHistoryState {
  open: boolean;
  agreementHistory: IAgreement[];
  isCollapsed: boolean[];
  loadMore: boolean;
  isLoading: boolean;
}

class ViewAgreementHistory extends React.Component<
  IViewAgreementHistoryProps,
  IViewAgreementHistoryState
> {
  static fieldsToCheck: string[] = [
    "name",
    "user_name",
    "sections",
    "products",
    "pax8_products",
    "internal_cost",
    "customer_cost",
    "margin",
    "margin_percentage",
    "discount_percentage",
    "total_discount",
    "version_description",
  ];

  static pax8FieldsToCheck: Set<string> = new Set([
    "billing_term",
    "commitment_term",
    "customer_cost",
    "internal_cost",
    "margin",
    "pricing_type",
    "product_name",
    "quantity",
    "revenue",
    "sku",
    "suggested_retail_price",
    "total_margin",
    "vendor_name",
  ]);

  constructor(props: IViewAgreementHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    agreementHistory: [],
    isCollapsed: [],
    loadMore: false,
    isLoading: false,
  });

  componentDidUpdate(prevProps: IViewAgreementHistoryProps) {
    if (this.props.id && this.props.id !== prevProps.id) {
      this.setState({ isLoading: true });
      this.props
        .getAgreementHistory(this.props.id)
        .then((action) => {
          if (action.type === GET_AGREEMENT_HISTORY_SUCCESS) {
            this.setState({
              agreementHistory: action.response.reverse(),
              isCollapsed: Array(action.response.length).fill(false),
              isLoading: false,
            });
          } else {
            this.setState({ isLoading: false });
          }
        })
        .catch(() => this.setState({ isLoading: false }));
    }
  }  

  getTitle = () => {
    return <div className="text-center">Agreement Versions</div>;
  };

  onClickLoadMore = () => {
    this.setState({ loadMore: !this.state.loadMore });
  };

  getExtendedCostMultiplier = (
    productBundle: IProductBundle,
    singleProductIdx: number
  ): Number => {
    return (
      getBillingMultiplier(productBundle) *
      Number(getCustomerCost(productBundle.products[singleProductIdx]))
    );
  };

  onRollbackClick = (agreement: IAgreement) => {
    this.props.history.push({
      pathname: "/sow/agreement/0",
      search: "?rollback=true",
      state: { ...agreement, id: this.props.id },
    });
  };

  getProductString = (
    singleProduct: IProduct,
    modifier: "__old" | "__new",
    onlyCols: boolean = false,
    multiplier?: number
  ): any => {
    let quantity =
      singleProduct.quantity === undefined
        ? undefined
        : typeof singleProduct.quantity === "object"
        ? singleProduct.quantity![modifier]
        : singleProduct.quantity;
    let extended_cost;
    if (quantity !== undefined && multiplier) {
      extended_cost = quantity * multiplier;
    }
    if (onlyCols) {
      let cols: string[] = [];
      if (quantity !== undefined) {
        cols.push("Quantity");
        cols.push("Extended Cost");
      }
      return cols;
    } else
      return (
        (quantity !== undefined ? `${quantity}\n` : "") +
        (extended_cost !== undefined
          ? `$ ${Math.round(extended_cost * 100) / 100}\n`
          : "")
      );
  };

  getColumns = (diff, type: string): string[] => {
    if (type === "sections") {
      let cols = [];
      const section = diff[1];
      if (section.name !== undefined) cols.push("Name");
      if (section.content !== undefined) cols.push("Content");
      return cols;
    } else if (type === "products") {
      let cols = [];
      const productBundle = diff[1];
      if (productBundle.selected_product !== undefined)
        cols.push("Selected Option");
      return cols;
    } else if (type === "pax8_products") {
      return Object.keys(diff)
        .sort()
        .filter((key) => ViewAgreementHistory.pax8FieldsToCheck.has(key))
        .map((el) => startCase(el));
    } else {
      return Object.keys(diff);
    }
  };

  getDiffString = (
    diff,
    type: string,
    diffType: "__old" | "__new",
    originalObject?
  ) => {
    if (type === "sections") {
      let name: string, content: string;
      const diffStatus = diff[0];
      const section = diff[1];
      if (diffStatus === "~") {
        name = section.name ? section.name[diffType] : undefined;
        content = section.content ? section.content[diffType] : undefined;
      } else if (
        (diffStatus === "-" && diffType === "__old") ||
        (diffStatus === "+" && diffType === "__new")
      ) {
        name = section.name;
        content = section.content;
      } else if (
        (diffStatus === "+" && diffType === "__old") ||
        (diffStatus === "-" && diffType === "__new")
      ) {
        return "";
      }
      return (
        (name !== undefined ? `${name}\n` : "") +
        (content !== undefined ? `\n${content}\n` : "")
      );
    } else if (type === "products") {
      let selected_product_idx: number;
      const diffStatus = diff[0];
      const productBundle: IProductBundle = diff[1];
      if (diffStatus === "~") {
        selected_product_idx = productBundle.selected_product
          ? productBundle.selected_product[diffType]
          : undefined;
        return selected_product_idx !== undefined &&
          selected_product_idx !== null
          ? originalObject.products[selected_product_idx].name
          : "";
      } else {
        return "";
      }
    } else if (type.startsWith("single_product")) {
      const diffStatus = diff[0];
      const singleProduct = diff[1];
      const multiplier = Number(type.split(" ")[1]);
      if (diffStatus === "~")
        return this.getProductString(
          singleProduct,
          diffType,
          false,
          multiplier
        );
      else return "";
    } else if (type === "pax8_products") {
      const diffStatus = diff[0];
      const products = diff[1];
      const prodObject = Array.from(
        ViewAgreementHistory.pax8FieldsToCheck
      ).reduce((obj, item) => {
        obj[item] = undefined;
        return obj;
      }, {});
      if (diffStatus === "~") {
        Object.keys(prodObject).forEach((key) => {
          prodObject[key] = products[key] ? products[key][diffType] : undefined;
        });
      } else if (
        (diffStatus === "-" && diffType === "__old") ||
        (diffStatus === "+" && diffType === "__new")
      ) {
        Object.keys(prodObject).forEach((key) => {
          prodObject[key] = products[key];
        });
      } else if (
        (diffStatus === "+" && diffType === "__old") ||
        (diffStatus === "-" && diffType === "__new")
      ) {
        return "";
      }
      return Object.keys(prodObject)
        .sort()
        .reduce((diffString, key) => {
          return prodObject[key] !== undefined
            ? diffString +
                (prodObject[key] !== null ? prodObject[key] : "N/A") +
                "\n"
            : diffString + "\n";
        }, "");
    } else {
      return String(diff[diffType]);
    }
  };

  renderHistoryContainer = (
    currentVersion: IAgreement,
    boardIndex: number,
    prevVersion?: IAgreement
  ) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];
    const diff = jsonDiff(prevVersion, currentVersion);
    const isDifference: boolean = ViewAgreementHistory.fieldsToCheck.reduce(
      (prev, current) => prev || current in diff,
      false
    );
    const commonDiffBoxProps = {
      splitView: true,
      disableWordDiff: true,
      hideLineNumbers: true,
    };
    const toggleCollapsedState = () =>
      this.setState((prevState) => ({
        isCollapsed: [
          ...prevState.isCollapsed.slice(0, boardIndex),
          !prevState.isCollapsed[boardIndex],
          ...prevState.isCollapsed.slice(boardIndex + 1),
        ],
      }));

    return (
      <div
        key={boardIndex}
        className={`agr-collapse-section collapsable-section ${
          isCollapsed
            ? "collapsable-section--collapsed"
            : "collapsable-section--not-collapsed"
        }`}
      >
        <div
          className="col-md-12 collapsable-heading"
          onClick={toggleCollapsedState}
        >
          <div className="left col-md-9">
            <div className="name">
              <span>Updated by: </span>
              {currentVersion.updated_by_name}
            </div>
            <div className="version">
              <span>Version: </span>
              {currentVersion.version}
            </div>
            <div className="date">
              <span>Updated On: </span>
              {utcToLocalInLongFormat(currentVersion.updated_on)}
            </div>
          </div>

          <div className="right col-md-3">
            <SquareButton
              onClick={() => this.onRollbackClick(currentVersion)}
              content={"Rollback & Update"}
              title="Rollback to this version and edit"
              bsStyle={ButtonStyle.DEFAULT}
            />
          </div>
        </div>
        <div className="collapsable-contents">
          {prevVersion === undefined ? (
            <div className="no-data">FIRST VERSION</div>
          ) : isDifference ? (
            <>
              <div className="heading-version">
                <span> {` Previous Version `} </span>
                <span>{` Version ${currentVersion.version}`}</span>
              </div>
              {ViewAgreementHistory.fieldsToCheck.map((field, outerIdx) => {
                if (field in diff) {
                  if (field === "sections") {
                    return (
                      <div key={outerIdx} className="agr-version-split">
                        <div className="agr-custom-vheading">Sections</div>
                        {diff[field].map((sectionDiff, sectionIdx) =>
                          sectionDiff[0] !== " " &&
                          this.getColumns(sectionDiff, field).length !== 0 ? (
                            <div
                              key={sectionIdx}
                              className="version-section-row"
                            >
                              <div className="version-column-split">
                                {this.getColumns(sectionDiff, field).map(
                                  (col, idx) => (
                                    <div key={idx}>{col}</div>
                                  )
                                )}
                              </div>
                              <DiffBox
                                customWrapper={"section-version-split"}
                                diffViewerProps={{
                                  oldValue: this.getDiffString(
                                    sectionDiff,
                                    field,
                                    "__old"
                                  ),
                                  newValue: this.getDiffString(
                                    sectionDiff,
                                    field,
                                    "__new"
                                  ),
                                  ...commonDiffBoxProps,
                                  disableWordDiff: false,
                                }}
                              />
                            </div>
                          ) : null
                        )}
                      </div>
                    );
                  } else if (field === "products") {
                    return (
                      <div key={outerIdx} className="agr-version-split">
                        <div className="agr-custom-vheading">Products</div>
                        {diff[field].map((productBundleDiff, bundleIdx) =>
                          productBundleDiff[0] !== " " ? (
                            <div
                              key={bundleIdx}
                              className="productbundle-split-version"
                            >
                              <div className="version-column-split">
                                {`PRODUCT BUNDLE ${bundleIdx + 1}`}
                              </div>
                              <div className="singleproduct-split-version">
                                <div className="version-column-split">
                                  {this.getColumns(
                                    productBundleDiff,
                                    field
                                  ).map((col, idx) => (
                                    <div key={idx}>{col}</div>
                                  ))}
                                </div>
                                <DiffBox
                                  diffViewerProps={{
                                    oldValue: this.getDiffString(
                                      productBundleDiff,
                                      field,
                                      "__old",
                                      prevVersion.products[bundleIdx]
                                    ),
                                    newValue: this.getDiffString(
                                      productBundleDiff,
                                      field,
                                      "__new",
                                      currentVersion.products[bundleIdx]
                                    ),
                                    ...commonDiffBoxProps,
                                  }}
                                />
                              </div>

                              {productBundleDiff[1].products && (
                                <div className="version-column-split">
                                  SINGLE PRODUCT QUANTITY CHANGES
                                </div>
                              )}
                              {productBundleDiff[1].products
                                ? productBundleDiff[1].products.map(
                                    (singleProductDiff, idx) =>
                                      productBundleDiff[0] === "~" &&
                                      singleProductDiff[0] !== " " ? (
                                        <div
                                          key={idx}
                                          className="singleproduct-split-version"
                                        >
                                          <div className="version-column-split">
                                            {this.getProductString(
                                              singleProductDiff[1],
                                              "__old",
                                              true
                                            ).map((col: string, idx) => (
                                              <div key={idx}>{col}</div>
                                            ))}
                                          </div>
                                          <DiffBox
                                            diffViewerProps={{
                                              oldValue: this.getDiffString(
                                                singleProductDiff,
                                                `single_product ${this.getExtendedCostMultiplier(
                                                  prevVersion.products[
                                                    bundleIdx
                                                  ],
                                                  idx
                                                )}`,
                                                "__old"
                                              ),
                                              newValue: this.getDiffString(
                                                singleProductDiff,
                                                `single_product ${this.getExtendedCostMultiplier(
                                                  currentVersion.products[
                                                    bundleIdx
                                                  ],
                                                  idx
                                                )}`,
                                                "__new"
                                              ),
                                              ...commonDiffBoxProps,
                                            }}
                                          />
                                        </div>
                                      ) : null
                                  )
                                : null}
                            </div>
                          ) : null
                        )}
                      </div>
                    );
                  } else if (field === "pax8_products") {
                    return (
                      <div key={outerIdx} className="agr-version-split">
                        <div className="agr-custom-vheading">PAX8 Products</div>
                        {diff[field].products.map(
                          (productsDiff, productsIdx: number) =>
                            productsDiff[0] !== " " &&
                            this.getColumns(productsDiff[1], field).length !==
                              0 ? (
                              <div
                                key={productsIdx}
                                className={"pax8-product-split"}
                              >
                                <div className="version-column-split">
                                  {`Product #${productsIdx + 1}`}
                                </div>
                                <div className="version-section-row">
                                  <div className="version-column-split">
                                    {this.getColumns(
                                      productsDiff[1],
                                      field
                                    ).map((col, idx) => (
                                      <div key={idx}>{col}</div>
                                    ))}
                                  </div>
                                  <DiffBox
                                    customWrapper={"section-version-split"}
                                    diffViewerProps={{
                                      oldValue: this.getDiffString(
                                        productsDiff,
                                        field,
                                        "__old"
                                      ),
                                      newValue: this.getDiffString(
                                        productsDiff,
                                        field,
                                        "__new"
                                      ),
                                      ...commonDiffBoxProps,
                                      disableWordDiff: true,
                                    }}
                                  />
                                </div>
                              </div>
                            ) : null
                        )}
                      </div>
                    );
                  } else {
                    return (
                      <DiffBox
                        heading={field.replace(/_/g, " ")}
                        key={outerIdx}
                        diffViewerProps={{
                          oldValue: this.getDiffString(
                            diff[field],
                            field,
                            "__old"
                          ),
                          newValue: this.getDiffString(
                            diff[field],
                            field,
                            "__new"
                          ),
                          ...commonDiffBoxProps,
                          disableWordDiff: !(
                            field === "version_description" || field === "name"
                          ),
                        }}
                      />
                    );
                  }
                } else {
                  return null;
                }
              })}
            </>
          ) : (
            <div className="no-data">NO CHANGE</div>
          )}
        </div>
      </div>
    );
  };

  getBody = () => {
    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.state.isLoading} />
        </div>
        <div className="history-section heading  col-md-12">
          {this.state.agreementHistory.length === 0 && !this.state.isLoading && (
            <div className="col-md-12 no-data" style={{ textAlign: "center" }}>
              No history available.
            </div>
          )}
          {this.state.agreementHistory.length !== 0 &&
            !this.state.isLoading &&
            !this.state.loadMore &&
            this.state.agreementHistory
              .slice(0, 5)
              .map((history, i, versions) =>
                this.renderHistoryContainer(
                  history,
                  i,
                  i !== versions.length - 1 ? versions[i + 1] : undefined
                )
              )}
          {this.state.agreementHistory.length !== 0 &&
            this.state.loadMore &&
            this.state.agreementHistory.map((history, i, versions) =>
              this.renderHistoryContainer(
                history,
                i,
                i !== versions.length - 1 ? versions[i + 1] : undefined
              )
            )}
          {this.state.agreementHistory.length > 5 && (
            <div
              className="col-md-12 show-more"
              onClick={(e) => this.onClickLoadMore()}
              style={{ textAlign: "center" }}
            >
              {!this.state.loadMore ? "load more..." : "show less"}
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`footer
        ${this.state.isLoading ? `loading` : ""}`}
      ></div>
    );
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="version-history"
      />
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getAgreementHistory: (id: number) => dispatch(getAgreementHistory(id)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewAgreementHistory)
);
