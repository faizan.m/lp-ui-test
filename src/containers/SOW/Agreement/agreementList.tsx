import React from "react";
import { connect } from "react-redux";
import { debounce, DebouncedFunc, has } from "lodash";
import moment from "moment";

import EditButton from "../../../components/Button/editButton";
import Table from "../../../components/Table/table";
import {
  agreementCRUD,
  getAgreementList,
  getAgreementPreview,
  sendEmailToAccountManagerAgreement,
  SEND_AGR_EMAIL_SUCCESS,
  SEND_AGR_EMAIL_FAILURE,
  CREATE_AGREEMENT_SUCCESS,
  DOWNLOAD_AGREEMENT_SUCCESS,
  getAgreementAuthorsList,
  FETCH_AGREEMENTS_AUTHORS_SUCCESS,
} from "../../../actions/agreement";
import { LAST_VISITED_CLIENT_360_TAB } from "../../../actions/customer";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import ViewHistory from "./agreementHistory";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../components/Button/deleteButton";
import AgreementFilter from "./filters";
import IconButton from "../../../components/Button/iconButton";
import AgreementsOverallHistory from "./agreementsHistory";
import { addSuccessMessage, addErrorMessage } from "../../../actions/appState";
import "../../../commonStyles/filtersListing.scss";
import "../../../commonStyles/versionHistory.scss";
import store from "../../../store";
import "./style.scss";

interface IAgreementListProps extends ICommonProps {
  customerId?: number;
  agreementList: IPaginatedDefault & {
    results: IAgreementShort[];
  };
  loggenInUser: ISuperUser;
  isFetchingAgreements: boolean;
  getAgreementList: (
    showDisable: boolean,
    params: AgreementQueryParams
  ) => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  sendEmailToAccountManager: (id: number) => Promise<any>;
  getAgreementAuthorsList: () => Promise<any>;
  agreementCRUD: (method: string, agreement: IAgreement) => Promise<any>;
  getAgreementPreview: (payload: IAgreement, id: number) => Promise<any>;
}

interface IAgreementListState {
  rows: IAgreementShort[];
  isAgreementPosting: boolean;
  isopenConfirm: boolean;
  downloadingPDFIds: number[];
  id: string;
  isFilterModalOpen: boolean;
  emailSending: number[];
  filters: IAgreementFilters;
  showDisable: boolean;
  viewhistory: boolean;
  openPreview: boolean;
  previewPDFIds: number[];
  previewHTML: string;
  pagination: {
    totalRows: number;
    totalPages: number;
    params?: AgreementQueryParams;
  };
  reset: boolean;
  authors: { author_id: string; author_name: string; profile_url: string }[];
  viewOverallHistory: boolean;
}

class AgreementListing extends React.Component<
  IAgreementListProps,
  IAgreementListState
> {
  private debouncedFetch: DebouncedFunc<
    (params?: IServerPaginationParams) => void
  >;

  constructor(props: IAgreementListProps) {
    super(props);

    this.state = {
      isAgreementPosting: false,
      isopenConfirm: false,
      downloadingPDFIds: [],
      id: "",
      rows: [],
      isFilterModalOpen: false,
      filters: {
        author_id: "",
        updated_after: "",
        updated_before: "",
      },
      showDisable: false,
      viewhistory: false,
      openPreview: false,
      previewPDFIds: [],
      emailSending: [],
      previewHTML: null,
      pagination: {
        totalRows: 0,
        totalPages: 0,
        params: {},
      },
      reset: false,
      authors: [],
      viewOverallHistory: false,
    };
    this.debouncedFetch = debounce(this.fetchData, 500);
  }

  componentDidMount() {
    if (this.props.location.pathname === "/client-360") {
      store.dispatch({
        type: LAST_VISITED_CLIENT_360_TAB,
        response: "Agreements",
      });
    }

    if (this.props.customerId) {
      this.setState((prevState) => ({
        filters: {
          ...prevState.filters,
          customer: this.props.customerId,
        },
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            customer: this.props.customerId,
          },
        },
      }));
    }
    this.setAuthorsList();
  }

  componentDidUpdate(prevProps: IAgreementListProps) {
    if (
      this.props.customerId &&
      prevProps.customerId !== this.props.customerId
    ) {
      this.setState(
        (prevState) => ({
          filters: {
            ...prevState.filters,
            customer: this.props.customerId,
          },
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              customer: this.props.customerId,
            },
          },
        }),
        this.fetchData
      );
    }
    if (
      this.props.agreementList &&
      prevProps.agreementList !== this.props.agreementList
    ) {
      this.setRows(this.props);
    }
  }

  fetchData = (params?: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;

    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getAgreementList(this.state.showDisable, newParams);
  };

  setAuthorsList = () => {
    this.props.getAgreementAuthorsList().then((a) => {
      if (a.type === FETCH_AGREEMENTS_AUTHORS_SUCCESS) {
        this.setState({
          authors: a.response,
        });
      }
    });
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  onEditRowClick = (id: number) => {
    this.props.history.push(`/sow/agreement/${id}`);
  };

  onCloneClick = (id: number) => {
    this.props.history.push(`/sow/agreement/${id}?cloned=true`);
  };

  toggleAgreementsHistory = () => {
    this.setState({
      viewOverallHistory: !this.state.viewOverallHistory,
    });
  };

  renderTopBar = () => {
    return (
      <div className="Agreement-users-listing__actions">
        <div className="row-action header-panel">
          <div className="left">
            <Input
              field={{
                label: "",
                type: InputFieldType.SEARCH,
                value: this.state.pagination.params.search,
                isRequired: false,
              }}
              width={6}
              placeholder="Search Agreements"
              name="searchString"
              onChange={this.onSearchStringChange}
              className="search"
            />
            <Checkbox
              isChecked={this.state.showDisable}
              name="option"
              onChange={(e) => this.onChecboxChanged(e)}
            >
              Show disabled
            </Checkbox>
          </div>
          <div className="field-section actions-right">
            {this.props.customerId && (
              <SquareButton
                content={
                  <>
                    <span className="add-plus">+</span>
                    <span className="add-text">Add Agreement</span>
                  </>
                }
                onClick={this.addAgreementClick}
                className="add-btn"
                bsStyle={ButtonStyle.OUTLINE}
              />
            )}
            <SquareButton
              onClick={this.toggleAgreementsHistory}
              content={
                <span>
                  <img
                    alt=""
                    src="/assets/icons/version.svg"
                    style={{ marginRight: "3px" }}
                  />
                  History
                </span>
              }
              bsStyle={ButtonStyle.PRIMARY}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.agreementList &&
                this.props.agreementList.results.length === 0
              }
              bsStyle={ButtonStyle.PRIMARY}
              className={"filter-agr"}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  onChecboxChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    const showDisable = event.target.checked;
    this.setState(
      {
        showDisable,
      },
      () => {
        this.debouncedFetch({
          page: 1,
        });
      }
    );
  };

  setRows = (nextProps: IAgreementListProps) => {
    let agreementListData = nextProps.agreementList;
    let agreementList: IAgreementShort[] = agreementListData.results;

    const rows: IAgreementShort[] = agreementList.map((template, index) => ({
      name: template.name,
      forecast_data: template.forecast_data,
      is_disabled: template.is_disabled,
      customer_name: template.customer_name ? template.customer_name : "N.A.",
      author_name: template.author_name ? template.author_name : "N.A.",
      updated_on: template.updated_on ? template.updated_on : "N.A.",
      updated_by_name: template.updated_by_name
        ? template.updated_by_name
        : "N.A.",
      id: template.id,
      version: template.version,
      index,
    }));

    this.setState((prevState) => ({
      reset: false,
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: agreementListData.count,
        totalPages: Math.ceil(
          agreementListData.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onFilterRemove = (removedFilters: IAgreementFilters) => {
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          ...removedFilters,
        },
      }),
      () => {
        this.setState((prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              ...prevState.filters,
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      filters.author_id || (filters.updated_before && filters.updated_after);

    const author_name = filters.author_id
      ? this.state.authors.find(
          (author) => author.author_id === filters.author_id
        ).author_name
      : "";

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.author_id && (
          <div className="section-show-filters agr-applied-filters-row">
            <label>Author: </label>
            <div className="agr-filter-content">
              {author_name + "  "}
              <span
                className="agr-filter-clear-icon"
                onClick={() => this.onFilterRemove({ author_id: undefined })}
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
        {filters.updated_after && (
          <div className="section-show-filters agr-applied-filters-row">
            <label>Updated between:</label>
            <div className="agr-filter-content">
              {moment(this.state.filters.updated_after).format("MM/DD/YYYY")}
              {" - "}
              {moment(this.state.filters.updated_before).format("MM/DD/YYYY")}
              <span
                className="date-clear agr-filter-clear-icon"
                onClick={() =>
                  this.onFilterRemove({
                    updated_before: undefined,
                    updated_after: undefined,
                  })
                }
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
      </div>
    ) : null;
  };

  onDeleteRowClick(original: any) {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props
      .agreementCRUD("delete", { id: this.state.id } as IAgreement)
      .then((action) => {
        if (action.type === CREATE_AGREEMENT_SUCCESS) {
          this.debouncedFetch();
          this.toggleConfirmOpen();
        }
      });
  };

  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: "",
    });
  };

  onClickViewHistory(id: string) {
    this.setState({
      viewhistory: true,
      id,
    });
  }

  addAgreementClick = () => {
    this.props.history.push(
      "/sow/agreement/0" +
        (this.props.customerId ? `?customer=${this.props.customerId}` : "")
    );
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IAgreementFilters) => {
    if (filters) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            ...filters,
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal();
    }
  };

  onPDFPreviewClick(template: IAgreement) {
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, template.id],
    });
    this.props.getAgreementPreview({} as IAgreement, template.id).then((a) => {
      if (a.type === DOWNLOAD_AGREEMENT_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          id: "",
          previewPDFIds: this.state.previewPDFIds.filter(
            (id) => template.id !== id
          ),
        });
      } else {
        this.setState({
          id: "",
          previewPDFIds: [],
        });
      }
    });
  }

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  onPDFClick = (agreement: IAgreement) => {
    this.setState({
      downloadingPDFIds: [...this.state.downloadingPDFIds, agreement.id],
    });
    this.props
      .getAgreementPreview({} as IAgreement, agreement.id)
      .then((action) => {
        if (action.type === DOWNLOAD_AGREEMENT_SUCCESS) {
          this.setState({
            id: "",
            downloadingPDFIds: this.state.downloadingPDFIds.filter(
              (id) => agreement.id !== id
            ),
          });
          const url = action.response.file_path;
          const link = document.createElement("a");
          link.href = url;
          link.target = "_blank";
          link.setAttribute("download", action.response.file_name);
          document.body.appendChild(link);
          link.click();
        } else {
          this.setState({
            id: "",
            previewPDFIds: [],
          });
        }
      });
  };

  onEmailSending(id: number): void {
    this.setState(
      {
        emailSending: [...this.state.emailSending, id],
      },
      () => {
        this.props.sendEmailToAccountManager(id).then((action) => {
          this.setState({
            emailSending: this.state.emailSending.filter((i) => i !== id),
          });
          if (action.type === SEND_AGR_EMAIL_SUCCESS) {
            this.props.addSuccessMessage("Sending Email to Account Manager");
          } else if (action.type === SEND_AGR_EMAIL_FAILURE) {
            this.props.addErrorMessage("Error sending email!");
          }
        });
      }
    );
  }

  render() {
    const getPDFPrevieMarkUp = (cell) => {
      if (this.state.previewPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <>
            <DeleteButton
              type="pdf_preview"
              title="Preview"
              onClick={() => this.onPDFPreviewClick(cell.original)}
            />
          </>
        );
      }
    };

    const getPDFDownloadMarkUp = (cell) => {
      if (this.state.downloadingPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_download"
            title="PDF Download"
            onClick={() => this.onPDFClick(cell.original)}
          />
        );
      }
    };

    const getEmailSendingMarkUp = (cell) => {
      if (this.state.emailSending.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <IconButton
            icon="mail.svg"
            onClick={() => {
              this.onEmailSending(cell.original.id);
            }}
            className="mail-btn"
            title={"Send email to account manager"}
          />
        );
      }
    };

    const columns: ITableColumn[] = [
      {
        accessor: "id",
        Header: "Name",
        id: "name",
        Cell: (cell) => {
          return (
            <div className="pl-15">
              {has(
                cell.original,
                "forecast_data.forecast_error_details.error_in_forecast_creation"
              ) &&
                cell.original.forecast_data.forecast_error_details
                  .error_in_forecast_creation && (
                  <img
                    alt="forecast-warning"
                    src="/assets/icons/warning.png"
                    className="forecast-warning"
                    title={
                      cell.original.forecast_data.forecast_error_details
                        .error_message
                    }
                  />
                )}
              <span>{cell.original.name}</span>
            </div>
          );
        },
      },
      {
        accessor: "author_name",
        Header: "Author",
        id: "author_name",
        sortable: false,
        Cell: (c) => <div>{c.value}</div>,
      },
      {
        accessor: "customer_name",
        Header: "Customer",
        id: "customer_name",
        sortable: false,
        Cell: (c) => <div>{c.value}</div>,
      },
      {
        accessor: "updated_by_name",
        Header: "Updated by",
        id: "updated_by_name",
        Cell: (c) => <div>{c.value}</div>,
        sortable: false,
      },
      {
        accessor: "version",
        Header: "Current Version",
        Cell: (c) => <div>{c.original.version}</div>,
        sortable: false,
      },
      {
        accessor: "updated_on",
        Header: "Updated On",
        id: "updated_on",
        sortable: true,
        width: 110,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Versions",
        sortable: false,
        width: 65,
        Cell: (cell) => (
          <IconButton
            icon="version.svg"
            onClick={() => {
              this.onClickViewHistory(cell.original.id);
            }}
            className="version-btn"
            title={"Show Agreement Versions"}
          />
        ),
      },
      {
        accessor: "id",
        Header: "Preview",
        width: 80,
        sortable: false,
        Cell: (cell) => (
          <div className="view-iconss">{getPDFPrevieMarkUp(cell)}</div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        sortable: false,
        Cell: (cell) => (
          <>
            {!cell.original.is_disabled && (
              <div>
                <EditButton onClick={() => this.onEditRowClick(cell.value)} />
                <DeleteButton onClick={() => this.onDeleteRowClick(cell)} />
                <DeleteButton
                  type="cloned"
                  title="Clone Agreement"
                  onClick={() => this.onCloneClick(cell.value)}
                />
                {getEmailSendingMarkUp(cell)}
              </div>
            )}
          </>
        ),
      },
      {
        accessor: "id",
        Header: "Download",
        sortable: false,
        width: 80,
        Cell: (cell) => (
          <div className="icons-template">{getPDFDownloadMarkUp(cell)}</div>
        ),
      },
    ];

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.debouncedFetch,
      reset: this.state.reset,
    };

    // Remove customer column, for a single customer
    if (this.props.customerId) columns.splice(2, 1);

    return (
      <div className="agreement-listing">
        {!this.props.customerId && (
          <div className="header">
            <h3>Agreements</h3>
            <SquareButton
              content="Add Agreement"
              onClick={this.addAgreementClick}
              className="save-mapping"
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        )}
        <Table
          columns={columns}
          rows={this.state.rows || []}
          customTopBar={this.renderTopBar()}
          className={`Agreement-users-listing__table ${
            this.props.isFetchingAgreements ? `loading` : ``
          }`}
          loading={this.props.isFetchingAgreements}
          manualProps={manualProps}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetchingAgreements}
        />
        <AgreementFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          authors={this.state.authors}
          prevFilters={this.state.filters}
        />
        <ViewHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
        />
        {this.state.viewOverallHistory && (
          <AgreementsOverallHistory
            show={this.state.viewOverallHistory}
            onClose={this.toggleAgreementsHistory}
            history={this.props.history}
          />
        )}
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View Agreement Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={ButtonStyle.DEFAULT}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  loggenInUser: state.profile.user,
  agreementList: state.agreement.agreementList,
  isFetchingAgreements: state.agreement.isFetchingAgreements,
});

const mapDispatchToProps = (dispatch: any) => ({
  getAgreementList: (showDisable: boolean, params: AgreementQueryParams) =>
    dispatch(getAgreementList(showDisable, params)),
  agreementCRUD: (method: string, agreement: IAgreement) =>
    dispatch(agreementCRUD(method, agreement)),
  getAgreementPreview: (payload: IAgreement, id: number) =>
    dispatch(getAgreementPreview(payload, id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  sendEmailToAccountManager: (id: number) =>
    dispatch(sendEmailToAccountManagerAgreement(id)),
  getAgreementAuthorsList: () => dispatch(getAgreementAuthorsList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AgreementListing);
