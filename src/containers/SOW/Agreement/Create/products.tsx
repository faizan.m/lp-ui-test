import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Input from "../../../../components/Input/input";
import {
  calculateCosts,
  getBillingMultiplier,
} from "../../../../utils/agreementCalculations";
import "./style.scss";
import IconButton from "../../../../components/Button/iconButton";
import SquareButton from "../../../../components/Button/button";
import { DraggableArea } from "react-draggable-tags";
import Checkbox from "../../../../components/Checkbox/checkbox";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";

interface IProductsProps {
  products: IProductBundle[];
  discount?: number;
  productError?: IFieldValidation;
  setProductsFn: (products: IProductBundle[]) => void;
}

const EmptyComment: IProductBundle = {
  billing_option: AgreementBillingTerm.NA,
  product_type: AgreementProductType.COMMENT,
  is_bundle: false,
  is_checked: false,
  selected_product: null,
  products: [],
  comment: "",
  is_required: true,
};

const Products: React.FC<IProductsProps> = ({
  products,
  setProductsFn,
  productError,
  discount = 0,
}) => {
  const [isOrdering, setIsOrdering] = useState<boolean>(false);

  useEffect(() => {
    if (productError && productError.errorState === IValidationState.ERROR)
      setIsOrdering(false);
  }, [productError]);

  const handleProductQuantityChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    productBundleIdx: number,
    singleProductIdx: number
  ) => {
    const newProducts = cloneDeep(products);
    const product_attribute = e.target.name;
    newProducts[productBundleIdx].products[singleProductIdx][
      product_attribute
    ] = Number(e.target.value);
    calculateCosts(newProducts[productBundleIdx], discount);
    setProductsFn(newProducts);
  };

  const addComment = () => {
    const newProducts = cloneDeep(products);
    newProducts.push({
      ...EmptyComment,
      id: Math.round(Math.random() * 100000000),
    });
    setProductsFn(newProducts);
  };

  const removeComment = (idx: number) => {
    const newProducts = cloneDeep(products);
    newProducts.splice(idx, 1);
    setProductsFn(newProducts);
  };

  const handleDraggableChange = (products: IProductBundle[]) => {
    setProductsFn(products);
  };

  const handleChangeOrderingCheckbox = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setIsOrdering(e.target.checked);
  };

  const handleChangeComment = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newProducts = cloneDeep(products);
    newProducts[index].comment = e.target.value;
    setProductsFn(newProducts);
  };

  const onRadioChanged = (e, productBundleIdx: number) => {
    const newProducts = cloneDeep(products);
    const productBundle = newProducts[productBundleIdx];
    // If a product in a bundle is already selected,then we have to
    // unselect the radio and discard the bundle in calculations
    if (productBundle.selected_product === Number(e.target.value)) {
      productBundle.selected_product = null;
      productBundle.is_checked = false;
    } else {
      productBundle.selected_product = Number(e.target.value);
      productBundle.is_checked = true;
    }
    calculateCosts(productBundle, discount);
    setProductsFn(newProducts);
  };

  const renderProductBundle = ({
    productBundle,
    externalIdx,
  }: {
    productBundle: IProductBundle;
    externalIdx: number;
  }) => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    const noDecimalFormatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: 0,
    });

    return (
      <div
        key={externalIdx}
        className={
          "agreement-product-block" + (isOrdering ? " ordering-mode" : "")
        }
      >
        <div className="product-bundle-main">
          {productBundle.product_type === AgreementProductType.COMMENT ? (
            <div className="agr-product-comment">
              <div className="comment-actions">
                <SmallConfirmationBox
                  onClickOk={() => removeComment(externalIdx)}
                  text={"Comment"}
                  showButton={true}
                  title="Delete Comment"
                />
                <CopyToClipboard text={productBundle.comment}>
                  <IconButton
                    icon="copy.svg"
                    onClick={() => null}
                    newIcon={true}
                    title="Copy Comment"
                    className="product-copy"
                  />
                </CopyToClipboard>
              </div>
              <Input
                field={{
                  label: "",
                  type: InputFieldType.TEXT,
                  value: productBundle.comment,
                }}
                width={12}
                name="comment"
                onChange={(e) => handleChangeComment(e, externalIdx)}
                disabled={isOrdering}
                error={
                  productBundle.hasError && {
                    errorState: IValidationState.ERROR,
                    errorMessage: "Required",
                  }
                }
                className={"agr-product-comment-input"}
              />
            </div>
          ) : (
            <div className="agreement-product-details">
              {productBundle.products &&
                productBundle.products.map(
                  (singleProduct: IProduct, internalIdx: number) => {
                    const billing_multiplier: number = getBillingMultiplier(
                      productBundle
                    );
                    let extended_cost =
                      billing_multiplier *
                      singleProduct.quantity *
                      singleProduct.customer_cost;
                    return (
                      <div
                        className="single-product-details radio-btn-group"
                        key={internalIdx}
                      >
                        <div className="radio-btn">
                          <input
                            type="radio"
                            name={`radio_option_${externalIdx}`}
                            onClick={(e) => onRadioChanged(e, externalIdx)}
                            onChange={() => null}
                            value={`${internalIdx}`}
                            checked={
                              productBundle.selected_product === internalIdx
                            }
                            disabled={isOrdering}
                            title={
                              productBundle.is_required
                                ? "Any one option must be selected"
                                : "Optional selection (If selected and quantity is set to zero, it'll not be considered in calculations but visible in agreement for showing pricing info)"
                            }
                          />
                          {productBundle.is_required ? (
                            <IconButton
                              icon="success.svg"
                              onClick={() => null}
                              newIcon={true}
                              title="Required Product (For selected option, quantity should be greater than zero)"
                              className="agr-product-icon"
                            />
                          ) : (
                            <div className="required-icon-placeholder" />
                          )}
                          <CopyToClipboard
                            text={
                              singleProduct.name +
                              "\n" +
                              singleProduct.description
                            }
                            onCopy={() => {}}
                          >
                            <IconButton
                              icon="copy.svg"
                              onClick={() => null}
                              newIcon={true}
                              title="Copy line item"
                              className="product-copy"
                            />
                          </CopyToClipboard>
                        </div>
                        <div className="name-col">{singleProduct.name}</div>
                        <div className="desc-col">
                          {singleProduct.description}
                        </div>
                        <div className="cost-column">
                          {formatter.format(singleProduct.internal_cost)}
                        </div>
                        <div className="cost-column">
                          {formatter.format(singleProduct.customer_cost)}
                        </div>
                        <div
                          className={
                            "quantity-col no-padding-top" +
                            (singleProduct.error &&
                            singleProduct.error.errorState ===
                              IValidationState.ERROR
                              ? ""
                              : " no-error")
                          }
                        >
                          <Input
                            field={{
                              label: null,
                              type: InputFieldType.NUMBER,
                              value: `${singleProduct.quantity}`,
                            }}
                            width={13}
                            name="quantity"
                            minimumValue={productBundle.is_required ? "1" : "0"}
                            className="product-quantity-input"
                            onChange={(e) =>
                              handleProductQuantityChange(
                                e,
                                externalIdx,
                                internalIdx
                              )
                            }
                            disabled={
                              isOrdering ||
                              internalIdx !== productBundle.selected_product
                            }
                            error={
                              !singleProduct.quantity && singleProduct.error
                                ? singleProduct.error
                                : undefined
                            }
                          />
                        </div>
                        <div className="billing-col">
                          {productBundle.billing_option}
                        </div>
                        <div className="extended-cost-col">
                          {internalIdx === productBundle.selected_product &&
                          productBundle.discount ? (
                            <>
                              <span className="agr-product-prev-cost">
                                {noDecimalFormatter.format(extended_cost)}
                              </span>
                              <span className="agr-product-new-cost">
                                {noDecimalFormatter.format(
                                  productBundle.customer_cost_total -
                                    productBundle.discount
                                )}
                              </span>
                            </>
                          ) : (
                            formatter.format(extended_cost)
                          )}
                        </div>
                      </div>
                    );
                  }
                )}
            </div>
          )}
        </div>
      </div>
    );
  };

  return (
    <div className="product-main-container">
      <h3 className="heading-three">
        Products
        <SquareButton
          content={
            <>
              <span className="add-plus">+</span>
              <span className="add-text">Comment</span>
            </>
          }
          bsStyle={ButtonStyle.OUTLINE}
          onClick={addComment}
          className="add-product-comment add-btn"
        />
      </h3>
      <Checkbox
        isChecked={isOrdering}
        name="orderingProducts"
        onChange={(e) => handleChangeOrderingCheckbox(e)}
        className="order-checkbox"
      >
        Reorder Products
      </Checkbox>
      <div className="agreement-product-block">
        <div className="product-bundle-main-header">
          <div className="agreement-product-header">
            <div className="header-placeholder" />
            <div className="name-col">Product</div>
            <div className="desc-col">Description</div>
            <div className="cost-column">Internal Cost</div>
            <div className="cost-column">Customer Cost</div>
            <div className="quantity-col">Quantity</div>
            <div className="billing-col">Billing Option</div>
            <div className="extended-cost-col">Extended Cost</div>
          </div>
        </div>
      </div>
      {isOrdering ? (
        <DraggableArea
          isList
          tags={products}
          render={({ tag, index }) =>
            renderProductBundle({ productBundle: tag, externalIdx: index })
          }
          onChange={(tags) => handleDraggableChange(tags)}
        />
      ) : (
        products.map((bundle, idx) =>
          renderProductBundle({ productBundle: bundle, externalIdx: idx })
        )
      )}
      {productError && productError.errorState === IValidationState.ERROR && (
        <div id="product-main-error">{productError.errorMessage}</div>
      )}
    </div>
  );
};

export default Products;
