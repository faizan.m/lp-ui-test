import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { cloneDeep, isNil, round } from "lodash";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  fetchPAX8ProductDetails,
  FETCH_PAX8_PRODUCT_SUCCESS,
} from "../../../../actions/agreement";
import SquareButton from "../../../../components/Button/button";
import EditButton from "../../../../components/Button/editButton";
import IconButton from "../../../../components/Button/iconButton";
import InfiniteList from "../../../../components/InfiniteList/infiniteList";
import Input from "../../../../components/Input/input";
import ModalBase from "../../../../components/ModalBase/modalBase";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../../components/Spinner";
import Checkbox from "../../../../components/Checkbox/checkbox";
import { DraggableArea } from "react-draggable-tags";

interface IPAX8ProductProps {
  pax8Name: string;
  products: IPAX8Product[];
  productsError: IFieldValidation;
  handleChangeProducts: (products: IPAX8Product[]) => void;
  addToQuotedProducts: (
    product?: IPAX8ProductDetail,
    comment?: boolean
  ) => void;
  fetchPAX8ProductDetails: (id: string) => Promise<any>;
}

const PAX8Products: React.FC<IPAX8ProductProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [isOrdering, setIsOrdering] = useState<boolean>(false);
  const [showCatalog, setShowCatalog] = useState<boolean>(false);
  const [editProduct, setEditProduct] = useState<IPAX8Product>();
  const [curProductIdx, setCurProductIdx] = useState<number>(-1);
  const [curPricingIdx, setCurPricingIdx] = useState<number>(-1);
  const [showAllPricing, setShowAllPricing] = useState<boolean>(false);
  const [currentProduct, setCurrentProduct] = useState<IPAX8ProductDetail>();

  useEffect(() => {
    if (
      props.productsError &&
      props.productsError.errorState === IValidationState.ERROR
    )
      setIsOrdering(false);
  }, [props.productsError]);

  const formatCurrency = (
    value: number,
    noDecimal: boolean = false
  ): string => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: noDecimal ? 0 : 2,
    });
    return formatter.format(value);
  };

  const closeModal = () => {
    setEditMode(false);
    setCurProductIdx(-1);
    setShowAllPricing(false);
    setEditProduct(undefined);
    setCurrentProduct(undefined);
  };

  const closeCatalogModal = () => {
    setShowCatalog(false);
  };

  const getContractLength = (
    commitment_term: string,
    commitment_term_in_months: number
  ): string => {
    let textInMonths = !isNil(commitment_term_in_months)
      ? commitment_term_in_months +
        " month" +
        (commitment_term_in_months > 1 ? "s" : "")
      : "";
    return commitment_term || commitment_term_in_months
      ? commitment_term
        ? commitment_term + " (" + textInMonths + ")"
        : textInMonths
      : "N/A";
  };

  const handleChangeMargin = (event: React.ChangeEvent<HTMLInputElement>) => {
    const margin: number =
      event.target.value === ""
        ? 0
        : Number(Number(event.target.value).toFixed(2));
    let customer_cost: number =
      margin >= 0 && margin < 100
        ? round(editProduct.internal_cost / (1 - margin / 100), 2)
        : 0;
    let revenue: number = editProduct.quantity * customer_cost;
    let total_margin: number =
      editProduct.quantity * (customer_cost - editProduct.internal_cost);
    setEditProduct((prevState) => ({
      ...prevState,
      margin,
      customer_cost,
      revenue,
      total_margin,
    }));
  };

  const handleChangeQuantity = (event: React.ChangeEvent<HTMLInputElement>) => {
    const quantity: number =
      event.target.value === "" ? 0 : parseInt(event.target.value);
    // Find the associated pricing rates w.r.t quantity range
    const pricingData: IPricingData =
      currentProduct.pricing_data[curPricingIdx];
    const pricingInfo: IPricingRates = pricingData.pricing_rates.find((el) => {
      let start = !isNil(el.start_quantity_range) ? el.start_quantity_range : 0;
      let end = !isNil(el.end_quantity_range)
        ? el.end_quantity_range
        : Infinity;
      return quantity >= start && quantity <= end;
    });

    let internal_cost: number = round(pricingInfo.partner_buy_rate, 2);
    let customer_cost: number = Math.max(
      0,
      round(internal_cost / (1 - editProduct.margin / 100), 2)
    );
    let revenue: number = quantity * customer_cost;
    let total_margin: number = quantity * (customer_cost - internal_cost);

    setEditProduct((prevState) => ({
      ...prevState,
      ...pricingInfo,
      quantity,
      revenue,
      total_margin,
      internal_cost,
      customer_cost,
    }));
  };

  const findMinMaxQuantity = (productPricing: IPricingData) => {
    let min_quantity: number = Infinity,
      max_quantity: number = 0;
    productPricing.pricing_rates.forEach((el: IPricingRates) => {
      let start = !isNil(el.start_quantity_range) ? el.start_quantity_range : 0;
      let end = !isNil(el.end_quantity_range)
        ? el.end_quantity_range
        : Infinity;
      min_quantity = Math.min(min_quantity, start);
      max_quantity = Math.max(max_quantity, end);
    });
    if (max_quantity === Infinity) max_quantity = 10 ** 9;
    return [min_quantity, max_quantity];
  };

  const removeProduct = (idx: number) => {
    props.handleChangeProducts(
      props.products.filter((el, index) => idx !== index)
    );
  };

  const validateProduct = (): boolean => {
    return (
      curPricingIdx !== -1 &&
      editProduct.margin < 100 &&
      editProduct.margin >= 0 &&
      editProduct.billing_term !== PAX8BillingTerm.NOT_SET &&
      editProduct.quantity >= editProduct.min_quantity &&
      editProduct.quantity <= editProduct.max_quantity
    );
  };

  const saveProduct = () => {
    const products = props.products;
    props.handleChangeProducts([
      ...products.slice(0, curProductIdx),
      editProduct,
      ...products.slice(curProductIdx + 1),
    ]);
    closeModal();
  };

  const handleChangeComment = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newProducts = cloneDeep(props.products);
    newProducts[index].comment = e.target.value;
    props.handleChangeProducts(newProducts);
  };

  const handleDraggableChange = (products: IPAX8Product[]) => {
    props.handleChangeProducts(products);
  };

  const handleChangeOrderingCheckbox = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setIsOrdering(e.target.checked);
  };

  const onClickEdit = (product: IPAX8Product, idx: number) => {
    setLoading(true);
    props
      .fetchPAX8ProductDetails(product.product_crm_id)
      .then((action) => {
        if (action.type === FETCH_PAX8_PRODUCT_SUCCESS) {
          const productInfo: IPAX8ProductDetail = action.response;
          setCurProductIdx(idx);
          setCurrentProduct(productInfo);
          const pricingIdx = productInfo.pricing_data.findIndex(
            (el) =>
              el.billing_term === product.billing_term &&
              el.commitment_term === product.commitment_term &&
              el.commitment_term_in_months ===
                product.commitment_term_in_months &&
              el.pricing_type === product.pricing_type &&
              el.unit_of_measurement === product.unit_of_measurement
          );
          setCurPricingIdx(pricingIdx);
          setEditProduct(product);
          // If a product with no contract is selected show all pricing
          if (
            product.billing_term !== PAX8BillingTerm.NOT_SET &&
            product.commitment_term === null &&
            product.commitment_term_in_months === null
          )
            setShowAllPricing(true);
          setEditMode(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const onPricingChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newIdx = Number(e.target.value);
    const productPricing: IPricingData = currentProduct.pricing_data[newIdx];
    const [min_quantity, max_quantity] = findMinMaxQuantity(productPricing);
    const defaultPricing: IPricingRates = productPricing.pricing_rates[0];
    const internalCost = round(defaultPricing.partner_buy_rate, 2);
    const defaultCustomerCost = round(defaultPricing.suggested_retail_price, 2);

    setCurPricingIdx(newIdx);
    setEditProduct((prevState) => ({
      ...prevState,
      ...defaultPricing,
      min_quantity,
      max_quantity,
      billing_term: productPricing.billing_term,
      commitment_term: productPricing.commitment_term,
      commitment_term_in_months: productPricing.commitment_term_in_months,
      pricing_type: productPricing.pricing_type,
      unit_of_measurement: productPricing.unit_of_measurement,
      internal_cost: internalCost,
      customer_cost: defaultCustomerCost,
      quantity: defaultPricing.start_quantity_range,
      revenue: defaultPricing.start_quantity_range * defaultCustomerCost,
      total_margin:
        defaultPricing.start_quantity_range *
        (defaultCustomerCost - internalCost),
      margin:
        defaultCustomerCost !== 0
          ? round((1 - internalCost / defaultCustomerCost) * 100, 2)
          : 0,
    }));
  };

  const renderPAX8Product = ({
    product,
    idx,
  }: {
    product: IPAX8Product;
    idx: number;
  }) => {
    return (
      <div
        className={
          "row-panel" +
          `${product.error ? " pax-error-border" : ""}` +
          (isOrdering ? " ordering-mode" : "")
        }
        key={idx}
      >
        {product.is_comment ? (
          <div className="pax8-comment">
            <div className="pax8-comment-actions">
              <div className="pax8-icon-placeholder" />
              <SmallConfirmationBox
                onClickOk={() => removeProduct(idx)}
                text={"Comment"}
                showButton={true}
                title="Delete Comment"
              />
              <CopyToClipboard text={product.comment}>
                <IconButton
                  icon="copy.svg"
                  onClick={() => null}
                  newIcon={true}
                  title="Copy Comment"
                  className="product-copy"
                />
              </CopyToClipboard>
            </div>
            <Input
              field={{
                label: "",
                type: InputFieldType.TEXT,
                value: product.comment,
              }}
              width={12}
              name="comment"
              onChange={(e) => handleChangeComment(e, idx)}
              disabled={isOrdering}
              error={
                product.error && {
                  errorState: IValidationState.ERROR,
                  errorMessage: "Required",
                }
              }
              className={"pax8-comment-input"}
            />
          </div>
        ) : (
          <>
            <div className="cell width-10 pax8-btns">
              <EditButton
                title="Edit Product"
                onClick={() => onClickEdit(product, idx)}
              />
              {props.products.filter((el) => !el.is_comment).length > 1 && (
                <SmallConfirmationBox
                  className="remove"
                  showButton={true}
                  onClickOk={() => removeProduct(idx)}
                  text={"Product"}
                  title="Remove PAX8 Product"
                />
              )}
              <CopyToClipboard
                text={product.short_description + "\n" + product.sku}
                onCopy={() => {}}
              >
                <IconButton
                  icon="copy.svg"
                  onClick={() => null}
                  newIcon={true}
                  title="Copy line item"
                  className="product-copy"
                />
              </CopyToClipboard>
            </div>
            <div className="cell pax8-desc-sku-col text-ellipsis">
              <span
                className="desc-line text-ellipsis"
                title={product.product_name}
              >
                {product.product_name}
              </span>
              <span className="sku-line" title={product.sku}>
                {product.sku}
              </span>
            </div>
            <div
              className="cell pax8-quantity-col align-center"
              title={String(product.quantity)}
            >
              {product.quantity}
            </div>
            <div
              className="cell width-10 align-center"
              title={formatCurrency(product.customer_cost)}
            >
              {formatCurrency(product.customer_cost)}
            </div>
            <div
              className="cell width-10 align-center"
              title={String(product.margin)}
            >
              {product.margin + "%"}
            </div>
            <div
              className="cell width-10 align-center"
              title={getContractLength(
                product.commitment_term,
                product.commitment_term_in_months
              )}
            >
              {getContractLength(
                product.commitment_term,
                product.commitment_term_in_months
              )}
            </div>
            <div
              className="cell width-10 align-center"
              title={product.billing_term}
            >
              {product.billing_term}
            </div>
          </>
        )}
      </div>
    );
  };

  const renderPAX8ProductContainer = () => {
    return (
      <section className="pax8-products-container">
        <Spinner show={loading} className="pax8-agr-spinner" />
        <div className="pax8-quoted-products-section">
          <h3>
            <span className="heading-text">{props.pax8Name}</span>
            <SquareButton
              onClick={() => setShowCatalog(true)}
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Add Product</span>
                </>
              }
              bsStyle={ButtonStyle.OUTLINE}
              className={"pax8-add-product add-btn"}
            />
            <SquareButton
              onClick={() => props.addToQuotedProducts(undefined, true)}
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Comment</span>
                </>
              }
              bsStyle={ButtonStyle.OUTLINE}
              className={"add-btn pax8-add-product"}
            />
          </h3>
          <Checkbox
            isChecked={isOrdering}
            name="orderingProducts"
            onChange={(e) => handleChangeOrderingCheckbox(e)}
            className="order-checkbox"
          >
            Reorder Products
          </Checkbox>
          <div className="quoted-products-list infinite-list-component">
            <div className="header">
              <div className="cell width-10" />
              <div className="cell align-center">Product</div>
              <div className="cell pax8-quantity-col align-center">
                Quantity
              </div>
              <div className="cell width-10 align-center">
                Customer Cost Per.
              </div>
              <div className="cell width-10 align-center">Margin %</div>
              <div className="cell width-10 align-center">Contract Length</div>
              <div className="cell width-10 align-center">Billing Option</div>
            </div>
            <div className="quoted-products-rows">
              {isOrdering ? (
                <DraggableArea
                  isList
                  tags={props.products}
                  render={({ tag, index }) =>
                    renderPAX8Product({ product: tag, idx: index })
                  }
                  onChange={(tags) => handleDraggableChange(tags)}
                />
              ) : (
                props.products.map((product, idx) =>
                  renderPAX8Product({ product, idx })
                )
              )}
            </div>
          </div>
          {props.productsError.errorState === IValidationState.ERROR && (
            <div id="pax8-products-error">
              {props.productsError.errorMessage}
            </div>
          )}
        </div>
      </section>
    );
  };

  const renderPAX8ProductCatalogModal = () => {
    return (
      <ModalBase
        show={showCatalog}
        onClose={closeCatalogModal}
        titleElement={"PAX8 Products Catalog"}
        bodyElement={
          <div className="pax8-catalog-section">
            <InfiniteList
              showSearch={false}
              showIncludeExclude={true}
              id="pax8-products-catalog"
              className="pax8-infinite-list"
              height="auto"
              url={`providers/sales/agreement/pax8/products`}
              columns={
                [
                  {
                    name: "",
                    className: "width-10 align-center",
                    Cell: (pax8_product: IPAX8ProductDetail) => (
                      <IconButton
                        onClick={() => props.addToQuotedProducts(pax8_product)}
                        icon="plus.svg"
                        title="Add to Quoted Products"
                        className="add-to-quoted-list"
                        newIcon={true}
                      />
                    ),
                  },
                  {
                    id: "sku",
                    name: "Product SKU",
                    className: "pax8-sku-col",
                  },
                  {
                    id: "product_name",
                    name: "Name",
                    className: "pax8-name-col",
                  },
                  {
                    id: "short_description",
                    name: "Description",
                    className: "pax8-desc-col text-ellipsis",
                  },
                  {
                    id: "vendor_name",
                    name: "Vendor",
                    className: "pax8-vendor-col",
                  },
                ] as IColumnInfinite<IPAX8ProductDetail>[]
              }
            />
          </div>
        }
        footerElement={null}
        className={`pax8-product-edit-modal`}
      />
    );
  };

  const renderLabelWithDetails = useCallback(
    (width: number, label: string, detail: string) => {
      return (
        <div className={`field-section col-xs-${width}`}>
          <div className="field__label row">
            <div className="field__label-label">{label}</div>
          </div>
          <div className="detail">{detail}</div>
        </div>
      );
    },
    []
  );

  const renderEditProductModal = () => {
    // There are 2 cases for pricing information table display:
    // CASE 1: No contracts (commitment_term: null) for all data. In this case,
    //         show all data without giving a checkbox option
    // CASE 2: Some or all pricing objects have contracts/commitment_term.
    //         In this case, by default hide the pricing information with no contracts
    //         and if user wants to view all of them, then it can be viewed by using a checkbox

    const allEmptyContracts: boolean =
      currentProduct.pricing_data.filter(
        (el) =>
          el.commitment_term === null && el.commitment_term_in_months === null
      ).length === currentProduct.pricing_data.length;
    return (
      <ModalBase
        show={editMode}
        onClose={closeModal}
        titleElement={"Configure " + currentProduct.sku}
        bodyElement={
          <div className="pax8-product-edit-container">
            <div className="pax8-modal-row">
              {renderLabelWithDetails(6, "Name", currentProduct.product_name)}
              {renderLabelWithDetails(6, "Vendor", currentProduct.vendor_name)}
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(
                12,
                "Description",
                currentProduct.short_description
              )}
            </div>
            <div className="pax8-product-pricing">
              <h4>
                Pricing Information
                {!allEmptyContracts && (
                  <Checkbox
                    isChecked={showAllPricing}
                    name="showAllPricing"
                    onChange={() => {
                      setShowAllPricing((prev) => !prev);
                    }}
                    className="pax8-all-price"
                  >
                    View pricing data with no contracts
                  </Checkbox>
                )}
              </h4>
              <div className="pax8-pricing-table">
                <div className="pax8-pricing-header">
                  <div className="right-headers">
                    <div className="col-headers">Quantity Range</div>
                    <div className="col-headers">Partner Buy Rate</div>
                    <div className="col-headers">Suggested Price</div>
                    <div className="col-headers">Charge Type</div>
                  </div>
                </div>
                {currentProduct.pricing_data.map(
                  (pricing: IPricingData, idx: number) => (
                    <div
                      style={{
                        display:
                          allEmptyContracts ||
                          showAllPricing ||
                          pricing.commitment_term !== null ||
                          pricing.commitment_term_in_months !== null
                            ? "flex"
                            : "none",
                      }}
                      className="pax8-pricing-row radio-btn-group"
                      key={idx}
                    >
                      <div className="radio-btn">
                        <input
                          type="radio"
                          name={`pax8_pricing`}
                          onChange={(e) => onPricingChanged(e)}
                          value={`${idx}`}
                          checked={curPricingIdx === idx}
                        />
                      </div>
                      <div className="pricing-left-col">
                        <div className="left-col-details">
                          <label>Billing Term:</label>
                          <span>{pricing.billing_term}</span>
                        </div>
                        <div className="left-col-details">
                          <label>Contract Length:</label>
                          <span>
                            {getContractLength(
                              pricing.commitment_term,
                              pricing.commitment_term_in_months
                            )}
                          </span>
                        </div>
                        <div className="left-col-details">
                          <label>Pricing Type:</label>
                          <span>
                            {pricing.pricing_type
                              ? pricing.pricing_type
                              : "N/A"}
                          </span>
                        </div>
                        <div className="left-col-details">
                          <label>Unit of Measurement:</label>
                          <span>
                            {pricing.unit_of_measurement
                              ? pricing.unit_of_measurement
                              : "N/A"}
                          </span>
                        </div>
                      </div>
                      <div className="pricing-right-col">
                        {pricing.pricing_rates
                          .sort(
                            (a, b) =>
                              a.start_quantity_range - b.start_quantity_range
                          )
                          .map((rates: IPricingRates, idx: number) => (
                            <div className="rates-row" key={idx}>
                              <div className="rates-row-info">
                                {rates.start_quantity_range +
                                  (rates.end_quantity_range !== null
                                    ? " - " + rates.end_quantity_range
                                    : " & more")}
                              </div>
                              <div className="rates-row-info">
                                {formatCurrency(rates.partner_buy_rate)}
                              </div>
                              <div className="rates-row-info">
                                {formatCurrency(rates.suggested_retail_price)}
                              </div>
                              <div className="rates-row-info">
                                {rates.charge_type ? rates.charge_type : "N/A"}
                              </div>
                            </div>
                          ))}
                      </div>
                    </div>
                  )
                )}
              </div>
            </div>
            {curPricingIdx !== -1 && (
              <>
                <div className="pax8-modal-row">
                  {renderLabelWithDetails(
                    4,
                    "Billing Term",
                    editProduct.billing_term ? editProduct.billing_term : "N/A"
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Contract Length",
                    getContractLength(
                      editProduct.commitment_term,
                      editProduct.commitment_term_in_months
                    )
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Pricing Type",
                    editProduct.pricing_type ? editProduct.pricing_type : "N/A"
                  )}
                </div>
                <div className="pax8-modal-row" style={{ marginBottom: "0" }}>
                  <Input
                    field={{
                      label: "Quantity",
                      type: InputFieldType.NUMBER,
                      value: `${editProduct.quantity}`,
                      isRequired: true,
                    }}
                    width={4}
                    name="quantity"
                    minimumValue={String(editProduct.min_quantity)}
                    maximumValue={String(editProduct.max_quantity)}
                    onlyInteger={true}
                    placeholder={"Enter Quantity"}
                    className="pax8-product-quantity"
                    onChange={handleChangeQuantity}
                    error={
                      editProduct.quantity < editProduct.min_quantity ||
                      editProduct.quantity > editProduct.max_quantity
                        ? {
                            errorState: IValidationState.ERROR,
                            errorMessage: `Quantity range: ${editProduct.min_quantity} - ${editProduct.max_quantity}`,
                          }
                        : undefined
                    }
                  />
                  {renderLabelWithDetails(
                    4,
                    "Internal Cost (Partner Buy Cost)",
                    formatCurrency(editProduct.internal_cost)
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Suggested Price",
                    formatCurrency(editProduct.suggested_retail_price)
                  )}
                </div>
                <div className="pax8-modal-row" style={{ marginBottom: "0" }}>
                  <Input
                    field={{
                      label: "Margin %",
                      type: InputFieldType.NUMBER,
                      value: `${editProduct.margin}`,
                      isRequired: true,
                    }}
                    width={4}
                    name="margin"
                    placeholder={"Enter Margin (in %)"}
                    className="pax8-product-margin"
                    onChange={handleChangeMargin}
                    minimumValue={"0"}
                    maximumValue={"100"}
                    error={
                      editProduct.margin < 0 || editProduct.margin >= 100
                        ? {
                            errorState: IValidationState.ERROR,
                            errorMessage: "Margin should be between 0 to 100",
                          }
                        : undefined
                    }
                  />
                  {renderLabelWithDetails(
                    4,
                    "Margin per license",
                    formatCurrency(
                      editProduct.margin === 0
                        ? 0
                        : editProduct.customer_cost - editProduct.internal_cost
                    )
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Customer Price",
                    formatCurrency(editProduct.customer_cost)
                  )}
                </div>
                <div className="pax8-modal-row">
                  {renderLabelWithDetails(
                    4,
                    "Total Revenue",
                    formatCurrency(editProduct.revenue)
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Total Margin",
                    formatCurrency(editProduct.total_margin)
                  )}
                  {renderLabelWithDetails(4, "", "")}
                </div>
              </>
            )}
          </div>
        }
        footerElement={
          <>
            <SquareButton
              content={"Save Product"}
              bsStyle={ButtonStyle.PRIMARY}
              onClick={saveProduct}
              disabled={!validateProduct()}
            />
            <SquareButton
              content={"Cancel"}
              bsStyle={ButtonStyle.DEFAULT}
              onClick={closeModal}
            />
          </>
        }
        className={`pax8-product-edit-modal`}
      />
    );
  };

  return (
    <>
      {renderPAX8ProductContainer()}
      {editMode && renderEditProductModal()}
      {showCatalog && renderPAX8ProductCatalogModal()}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPAX8ProductDetails: (id: string) =>
    dispatch(fetchPAX8ProductDetails(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PAX8Products);
