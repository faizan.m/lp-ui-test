import React from "react";
import { connect } from "react-redux";
import {
  map,
  pick,
  cloneDeep,
  isNil,
  round,
  debounce,
  DebouncedFunc,
  random,
} from "lodash";

import {
  getTemplate,
  saveTemplate,
  getVendorsList,
  updateTemplate,
  getTemplateList,
  getBaseTemplate,
  getCategoryList,
  getTemplateByType,
  fetchServiceCategoriesFull,
  getShortServiceCatalogList,
  DOWNLOAD_SOW_SUCCESS,
  EDIT_TEMPLATE_SUCCESS,
  EDIT_TEMPLATE_FAILURE,
  CREATE_TEMPLATE_FAILURE,
  CREATE_TEMPLATE_SUCCESS,
  getVendorMappingList,
} from "../../../actions/sow";

import {
  getCustomerCost,
  getRecommendedHours,
  getSowCalculationFields,
  getCalculatedHourlyResource,
} from "../../../utils/sowCalculations";
import AppValidators from "../../../utils/validator";
import { DraggableArea } from "react-draggable-tags";
import { commonFunctions } from "../../../utils/commonFunctions";
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
import AddVendor from "../Sow/addVendor";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import Checkbox from "../../../components/Checkbox/checkbox";
import SquareButton from "../../../components/Button/button";
import { fetchSOWDOCSetting } from "../../../actions/setting";
import IconButton from "../../../components/Button/iconButton";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import SelectInput from "../../../components/Input/Select/select";
import { QuillEditorAcela } from "../../../components/QuillEditor/QuillEditor";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import EditButton from "../../../components/Button/editButton";
import VendorMappingModal from "../../SettingSOW/vendorMappingModal";
import "../../../commonStyles/serviceCostCalculations.scss";
import "./style.scss";
import PromptUnsaved from "../../../components/UnsavedWarning/PromptUnsaved";

interface IAddTemplateProps extends ICommonProps {
  template: ISoWTemplate;
  user: ISuperUser;
  isFetching: boolean;
  docSetting: IDOCSetting;
  isFetchingVendors: boolean;
  isFetchingTemplate: boolean;
  isFetchingCategory: boolean;
  categoryList: ICategoryList[];
  vendorOptions: IPickListOptions[];
  vendorMapping: IVendorAliasMapping[];
  serviceCatalogShortList: IserviceCatalogShort[];
  serviceCatalogCategories: IserviceTechnologyTypes[];
  baseTemplates: { base_config: IJSONConfig; change_request_config: any };
  getVendorsList: () => Promise<any>;
  getBaseTemplate: () => Promise<any>;
  getTemplateList: () => Promise<any>;
  getCategoryList: () => Promise<any>;
  getVendorMapping: () => Promise<any>;
  fetchSOWDOCSetting: () => Promise<any>;
  fetchServiceCategoriesFull: () => Promise<any>;
  getShortServiceCatalogList: () => Promise<any>;
  getTemplate: (id: number) => Promise<any>;
  saveTemplate: (template: ISoWTemplate) => Promise<any>;
  updateTemplate: (template: ISoWTemplate) => Promise<any>;
  getTemplateByType: (payload: ISoWTemplate, type: SowDocType) => Promise<any>;
}

interface IAddTemplateState {
  showHidden: boolean;
  errorList?: object;
  template: ISoWTemplate;
  isopenConfirm: boolean;
  id: string;
  isValid: boolean;
  showError: boolean;
  isCollapsed: { [fieldName: string]: boolean };
  orderingPhase: boolean;
  orderingHourlyResources: boolean;
  showVendorModal: boolean;
  phasesGroupedByResources: object;
  phaseResources: IPickListOptions[];
  sowCalculations: ISoWCalculationFields;
  errorsLabel: {
    [fieldName: string]: IFieldValidation;
  };
  errorsValue: {
    [fieldName: string]: IFieldValidation;
  };
  error: {
    name: IFieldValidation;
    category: IFieldValidation;
    base_template: IFieldValidation;
    json_config: IFieldValidation;
    service_catalog_category: IFieldValidation;
    linked_service_catalog: IFieldValidation;
    engineering_hourly_rate: IFieldValidation;
    integration_technician_hourly_rate: IFieldValidation;
    after_hours_rate: IFieldValidation;
    project_management_hourly_rate: IFieldValidation;
  };
  saving: boolean;
  loading: boolean;
  versionObj: boolean;
  openPreview: boolean;
  previewHTML: string;
  technologyTypes: IPickListOptions[];
  vendorDescriptionMapping: Map<number, IVendorAliasMapping>;
  showVendorMappingModal: boolean;
  currentVendorMapping: IVendorAliasMapping;
  unsaved: boolean;
}

class AddTemplate extends React.Component<
  IAddTemplateProps,
  IAddTemplateState
> {
  static EmptyHourlyResource: IHourlyResource = {
    hours: 0,
    override: false,
    is_hidden: false,
    hourly_rate: 0,
    hourly_cost: 0,
    resource_id: null,
    resource_name: "",
    internal_cost: 0,
    customer_cost: 0,
    margin: 0,
    margin_percentage: 0,
    resource_description: null,
  };
  private debouncedCalculations: DebouncedFunc<() => void>;

  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: IAddTemplateProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedCalculations = debounce(this.doTemplateCalculations, 1000);
  }

  getEmptyState = () => ({
    template: {
      json_config: null,
      name: "",
      id: 0,
      linked_service_catalog: null,
      update_version: false,
      major_version: 0,
      minor_version: 0,
      version_description: "",
      category: null,
    },
    showHidden: false,
    isopenConfirm: false,
    orderingPhase: false,
    orderingHourlyResources: false,
    showVendorModal: false,
    phasesGroupedByResources: {},
    phaseResources: [
      { value: -1, label: "Engineer" },
      { value: -2, label: "After Hours Engineer" },
      { value: -3, label: "Project Management" },
      { value: -4, label: "Integration Technician" },
    ],
    vendorDescriptionMapping: new Map<number, IVendorAliasMapping>(),
    showVendorMappingModal: false,
    currentVendorMapping: {
      vendor_name: "",
      vendor_crm_id: null,
      resource_description: "",
    },
    id: "",
    isCollapsed: {},
    errorsLabel: {},
    errorsValue: {},
    isValid: false,
    showError: false,
    loading: false,
    saving: false,
    error: {
      name: { ...AddTemplate.emptyErrorState },
      category: { ...AddTemplate.emptyErrorState },
      base_template: { ...AddTemplate.emptyErrorState },
      json_config: { ...AddTemplate.emptyErrorState },
      service_catalog_category: { ...AddTemplate.emptyErrorState },
      linked_service_catalog: { ...AddTemplate.emptyErrorState },
      engineering_hourly_rate: { ...AddTemplate.emptyErrorState },
      after_hours_rate: { ...AddTemplate.emptyErrorState },
      integration_technician_hourly_rate: { ...AddTemplate.emptyErrorState },
      project_management_hourly_rate: { ...AddTemplate.emptyErrorState },
    },
    technologyTypes: [],
    versionObj: false,
    openPreview: false,
    previewHTML: null,
    sowCalculations: {
      engineeringHoursInternalCost: 0,
      engineeringHoursCustomerCost: 0,
      engineeringHoursMargin: 0,
      engineeringHoursMarginPercent: 0,
      afterHoursInternalCost: 0,
      afterHoursCustomerCost: 0,
      afterHoursMargin: 0,
      afterHoursMarginPercent: 0,
      integrationTechnicianInternalCost: 0,
      integrationTechnicianCustomerCost: 0,
      integrationTechnicianMargin: 0,
      integrationTechnicianMarginPercent: 0,
      pmHoursInternalCost: 0,
      pmHoursCustomerCost: 0,
      pmHoursMargin: 0,
      pmHoursMarginPercent: 0,
      totalCustomerCost: 0,
      totalInternalCost: 0,
      totalMargin: 0,
      totalMarginPercent: 0,
      hourlyLaborCustomerCost: 0,
      hourlyLaborInternalCost: 0,
      hourlyLaborMargin: 0,
      hourlyLaborMarginPercent: 0,
      riskBudgetCustomerCost: 0,
      riskBudgetInternalCost: 0,
      riskBudgetMargin: 0,
      riskBudgetMarginPercent: 0,
      contractorCustomerCost: 0,
      contractorInternalCost: 0,
      contractorMargin: 0,
      contractorMarginPercent: 0,
      proSerCustomerCost: 0,
      proSerInternalCost: 0,
      proSerMargin: 0,
      proSerMarginPercent: 0,
      travelCost: 0,
    },
    unsaved: false,
  });

  componentDidMount() {
    if (!this.props.categoryList) {
      this.props.getCategoryList();
    }
    this.props.getVendorsList();
    this.props.getTemplateList();
    this.props.fetchSOWDOCSetting();
    this.props.fetchServiceCategoriesFull();
    this.props.getShortServiceCatalogList();
    this.props.getVendorMapping();
    const id = this.props.match.params.id;
    const query = new URLSearchParams(this.props.location.search);
    const versionObj = query.get("versionobj");
    if (versionObj) {
      const obj = sessionStorage.getItem("versionobj") as any;
      let sowObj = JSON.parse(obj);
      const json_config: IJSONConfig = sowObj.json_config;
      this.setPhaseIds(json_config);
      this.setHourlyResourceIds(json_config);
      this.setHourlyResourceDescription(json_config);
      const template: ISoWTemplate = {
        json_config,
        name: sowObj.name,
        service_catalog_category: sowObj.service_catalog_category_id,
        category: sowObj.category_id,
        id: sowObj.id,
        author_name: sowObj.author_name ? sowObj.author_name : "N.A.",
        updated_on: sowObj.updated_on ? sowObj.updated_on : "N.A.",
        updated_by_name: sowObj.updated_by_name,
        update_version: false,
        major_version: sowObj.major_version,
        minor_version: sowObj.minor_version,
        linked_service_catalog: null,
      };

      this.setState({
        template,
        phaseResources: this.getPhaseResources(
          json_config.service_cost.hourly_resources
        ),
        isCollapsed: Object.keys(template.json_config).reduce(
          (prev, cur) => ({ ...prev, [cur]: false }),
          {}
        ),
        versionObj: true,
      });
    } else {
      if (id !== "0") {
        this.props.getTemplate(id);
      } else {
        this.props.getBaseTemplate();
      }
    }
  }

  componentDidUpdate(prevProps: IAddTemplateProps) {
    if (this.props.template && this.props.template !== prevProps.template) {
      const template = this.props.template;
      let unsaved = false;
      this.setPhaseIds(template.json_config);
      this.setHourlyResourceIds(template.json_config);
      this.setHourlyResourceDescription(template.json_config);
      const query = new URLSearchParams(prevProps.location.search);
      const cloned = query.get("cloned");
      if (cloned === "true") {
        template.id = 0;
        template.name = this.props.template.name + " (cloned)";
        template.major_version = 1;
        template.minor_version = 0;
        unsaved = true;
      }

      let types = [];
      if (prevProps.categoryList) {
        const types1 = prevProps.categoryList.filter(
          (type) => template.category === type.id
        );
        types =
          types1 &&
          types1[0].technology_types.map((t) => ({
            value: t.id,
            label: t.name,
            disabled: false,
          }));
      }
      this.setState(
        {
          technologyTypes: types,
          template,
          phaseResources: this.getPhaseResources(
            template.json_config.service_cost.hourly_resources
          ),
          isCollapsed: Object.keys(template.json_config).reduce(
            (prev, cur) => ({ ...prev, [cur]: false }),
            {}
          ),
          unsaved,
        },
        () => {
          this.doTemplateCalculations();
          this.setPhasesGroupedByResources();
        }
      );
    }
    if (
      this.props.baseTemplates &&
      this.props.baseTemplates !== prevProps.baseTemplates
    ) {
      const newState = cloneDeep(this.state);
      newState.template.json_config = this.props.baseTemplates.base_config;
      if (
        prevProps.docSetting &&
        newState.template.json_config &&
        newState.template.json_config.service_cost
      ) {
        if (
          newState.template.json_config.service_cost.engineering_hourly_rate ===
          0
        ) {
          newState.template.json_config.service_cost.engineering_hourly_rate = this.props.docSetting.engineering_hourly_rate;
        }
        if (
          newState.template.json_config.service_cost.engineering_hourly_rate ===
          0
        ) {
          newState.template.json_config.service_cost.engineering_hourly_rate = this.props.docSetting.engineering_hourly_rate;
        }
        if (newState.template.json_config.service_cost.after_hours_rate === 0) {
          newState.template.json_config.service_cost.after_hours_rate = this.props.docSetting.after_hours_rate;
        }
        if (
          newState.template.json_config.service_cost
            .integration_technician_hourly_rate === 0
        ) {
          newState.template.json_config.service_cost.integration_technician_hourly_rate = this.props.docSetting.integration_technician_hourly_rate;
        }
        if (
          newState.template.json_config.service_cost
            .project_management_hourly_rate === 0
        ) {
          newState.template.json_config.service_cost.project_management_hourly_rate = this.props.docSetting.project_management_hourly_rate;
        }
        if (
          newState.template.json_config.project_management_fixed_fee[
            JSONConfigSectionKeys.project_management
          ].value === ""
        ) {
          newState.template.json_config.project_management_fixed_fee[
            JSONConfigSectionKeys.project_management
          ].value = this.props.docSetting.project_management_fixed_fee;
        }
        if (
          newState.template.json_config.project_management_t_and_m[
            JSONConfigSectionKeys.project_management
          ].value === ""
        ) {
          newState.template.json_config.project_management_t_and_m[
            JSONConfigSectionKeys.project_management
          ].value = this.props.docSetting.project_management_t_and_m;
        }
        if (
          newState.template.json_config.terms_fixed_fee[
            JSONConfigSectionKeys.terms
          ].value === ""
        ) {
          newState.template.json_config.terms_fixed_fee[
            JSONConfigSectionKeys.terms
          ].value = this.props.docSetting.terms_fixed_fee;
        }
        if (
          newState.template.json_config.terms_t_and_m[
            JSONConfigSectionKeys.terms
          ].value === ""
        ) {
          newState.template.json_config.terms_t_and_m[
            JSONConfigSectionKeys.terms
          ].value = this.props.docSetting.terms_t_and_m;
        }
      }
      if (newState.template && newState.template.json_config) {
        (newState.isCollapsed as object) = Object.keys(
          newState.template.json_config
        ).reduce((prev, cur) => ({ ...prev, [cur]: false }), {});
      }
      this.setPhaseIds(newState.template.json_config);
      newState.template.json_config.service_cost.total_hours = this.calculateTotalHours(
        newState.template.json_config.service_cost
      );
      this.setState(newState);
    }
    if (
      this.props.docSetting &&
      this.props.docSetting !== prevProps.docSetting &&
      this.state.template.json_config &&
      this.state.template.json_config.service_cost
    ) {
      const newState = cloneDeep(this.state);

      if (
        newState.template.json_config.service_cost.engineering_hourly_rate === 0
      ) {
        newState.template.json_config.service_cost.engineering_hourly_rate = this.props.docSetting.engineering_hourly_rate;
      }
      if (newState.template.json_config.service_cost.after_hours_rate === 0) {
        newState.template.json_config.service_cost.after_hours_rate = this.props.docSetting.after_hours_rate;
      }
      if (
        newState.template.json_config.service_cost
          .integration_technician_hourly_rate === 0
      ) {
        newState.template.json_config.service_cost.integration_technician_hourly_rate = this.props.docSetting.integration_technician_hourly_rate;
      }
      if (
        newState.template.json_config.service_cost
          .project_management_hourly_rate === 0
      ) {
        newState.template.json_config.service_cost.project_management_hourly_rate = this.props.docSetting.project_management_hourly_rate;
      }
      if (
        newState.template.json_config.project_management_fixed_fee[
          JSONConfigSectionKeys.project_management
        ].value === ""
      ) {
        newState.template.json_config.project_management_fixed_fee[
          JSONConfigSectionKeys.project_management
        ].value = this.props.docSetting.project_management_fixed_fee;
      }
      if (
        newState.template.json_config.project_management_t_and_m[
          JSONConfigSectionKeys.project_management
        ].value === ""
      ) {
        newState.template.json_config.project_management_t_and_m[
          JSONConfigSectionKeys.project_management
        ].value = this.props.docSetting.project_management_t_and_m;
      }
      if (
        newState.template.json_config.terms_fixed_fee[
          JSONConfigSectionKeys.terms
        ].value === ""
      ) {
        newState.template.json_config.terms_fixed_fee[
          JSONConfigSectionKeys.terms
        ].value = this.props.docSetting.terms_fixed_fee;
      }
      if (
        newState.template.json_config.terms_t_and_m[JSONConfigSectionKeys.terms]
          .value === ""
      ) {
        newState.template.json_config.terms_t_and_m[
          JSONConfigSectionKeys.terms
        ].value = this.props.docSetting.terms_t_and_m;
      }
      if (this.props.template && this.props.template.json_config) {
        (newState.isCollapsed as object) = Object.keys(
          newState.template.json_config
        ).reduce((prev, cur) => ({ ...prev, [cur]: false }), {});
      }
      this.setState(newState);
    }
    if (
      this.props.vendorMapping &&
      this.props.vendorMapping !== prevProps.vendorMapping
    ) {
      const vendorMapping = new Map<number, IVendorAliasMapping>();
      this.props.vendorMapping.forEach((el) => {
        vendorMapping.set(el.vendor_crm_id, el);
      });
      this.setState({ vendorDescriptionMapping: vendorMapping }, () => {
        if (this.state.template.json_config) {
          const template = cloneDeep(this.state.template);
          this.setHourlyResourceDescription(template.json_config);
          this.setState({ template });
        }
      });
    }
  }

  setValidationErrors = (errorList: object) => {
    const newState: IAddTemplateState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  onChecboxChangeVersionUpdate = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.update_version = event.target.checked;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  getServiceCatalogsOptions = () => {
    const serviceCatalogShortList = this.props.serviceCatalogShortList;
    let templates = [];
    if (serviceCatalogShortList && this.state.template.category) {
      templates = serviceCatalogShortList.filter(
        (id) => this.state.template.category === id.service_type.id
      );
    }
    if (
      serviceCatalogShortList &&
      this.state.template.category &&
      this.state.template.service_catalog_category
    ) {
      templates = templates.filter(
        (id) =>
          this.state.template.service_catalog_category ===
          id.service_category.id
      );
    }
    const templatesList = templates
      ? templates.map((t) => ({
          value: t.id,
          label: t.service_name,
          disabled: false,
        }))
      : [];

    templatesList.push({ label: "None", value: "null", disabled: false });

    return templatesList;
  };

  getCategoryOptions = () => {
    const categories = this.props.categoryList
      ? this.props.categoryList.map((cat) => ({
          value: cat.id,
          label: cat.name,
          disabled: cat.name === SowDocType.CHANGE_REQUEST,
        }))
      : [];

    return categories;
  };

  setPhaseIds = (jsonConfig: IJSONConfig) => {
    if (
      jsonConfig.service_cost.engineering_hours_breakup &&
      jsonConfig.service_cost.engineering_hours_breakup.phases
    ) {
      jsonConfig.service_cost.engineering_hours_breakup.phases.forEach(
        (phase: IPhase) => {
          phase.id = random(1, 100000);
        }
      );
    }
  };

  setHourlyResourceIds = (jsonConfig: IJSONConfig) => {
    if (jsonConfig.service_cost.hourly_resources) {
      jsonConfig.service_cost.hourly_resources.forEach((resource) => {
        resource.id = random(1, 100000);
      });
    }
  };

  setHourlyResourceDescription = (jsonConfig: IJSONConfig) => {
    if (jsonConfig && jsonConfig.service_cost.hourly_resources) {
      jsonConfig.service_cost.hourly_resources.forEach((resource) => {
        if (this.state.vendorDescriptionMapping.has(resource.resource_id))
          resource.resource_description = this.state.vendorDescriptionMapping.get(
            resource.resource_id
          ).resource_description;
        else resource.resource_description = null;
      });
    }
  };

  getPhaseResources = (
    hourlyResources: IHourlyResource[]
  ): IPickListOptions[] => {
    let vendorResources: IPickListOptions[] = [];
    const uniqueIds: Set<number> = new Set(); // For avoiding repetitive phases
    if (hourlyResources) {
      hourlyResources.forEach((el) => {
        if (!uniqueIds.has(el.resource_id))
          vendorResources.push({
            value: el.resource_id,
            label: el.resource_name,
          });
        uniqueIds.add(el.resource_id);
      });
    }
    return [
      { value: -1, label: "Engineer" },
      { value: -2, label: "After Hours Engineer" },
      { value: -3, label: "Project Management" },
      { value: -4, label: "Integration Technician" },
      ...vendorResources,
    ];
  };

  calculateTotalHours = (serviceCost: IServiceCost): number => {
    let total_hours: number =
      serviceCost.after_hours +
      serviceCost.engineering_hours +
      serviceCost.integration_technician_hours +
      serviceCost.project_management_hours;
    if (serviceCost.hourly_resources)
      serviceCost.hourly_resources.forEach((el) => {
        total_hours += el.hours ? el.hours : 0;
      });
    return total_hours;
  };

  handleChangeCategory = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.template[e.target.name] = e.target.value;
    let types: IPickListOptions[] = [];
    if (e.target.value !== "" && this.props.categoryList) {
      const types1 = this.props.categoryList.filter(
        (type) => Number(e.target.value) === type.id
      );

      types =
        types1 &&
        types1[0].technology_types.map((t) => ({
          value: t.id,
          label: t.name,
          disabled: false,
        }));
    }
    (newState.technologyTypes as IPickListOptions[]) = types;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  doTemplateCalculations = () => {
    if (this.state.template.json_config && this.props.docSetting)
      this.setState({
        sowCalculations: getSowCalculationFields(
          this.state.template.json_config.service_cost,
          this.props.docSetting,
          SowDocType.FIXED_FEE
        ),
      });
  };

  handleChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: string,
    field: string,
    type: string
  ) => {
    const newState = cloneDeep(this.state);
    (newState.template.json_config[key][field][
      type
    ] as string) = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeSubSection = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx: number,
    field: string,
    type: string
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.implementation_logistics.sections[idx][field][
      type
    ] = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeSimpleFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prevState) => ({
      template: { ...prevState.template, [e.target.name]: e.target.value },
      unsaved: true,
    }));
  };

  handleChangeMarkdown = (
    html: string,
    key: string,
    field: string,
    type: string
  ) => {
    const newState = cloneDeep(this.state);
    (newState.template.json_config[key][field][type] as string) = html;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleClickResetMarkdown = (key: string, field: string, type: string) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config[key][field][type] = this.props.docSetting[
      key
    ];
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  getCategoryList = () => {
    const serviceCatalogCategories = this.props.serviceCatalogCategories
      ? this.props.serviceCatalogCategories.map((t) => ({
          value: t.id,
          label: t.name,
          disabled: t.is_disabled,
        }))
      : [];

    return serviceCatalogCategories;
  };

  handlePhaseReorder = (phases: IPhase[]) => {
    const newState = cloneDeep(this.state);
    (newState.unsaved as boolean) = true;
    newState.template.json_config.service_cost.engineering_hours_breakup.phases = phases;
    this.setState(newState);
  };

  handleHourlyResourceReorder = (hourlyResources: IHourlyResource[]) => {
    const newState = cloneDeep(this.state);
    (newState.unsaved as boolean) = true;
    newState.template.json_config.service_cost.hourly_resources = hourlyResources;
    this.setState(newState);
  };

  // render methods
  renderTemplateByJson = (rawJson: IJSONConfig) => {
    let count = 0;
    const json: IJSONConfig = Object.assign({}, rawJson);
    delete json.service_cost;

    return (
      <div className="template-fields col-md-12 row">
        {json &&
          Object.keys(json).length > 0 &&
          Object.keys(json)
            .filter(
              (field) =>
                // Should this be hidden in SoW Template
                // (currently for Implementation Logistics)
                !json[field].hide_in_sow &&
                // For showing the hidden sections
                (this.state.showHidden === true
                  ? true
                  : !json[field].default_hidden)
            )
            .sort((a, b) => json[a].ordering - json[b].ordering)
            .map((key, fieldIndex) => {
              const isCollapsed = !this.state.isCollapsed[key];
              const toggleCollapsedState = () =>
                this.setState((prevState) => ({
                  isCollapsed: { ...prevState.isCollapsed, [key]: isCollapsed },
                }));

              return (
                <div
                  className={`field ${isCollapsed ? "field--collapsed" : ""}`}
                  key={fieldIndex}
                >
                  <div
                    className="section-heading"
                    onClick={toggleCollapsedState}
                  >
                    {json[key].section_label}{" "}
                    <div className="action-collapse">
                      {isCollapsed ? "+" : "-"}
                    </div>
                  </div>
                  <div className="body-section">
                    {" "}
                    {key &&
                      Object.keys(json[key])
                        .sort(
                          (a, b) =>
                            ((json[key][a] && json[key][a].ordering) || 0) -
                            ((json[key][b] && json[key][b].ordering) || 0)
                        )
                        .map((field, i) => {
                          if (
                            field === "section_label" ||
                            field === "visible_in" ||
                            field === "ordering" ||
                            field === "default_hidden" ||
                            field === "hide_in_sow"
                          ) {
                            return false;
                          }

                          count++;

                          return key === "implementation_logistics" &&
                            json.implementation_logistics.sections &&
                            json.implementation_logistics.sections.length >
                              0 ? (
                            this.renderImplementationLogisticsSections(
                              json.implementation_logistics.sections
                            )
                          ) : (
                            <div
                              className={`${
                                json[key][field].type === "MARKDOWN"
                                  ? "single-field col-lg-12 col-md-12 col-xs-12"
                                  : "single-field col-lg-12 col-md-12 col-xs-12"
                              }`}
                              key={i}
                            >
                              {json[key][field].type === "MARKDOWN" && (
                                <div
                                  className={`${
                                    this.state.errorsValue &&
                                    this.state.errorsValue[count] &&
                                    this.state.errorsValue[count].errorMessage
                                      ? "markdown markdown-error"
                                      : "markdown"
                                  }`}
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: InputFieldType.TEXT,
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  {json[key][field].reset && (
                                    <label
                                      className="img-button-reset"
                                      onClick={() =>
                                        this.handleClickResetMarkdown(
                                          key,
                                          field,
                                          "value"
                                        )
                                      }
                                    >
                                      <img
                                        className="icon-reset"
                                        src="/assets/icons/reset.svg"
                                        title="Reset to default from setting"
                                      />
                                    </label>
                                  )}
                                  <QuillEditorAcela
                                    onChange={(e) =>
                                      this.handleChangeMarkdown(
                                        e,
                                        key,
                                        field,
                                        "value"
                                      )
                                    }
                                    value={json[key][field].value}
                                    wrapperClass={"template-config-quill"}
                                    label={`${
                                      json[key][field].is_label_editable ===
                                      "true"
                                        ? "Value"
                                        : json[key][field].label
                                    }`}
                                    isRequired={
                                      String(json[key][field].is_required) ===
                                      "true"
                                    }
                                    scrollingContainer=".add-template"
                                    error={
                                      this.state.errorsValue &&
                                      this.state.errorsValue[count] &&
                                      this.state.errorsValue[count].errorMessage
                                        ? {
                                            errorState: IValidationState.ERROR,
                                            errorMessage: this.state
                                              .errorsValue[count].errorMessage,
                                          }
                                        : undefined
                                    }
                                  />
                                </div>
                              )}
                              {json[key][field].type === "TEXTBOX" && (
                                <div className="input-label-box">
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: InputFieldType.TEXT,
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  <Input
                                    field={{
                                      value: json[key][field].value,
                                      label: `${
                                        json[key][field].is_label_editable ===
                                        "true"
                                          ? "Value"
                                          : json[key][field].label
                                      }`,
                                      type: InputFieldType.TEXT,
                                      isRequired:
                                        json[key][field].is_required &&
                                        JSON.parse(
                                          json[key][field].is_required
                                        ),
                                    }}
                                    width={8}
                                    name={field}
                                    onChange={(e) =>
                                      this.handleChange(e, key, field, "value")
                                    }
                                    error={this.state.errorsValue[count]}
                                  />
                                </div>
                              )}
                              {json[key][field].type === "RADIOBOX" && (
                                <div
                                  key={field}
                                  className="options input-label-box"
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: InputFieldType.TEXT,
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}

                                  <div className="options-content">
                                    <Input
                                      field={{
                                        value: json[key][field].value,
                                        label:
                                          json[key][field].is_label_editable ===
                                          "false"
                                            ? json[key][field].label
                                            : "",
                                        type: InputFieldType.RADIO,
                                        isRequired: false,
                                        options: json[key][field].options.map(
                                          (role: string) => ({
                                            value: role,
                                            label: role,
                                          })
                                        ),
                                      }}
                                      width={8}
                                      name={field + count}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "value"
                                        )
                                      }
                                      error={this.state.errorsValue[count]}
                                    />
                                  </div>
                                </div>
                              )}
                            </div>
                          );
                        })}
                  </div>
                </div>
              );
            })}
        {this.state.error.json_config.errorMessage && (
          <div className="board-error">
            {this.state.error.json_config.errorMessage}
          </div>
        )}
      </div>
    );
  };

  getColWidth = (type: string) => {
    let width = "col-md-6";
    switch (type) {
      case "MARKDOWN":
        width = "col-md-6";
        break;
      case "TEXTBOX":
        width = "col-md-5";
        break;
      case "TEXTAREA":
        width = "col-md-5";
        break;
      case "RADIOBOX":
        width = "col-md-2";
        break;

      default:
        break;
    }

    return width;
  };

  renderImplementationLogisticsSections = (
    sections: {
      column1?: IJSONConfigSectionInfo;
      column2?: IJSONConfigSectionInfo;
      column3?: IJSONConfigSectionInfo;
      column4?: IJSONConfigSectionInfo;
      delivery_model: IJSONConfigSectionInfo;
      detail: IJSONConfigSectionInfo;
    }[]
  ) => {
    let count = 0;
    return (
      <div className="sub-section-fields col-md-12" key={1000}>
        {sections.map((section, fieldIndex) => {
          return (
            <div className={`sub-section col-md-12`} key={fieldIndex}>
              {Object.keys(section)
                .sort(
                  (a, b) =>
                    (section[a].ordering || 0) - (section[b].ordering || 0)
                )
                .map((field, i) => {
                  count++;

                  return (
                    <div
                      className={`${this.getColWidth(section[field].type)}`}
                      key={i}
                    >
                      {section[field].type === "TEXTBOX" && (
                        <div className="input-label-box">
                          <Input
                            field={{
                              value: section[field].value,
                              label: `${
                                section[field].is_label_editable === "true"
                                  ? "Value"
                                  : section[field].label
                              }`,
                              type: InputFieldType.TEXT,
                              isRequired:
                                section[field].is_required &&
                                JSON.parse(section[field].is_required),
                            }}
                            width={12}
                            name={field}
                            onChange={(e) =>
                              this.handleChangeSubSection(
                                e,
                                fieldIndex,
                                field,
                                "value"
                              )
                            }
                            error={section[field].error && section[field].error}
                          />
                        </div>
                      )}
                      {section[field].type === "TEXTAREA" && (
                        <div className="input-label-box">
                          <Input
                            field={{
                              value: section[field].value,
                              label: `${
                                section[field].is_label_editable === "true"
                                  ? "Value"
                                  : section[field].label
                              }`,
                              type: InputFieldType.TEXTAREA,
                              isRequired:
                                section[field].is_required &&
                                JSON.parse(section[field].is_required),
                            }}
                            width={12}
                            name={field}
                            onChange={(e) =>
                              this.handleChangeSubSection(
                                e,
                                fieldIndex,
                                field,
                                "value"
                              )
                            }
                            error={section[field].error && section[field].error}
                          />
                        </div>
                      )}
                      {section[field].type === "RADIOBOX" && (
                        <div key={field} className="options input-label-box">
                          <div className="options-content">
                            <Input
                              field={{
                                value: section[field].value,
                                label:
                                  section[field].is_label_editable === "false"
                                    ? section[field].label
                                    : "",
                                type: InputFieldType.RADIO,
                                isRequired: false,
                                options: section[field].options.map(
                                  (role: string) => ({
                                    value: role,
                                    label: role,
                                  })
                                ),
                              }}
                              width={12}
                              name={field + count}
                              onChange={(e) =>
                                this.handleChangeSubSection(
                                  e,
                                  fieldIndex,
                                  field,
                                  "value"
                                )
                              }
                              error={
                                section[field].error && section[field].error
                              }
                            />
                          </div>
                        </div>
                      )}
                    </div>
                  );
                })}
            </div>
          );
        })}
        {this.state.error.json_config.errorMessage && (
          <div className="board-error">
            {this.state.error.json_config.errorMessage}
          </div>
        )}
      </div>
    );
  };

  renderPhase = ({ tag, index }: { tag: IPhase; index: number }) => {
    return (
      <div
        className={
          "col-md-12 mapping-row" +
          (this.state.orderingPhase ? " ordering-phase" : "")
        }
        key={index}
      >
        <Input
          field={{
            label: "",
            type: InputFieldType.TEXT,
            value: tag.name,
            isRequired: false,
          }}
          width={4}
          multi={true}
          disabled={tag.default || this.state.orderingPhase}
          name="name"
          onChange={(e) => this.handleChangeStaticCalculationsPhases(e, index)}
          placeholder={`Enter Name`}
          className="phase-name"
          error={tag.errorName}
        />
        <div className="override-img col-md-1">
          <img
            className="daily-check-passed"
            alt=""
            src={`/assets/icons/${tag.hours === 0 ? "delete" : "check"}.png`}
            title=""
          />
        </div>
        <Input
          field={{
            label: "",
            type: InputFieldType.NUMBER,
            value: tag.hours,
            isRequired: false,
          }}
          width={2}
          name="hours"
          minimumValue={"0"}
          disabled={this.state.orderingPhase}
          onChange={(e) => this.handleChangeStaticCalculationsPhases(e, index)}
          placeholder={`Hours`}
        />
        <Input
          field={{
            label: "",
            type: InputFieldType.PICKLIST,
            options: this.state.phaseResources,
            value: tag.resource ? tag.resource : -1,
            isRequired: false,
          }}
          width={4}
          multi={false}
          name="resource"
          onChange={(e) => this.handleChangeStaticCalculationsPhases(e, index)}
          placeholder={`Select Resource`}
          loading={this.props.isFetchingVendors}
          disabled={this.state.orderingPhase}
          error={tag.errorResource}
        />
        {!this.state.orderingPhase && (
          <SmallConfirmationBox
            showButton={true}
            onClickOk={() => this.deleteCalculationsPhases(index)}
            className="col-md-1"
            text="phase"
          />
        )}
      </div>
    );
  };

  renderHourlyResource = ({
    tag,
    index,
  }: {
    tag: IHourlyResource;
    index: number;
  }) => {
    return (
      <div
        className={
          "col-md-12 mapping-row" +
          (this.state.orderingHourlyResources ? " ordering-phase" : "")
        }
        key={index}
      >
        {!this.state.orderingHourlyResources && (
          <EditButton
            title="Edit Default Description"
            onClick={() => this.handleClickEditDescription(tag)}
          />
        )}
        <Input
          field={{
            label: "",
            type: InputFieldType.PICKLIST,
            options: this.props.vendorOptions,
            value: tag.resource_id,
            isRequired: false,
          }}
          width={3}
          multi={false}
          name="resource_id"
          onChange={(e) => this.handleChangeStatic(e, index)}
          disabled={this.state.orderingHourlyResources}
          placeholder={`Select Resource`}
          loading={this.props.isFetchingVendors}
          error={
            tag.errorName && tag.errorName.errorState === IValidationState.ERROR
              ? tag.errorName
              : {
                  errorState: IValidationState.WARNING,
                  errorMessage: tag.resource_description
                    ? tag.resource_description
                    : "",
                }
          }
        />
        <Input
          field={{
            value: tag.hours,
            label: "",
            type: InputFieldType.NUMBER,
            isRequired: false,
          }}
          width={2}
          name={"hours"}
          minimumValue={"0"}
          placeholder="Enter Hours"
          onChange={(e) => this.handleChangeStatic(e, index)}
          className="phase-name"
          disabled={!tag.override || this.state.orderingHourlyResources}
        />

        <div className="col-md-1 override-column">
          <Checkbox
            isChecked={tag.override}
            name="override"
            onChange={(e) => this.handleChangeStaticOverride(e, index)}
            disabled={this.state.orderingHourlyResources}
          />
        </div>
        <Input
          field={{
            value: tag.hourly_rate,
            label: "",
            type: InputFieldType.NUMBER,
            isRequired: false,
          }}
          width={2}
          name={"hourly_rate"}
          minimumValue={"0"}
          placeholder="Enter Rate"
          onChange={(e) => this.handleChangeStatic(e, index)}
          error={tag.errorRate}
          disabled={this.state.orderingHourlyResources}
        />
        <Input
          field={{
            value: tag.hourly_cost,
            label: "",
            type: InputFieldType.NUMBER,
            isRequired: false,
          }}
          width={2}
          name={"hourly_cost"}
          minimumValue={"0"}
          placeholder="Enter Rate"
          disabled={this.state.orderingHourlyResources}
          onChange={(e) => this.handleChangeStatic(e, index)}
          error={tag.errorCost}
        />
        <div className="col-md-1 mapping-row-text">
          {tag.margin_percentage + "%"}
        </div>
        {!this.state.orderingHourlyResources && (
          <SmallConfirmationBox
            text="Hourly Resource"
            showButton={true}
            onClickOk={() => this.removeHourlyResource(index)}
          />
        )}
      </div>
    );
  };

  renderStaticFields = () => {
    if (!this.state.template.json_config.service_cost) {
      return null;
    }

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    const serviceCost: ISoWCalculationFields = this.state.sowCalculations;
    const recommendedHours: number = this.getProjectManagementRecommendedHours();

    return (
      <div className="template-fields col-md-12 row">
        <div className="section-heading" style={{ cursor: "normal" }}>
          SERVICE COST
          <div className="action-collapse">{""}</div>
        </div>
        <div className="service-cost-body">
          {this.state.template.json_config.service_cost
            .engineering_hours_breakup && (
            <>
              <div className="project-scope-detail">
                <div className="sub-heading col-md-12">
                  <div className="text">PROJECT SCOPE DETAIL</div>
                </div>
                <div className="total-nos col-md-12">
                  <div className="project-scope-row">
                    <Input
                      field={{
                        value: this.state.template.json_config.service_cost
                          .engineering_hours_breakup.total_sites,
                        label: (
                          <div className="override">
                            Total no. of sites
                            <span className="hours-override-required ">*</span>
                          </div>
                        ),
                        type: InputFieldType.NUMBER,
                        isRequired: false,
                      }}
                      width={4}
                      name={"total_sites"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      onChange={(e) => this.handleChangeStaticCalculations(e)}
                      className="select-type"
                      disabled={
                        this.state.template.json_config.service_cost
                          .engineering_hours_breakup.total_sites_override
                      }
                    />
                    <Checkbox
                      isChecked={
                        this.state.template.json_config.service_cost
                          .engineering_hours_breakup.total_sites_override
                      }
                      name="total_sites_override"
                      onChange={(e) => this.handleChangeSiteCutOversOverride(e)}
                    >
                      Remove from SOW
                    </Checkbox>
                  </div>
                  <div className="project-scope-row">
                    <Input
                      field={{
                        value: this.state.template.json_config.service_cost
                          .engineering_hours_breakup.total_cutovers,
                        label: (
                          <div className="override">
                            Total no. of cutovers
                            <span className="hours-override-required ">*</span>
                          </div>
                        ),
                        type: InputFieldType.NUMBER,
                        isRequired: false,
                      }}
                      width={4}
                      name={"total_cutovers"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      onChange={(e) => this.handleChangeStaticCalculations(e)}
                      className="select-type"
                      disabled={
                        this.state.template.json_config.service_cost
                          .engineering_hours_breakup.total_cutovers_override
                      }
                    />
                    <Checkbox
                      isChecked={
                        this.state.template.json_config.service_cost
                          .engineering_hours_breakup.total_cutovers_override
                      }
                      name="total_cutovers_override"
                      onChange={(e) => this.handleChangeSiteCutOversOverride(e)}
                    >
                      Remove from SOW
                    </Checkbox>
                  </div>
                </div>
              </div>
              <div className="phases">
                <div className="sub-heading col-md-12">
                  <div className="text">
                    <span>PHASES</span>
                  </div>
                  <Checkbox
                    isChecked={this.state.orderingPhase}
                    name="orderingPhase"
                    onChange={(e) => this.handleChangeOrderingPhases(e)}
                    className="order-checkbox"
                  >
                    Reorder Phases
                  </Checkbox>
                </div>
                <div className="phase-header">
                  <div className="col-md-4">Phase</div>
                  <div className="col-md-1 override-image" />
                  <div className="col-md-2">Hours</div>
                  <div className="col-md-3">Resource</div>
                </div>
                {this.state.template.json_config.service_cost
                  .engineering_hours_breakup.phases && (
                  <>
                    <div className="col-md-12 mapping-row default-mapping-row">
                      <div className="col-md-4 mapping-row-text">
                        Project Management
                        <span className="info-recomended">
                          {`Recommended Hours: ${recommendedHours}`}
                        </span>
                      </div>
                      <div className="col-md-1 override-image" />
                      <div className="col-md-2 mapping-row-text">
                        {Math.round(recommendedHours)}
                      </div>
                      <div className="col-md-3 mapping-row-text">
                        Project Management
                      </div>
                    </div>
                    {this.state.orderingPhase ? (
                      <DraggableArea
                        isList
                        tags={
                          this.state.template.json_config.service_cost
                            .engineering_hours_breakup.phases
                        }
                        render={this.renderPhase}
                        onChange={(tags) => this.handlePhaseReorder(tags)}
                      />
                    ) : (
                      this.state.template.json_config.service_cost.engineering_hours_breakup.phases.map(
                        (phase, index) =>
                          this.renderPhase({ tag: phase, index })
                      )
                    )}
                  </>
                )}
                <div
                  className="add-new-row"
                  onClick={this.addNewCalculationsPhases}
                >
                  <span className="add-new-row-plus">+</span>
                  <span className="add-new-row-text">Add Phase</span>
                </div>
              </div>
            </>
          )}
          <div className="estimated-hours-rates-container">
            <div className="sub-heading col-md-12">
              <div className="text">
                <span>ESTIMATED HOURS TOTAL</span>
                <div className="eht-actions-right">
                  <img
                    className="icon-reset-static"
                    src="/assets/icons/reset.svg"
                    title="Reset to default from setting"
                    onClick={this.setDefaultStatic}
                  />
                  <SquareButton
                    content={
                      <>
                        <span className="add-plus">+</span>
                        <span className="add-text">Create Vendor</span>
                      </>
                    }
                    className="add-vendor add-btn"
                    bsStyle={ButtonStyle.OUTLINE}
                    onClick={() => this.toggleVendorModal(true)}
                    title={"Create new vendor"}
                  />
                </div>
              </div>
              <Checkbox
                isChecked={this.state.orderingHourlyResources}
                name="orderingHourlyResources"
                onChange={(e) => this.handleChangeOrderingResources(e)}
                className="order-checkbox"
              >
                Reorder Hourly Resources
              </Checkbox>
            </div>
            <div className="estimated-hours-rates">
              <div className="estimated-hours-header">
                <div className="col-md-3">Resource</div>
                <div className="col-md-2">Total Hours</div>
                <div
                  style={{ padding: 0, textAlign: "center" }}
                  className="col-md-1"
                >
                  Override
                </div>
                <div className="col-md-2">Hourly Rate (in $)</div>
                <div className="col-md-2">Hourly Cost (in $)</div>
                <div className="col-md-1">GP%</div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>Engineer</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.engineering_hours_description
                      ? this.props.docSetting.engineering_hours_description
                      : ""}
                  </div>
                </div>{" "}
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .engineering_hours,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  placeholder="Enter Hours"
                  name={"engineering_hours"}
                  className="phase-name"
                  onChange={(e) => this.handleChangeStatic(e)}
                  disabled={
                    !this.state.template.json_config.service_cost
                      .engineering_hours_override
                  }
                />
                <div className="col-md-1 override-column">
                  <Checkbox
                    isChecked={
                      this.state.template.json_config.service_cost
                        .engineering_hours_override
                    }
                    name="engineering_hours_override"
                    onChange={(e) => this.handleChangeStaticOverride(e)}
                  />
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .engineering_hourly_rate,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  name={"engineering_hourly_rate"}
                  placeholder="Enter Rate"
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.engineering_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.engineeringHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.engineeringHoursMarginPercent)
                    ? serviceCost.engineeringHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>After Hours Engineer</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.after_hours_description
                      ? this.props.docSetting.after_hours_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .after_hours,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  name={"after_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  disabled={
                    !this.state.template.json_config.service_cost
                      .after_hours_override
                  }
                />
                <div className="col-md-1 override-column">
                  <Checkbox
                    isChecked={
                      this.state.template.json_config.service_cost
                        .after_hours_override
                    }
                    name="after_hours_override"
                    onChange={(e) => this.handleChangeStaticOverride(e)}
                  />
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .after_hours_rate,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  name={"after_hours_rate"}
                  placeholder="Enter Rate"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.after_hours_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.afterHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.afterHoursMarginPercent)
                    ? serviceCost.afterHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>Integration Technician</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.integration_technician_description
                      ? this.props.docSetting.integration_technician_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .integration_technician_hours,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  name={"integration_technician_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  disabled={
                    !this.state.template.json_config.service_cost
                      .integration_technician_hours_override
                  }
                />
                <div className="col-md-1 override-column">
                  <Checkbox
                    isChecked={
                      this.state.template.json_config.service_cost
                        .integration_technician_hours_override
                    }
                    name="integration_technician_hours_override"
                    onChange={(e) => this.handleChangeStaticOverride(e)}
                  />
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .integration_technician_hourly_rate,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  name={"integration_technician_hourly_rate"}
                  placeholder="Enter Rate"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.integration_technician_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(
                    serviceCost.integrationTechnicianCustomerCost
                  )}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.integrationTechnicianMarginPercent)
                    ? serviceCost.integrationTechnicianMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>Project Management</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.project_management_hours_description
                      ? this.props.docSetting
                          .project_management_hours_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .project_management_hours,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  name={"project_management_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  disabled={
                    !this.state.template.json_config.service_cost
                      .project_management_hours_override
                  }
                />
                <div className="col-md-1 override-column">
                  <Checkbox
                    isChecked={
                      this.state.template.json_config.service_cost
                        .project_management_hours_override
                    }
                    name="project_management_hours_override"
                    onChange={(e) => this.handleChangeStaticOverride(e)}
                  />
                </div>
                <Input
                  field={{
                    value: this.state.template.json_config.service_cost
                      .project_management_hourly_rate,
                    label: "",
                    type: InputFieldType.NUMBER,
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  name={"project_management_hourly_rate"}
                  placeholder="Enter Rate"
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.project_management_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.pmHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.pmHoursMarginPercent)
                    ? serviceCost.pmHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              {this.state.template.json_config.service_cost.hourly_resources &&
                (this.state.orderingHourlyResources ? (
                  <DraggableArea
                    isList
                    tags={
                      this.state.template.json_config.service_cost
                        .hourly_resources
                    }
                    render={this.renderHourlyResource}
                    onChange={(tags) => this.handleHourlyResourceReorder(tags)}
                  />
                ) : (
                  this.state.template.json_config.service_cost.hourly_resources.map(
                    (hourlyResource: IHourlyResource, index) =>
                      this.renderHourlyResource({ tag: hourlyResource, index })
                  )
                ))}
              <div className="add-new-row" onClick={this.addNewHourlyResource}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Hourly Resource</span>
              </div>
            </div>
          </div>
          {this.state.template.json_config.service_cost.contractors && (
            <div className="contractors">
              <div className="sub-heading col-md-12">
                <div className="text">FEE BASED CONTRACTORS</div>
              </div>
              {this.state.template.json_config.service_cost.contractors.map(
                (contractor, index) => {
                  return (
                    <div className="contractor-row col-md-12" key={index}>
                      <Input
                        field={{
                          value: contractor.vendor_id,
                          label: "Contractor Name",
                          type: InputFieldType.PICKLIST,
                          options: this.props.vendorOptions,
                          isRequired: false,
                        }}
                        width={3}
                        name={"vendor_id"}
                        placeholder="Select Contractor"
                        loading={this.props.isFetchingVendors}
                        onChange={(e) => this.handleChangeContractors(e, index)}
                        className="select-type"
                        error={contractor.errorName}
                      />
                      <Input
                        field={{
                          value: contractor.partner_cost,
                          label: "Partner Cost(In $)",
                          type: InputFieldType.NUMBER,
                          isRequired: false,
                        }}
                        width={2}
                        name={"partner_cost"}
                        minimumValue={"0"}
                        placeholder="Enter Rate"
                        onChange={(e) => this.handleChangeContractors(e, index)}
                        className="select-type"
                        error={contractor.errorRate}
                      />
                      <Input
                        field={{
                          value: contractor.margin_percentage,
                          label: "GP %",
                          type: InputFieldType.NUMBER,
                          isRequired: false,
                        }}
                        width={1}
                        minimumValue={"0"}
                        maximumValue={"100"}
                        name={"margin_percentage"}
                        placeholder="Enter %"
                        onChange={(e) => this.handleChangeContractors(e, index)}
                        className="select-type"
                      />
                      <Input
                        field={{
                          label: "Customer Cost(In $)",
                          value: formatter.format(
                            Number(getCustomerCost(contractor))
                          ),
                          type: InputFieldType.TEXT,
                          isRequired: false,
                        }}
                        width={2}
                        name={"margin_percentage"}
                        placeholder=" "
                        disabled={true}
                        className="disabled-calculations"
                        onChange={(e) => null}
                      />
                      <Input
                        field={{
                          label: "GP $",
                          value: formatter.format(
                            Number(getCustomerCost(contractor)) -
                              contractor.partner_cost
                          ),
                          type: InputFieldType.TEXT,
                          isRequired: false,
                        }}
                        width={2}
                        name="gross_profit"
                        disabled={true}
                        className="disabled-calculations"
                        onChange={(e) => null}
                      />
                      <Checkbox
                        isChecked={contractor.type === "Product"}
                        name="type"
                        onChange={(e) =>
                          this.handleChangeContractorsProduct(e, index)
                        }
                        className="product-type"
                      >
                        Is Product ?
                      </Checkbox>
                      {this.state.template.json_config.service_cost.contractors
                        .length > 1 && (
                        <SmallConfirmationBox
                          text="contractor"
                          onClickOk={() => this.removeContractor(index)}
                          showButton={true}
                        />
                      )}
                    </div>
                  );
                }
              )}
              <div className="add-new-row" onClick={this.addNewContractor}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Contractor</span>
              </div>
            </div>
          )}
          {this.state.template.json_config.service_cost.travels && (
            <div className="travelscontractors">
              <div className="sub-heading col-md-12">
                <div className="text">TRAVEL</div>
              </div>
              {this.state.template.json_config.service_cost.travels.map(
                (travel, index) => {
                  return (
                    <div className="contractor-row col-md-12" key={index}>
                      <Input
                        field={{
                          value: travel.description,
                          label: "Description",
                          type: InputFieldType.TEXT,
                          isRequired: false,
                        }}
                        width={4}
                        name={"description"}
                        placeholder="Enter description"
                        onChange={(e) => this.handleChangeTravels(e, index)}
                        error={travel.errorDescription}
                      />
                      <Input
                        field={{
                          value: travel.cost,
                          label: "Cost (in $)",
                          type: InputFieldType.NUMBER,
                          isRequired: false,
                        }}
                        width={3}
                        name={"cost"}
                        minimumValue={"0"}
                        placeholder="Enter Rate"
                        onChange={(e) => this.handleChangeTravels(e, index)}
                        error={travel.errorCost}
                      />
                      {this.state.template.json_config.service_cost.travels
                        .length > 1 && (
                        <SmallConfirmationBox
                          text="travel"
                          showButton={true}
                          onClickOk={() => this.removeTravel(index)}
                        />
                      )}
                    </div>
                  );
                }
              )}
              <div className="add-new-row" onClick={this.addNewTravel}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Travel Estimate</span>
              </div>
            </div>
          )}

          <div className="calculations">
            <div className="sub-heading col-md-12">
              <div className="text">SERVICE COST</div>
            </div>
            <div className="calculations-table">
              <div className="calculations-table-header">
                <div className="calculations-header-title">Description</div>
                <div className="calculations-header-title">Revenue</div>
                <div className="calculations-header-title">Internal Cost</div>
                <div className="calculations-header-title">GP $</div>
                <div className="calculations-header-title">GP %</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Engineering Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.engineeringHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Engineering After Hours Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.afterHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Integration Technician Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serviceCost.integrationTechnicianCustomerCost
                    )}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serviceCost.integrationTechnicianInternalCost
                    )}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.integrationTechnicianMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.integrationTechnicianMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Project Management Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.pmHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Hourly Contract Labor
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborMarginPercent + "%"}
                  </div>
                </div>
              </div>
            </div>
            <div className="calculations-table">
              <h4>Project Summary</h4>
              <div className="calculations-table-header">
                <div className="calculations-header-title">Description</div>
                <div className="calculations-header-title">Revenue</div>
                <div className="calculations-header-title">Internal Cost</div>
                <div className="calculations-header-title">GP $</div>
                <div className="calculations-header-title">GP %</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Professional Services
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.proSerMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Risk Budget</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.riskBudgetCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.riskBudgetInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.riskBudgetMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.riskBudgetMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Contractors</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.contractorCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.contractorInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.contractorMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.contractorMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Travel Budget</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.travelCost)}
                  </div>
                  <div className="calculations-table-col">-</div>
                  <div className="calculations-table-col">-</div>
                  <div className="calculations-table-col">-</div>
                </div>
              </div>
            </div>
            <div className="calculations-table project-total-section">
              <h4>Project Total (FIXED FEE)</h4>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Internal Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalInternalCost)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Total Margin</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalMargin)}
                    <span className="total-margin-percent">
                      @ {serviceCost.totalMarginPercent}% GP
                    </span>
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Customer Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalCustomerCost)}
                  </div>
                </div>
              </div>
            </div>
            <div className="calculations-table project-total-section">
              <h4>Project Total (T & M)</h4>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Internal Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerInternalCost)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Total Margin</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerMargin)}
                    <span className="total-margin-percent">
                      @ {serviceCost.proSerMarginPercent}% GP
                    </span>
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Customer Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerCustomerCost)}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="notes col-md-12">
            <div className="sub-heading-notes col-md-12">
              <div className="text">NOTES</div>
            </div>
            <div className="col-md-12">
              <QuillEditorAcela
                onChange={(e) => this.handleChangeNoteMarkDown(e)}
                value={this.state.template.json_config.service_cost.notes}
                wrapperClass={"ql-template-notes"}
                scrollingContainer=".add-template"
              />
            </div>
          </div>
          {this.state.template.id !== 0 && (
            <div className="version-section col-md-10">
              <Input
                field={{
                  value: this.state.template.version_description,
                  label: "Version Description",
                  type: InputFieldType.TEXTAREA,
                  isRequired: false,
                }}
                width={12}
                name={"version_description"}
                placeholder=" "
                onChange={(e) => this.handleChangeSimpleFields(e)}
                className="Enter version description"
              />
            </div>
          )}
        </div>
      </div>
    );
  };

  getProjectManagementRecommendedHours = (): number => {
    let hours = 0;
    if (
      this.state.template.json_config.service_cost &&
      this.state.template.json_config.service_cost.engineering_hours_breakup &&
      this.state.template.json_config.service_cost.engineering_hours_breakup
        .phases
    ) {
      this.state.template.json_config.service_cost.engineering_hours_breakup.phases.forEach(
        (el) => {
          if (el.resource !== -3) {
            hours += el.hours;
          }
        }
      );
    }
    return parseFloat(getRecommendedHours(hours));
  };

  handleChangeOrderingPhases = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ orderingPhase: e.target.checked, unsaved: true });
  };

  handleChangeOrderingResources = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ orderingHourlyResources: e.target.checked, unsaved: true });
  };

  handleClickEditDescription = (resource: IHourlyResource) => {
    const mapping: IVendorAliasMapping = this.state.vendorDescriptionMapping.has(
      resource.resource_id
    )
      ? this.state.vendorDescriptionMapping.get(resource.resource_id)
      : {
          vendor_crm_id: resource.resource_id,
          vendor_name: resource.resource_name,
          resource_description: null,
        };
    this.setState({
      currentVendorMapping: mapping,
      showVendorMappingModal: true,
    });
  };

  handleChangeStatic = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    // Hours should be integer
    const value = this.getValues(
      e,
      [
        "hours",
        "after_hours",
        "engineering_hours",
        "project_management_hours",
        "integration_technician_hours",
      ].includes(e.target.name)
    );

    // For hourly resource, index will be passed
    if (!isNil(idx)) {
      let updatedResource: IHourlyResource;
      if (e.target.name === "resource_id") {
        const resource_id = value as number;
        const resource_name = this.props.vendorOptions.find(
          (el) => el.value === value
        ).label as string;
        let resource_description: string = null;
        if (this.state.vendorDescriptionMapping.has(resource_id))
          resource_description = this.state.vendorDescriptionMapping.get(
            resource_id
          ).resource_description;
        // If the hourly resource is not overridden, then set the calculated value
        // of resource, else just update the hourly resource name
        if (
          !newState.template.json_config.service_cost.hourly_resources[idx]
            .override
        ) {
          const hours = this.state.phasesGroupedByResources[resource_name]
            ? this.state.phasesGroupedByResources[resource_name]
            : 0;
          updatedResource = getCalculatedHourlyResource({
            ...newState.template.json_config.service_cost.hourly_resources[idx],
            hours,
            resource_id,
            resource_name,
            resource_description,
          });
        } else {
          updatedResource = {
            ...newState.template.json_config.service_cost.hourly_resources[idx],
            resource_id,
            resource_name,
            resource_description,
          };
        }
      } else {
        updatedResource = getCalculatedHourlyResource({
          ...newState.template.json_config.service_cost.hourly_resources[idx],
          [e.target.name]: value,
        });
      }
      newState.template.json_config.service_cost.hourly_resources[
        idx
      ] = updatedResource;
    } else newState.template.json_config.service_cost[e.target.name] = value;

    (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
      newState.template.json_config.service_cost.hourly_resources
    );
    newState.template.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.template.json_config.service_cost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    this.debouncedCalculations();
  };

  handleChangeStaticOverride = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    if (!isNil(idx)) {
      newState.template.json_config.service_cost.hourly_resources[
        idx
      ].override = e.target.checked;
      if (!e.target.checked) {
        let currentResource =
          newState.template.json_config.service_cost.hourly_resources[idx];
        let calculatedHours = this.state.phasesGroupedByResources[
          currentResource.resource_name
        ]
          ? this.state.phasesGroupedByResources[currentResource.resource_name]
          : 0;
        newState.template.json_config.service_cost.hourly_resources[
          idx
        ] = getCalculatedHourlyResource({
          ...currentResource,
          hours: calculatedHours,
          override: e.target.checked,
        });
      }
    } else {
      newState.template.json_config.service_cost[e.target.name] =
        e.target.checked;

      if (
        e.target.name === "project_management_hours_override" &&
        !e.target.checked
      ) {
        newState.template.json_config.service_cost.project_management_hours = Math.round(
          (this.state.phasesGroupedByResources["Project Management"]
            ? this.state.phasesGroupedByResources["Project Management"]
            : 0) + this.getProjectManagementRecommendedHours()
        );
      }
      if (e.target.name === "engineering_hours_override" && !e.target.checked) {
        if (
          newState.template.json_config.service_cost
            .engineering_hours_breakup &&
          newState.template.json_config.service_cost.engineering_hours_breakup
            .phases
        ) {
          newState.template.json_config.service_cost.engineering_hours = this
            .state.phasesGroupedByResources["Engineer"]
            ? this.state.phasesGroupedByResources["Engineer"]
            : 0;
        }
      }
      if (e.target.name === "after_hours_override" && !e.target.checked) {
        if (
          newState.template.json_config.service_cost
            .engineering_hours_breakup &&
          newState.template.json_config.service_cost.engineering_hours_breakup
            .phases
        ) {
          newState.template.json_config.service_cost.after_hours = this.state
            .phasesGroupedByResources["After Hours Engineer"]
            ? this.state.phasesGroupedByResources["After Hours Engineer"]
            : 0;
        }
      }
      if (
        e.target.name === "integration_technician_hours_override" &&
        !e.target.checked
      ) {
        if (
          newState.template.json_config.service_cost
            .engineering_hours_breakup &&
          newState.template.json_config.service_cost.engineering_hours_breakup
            .phases
        ) {
          newState.template.json_config.service_cost.integration_technician_hours = this
            .state.phasesGroupedByResources["Integration Technician"]
            ? this.state.phasesGroupedByResources["Integration Technician"]
            : 0;
        }
      }
    }
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    this.debouncedCalculations();
  };

  handleChangeSiteCutOversOverride = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.engineering_hours_breakup[
      e.target.name
    ] = e.target.checked;
    if (e.target.name === "total_sites_override" && e.target.checked) {
      newState.template.json_config.service_cost.engineering_hours_breakup.total_sites = 1;
    }
    if (e.target.name === "total_cutovers_override" && e.target.checked) {
      newState.template.json_config.service_cost.engineering_hours_breakup.total_cutovers = 1;
    }
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    this.debouncedCalculations();
  };

  handleChangeStaticCalculations = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.engineering_hours_breakup[
      e.target.name
    ] = this.getValues(e);
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    if (e.target.type === "number") this.debouncedCalculations();
  };

  setPhasesGroupedByResources = () => {
    if (
      this.state.template.json_config.service_cost.engineering_hours_breakup &&
      this.state.template.json_config.service_cost.engineering_hours_breakup
        .phases
    ) {
      const phasesGroupedByResources: object = this.state.template.json_config.service_cost.engineering_hours_breakup.phases.reduce(
        (sumObj, el) => {
          let resource_name = el.resource_name ? el.resource_name : "Engineer";
          if (resource_name in sumObj) {
            sumObj[resource_name] += el.hours;
          } else sumObj[resource_name] = el.hours;
          return sumObj;
        },
        {}
      );
      this.setState({ phasesGroupedByResources });
    }
  };

  changeEstimatedNonOverrideHours = debounce(() => {
    // Group resources from phases section and then calculate total hours for each resource
    // Associate the calculated hours to each resource in estimated hours total (if override is not enabled)
    // this is to be done
    const newState = cloneDeep(this.state);
    const phasesGroupedByResources: object = newState.template.json_config.service_cost.engineering_hours_breakup.phases.reduce(
      (sumObj, el) => {
        let resource_name = el.resource_name ? el.resource_name : "Engineer";
        if (resource_name in sumObj) {
          sumObj[resource_name] += el.hours;
        } else sumObj[resource_name] = el.hours;
        return sumObj;
      },
      {}
    );
    (newState.phasesGroupedByResources as object) = phasesGroupedByResources;
    let totalEngineeringHours = phasesGroupedByResources["Engineer"]
      ? phasesGroupedByResources["Engineer"]
      : 0;
    let totalEngineeringAfterHours = phasesGroupedByResources[
      "After Hours Engineer"
    ]
      ? phasesGroupedByResources["After Hours Engineer"]
      : 0;
    let totalIntegrationTechnicianHours = phasesGroupedByResources[
      "Integration Technician"
    ]
      ? phasesGroupedByResources["Integration Technician"]
      : 0;
    let totalProjectManagementHours = Math.round(
      (phasesGroupedByResources["Project Management"]
        ? phasesGroupedByResources["Project Management"]
        : 0) + this.getProjectManagementRecommendedHours()
    );

    if (!newState.template.json_config.service_cost.engineering_hours_override)
      newState.template.json_config.service_cost.engineering_hours = totalEngineeringHours;
    if (!newState.template.json_config.service_cost.after_hours_override)
      newState.template.json_config.service_cost.after_hours = totalEngineeringAfterHours;
    if (
      !newState.template.json_config.service_cost
        .integration_technician_hours_override
    )
      newState.template.json_config.service_cost.integration_technician_hours = totalIntegrationTechnicianHours;
    if (
      !newState.template.json_config.service_cost
        .project_management_hours_override
    )
      newState.template.json_config.service_cost.project_management_hours = totalProjectManagementHours;
    if (newState.template.json_config.service_cost.hourly_resources)
      newState.template.json_config.service_cost.hourly_resources.forEach(
        (el) => {
          let phaseHours = phasesGroupedByResources[el.resource_name]
            ? phasesGroupedByResources[el.resource_name]
            : 0;
          if (!el.override) {
            el.hours = phaseHours;
            const newPhase = getCalculatedHourlyResource(el);
            el.customer_cost = newPhase.customer_cost;
            el.internal_cost = newPhase.internal_cost;
            el.margin = newPhase.margin;
            el.margin_percentage = newPhase.margin_percentage;
          }
        }
      );

    this.setState(newState, () => this.doTemplateCalculations());
  }, 500);

  handleChangeStaticCalculationsPhases = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.engineering_hours_breakup.phases[
      index
    ][e.target.name] = this.getValues(e, true);
    if (e.target.name === "resource") {
      newState.template.json_config.service_cost.engineering_hours_breakup.phases[
        index
      ].resource_name = this.state.phaseResources.find(
        (el) => el.value === e.target.value
      ).label as string;
    }
    this.setState(newState, this.changeEstimatedNonOverrideHours);
  };

  addNewCalculationsPhases = () => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.engineering_hours_breakup &&
      newState.template.json_config.service_cost.engineering_hours_breakup.phases.push(
        {
          id: random(1, 100000),
          name: "",
          hours: 0,
          override: false,
          after_hours: false,
          resource_name: "Engineer",
          resource: -1,
        }
      );
    this.setState(newState);
  };

  deleteCalculationsPhases = (index: number) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.engineering_hours_breakup.phases.splice(
      index,
      1
    );

    this.setState(newState, this.changeEstimatedNonOverrideHours);
  };

  handleChangeNoteMarkDown = (html: string) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.notes = html;
    this.setState(newState);
  };

  setDefaultStatic = () => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.engineering_hourly_rate = this.props.docSetting.engineering_hourly_rate;
    newState.template.json_config.service_cost.after_hours_rate = this.props.docSetting.after_hours_rate;
    newState.template.json_config.service_cost.project_management_hourly_rate = this.props.docSetting.project_management_hourly_rate;
    newState.template.json_config.service_cost.integration_technician_hourly_rate = this.props.docSetting.integration_technician_hourly_rate;
    newState.template.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.template.json_config.service_cost
    );
    this.setState(newState, () => this.doTemplateCalculations());
  };

  getValues = (
    e: React.ChangeEvent<HTMLInputElement>,
    integer: boolean = false
  ) => {
    const value =
      e.target.type === "number"
        ? round(Number(e.target.value), integer ? 0 : 2)
        : e.target.value;

    return value;
  };

  handleChangeContractors = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.contractors[index][
      e.target.name
    ] = this.getValues(e);
    if (e.target.name === "vendor_id") {
      if (e.target.value)
        newState.template.json_config.service_cost.contractors[
          index
        ].name = this.props.vendorOptions.find(
          (el) => el.value === e.target.value
        ).label as string;
      else
        newState.template.json_config.service_cost.contractors[index].name = "";
    }
    if (e.target.type === "number")
      newState.template.json_config.service_cost.contractors[
        index
      ].customer_cost = getCustomerCost(
        newState.template.json_config.service_cost.contractors[index]
      );
    (newState.unsaved as boolean) = true;
    this.setState(newState, () => {
      if (e.target.type === "number") this.debouncedCalculations();
    });
  };

  handleChangeContractorsProduct = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.contractors[index].type = e
      .target.checked
      ? "Product"
      : "Service";
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addNewContractor = () => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.contractors.push({
      name: "",
      partner_cost: 0,
      customer_cost: 0,
      margin_percentage: 0,
      type: "Service",
      vendor_id: null,
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeContractor = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.contractors.splice(idx, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addNewTravel = () => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.travels.push({
      cost: 0,
      description: "",
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeTravel = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.travels.splice(idx, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addNewHourlyResource = () => {
    const newState = cloneDeep(this.state);
    if (!newState.template.json_config.service_cost.hourly_resources)
      newState.template.json_config.service_cost.hourly_resources = [];
    newState.template.json_config.service_cost.hourly_resources.push({
      ...AddTemplate.EmptyHourlyResource,
      id: random(1, 100000),
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeHourlyResource = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.hourly_resources.splice(idx, 1);
    newState.template.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.template.json_config.service_cost
    );
    (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
      newState.template.json_config.service_cost.hourly_resources
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doTemplateCalculations);
  };

  handleChangeTravels = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.service_cost.travels[index][
      e.target.name
    ] = this.getValues(e);
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    if (e.target.type === "number") this.debouncedCalculations();
  };

  toggleVendorModal = (show: boolean) => {
    this.setState((prevState) => ({
      showVendorModal: show,
    }));
  };

  onSubmitVendorModal = () => {
    this.props.getVendorsList();
    this.toggleVendorModal(false);
  };

  closeVendorMappingModal = (refresh: boolean) => {
    this.setState({
      showVendorMappingModal: false,
      currentVendorMapping: {
        vendor_name: "",
        vendor_crm_id: null,
        resource_description: null,
      },
    });
    if (refresh) {
      this.props.getVendorMapping();
    }
  };

  checkValidaBoards = () => {
    const error = this.getEmptyState().error;

    const template = this.state.template.json_config;
    let isValid = true;
    const isCollapsed = {};
    const errorsLabel = {};
    const errorsValue = {};

    if (
      !this.state.template.name ||
      this.state.template.name.trim().length === 0
    ) {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage = "Enter a valid template name";

      isValid = false;
    } else if (this.state.template.name.length > 300) {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage = "name should be less than 300 chars.";

      isValid = false;
    }
    if (!this.state.template.category) {
      error.category.errorState = IValidationState.ERROR;
      error.category.errorMessage = "Please select category";

      isValid = false;
    }
    if (!this.state.template.service_catalog_category) {
      error.service_catalog_category.errorState = IValidationState.ERROR;
      error.service_catalog_category.errorMessage =
        "Please select service category";

      isValid = false;
    }

    if (template) {
      template.service_cost.contractors.forEach((contractor) => {
        contractor.errorName = {
          ...AddTemplate.emptyErrorState,
        };
        contractor.errorRate = {
          ...AddTemplate.emptyErrorState,
        };
      });

      template.service_cost.contractors.forEach((contractor) => {
        if (
          template.service_cost.contractors.length > 1 ||
          contractor.vendor_id ||
          contractor.partner_cost > 0
        ) {
          if (!contractor.vendor_id) {
            contractor.errorName = {
              errorState: IValidationState.ERROR,
              errorMessage: "Please select a contractor",
            };
            isValid = false;
          }
          if (contractor.partner_cost === 0) {
            contractor.errorRate = {
              errorState: IValidationState.ERROR,
              errorMessage: "Positive No. Required",
            };
            isValid = false;
          }
        }
      });

      template.service_cost.travels &&
        template.service_cost.travels.map((travel) => {
          travel.errorDescription = {
            ...AddTemplate.emptyErrorState,
          };
          travel.errorCost = {
            ...AddTemplate.emptyErrorState,
          };
        });

      template.service_cost.travels &&
        template.service_cost.travels.map((travel) => {
          if (
            template.service_cost.travels.length > 1 ||
            travel.description.trim() ||
            travel.cost > 0
          ) {
            if (travel.description.trim() === "") {
              travel.errorDescription = {
                errorState: IValidationState.ERROR,
                errorMessage: "Please enter description",
              };
              isValid = false;
            }
            if (travel.cost === 0) {
              travel.errorCost = {
                errorState: IValidationState.ERROR,
                errorMessage: "Please enter cost",
              };
              isValid = false;
            }
          }
        });
      if (
        template.service_cost.engineering_hours_breakup &&
        template.service_cost.engineering_hours_breakup.phases
      ) {
        const resourcesIds: Set<number> = new Set(
          this.state.phaseResources.map((el) => el.value)
        );
        template.service_cost.engineering_hours_breakup.phases.forEach(
          (resource) => {
            resource.errorName = {
              ...AddTemplate.emptyErrorState,
            };
            resource.errorResource = {
              ...AddTemplate.emptyErrorState,
            };
          }
        );

        template.service_cost.engineering_hours_breakup.phases.forEach(
          (resource) => {
            if (!resource.name.trim()) {
              resource.errorName = {
                errorState: IValidationState.ERROR,
                errorMessage: "Please enter phase name",
              };
              isValid = false;
            }
            if (resource.resource && !resourcesIds.has(resource.resource)) {
              resource.errorResource = {
                errorState: IValidationState.ERROR,
                errorMessage: "Please select a resource",
              };
              isValid = false;
            }
          }
        );
      }

      if (template.service_cost.hourly_resources) {
        template.service_cost.hourly_resources.forEach((resource) => {
          resource.errorName = {
            ...AddTemplate.emptyErrorState,
          };
          resource.errorRate = {
            ...AddTemplate.emptyErrorState,
          };
          resource.errorCost = {
            ...AddTemplate.emptyErrorState,
          };
        });

        let uniqueHourlyResources: Set<number> = new Set();
        template.service_cost.hourly_resources.forEach((resource) => {
          if (!resource.resource_id) {
            resource.errorName = {
              errorState: IValidationState.ERROR,
              errorMessage: "Please select resource",
            };
            isValid = false;
          }
          if (uniqueHourlyResources.has(resource.resource_id)) {
            resource.errorName = {
              errorState: IValidationState.ERROR,
              errorMessage: "Duplicate resource",
            };
            isValid = false;
          }
          if (resource.hourly_rate === 0) {
            resource.errorRate = {
              errorState: IValidationState.ERROR,
              errorMessage: "Required",
            };
            isValid = false;
          }
          if (resource.hourly_cost === 0) {
            resource.errorCost = {
              errorState: IValidationState.ERROR,
              errorMessage: "Required",
            };
            isValid = false;
          }
          if (resource.resource_id)
            uniqueHourlyResources.add(resource.resource_id);
        });
      }
      let count = 1;

      Object.keys(template)
        .sort((a, b) => template[a].ordering - template[b].ordering)
        .filter(
          (field) => template[field].visible_in && !template[field].hide_in_sow
        )
        .map((key, index) => {
          const boardContainerData = template[key];
          {
            Object.keys(boardContainerData)
              .sort(
                (a, b) =>
                  ((template[key][a] && template[key][a].ordering) || 0) -
                  ((template[key][b] && template[key][b].ordering) || 0)
              )
              .map((k, i) => {
                if (
                  k !== "section_label" &&
                  k !== "ordering" &&
                  k !== "visible_in" &&
                  k !== "default_hidden" &&
                  k !== "hide_in_sow"
                ) {
                  const hasOptions =
                    Object.keys(boardContainerData[k]).findIndex(
                      (v) => v === "options"
                    ) !== -1;
                  if (
                    boardContainerData[k].type !== "TEXTBOX" &&
                    boardContainerData[k].type !== "MARKDOWN"
                  ) {
                    if (hasOptions) {
                      if (boardContainerData[k].is_required) {
                        if (template[key] && template[key][k]) {
                          if (template[key][k].value.length === 0) {
                            isValid = false;
                            isCollapsed[k] = true;
                            errorsValue[count] = {
                              errorState: IValidationState.ERROR,
                              errorMessage: `Required`,
                            };
                          }
                        } else {
                          isValid = false;
                        }
                      }
                    } else {
                      Object.keys(boardContainerData[k]).map((l, j) => {
                        if (boardContainerData[k][l].is_required) {
                          if (
                            template[key] &&
                            template[key][k] &&
                            template[key][k][l]
                          ) {
                            if (template[key][k][l].length === 0) {
                              isValid = false;
                            }
                          } else {
                            isValid = false;
                          }
                        }
                      });
                    }
                  } else {
                    if (boardContainerData[k].is_required === "true") {
                      if (template[key] && template[key][k]) {
                        if (
                          commonFunctions.isEditorEmpty(template[key][k].value)
                        ) {
                          isValid = false;
                          isCollapsed[key] = true;
                          errorsValue[count] = {
                            errorState: IValidationState.ERROR,
                            errorMessage: `Required`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  }
                  if (boardContainerData[k]) {
                    if (template[key] && template[key][k]) {
                      if (template[key][k].label === "") {
                        isValid = false;
                        isCollapsed[key] = true;
                        errorsLabel[count] = {
                          errorState: IValidationState.ERROR,
                          errorMessage: `Label is required`,
                        };
                      }
                    } else {
                      isValid = false;
                    }
                  }

                  if (boardContainerData[k].input_type === "FLOAT") {
                    if (boardContainerData[k].is_required) {
                      if (template[key] && template[key][k]) {
                        if (
                          template[key][k].value.length > 0 &&
                          !AppValidators.isValidPositiveFloat(
                            template[key][k].value
                          )
                        ) {
                          isValid = false;
                          isCollapsed[key] = true;
                          errorsValue[count] = {
                            errorState: IValidationState.ERROR,

                            errorMessage: `Please enter numeric values only`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  }
                  if (
                    boardContainerData.sections &&
                    boardContainerData.sections.length > 0
                  ) {
                    boardContainerData.sections.map((keyS, indexS) => {
                      Object.keys(keyS).map((secKey, x) => {
                        if (keyS[secKey].is_required) {
                          delete template[key].sections[indexS][secKey].error;
                        }
                      });
                    });
                  }
                  if (
                    boardContainerData.sections &&
                    boardContainerData.sections.length > 0
                  ) {
                    boardContainerData.sections.map((keyS, indexS) => {
                      Object.keys(keyS).map((secKey, x) => {
                        if (keyS[secKey].is_required) {
                          if (keyS[secKey].value === "") {
                            isValid = false;
                            isCollapsed[key] = true;
                            template[key].sections[indexS][secKey].error = {
                              errorState: IValidationState.ERROR,
                              errorMessage: `Required input`,
                            };
                          }
                        }
                      });
                    });
                  }
                  count++;
                }
              });
          }
        });
    }

    this.setState((prevState) => ({
      error,
      errorsLabel,
      errorsValue,
      isValid,
      isCollapsed,
      showError: !isValid,
      template: {
        ...prevState.template,
        json_config: template,
      },
    }));

    return isValid;
  };

  saveTemplate = (closeDocument: boolean) => {
    if (this.checkValidaBoards()) {
      this.setState({ saving: true });
      const template = cloneDeep(this.state).template;
      Object.keys(template.json_config).map((key, index) => {
        const boardContainerData = template.json_config[key];
        {
          Object.keys(boardContainerData).map((k, i) => {
            if (k !== "section_label" && k !== "ordering") {
              if (boardContainerData[k].type === "MARKDOWN") {
                template.json_config[key][
                  k
                ].value_markdown = commonFunctions.convertToMarkdown(
                  template.json_config[key][k].value
                );
              }
            }
          });
        }
      });
      template.json_config.service_cost.contractors = map(
        template.json_config.service_cost.contractors,
        (object) => {
          return pick(object, [
            "customer_cost",
            "margin_percentage",
            "name",
            "partner_cost",
            "type",
            "vendor_id",
          ]);
        }
      );
      template.json_config.service_cost.travels = map(
        template.json_config.service_cost.travels,
        (object) => {
          return pick(object, ["cost", "description"]);
        }
      );
      template.json_config.service_cost.hourly_resources = map(
        template.json_config.service_cost.hourly_resources,
        (object) => {
          return pick(object, [
            "hours",
            "override",
            "is_hidden",
            "hourly_cost",
            "hourly_rate",
            "resource_id",
            "resource_name",
            "internal_cost",
            "customer_cost",
            "margin",
            "margin_percentage",
            "resource_description",
          ]);
        }
      );
      if (
        template.json_config.service_cost.engineering_hours_breakup &&
        template.json_config.service_cost.engineering_hours_breakup.phases
      ) {
        template.json_config.service_cost.engineering_hours_breakup.phases = template.json_config.service_cost.engineering_hours_breakup.phases.map(
          (el) => {
            if (!el.resource) {
              el.resource = -1;
              el.resource_name = "Engineer";
            }
            delete el.id;
            delete el.errorName;
            delete el.errorResource;
            return el;
          }
        );
      }
      if (
        !template.linked_service_catalog ||
        template.linked_service_catalog === "null"
      ) {
        template.linked_service_catalog = null;
      }
      template.json_config.service_cost.notesMD = commonFunctions.convertToMarkdown(
        template.json_config.service_cost.notes
      );
      if (this.state.template.id && this.state.template.id !== 0) {
        this.props
          .updateTemplate(template)
          .then((a) => {
            if (a.type === EDIT_TEMPLATE_SUCCESS) {
              if (closeDocument) {
                this.setState(
                  {
                    unsaved: false,
                  },
                  () => {
                    this.props.history.push("/sow/templates");
                  }
                );
              } else {
                const json_config = cloneDeep(this.state.template.json_config);
                this.setPhaseIds(json_config);
                this.setHourlyResourceIds(json_config);
                this.setState({
                  template: {
                    ...this.state.template,
                    json_config,
                    update_version: false,
                    updated_on: new Date().toISOString(),
                    major_version: a.response.major_version,
                    minor_version: a.response.minor_version,
                    version: `${a.response.major_version}.${a.response.minor_version}`,
                  },
                  unsaved: false,
                });
              }
            }
            if (a.type === EDIT_TEMPLATE_FAILURE) {
              this.setValidationErrors(a.errorList.data);
            }
          })
          .finally(() => this.setState({ saving: false }));
      } else {
        this.props
          .saveTemplate(template)
          .then((a) => {
            if (a.type === CREATE_TEMPLATE_SUCCESS) {
              this.setState(
                {
                  unsaved: false,
                },
                () => {
                  this.props.history.push("/sow/templates");
                }
              );
            }
            if (a.type === CREATE_TEMPLATE_FAILURE) {
              this.setValidationErrors(a.errorList.data);
            }
          })
          .finally(() => this.setState({ saving: false }));
      }
    } else if (this.state.orderingPhase || this.state.orderingHourlyResources) {
      this.setState({ orderingPhase: false, orderingHourlyResources: false });
    }
  };
  onChecboxChangedHidden = (event: any) => {
    const showHidden = event.target.checked;
    this.setState({
      showHidden,
    });
  };
  previewDoc = (event: any, type: SowDocType) => {
    if (this.checkValidaBoards()) {
      // tslint:disable-next-line: variable-name
      const json_config = cloneDeep(this.state.template.json_config);
      const template = cloneDeep(this.state.template);
      if (template && json_config && json_config.service_cost) {
        json_config.service_cost.customer_cost_fixed_fee = this.state.sowCalculations.totalCustomerCost;
        json_config.service_cost.internal_cost_fixed_fee = this.state.sowCalculations.totalInternalCost;
        json_config.service_cost.customer_cost_t_and_m_fee = this.state.sowCalculations.proSerCustomerCost;
        json_config.service_cost.internal_cost_t_and_m_fee = this.state.sowCalculations.proSerInternalCost;
        json_config.service_cost.customer_cost = this.state.sowCalculations.totalCustomerCost;
        json_config.service_cost.internal_cost = this.state.sowCalculations.totalInternalCost;
      }
      this.setState({ loading: true });
      template.json_config = json_config;
      template.provider = this.props.user.provider;
      delete template.update_version;
      this.props.getTemplateByType(template, type).then((a) => {
        if (a.type === DOWNLOAD_SOW_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            loading: false,
          });
        } else {
          this.setState({ loading: false });
          this.setValidationErrors(a.errorList.data);
        }
      });
    }
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  onHideImplementationLogistics = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.template.json_config.implementation_logistics.hide_in_sow =
      e.target.checked;
    this.setState(newState);
  };

  render() {
    return (
      <div className="template-container">
        <div className="add-template col-md-10">
          <div className="template-add-edit-header">
            <h3>
              {this.state.template.id && this.state.template.id !== 0
                ? "Edit Template"
                : "Add Template"}
            </h3>
            <div className="checkbox-section">
              <Checkbox
                isChecked={this.state.showHidden}
                name="option"
                onChange={(e) => this.onChecboxChangedHidden(e)}
                className="show-hidden-section-checkbox"
              >
                Show hidden sections
              </Checkbox>
              {this.state.template.json_config && (
                <Checkbox
                  isChecked={
                    this.state.template.json_config.implementation_logistics
                      .hide_in_sow
                  }
                  name="option"
                  onChange={(e) => this.onHideImplementationLogistics(e)}
                  className="show-hidden-section-checkbox"
                >
                  Hide Implementation Logistics
                </Checkbox>
              )}
            </div>
          </div>

          <div className="loader">
            <Spinner
              show={
                this.state.loading ||
                this.props.isFetching ||
                this.props.isFetchingTemplate ||
                this.props.isFetchingCategory
              }
            />
          </div>
          <div className="basic-field">
            <Input
              field={{
                value: this.state.template.name,
                label: "Name",
                type: InputFieldType.TEXT,
                isRequired: true,
              }}
              width={6}
              name={"name"}
              placeholder="Enter Template name"
              onChange={(e) => this.handleChangeSimpleFields(e)}
              className="select-type"
              error={this.state.error.name}
            />

            <div
              className="select-type select-sow
          field-section col-lg-6 col-md-6"
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Category
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.category.errorMessage ? `error-input` : ""
                }`}
              >
                <SelectInput
                  name="category"
                  value={this.state.template.category}
                  onChange={(e) => this.handleChangeCategory(e)}
                  options={this.getCategoryOptions()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Category"
                />
              </div>
              {this.state.error.category.errorMessage && (
                <div className="select-sow-error">
                  {this.state.error.category.errorMessage}
                </div>
              )}
            </div>
            <div
              className="select-type select-
                field-section  field-section--required  col-lg-6 col-md-6"
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Service Category
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.service_catalog_category.errorMessage
                    ? `error-input`
                    : ""
                }`}
              >
                <SelectInput
                  name="service_catalog_category"
                  value={this.state.template.service_catalog_category}
                  onChange={(e) => this.handleChangeSimpleFields(e)}
                  options={this.getCategoryList()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Service Category"
                />
              </div>
              {this.state.error.service_catalog_category.errorMessage && (
                <div className="select-serviceCatalog-error field__error">
                  {this.state.error.service_catalog_category.errorMessage}
                </div>
              )}
            </div>
            <div
              className="select-type select-sow
          field-section col-lg-6 col-md-6"
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Service Catalog
                </label>
              </div>
              <div
                className={`${
                  this.state.error.linked_service_catalog.errorMessage
                    ? `error-input`
                    : ""
                }`}
              >
                <SelectInput
                  name="linked_service_catalog"
                  value={
                    this.state.template.linked_service_catalog
                      ? this.state.template.linked_service_catalog
                      : "null"
                  }
                  onChange={(e) => this.handleChangeSimpleFields(e)}
                  options={this.getServiceCatalogsOptions()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Service Catalog"
                  disabled={
                    this.state.template.category &&
                    this.state.template.service_catalog_category
                      ? false
                      : true
                  }
                />
              </div>
              {this.state.error.linked_service_catalog.errorMessage && (
                <div className="select-sow-error">
                  {this.state.error.linked_service_catalog.errorMessage}
                </div>
              )}
            </div>
          </div>
          {this.state.template.json_config &&
            this.renderTemplateByJson(this.state.template.json_config)}
          {this.state.template.json_config &&
            this.props.docSetting &&
            this.renderStaticFields()}
          {this.state.showError && (
            <div className="board-error">Please enter all required fields.</div>
          )}
          <AddVendor
            show={this.state.showVendorModal}
            onClose={() => this.toggleVendorModal(false)}
            onSubmit={() => this.onSubmitVendorModal()}
          />
          <VendorMappingModal
            show={this.state.showVendorMappingModal}
            closeModal={this.closeVendorMappingModal}
            mapping={this.state.currentVendorMapping}
          />
          <PDFViewer
            show={this.state.openPreview}
            onClose={this.toggleOpenPreview}
            titleElement={`View Template Preview`}
            previewHTML={this.state.previewHTML}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={ButtonStyle.DEFAULT}
                onClick={this.toggleOpenPreview}
              />
            }
            className=""
          />
        </div>
        <div className="col-md-2 footer">
          <div className="action-btns">
            <div className="preview-btns">
              {"Preview : "}
              <IconButton
                className="view-button-template"
                icon="coin.svg"
                onClick={(e) => this.previewDoc(e, SowDocType.FIXED_FEE)}
                title={"View Fixed Fee Template "}
              />
              <IconButton
                className="view-button-template"
                icon="timer.svg"
                onClick={(e) => this.previewDoc(e, SowDocType.T_AND_M)}
                title={"View T & M Template "}
              />
            </div>
            {this.state.template.id !== 0 && (
              <div className="doc-details  col-md-12">
                <div>
                  {" "}
                  Created by <span>{this.state.template.author_name}</span>.
                </div>
                <div>
                  {" "}
                  Last updated by{" "}
                  <span>{this.state.template.updated_by_name}</span> on{" "}
                  <span>
                    {utcToLocalInLongFormat(this.state.template.updated_on)}
                  </span>
                </div>
                {Boolean(this.state.template.id) && (
                  <div>
                    {" "}
                    Version -{" "}
                    <span>
                      {this.state.template.version
                        ? this.state.template.version
                        : `${this.state.template.major_version || "1"}.${
                            this.state.template.minor_version
                          }`}
                    </span>
                  </div>
                )}
              </div>
            )}
            <Checkbox
              isChecked={this.state.template.update_version}
              name="option"
              onChange={(e) => this.onChecboxChangeVersionUpdate(e)}
              className="show-hidden-section-checkbox"
            >
              Create major version
            </Checkbox>
            <SquareButton
              content="Close"
              onClick={() => this.props.history.push("/sow/templates")}
              className="save-mapping"
              bsStyle={ButtonStyle.DEFAULT}
            />
          </div>

          {Boolean(this.state.template.id && this.state.template.id !== 0) && (
            <SquareButton
              content={
                <span>
                  {this.state.saving && (
                    <img src={"/assets/icons/loading.gif"} alt="Saving" />
                  )}
                  {"Update Template"}
                </span>
              }
              onClick={() => this.saveTemplate(false)}
              className="save-mapping"
              bsStyle={ButtonStyle.PRIMARY}
              disabled={
                this.props.docSetting &&
                this.props.docSetting.engineering_hourly_cost === null &&
                this.state.template === this.props.template &&
                this.state.saving
              }
              title={`${
                this.props.docSetting &&
                this.props.docSetting.engineering_hourly_cost === null
                  ? "Complete Template setting to enable Save."
                  : ""
              }`}
            />
          )}
          <SquareButton
            content={
              <span>
                {this.state.saving && (
                  <img src={"/assets/icons/loading.gif"} alt="Saving" />
                )}
                {this.state.template.id && this.state.template.id !== 0
                  ? "Update & Close"
                  : "Save Template"}
              </span>
            }
            onClick={() => this.saveTemplate(true)}
            className="save-mapping"
            bsStyle={ButtonStyle.PRIMARY}
            disabled={
              this.props.docSetting &&
              this.props.docSetting.engineering_hourly_cost === null &&
              this.state.template === this.props.template &&
              this.state.saving
            }
            title={`${
              this.props.docSetting &&
              this.props.docSetting.engineering_hourly_cost === null
                ? "Complete Template setting to enable Save."
                : ""
            }`}
          />
        </div>
        <PromptUnsaved
          when={this.state.unsaved}
          navigate={(path) => this.props.history.push(path)}
          shouldBlockNavigation={(location) => {
            if (this.state.unsaved) {
              return true;
            }
            return false;
          }}
          onSaveClick={(e) => this.saveTemplate(true)}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  template: state.sow.template,
  user: state.profile.user,
  baseTemplates: state.sow.baseTemplates,
  vendorMapping: state.sow.vendorMapping,
  categoryList: state.sow.categoryList,
  vendorOptions: state.sow.vendorOptions,
  isFetchingVendors: state.sow.isFetchingVendors,
  isFetchingTemplate: state.sow.isFetchingCategory,
  isFetchingCategory: state.sow.isFetchingTemplate,
  isFetching: state.sow.isFetching,
  serviceCatalogCategories: state.sow.serviceCatalogCategories,
  serviceCatalogShortList: state.sow.serviceCatalogShortList,
  docSetting: state.setting.docSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getVendorsList: () => dispatch(getVendorsList()),
  getBaseTemplate: () => dispatch(getBaseTemplate()),
  getCategoryList: () => dispatch(getCategoryList()),
  getTemplateList: () => dispatch(getTemplateList()),
  getTemplate: (id: number) => dispatch(getTemplate(id)),
  getVendorMapping: () => dispatch(getVendorMappingList()),
  fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
  getShortServiceCatalogList: () => dispatch(getShortServiceCatalogList()),
  saveTemplate: (template: ISoWTemplate) => dispatch(saveTemplate(template)),
  updateTemplate: (template: ISoWTemplate) =>
    dispatch(updateTemplate(template)),
  fetchServiceCategoriesFull: () => dispatch(fetchServiceCategoriesFull()),
  getTemplateByType: (payload: ISoWTemplate, type: SowDocType) =>
    dispatch(getTemplateByType(payload, type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTemplate);
