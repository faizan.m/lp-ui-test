import React from "react";
import { connect } from "react-redux";

import EditButton from "../../../components/Button/editButton";
import Select from "../../../components/Input/Select/select";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";

import { fetchServiceTypeSTT } from "../../../actions/setting";
import {
  deleteTemplate,
  getTemplateList,
  getTemplateByTypeWithId,
  fetchServiceCategoriesFull,
  getShortServiceCatalogList,
  EDIT_TEMPLATE_SUCCESS,
  FETCH_TEMPLATE_PREVIEW_SUCCESS,
} from "../../../actions/sow";
import SquareButton from "../../../components/Button/button";
import DeleteButton from "../../../components/Button/deleteButton";
import Checkbox from "../../../components/Checkbox/checkbox";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import ServiceCatalogFilter from "../Template/filterServiceCatalog";
import { searchInFields } from "../../../utils/searchListUtils";
import IconButton from "../../../components/Button/iconButton";
import ViewSOWHistory from "./templateHistory";
import ViewTemplateCDHistory from "./templateCDHistory";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import "../../../commonStyles/filtersListing.scss";
import "./style.scss";
interface ITemplateListProps extends ICommonProps {
  Template: any;
  getTemplateList: any;
  createTemplate: any;
  editTemplate: any;
  isFetching: boolean;
  loggenInUser: ISuperUser;
  templates: any[];
  deleteTemplate: any;
  serviceCatalogCategories: any;
  serviceType: any;
  fetchServiceCategoriesFull: any;
  fetchServiceTypeSTT: any;
  getShortServiceCatalogList: any;
  serviceCatalogShortList: IserviceCatalogShort[];
  isFetchingTemplates: boolean;
  getTemplateByType: any;
}

interface ITemplateListtate {
  errorList?: any;
  TemplateList: any[];
  rows: any[];
  isTemplatePosting: boolean;
  isopenConfirm: boolean;
  id: string;
  searchString: string;
  isFilterModalOpen: boolean;
  filters: IServiceCatalogFilters;
  showDisable: boolean;
  viewhistory: boolean;
  viewCDhistory: boolean;
  openPreview: boolean;
  previewPDFIds: number[];
  previewHTML: any;
}

class TemplateListing extends React.Component<
  ITemplateListProps,
  ITemplateListtate
> {
  constructor(props: ITemplateListProps) {
    super(props);

    this.state = {
      TemplateList: [],
      isTemplatePosting: false,
      isopenConfirm: false,
      id: "",
      searchString: "",
      rows: [],
      isFilterModalOpen: false,
      filters: {
        service_type: [],
        service_category: [],
        service_technology_types: [],
        linked_service_catalog: [],
      },
      showDisable: false,
      viewhistory: false,
      viewCDhistory: false,
      openPreview: false,
      previewPDFIds: [],
      previewHTML: null,
    };
  }

  componentDidMount() {
    this.props.getTemplateList();
    this.props.fetchServiceCategoriesFull();
    this.props.getShortServiceCatalogList();
    this.props.fetchServiceTypeSTT();
  }

  componentDidUpdate(prevProps: ITemplateListProps) {
    if (this.props.templates && prevProps.templates !== this.props.templates) {
      const rows = this.getRows(this.props, this.state.filters, "");
      this.setState({
        rows,
      });
    }
  }

  onEditRowClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/template/${id}`);
  };

  onCloneClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/template/${id}?cloned=true`);
  };
  onCreateSOWClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/sow/0?template=${id}`);
  };

  toggleviewCDHistoryPopup = (e, reverted) => {
    if (reverted) {
      this.props.getTemplateList();
    }
    this.setState({
      viewCDhistory: false,
    });
  };

  onClickViewviewCDHistory = () => {
    this.setState({
      viewCDhistory: true,
    });
  };

  renderTopBar = () => {
    return (
      <div className="Template-users-listing__actions">
        <div className="row-action header-panel">
          <div className="left">
            <Input
              field={{
                label: "",
                type: InputFieldType.SEARCH,
                value: this.state.searchString,
                isRequired: false,
              }}
              width={6}
              placeholder="Search Templates"
              name="searchString"
              onChange={this.handleChange}
              className="search"
            />
            <Checkbox
              isChecked={this.state.showDisable}
              name="option"
              onChange={(e) => this.onChecboxChanged(e)}
            >
              Show disabled templates
            </Checkbox>
          </div>
          <div className="field-section actions-right">
            <SquareButton
              onClick={this.onClickViewviewCDHistory}
              content={
                <span>
                  <img alt="" src="/assets/icons/version.svg" />
                  History
                </span>
              }
              bsStyle={ButtonStyle.PRIMARY}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.templates && this.props.templates.length === 0
              }
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  onChecboxChanged = (event: any) => {
    const showDisable = event.target.checked;
    this.setState({
      showDisable,
    });
    this.props.getTemplateList(showDisable);
  };
  getRows = (
    nextProps: ITemplateListProps,
    filters: IServiceCatalogFilters,
    searchString?: string
  ) => {
    let templates = nextProps.templates;
    const search = searchString ? searchString : this.state.searchString;

    if (filters) {
      // Add filter for type once included.
      const service_category = filters.service_category;
      const service_technology_types = filters.service_technology_types;
      const service_type = filters.service_type;
      const linked_service_catalog = filters.linked_service_catalog;

      templates =
        templates &&
        templates.filter((template) => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
          const hasFilteredServiceType =
            service_category.length > 0
              ? service_category.includes(template.service_catalog_category)
              : true;
          const hasFilteredServiceTechType =
            service_technology_types.length > 0
              ? service_technology_types.includes(
                  template.linked_service_catalog
                )
              : true;
          const hasFilteredCatalogCategory =
            service_type.length > 0
              ? service_type.includes(template.category)
              : true;
          const hasFilteredCataloLinked =
            linked_service_catalog.length > 0
              ? this.getIsCatalog(
                  linked_service_catalog,
                  template.linked_service_catalog
                )
              : true;

          return (
            hasFilteredServiceType &&
            hasFilteredServiceTechType &&
            hasFilteredCatalogCategory &&
            hasFilteredCataloLinked
          );
        });
    }
    if (search && search.length > 0) {
      templates = templates.filter((row) =>
        searchInFields(row, search, [
          "name",
          "author_name",
          "updated_on",
          "updated_by_name",
          "category_name",
          "type",
        ])
      );
    }
    const rows: any = templates.map((template, index) => ({
      name: template.name,
      category_name: template.category_name,
      type: template.type,
      author_name: template.author_name ? template.author_name : "N.A.",
      updated_on: template.updated_on ? template.updated_on : "N.A.",
      updated_by_name: template.updated_by_name
        ? template.updated_by_name
        : "N.A.",
      id: template.id,
      service_category: template.service_catalog_category,
      service_technology_types: template.service_technology_types,
      linked_service_catalog: template.linked_service_catalog,
      category: template.category,
      version: template.version,
      template: template,
      index,
    }));

    return rows;
  };
  getIsCatalog = (isCatalog: any, value: any) => {
    let isTemp = false;
    if (isCatalog.includes("Available") && value) {
      isTemp = true;
    }
    if (isCatalog.includes("NotAvailable") && !value) {
      isTemp = true;
    }
    if (isCatalog.length === 2) {
      isTemp = true;
    }

    return isTemp;
  };

  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.handleRows();
      }
    );
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      filters.service_type.length > 0 ||
      filters.service_category.length > 0 ||
      filters.service_technology_types.length > 0 ||
      filters.linked_service_catalog.length > 0
        ? true
        : false;
    const serviceTypes = this.props.serviceType
      ? this.props.serviceType.map((user) => ({
          value: user.id,
          label: `${user.name}`,
        }))
      : [];

    const serviceCategories = this.props.serviceCatalogCategories
      ? this.props.serviceCatalogCategories.map((user) => ({
          value: user.id,
          label: `${user.name}`,
        }))
      : [];

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.service_type.length > 0 && (
          <div className="section-show-filters">
            <label>Service Type: </label>
            <Select
              name="service_type"
              value={filters.service_type}
              onChange={this.onFilterChange}
              options={serviceTypes}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.service_category.length > 0 && (
          <div className="section-show-filters">
            <label>Service Category: </label>
            <Select
              name="service_category"
              value={filters.service_category}
              onChange={this.onFilterChange}
              options={serviceCategories}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}

        {filters.service_technology_types.length > 0 && (
          <div className="section-show-filters">
            <label>Service Catalog: </label>
            <Select
              name="service_technology_types"
              value={filters.service_technology_types}
              onChange={this.onFilterChange}
              options={this.getServiceCatalogsOptions()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.linked_service_catalog.length > 0 && (
          <div className="section-show-filters">
            <label>Catalog: </label>
            <Select
              name="linked_service_catalog"
              value={filters.linked_service_catalog}
              onChange={this.onFilterChange}
              options={[
                { value: "Available", label: "Available" },
                { value: "NotAvailable", label: "Not Available" },
              ]}
              multi={true}
              placeholder="Select"
            />
          </div>
        )}
      </div>
    ) : null;
  };

  getServiceCatalogsOptions = () => {
    const serviceCatalogShortList = this.props.serviceCatalogShortList
      ? this.props.serviceCatalogShortList.map((s) => ({
          value: s.id,
          label: s.service_name,
          disabled: false,
        }))
      : [];

    return serviceCatalogShortList;
  };

  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.filters,
      this.state.searchString
    );
    this.setState({ rows });
  };

  handleChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };
  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteTemplate(this.state.id).then((action) => {
      if (action.type === EDIT_TEMPLATE_SUCCESS) {
        this.props.getTemplateList(this.state.showDisable);
        this.toggleConfirmOpen();
      }
    });
  };

  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: "",
    });
  };

  onClickViewHistory(id: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      id,
    });
  }

  getSOWFromVersions = (sow: any) => {
    this.setState({
      viewhistory: false,
      id: "",
    });
  };
  addTemplateClick = () => {
    this.props.history.push("/sow/template/0");
  };
  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };
  onFiltersUpdate = (filters: IServiceCatalogFilters) => {
    if (filters) {
      this.setState({ filters }, () => {
        this.handleRows();
      });
      this.toggleFilterModal();
    }
  };
  onPDFPreviewClick(template: any, e: any, type): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, template.id],
    });
    this.props.getTemplateByType(template.id, type).then((a) => {
      if (a.type === FETCH_TEMPLATE_PREVIEW_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          id: "",
          previewPDFIds: this.state.previewPDFIds.filter(
            (id) => template.id !== id
          ),
        });
      } else {
        this.setState({
          id: "",
          previewPDFIds: [],
        });
      }
    });
  }
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  render() {
    const getPDFPrevieMarkUp = (cell) => {
      if (this.state.previewPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <>
            <IconButton
              className="view-button-template"
              icon="coin.svg"
              onClick={(e) =>
                this.onPDFPreviewClick(cell.original.template, e, "Fixed Fee")
              }
              title={"View Fixed Fee Template "}
            />
            <IconButton
              className="view-button-template"
              icon="timer.svg"
              onClick={(e) =>
                this.onPDFPreviewClick(cell.original.template, e, "T & M")
              }
              title={"View T & M Template "}
            />
          </>
        );
      }
    };
    const columns: ITableColumn[] = [
      {
        accessor: "type",
        Header: "Service & Category",
        id: "type",
        width: 130,
        sortable: false,
        Cell: (cell) => (
          <div className="icons-template">
            {cell.original.linked_service_catalog && (
              <img
                src={`/assets/icons/verified.svg`}
                className="left"
                alt=""
                title={"Service Available"}
              />
            )}
            {!cell.original.linked_service_catalog && (
              <img
                src={`/assets/icons/cancel.svg`}
                className="left"
                alt=""
                title={"Service Not Available"}
              />
            )}
            {cell.original.category_name === "Collaboration" && (
              <img
                title="Collaboration"
                alt=""
                src="/assets/icons/collabHomeHover.webp"
              />
            )}
            {cell.original.category_name === "Cloud" && (
              <img
                title="Cloud"
                alt="Cloud"
                src="/assets/new-icons/cloud.svg"
                className="cloud-svg"
              />
            )}
            {cell.original.category_name === "Networking" && (
              <img
                title="Networking"
                alt=""
                src="/assets/icons/NetworkingHomeHover.webp"
              />
            )}
            {cell.original.category_name === "Data Center" && (
              <img
                title="Data Center"
                alt=""
                src="/assets/icons/lifecycle.webp"
              />
            )}
            {cell.original.category_name === "Security" && (
              <img
                title="Security"
                alt=""
                src="/assets/icons/SecurityHomeHover.webp"
              />
            )}
          </div>
        ),
      },
      {
        accessor: "name",
        Header: "Name",
        id: "name",
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: "author_name",
        Header: "Author",
        id: "author_name",
        sortable: true,
        Cell: (c) => <div>{c.value}</div>,
      },
      {
        accessor: "updated_by_name",
        Header: "Updated by",
        id: "updated_by_name",
        Cell: (c) => <div>{c.value}</div>,
        sortable: true,
      },
      {
        accessor: "version",
        Header: "Current Version",
        Cell: (c) => <div>{c.original.version}</div>,
        sortable: false,
      },
      {
        accessor: "updated_on",
        Header: "Updated On",
        id: "updated_on",
        sortable: true,
        width: 110,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Versions",
        sortable: false,
        width: 65,
        Cell: (cell) => (
          <IconButton
            icon="version.svg"
            onClick={(e) => {
              this.onClickViewHistory(cell.original.id, e);
            }}
            className="version-btn"
            title={"Show SOW versions"}
          />
        ),
      },
      {
        accessor: "id",
        Header: "Preview",
        width: 80,
        sortable: false,
        Cell: (cell) => (
          <div className="view-icons-template">{getPDFPrevieMarkUp(cell)}</div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        sortable: false,
        Cell: (cell) => (
          <div>
            <EditButton onClick={(e) => this.onEditRowClick(cell.value, e)} />
            <DeleteButton onClick={(e) => this.onDeleteRowClick(cell, e)} />
            <DeleteButton
              type="cloned"
              title="Clone template"
              onClick={(e) => this.onCloneClick(cell.value, e)}
            />
            <DeleteButton
              type="create"
              title="Create SOW"
              onClick={(e) => this.onCreateSOWClick(cell.value, e)}
            />
          </div>
        ),
      },
    ];

    return (
      <div className="template-listing">
        <div className="header">
          <h3>Templates</h3>
          <SquareButton
            content="Add Template"
            onClick={this.addTemplateClick}
            className="save-mapping"
            bsStyle={ButtonStyle.PRIMARY}
          />
        </div>
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows || []}
          customTopBar={this.renderTopBar()}
          className={`Template-users-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          loading={this.props.isFetchingTemplates}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
        />

        <ServiceCatalogFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          service_type={this.props.serviceType}
          service_category={this.props.serviceCatalogCategories}
          linked_service_catalog={this.props.serviceCatalogShortList}
          prevFilters={this.state.filters}
        />
        <ViewSOWHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
          getSow={this.getSOWFromVersions}
        />
        {this.state.viewCDhistory && (
          <ViewTemplateCDHistory
            show={this.state.viewCDhistory}
            onClose={this.toggleviewCDHistoryPopup}
          />
        )}
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View Template Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={ButtonStyle.DEFAULT}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  loggenInUser: state.profile.user,
  templates: state.sow.templates,
  isFetching: state.sow.isFetching,
  isFetchingTemplates: state.sow.isFetchingTemplates,
  serviceType: state.setting.serviceType,
  serviceCatalogCategories: state.sow.serviceCatalogCategories,
  serviceCatalogShortList: state.sow.serviceCatalogShortList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getTemplateList: (showDisable: boolean) =>
    dispatch(getTemplateList(showDisable)),
  deleteTemplate: (id: any) => dispatch(deleteTemplate(id)),
  fetchServiceCategoriesFull: () => dispatch(fetchServiceCategoriesFull()),
  fetchServiceTypeSTT: () => dispatch(fetchServiceTypeSTT()),
  getShortServiceCatalogList: () => dispatch(getShortServiceCatalogList()),
  getTemplateByType: (id: number, type: string) =>
    dispatch(getTemplateByTypeWithId(id, type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TemplateListing);
