import React from 'react';
import { connect } from 'react-redux';

import Spinner from '../../../components/Spinner';
import 'easymde/dist/easymde.min.css';
import { cloneDeep } from 'lodash';
import moment from 'moment';
import SimpleMDE from 'react-simplemde-editor';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import { fetchSOWDOCSetting } from '../../../actions/setting';
import {
    fetchServiceCategoriesFull,
    getBaseTemplate,
    getCategoryList,
    getShortServiceCatalogList,
    getTemplate,
    getTemplateList,
    saveTemplate,
    updateTemplate,
    uploadImage,
} from '../../../actions/sow';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';
import {
    getCustomerCost,
    getCustomerCostFixedFee,
    getCustomerCostTM,
    getFixedFeeInternalCost,
    getInternalCostTM,
    getRecommendedHours,
} from '../../../utils/sowCalculations';
import './style.scss';
import Checkbox from '../../../components/Checkbox/checkbox';
import ModalBase from '../../../components/ModalBase/modalBase';
import SquareButton from '../../../components/Button/button';

interface IViewTemplateProps extends ICommonProps {
    show: boolean;
    onClose: (e: any) => void;
    template: any;
    templateDetails: any;
    isFetching: boolean;
    loggenInUser: ISuperUser;
    baseTemplates: any;
    getBaseTemplate: any;
    getTemplateList: any;
    saveTemplate: any;
    getTemplate: any;
    isFetchingTemplate: boolean;
    updateTemplate: any;
    getCategoryList: any;
    categoryList: any;
    isFetchingCategory: boolean;
    errorList?: any;
    addSuccessMessage: TShowSuccessMessage;
    addErrorMessage: TShowErrorMessage;
    uploadImage: any;
    fetchServiceCategoriesFull: any;
    serviceCatalogCategories: any;
    getShortServiceCatalogList: any;
    serviceCatalogShortList: IserviceCatalogShort[];
    fetchSOWDOCSetting: any;
    docSetting: IDOCSetting;
}

interface IViewTemplatetate {
    showHidden: boolean;
    errorList?: any;
    ViewTemplate: any[];
    template: any;
    isViewTemplatePosting: boolean;
    isopenConfirm: boolean;
    id: string;
    isCollapsed: boolean[];
    isValid: boolean;
    showError: boolean;
    errorsLabel: {
        [fieldName: string]: IFieldValidation;
    };
    errorsValue: {
        [fieldName: string]: IFieldValidation;
    };
    loading: boolean;
    technologyTypes: any;
    versionObj: boolean;
}

class ViewTemplate extends React.Component<IViewTemplateProps, IViewTemplatetate> {
    static emptyErrorState: IFieldValidation = {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
    };
    static hideIcons = [
        'guide',
        'heading',
        'fullscreen',
        'image',
        'side-by-side',
        'bold',
        'italic',
        'quote',
        'unordered-list',
        'ordered-list',
        'link',
        'preview']

    constructor(props: IViewTemplateProps) {
        super(props);
        this.state = this.getEmptyState();
    }
    getEmptyState = () => ({
        template: {
            json_config: null,
            name: '',
            id: 0,
            service_technology_types: [],
            linked_service_catalog: null,
            update_version: false,
            major_version: 0,
            minor_version: 0,
            version_description: '',
        },
        showHidden: false,
        ViewTemplate: [],
        isViewTemplatePosting: false,
        isopenConfirm: false,
        id: '',
        isCollapsed: [],
        errorsLabel: {},
        errorsValue: {},
        isValid: false,
        showError: false,
        loading: false,
        technologyTypes: [],
        versionObj: false,
    });

    componentDidMount() {
        this.props.getCategoryList();
        this.props.getTemplateList();
        this.props.fetchServiceCategoriesFull();
        this.props.getShortServiceCatalogList();
        this.props.fetchSOWDOCSetting();
    }

    componentDidUpdate(prevProps: IViewTemplateProps) {
        if (this.props.templateDetails !== prevProps.templateDetails) {
            const sowObj = this.props.templateDetails;


            let types = [];
            if (this.props.categoryList) {
                const types1 = this.props.categoryList.filter(
                    type => sowObj.category_id === type.id
                );
                types =
                    types1 &&
                    types1[0].technology_types.map(t => ({
                        value: t.id,
                        label: t.name,
                        disabled: false,
                    }));
            }
            const template = {
                json_config: sowObj.json_config,
                name: sowObj.name,
                service_catalog_category: sowObj.service_catalog_category_id,
                category: sowObj.category_id,
                customer: sowObj.customer_id,
                doc_type: sowObj.doc_type,
                quote_id: sowObj.quote_id,
                user: sowObj.user,
                id: sowObj.id,
                author_name: sowObj.author_name
                    ? sowObj.author_name
                    : 'N.A.',
                updated_on: sowObj.updated_on
                    ? sowObj.updated_on
                    : 'N.A.',
                updated_by_name: sowObj.updated_by_name,
                update_version: false,
                major_version: sowObj.major_version,
                minor_version: sowObj.minor_version
            };
            this.setState({
                technologyTypes: types,
                template,
                isCollapsed: Object.keys(template.json_config).map(
                    key => false
                ),
            });
        }
    }

    getServiceCatalogsOptions = () => {
        const serviceCatalogShortList = this.props.serviceCatalogShortList;
        let templates = [];
        if (serviceCatalogShortList && this.state.template.category) {
            templates = serviceCatalogShortList.filter(
                id => this.state.template.category === id.service_type.id
            );
        }
        if (
            serviceCatalogShortList &&
            this.state.template.category &&
            this.state.template.service_catalog_category
        ) {
            templates = templates.filter(
                id =>
                    this.state.template.service_catalog_category ===
                    id.service_category.id
            );
        }
        const templatesList = templates
            ? templates.map(t => ({
                value: t.id,
                label: t.service_name,
                disabled: false,
            }))
            : [];

        templatesList.push({ label: 'None', value: 'null', disabled: false });

        return templatesList;
    };
    getCategoryOptions = () => {
        const categories = this.props.categoryList
            ? this.props.categoryList.map(cat => ({
                value: cat.id,
                label: cat.name,
                disabled: false,
            }))
            : [];

        return categories;
    };
    handleChangeCategory = e => {
        const newState = cloneDeep(this.state);
        newState.template[e.target.name] = e.target.value;
        let types = [];
        if (e.target.value !== '' && this.props.categoryList) {
            const types1 = this.props.categoryList.filter(
                type => e.target.value === type.id
            );

            types =
                types1 &&
                types1[0].technology_types.map(t => ({
                    value: t.id,
                    label: t.name,
                    disabled: false,
                }));
        }
        (newState.technologyTypes as any) = types;
        (newState.template.service_technology_types as any) = [];
        this.setState(newState);
    };

    handleChange = (e, key, field, type) => {
        const newState = cloneDeep(this.state);
        (newState.template.json_config[key][field][type] as any) = e.target.value;
        this.setState(newState);
    };
    handleChangeSubSection = (e, parentKey, key, field, type) => {
        const newState = cloneDeep(this.state);
        (newState.template.json_config[parentKey].sections[key][field][
            type
        ] as any) = e.target.value;
        this.setState(newState);
    };

    handleChangeMarkdownSubSection = (e, parentKey, key, field, type) => {
        const newState = cloneDeep(this.state);
        (newState.template.json_config[key][field][type] as any) = e;
        //(newState.sow.json_config[key][field].markdownKey as any) = Math.random();
        this.setState(newState);
    };

    handleChangeName = e => {
        const newState = cloneDeep(this.state);
        newState.template[e.target.name] = e.target.value;
        this.setState(newState);
    };

    handleChangeMarkdown = (e, key, field, type) => {
        const newState = cloneDeep(this.state);
        (newState.template.json_config[key][field][type] as any) = e;
        this.setState(newState);
    };

    handleClickResetMarkdown = (e, key, field, type) => {
        const newState = cloneDeep(this.state);
        (newState.template.json_config[key][field][
            type
        ] as any) = this.props.docSetting[key];
        this.setState(newState);
    };

    getIntance = instance => {
        // You can now store and manipulate the simplemde instance.
        if (this.state.template.id && this.state.template.id !== 0) {
            instance.togglePreview();
        }
    };

    getCategoryList = () => {
        const serviceCatalogCategories = this.props.serviceCatalogCategories
            ? this.props.serviceCatalogCategories.map(t => ({
                value: t.id,
                label: t.name,
                disabled: t.is_disabled,
            }))
            : [];

        return serviceCatalogCategories;
    };

    // render methods
    renderTemplateByJson = rawJson => {
        let count = 0;
        const json = Object.assign({}, rawJson);
        delete json.service_cost;

        return (
            <div className="template-fields col-md-12 row">
                {json &&
                    Object.keys(json).length > 0 &&
                    Object.keys(json)
                        .sort((a, b) => json[a].ordering - json[b].ordering)
                        .filter(field =>
                            this.state.showHidden === true
                                ? true
                                : !json[field].default_hidden
                        )
                        .map((key, fieldIndex) => {
                            const isCollapsed = !this.state.isCollapsed[fieldIndex];
                            const toggleCollapsedState = () =>
                                this.setState(prevState => ({
                                    isCollapsed: [
                                        ...prevState.isCollapsed.slice(0, fieldIndex),
                                        !prevState.isCollapsed[fieldIndex],
                                        ...prevState.isCollapsed.slice(fieldIndex + 1),
                                    ],
                                }));

                            return (
                                <div
                                    className={`field ${isCollapsed ? 'field--collapsed' : ''}`}
                                    key={fieldIndex}
                                >
                                    <div
                                        className="section-heading"
                                        onClick={toggleCollapsedState}
                                    >
                                        {json[key].section_label}{' '}
                                        <div className="action-collapse">
                                            {isCollapsed ? '+' : '-'}
                                        </div>
                                    </div>
                                    <div className="body-section">
                                        {' '}
                                        {key &&
                                            Object.keys(json[key])
                                                .sort(
                                                    (a, b) =>
                                                        ((json[key][a] && json[key][a].ordering) || 0) -
                                                        ((json[key][b] && json[key][b].ordering) || 0)
                                                )
                                                .map((field, i) => {
                                                    if (
                                                        field === 'section_label' ||
                                                        field === 'visible_in' ||
                                                        field === 'ordering' ||
                                                        field === 'default_hidden'
                                                    ) {
                                                        return false;
                                                    }

                                                    count++;

                                                    return json[key].sections &&
                                                        json[key].sections.length > 0 ? (
                                                            // tslint:disable-next-line: max-line-length
                                                            this.renderSubSectionsByJson(
                                                                json[key].sections,
                                                                key
                                                            )
                                                        ) : (
                                                            <div
                                                                className={`${
                                                                    json[key][field].type === 'MARKDOWN'
                                                                        ? 'single-field col-lg-12 col-md-12 col-xs-12'
                                                                        : 'single-field col-lg-12 col-md-12 col-xs-12'
                                                                    }`}
                                                                key={i}
                                                            >
                                                                {json[key][field].type === 'MARKDOWN' && (
                                                                    <div
                                                                        className={`${
                                                                            this.state.errorsValue &&
                                                                                this.state.errorsValue[count] &&
                                                                                this.state.errorsValue[count].errorMessage
                                                                                ? 'markdown markdown-error'
                                                                                : 'markdown'
                                                                            }`}
                                                                    >
                                                                        {json[key][field].is_label_editable ===
                                                                            'true' && (
                                                                                <Input
                                                                                    field={{
                                                                                        value: json[key][field].label,
                                                                                        label: json[key][field].section_label,
                                                                                        type: InputFieldType.TEXT,
                                                                                        isRequired: true,
                                                                                    }}
                                                                                    width={6}
                                                                                    name={field}
                                                                                    disabled={true}
                                                                                    onChange={e =>
                                                                                        this.handleChange(
                                                                                            e,
                                                                                            key,
                                                                                            field,
                                                                                            'label'
                                                                                        )
                                                                                    }
                                                                                    className="label-input"
                                                                                    error={this.state.errorsLabel[count]}
                                                                                />
                                                                            )}

                                                                        <SimpleMDE
                                                                            key={json[key][field] + count}
                                                                            onChange={e => null}
                                                                            options={{
                                                                                placeholder: '',
                                                                                autofocus: false,
                                                                                spellChecker: true,
                                                                                toolbar: false
                                                                            }}
                                                                            label={`${
                                                                                json[key][field].is_label_editable ===
                                                                                    'true'
                                                                                    ? 'Value'
                                                                                    : json[key][field].label
                                                                                }`}
                                                                            value={json[key][field].value}
                                                                            className={`${
                                                                                json[key][field].is_required === 'true'
                                                                                    ? `markdown-editor-input
                                        markdown-required`
                                                                                    : 'markdown-editor-input'
                                                                                }`}
                                                                            getMdeInstance={this.getIntance}
                                                                        />
                                                                        {this.state.errorsValue &&
                                                                            this.state.errorsValue[count] &&
                                                                            this.state.errorsValue[count]
                                                                                .errorMessage && (
                                                                                <div className="select-markdown-error">
                                                                                    {
                                                                                        this.state.errorsValue[count]
                                                                                            .errorMessage
                                                                                    }
                                                                                </div>
                                                                            )}
                                                                    </div>
                                                                )}
                                                                {json[key][field].type === 'TEXTBOX' && (
                                                                    <div className="input-label-box">
                                                                        {json[key][field].is_label_editable ===
                                                                            'true' && (
                                                                                <Input
                                                                                    field={{
                                                                                        value: json[key][field].label,
                                                                                        label: json[key][field].section_label,
                                                                                        type: InputFieldType.TEXT,
                                                                                        isRequired: true,
                                                                                    }}
                                                                                    width={6}
                                                                                    name={field}
                                                                                    onChange={e => null}
                                                                                    className="label-input"
                                                                                    error={this.state.errorsLabel[count]}
                                                                                />
                                                                            )}
                                                                        <Input
                                                                            field={{
                                                                                value: json[key][field].value,
                                                                                label: `${
                                                                                    json[key][field].is_label_editable ===
                                                                                        'true'
                                                                                        ? 'Value'
                                                                                        : json[key][field].label
                                                                                    }`,
                                                                                type: InputFieldType.TEXT,
                                                                                isRequired:
                                                                                    json[key][field].is_required &&
                                                                                    JSON.parse(
                                                                                        json[key][field].is_required
                                                                                    ),
                                                                            }}
                                                                            width={8}
                                                                            name={field}
                                                                            onChange={e => null}
                                                                            error={this.state.errorsValue[count]}
                                                                        />
                                                                    </div>
                                                                )}
                                                                {json[key][field].type === 'RADIOBOX' && (
                                                                    <div
                                                                        key={field}
                                                                        className="options input-label-box"
                                                                    >
                                                                        {json[key][field].is_label_editable ===
                                                                            'true' && (
                                                                                <Input
                                                                                    field={{
                                                                                        value: json[key][field].label,
                                                                                        label: json[key][field].section_label,
                                                                                        type: InputFieldType.TEXT,
                                                                                        isRequired: true,
                                                                                    }}
                                                                                    width={6}
                                                                                    name={field}
                                                                                    onChange={e =>
                                                                                        this.handleChange(
                                                                                            e,
                                                                                            key,
                                                                                            field,
                                                                                            'label'
                                                                                        )
                                                                                    }
                                                                                    disabled={true}
                                                                                    className="label-input"
                                                                                    error={this.state.errorsLabel[count]}
                                                                                />
                                                                            )}
                                                                        <div className="options-content">
                                                                            <Input
                                                                                field={{
                                                                                    value: json[key][field].value,
                                                                                    label:
                                                                                        json[key][field].is_label_editable ===
                                                                                            'false'
                                                                                            ? json[key][field].label
                                                                                            : '',
                                                                                    type: InputFieldType.RADIO,
                                                                                    isRequired: false,
                                                                                    options: json[key][field].options.map(
                                                                                        role => ({
                                                                                            value: role,
                                                                                            label: role,
                                                                                        })
                                                                                    ),
                                                                                }}
                                                                                width={8}
                                                                                name={field + count}
                                                                                onChange={e =>
                                                                                    this.handleChange(
                                                                                        e,
                                                                                        key,
                                                                                        field,
                                                                                        'value'
                                                                                    )
                                                                                }
                                                                                disabled={true}
                                                                                error={this.state.errorsValue[count]}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                )}
                                                            </div>
                                                        );
                                                })}
                                    </div>
                                </div>
                            );
                        })}
            </div>
        );
    };

    getColWidth = type => {
        let width = 'col-md-6';
        switch (type) {
            case 'MARKDOWN':
                width = 'col-md-6';
                break;
            case 'TEXTBOX':
                width = 'col-md-5';
                break;
            case 'TEXTAREA':
                width = 'col-md-6';
                break;
            case 'RADIOBOX':
                width = 'col-md-3';
                break;

            default:
                break;
        }

        return width;
    };

    renderSubSectionsByJson = (rawJson, parentKey) => {
        let count = 0;
        const json = Object.assign({}, rawJson);

        return (
            <div className="sub-section-fields col-md-12 row" key={parentKey}>
                {json &&
                    Object.keys(json).length > 0 &&
                    Object.keys(json)
                        .sort((a, b) => json[a].ordering - json[b].ordering)
                        .map((key, fieldIndex) => {
                            return (
                                <div className={`sub-section col-md-12`} key={fieldIndex}>
                                    {' '}
                                    {key &&
                                        Object.keys(json[key])
                                            .sort(
                                                (a, b) =>
                                                    ((json[key][a] && json[key][a].ordering) || 0) -
                                                    ((json[key][b] && json[key][b].ordering) || 0)
                                            )
                                            .map((field, i) => {
                                                count++;

                                                return (
                                                    <div
                                                        className={`${this.getColWidth(
                                                            json[key][field].type
                                                        )}`}
                                                        key={i}
                                                    >
                                                        {json[key][field].type === 'MARKDOWN' && (
                                                            <div
                                                                className={`${
                                                                    this.state.errorsValue &&
                                                                        this.state.errorsValue[count] &&
                                                                        this.state.errorsValue[count].errorMessage
                                                                        ? 'markdown markdown-error'
                                                                        : 'markdown'
                                                                    }`}
                                                            >
                                                                {json[key][field].is_label_editable ===
                                                                    'true' && (
                                                                        <Input
                                                                            field={{
                                                                                value: json[key][field].label,
                                                                                label: json[key][field].section_label,
                                                                                type: InputFieldType.TEXT,
                                                                                isRequired: true,
                                                                            }}
                                                                            width={12}
                                                                            name={field}
                                                                            onChange={e =>
                                                                                this.handleChangeSubSection(
                                                                                    e,
                                                                                    parentKey,
                                                                                    key,
                                                                                    field,
                                                                                    'label'
                                                                                )
                                                                            }
                                                                            className="label-input"
                                                                            error={this.state.errorsLabel[count]}
                                                                        />
                                                                    )}


                                                                <SimpleMDE
                                                                    onChange={e =>
                                                                        this.handleChangeMarkdownSubSection(
                                                                            e,
                                                                            parentKey,
                                                                            key,
                                                                            field,
                                                                            'value'
                                                                        )
                                                                    }
                                                                    options={{
                                                                        placeholder: '',
                                                                        autofocus: false,
                                                                        spellChecker: true,
                                                                        toolbar: false
                                                                    }}
                                                                    label={`${
                                                                        json[key][field].is_label_editable ===
                                                                            'true'
                                                                            ? 'Value'
                                                                            : json[key][field].label
                                                                        }`}
                                                                    value={json[key][field].value}
                                                                    className={`${
                                                                        json[key][field].is_required === 'true'
                                                                            ? `markdown-editor-input
                                        markdown-required`
                                                                            : 'markdown-editor-input'
                                                                        }`}
                                                                    key={json[key][field].markdownKey}
                                                                    getMdeInstance={this.getIntance}
                                                                />
                                                                {this.state.errorsValue &&
                                                                    this.state.errorsValue[count] &&
                                                                    this.state.errorsValue[count]
                                                                        .errorMessage && (
                                                                        <div className="select-markdown-error">
                                                                            {
                                                                                this.state.errorsValue[count]
                                                                                    .errorMessage
                                                                            }
                                                                        </div>
                                                                    )}
                                                            </div>
                                                        )}
                                                        {json[key][field].type === 'TEXTBOX' && (
                                                            <div className="input-label-box">
                                                                {json[key][field].is_label_editable ===
                                                                    'true' && (
                                                                        <Input
                                                                            field={{
                                                                                value: json[key][field].label,
                                                                                label: json[key][field].section_label,
                                                                                type: InputFieldType.TEXT,
                                                                                isRequired: true,
                                                                            }}
                                                                            width={12}
                                                                            name={field}
                                                                            onChange={e =>
                                                                                this.handleChangeSubSection(
                                                                                    e,
                                                                                    parentKey,
                                                                                    key,
                                                                                    field,
                                                                                    'label'
                                                                                )
                                                                            }
                                                                            className="label-input"
                                                                            error={
                                                                                json[key][field].error &&
                                                                                json[key][field].error
                                                                            }
                                                                        />
                                                                    )}
                                                                <Input
                                                                    field={{
                                                                        value: json[key][field].value,
                                                                        label: `${
                                                                            json[key][field].is_label_editable ===
                                                                                'true'
                                                                                ? 'Value'
                                                                                : json[key][field].label
                                                                            }`,
                                                                        type: InputFieldType.TEXT,
                                                                        isRequired:
                                                                            json[key][field].is_required &&
                                                                            JSON.parse(json[key][field].is_required),
                                                                    }}
                                                                    width={12}
                                                                    name={field}
                                                                    onChange={e =>
                                                                        this.handleChangeSubSection(
                                                                            e,
                                                                            parentKey,
                                                                            key,
                                                                            field,
                                                                            'value'
                                                                        )
                                                                    }
                                                                    error={
                                                                        json[key][field].error &&
                                                                        json[key][field].error
                                                                    }
                                                                />
                                                            </div>
                                                        )}
                                                        {json[key][field].type === 'TEXTAREA' && (
                                                            <div className="input-label-box">
                                                                {json[key][field].is_label_editable ===
                                                                    'true' && (
                                                                        <Input
                                                                            field={{
                                                                                value: json[key][field].label,
                                                                                label: json[key][field].section_label,
                                                                                type: InputFieldType.TEXT,
                                                                                isRequired: true,
                                                                            }}
                                                                            width={12}
                                                                            name={field}
                                                                            onChange={e =>
                                                                                this.handleChangeSubSection(
                                                                                    e,
                                                                                    parentKey,
                                                                                    key,
                                                                                    field,
                                                                                    'label'
                                                                                )
                                                                            }
                                                                            className="label-input"
                                                                            error={this.state.errorsLabel[count]}
                                                                        />
                                                                    )}
                                                                <Input
                                                                    field={{
                                                                        value: json[key][field].value,
                                                                        label: `${
                                                                            json[key][field].is_label_editable ===
                                                                                'true'
                                                                                ? 'Value'
                                                                                : json[key][field].label
                                                                            }`,
                                                                        type: InputFieldType.TEXTAREA,
                                                                        isRequired:
                                                                            json[key][field].is_required &&
                                                                            JSON.parse(json[key][field].is_required),
                                                                    }}
                                                                    width={12}
                                                                    name={field}
                                                                    onChange={e =>
                                                                        this.handleChangeSubSection(
                                                                            e,
                                                                            parentKey,
                                                                            key,
                                                                            field,
                                                                            'value'
                                                                        )
                                                                    }
                                                                    error={
                                                                        json[key][field].error &&
                                                                        json[key][field].error
                                                                    }
                                                                />
                                                            </div>
                                                        )}
                                                        {json[key][field].type === 'RADIOBOX' && (
                                                            <div
                                                                key={field}
                                                                className="options input-label-box"
                                                            >
                                                                {json[key][field].is_label_editable ===
                                                                    'true' && (
                                                                        <Input
                                                                            field={{
                                                                                value: json[key][field].label,
                                                                                label: json[key][field].section_label,
                                                                                type: InputFieldType.TEXT,
                                                                                isRequired: true,
                                                                            }}
                                                                            width={12}
                                                                            name={field}
                                                                            onChange={e =>
                                                                                this.handleChangeSubSection(
                                                                                    e,
                                                                                    parentKey,
                                                                                    key,
                                                                                    field,
                                                                                    'label'
                                                                                )
                                                                            }
                                                                            className="label-input"
                                                                            error={
                                                                                json[key][field].error &&
                                                                                json[key][field].error
                                                                            }
                                                                        />
                                                                    )}

                                                                <div className="options-content">
                                                                    <Input
                                                                        field={{
                                                                            value: json[key][field].value,
                                                                            label:
                                                                                json[key][field].is_label_editable ===
                                                                                    'false'
                                                                                    ? json[key][field].label
                                                                                    : '',
                                                                            type: InputFieldType.RADIO,
                                                                            isRequired: false,
                                                                            options: json[key][field].options.map(
                                                                                role => ({
                                                                                    value: role,
                                                                                    label: role,
                                                                                })
                                                                            ),
                                                                        }}
                                                                        width={12}
                                                                        name={field + count}
                                                                        onChange={e =>
                                                                            this.handleChangeSubSection(
                                                                                e,
                                                                                parentKey,
                                                                                key,
                                                                                field,
                                                                                'value'
                                                                            )
                                                                        }
                                                                        error={
                                                                            json[key][field].error &&
                                                                            json[key][field].error
                                                                        }
                                                                    />
                                                                </div>
                                                            </div>
                                                        )}
                                                    </div>
                                                );
                                            })}
                                </div>
                            );
                        })}
            </div>
        );
    };

    renderStaticFields = () => {
        if (!this.state.template.json_config.service_cost) {
            return null;
        }

        const formatter = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
        });

        return (
            <div className="template-fields col-md-12 row">
                <div className="section-heading">
                    {'SERVICE COST '}
                    <div className="action-collapse">{''}</div>
                </div>
                <div className="service-cost-body">
                    <div className="estimated-hours-rates">
                        <div className="header">
                            <div className="sub-heading col-md-6">
                                <div className="text">ESTIMATED HOURS</div>
                            </div>

                            <div className="sub-heading col-md-6">
                                <div className="text">RATES</div>
                            </div>
                        </div>
                        <div className="estimated-hours">
                            <Input
                                field={{
                                    value: this.state.template.json_config.service_cost
                                        .engineering_hours,
                                    label: 'Engineering Hours (Estimate)',
                                    type: InputFieldType.NUMBER,
                                    isRequired: true,
                                }}
                                width={6}
                                name={'engineering_hours'}
                                placeholder="Enter Hours"
                                onChange={e => this.handleChangeStatic(e)}
                                className="select-type"
                                disabled={true}
                            />
                            <Input
                                field={{
                                    value: this.state.template.json_config.service_cost
                                        .engineering_hourly_rate,
                                    label: 'Engineering Hourly Rate(In $)',
                                    type: InputFieldType.NUMBER,
                                    isRequired: true,
                                }}
                                width={6}
                                name={'engineering_hourly_rate'}
                                placeholder="Enter Rate"
                                onChange={e => this.handleChangeStatic(e)}
                                className="select-type"
                                disabled={true}
                            />
                            <Input
                                field={{
                                    value: this.state.template.json_config.service_cost
                                        .after_hours,
                                    label: 'After Hours  (Sat, Sun, and M-F after 6pm)',
                                    type: InputFieldType.NUMBER,
                                    isRequired: false,
                                }}
                                width={6}
                                name={'after_hours'}
                                placeholder="Enter Hours"
                                onChange={e => this.handleChangeStatic(e)}
                                className="select-type"
                                disabled={true}
                            />
                            <Input
                                field={{
                                    value: this.state.template.json_config.service_cost
                                        .after_hours_rate,
                                    label:
                                        'After Hours Rate (Sat, Sun, and M-F after 6pm)(In $)',
                                    type: InputFieldType.NUMBER,
                                    isRequired: true,
                                }}
                                width={6}
                                name={'after_hours_rate'}
                                placeholder="Enter Rate"
                                onChange={e => this.handleChangeStatic(e)}
                                disabled={true}
                                className="select-type"
                            />
                        </div>
                        <div className="project-managment ">
                            <div className="project-mng-box col-md-6">
                                <Input
                                    field={{
                                        value: this.state.template.json_config.service_cost
                                            .project_management_hours,
                                        label: 'Project Managment Hours (Estimate)',
                                        type: InputFieldType.NUMBER,
                                        isRequired: true,
                                    }}
                                    width={12}
                                    name={'project_management_hours'}
                                    placeholder="Enter Hours"
                                    onChange={e => this.handleChangeStatic(e)}
                                    className="select-type"
                                    disabled={true}

                                />
                                <span className="info-recomended">
                                    Recommended Hours:{' '}
                                    {getRecommendedHours(
                                        this.state.template.json_config.service_cost
                                            .engineering_hours
                                    )}
                                </span>
                            </div>

                            <Input
                                field={{
                                    value: this.state.template.json_config.service_cost
                                        .project_management_hourly_rate,
                                    label: 'Project Managment Hourly Rate(In $)',
                                    type: InputFieldType.NUMBER,
                                    isRequired: true,
                                }}
                                width={6}
                                name={'project_management_hourly_rate'}
                                placeholder="Enter Rate"
                                onChange={e => this.handleChangeStatic(e)}
                                className="select-type"
                                disabled={true}
                            />
                        </div>
                    </div>
                    <div className="contractors">
                        <div className="sub-heading col-md-12">
                            <div className="text">CONTRACTORS</div>
                        </div>
                        {this.state.template.json_config.service_cost &&
                            this.state.template.json_config.service_cost.contractors &&
                            this.state.template.json_config.service_cost.contractors.map(
                                (contractor, index) => {
                                    return (
                                        <div className="contractor-row col-md-12" key={index}>
                                            <Input
                                                field={{
                                                    value: contractor.name,
                                                    label: 'Contractor Name',
                                                    type: InputFieldType.TEXT,
                                                    isRequired: false,
                                                }}
                                                width={3}
                                                name={'name'}
                                                placeholder="Enter name"
                                                onChange={e =>
                                                    this.handleChangeContractors(e, index, 'contractors')
                                                }
                                                className="select-type"
                                                disabled={true}
                                                error={contractor.errorName}
                                            />
                                            <Input
                                                field={{
                                                    value: contractor.partner_cost,
                                                    label: 'Partner Cost(In $)',
                                                    type: InputFieldType.NUMBER,
                                                    isRequired: false,
                                                }}
                                                width={3}
                                                name={'partner_cost'}
                                                placeholder="Enter Rate"
                                                onChange={e =>
                                                    this.handleChangeContractors(e, index, 'contractors')
                                                }
                                                className="select-type"
                                                error={contractor.errorRate}
                                                disabled={true}
                                            />
                                            <Input
                                                field={{
                                                    value: contractor.margin_percentage,
                                                    label: 'Margin %',
                                                    type: InputFieldType.NUMBER,
                                                    isRequired: false,
                                                }}
                                                width={2}
                                                name={'margin_percentage'}
                                                placeholder="Enter %"
                                                onChange={e =>
                                                    this.handleChangeContractors(e, index, 'contractors')
                                                }
                                                className="select-type"
                                                disabled={true}

                                                error={contractor.errorMargin}
                                            />
                                            <Input
                                                field={{
                                                    label: 'Customer Cost(In $)',
                                                    value: formatter.format(getCustomerCost(contractor)),
                                                    type: InputFieldType.TEXT,
                                                    isRequired: false,
                                                }}
                                                width={2}
                                                name={'margin_percentage'}
                                                placeholder=" "
                                                disabled={true}
                                                className="select-type"
                                                onChange={e => null}
                                            />
                                        </div>
                                    );
                                }
                            )}
                    </div>
                    <div className="travelscontractors">
                        <div className="sub-heading col-md-12">
                            <div className="text">TRAVELS</div>
                        </div>
                        {this.state.template.json_config.service_cost &&
                            this.state.template.json_config.service_cost.travels &&
                            this.state.template.json_config.service_cost.travels.map(
                                (travel, index) => {
                                    return (
                                        <div className="contractor-row col-md-12" key={index}>
                                            <Input
                                                field={{
                                                    value: travel.description,
                                                    label: 'Description',
                                                    type: InputFieldType.TEXT,
                                                    isRequired: false,
                                                }}
                                                width={4}
                                                name={'description'}
                                                placeholder="Enter description"
                                                onChange={e =>
                                                    this.handleChangeTravels(e, index, 'travels')
                                                }
                                                className="select-type"
                                                disabled={true}
                                                error={travel.errorDescription}
                                            />
                                            <Input
                                                field={{
                                                    value: travel.cost,
                                                    label: 'Cost (in $)',
                                                    type: InputFieldType.NUMBER,
                                                    isRequired: false,
                                                }}
                                                width={3}
                                                name={'cost'}
                                                placeholder="Enter Rate"
                                                onChange={e =>
                                                    this.handleChangeTravels(e, index, 'travels')
                                                }
                                                disabled={true}

                                                className="select-type"
                                                error={travel.errorCost}
                                            />
                                        </div>
                                    );
                                }
                            )}
                    </div>
                    <div className="notes col-md-12">
                        <div className="sub-heading-notes col-md-12">
                            <div className="text">NOTES</div>
                        </div>
                        <div className="col-md-12">
                            <SimpleMDE
                                onChange={e => null}
                                options={{
                                    placeholder: '',
                                    autofocus: false,
                                    spellChecker: true,
                                    toolbar: false
                                }}
                                value={this.state.template.json_config.service_cost.notesMD}
                                className={`${'markdown-editor-input'}`}
                                getMdeInstance={this.getIntance}
                            />
                        </div>
                    </div>
                    <div className="calculations">
                        <div className="header">
                            <div className="sub-heading col-md-4">
                                <div className="text">SERVICE COST (FIXED FEE)</div>
                                <Input
                                    field={{
                                        value: formatter.format(
                                            Number(
                                                getCustomerCostFixedFee(
                                                    this.state.template.json_config,
                                                    this.props.docSetting
                                                )
                                            )
                                        ),
                                        label: 'Customer Cost(In $)',
                                        type: InputFieldType.TEXT,
                                        isRequired: true,
                                    }}
                                    width={12}
                                    disabled={true}
                                    name={'engineering_hours'}
                                    placeholder=" "
                                    onChange={e => null}
                                    className="select-type"
                                />
                                <Input
                                    field={{
                                        value: formatter.format(
                                            Number(
                                                getFixedFeeInternalCost(
                                                    this.state.template.json_config,
                                                    this.props.docSetting
                                                )
                                            )
                                        ),
                                        label: 'Internal Cost(In $)',
                                        type: InputFieldType.TEXT,
                                        isRequired: true,
                                    }}
                                    width={12}
                                    disabled={true}
                                    name={'engineering_hours'}
                                    placeholder=" "
                                    onChange={e => null}
                                    className="select-type"
                                />
                            </div>

                            <div className="sub-heading col-md-4">
                                <div className="text">SERVICE COST (T&M)</div>
                                <Input
                                    field={{
                                        value: getCustomerCostTM(this.state.template.json_config),
                                        label: 'Customer Cost(In $)',
                                        type: InputFieldType.NUMBER,
                                        isRequired: true,
                                    }}
                                    width={12}
                                    disabled={true}
                                    name={'engineering_hours'}
                                    placeholder=" "
                                    onChange={e => null}
                                    className="select-type"
                                />
                                <Input
                                    field={{
                                        value: getInternalCostTM(
                                            this.state.template.json_config,
                                            this.props.docSetting
                                        ),
                                        label: 'Internal Cost(In $)',
                                        type: InputFieldType.NUMBER,
                                        isRequired: true,
                                    }}
                                    width={12}
                                    disabled={true}
                                    name={'engineering_hours'}
                                    placeholder=" "
                                    onChange={e => null}
                                    className="select-type"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    handleChangeStatic = e => {
        const newState = cloneDeep(this.state);
        newState.template.json_config.service_cost[e.target.name] = this.getValues(
            e
        );
        this.setState(newState);
    };

    getValues = e => {
        const value =
            e.target.type === 'number'
                ? parseFloat(e.target.value) > 0
                    ? parseFloat(e.target.value)
                    : 0
                : e.target.value;

        return value;
    };

    handleChangeContractors = (e, index, list) => {
        const newState = cloneDeep(this.state);
        newState.template.json_config.service_cost[list][index][
            e.target.name
        ] = this.getValues(e);
        newState.template.json_config.service_cost[list][
            index
        ].customer_cost = getCustomerCost(
            newState.template.json_config.service_cost[list][index]
        );
        this.setState(newState);
    };

    handleChangeTravels = (e, index, list) => {
        const newState = cloneDeep(this.state);
        newState.template.json_config.service_cost[list][index][
            e.target.name
        ] = this.getValues(e);
        this.setState(newState);
    };

    onChecboxChangedHidden = (event: any) => {
        const showHidden = event.target.checked;
        this.setState({
            showHidden,
        });
    };
    getBody = () => {
        return (
            <div className="add-template">


                <div className="loader">
                    <Spinner
                        show={
                            this.state.loading ||
                            this.props.isFetching ||
                            this.props.isFetchingTemplate ||
                            this.props.isFetchingCategory
                        }
                    />
                </div>
                <div className="basic-field">
                    <Input
                        field={{
                            value: this.state.template.name,
                            label: 'Name',
                            type: InputFieldType.TEXT,
                            isRequired: true,
                        }}
                        width={6}
                        name={'name'}
                        placeholder="Enter Template name"
                        onChange={e => this.handleChangeName(e)}
                        className="select-type"
                        disabled={true}
                    />
                    {this.state.template.id !== 0 && (
                        <div className="doc-details col-lg-6 col-md-6">
                            <div className="template-add-edit-header">
                                <Checkbox
                                    isChecked={this.state.showHidden}
                                    name="option"
                                    onChange={e => this.onChecboxChangedHidden(e)}
                                    className="show-hidden-section-checkbox"
                                >
                                    Show hidden Sections
          </Checkbox>
                            </div>
                            <div>
                                {' '}
                Created by <span>{this.state.template.author_name}</span>.
              </div>
                            <div>
                                {' '}
                Last updated by{' '}
                                <span>{this.state.template.updated_by_name}</span> on{' '}
                                <span>
                                    {moment
                                        .utc(this.state.template.updated_on)
                                        .local()
                                        .format('MM/DD/YYYY hh:mm A')}
                                </span>
                            </div>
                            {
                                this.state.template.id && <div>
                                    {' '}
                Version - {' '}
                                    <span>
                                        {this.state.template.major_version || '1'}.
                {this.state.template.minor_version}

                                    </span>
                                </div>
                            }
                        </div>
                    )}
                    <div
                        className="select-type select-sow
          field-section col-lg-6 col-md-6"
                    >
                        <div className="field__label row">
                            <label className="field__label-label" title="">
                                Category
              </label>
                            <span className="field__label-required" />
                        </div>
                        <div
                            className={``}
                        >
                            <SelectInput
                                name="category"
                                value={this.state.template.category}
                                onChange={e => this.handleChangeCategory(e)}
                                options={this.getCategoryOptions()}
                                multi={false}
                                searchable={true}
                                placeholder="Select Category"
                                disabled={true}
                            />
                        </div>
                    </div>
                    <div
                        className="select-type select-
                field-section  field-section--required  col-lg-6 col-md-6"
                    >
                        <div className="field__label row">
                            <label className="field__label-label" title="">
                                Service Category
              </label>
                            <span className="field__label-required" />
                        </div>
                        <div
                            className={''}
                        >
                            <SelectInput
                                name="service_catalog_category"
                                value={this.state.template.service_catalog_category}
                                onChange={e => this.handleChangeName(e)}
                                options={this.getCategoryList()}
                                multi={false}
                                searchable={true}
                                placeholder="Select Service Category"
                                disabled={true}
                            />
                        </div>
                    </div>
                    <div
                        className="select-type select-sow
          field-section col-lg-6 col-md-6"
                    >
                        <div className="field__label row">
                            <label className="field__label-label" title="">
                                Service Catalog
              </label>
                        </div>
                        <div
                            className={``}
                        >
                            <SelectInput
                                name="linked_service_catalog"
                                value={
                                    this.state.template.linked_service_catalog
                                        ? this.state.template.linked_service_catalog
                                        : 'null'
                                }
                                onChange={e => this.handleChangeName(e)}
                                options={this.getServiceCatalogsOptions()}
                                multi={false}
                                searchable={true}
                                placeholder="Select Service Catalog"
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
                {this.state.template.json_config &&
                    this.renderTemplateByJson(this.state.template.json_config)}
                {this.state.template.json_config &&
                    this.props.docSetting &&
                    this.renderStaticFields()}

            </div>)
    }

    getFooter = () => {
        return (
            <div
                className={`footer`}
            >
                <SquareButton
                    onClick={this.props.onClose}
                    content="Close"
                    bsStyle={ButtonStyle.PRIMARY}
                />
            </div>
        );
    };
    render() {
        return (
            <ModalBase
                show={this.props.show}
                onClose={this.props.onClose}
                titleElement={'View Template'}
                bodyElement={this.getBody()}
                footerElement={this.getFooter()}
                className="version-history"
            />);
    }
}

const mapStateToProps = (state: IReduxStore) => ({
    loggenInUser: state.profile.user,
    template: state.sow.template,
    baseTemplates: state.sow.baseTemplates,
    categoryList: state.sow.categoryList,
    isFetchingTemplate: state.sow.isFetchingCategory,
    isFetchingCategory: state.sow.isFetchingTemplate,
    isFetching: state.sow.isFetching,
    serviceCatalogCategories: state.sow.serviceCatalogCategories,
    serviceCatalogShortList: state.sow.serviceCatalogShortList,
    docSetting: state.setting.docSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
    getBaseTemplate: () => dispatch(getBaseTemplate()),
    getCategoryList: () => dispatch(getCategoryList()),
    getTemplateList: () => dispatch(getTemplateList()),
    getTemplate: (id: any) => dispatch(getTemplate(id)),
    saveTemplate: (template: any) => dispatch(saveTemplate(template)),
    getShortServiceCatalogList: () => dispatch(getShortServiceCatalogList()),
    updateTemplate: (template: any) => dispatch(updateTemplate(template)),
    uploadImage: (file: any, name: string) => dispatch(uploadImage(file, name)),
    addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
    addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
    fetchServiceCategoriesFull: () => dispatch(fetchServiceCategoriesFull()),
    fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewTemplate);
