import { cloneDeep } from 'lodash';
import _ from 'lodash';
import React, { Component } from "react";
import { Mention, MentionsInput } from 'react-mentions';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import 'quill-mention';
import { connect } from "react-redux";
import "./style.scss";
import SquareButton from '../../components/Button/button';
import {
  getMeetingTemplateSetting,
  createMeetingTemplateSetting,
} from "../../actions/pmo";
import { commonFunctions } from '../../utils/commonFunctions';

interface IMeetingState {
  meetingMailSubject: string;
  meetingMailBodyText: string;
  meetingMailBodyMarkDown: string;
  showActionButtons: boolean;
  savedSuccess: boolean;
}

interface IIProjectSettingProps extends ICommonProps {
  getmeetingTemplateSetting: any;
  createmeetingTemplateSetting: any;
  type: string;
}


class MeetingMailGenericTemplate extends Component<
  IIProjectSettingProps,
  IMeetingState
> {
  type: string;
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  customerMailBodyRef: any;
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    meetingMailSubject: "",
    meetingMailBodyMarkDown: "",
    meetingMailBodyText: "",
    showActionButtons: false,
    savedSuccess: false
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];
      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);

    },
  };

  componentDidMount() {
    this.getmeetingTemplateSetting();
  }

  getmeetingTemplateSetting = async () => {
    const meetingTemplateSetting: any = await this.props.getmeetingTemplateSetting();
    const { email_subject, message_html, message_markdown } = meetingTemplateSetting.response;

    if (email_subject !== "" && message_html != "") {
      this.setState({ savedSuccess: true });
    }

    this.setState({
      meetingMailSubject: email_subject,
      meetingMailBodyText: message_html,
      meetingMailBodyMarkDown: message_markdown,
    });
  }

  handleChangeMailSubject = e => {
    const newState = cloneDeep(this.state);
    (newState.meetingMailSubject as any) = e.target.value;
    this.setState({ ...newState, showActionButtons: true });
  };

  createMeetingTemplateSetting = () => {
    const {
      savedSuccess,
      meetingMailSubject,
      meetingMailBodyMarkDown,
      meetingMailBodyText
    } = this.state;

    const regex = /#RULE#/gi;
    const mailSubject = meetingMailSubject.replace(regex, '');
    const mailBodyTxtArr = meetingMailBodyText.split(" ")

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith('{')) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    })

    if (savedSuccess) {
      this.props.createmeetingTemplateSetting({
        email_subject: mailSubject,
        message_html: mailBodyTxtFinalArr.join(" "),
        message_markdown: meetingMailBodyMarkDown
      })
        .then(action => {
          this.setState({ showActionButtons: false })
        });
    }
  }

   

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChange = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.meetingMailBodyText as any) = this.getContentFromObject(object);
    (newState.meetingMailBodyMarkDown as any) = content;
    (newState.savedSuccess as boolean) = false;
    this.setState({ ...newState, showActionButtons: true });
  };

  render() {
    return (
      <div className="customer-contact-role col-md-12">
        <div className="col-md-12 row customer-email">
          <div>
            <label className="col-md-3 field__label-label email-subject" >
              <span><b>Email Subject</b> (The subject that all be populated by default in the Customer Touch Meeting)
              </span>
            </label>
          </div>
          <div className="mail-subject-mention-input">
            <MentionsInput
              markup="[__display__]"
              value={this.state.meetingMailSubject || ''}
              onChange={(e) => this.handleChangeMailSubject(e)}
              className={'outer'}
            >
              <Mention
                trigger="#"
                data={commonFunctions.rangeVariables()}
                className={'inner-drop'}
                markup="#RULE#__display__"
              />
            </MentionsInput>
          </div>
        </div>
        <div className="col-md-12 row customer-email">
          <label className="col-md-3 field__label-label email-subject" >
            <span><b>Email Message</b> (The default message that will be shown in the email sent for customer touch meeting)
            </span>
          </label>
          <div className="cust-touch-md-editor">
            <ReactQuill
              ref={this.customerMailBodyRef}
              value={this.state.meetingMailBodyMarkDown || ''}
              onChange={this.handleChange}
              modules={{ mention: this.mentionModule }}
            />
          </div>
          {
            this.state.showActionButtons &&
            this.state.meetingMailBodyMarkDown !== "" &&
            this.state.meetingMailSubject !== "" && (
              <SquareButton
                onClick={e => this.createMeetingTemplateSetting()}
                content="Save"
                bsStyle={ButtonStyle.PRIMARY}
                className="save-mail-btn"
              />
            )
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  state: IReduxStore,
) => ({
});

const mapDispatchToProps = (dispatch: any) => ({
  getMeetingTemplateSetting: () => dispatch(getMeetingTemplateSetting()),
  createMeetingTemplateSetting: (data) => dispatch(createMeetingTemplateSetting(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MeetingMailGenericTemplate);
