import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { getQuoteStageList, getQuoteStatusesList } from "../../actions/sow";
import Input from "../../components/Input/input";
import {
  cancelledCRSettingCRU,
  CANCELLED_CR_SETTING_SUCCESS,
  CANCELLED_CR_SETTING_FAILURE,
} from "../../actions/pmo";
import { commonFunctions } from "../../utils/commonFunctions";

interface CancelledCRProps {
  isFetchingQStages: boolean;
  qStageList: IDLabelObject[];
  isFetchingQStatuses: boolean;
  qStatusList: IDLabelObject[];
  getQuoteStageList: () => Promise<any>;
  getQuoteStatusesList: () => Promise<any>;
  cancelledCRSettingCRU: (
    method: HTTPMethods,
    data?: ICancelledCRSetting
  ) => Promise<any>;
}

const CancelledCR: React.FC<CancelledCRProps> = (props) => {
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [settings, setSettings] = useState<ICancelledCRSetting>({
    opp_stage_crm_id: null,
    opp_status_crm_id: null,
  });
  const [error, setError] = useState<IErrorValidation>({
    opp_stage_crm_id: commonFunctions.getErrorState(),
    opp_status_crm_id: commonFunctions.getErrorState(),
  });

  const quoteStatuses = useMemo(
    () =>
      props.qStatusList
        ? props.qStatusList.map((el) => ({
            value: el.id,
            label: el.label,
          }))
        : [],
    [props.qStatusList]
  );

  const quoteStages = useMemo(
    () =>
      props.qStageList
        ? props.qStageList.map((el) => ({
            value: el.id,
            label: el.label,
          }))
        : [],
    [props.qStageList]
  );

  useEffect(() => {
    fetchCancelledCRSetting();
    if (!props.qStageList) props.getQuoteStageList();
    if (!props.qStatusList) props.getQuoteStatusesList();
  }, []);

  const fetchCancelledCRSetting = () => {
    props.cancelledCRSettingCRU(HTTPMethods.GET).then((action) => {
      if (action.type === CANCELLED_CR_SETTING_SUCCESS) {
        setSettings(action.response);
      } else if (action.type === CANCELLED_CR_SETTING_FAILURE) {
        setFirstSave(true);
      }
    });
  };

  const handleChangeSetting = (e: React.ChangeEvent<HTMLInputElement>) => {
    const updatedSettings: ICancelledCRSetting = {
      ...settings,
      [e.target.name]: e.target.value,
    };
    setSettings(updatedSettings);
    saveSettings(updatedSettings);
  };

  const validate = (settings: ICancelledCRSetting): boolean => {
    let isValid = true;
    const errors: IErrorValidation = {};
    const keys = ["opp_status_crm_id", "opp_stage_crm_id"];
    keys.forEach((key) => {
      if (!settings[key]) {
        isValid = false;
        errors[key] = commonFunctions.getErrorState("This field is required");
      } else errors[key] = commonFunctions.getErrorState();
    });
    setError(errors);
    return isValid;
  };

  const saveSettings = (settings: ICancelledCRSetting) => {
    if (validate(settings)) {
      props
        .cancelledCRSettingCRU(
          firstSave ? HTTPMethods.POST : HTTPMethods.PUT,
          settings
        )
        .then((action) => {
          if (action.type === CANCELLED_CR_SETTING_SUCCESS) {
            setFirstSave(false);
          }
        });
    }
  };

  return (
    <div className="cancelled-cr-setting-container">
      <div className="col-md-12">
        <Input
          field={{
            label: "Cancelled Opportunity Status",
            type: InputFieldType.PICKLIST,
            value: settings.opp_status_crm_id,
            options: quoteStatuses,
            isRequired: true,
          }}
          className="cr-select"
          width={6}
          multi={false}
          placeholder={`Select Opportunity Status`}
          name="opp_status_crm_id"
          onChange={handleChangeSetting}
          labelIcon="info"
          labelTitle={
            "Opportunities linked to the Cancelled Change Request will be updated with this Status"
          }
          loading={props.isFetchingQStatuses}
          error={error.opp_status_crm_id}
        />
        <Input
          field={{
            label: "Cancelled Opportunity Stage",
            type: InputFieldType.PICKLIST,
            value: settings.opp_stage_crm_id,
            options: quoteStages,
            isRequired: true,
          }}
          className="cr-select"
          width={6}
          multi={false}
          placeholder={`Select Opportunity Stage`}
          name="opp_stage_crm_id"
          onChange={handleChangeSetting}
          labelIcon="info"
          labelTitle={
            "Opportunities linked to the Cancelled Change Request will be updated with this Stage"
          }
          loading={props.isFetchingQStages}
          error={error.opp_stage_crm_id}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  qStageList: state.sow.qStageList,
  qStatusList: state.sow.qStatusList,
  isFetchingQStages: state.sow.isFetchingQStageList,
  isFetchingQStatuses: state.sow.isFetchingQStatusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStageList: () => dispatch(getQuoteStageList()),
  getQuoteStatusesList: () => dispatch(getQuoteStatusesList()),
  cancelledCRSettingCRU: (method: HTTPMethods, data?: ICancelledCRSetting) =>
    dispatch(cancelledCRSettingCRU(method, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CancelledCR);
