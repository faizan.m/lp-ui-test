import { cloneDeep } from 'lodash';
import _ from 'lodash';
import React, { Component } from "react";
import showdown from 'showdown';
import { Mention, MentionsInput } from 'react-mentions';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import 'quill-mention';
import { connect } from "react-redux";
import "./style.scss";
import SquareButton from '../../components/Button/button';
import {
  getCustomerTouchMeetingTemplate,
  createCustomerTouchMeetingTemplate,
  udpateCustomerTouchMeetingTemplate,
} from "../../actions/pmo";
import { commonFunctions } from '../../utils/commonFunctions';

interface ICustomerTouchMeetingState {
  customerTouchMeetingMailSubject: string;
  customerTouchMeetingMailBodyText: string;
  customerTouchMeetingMailBodyMarkDown: string;
  showActionButtons: boolean;
  savedSuccess: boolean;
}

interface IIProjectSettingProps extends ICommonProps {
  getCustomerTouchMeetingTemplate: any;
  createCustomerTouchMeetingTemplate: any;
  udpateCustomerTouchMeetingTemplate: any;
}

interface IPassedProps {
  meeting?: string;
  setData?: any;
  viewOnly?: boolean;
  initialData?: any
  mailTemplateData?: any;
  setSettingData?: any;
}

class CustomerTouchMeeting extends Component<
  IPassedProps & IIProjectSettingProps,
  ICustomerTouchMeetingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  customerMailBodyRef: any;
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    customerTouchMeetingMailSubject: "",
    customerTouchMeetingMailBodyMarkDown: "",
    customerTouchMeetingMailBodyText: "",
    showActionButtons: false,
    savedSuccess: false,
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);
    },
  };

  componentDidMount() {
    if (!this.props.meeting || this.props.meeting === "0") {
      this.setState({
        customerTouchMeetingMailSubject: this.props.mailTemplateData
          .email_subject,
        customerTouchMeetingMailBodyMarkDown: this.props.mailTemplateData
          .email_body_markdown,
        customerTouchMeetingMailBodyText: this.props.mailTemplateData
          .email_body_text,
        savedSuccess: this.props.mailTemplateData.email_body_text !== "",
      });
    } else {
      this.setState({
        customerTouchMeetingMailSubject: this.props.initialData.email_subject,
        customerTouchMeetingMailBodyMarkDown: this.props.initialData.email_body,
      });
    }
  }

  componentDidUpdate(prevProps: IPassedProps) {
    if (this.props.meeting && this.props.meeting !== prevProps.meeting) {
      if (
        this.state.customerTouchMeetingMailBodyMarkDown === "" &&
        this.props.initialData !== prevProps.initialData
      ) {
        let parsedMarkdown = "";

        if (this.props.initialData.email_body_replaced_text) {
          parsedMarkdown = this.convertTextToHTML(
            this.props.initialData.email_body_replaced_text
          );
        } else {
          parsedMarkdown = this.props.initialData.email_body;
        }

        this.setState({
          customerTouchMeetingMailSubject: this.props.initialData.email_subject,
          customerTouchMeetingMailBodyMarkDown: parsedMarkdown,
        });
      }
    } else {
      if (
        this.props.mailTemplateData &&
        this.props.mailTemplateData !== prevProps.mailTemplateData
      ) {
        this.setState({
          customerTouchMeetingMailSubject: this.props.mailTemplateData
            .email_subject,
          customerTouchMeetingMailBodyMarkDown: this.props.mailTemplateData
            .email_body_markdown,
          customerTouchMeetingMailBodyText: this.props.mailTemplateData
            .email_body_text,
          savedSuccess: this.props.mailTemplateData.email_body_text !== "",
        });
      }
    }
  }

  convertTextToHTML = (replacedText: string) => {
    let formattedReplaceText = replacedText.replace(/(\r\n|\r|\n)/g, "<br>");
    formattedReplaceText = formattedReplaceText.replace(
      /<br\/>([^<])/g,
      "<br/>\n\n$1"
    );

    const converter = new showdown.Converter();
    const parsedMarkdown = converter.makeHtml(formattedReplaceText);

    return parsedMarkdown;
  };

  getCustomerTouchMeetingTemplate = async () => {
    const meetingTemplate: any = await this.props.getCustomerTouchMeetingTemplate();
    const {
      email_subject,
      message_html,
      message_markdown,
    } = meetingTemplate.response;

    if (email_subject !== "" && message_html != "") {
      this.setState({ savedSuccess: true });
    }

    this.setState({
      customerTouchMeetingMailSubject: email_subject,
      customerTouchMeetingMailBodyText: message_html,
      customerTouchMeetingMailBodyMarkDown: message_markdown,
    });
  };

  handleChangeMailSubject = (e) => {
    const newState = cloneDeep(this.state);
    (newState.customerTouchMeetingMailSubject as any) = e.target.value;
    this.setState({ ...newState, showActionButtons: true });
    this.props.setData && this.props.setData(newState);
  };

  createMeetingTemplate = () => {
    const {
      savedSuccess,
      customerTouchMeetingMailSubject,
      customerTouchMeetingMailBodyMarkDown,
      customerTouchMeetingMailBodyText,
    } = this.state;

    const regex = /#RULE#/gi;
    const mailSubject = customerTouchMeetingMailSubject.replace(regex, "");
    const mailBodyTxtArr = customerTouchMeetingMailBodyText.split(" ");

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith("{")) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    });

    this.props.setSettingData({
      meeting_type: "Customer Touch",
      email_subject: mailSubject,
      email_body_text: mailBodyTxtFinalArr.join(" "),
      email_body_markdown: customerTouchMeetingMailBodyMarkDown,
      isCreated: savedSuccess,
    });
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChange = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.customerTouchMeetingMailBodyText as any) = this.getContentFromObject(
      object
    );
    (newState.customerTouchMeetingMailBodyMarkDown as any) = content;
    this.setState({ ...newState, showActionButtons: true });

    this.props.setData && this.props.setData(newState);
  };

  render() {
    return (
      <div className="customer-contact-role col-md-12">
        <div className="col-md-12 row customer-email">
          <div>
            <label className="col-md-3 field__label-label email-subject">
              <span>
                <b>Email Subject</b> (Populated by Customer Touch Meeting
                template in Project Settings)
              </span>
            </label>
          </div>
          <div className="mail-subject-mention-input">
            <MentionsInput
              markup="[__display__]"
              value={this.state.customerTouchMeetingMailSubject || ""}
              onChange={(e) => this.handleChangeMailSubject(e)}
              className={"outer"}
              readOnly={this.props.viewOnly}
            >
              <Mention
                trigger="#"
                data={commonFunctions.rangeVariables()}
                className={"inner-drop"}
                markup="#RULE#__display__"
              />
            </MentionsInput>
          </div>
        </div>
        <div className="col-md-12 row customer-email">
          <label className="col-md-3 field__label-label email-subject">
            <span>
              <b>Email Message</b> (The default message that will be shown in
              the email sent for customer touch meeting)
            </span>
          </label>
          <div className="cust-touch-md-editor">
            <ReactQuill
              ref={this.customerMailBodyRef}
              value={this.state.customerTouchMeetingMailBodyMarkDown || ""}
              onChange={this.handleChange}
              modules={{ mention: this.mentionModule }}
              readOnly={this.props.viewOnly}
            />
          </div>
          {!this.props.meeting &&
            this.state.showActionButtons &&
            this.state.customerTouchMeetingMailBodyMarkDown !== "" &&
            this.state.customerTouchMeetingMailSubject !== "" && (
              <SquareButton
                onClick={(e) => this.createMeetingTemplate()}
                content="Save"
                bsStyle={ButtonStyle.PRIMARY}
                className="save-mail-btn"
              />
            )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  state: IReduxStore,
  ownProps: IPassedProps,
) => ({
  meeting: ownProps.meeting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getCustomerTouchMeetingTemplate: () => dispatch(getCustomerTouchMeetingTemplate()),
  createCustomerTouchMeetingTemplate: (data) => dispatch(createCustomerTouchMeetingTemplate(data)),
  udpateCustomerTouchMeetingTemplate: (data) => dispatch(udpateCustomerTouchMeetingTemplate(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerTouchMeeting);
