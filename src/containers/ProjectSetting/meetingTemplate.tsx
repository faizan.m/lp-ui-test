import React, { Component } from "react";
import { connect } from "react-redux";
import cloneDeep from "lodash/cloneDeep";
import {
  getStatusMeetings,
  getKickoffMeetings,
  createKickoffMeeting,
  deleteKickoffMeeting,
  NEW_KICK_OFF_MEET_FAILURE,
  NEW_KICK_OFF_MEET_SUCCESS,
} from "../../actions/pmo";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Input from "../../components/Input/input";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import { commonFunctions } from "../../utils/commonFunctions";

interface IMeetingTemplate {
  id?: number;
  template_name: string;
  action_items?: string[];
  agenda_topics: any[];
  edited?: boolean;
  open?: boolean;
  titleError?: string;
  hasError?: boolean;
  meeting_type?: string;
}

interface IMeetingTemplateState {
  meetingsList: IMeetingTemplate[];
}

interface IMeetingTemplateProps {
  meetingType: "Kickoff" | "Status";
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getStatusMeetings: () => Promise<any>;
  getKickoffMeetings: () => Promise<any>;
  createMeeting: (data: IMeetingTemplate) => Promise<any>;
  deleteMeeting: (data: IMeetingTemplate) => Promise<any>;
}

class MeetingTemplate extends Component<
  IMeetingTemplateProps,
  IMeetingTemplateState
> {
  constructor(props: IMeetingTemplateProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    meetingsList: [],
  });

  componentDidMount() {
    this.getMeetings();
  }

  getMeetings = async () => {
    const mapping = await (this.props.meetingType === "Kickoff"
      ? this.props.getKickoffMeetings()
      : this.props.getStatusMeetings());
    this.setState({ meetingsList: mapping.response });
  };

  handleChangeMeeting = (
    event: React.ChangeEvent<HTMLInputElement>,
    meetingIndex: number
  ) => {
    const newState = cloneDeep(this.state);
    const value = event.target.value;
    newState.meetingsList[meetingIndex].template_name = value;
    newState.meetingsList[meetingIndex].edited = true;
    this.setState(newState);
  };

  handleChangeMeetingTopic = (
    htmlString: string,
    type: string,
    meetingIndex: number,
    topicIndex: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.meetingsList[meetingIndex][type][topicIndex]["topic"] = htmlString;
    newState.meetingsList[meetingIndex].edited = true;
    this.setState(newState);
  };

  handleChangeMeetingItems = (
    htmlString: string,
    type: string,
    meetingIndex: number,
    topicIndex: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.meetingsList[meetingIndex][type][topicIndex] = htmlString;
    newState.meetingsList[meetingIndex].edited = true;
    this.setState(newState);
  };

  onClickMeetingToggle = (meetingIndex: number) => {
    const newState = cloneDeep(this.state);
    newState.meetingsList[meetingIndex].open = !newState.meetingsList[
      meetingIndex
    ].open;
    this.setState(newState);
  };

  deleteMeeting = (data: IMeetingTemplate, index: number) => {
    const newState = cloneDeep(this.state);
    if (data.id) {
      this.props
        .deleteMeeting({ id: data.id } as IMeetingTemplate)
        .then((action) => {
          if (action.type === NEW_KICK_OFF_MEET_SUCCESS) {
            newState.meetingsList.splice(index, 1);
            this.props.addSuccessMessage(
              `Meeting ${data.template_name} deleted successfully!`
            );
          }
          this.setState(newState);
        });
    } else {
      newState.meetingsList.splice(index, 1);
      this.setState(newState);
    }
  };

  validateMeeting = (
    currentMeeting: IMeetingTemplate,
    meetingIndex: number
  ) => {
    let isValid = true;
    const newState = cloneDeep(this.state);
    if (!currentMeeting.template_name.trim()) {
      isValid = false;
      newState.meetingsList[meetingIndex].titleError =
        "This field may not be blank";
    }
    currentMeeting.action_items.forEach((el) => {
      if (commonFunctions.isEditorEmpty(el)) {
        isValid = false;
        newState.meetingsList[meetingIndex].hasError = true;
      }
    });
    currentMeeting.agenda_topics.forEach((el) => {
      if (commonFunctions.isEditorEmpty(el.topic)) {
        isValid = false;
        newState.meetingsList[meetingIndex].hasError = true;
      }
    });
    if (!isValid) this.setState(newState);
    return isValid;
  };

  onSaveRowClick = (currentMeeting: IMeetingTemplate, meetingIndex: number) => {
    if (this.validateMeeting(currentMeeting, meetingIndex)) {
      const newState = cloneDeep(this.state);
      currentMeeting.meeting_type = this.props.meetingType;
      this.props.createMeeting(currentMeeting).then((action) => {
        if (action.type === NEW_KICK_OFF_MEET_SUCCESS) {
          newState.meetingsList[meetingIndex].edited = false;
          newState.meetingsList[meetingIndex].titleError = "";
          newState.meetingsList[meetingIndex].id = action.response.id;
          newState.meetingsList[meetingIndex].hasError = false;
          this.props.addSuccessMessage(
            `Meeting ${currentMeeting.template_name} Saved Successfully!`
          );
        }
        if (action.type === NEW_KICK_OFF_MEET_FAILURE) {
          newState.meetingsList[meetingIndex].edited = true;
          newState.meetingsList[meetingIndex].titleError =
            action.errorList.data.template_name;
          newState.meetingsList[meetingIndex].hasError = true;
          this.props.addErrorMessage(`Error saving meeting!`);
        }
        this.setState(newState);
      });
    }
  };

  deleteMeetingTopic = (
    meetingIndex: number,
    type: string,
    topicIndex: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.meetingsList[meetingIndex][type].splice(topicIndex, 1);
    newState.meetingsList[meetingIndex].edited = true;
    this.setState(newState);
  };

  render() {
    return (
      <div className="kick-off-meeting-component">
        <div className="board-list-status">
          {this.state.meetingsList.map((meeting, meetingIndex) => {
            return (
              <div
                className="kick-off-meeting-row col-md-12 board-row"
                key={meetingIndex}
              >
                <div className="header">
                  <div className="left">
                    <div
                      className="action-collapse right"
                      onClick={(e) => this.onClickMeetingToggle(meetingIndex)}
                    >
                      {!meeting.open ? (
                        <img
                          className="arrow-play"
                          alt=""
                          src={"/assets/icons/play-arrow-blue.svg"}
                        />
                      ) : (
                        <img
                          className="arrow-play down"
                          alt=""
                          src={"/assets/icons/play-arrow-blue.svg"}
                        />
                      )}
                    </div>
                    <Input
                      field={{
                        label: "",
                        type: InputFieldType.TEXT,
                        value: meeting.template_name,
                        isRequired: false,
                      }}
                      width={12}
                      multi={false}
                      name="template_name"
                      error={
                        meeting.titleError && {
                          errorState: IValidationState.ERROR,
                          errorMessage: meeting.titleError,
                        }
                      }
                      onChange={(e) =>
                        this.handleChangeMeeting(e, meetingIndex)
                      }
                      placeholder={`Enter Name `}
                      className="template-name"
                    />
                  </div>

                  <div className="row-action">
                    {meeting.edited && (
                      <img
                        className="saved col-md-1"
                        alt=""
                        src={"/assets/icons/tick-blue.svg"}
                        onClick={(e) =>
                          this.onSaveRowClick(meeting, meetingIndex)
                        }
                      />
                    )}
                    <SmallConfirmationBox
                      className="remove-meeting"
                      onClickOk={() =>
                        this.deleteMeeting(meeting, meetingIndex)
                      }
                      text={`${this.props.meetingType} Meeting`}
                    />
                  </div>
                </div>
                {meeting.open && (
                  <>
                    <div className="action-list">
                      <div className="sub-heading">Agenda Topics</div>
                      <div className="action-item-list">
                        {meeting.agenda_topics.map((topic, topicIndex) => {
                          return (
                            <div className="action-item" key={topicIndex}>
                              <Input
                                field={{
                                  label: "",
                                  type: InputFieldType.CUSTOM,
                                  value: "",
                                  isRequired: false,
                                }}
                                width={7}
                                onChange={() => null}
                                name=""
                                customInput={
                                  <QuillEditorAcela
                                    onChange={(htmlString: string) =>
                                      this.handleChangeMeetingTopic(
                                        htmlString,
                                        "agenda_topics",
                                        meetingIndex,
                                        topicIndex
                                      )
                                    }
                                    value={topic.topic}
                                    customToolbar={[
                                      { list: "ordered" },
                                      { list: "bullet" },
                                    ]}
                                    wrapperClass={`template-agenda-topics-quill`}
                                    label={null}
                                    isRequired={false}
                                    markDownVariables={[]}
                                    hideFullscreen={true}
                                    hideTable={true}
                                    error={
                                      commonFunctions.isEditorEmpty(
                                        topic.topic
                                      ) &&
                                      meeting.hasError && {
                                        errorState: IValidationState.ERROR,
                                        errorMessage:
                                          "This field should not be blank",
                                      }
                                    }
                                  />
                                }
                              />
                              <SmallConfirmationBox
                                className="remove-action-item"
                                onClickOk={() =>
                                  this.deleteMeetingTopic(
                                    meetingIndex,
                                    "agenda_topics",
                                    topicIndex
                                  )
                                }
                                text={"Agenda topic"}
                              />
                            </div>
                          );
                        })}
                      </div>
                      <div
                        onClick={(e) => {
                          const newState = cloneDeep(this.state);
                          newState.meetingsList[
                            meetingIndex
                          ].agenda_topics.push({ topic: "", notes: "" });
                          this.setState(newState);
                        }}
                        className="add-new-status-topic"
                      >
                        Add Agenda
                      </div>
                    </div>
                    <div className="action-list action-list-margin">
                      <div className="sub-heading">Action Items</div>
                      <div className="action-item-list">
                        {meeting.action_items.map((action_item, topicIndex) => {
                          return (
                            <div className="action-item" key={topicIndex}>
                              <Input
                                field={{
                                  label: "",
                                  type: InputFieldType.CUSTOM,
                                  value: "",
                                  isRequired: false,
                                }}
                                width={7}
                                onChange={() => null}
                                name=""
                                customInput={
                                  <QuillEditorAcela
                                    onChange={(htmlString: string) =>
                                      this.handleChangeMeetingItems(
                                        htmlString,
                                        "action_items",
                                        meetingIndex,
                                        topicIndex
                                      )
                                    }
                                    value={action_item}
                                    customToolbar={[
                                      { list: "ordered" },
                                      { list: "bullet" },
                                    ]}
                                    wrapperClass={`template-agenda-topics-quill`}
                                    label={null}
                                    isRequired={false}
                                    markDownVariables={[]}
                                    hideFullscreen={true}
                                    hideTable={true}
                                    error={
                                      commonFunctions.isEditorEmpty(
                                        action_item
                                      ) &&
                                      meeting.hasError && {
                                        errorState: IValidationState.ERROR,
                                        errorMessage:
                                          "This field should not be blank",
                                      }
                                    }
                                  />
                                }
                              />
                              <SmallConfirmationBox
                                className="remove-action-item"
                                onClickOk={() =>
                                  this.deleteMeetingTopic(
                                    meetingIndex,
                                    "action_items",
                                    topicIndex
                                  )
                                }
                                text={"Action Item"}
                              />
                            </div>
                          );
                        })}
                      </div>
                      <div
                        onClick={(e) => {
                          const newState = cloneDeep(this.state);
                          newState.meetingsList[meetingIndex].action_items.push(
                            ""
                          );
                          this.setState(newState);
                        }}
                        className="add-new-status-topic"
                      >
                        Add Action Item
                      </div>
                    </div>
                  </>
                )}
              </div>
            );
          })}
        </div>
        {this.state.meetingsList.length === 0 && (
          <div className="no-status col-md-6">No template available</div>
        )}
        <div
          onClick={(e) => {
            const newState = cloneDeep(this.state);
            newState.meetingsList.push({
              template_name: "",
              action_items: [""],
              agenda_topics: [{ topic: "", notes: "" }],
              edited: true,
              open: true,
              hasError: false,
            });
            this.setState(newState);
          }}
          className="add-new-status"
        >
          {`Add ${this.props.meetingType} Meetings Template`}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getStatusMeetings: () => dispatch(getStatusMeetings()),
  getKickoffMeetings: () => dispatch(getKickoffMeetings()),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  createMeeting: (data: IMeetingTemplate) =>
    dispatch(createKickoffMeeting(data)),
  deleteMeeting: (data: IMeetingTemplate) =>
    dispatch(deleteKickoffMeeting(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MeetingTemplate);
