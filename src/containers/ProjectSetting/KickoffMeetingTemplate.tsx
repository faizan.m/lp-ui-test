import { cloneDeep } from 'lodash';
import _ from 'lodash';
import React, { Component } from "react";
import { Mention, MentionsInput } from 'react-mentions';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import 'quill-mention';
import { connect } from "react-redux";
import "./style.scss";
import SquareButton from '../../components/Button/button';
import { commonFunctions } from '../../utils/commonFunctions';

interface IMeetingEmailTemplateState {
  meetingEmailTemplateMailSubject: string;
  meetingEmailTemplateMailBodyText: string;
  meetingEmailTemplateMailBodyMarkDown: string;
  showActionButtons: boolean;
  savedSuccess: boolean;
}

interface IIProjectSettingProps extends ICommonProps {
 
}

interface IPassedProps {
  mailTemplateData?: any;
  setData?: any;
}

class KickoffMeetingEmailTemplate extends Component<
  IPassedProps & IIProjectSettingProps,
  IMeetingEmailTemplateState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  customerMailBodyRef: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    meetingEmailTemplateMailSubject: "",
    meetingEmailTemplateMailBodyMarkDown: "",
    meetingEmailTemplateMailBodyText: "",
    showActionButtons: false,
    savedSuccess: false
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);

    },
  };

  componentDidMount() {
    this.setState({
      meetingEmailTemplateMailSubject: this.props.mailTemplateData.email_subject,
      meetingEmailTemplateMailBodyMarkDown: this.props.mailTemplateData.email_body_markdown,
      meetingEmailTemplateMailBodyText: this.props.mailTemplateData.email_body_text,
      savedSuccess: this.props.mailTemplateData.email_body_text !== ""
    });

  }

  componentDidUpdate(prevProps: IIProjectSettingProps & IPassedProps) {
    if (
      this.props.mailTemplateData &&
      this.props.mailTemplateData !== prevProps.mailTemplateData
    ) {
      this.setState({
        meetingEmailTemplateMailSubject: this.props.mailTemplateData
          .email_subject,
        meetingEmailTemplateMailBodyMarkDown: this.props.mailTemplateData
          .email_body_markdown,
        meetingEmailTemplateMailBodyText: this.props.mailTemplateData
          .email_body_text,
        savedSuccess: this.props.mailTemplateData.email_body_text !== "",
      });
    }
  }

  handleChangeMailSubject = e => {
    const newState = cloneDeep(this.state);
    (newState.meetingEmailTemplateMailSubject as any) = e.target.value;
    this.setState({ ...newState, showActionButtons: true });
  };

  createMeetingTemplate = () => {
    const {
      savedSuccess,
      meetingEmailTemplateMailSubject,
      meetingEmailTemplateMailBodyMarkDown,
      meetingEmailTemplateMailBodyText
    } = this.state;

    const regex = /#RULE#/gi;
    const mailSubject = meetingEmailTemplateMailSubject.replace(regex, '');
    const mailBodyTxtArr = meetingEmailTemplateMailBodyText.split(" ")

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith('{')) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    })

    this.props.setData({
      meeting_type: "Kickoff",
      email_subject: mailSubject,
      email_body_text: mailBodyTxtFinalArr.join(" "),
      email_body_markdown: meetingEmailTemplateMailBodyMarkDown,
      isCreated: savedSuccess
    })

  }
 

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChange = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.meetingEmailTemplateMailBodyText as any) = this.getContentFromObject(object);
    (newState.meetingEmailTemplateMailBodyMarkDown as any) = content;
    this.setState({ ...newState, showActionButtons: true });
  };

  render() {
    return (
      <div className="customer-contact-role col-md-12">
        <div className="col-md-12 row customer-email">
          <div>
            <label className="col-md-3 field__label-label email-subject" >
              <span><b>Email Subject</b> (The subject that will be populated by default in the Kickoff Meeting)
              </span>
            </label>
          </div>
          <div className="mail-subject-mention-input">
            <MentionsInput
              markup="[__display__]"
              value={this.state.meetingEmailTemplateMailSubject || ''}
              onChange={(e) => this.handleChangeMailSubject(e)}
              className={'outer'}
            >
              <Mention
                trigger="#"
                data={commonFunctions.rangeVariables()}
                className={'inner-drop'}
                markup="#RULE#__display__"
              />
            </MentionsInput>
          </div>
        </div>
        <div className="col-md-12 row customer-email">
          <label className="col-md-3 field__label-label email-subject" >
            <span><b>Email Message</b> (The default message that will be shown in the email sent for Kickoff meeting)
            </span>
          </label>
          <div className="cust-touch-md-editor">
            <ReactQuill
              ref={this.customerMailBodyRef}
              value={this.state.meetingEmailTemplateMailBodyMarkDown || ''}
              onChange={this.handleChange}
              modules={{ mention: this.mentionModule }}
            />
          </div>
          {
            this.state.showActionButtons &&
            this.state.meetingEmailTemplateMailBodyMarkDown !== "" &&
            this.state.meetingEmailTemplateMailSubject !== "" && (
              <SquareButton
                onClick={e => this.createMeetingTemplate()}
                content="Save"
                bsStyle={ButtonStyle.PRIMARY}
                className="save-mail-btn"
              />
            )
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  state: IReduxStore,
  ownProps: IPassedProps,
) => ({
  mailTemplateData: ownProps.mailTemplateData
});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KickoffMeetingEmailTemplate);
