import { cloneDeep } from 'lodash';
import _ from 'lodash';
import React, { Component } from "react";
import { Mention, MentionsInput } from 'react-mentions';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import 'quill-mention';
import { connect } from "react-redux";
import "./style.scss";
import SquareButton from '../../components/Button/button';
import {
  createCloseOutMeetingTemplate,
  udpateCloseOutMeetingTemplate
} from "../../actions/pmo";
import { commonFunctions } from '../../utils/commonFunctions';

interface ICloseOutMeetingTemplateState {
  closeOutMeetingMailBodyText: string;
  closeOutMeetingMailBodyMarkDown: string;
  closeOutMeetingEmailTemplateMailSubject: string;
  showActionButtons: boolean;
  savedSuccess: boolean;
}

interface IIProjectSettingProps extends ICommonProps {
  createCloseOutMeetingTemplate: any;
  udpateCloseOutMeetingTemplate: any;
}

interface IPassedProps {
  meeting?: boolean;
  mailTemplateData?: any;
  setData?: any;
}

class CloseOutMeetingTemplate extends Component<
  IPassedProps & IIProjectSettingProps,
  ICloseOutMeetingTemplateState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  customerMailBodyRef: any;
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    closeOutMeetingMailBodyMarkDown: "",
    closeOutMeetingMailBodyText: "",
    closeOutMeetingEmailTemplateMailSubject: "",
    showActionButtons: false,
    savedSuccess: false,
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);

    },
  };

  componentDidUpdate(prevProps: IIProjectSettingProps & IPassedProps) {
    if (
      this.props.mailTemplateData &&
      this.props.mailTemplateData !== prevProps.mailTemplateData
    ) {
      this.setState({
        closeOutMeetingEmailTemplateMailSubject: this.props.mailTemplateData
          .email_subject,
        closeOutMeetingMailBodyMarkDown: this.props.mailTemplateData
          .email_body_markdown,
        closeOutMeetingMailBodyText: this.props.mailTemplateData
          .email_body_text,
        savedSuccess: this.props.mailTemplateData.email_body_text !== "",
      });
    }
  }

  createMeetingTemplate = () => {
    const {
      savedSuccess,
      closeOutMeetingMailBodyMarkDown,
      closeOutMeetingMailBodyText,
      closeOutMeetingEmailTemplateMailSubject
    } = this.state;

    const mailBodyTxtArr = closeOutMeetingMailBodyText.split(" ")

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith('{')) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    })

    this.props.setData({
      meeting_type: "Close Out",
      email_subject: closeOutMeetingEmailTemplateMailSubject,
      email_body_text: mailBodyTxtFinalArr.join(" "),
      email_body_markdown: closeOutMeetingMailBodyMarkDown,
      isCreated: savedSuccess
    })
  }


  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangeMailSubject = e => {
    const newState = cloneDeep(this.state);
    (newState.closeOutMeetingEmailTemplateMailSubject as any) = e.target.value;
    this.setState({ ...newState, showActionButtons: true });
  };

  handleChange = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.closeOutMeetingMailBodyText as any) = this.getContentFromObject(object);
    (newState.closeOutMeetingMailBodyMarkDown as any) = content;
    this.setState({ ...newState, showActionButtons: true });
  };

  render() {
    return (
      <div className="customer-contact-role col-md-12">
        <label className="col-md-3 field__label-label email-subject" >
          <span><b>Project Acceptance</b></span>
        </label>
        <div className="col-md-12 row customer-email">
          <div>
            <label className="col-md-3 field__label-label email-subject" >
              <span><b>Email Subject</b> (The subject that will be populated by default in the Status Meeting)
              </span>
            </label>
          </div>
          <div className="mail-subject-mention-input">
            <MentionsInput
              markup="[__display__]"
              value={this.state.closeOutMeetingEmailTemplateMailSubject || ''}
              onChange={(e) => this.handleChangeMailSubject(e)}
              className={'outer'}
            >
              <Mention
                trigger="#"
                data={commonFunctions.rangeVariables()}
                className={'inner-drop'}
                markup="#RULE#__display__"
              />
            </MentionsInput>
          </div>
        </div>
        <div className="col-md-12 row customer-email">
          <label className="col-md-3 field__label-label email-subject" >
            <span><b>Email Message</b> (The default message that will be shown in the email sent for Close Out meeting)
            </span>
          </label>
          <div className="cust-touch-md-editor">
            <ReactQuill
              ref={this.customerMailBodyRef}
              value={this.state.closeOutMeetingMailBodyMarkDown || ''}
              onChange={this.handleChange}
              modules={{ mention: this.mentionModule }}
            />
          </div>
          {
            !this.props.meeting &&
            this.state.showActionButtons &&
            this.state.closeOutMeetingMailBodyMarkDown !== "" && (
              <SquareButton
                onClick={e => this.createMeetingTemplate()}
                content="Save"
                bsStyle={ButtonStyle.PRIMARY}
                className="save-mail-btn"
              />
            )
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  state: IReduxStore,
  ownProps: IPassedProps,
) => ({
  meeting: ownProps.meeting,
});

const mapDispatchToProps = (dispatch: any) => ({
  createCloseOutMeetingTemplate: (data) => dispatch(createCloseOutMeetingTemplate(data)),
  udpateCloseOutMeetingTemplate: (data) => dispatch(udpateCloseOutMeetingTemplate(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CloseOutMeetingTemplate);
