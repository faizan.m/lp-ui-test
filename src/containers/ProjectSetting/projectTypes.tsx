import React from "react";
import Select from 'react-select';
import { cloneDeep, flatMap, uniqBy } from "lodash";
import Input from "../../components/Input/input";
import { connect } from "react-redux";
import Spinner from "../../components/Spinner";
import { DraggableArea } from 'react-draggable-tags';
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import PMOTooltip from "../../components/PMOTooltip/pmoTooltip";
import { NEW_PROJECT_TYPE_SUCCESS, NEW_PROJECT_TYPE_FAILURE, NEW_PROJECT_PHASE_SUCCESS, NEW_PROJECT_PHASE_FAILURE, getProjectTypeList, postProjectType, editProjectType, deleteProjectType, editProjectPhase, postProjectPhase } from "../../actions/pmo";
interface IProjectTypeProps {
  getProjectTypeList: any;
}

interface IProjectTypeState {
  ProjectTypeList: IProjectType[];
  open: boolean;
  loading: boolean;
}

class ProjectTypePhase extends React.Component<
  any,
  IProjectTypeState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  creatableEl: any;

  constructor(props: IProjectTypeProps) {
    super(props);

    this.state = {
      ProjectTypeList: [{
        title: "",
        edited: true,
        titleError: '',
        phases: [],
      }],
      open: true,
      loading: false,
    };
  }
  componentDidMount() {
    this.setState({ loading: true })
    this.getProjectTypeList();
  }

  getProjectTypeList = () => {
    this.props.getProjectTypeList().then(
      action => {
        this.setState({ loading: false, ProjectTypeList: action.response })
      }
    )
  }

  addProjectType = () => {
    const newState = cloneDeep(this.state);
    newState.ProjectTypeList.push({
      title: "",
      edited: true,
      phases: [],
    });

    this.setState(newState);
  };


  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    if (rowData.id) {
      this.props.deleteProjectType(rowData).then(
        action => {
          if (action.type === NEW_PROJECT_TYPE_SUCCESS) {
            newState.ProjectTypeList.splice(index, 1);
          }
          if (action.type === NEW_PROJECT_TYPE_FAILURE) {
            newState.ProjectTypeList[index].titleError = `Couldn't delete`;
          }
          this.setState(newState);
        }
      )
    }
  };

  onSaveRowClick = (e, index, rowData) => {
    const newState = cloneDeep(this.state);
    rowData.phases = rowData.phases.map((tag, i) => ({ ...tag, order: i + 1 }))

    if (rowData.id) {
      this.props.editProjectType(rowData).then(
        action => {
          if (action.type === NEW_PROJECT_TYPE_SUCCESS) {
            newState.ProjectTypeList[index].edited = false;
            newState.ProjectTypeList[index].titleError = '';
            rowData.id = action.response.id;
            this.editProjectPhase(e, index, rowData)
          }
          if (action.type === NEW_PROJECT_TYPE_FAILURE) {
            newState.ProjectTypeList[index].edited = true;
            newState.ProjectTypeList[index].titleError = action.errorList.data &&
              action.errorList.data.title &&
              action.errorList.data.title.join(' ') || action.errorList.data && action.errorList.data[0] && action.errorList.data[0].message.join(' ');
          }
          this.setState(newState);
        }
      )
    } else {
      this.props.postProjectType(rowData).then(
        action => {
          if (action.type === NEW_PROJECT_TYPE_SUCCESS) {
            newState.ProjectTypeList[index].edited = false;
            newState.ProjectTypeList[index].titleError = '';
            newState.ProjectTypeList[index].id = action.response.id;
            rowData.id = action.response.id;
            this.props.getProjectTypeList().then(
              a => {
                this.setState({ loading: false, ProjectTypeList: a.response })
              }
            )
          }
          if (action.type === NEW_PROJECT_TYPE_FAILURE) {
            newState.ProjectTypeList[index].edited = true;
            newState.ProjectTypeList[index].titleError = action.errorList.data &&
              action.errorList.data.title &&
              action.errorList.data.title.join(' ') || action.errorList.data && action.errorList.data[0] && action.errorList.data[0].message.join(' ');
          }
          this.setState(newState);
        }
      )
    }

  };
  editProjectPhase = (e, index, rowData) => {
    const newState = cloneDeep(this.state);

    if (rowData.id) {
      this.props.editProjectPhase(rowData).then(
        action => {
          if (action.type === NEW_PROJECT_TYPE_SUCCESS) {
            newState.ProjectTypeList[index].edited = false;
            newState.ProjectTypeList[index].titleError = '';

          }
          if (action.type === NEW_PROJECT_TYPE_FAILURE) {
            newState.ProjectTypeList[index].edited = true;
            newState.ProjectTypeList[index].titleError = action.errorList.data &&
              action.errorList.data.title &&
              action.errorList.data.title.join(' ') || action.errorList.data.join(' ');
          }
          this.setState(newState);
        }
      )
    }
  };

  handleChangPhases = (tags, index) => {
    const orderedPhases = tags.map((tag, i) => ({ ...tag, order: i + 1 }))
    const newState = cloneDeep(this.state);
    newState.ProjectTypeList[index].phases = orderedPhases;
    newState.ProjectTypeList[index].edited = true;
    this.setState(newState);
  };

  addProjectPhase = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.ProjectTypeList[index].newTag = true;
    this.setState(newState);
  };

  handleClickDelete(tag, index, typeIndex) {
    const newState = cloneDeep(this.state);
    newState.ProjectTypeList[typeIndex].edited = true;
    newState.ProjectTypeList[typeIndex].phases.splice(index, 1);
    this.setState(newState);
  }

  handleChangeType = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.ProjectTypeList[index].title = e.target.value;
    newState.ProjectTypeList[index].edited = true;
    this.setState(newState);
  };

  handleOnBlur = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.ProjectTypeList[index].newTag = false;
    this.setState(newState);
  };
  onNewOptionClick = (name, index, creatableEl) => {
    const newState = cloneDeep(this.state);
    const allTitles = newState.ProjectTypeList[index].phases.map(p => p.title);
    if (!allTitles.includes(name)) {

      this.props.postProjectPhase(newState.ProjectTypeList[index], { title: name, order: newState.ProjectTypeList[index].phases.length + 1 }).then(
        action => {
          if (action.type === NEW_PROJECT_PHASE_SUCCESS) {
            newState.ProjectTypeList[index].edited = false;
            newState.ProjectTypeList[index].titleError = '';
            newState.ProjectTypeList[index].phases.push({ title: name, id: action.response.id, order: action.response.order });
          }
          if (action.type === NEW_PROJECT_PHASE_FAILURE) {
            newState.ProjectTypeList[index].edited = true;
            newState.ProjectTypeList[index].titleError = action.errorList.data &&
              action.errorList.data.title &&
              action.errorList.data.title.join(' ') ||
              action.errorList.data.join(' ');
          }
          this.setState(newState);
        }
      )
      this.setState(newState);
    }
    creatableEl.select.setState({ inputValue: '' });  // <------- clears the input text
  };
  getAllPhases = (rowData) => {
    const allPhases = uniqBy(
      flatMap(this.state.ProjectTypeList, (t) => t.phases)
        .map(type => ({ label: type.title, value: type.title })), 'label')
    const rowDataPhases = rowData.phases.map(phase => phase.title);
    const filteredPhases = allPhases.filter(x => !rowDataPhases.includes(x.label));
    return filteredPhases;
  }
  render() {
    const ProjectTypeList = this.state.ProjectTypeList;

    return (
      <div className="project-types board-list-status">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {
          ProjectTypeList.length === 0 && !this.state.loading && (
            <div className="no-status col-md-6">
              No project types available
            </div>
          )
        }
        {
          this.state.ProjectTypeList && !this.state.loading &&
          this.state.ProjectTypeList.map((row, typeIndex) => (
            <div className="col-md-12 board-row" key={typeIndex}>
              <div className="header">
                <Input
                  field={{
                    label: 'Type ',
                    type: InputFieldType.TEXT,
                    value: row.title,
                    isRequired: false,
                  }}
                  width={4}
                  multi={true}
                  name="title"
                  onChange={e => this.handleChangeType(e, typeIndex)}
                  placeholder={`Project Type`}
                  error={row.titleError && {
                    errorState: IValidationState.ERROR,
                    errorMessage: row.titleError,
                  }}
                  loading={this.state.loading}
                />
                <div className="row-action">
                  {
                    row.edited && (
                      <img
                        className="saved col-md-1 refresh-btn"
                        alt=""
                        src={'/assets/icons/refresh.svg'}
                        onClick={(e) => this.getProjectTypeList()}
                      />)
                  }
                  {
                    row.edited && (
                      <img
                        className="saved col-md-1"
                        alt=""
                        src={'/assets/icons/tick-blue.svg'}
                        onClick={(e) => this.onSaveRowClick(e, typeIndex, row)}
                      />)
                  }
                  {
                    !row.in_use ?
                      <SmallConfirmationBox
                        className='delete-icon-board'
                        onClickOk={() => this.onDeleteRowClick(typeIndex, row)}
                        text={'Type'}
                      /> :
                      <PMOTooltip
                        iconClass="status-svg-images-black"
                        icon={`/assets/icons/cross-sign.svg`}
                        className="disable-delete"
                      >
                        <div style={{ borderColor: 'red' }} className="data-box">
                          <div style={{ color: 'red' }} className="text">  Type in use can't delete </div>
                        </div>
                      </PMOTooltip>
                  }

                </div>

              </div>
              <div className="project-phases" 
>
                <DraggableArea
                  tags={row.phases}
                  render={({ tag, index }) => (
                    <div className="tag">
                      <div className="tag-index">{index + 1}</div>
                      <div className="tag-text">{tag.title}</div>
                      {
                        row.in_use ?
                          <PMOTooltip
                            iconClass="status-svg-images-black"
                            icon={`/assets/icons/cross-sign.svg`}
                            className="disable-delete"
                          >
                            <div style={{ borderColor: 'red' }} className="data-box">
                              <div style={{ color: 'red'}} className="text">  Type in use can't update & delete </div>
                            </div>
                          </PMOTooltip> :
                          <img
                            className="delete"
                            src={'/assets/icons/cross-sign.svg'}
                            onClick={() => this.handleClickDelete(tag, index, typeIndex)}
                          />
                      }

                    </div>
                  )}
                  onChange={tags => this.handleChangPhases(tags, typeIndex)}
                />
                {
                  !row.newTag && row.id && <div
                    onClick={e => this.addProjectPhase(e, typeIndex)}
                    className="add-new-project-phase"
                  >Add Project Phase</div>
                }
                {
                  row.newTag === true &&
                  <div className="col-md-4 createble-phases">
                    <Select.Creatable
                      name="classification"
                      options={this.getAllPhases(row)}
                      onChange={e => this.onNewOptionClick(e.label, typeIndex, this.creatableEl)}
                      onNewOptionClick={e => this.onNewOptionClick(e.label, typeIndex, this.creatableEl)}
                      ref={el => this.creatableEl = el}   // <-------- here
                      loading={this.props.isFetching}
                      placeholder="Select or Type to Create New"
                      clearable={true}
                      promptTextCreator={name => `Create Type "${name}"`}
                      onBlur={e => this.handleOnBlur(e, typeIndex)}
                    />
                  </div>
                }


              </div>



            </div>
          ))
        }

        <div
          onClick={e => this.addProjectType()}
          className="add-new-project-board"
        >Add Project Type</div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectTypeList: () => dispatch(getProjectTypeList()),
  postProjectType: (data: any) => dispatch(postProjectType(data)),
  editProjectType: (data: any) => dispatch(editProjectType(data)),
  deleteProjectType: (data: any) => dispatch(deleteProjectType(data)),
  editProjectPhase: (data: any) => dispatch(editProjectPhase(data)),
  postProjectPhase: (data: any, phase) => dispatch(postProjectPhase(data, phase)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectTypePhase);

