import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";
import { connect } from "react-redux";
import { fetchCustomersShort, setCustomerId } from "../../actions/customer";
import { fetchDevices } from "../../actions/inventory";
import {
  fetchUserProfile,
  editUserProfile,
  PROFILE_EDIT_FAILURE,
  PROFILE_EDIT_SUCCESS,
} from "../../actions/profile";
import SelectInput from "../../components/Input/Select/select";
import { isValidVal } from "../../utils/CommonUtils";
import "./style.scss";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";
import Input from "../../components/Input/input";
import cloneDeep from "lodash/cloneDeep";
import { addSuccessMessage, addErrorMessage } from "../../actions/appState";
import _ from "lodash";
import AppValidators from "../../utils/validator";

interface IHeaderProps extends ICommonProps {
  fetchUserProfile: TFetchUserProfile;
  fetchCustomersShort: TFetchCustomers;
  user: ISuperUser;
  customers: ICustomerShort[];
  fetchDevices: TFetchDevices;
  setCustomerId: any;
  showCustomerSelection: boolean;
  disableCustomerSelection: boolean;
  customerId: number;
  customerProfile: any;
  customersShortFetching: any;
  isFetching: any;
  editUserProfile: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  configStatus: IConfigStatus;
  logoUrl: string;
  company_url: string;
  enable_url: boolean;
}

interface IHeaderState {
  searchQuery: string;
  user: ISuperUser;
  customers: ICustomerShort[];
  showCustomerSelection: boolean;
  customerId?: number;
  ccoidMissingModal: boolean;
  showccoidAckModal: boolean;
  error: {
    cco_id: IFieldValidation;
  };
}

class HeaderMenu extends React.Component<IHeaderProps, IHeaderState> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: IHeaderProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    user: null,
    searchQuery: null,
    customers: [],
    showCustomerSelection: false,
    ccoidMissingModal: false,
    showccoidAckModal: false,
    error: {
      cco_id: {
        ...HeaderMenu.emptyErrorState,
      },
    },
  });
  componentDidMount() {
    if (!this.props.user) {
      this.props.fetchUserProfile();
    }
    this.props.fetchCustomersShort();
    const searchQuery = localStorage.getItem("customerId");
    this.setState({ searchQuery });
    this.props.setCustomerId(searchQuery);
    const defaultSet = localStorage.getItem("invertFilter") || "";
    const filter = defaultSet === "" ? "" : `invert(0.85)`;
    document.documentElement.style.setProperty("--invertFilter", filter);
    localStorage.setItem("invertFilter", filter);
  }

  componentDidUpdate(prevProps: IHeaderProps) {
    if (this.props.user && this.props.user !== prevProps.user) {
      this.setState({
        user: this.props.user,
      });
      if (
        this.props.user.type === "customer" &&
        !this.props.user.profile.cco_id_acknowledged &&
        !this.props.user.profile.cco_id
      ) {
        this.setState({ ccoidMissingModal: true });
      }
    }
    if (this.props.customers !== prevProps.customers) {
      this.setState({
        customers: this.props.customers,
      });
    }
  }

  handleChange = (event: any, parentName?: any) => {
    this.setState({ searchQuery: event.target.value });
    //this.props.fetchDevices(event.target.value, false);
    localStorage.setItem("customerId", event.target.value);
    this.props.setCustomerId(event.target.value);
  };

  getCustomerOptions = () => {
    if (this.state.customers && this.state.customers.length > 0) {
      return this.state.customers.map((role) => ({
        value: role.id,
        label: role.name,
      }));
    } else {
      return [];
    }
  };

  toggleCcoidMissingAckModalDialog = () => {
    this.setState((prevState) => ({
      ccoidMissingModal: false,
      showccoidAckModal: false,
    }));
  };

  toggleCcoidMissingModalDialog = () => {
    this.setState((prevState) => ({
      ccoidMissingModal: !prevState.ccoidMissingModal,
      showccoidAckModal: !prevState.showccoidAckModal,
    }));
  };

  onClickConfirmDialog = (e) => {
    if (this.validateForm()) {
      const user = this.state.user;

      if (user.profile.cco_id && user.profile.cco_id !== "") {
        user.profile.cco_id_acknowledged = true;
      }

      this.props.editUserProfile(user).then((action) => {
        if (action.type === PROFILE_EDIT_FAILURE) {
          this.props.addErrorMessage("CCO ID update failed.");
        } else if (action.type === PROFILE_EDIT_SUCCESS) {
          this.props.addSuccessMessage("CCO ID Updated.");
        }
        this.setState({ ccoidMissingModal: false });
      });
    }
  };

  onClickAckConfirmDialog = () => {
    const user = this.state.user;

    user.profile.cco_id_acknowledged = true;

    this.props.editUserProfile(user).then((action) => {
      if (action.type === PROFILE_EDIT_FAILURE) {
        this.props.addErrorMessage("CCO ID association update failed.");
      } else if (action.type === PROFILE_EDIT_SUCCESS) {
        this.props.addSuccessMessage("CCO ID association Updated.");
      }
      this.setState({ showccoidAckModal: false, ccoidMissingModal: false });
    });
  };

  confirmBoxBody = () => {
    return (
      <div className="ccoid-box   col-md-12 col-xs-12">
        <div className="import-error-msg">
          We do not have your Cisco CCO information. Would you like to enter it
          now?
        </div>
        <Input
          field={{
            label: "CCO ID",
            type: InputFieldType.TEXT,
            isRequired: true,
            value: _.get(this.state.user, "profile.cco_id"),
          }}
          error={this.state.error.cco_id}
          width={12}
          placeholder="Enter CCO ID"
          name="cco_id"
          onChange={this.handleChangeConfirm}
        />
      </div>
    );
  };

  confirmAckBoxBody = () => {
    return (
      <div className="ccoid-box   col-md-12 col-xs-12">
        <div className="import-error-msg">
          If no Cisco.com ID provided a manual access/association request for
          the contract will be required.
        </div>
      </div>
    );
  };

  validateForm = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      this.state.user.type === "customer" &&
      !_.get(this.state.user, "profile.cco_id")
    ) {
      error.cco_id.errorState = IValidationState.ERROR;
      error.cco_id.errorMessage = "Enter CCO ID";

      isValid = false;
    }
    if (
      this.state.user.type === "customer" &&
      _.get(this.state.user, "profile.cco_id") &&
      !AppValidators.noSpaceAllowed(_.get(this.state.user, "profile.cco_id"))
    ) {
      error.cco_id.errorState = IValidationState.ERROR;
      error.cco_id.errorMessage = "No Spaces allowed";

      isValid = false;
    }
    this.setState({ error });

    return isValid;
  };

  handleChangeConfirm = (event: any) => {
    const targetValue = event.target.value;
    const newState = cloneDeep(this.state);
    newState.user.profile[event.target.name] = targetValue.trim();
    this.setState(newState);
  };

  onSaveDefaultPage = () => {
    const user = cloneDeep(this.state.user);
    document.body.click();
    user.profile.default_landing_page =
      window.location.pathname + window.location.search;
    this.props.editUserProfile(user).then((action) => {
      if (action.type === PROFILE_EDIT_SUCCESS) {
        this.props.addSuccessMessage("Landing page updated");
      }
      if (action.type === PROFILE_EDIT_FAILURE) {
        this.props.addErrorMessage("Error updating landing page!");
      }
    });
  };

  invertFilter = () => {
    const filter =
      localStorage.getItem("invertFilter") === "" ? `invert(0.85)` : "";

    document.documentElement.style.setProperty("--invertFilter", filter);
    localStorage.setItem("invertFilter", filter);
    document.body.click();
  };
  openNewTab = () => {
    if (this.props.company_url && this.props.enable_url) {
      window.open(this.props.company_url);
    }
  };
  render() {
    const popoverRight = (
      <Popover
        id="popover-positioned-right"
        title=""
        className="popover-header-user"
      >
        <div className="header-user__pop">
          <div
            className="header-user-options default-url"
            onMouseDown={() => this.props.history.push("/profile")}
          >
            Profile
          </div>
          <div
            className="header-user-options default-url"
            onMouseDown={() => this.props.history.push("/reset-password")}
          >
            Reset Password
          </div>
          <div
            className="header-user-options default-url"
            onMouseDown={() => this.onSaveDefaultPage()}
          >
            Set Landing page
          </div>
          <div
            className="header-user-options default-url"
            onMouseDown={() => this.invertFilter()}
          >
            Dark/Light(Beta)
          </div>
        </div>
      </Popover>
    );

    return (
      <div className="app-header">
        <div className="app-header__left">
          <div
            className={`side-menu__logo ${
              this.props.company_url && this.props.enable_url === true
                ? "pointer"
                : ""
            }`}
            onClick={this.openNewTab}
            style={{
              backgroundImage: `url(${
                isValidVal(this.props.logoUrl)
                  ? `${this.props.logoUrl}`
                  : window.location.hostname === "admin.acela.io"
                  ? "/assets/logo/logo.png"
                  : ""
              })`,
            }}
          ></div>
          <h2 />
          {this.props.showCustomerSelection &&
            this.props.user &&
            this.props.user.type &&
            this.props.user.type === "provider" && (
              <div
                className={`header-customer-select ${
                  !this.props.customerId ? "select-error" : ""
                }`}
              >
                <SelectInput
                  name="select"
                  value={this.state.searchQuery}
                  onChange={this.handleChange}
                  options={this.getCustomerOptions()}
                  searchable={true}
                  placeholder="Select Customer"
                  clearable={false}
                  disabled={this.props.disableCustomerSelection}
                  loading={this.props.customersShortFetching}
                />
                {this.props.configStatus &&
                  this.props.configStatus.crm_authentication_configured &&
                  this.props.configStatus.crm_board_mapping_configured &&
                  this.props.configStatus.crm_device_categories_configured &&
                  !window.location.pathname.includes("configuration") &&
                  !this.props.customerId && (
                    <div className="select-company-error">
                      Please select a company from the drop-down menu at the top
                      of the screen.
                    </div>
                  )}
              </div>
            )}
          {this.props.user &&
            this.props.user.type &&
            this.props.user.type === "customer" && (
              <div className="customer-name">
                {this.props.customerProfile && this.props.customerProfile.name}
              </div>
            )}
        </div>
        <div className="app-header__right">
          <div
            style={{
              backgroundImage: `url(${
                isValidVal(
                  this.props.user &&
                    this.props.user.profile &&
                    this.props.user.profile.profile_pic
                )
                  ? `${this.props.user.profile.profile_pic}`
                  : "/assets/icons/attachment-placeholder.png"
              })`,
            }}
            className="user-image-circle"
          />
          <OverlayTrigger
            trigger={"focus"}
            placement="bottom"
            overlay={popoverRight}
          >
            <div className="app-header__right-user-name" tabIndex={0}>
              <button className="dropdown-toggle btn btn-default">
                <span>
                  {this.state.user
                    ? `${this.state.user.first_name} ${this.state.user.last_name}`
                    : `User`}
                  <span className="Select-arrow select-arrow-user-right" />
                </span>
              </button>
            </div>
          </OverlayTrigger>
        </div>
        {this.state.ccoidMissingModal && (
          <ConfirmBox
            show={this.state.ccoidMissingModal}
            onClose={this.toggleCcoidMissingModalDialog}
            onSubmit={this.onClickConfirmDialog}
            isLoading={this.props.isFetching}
            title={"Pending"}
            okText={"Submit"}
            cancelText={"Cancel"}
            message={this.confirmBoxBody()}
            className="get-ccoid"
          />
        )}
        {this.state.showccoidAckModal && (
          <ConfirmBox
            show={this.state.showccoidAckModal}
            onClose={this.toggleCcoidMissingAckModalDialog}
            onSubmit={this.onClickAckConfirmDialog}
            isLoading={this.props.isFetching}
            title={"Pending"}
            okText={"Continue"}
            cancelText={"Cancel"}
            message={this.confirmAckBoxBody()}
            className="get-ccoid"
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customers: state.customer.customersShort,
  customerId: state.customer.customerId,
  customerProfile: state.customerUser.customerProfile,
  customersShortFetching: state.customer.customersShortFetching,
  isFetching: state.profile.isFetching,
  configStatus: state.providerIntegration.configStatus,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchUserProfile: () => dispatch(fetchUserProfile()),
  fetchCustomersShort: () => dispatch(fetchCustomersShort()),
  fetchDevices: (id: number, show: boolean) => dispatch(fetchDevices(id)),
  setCustomerId: (id: number) => dispatch(setCustomerId(id)),
  editUserProfile: (profile: ISuperUser) => dispatch(editUserProfile(profile)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMenu);
