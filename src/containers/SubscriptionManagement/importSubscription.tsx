import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import {
  addSite, batchCreateImportSubscription, BATCH_CREATE_SUCCESS, fetchCountriesAndStates,
  fetchDateFormat,
  fetchSites,
  fetchStatuses,
  fetchTaskStatus,
  TASK_STATUS_FAILURE,
  TASK_STATUS_SUCCESS, uploadDeviceFile, UPLOAD_DEVICE_FILE_SUCCESS
} from '../../actions/inventory';
import { fetchSubscriptions } from '../../actions/subscription';
import SquareButton from '../../components/Button/button';
import Checkbox from '../../components/Checkbox/checkbox';
import SelectInput from '../../components/Input/Select/select';
import Spinner from '../../components/Spinner';
import './style.scss';

const fields: any[] = [
  {
    count: 0,
    label: 'Configuration Name',
    value: 'subscription_name',
    isRequired: true,
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 1,
    label: 'Subscription ID',
    value: 'subscription_id',
    isRequired: true,
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 2,
    label: 'Contract Number',
    value: 'contract_number',
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 3,
    label: 'Start Date',
    value: 'start_date',
    isRequired: true,
    isDate: true,
    isCheckBox: false,
  },

  {
    count: 4,
    label: 'Renewal Date',
    value: 'expiration_date',
    isRequired: true,
    isDate: true,
    isCheckBox: false,
  },
  {
    count: 5,
    label: 'Term Length',
    value: 'term_length',
    message: '',
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 6,
    label: 'Auto Renewal',
    value: 'auto_renewal_term',
    message: '',
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 7,
    label: 'License Qty',
    value: 'licence_qty',
    isRequired: false,
    isDate: false,
  },
  {
    count: 8,
    label: 'Subscription SKU',
    value: 'model',
    isRequired: false,
    isDate: false,
  },
  {
    count: 9,
    label: 'Billing Model',
    value: 'billing_model',
    isRequired: false,
    isDate: false,
  },
  {
    count: 10,
    label: 'End Customer',
    value: 'end_customer',
    isRequired: true,
    isDate: false,
  },
  {
    count: 11,
    label: 'Status',
    value: 'status',
    isRequired: false,
    isDate: false,
  },
  {
    count: 12,
    label: 'True Forward Date',
    value: 'true_forward_date',
    isRequired: false,
    isDate: true,
  },
];

interface ISubscriptionMappingProps extends ICommonProps {
  xlsxData: any;
  sites: ISite[];
  fetchSites: (customerId: number) => any;
  customerId: number;
  addSite: (customerId: number, newSite: any) => any;
  batchCreateImportSubscription: (deviceInfo: any) => any;
  deviceSuccess: IDeviceCreateRes;
  customersShort: ICustomerShort[];
  countries: any;
  fetchCountriesAndStates: any;
  isPostingBatch: boolean;
  manufacturers: any[];
  fetchSubscriptions: any;
  fetchDateFormat: any;
  dateFormats: any;
  xlsxFile: any;
  uploadDeviceFile: (fileReq: any, name: string) => Promise<any>;
  addErrorMessage: any;
  fetchTaskStatus: any;
  addSuccessMessage: any;
  fetchStatuses: any;
  statusList: any[];
}
interface ISubscriptionMappingState {
  searchQueryField: any;
  searchQueryCustomer: any;
  checkboxList: any;
  rows: any[];
  isCustomerMappingOpen: boolean;
  xlsxData: any;
  columns: any[];
  deviceList: any;
  customerList: any[];
  deviceTypes: any[];
  statusList: any[];
  statusMapped: any[];
  statusErrors: any[];
  statusInclude: any[];
  minStatusError: string;
  sites: any[];
  customerId?: number;
  isCreateSiteModal: boolean;
  deviceSuccess: IDeviceCreateRes;
  isPostingSite: boolean;
  isopenConfirm: boolean;
  errors: string[];
  manufacturer_id: number;
  error: {
    manufacturer_id: IFieldValidation;
  };
  isSuccess: boolean;
  dateFormat: any;
  isopenFilterPrototype: boolean;
  taskId: any;
  showCustomerMappingError: boolean;
  uploading: boolean;
}

class FieldMapping extends React.Component<
  ISubscriptionMappingProps,
  ISubscriptionMappingState
> {
  constructor(props: ISubscriptionMappingProps) {
    super(props);

    this.state = {
      rows: [],
      searchQueryField: [],
      searchQueryCustomer: [],
      checkboxList: [],
      dateFormat: [],
      isCustomerMappingOpen: false,
      isCreateSiteModal: false,
      xlsxData: null,
      columns: [],
      deviceList: [],
      statusList: [],
      statusMapped: [],
      statusInclude: [],
      customerList: [],
      deviceTypes: [],
      sites: [],
      deviceSuccess: {
        total_request_items_count: 0,
        successfully_updated_items_count: 0,
        successfully_created_items_count: 0,
        failed_updated_items_count: 0,
        failed_created_items_count: 0,
        import_warnings_file_info: {
          file_name: '',
          file_path: '',
        },
      },
      isPostingSite: false,
      isopenConfirm: false,
      isopenFilterPrototype: false,
      manufacturer_id: null,
      errors: ['', '', '', '', '', '', '', '', '', ''],
      error: {
        manufacturer_id: {
          errorState: IValidationState.SUCCESS,
          errorMessage: '',
        },
      },
      isSuccess: false,
      taskId: null,
      showCustomerMappingError: false,
      statusErrors: [],
      minStatusError: '',
      uploading: false,
    };
  }

  componentDidMount() {
    this.props.fetchCountriesAndStates();
    this.props.fetchDateFormat();
    if (this.props.statusList.length === 0) {
      this.props.fetchStatuses();
    }

    if (!this.props.xlsxFile) {
      this.props.history.push('/subscription-management');
    }
    if (this.props.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
  }

  // check in future for getDerivedStateFromProps
  componentDidUpdate(prevProps: ISubscriptionMappingProps) {
    if (this.props.deviceSuccess && prevProps.deviceSuccess !== this.props.deviceSuccess) {
      this.setState({ deviceSuccess: this.props.deviceSuccess });
    }
  }

  setColumns = (nextProps: ISubscriptionMappingProps) => {
    const xlsxDataResponce = nextProps.xlsxData;
    const columns = [];
    let index = 0;
    let tempKey = '';
    for (const key in xlsxDataResponce[0]) {
      if (xlsxDataResponce[0].hasOwnProperty(key)) {
        if (key.includes('EMPTY')) {
          tempKey = `column_${index + 1}`;
        }
        columns.push({ key: tempKey !== '' ? tempKey : key, value: key });
        index++;
        tempKey = '';
      }
    }
    this.setState({ columns });
  };

  setcustomerList = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const customerList = [];
    xlsxDataResponce.map(data => {
      customerList.push(data[this.state.searchQueryField[10]]);
    });
    const onlyUniqueCustomer = customerList.filter(this.onlyUnique);
    if (
      onlyUniqueCustomer &&
      onlyUniqueCustomer.length > 0 &&
      onlyUniqueCustomer[onlyUniqueCustomer.length - 1].trim() === ''
    ) {
      onlyUniqueCustomer.pop();
    }
    const searchQueryCustomer = [];
    onlyUniqueCustomer.map((c, index) => {
      const isPresent = this.props.customersShort.filter(
        customer => customer.name.toUpperCase() === c
      );
      if (isPresent.length > 0) {
        searchQueryCustomer[index] = c;
      }
    });

    this.setState({
      customerList: onlyUniqueCustomer,
      searchQueryCustomer,
    });
  };

  setStatusList = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const statusList = [];
    xlsxDataResponce.map(data => {
      statusList.push(data[this.state.searchQueryField[11]]);
    });
    const onlyUniquestatus = statusList.filter(this.onlyUnique);
    if (
      onlyUniquestatus &&
      onlyUniquestatus.length > 0 &&
      onlyUniquestatus[onlyUniquestatus.length - 1].trim() === ''
    ) {
      onlyUniquestatus.pop();
    }
    const statusMapped = [];
    const statusInclude = [];
    onlyUniquestatus.map((c, index) => {
      const isPresent = this.props.customersShort.filter(
        customer => customer.name.toUpperCase() === c
      );
      if (isPresent.length > 0) {
        statusMapped[index] = c;
      }
      statusInclude[index] = true;
    });

    this.setState({
      statusList: onlyUniquestatus,
      statusInclude,
      statusMapped,
    });
  };

  onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  toggleCustomerMapping = () => {
    this.setState(prevState => ({
      isCustomerMappingOpen: !prevState.isCustomerMappingOpen,
    }));
  };

  getFieldsOptions = () => {
    if (this.state.columns.length > 0) {
      return this.state.columns.map(role => ({
        value: role.value,
        label: role.key,
      }));
    } else {
      return [];
    }
  };

  getDateFormatOptions = () => {
    const formats = [];
    if (this.props.dateFormats) {
      Object.keys(this.props.dateFormats).map(key => {
        formats.push({
          value: this.props.dateFormats[key],
          label: key,
        });
      });

      return formats;
    } else {
      return [];
    }
  };

  getOptions = () => {
    if (this.props.customersShort && this.props.customersShort.length > 0) {
      return this.props.customersShort.map(site => ({
        value: site.name.toUpperCase(),
        label: site.name,
      }));
    } else {
      return [];
    }
  };

  getStatusOptions = () => {
    if (this.props.statusList && this.props.statusList.length > 0) {
      return this.props.statusList.map(site => ({
        value: site.description.toUpperCase(),
        label: site.description,
      }));
    } else {
      return [];
    }
  };

  onClickCancel = () => {
    this.props.history.push('/subscription-management');
  };

  handleChangeField = (event: any, index?: any) => {
    const searchQueryField = this.state.searchQueryField;
    searchQueryField[index] = event.target.value;

    this.setState({
      searchQueryField,
    });
    if (event.target.name === 'status') {
      this.setStatusList();
    }
  };

  handleChangeDateFormat = (event: any, index?: any) => {
    const dateFormat = this.state.dateFormat;
    dateFormat[index] = event.target.value;

    this.setState({
      dateFormat,
    });
  };

  handleChangeCustomer = (event: any, index?: any) => {
    const searchQueryCustomer = this.state.searchQueryCustomer;
    searchQueryCustomer[index] = event.target.value;

    this.setState({
      searchQueryCustomer,
    });
  };

  handleChangeStatus = (event: any, index?: any) => {
    const statusMapped = this.state.statusMapped;
    statusMapped[index] = event.target.value;

    this.setState({
      statusMapped,
    });
  };

  onClickImport = (event: any, index?: any) => {
    if (this.validateForm()) {
      this.setcustomerList();
      this.toggleCustomerMapping();
    }
  };

  validateForm = () => {
    let isValid = true;
    const newState = cloneDeep(this.state);
    fields.map((field, index) => {
      newState.errors[index] = '';
    });
    (newState.minStatusError as string) = '';
    newState.error.manufacturer_id.errorState = IValidationState.SUCCESS;
    newState.error.manufacturer_id.errorMessage = '';
    fields.map((field, index) => {
      if (field.isRequired && !this.state.searchQueryField[index]) {
        newState.errors[index] = `${field.label} is required`;
        isValid = false;
      }
      if (
        this.state.searchQueryField[index] &&
        field.isDate &&
        !this.state.dateFormat[index]
      ) {
        newState.errors[index] = `Please select Date format`;
        isValid = false;
      }
    });

    if (!this.state.manufacturer_id) {
      newState.error.manufacturer_id.errorState = IValidationState.ERROR;
      newState.error.manufacturer_id.errorMessage =
        'Please select service provider ';
      isValid = false;
    }

    if (newState.searchQueryField[11]) {
      let isStatus = false;
      newState.statusList.map((field, index) => {
        newState.statusErrors[index] = ``;
      });
      newState.statusList.map((field, index) => {
        if (
          this.state.statusInclude[index] &&
          !this.state.statusMapped[index]
        ) {
          newState.statusErrors[index] = `Required`;
          isValid = false;
        }
        if (this.state.statusInclude[index]) {
          isStatus = true;
        }
      });
      if (!isStatus) {
        isValid = false;
        // tslint:disable-next-line: max-line-length
        (newState.minStatusError as string) = 'Please include at least one status';
      }
    }

    this.setState(newState);

    return isValid;
  };

  validateCustomerMapping = () => {
    let isValid = true;
    this.setState({ showCustomerMappingError: false });

    for (const value of this.state.searchQueryCustomer) {
      if (!value) {
        isValid = false;
        this.setState({ showCustomerMappingError: true });
      }
    }

    return isValid;
  };

  getCustomerId = (name: string) => {
    const customerData = this.props.customersShort.filter(
      customer => customer.name.toUpperCase() === name
    );

    return customerData[0].id;
  };

  getStatusId = (name: string) => {
    const statusData = this.props.statusList.filter(
      customer => customer.description.toUpperCase() === name
    );

    return statusData[0].id;
  };
  onClickImportSave = (event: any, index?: any) => {
    const customerMappingObject = {};

    if (this.validateCustomerMapping()) {
      this.setState({ uploading: true, showCustomerMappingError: false });

      this.state.searchQueryCustomer.map((name, i) => {
        customerMappingObject[this.state.customerList[i]] = this.getCustomerId(
          name
        );
      });
      const fieldMappings = {};

      fields.map((field, j) => {
        fieldMappings[field.value] = this.state.searchQueryField[j];
        if (this.state.searchQueryField[j] && this.state.dateFormat[j]) {
          fieldMappings[`${field.value}_format`] = this.state.dateFormat[j];
        }
      });
      const statusMappings = {};
      this.state.statusInclude.map((is, j) => {
        if (is) {
          statusMappings[this.state.statusList[j]] = this.getStatusId(
            this.state.statusMapped[j]
          );
        }
      });
      const deviceInfo = {
        file_name: this.props.xlsxFile && this.props.xlsxFile.name,
        field_mappings: fieldMappings,
        status_mapping: statusMappings,
        manufacturer_id: this.state.manufacturer_id,
        customer_name_mapping: customerMappingObject
          ? customerMappingObject
          : null,
      };

      if (this.props.xlsxFile) {
        const data = new FormData();
        data.append('filename', this.props.xlsxFile);
        this.props
          .uploadDeviceFile(
            data,
            this.props.xlsxFile && this.props.xlsxFile.name
          )
          .then(action => {
            if (action.type === UPLOAD_DEVICE_FILE_SUCCESS) {
              deviceInfo.file_name = action.response;
              this.props.batchCreateImportSubscription(deviceInfo).then(a => {
                if (a.type === BATCH_CREATE_SUCCESS) {
                  if (a.response && a.response.task_id) {
                    this.setState({
                      uploading: false,
                      taskId: a.response.task_id,
                    });
                    this.fetchTaskStatus();
                  }
                }
              });
            } else {
              this.setState({ uploading: false });
            }
          });
      }
    }
  };

  fetchTaskStatus = () => {
    if (this.state.taskId) {
      this.props.fetchTaskStatus(this.state.taskId).then(a => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage('Bulk import completed.');
            if (this.props.customerId) {
              this.props.fetchSubscriptions(this.props.customerId, false);
            }
            this.setState({ isSuccess: true, taskId: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage('Bulk import failed.');
            this.setState({ isSuccess: true, taskId: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatus();
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          if (this.props.customerId) {
            this.props.fetchSubscriptions(this.props.customerId, false);
          }
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState(prevState => ({
      [targetName]: targetValue,
    }));
  };

  getFieldMapping = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(manufacturer => ({
          value: manufacturer.id,
          label: manufacturer.label,
        }))
      : [];

    return (
      <div className="field-mapping  subscription-mapping-import">
        <h3>Subscription Field Mapping</h3>
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-row" />
            <tr className="field-mapping__table-row">
              <td className="first-manufacturer">
                Service Provider
                <span className="field-mapping__label-required" />
                {this.state.error.manufacturer_id.errorMessage ? (
                  <span className="field-mapping__label-error">
                    {this.state.error.manufacturer_id.errorMessage}
                  </span>
                ) : null}
              </td>
              <td className="middle-manufacturer">
                <img src="/assets/new-icons/oppositearrow.svg" />
              </td>
              <td className="sheet-column">
                {
                  <SelectInput
                    name="manufacturer_id"
                    value={this.state.manufacturer_id}
                    onChange={this.handleChange}
                    options={manufacturers}
                    searchable={true}
                    placeholder="Select Service Provider"
                    clearable={false}
                  />
                }
              </td>
              <td className="last" />
              <th className="" />
            </tr>
          </tbody>
        </table>
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-row" />
            <tr className="field-mapping__table-header">
              <th className="first">Map Field</th>
              <th className="middle" />
              <th className="last"> Sheet Columns </th>
              <th className="" />
            </tr>

            {fields.map((field, index) => (
              <tr key={index} className="field-mapping__table-row">
                {field.isCheckBox && (
                  <td className="first">
                    <Checkbox
                      isChecked={this.state.checkboxList[index]}
                      name="option"
                      onChange={e => this.onChecboxChanged(e, index)}
                    >
                      {field.header}
                    </Checkbox>
                  </td>
                )}

                <td className="first">
                  {((field.isCheckBox && this.state.checkboxList[index]) ||
                    !field.isCheckBox) &&
                    field.label}

                  {field.message ? (
                    <div className="field-mapping__label-message">
                      {field.message}
                    </div>
                  ) : null}
                  {field.isRequired && (
                    <span className="field-mapping__label-required" />
                  )}
                  {field.isCheckBox && this.state.checkboxList[index] && (
                    <img
                      className="opposite-arrow"
                      src="/assets/new-icons/oppositearrow.svg"
                    />
                  )}
                  {field.message ? (
                    <div className="field-mapping__label-message">
                      {field.message}
                    </div>
                  ) : null}
                  {this.state.errors[index] ? (
                    <span className="field-mapping__label-error">
                      {this.state.errors[index]}
                    </span>
                  ) : null}
                </td>
                {!field.isCheckBox && (
                  <td className="middle">
                    <img src="/assets/new-icons/oppositearrow.svg" />
                  </td>
                )}
                {((field.isCheckBox && this.state.checkboxList[index]) ||
                  !field.isCheckBox) && (
                  <td className="sheet-column">
                    <SelectInput
                      name={field.value}
                      value={this.state.searchQueryField[index]}
                      onChange={event => this.handleChangeField(event, index)}
                      options={this.getFieldsOptions()}
                      searchable={true}
                      placeholder="Select field"
                      clearable={false}
                    />
                  </td>
                )}
                {field.isDate && (
                  <td className="last">
                    <SelectInput
                      name="select"
                      value={this.state.dateFormat[index]}
                      onChange={event =>
                        this.handleChangeDateFormat(event, index)
                      }
                      options={this.getDateFormatOptions()}
                      searchable={true}
                      placeholder="Select Date Format"
                      clearable={false}
                      disabled={!this.state.searchQueryField[index]}
                    />
                  </td>
                )}

                <th className="" />
                <th className="" />
                <th className="" />
              </tr>
            ))}
          </tbody>
        </table>
        {this.state.statusList && this.state.statusList.length > 0 && (
          <table className="field-mapping__table">
            <tbody>
              <tr className="field-mapping__table-row" />
              <tr className="field-mapping__table-header">
                <th className="first">Status from Sheet</th>
                <th className="middle" />
                <th className="last">Status to Map</th>
                <th className="include">Include in Import</th>
                <th className="" />
              </tr>

              {this.state.statusList &&
                this.state.statusList.map((field, index) => (
                  <tr key={index} className="field-mapping__table-row">
                    <td className="first">
                      {field}
                      {this.state.statusErrors[index] ? (
                        <span className="field-mapping__label-error">
                          {this.state.statusErrors[index]}
                        </span>
                      ) : null}
                    </td>
                    <td className="middle">
                      <img src="/assets/new-icons/oppositearrow.svg" />
                    </td>
                    <td className="last">
                      {
                        <SelectInput
                          name="select"
                          value={this.state.statusMapped[index]}
                          onChange={event =>
                            this.handleChangeStatus(event, index)
                          }
                          options={this.getStatusOptions()}
                          searchable={true}
                          placeholder="Select Status"
                          clearable={false}
                          disabled={!this.state.statusInclude[index]}
                        />
                      }
                    </td>
                    <td className="include">
                      {
                        <Checkbox
                          isChecked={this.state.statusInclude[index]}
                          name="option"
                          onChange={e =>
                            this.onChecboxChangedStatusInclude(e, index)
                          }
                        >
                          {field.header}
                        </Checkbox>
                      }
                    </td>

                    <th className="" />
                  </tr>
                ))}
            </tbody>
          </table>
        )}
        {this.state.statusList && this.state.minStatusError && (
          <div className="error-customer_mapping">
            {this.state.minStatusError}
          </div>
        )}
        <div className="field-mapping__footer">
          <SquareButton
            onClick={this.onClickCancel}
            content="Cancel"
            bsStyle={ButtonStyle.DEFAULT}
            className="import-button-1"
          />
          <SquareButton
            onClick={this.onClickImport}
            content="Next"
            bsStyle={ButtonStyle.PRIMARY}
            className="import-button-1"
          />
        </div>
      </div>
    );
  };

  onChecboxChanged = (event: any, index?: any) => {
    const checkboxList = this.state.checkboxList;
    const searchQueryField = this.state.searchQueryField;
    let deviceTypes = this.state.deviceTypes;
    if (index === 9) {
      deviceTypes = [];
    }
    if (index === 8) {
      deviceTypes = deviceTypes.map((item, i) => {
        if (item.include) {
          item.ignore_zero_dollar_items = event.target.checked;
        }

        return item;
      });
    }
    checkboxList[index] = event.target.checked;

    searchQueryField[index] = '';
    const errors = this.state.errors;
    errors[8] = '';
    errors[9] = '';
    this.setState({
      checkboxList,
      searchQueryField,
      errors,
      deviceTypes,
    });
  };

  onChecboxChangedStatusInclude = (event: any, index?: any) => {
    const statusInclude = this.state.statusInclude;
    statusInclude[index] = event.target.checked;
    this.setState({
      statusInclude,
    });
  };

  toggleConfirmOpen = () => {
    this.setState(prevState => ({
      isopenConfirm: !prevState.isopenConfirm,
    }));
  };

  getCustomerMapping = () => {
    return (
      <div className="field-mapping customer-mapping">
        <h3>Customer Mapping</h3>
        {this.props.isPostingBatch ? (
          <Spinner show={true} />
        ) : (
          <div>
            <table className="field-mapping__table">
              <tbody>
                <tr className="field-mapping__table-row" />
                <tr className="field-mapping__table-header">
                  <th className="first">Customers from Sheet</th>
                  <th className="middle" />
                  <th className="last">Customer to Map</th>
                  <th className="" />
                </tr>

                {this.state.customerList &&
                  this.state.customerList.map((field, index) => (
                    <tr key={index} className="field-mapping__table-row">
                      <td className="first">{field}</td>
                      <td className="middle">
                        <img src="/assets/new-icons/oppositearrow.svg" />
                      </td>
                      <td className="last">
                        {
                          <SelectInput
                            name="select"
                            value={this.state.searchQueryCustomer[index]}
                            onChange={event =>
                              this.handleChangeCustomer(event, index)
                            }
                            options={this.getOptions()}
                            searchable={true}
                            placeholder="Select Customer"
                            clearable={false}
                          />
                        }
                      </td>
                      <th className="" />
                    </tr>
                  ))}
              </tbody>
            </table>
            {this.state.showCustomerMappingError && (
              <div className="error-customer_mapping">
                Please select all customer
              </div>
            )}

            <div className="field-mapping__footer">
              <SquareButton
                onClick={this.toggleCustomerMapping}
                content="Back"
                bsStyle={ButtonStyle.DEFAULT}
                className="import-button-1"
              />
              <SquareButton
                onClick={this.onClickImportSave}
                content="Import"
                bsStyle={ButtonStyle.PRIMARY}
                className="import-button-1"
              />
            </div>
          </div>
        )}
      </div>
    );
  };

  downloadReport = response => {
    if (response && response !== '') {
      const url = response.file_path;
      const link = document.createElement('a');
      link.href = url;
      link.target = '_blank';
      link.setAttribute('download', response.file_name);
      document.body.appendChild(link);
      link.click();
    }
  };

  render() {
    return (
      <div className="mapping">
        {!this.state.isCustomerMappingOpen && this.getFieldMapping()}
        {this.state.isCustomerMappingOpen &&
          !this.props.isPostingBatch &&
          this.state.deviceSuccess.total_request_items_count === 0 &&
          !this.state.isSuccess &&
          this.getCustomerMapping()}
        {this.props.isPostingBatch || this.state.uploading ? (
          <Spinner show={true} />
        ) : null}
        {(this.state.isSuccess ||
          (this.state.deviceSuccess.import_warnings &&
            this.state.deviceSuccess.import_warnings.length > 0) ||
          this.state.deviceSuccess.failed_created_items_count !== 0 ||
          this.state.deviceSuccess.failed_updated_items_count !== 0 ||
          this.state.deviceSuccess.successfully_updated_items_count !== 0 ||
          this.state.deviceSuccess.successfully_created_items_count !== 0 ||
          this.state.deviceSuccess.total_request_items_count !== 0) && (
          <div className="device-responce">
            <h3>Status :</h3>
            <div className="row ">
              Total subscriptions :
              <span>
                {' '}
                {this.state.deviceSuccess.total_request_items_count || 0}
              </span>
            </div>
            <div className="row success">
              Subscription created :
              <span>
                {this.state.deviceSuccess.successfully_created_items_count || 0}
              </span>
            </div>
            <div className="row success">
              Updated subscription :
              <span>
                {this.state.deviceSuccess.successfully_updated_items_count || 0}
              </span>
            </div>
            <div className="row error">
              Failed to create subscription :
              <span>
                {this.state.deviceSuccess.failed_created_items_count || 0}
              </span>
            </div>
            <div className="row error">
              Failed to update subscription :
              <span>
                {this.state.deviceSuccess.failed_updated_items_count || 0}
              </span>
            </div>
            {this.state.deviceSuccess.import_warnings &&
              this.state.deviceSuccess.import_warnings.length > 0 && (
                <div className="row error warning-list">
                  Warnings :
                  {this.state.deviceSuccess.import_warnings.map(
                    (data, index) => (
                      <div key={index} className="warning-text">
                        {index + 1}. {data}
                      </div>
                    )
                  )}
                </div>
              )}
            {this.state.deviceSuccess.import_warnings_file_info &&
              this.state.deviceSuccess.import_warnings_file_info.file_path && (
                <div className="row error warning-list">
                  Download error report :{' '}
                  <img
                    onClick={e =>
                      this.downloadReport(
                        this.state.deviceSuccess.import_warnings_file_info
                      )
                    }
                    alt=""
                    className="import-error-btn"
                    src="/assets/icons/download.png"
                  />
                </div>
              )}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  xlsxData: state.inventory.xlsxData,
  sites: state.inventory.sites,
  customerId: state.customer.customerId,
  deviceSuccess: state.inventory.deviceSuccess,
  customersShort: state.customer.customersShort,
  countries: state.inventory.countries,
  isPostingBatch: state.inventory.isPostingBatch,
  manufacturers: state.inventory.manufacturers,
  dateFormats: state.inventory.dateFormats,
  xlsxFile: state.inventory.xlsxFile,
  statusList: state.inventory.statusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  addSite: (customerId: number, newSite: any) =>
    dispatch(addSite(customerId, newSite)),
  batchCreateImportSubscription: (devices: any[]) =>
    dispatch(batchCreateImportSubscription(devices)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
  fetchSubscriptions: (id: number, show: boolean) =>
    dispatch(fetchSubscriptions(id)),
  fetchDateFormat: () => dispatch(fetchDateFormat()),
  uploadDeviceFile: (fileReq: any, name: string) =>
    dispatch(uploadDeviceFile(fileReq, name)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  fetchStatuses: () => dispatch(fetchStatuses()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FieldMapping);
