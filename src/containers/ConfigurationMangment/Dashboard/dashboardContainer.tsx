import React from 'react';
import { Doughnut, HorizontalBar } from 'react-chartjs-2';
import 'chartjs-plugin-datalabels';
import { connect } from 'react-redux';
import {
  downloadDeviceReportSingle,
  downloadCustomerDeviceReportSingle,
  fetchComplianceDashboardData,
  fetchCustomerComplianceDashboardData,
  fetchClassifications,
  fetchRulesLevels,
} from '../../../actions/configuration';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import {
  TASK_STATUS_SUCCESS,
  TASK_STATUS_FAILURE,
  fetchTaskStatus,
} from '../../../actions/inventory';
import { RERUN_COMPLIANCE_SUCCESS, rerunConfigCompliance } from '../../../actions/collector';
import DeleteButton from '../../../components/Button/deleteButton';
import Input from '../../../components/Input/input';

import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import { allowPermission } from '../../../utils/permissions';
import { searchInFields } from '../../../utils/searchListUtils';

import './style.scss';
import ViewConfig from './view';
import SquareButton from '../../../components/Button/button';
import RulesFilter from './filter';
import _, { cloneDeep } from 'lodash';
import { fetchTypes } from '../../../actions/inventory';
import SelectInput from '../../../components/Input/Select/select';
import '../../../commonStyles/filtersListing.scss';

interface IDashboardProps extends ICommonProps {
  fetchComplianceDashboardData: any;
  isFetchingComplianceDashboardData: boolean;
  complianceDashboardData: {} | any;
  customerId: number;
  user: ISuperUser;
  downloadDeviceReportSingle: any;
  downloadCustomerDeviceReportSingle: any;
  fetchCustomerComplianceDashboardData: any;
  fetchClassifications: any;
  fetchRulesLevels: any;
  classifications: any;
  levels: any;
  types: any;
  fetchTypes: any;
  rerunConfigCompliance: any;
  fetchTaskStatus: any;
  addErrorMessage: any;
  addSuccessMessage: any;
  isPostingBatch: boolean;
}

interface IDashboardState {
  isFilterModalOpen: boolean;
  rows: IRule[];
  selectedRows: number[];
  searchString: string,
  isViewModalOpen: boolean;
  rule: IRule;
  id: any;
  filters: IRulesFilter;
  showComplianceRerunBtn: boolean;
  complianceRunTaskId: null;
  isSuccess: boolean;
}

class DashBoard extends React.Component<IDashboardProps, IDashboardState> {
  constructor(props: IDashboardProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  priorityColorMap = {
    medium: '#F0B65C',
    high: '#9E3D9A',
    low: '#7F7FB1'
  };

  ruleLevelMap = {
    HIGH: 3,
    MEDIUM: 2,
    LOW: 1
  }

  getEmptyState = () => ({
    isFilterModalOpen: false,
    isViewModalOpen: false,
    rows: [],
    selectedRows: [],
    downloadingPDFIds: [],
    searchString: '',
    rule: null,
    id: null,
    filters: {
      level: [],
      classification: [],
      applies_to: [],
      status: [],
      is_global: [],
      is_enabled: [],
    },
    showComplianceRerunBtn: false,
    complianceRunTaskId: null,
    isSuccess: false
  });
  componentDidMount() {
    const { customerId } = this.props;

    if (customerId) {
      this.props.fetchComplianceDashboardData(customerId);
    } else if(_.get(this.props, 'user.type') === 'customer') {
      this.props.fetchCustomerComplianceDashboardData();
    }
    if (this.props.levels.length === 0) {
      this.props.fetchRulesLevels();
    }
    if (this.props.classifications.length === 0) {
      this.props.fetchClassifications(_.get(this.props, 'user.type'));
    }
    if (this.props.types === null) {
      this.props.fetchTypes();
    }
  }

  componentDidUpdate(prevProps: IDashboardProps) {
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchComplianceDashboardData(this.props.customerId);
    }

    if (
      !this.props.customerId && this.props.user &&
      this.props.user !== prevProps.user &&
      _.get(this.props, 'user.type') === 'customer'
    ) {
      this.props.fetchCustomerComplianceDashboardData();
    }

    if (this.props.complianceDashboardData !== prevProps.complianceDashboardData) {
      const rows = this.getRows(this.props, '', '');
      this.setState({
        rows,
      });
    }
  }

  getTotal = (array: number[]) => {
    let total = 0;
    total = array.reduce((a, b) => (a || 0) + (b || 0));

    return total ? total : null;
  };

  getRows = (
    nextProps: IDashboardProps,
    searchString?: string,
    filters?: any
  ) => {
    let rules: any[] = nextProps.complianceDashboardData.rules && Object.values(nextProps.complianceDashboardData.rules);
    const search = searchString ? searchString : this.state.searchString;
    if (filters) {
      // Add filter for type once included.
      const filterLevel = filters.level;
      const filterClassification = filters.classification;
      const filterTypes =
        filters.applies_to &&
        filters.applies_to.map(item => {
          return item === 'N.A.' ? '' : item;
        }) || [];
      rules =
        rules &&
        rules.filter(config => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
   
          const hasFilteredType =
            filterTypes.length > 0
              ? filterTypes.some(v =>
                config.applies_to.includes(v))
              : true;

          const hasFilteredLevel =
            filterLevel.length > 0
              ? filterLevel.includes(config.level)
              : true;
          const hasFilteredfilterClassification =
            filterClassification.length > 0
              ? filterClassification.includes(config.classification_name)
              : true;



          return (
            hasFilteredType &&
            hasFilteredLevel &&
            hasFilteredfilterClassification
          );
        });
    }
    if (search && search.length > 0) {
      rules =
        rules &&
        rules.filter(
          row =>
            searchInFields(row, search, [
              'name',
            ])
        );
    }
    const rows: IRule[] =
      rules &&
      rules.map((rule, index) => ({
        classification: rule.classification,
        classification_name: rule.classification_name,
        customers_assigned: rule.customers_assigned,
        detail: rule.detail,
        level: rule.level,
        updated_on: rule.updated_on,
        created_on: rule.created_on,
        name: rule.name,
        description: rule.description,
        applies_to: rule.applies_to,
        applies_to_manufacturers: rule.applies_to_manufacturers,
        is_assigned: rule.is_assigned,
        is_enabled: rule.is_enabled,
        is_global: rule.is_global,
        id: rule.id,
        function: rule.function,
        violations_count: rule.violations_count,
        deivces_list: rule.deivces_list,
        level_int: this.ruleLevelMap[rule.level],
        index,
      }));

    return rows;
  };

  toggleFilterModal = () => {
    this.setState(prevState => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  levelList = () => {
    const levelList = this.props.levels
      ? this.props.levels.map(c => ({
        value: c,
        label: c,
        disabled: false,
      }))
      : [];

    return levelList;
  };

  typeList = () => {
    const types = this.props.types
      ? this.props.types.map(c => ({
        value: c,
        label: c ? c : 'N.A.',
        disabled: false,
      }))
      : [];

    return types;
  };

  classificationList = () => {
    const classification =
      this.props.classifications.length > 0
        ? this.props.classifications.map(c => ({
          value: c.name,
          label: c.name,
          disabled: false,
        }))
        : [];

    return classification;
  };
  onFiltersUpdate = (filters: IConfigurationFilters) => {
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState({
      rows,
      filters,
      isFilterModalOpen: false,
    });
  };
  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    const newState = cloneDeep(this.state);
    newState.filters[targetName] = targetValue;
    this.setState(newState);
    this.onFiltersUpdate(newState.filters);
  };

  onClickReRunCompliance = (devices: number[]) => {
    const { selectedRows } = this.state;

    const data = {
      customer_id: this.props.customerId,
      rule_ids:[],
      device_ids:[],
    };
    if(devices && devices.length > 0){
      data.device_ids = devices
    }else{
      data.rule_ids = selectedRows
    }

    this.props
      .rerunConfigCompliance(data)
      .then(a => {
        if (a.type === RERUN_COMPLIANCE_SUCCESS) {
          if (a.response && a.response.task_id) {
            this.setState({
              complianceRunTaskId: a.response.task_id,
            });
            this.fetchTaskStatus(a.response.task_id, undefined, 'Re-run compliance ', 'ComplianceRuntaskId');
          }
        }
      });
  };

  fetchTaskStatus = (taskId, list, message: string, idInState) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then(a => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            // list();
            if (this.props.customerId) {
              this.props.fetchComplianceDashboardData(this.props.customerId);
            } else if(_.get(this.props, 'user.type') === 'customer') {
              this.props.fetchCustomerComplianceDashboardData();
            }
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatus(taskId, list, message, idInState);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters = filters
      ? (filters.applies_to && filters.applies_to.length > 0) ||
      (filters.classification && filters.classification.length > 0) ||
      (filters.level && filters.level.length > 0) : false;

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.applies_to && filters.applies_to.length > 0 && (
          <div className="section-show-filters">
            <label>Type: </label>
            <SelectInput
              name="applies_to"
              value={filters.applies_to}
              onChange={this.onFilterChange}
              options={this.typeList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.level.length > 0 && (
          <div className="section-show-filters">
            <label>Level: </label>
            <SelectInput
              name="level"
              value={filters.level}
              onChange={this.onFilterChange}
              options={this.levelList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.classification.length > 0 && (
          <div className="section-show-filters">
            <label>Classification: </label>
            <SelectInput
              name="classification"
              value={filters.classification}
              onChange={this.onFilterChange}
              options={this.classificationList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };
  renderTopBar = () => {
    return (
      <div
        className={
          this.props.customerId ||
            (this.props.user &&
              this.props.user.type &&
              this.props.user.type === 'customer')
            ? 'dashboard-rules-listing__table-top '
            : 'dashboard-rules-listing__table-top '
        }
      >
        <div className="dashboard-rules-listing__table-actions header-panel">
          <div
            className={
              this.props.customerId ||
                (this.props.user &&
                  this.props.user.type &&
                  this.props.user.type === 'customer')
                ? 'dashboard-rules-listing__table-header-search'
                : 'dashboard-rules-listing__table-header-search disable-div'
            }
          >

            {allowPermission('search_device') && (
              <Input
                field={{
                  label: '',
                  type: InputFieldType.SEARCH,
                  value: this.state.searchString,
                  isRequired: false,
                }}
                width={12}
                placeholder="Search"
                name="searchString"
                onChange={this.handleChange}
                className="dashboard-rules-listing__search search"
              />
            )}
          </div>
          <div className="heading-center"> Rules</div>
          <div className="btn-group-left">
            { this.state.showComplianceRerunBtn && (
                <SquareButton
                  onClick={() =>this.onClickReRunCompliance([])}
                  content={
                    <span>
                      <img alt="" src="/assets/icons/reload.png" />
                        &nbsp; Re-Run Compliance
                    </span>
                  }
                  bsStyle={ButtonStyle.PRIMARY}
                  className={`btn-img ${this.props.isPostingBatch ? 'rotate-img-running' : ''}`}
                />
            )}
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };
  handleChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };
  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.searchString,
    );

    this.setState({ rows });
  };

  onRowsToggle = selectedRows => {
    this.setState({
      selectedRows,
      showComplianceRerunBtn: selectedRows.length > 0
    });
  };

  rulesListingDashboard = () => {
    const columns: ITableColumn[] = [
      {
        accessor: 'name',
        Header: 'Rule Name',
        id: 'name',
        sortable: true,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'violations_count',
        Header: 'Violations count',
        sortable: true,
        width: 130,
        id: 'violations_count',
        Cell: c => <div>{c.value} of {c.original.deivces_list.length}</div>
      },
      {
        accessor: 'level_int',
        Header: 'Priority',
        id: 'level_int',
        width: 80,
        sortable: true,
        Cell: cell => (
          <div className="icons-template">
            {cell.original.level && (
              <img
                src={`/assets/icons/l-${cell.original.level}.svg`}
                alt=""
                title={cell.original.level}
                className="status-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'applies_to',
        Header: 'Applies To',
        sortable: true,
        id: 'applies_to',
        Cell: c => <div title={c.value && c.value.join()}>{c.value && c.value.join()}</div>,
      },

      {
        accessor: 'id',
        Header: 'Actions',
        width: 131,
        sortable: false,
        Cell: cell => (
          <div className="icons-template">
            <DeleteButton
              type="pdf_preview"
              title="Preview"
              onClick={e => this.toggleViewDeviceModal(cell.original)}
            />
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };
    return (
      <div style={{marginTop: this.state.rows && this.state.rows.length ? '10px' : '25px'}}  className="rules-listing-dashboard col-md-12">
        <Table
          columns={columns}
          rows={this.state.rows || []}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`rules-listing__table ${this.props.isFetchingComplianceDashboardData ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          loading={this.props.isFetchingComplianceDashboardData}
          defaultSorted={[
            {
              id: "violations_count",
              desc: true
            }
          ]}
        />
        {
          this.state.isViewModalOpen && this.state.rule &&
          <ViewConfig
            show={this.state.isViewModalOpen}
            onClose={this.toggleViewDeviceModalClose}
            rule={this.state.rule}
            downloadDeviceReportSingle={this.props.downloadDeviceReportSingle}
            downloadCustomerDeviceReportSingle={this.props.downloadCustomerDeviceReportSingle}
            complianceDashboardData={this.props.complianceDashboardData}
            customerId={this.props.customerId}
            rerunConfigCompliance={this.onClickReRunCompliance}
            isPostingBatch={this.props.isPostingBatch}
          />
        }
      </div>
    )
  }
  onRowClick = rowInfo => {
    this.setState(() => ({
      isViewModalOpen: true,
      rule: this.state.rows[rowInfo.index].id,
    }));
  };
  toggleViewDeviceModal = (rowInfo) => {
    this.setState(() => ({
      isViewModalOpen: true,
      rule: rowInfo.id,
    }));
  };
  toggleViewDeviceModalClose = () => {
    this.setState(() => ({
      isViewModalOpen: false,
      rule: null,
    }));
  };

  getRuleVilotaionsByPriorityData = () => {
    let labels = [];
    const data = [];

    const {
      rules_violations_by_priority_obj,
    } = this.props.complianceDashboardData;

    if (
      rules_violations_by_priority_obj === undefined ||
      rules_violations_by_priority_obj === null
    ) {
      return {
        labels,
        data,
      };
    }

    labels = ["HIGH", "MEDIUM", "LOW"]

    labels.map(val => data.push(rules_violations_by_priority_obj[val]));

    return {
      labels,
      data,
    };
  };

  getTopDevicesData = () => {
    const labels = [];
    const data = [];
    const high = [];
    const medium = [];
    const low = [];

    const { top_devices_ids, devices } = this.props.complianceDashboardData;

    if (top_devices_ids === undefined || top_devices_ids === null) {
      return {
        labels,
        data,
      };
    }

    const totalViloationArr = [];

    top_devices_ids.map(id => {
      let totalViolationCount = 0;
      labels.push(devices[id].device_name);
      const highCount = devices[id].rules_violations_by_priority.HIGH;
      high.push(highCount);
      totalViolationCount += highCount;

      const mediumCount = devices[id].rules_violations_by_priority.MEDIUM;
      medium.push(mediumCount);
      totalViolationCount += mediumCount;

      const lowCount = devices[id].rules_violations_by_priority.LOW;
      low.push(lowCount);
      totalViolationCount += lowCount;

      totalViloationArr.push(totalViolationCount);
    });

    return {
      labels,
      data,
      high,
      medium,
      low,
      totalViloationArr
    };
  };

  getTopRulesData = () => {
    const labels = [];
    const data = [];

    const { top_rules_ids, rules } = this.props.complianceDashboardData;

    if (top_rules_ids === undefined || top_rules_ids === null) {
      return {
        labels,
        data,
      };
    }

    top_rules_ids.map(id => {
      labels.push(rules[id].name);
      data.push(rules[id].violations_count);
    });

    return {
      labels,
      data,
    };
  };

  render() {
    const rulesViolationsByPriorityData = this.getRuleVilotaionsByPriorityData();

    const topDevicesData = this.getTopDevicesData();

    const topRulesData = this.getTopRulesData();

    const options = {
      maintainAspectRatio: false,
      plugins: {
        datalabels: {
          display: false,
        },
      },
      legend: {
        position: 'bottom',
        labels: {
          boxWidth: 13,
          fontSize: 11,
          boxRadius: 50,
        },
      },
      layout: {
        padding: {
          left: 20,
          right: 20,
          top: 20,
          bottom: 20,
        },
      },
      cutoutPercentage: 80,
    };

    const horizontalBarOptions = {
      ...options,
      layout: {
        padding: {
          right: 25,
          left: 110,
        }
      },
      plugins: {
        datalabels: {
          display: true,
          anchor: 'end',
          align: 'end',
          padding: {
            bottom: 25,
          },
          font: {
            weight: 'bold',
          },
        },
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            barThickness: 3,
            maxBarThickness: 6,
            ticks: {
              display: false,
            },
            scaleLabel: {
              display: false,
            },
            gridLines: {
              display: false,
            },
          },
        ],
        yAxes: [
          {
            gridLines: {
              display: false,
            },
          },
        ],
      },
    };

    const horizontalStackedBarOptions = {
      ...horizontalBarOptions,
      layout: {
        padding: {
          right: 25,
          left: 25,
        }
      },
      plugins: {
        datalabels: {
          display: true,
          clamp: true,
          anchor: 'end',
          align: 'end',
          formatter: (value, context) => {
            if (context.datasetIndex === 2) {
              return topDevicesData.totalViloationArr[context.dataIndex];
            } else {
              return '';
            }
          }
        },
      },
      scales: {
        xAxes: [
          {
            ticks: {
              display: false,
            },
            scaleLabel: {
              display: false,
            },
            gridLines: {
              display: false,
            },
            stacked: true,
          },
        ],
        yAxes: [
          {
            gridLines: {
              display: false,
            },
            stacked: true,
          },
        ],
      },
    };

    const horizontalStackedBarData = {
      labels: topDevicesData.labels,
      datasets: [
        {
          label: 'low',
          data: topDevicesData.low,
          backgroundColor: this.priorityColorMap.low, // green
        },
        {
          label: 'medium',
          data: topDevicesData.medium,
          backgroundColor: this.priorityColorMap.medium, // yellow
        },
        {
          label: 'high',
          data: topDevicesData.high,
          backgroundColor: this.priorityColorMap.high, // red
        },
      ],
    };

    const horizontalBarData = {
      labels: topRulesData.labels,
      datasets: [
        {
          backgroundColor: [
            '#9E3D9A',
            '#9e3d9a8c',
            '#F0B65C',
            '#f0b65c5e',
            '#7F7FB1',
          ],
          data: topRulesData.data,
        },
      ],
    };

    return (
      <div>
        {this.props.user &&
          this.props.user.type &&
          (this.props.user.type === 'provider' ||
            this.props.user.type === 'customer') && (
            <div className="dashboard configuration-dashboard">
              <div className="col-md-4 graph-box">
                <div
                  className={`graph-heading ${this.props.isFetchingComplianceDashboardData
                    ? 'loading'
                    : ''
                    }`}
                >
                  Rule Violations By Priority
                </div>
                <div
                  className={
                    this.props.isFetchingComplianceDashboardData ? 'loader' : ''
                  }
                >
                  <Spinner
                    show={this.props.isFetchingComplianceDashboardData}
                  />
                </div>
                <div
                  className={`dashboard-graph-img ${this.props.isFetchingComplianceDashboardData
                    ? 'loading'
                    : ''
                    }`}
                >
                  {rulesViolationsByPriorityData.data.length > 0 ?
                    <>
                      <Doughnut
                        data={{
                          labels: rulesViolationsByPriorityData.labels,
                          datasets: [
                            {
                              data: rulesViolationsByPriorityData.data,
                              backgroundColor: [this.priorityColorMap.high, this.priorityColorMap.medium, this.priorityColorMap.low],
                              hoverBackgroundColor: [
                                this.priorityColorMap.high,
                                this.priorityColorMap.medium,
                                this.priorityColorMap.low,
                              ],
                              borderWidth: 1,
                            },
                          ],
                        }}
                        options={options}
                      />
                      <div className="donut-inner">
                        <span>{this.getTotal(rulesViolationsByPriorityData.data)}</span>
                      </div>
                    </>
                    :
                    <div className='dashboard-graph-no-data'>NO DATA</div>
                  }
                </div>
              </div>

              <div className="col-md-4 graph-box">
                <div
                  className={`graph-heading  ${this.props.isFetchingComplianceDashboardData
                    ? 'loading'
                    : ''
                    }`}
                >
                  Top 5 Devices
                </div>
                <div
                  className={
                    this.props.isFetchingComplianceDashboardData ? 'loader' : ''
                  }
                >
                  <Spinner
                    show={this.props.isFetchingComplianceDashboardData}
                  />
                </div>
                <div
                  className={`dashboard-graph-img ${this.props.isFetchingComplianceDashboardData
                    ? 'loading'
                    : ''
                    }`}
                >
                  {
                    topDevicesData.labels.length > 0 ?
                      <HorizontalBar
                        data={horizontalStackedBarData}
                        options={horizontalStackedBarOptions}
                      />
                      :
                      <div className='dashboard-graph-no-data'>NO DATA</div>
                  }
                </div>
              </div>
              <div className="col-md-4 graph-box">
                <div
                  className={`graph-heading ${this.props.isFetchingComplianceDashboardData
                    ? 'loading'
                    : ''
                    }`}
                >
                  Top 5 Rules
                </div>
                <div
                  className={
                    this.props.isFetchingComplianceDashboardData ? 'loader' : ''
                  }
                >
                  <Spinner
                    show={this.props.isFetchingComplianceDashboardData}
                  />
                </div>
                <div
                  className={`dashboard-graph-img ${this.props.isFetchingComplianceDashboardData
                    ? 'loading'
                    : ''
                    }`}
                >
                  {
                    topRulesData.data.length > 0 ?
                      <HorizontalBar
                        data={horizontalBarData}
                        options={horizontalBarOptions}
                      />
                      :
                      <div className='dashboard-graph-no-data'>NO DATA</div>
                  }
                </div>
              </div>
            </div>
          )}
        {this.rulesListingDashboard()}
        <RulesFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          levels={this.props.levels}
          classifications={this.props.classifications}
          types={this.props.types}
          prevFilters={this.state.filters}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customerId: state.customer.customerId,
  complianceDashboardData: state.configuration.complianceDashboardData,
  isFetchingComplianceDashboardData:
    state.configuration.isFetchingComplianceDashboardData,
  classifications: state.configuration.classifications,
  levels: state.configuration.levels,
  types: state.inventory.types,
  isPostingBatch: state.inventory.isPostingBatch,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchComplianceDashboardData: (id: any) =>
    dispatch(fetchComplianceDashboardData(id)),
  fetchCustomerComplianceDashboardData: (id: any) =>
    dispatch(fetchCustomerComplianceDashboardData()),
  downloadDeviceReportSingle: (customerID: any, id: number) => dispatch(downloadDeviceReportSingle(customerID, id)),
  downloadCustomerDeviceReportSingle: (id: number) => dispatch(downloadCustomerDeviceReportSingle(id)),
  fetchClassifications: () => dispatch(fetchClassifications()),
  fetchRulesLevels: () => dispatch(fetchRulesLevels()),
  fetchTypes: () => dispatch(fetchTypes()),
  rerunConfigCompliance: (data: any) => dispatch(rerunConfigCompliance(data)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);
