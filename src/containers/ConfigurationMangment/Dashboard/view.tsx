import { ControlledEditor } from '@monaco-editor/react';
import cloneDeep from 'lodash/cloneDeep';
import React from 'react';
import Spinner from '../../../components/Spinner';
import _ from 'lodash';
import RightMenu from '../../../components/RighMenuBase/rightMenuBase';
import Input from '../../../components/Input/input';
import ReactTable from 'react-table';
import IconButton from '../../../components/Button/iconButton';
import Checkbox from '../../../components/Checkbox/checkbox';
import ComplianceResult from './ruleComplianceResult';
import { DOWNLOD_DEVICE_REP_SUCCESS } from '../../../actions/configuration';
import ExpandableRuleComplianceResult from './expandableRuleComplianceResult';
import SquareButton from '../../../components/Button/button';

enum PageType {
  Config,
  Details
}
interface IViewConfigProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  rule: any;
  complianceDashboardData: any;
  downloadDeviceReportSingle: any;
  downloadCustomerDeviceReportSingle: any;
  customerId: any;
  rerunConfigCompliance: any;
  isPostingBatch: any;
}

interface IEditDeviceFormState {
  searchKey: string;
  activeIndex: number;
  lineNo: number;
  currentPage: {
    pageType: PageType;
  };
  rule: IRule;
  devices: any[];
  device: any;
  downloadingPDFIds: any[];
  showOnlyFailedDevices: boolean;
  showComplianceResult: boolean;
  config: any;
  selectedRows: any[];
}

class ViewConfig extends React.Component<
  IViewConfigProps,
  IEditDeviceFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  props: any;
  static _editor;
  constructor(props: IViewConfigProps) {
    super(props);
    this.state = {
      searchKey: '',
      activeIndex: -1,
      lineNo: 0,
      currentPage: {
        pageType: PageType.Details,
      },
      rule: null,
      devices: [],
      device: null,
      downloadingPDFIds: [],
      showOnlyFailedDevices: true,
      showComplianceResult: false,
      config: null,
      selectedRows: []
    };
  }
  componentDidMount() {
    if (this.props.rule) {
      const rule = this.props.complianceDashboardData.rules[this.props.rule]

      const devices = [];

      rule.deivces_list.map(id => {
        const { id: ruleSateId } = rule;
        const deviceObj = this.props.complianceDashboardData.devices[id];

        deviceObj.compliance_statistics.map((deviceRule, index) => {
          const { id: ruleID, is_valid_rule } = deviceRule

          if (ruleSateId === ruleID) {
            if (!is_valid_rule) devices.push(this.props.complianceDashboardData.devices[deviceObj.id]);
          }
        });
      });
      devices.map(x => x.checked = false);
      this.setState({ rule, devices, device: devices.length > 0 && devices[0].id });
    }
  }

  componentDidUpdate(prevProps: IViewConfigProps) {
    if (this.props.rule !== prevProps.rule) {
      const rule = this.props.complianceDashboardData.rules[this.props.rule];
      const devices = [];

      rule.deivces_list.map(id => {
        const { id: ruleSateId } = rule;
        const deviceObj = this.props.complianceDashboardData.devices[id];
        deviceObj.compliance_statistics.map((deviceRule, index) => {
          const { id: ruleID, is_valid_rule } = deviceRule;
          if (ruleSateId === ruleID) {
            if (!is_valid_rule) devices.push(this.props.complianceDashboardData.devices[deviceObj.id]);
          }
        });
      });
      devices.map(x => x.checked = false)
      this.setState({ rule, devices, device: devices.length > 0 && devices[0].id });
    }
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };
  onClose = (event: any) => {
    this.setState({ searchKey: '' });
    this.props.onClose(event);
  };

  handleEditorDidMount = (_valueGetter, editor) => {
    ViewConfig._editor = editor;
    editor.focus();
    editor.getAction('actions.find').run();
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="rule-devices-list__header">
        <div
          className={`rule-devices-list__header-link ${currentPage.pageType === PageType.Details
            ? 'rule-devices-list__header-link--active'
            : ''
            }`}
          onClick={() => this.changePage(PageType.Details)}
        >
          Details
        </div>
        <div
          className={`rule-devices-list__header-link ${currentPage.pageType === PageType.Config
            ? 'rule-devices-list__header-link--active'
            : ''
            }`}
          onClick={() => this.changePage(PageType.Config)}
        >
          Configuration
        </div>
      </div>
    );
  };

  handleChangeCheckBox = (event: any) => {
    const targetValue = event.target.checked;
    if (targetValue) {
      const devices = [];

      this.state.rule.deivces_list.map(id => {
        const { id: ruleSateId } = this.state.rule;
        const deviceObj = this.props.complianceDashboardData.devices[id];

        deviceObj.compliance_statistics.map((deviceRule, index) => {
          const { id: ruleID, is_valid_rule } = deviceRule

          if (ruleSateId === ruleID) {
            if (!is_valid_rule) devices.push(this.props.complianceDashboardData.devices[deviceObj.id]);
          }
        });
      });
      this.setState({ devices, device: devices.length > 0 && devices[0].id, showOnlyFailedDevices: targetValue })
    } else {
      const devices = this.state.rule.deivces_list.map(id => this.props.complianceDashboardData.devices[id]);
      this.setState({ devices, device: devices && devices[0].id, showOnlyFailedDevices: targetValue })
    }

  };

  onComplianceResultClose = () => {
    this.setState(() => ({
      showComplianceResult: false,
    }));
  }

  onRowsToggle = selectedRows => {
    this.setState({
      selectedRows,
    });
  };

  renderDeviceDetails = () => {
    const options = {
      selectOnLineNumbers: false,
      readOnly: true,
    };

    const rule = this.props.complianceDashboardData.rules[this.props.rule];
    const currentPage = this.state.currentPage;

    const getRuleStatus = cell => {
      let ruleStatus = 'FAIL';
      const { id } = this.state.rule;

      cell.original.compliance_statistics &&
        cell.original.compliance_statistics.map((rule, index) => {
          const { id: ruleID, is_valid_rule } = rule

          if (ruleID === id) {
            ruleStatus = is_valid_rule ? 'PASS' : 'FAIL'
          }
        });

      return (<div>{ruleStatus}</div>)
    };

    const getPDFDownloadMarkUp = cell => {
      if (this.state.downloadingPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <div style={{ marginLeft: '5px' }}>
            <IconButton
              icon="direct-download.svg"
              title="Download"
              onClick={e => this.downloadPDFReport(cell.value)}
            />
          </div>
        );
      }
    };

    const columns: ITableColumn[] = [
      {
        id: "checkbox",
        accessor: "",
        Cell: (rowInfo) => {
          return (
            <Checkbox
              className="checkbox "
              isChecked={rowInfo.original.checked}
              onChange={(e) => {
                e.stopPropagation();
                const newState = cloneDeep(this.state);
                const data = this.state.devices
                data[rowInfo.index].checked = e.target.checked;
                (newState.devices as any) = [...data];
                this.setState(newState);
              }}
              name="one"
            />
          );
        },
        Header: title => {
          return (
            <Checkbox
              className="checkbox device-select-row"
              isChecked={this.state.devices.every(x => x.checked === true)}
              onChange={(e) => {
                e.stopPropagation();
                const newState = cloneDeep(this.state);
                const data = this.state.devices
                data.map(x => x.checked = e.target.checked);
                (newState.devices as any) = [...data];
                this.setState(newState);
              }}
              name="all"
            />
          );
        },
        sortable: false,
        width: 30,
        resizable: false,
      },
      {
        accessor: 'device_name',
        Header: 'Device Name',
        id: 'device_name',
        sortable: true,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'model_number',
        Header: 'Model',
        sortable: true,
        width: 150,
        id: 'model_number',
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'model_number',
        Header: 'IP Address',
        sortable: true,
        width: 130,
        id: 'model_number',
        Cell: c => <div>{_.get(c.original, 'monitoring_data.host_name', '-')}</div>,
      },
      {
        accessor: 'model_number',
        Header: 'OS Version',
        sortable: true,
        id: 'model_number',
        Cell: c => <div>{_.get(c.original, 'monitoring_data.os_version', '-')}</div>,
      },
      {
        accessor: 'id',
        Header: 'Status',
        sortable: false,
        Cell: cell => (
          <div className="icons-template">
            <div>{getRuleStatus(cell)}</div>
          </div>
        ),
      },
      {
        accessor: 'id',
        Header: 'Actions',
        width: 131,
        sortable: false,
        Cell: cell => (
          <div className="icons-template" style={{ display: 'flex' }}>
            {getPDFDownloadMarkUp(cell)}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };

    let configurationData = null;

    const deviceData = this.state.devices.find(x => x.id === this.state.device)
    if (deviceData) {
      configurationData = deviceData.configuration_data.config_data.config
    }

    return (
      <div className="rule-details-modal__body">
        <div className="rule-devices-list">
          {this.renderTopBar()}
          {currentPage.pageType === PageType.Config && (

            <div className="rule-details-config-data  col-md-12">
              {(
                <>
                  <Input
                    field={{
                      label: 'Device',
                      type: InputFieldType.PICKLIST,
                      value: this.state.device,
                      isRequired: true,
                      options: this.state.devices
                        .map(d => { return { label: d.device_name, value: d.id } })
                    }}
                    width={6}
                    placeholder="Select Device"
                    name="device"
                    className="device-select"
                    onChange={this.handleChange}
                  />
                  <ControlledEditor
                    height="70vh"
                    width="1034px"
                    value={configurationData}
                    onChange={a => null}
                    language="javascript"
                    options={options}
                    editorDidMount={this.handleEditorDidMount}
                    theme="dark"
                  />
                </>
              )}
            </div>
          )}

          {currentPage.pageType === PageType.Details && (
            <div className="rule-details-config-data row">
              <div className="details-setion row">
                <div className="left col-md-6">
                  <div className="field-section">
                    <div className="field__label row ">
                      <label className="field__label-label" title="">
                        Detail
                      </label>
                    </div>
                    <div className="preline-wrap rule-details">{rule.detail || 'N.A.'}</div>
                  </div>
                </div>
                <div className="right col-md-5">
                  <div className="field-section     col-md-6 col-xs-6">
                    <div className="field__label row">
                      <label className="field__label-label" title="">
                        Level
                      </label>
                    </div>
                    <div>{rule.level || 'N.A.'}</div>
                  </div>
                  <div className="field-section     col-md-6 col-xs-6">
                    <div className="field__label row">
                      <label className="field__label-label" title="">
                        Applies To Manufacturers
                      </label>
                    </div>
                    <div>{_.get(rule, 'applies_to_manufacturers', []).join(', ') || 'N.A.'}</div>
                  </div>
                  <div className="field-section     col-md-6 col-xs-6">
                    <div className="field__label row">
                      <label className="field__label-label" title="">
                        Applies To
                      </label>
                    </div>
                    <div>{_.get(rule, 'applies_to', []).join(', ') || 'N.A.'}</div>
                  </div>
                  <div className="field-section     col-md-6 col-xs-6">
                    <div className="field__label row">
                      <label className="field__label-label" title="">
                        Classification
                      </label>
                    </div>
                    <div>{rule.classification_name || 'N.A.'}</div>
                  </div>

                  {(rule.function && rule.function !== "") &&
                    <div className="field-section     col-md-6 col-xs-6">
                      <div className="field__label row">
                        <label className="field__label-label" title="">
                          Function
                        </label>
                      </div>
                      <div>{rule.function}</div>
                    </div>
                  }
                  <div className="field-section     col-md-6 col-xs-6">
                    <div className="field__label row">
                      <label className="field__label-label" title="">
                        Is Global
                      </label>
                    </div>
                    <div>{rule.is_global && 'True' || 'False'}</div>
                  </div>
                  <div className="field-section     col-md-6 col-xs-6">
                    <div className="field__label row">
                      <label className="field__label-label" title="">
                        Description
                      </label>
                    </div>
                    <div>{rule.description || 'N.A.'}</div>
                  </div>
                </div>
              </div>
              <div className='devices-table-header'>
                <div className="header-device-List">  Device List </div>
                <div className="sub-heading">
                  <Checkbox
                    isChecked={this.state.showOnlyFailedDevices}
                    name="loa"
                    onChange={e => this.handleChangeCheckBox(e)}
                  >
                    Show only failed devices
                  </Checkbox>
                  {this.state.devices.some(x => x.checked) && (
                    <SquareButton
                      onClick={() => this.props.rerunConfigCompliance(this.state.devices.filter(x => x.checked).map(y => y.id))}
                      content={
                        <span>
                          <img alt="" src="/assets/icons/reload.png" />
                          &nbsp; Re-Run Compliance
                        </span>
                      }
                      bsStyle={ButtonStyle.PRIMARY}
                      className={`btn-img ${this.props.isPostingBatch ? 'rotate-img-running' : ''}`}
                    />
                  )}
                </div>
              </div>
              <ReactTable
                data={this.state.devices || []}
                columns={columns}
                defaultPageSize={10}
                rowSelection={rowSelectionProps}
                SubComponent={row => {
                  return (
                    <ExpandableRuleComplianceResult
                      show={this.state.showComplianceResult}
                      onClose={this.onComplianceResultClose}
                      rule={this.state.rule}
                      config={row.original.compliance_statistics.find(o => { return o.id === this.state.rule.id })}
                    />
                  );
                }}
                className="expandable"
              />
            </div>
          )}
        </div>
      </div>
    );
  };

  getTitle = () => {
    return (
      <div className="rule-details">
        RULE : {this.props.complianceDashboardData.rules[this.props.rule].name}
      </div>
    );
  };

  downloadPDFReport = (docId: any) => {
    this.setState({
      downloadingPDFIds: [...this.state.downloadingPDFIds, docId],
    });
    if (this.props.customerId) {
      this.props.downloadDeviceReportSingle(this.props.customerId, docId)
        .then(action => {
          if (action.type === DOWNLOD_DEVICE_REP_SUCCESS) {
            this.setState({
              downloadingPDFIds: this.state.downloadingPDFIds.filter(
                id => docId !== id
              ),
            });
            const url = action.response.file_path;
            const link = document.createElement('a');
            link.href = url;
            link.target = '_blank';
            link.setAttribute('download', action.response.file_name);
            document.body.appendChild(link);
            link.click();
          }
        });
    } else {
      this.props.downloadCustomerDeviceReportSingle(docId)
        .then(action => {
          if (action.type === DOWNLOD_DEVICE_REP_SUCCESS) {
            this.setState({
              downloadingPDFIds: this.state.downloadingPDFIds.filter(
                id => docId !== id
              ),
            });
            const url = action.response.file_path;
            const link = document.createElement('a');
            link.href = url;
            link.target = '_blank';
            link.setAttribute('download', action.response.file_name);
            document.body.appendChild(link);
            link.click();
          }
        });
    }
  };

  getBody = () => {
    return (
      <div className="edit-rule">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className={`${this.props.isFetching ? `loading` : ''}`}>
          {this.props.rule && this.renderDeviceDetails()}
        </div>
        {this.state.showComplianceResult &&
          <ComplianceResult
            show={this.state.showComplianceResult}
            onClose={this.onComplianceResultClose}
            rule={this.state.rule}
            config={this.state.config}
          />
        }
      </div>
    );
  };

  getFooter = () => {
    return ('');
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.props.rule && this.getTitle()}
        bodyElement={this.props.rule && this.getBody()}
        footerElement={this.props.rule && this.getFooter()}
        className="add-rule rule-view"
      />
    );
  }
}



// const mapStateToProps = (state: IReduxStore) => ({
//   isFetching: state.rule.isFetching,
//   rule: state.rule.rule,
// });

// const mapDispatchToProps = (dispatch: any) => ({
//   downloadDeviceReportSingle: (customerID: any,id: number) => dispatch(downloadDeviceReportSingle(customerID,id)),
// });

export default ViewConfig;
