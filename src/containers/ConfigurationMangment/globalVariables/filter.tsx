import React from 'react';

import SquareButton from '../../../components/Button/button';
import Select from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import './style.scss';

interface IVariableFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: IVariableFilter) => void;
  prevFilters?: IVariableFilter;
  types: any[];
}

interface IVariableFilterState {
  filters: IVariableFilter;
}

export default class VariableFilter extends React.Component<
  IVariableFilterProps,
  IVariableFilterState
> {
  constructor(props: IVariableFilterProps) {
    super(props);

    this.state = {
      filters: {
        type: [],
        option: [],
      },
    };
  }

  componentDidUpdate(prevProps: IVariableFilterProps) {
    if (this.props.show !== prevProps.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }

  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };

  getBody = () => {
    const types = this.props.types
      ? this.props.types.map(user => ({
          value: user,
          label: user,
          disabled: false,
        }))
      : [];

    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Option</label>
            <div className="field__input">
              <Select
                name="option"
                value={filters.option}
                onChange={this.onFilterChange}
                options={[
                  { value: 'OR', label: 'OR' },
                  { value: 'AND', label: 'AND' },
                ]}
                multi={true}
                placeholder="Select"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Type</label>
            <div className="field__input">
              <Select
                name="type"
                value={filters.type}
                onChange={this.onFilterChange}
                options={types}
                multi={true}
                placeholder="Select"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
