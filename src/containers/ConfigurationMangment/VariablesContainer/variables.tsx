import React from 'react';
import { connect } from 'react-redux';
import CustomerVariablesList from '../customerVariables/variables';
import GlobalVariableList from '../globalVariables/variables';
import _ from 'lodash';
import '../../../commonStyles/versionHistory.scss';
import '../../../commonStyles/filterModal.scss';
import './style.scss';

enum PageType {
  GlobalVariables,
  CustomerVariables,
}
interface IVariablesContainerProps extends ICommonProps {
  variableProviderCustomerUser: any;
  user: ISuperUser;
  variableError: string;
  isFetching: boolean;
  customers: ICustomerShort[];
  downloadCircuitReportPU: any;
  downloadCircuitReportCU: any;
  variableDownloading: boolean;
  getCircuitInfoReportTypes: any;
  cuircuitInfoReportTypes: any[];
}

interface IVariablesContainerState {
  currentPage: {
    pageType: PageType;
  };
  customerId: any;
  type: string;
}

class VariablesContainer extends React.Component<
  IVariablesContainerProps,
  IVariablesContainerState
  > {
  constructor(props: IVariablesContainerProps) {
    super(props);

    this.state = {
      customerId: '',
      type: '',
      currentPage: {
        pageType: PageType.GlobalVariables,
      },
    };
  }
  componentDidMount() {
    if (_.get(this.props, 'user.type') === 'customer') {
      this.changePage(PageType.CustomerVariables);
    }
  }

  componentDidUpdate(prevProps: IVariablesContainerProps) {
    if (prevProps.user !== this.props.user && (_.get(this.props, 'user.type') === 'customer')) {
      this.changePage(PageType.CustomerVariables);
    }
  }

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="variable__header">
        {
          (_.get(this.props, 'user.type') === 'provider') &&
          <div
            className={`variable__header-link ${
              currentPage.pageType === PageType.GlobalVariables
                ? 'variable__header-link--active'
                : ''
              }`}
            onClick={() => this.changePage(PageType.GlobalVariables)}
          >
            Global Variables
        </div>
        }
        <div
          className={`variable__header-link ${
            currentPage.pageType === PageType.CustomerVariables
              ? 'variable__header-link--active'
              : ''
            }`}
          onClick={() => this.changePage(PageType.CustomerVariables)}
        >
          Customer Variables
        </div>
      </div>
    );
  };

  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="variable-container">
        <div className="variable">
          {this.renderTopBar()}
          {currentPage.pageType === PageType.GlobalVariables &&
            (_.get(this.props, 'user.type') === 'provider') && (
              <div>
                <GlobalVariableList />
              </div>
            )}
          {currentPage.pageType === PageType.CustomerVariables && (
            <div>
              <CustomerVariablesList />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VariablesContainer);
