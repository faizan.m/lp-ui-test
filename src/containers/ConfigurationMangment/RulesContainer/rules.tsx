import React from 'react';
import { connect } from 'react-redux';
import CustomerRulesList from '../customerRules/rules';
import GlobalRulesList from '../GlobalRules/rules';
import './style.scss';
import _ from 'lodash';

enum PageType {
  GlobalRules,
  CustomerRules,
}
interface IRulesContainerProps extends ICommonProps {
  variableProviderCustomerUser: any;
  user: ISuperUser;
  variableError: string;
  isFetching: boolean;
  customers: ICustomerShort[];
  downloadCircuitReportPU: any;
  downloadCircuitReportCU: any;
  variableDownloading: boolean;
  getCircuitInfoReportTypes: any;
  cuircuitInfoReportTypes: any[];
}

interface IRulesContainerState {
  currentPage: {
    pageType: PageType;
  };
  customerId: any;
  type: string;
  editId?: number;
  searchName?: string;
  searchPageIsGlobal?: boolean;
}

class RulesContainer extends React.Component<
  IRulesContainerProps,
  IRulesContainerState
  > {
  constructor(props: IRulesContainerProps) {
    super(props);

    this.state = {
      customerId: '',
      type: '',
      currentPage: {
        pageType: PageType.GlobalRules,
      },
    };
  }
  componentDidMount() {
    if (_.get(this.props, 'user.type') === 'customer') {
      this.changePage(PageType.CustomerRules);
    }
    const queryParams = this.props.location.search;
    if(queryParams){
      const isGlobal = new URLSearchParams(queryParams).get("is_global");
      const id = new URLSearchParams(queryParams).get("id");
      const name = new URLSearchParams(queryParams).get("name");
      if(isGlobal && id && name){
        this.changePage(isGlobal === "true" ? PageType.GlobalRules : PageType.CustomerRules)
        this.setState({
          editId: Number(id),
          searchName: name,
          searchPageIsGlobal: isGlobal === "true"
        })
        const updatedUrl =
          window.location.protocol +
          "//" +
          window.location.host +
          window.location.pathname;
        window.history.pushState({ path: updatedUrl }, "", updatedUrl);
      }
    }

  }

  componentDidUpdate(prevProps: IRulesContainerProps) {
    if (prevProps.user !== this.props.user && (_.get(this.props, 'user.type') === 'customer')) {
      this.changePage(PageType.CustomerRules);
    }
  }
  
  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="variable__header">
        {
          (_.get(this.props, 'user.type') === 'provider') &&
          <div
            className={`variable__header-link ${
              currentPage.pageType === PageType.GlobalRules
                ? 'variable__header-link--active'
                : ''
              }`}
            onClick={() => this.changePage(PageType.GlobalRules)}
          >
            Global Rules
        </div>
        }

        <div
          className={`variable__header-link ${
            currentPage.pageType === PageType.CustomerRules
              ? 'variable__header-link--active'
              : ''
            }`}
          onClick={() => this.changePage(PageType.CustomerRules)}
        >
          Customer Rules
        </div>
      </div>
    );
  };

  render() {
    const currentPage = this.state.currentPage;
    const ruleProps = this.state.searchPageIsGlobal !== undefined ? {
      editId: this.state.editId,
      searchName: this.state.searchName
    } : {};

    return (
      <div className="rules-container">
        <div className="variable">
          {this.renderTopBar()}
          {currentPage.pageType === PageType.GlobalRules &&
            _.get(this.props, "user.type") === "provider" && (
              <div>
                <GlobalRulesList
                 {...(this.state.searchPageIsGlobal ? ruleProps : {})}
                />
              </div>
            )}
          {currentPage.pageType === PageType.CustomerRules && (
            <div>
              <CustomerRulesList
                {...(!this.state.searchPageIsGlobal ? ruleProps : {})}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(RulesContainer);
