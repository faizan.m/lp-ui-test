import React from 'react';

import SquareButton from '../../../components/Button/button';
import Select from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';

import './style.scss';

interface IAddDeviceFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: IConfigurationFilters) => void;
  prevFilters: IConfigurationFilters;
  sites: any[];
  types: any[];
  statusList?: any[];
  levels: any[];
  classifications: any[];
}

interface IAddDeviceFormState {
  filters: IConfigurationFilters;
}

export default class AddDeviceForm extends React.Component<
  IAddDeviceFormProps,
  IAddDeviceFormState
> {
  constructor(props: IAddDeviceFormProps) {
    super(props);

    this.state = {
      filters: {
        site: [],
        type: [],
        status: [],
      },
    };
  }

  componentDidUpdate(prevProps: IAddDeviceFormProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }
  
  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };

  getBody = () => {
    const sites = this.props.sites
      ? this.props.sites.map(site => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];
    const types = this.props.types
      ? this.props.types.map((type, index) => ({
          value: type,
          label: type ? type : 'N.A.',
        }))
      : [];
    const statusList = this.props.statusList
      ? this.props.statusList.map(s => ({
          value: s.id,
          label: s.description,
        }))
      : [];
    const filters = this.state.filters;
    const levels = this.props.levels
      ? this.props.levels.map(user => ({
          value: user,
          label: user,
          disabled: false,
        }))
      : [];

    return (
      <div className="filters-modal__body col-md-12">
        {' '}
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Site</label>
            <div className="field__input">
              <Select
                name="site"
                value={filters.site}
                onChange={this.onFilterChange}
                options={sites}
                multi={true}
                placeholder="Select Site"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Status</label>
            <div className="field__input">
              <Select
                name="status"
                value={filters.status}
                onChange={this.onFilterChange}
                options={statusList}
                multi={true}
                placeholder="Select Status"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Type</label>
            <div className="field__input">
              <Select
                name="type"
                value={filters.type}
                onChange={this.onFilterChange}
                options={types.concat([
                  {
                    value: 'N.A.',
                    label: 'N.A.',
                  },
                ])}
                multi={true}
                placeholder="Select Type"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Level</label>
            <div className="field__input">
              <Select
                name="level"
                value={filters.level}
                onChange={this.onFilterChange}
                options={levels}
                multi={true}
                placeholder="Select Levels"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
