import React from 'react';

import { isEmpty } from 'lodash';
import SquareButton from '../../../components/Button/button';
import ModalBase from '../../../components/ModalBase/modalBase';
import Spinner from '../../../components/Spinner';
interface IViewRulesProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  configuration: IConfiguration;
  isFetching: boolean;
}

interface IEditDeviceFormState {
  searchKey: string;
  activeIndex: number;
  openSectionInvalid: boolean;
  openSectionValid: boolean;
}

export default class ViewRules extends React.Component<
  IViewRulesProps,
  IEditDeviceFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  props: any;

  constructor(props: IViewRulesProps) {
    super(props);
    this.state = {
      searchKey: '',
      activeIndex: -1,
      openSectionInvalid: true,
      openSectionValid: false,
    };
  }

  onClose = (event: any) => {
    this.setState({
      searchKey: '',
      openSectionInvalid: true,
      openSectionValid: false,
    });
    this.props.onClose(event);
  };

  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };

  renderDeviceDetails = () => {
    const configuration = this.props.configuration;
    let groupedByValid = {};
    if (configuration && configuration.valid_compliance_statistics) {
      groupedByValid = this.groupBy(
        configuration.valid_compliance_statistics,
        'level'
      );
    }
    let groupedByInvalid = {};
    if (configuration && configuration.invalid_compliance_statistics) {
      groupedByInvalid = this.groupBy(
        configuration.invalid_compliance_statistics,
        'level'
      );
    }

    return (
      <div className="configuration-violation-modal__body">
        <div className="configuration-details-modal__body-field">
          <label>Serial #</label>
          <label>
            {configuration.serial_number ? configuration.serial_number : '-'}
          </label>
        </div>
        <div className="configuration-violation-config-data col-md-12">
          <div className="device-fields col-md-12 row">
            <div
              className={`field ${
                this.state.openSectionInvalid ? 'field--collapsed' : ''
              }`}
              key={6}
            >
              <div
                className={`section-heading invalid-configuration
                    ${isEmpty(groupedByInvalid) ? 'disable-heading' : ''}`}
                onClick={() =>
                  this.setState({
                    openSectionInvalid: !this.state.openSectionInvalid,
                  })
                }
              >
                InValid configuration
                {isEmpty(groupedByInvalid) ? (
                  ''
                ) : (
                  <div className="action-collapse">
                    {this.state.openSectionInvalid ? '-' : '+'}
                  </div>
                )}
              </div>
              {this.state.openSectionInvalid &&
                groupedByInvalid &&
                groupedByInvalid !== {} &&
                Object.keys(groupedByInvalid).map((data, i) => (
                  <div key={i} className="voilation-statistics col-md-12">
                    <div className="name">
                      <div className="classification-name">
                        <span>Level: </span> {groupedByInvalid[data][0].level}
                      </div>
                      {groupedByInvalid[data] &&
                        groupedByInvalid[data].length > 0 && (
                          <div className="compliance_statistics-row col-md-12">
                            <div className="classification-name col-md-4">
                              Classification{' '}
                            </div>
                            <div className="classification-name col-md-8">
                              Text
                            </div>
                          </div>
                        )}
                      {groupedByInvalid[data].map((rule, index) => (
                        <div
                          className="compliance_statistics-row col-md-12"
                          key={index}
                        >
                          <div className="col-md-4">
                            {rule.classification_name}{' '}
                          </div>
                          <div className="detail col-md-8">{rule.detail} </div>
                        </div>
                      ))}
                    </div>
                  </div>
                ))}
            </div>
            <div
              className={`field ${
                this.state.openSectionValid ? 'field--collapsed' : ''
              }`}
              key={5}
            >
              <div
                className={`section-heading valid-configuration
                    ${isEmpty(groupedByValid) ? 'disable-heading' : ''}`}
                onClick={() =>
                  this.setState({
                    openSectionValid: !this.state.openSectionValid,
                  })
                }
              >
                Valid configuration
                {isEmpty(groupedByValid) ? (
                  ''
                ) : (
                  <div className="action-collapse">
                    {!this.state.openSectionValid ? '+' : '-'}
                  </div>
                )}
              </div>
              {this.state.openSectionValid &&
                groupedByValid &&
                groupedByValid !== {} &&
                Object.keys(groupedByValid).map((data, i) => (
                  <div key={i + 55} className="voilation-statistics col-md-12">
                    <div className="name">
                      <div className="classification-name">
                        <span>Level: </span> {groupedByValid[data][0].level}
                      </div>
                      {groupedByValid[data] && groupedByValid[data].length > 0 && (
                        <div className="compliance_statistics-row col-md-12">
                          <div className="classification-name col-md-2">
                            Classification{' '}
                          </div>
                          <div className="classification-name col-md-5">
                            Text
                          </div>
                          <div className="classification-name col-md-5">
                            Details
                          </div>
                        </div>
                      )}
                      {groupedByValid[data].map((rule, index) => (
                        <div
                          className="compliance_statistics-row col-md-12"
                          key={index}
                        >
                          <div className="col-md-2">
                            {rule.classification_name}{' '}
                          </div>
                          <div className="detail col-md-5">{rule.detail} </div>
                          <div className="detail col-md-5">
                            {rule.valid_compliance &&
                              rule.valid_compliance.join('\r\n\r\n')}{' '}
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  };

  getTitle = () => {
    return <div className="configuration-details">Compliance Violation</div>;
  };

  getBody = () => {
    return (
      <div className="configuration-violation">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className={`${this.props.isFetching ? `loading` : ''}`}>
          {this.props.configuration && this.renderDeviceDetails()}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-configuration__footer
        ${this.props.isFetching ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Close"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.props.configuration && this.getTitle()}
        bodyElement={this.props.configuration && this.getBody()}
        footerElement={this.props.configuration && this.getFooter()}
        className="add-configuration configuration-view"
      />
    );
  }
}
