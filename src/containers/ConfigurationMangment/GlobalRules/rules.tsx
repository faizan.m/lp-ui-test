import React from 'react';
import { Mention, MentionsInput } from 'react-mentions';
import { connect } from 'react-redux';

import Select from 'react-select';
import EditButton from '../../../components/Button/editButton';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import Checkbox from '../../../components/Checkbox/checkbox';
import ViewRuleHistory from '../RulesContainer/ruleHistory';
import ViewRuleCDHistory from './../RulesContainer/rulesCDHistory';
import { cloneDeep, debounce } from 'lodash';
import {
  createClassifications,
  deleteRule,
  editRule,
  FETCH_COMP_CLASS_SUCCESS,
  FETCH_RULE_SUCCESS,
  fetchClassifications,
  fetchGlobalVariables,
  fetchRules,
  fetchRulesLevels,
  fetchSingleRule,
  saveRule,
  fetchRuleFunctions, FETCH_RULE_FUNCTION_SUCCESS
} from '../../../actions/configuration';
import { fetchTypes,
  fetchManufacturers,
  fetchAllCustomerDevices,
  TASK_STATUS_SUCCESS,
  TASK_STATUS_FAILURE,
  fetchTaskStatus,
} from '../../../actions/inventory';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import { RERUN_COMPLIANCE_SUCCESS, rerunConfigCompliance } from '../../../actions/collector';
import SquareButton from '../../../components/Button/button';
import DeleteButton from '../../../components/Button/deleteButton';
import ConfirmBox from '../../../components/ConfirmBox/ConfirmBox';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';
import RulesFilter from './filter';
import './style.scss';
import _ from 'lodash';
import IconButton from '../../../components/Button/iconButton';
import { RuleDetails } from '../RulesContainer/RuleDetails';
import TooltipCustom from '../../../components/Tooltip/tooltip';
import RuleExceptionCustomTooltip from '../../../components/Tooltip/ruleExceptionTooltip';
import IOSVersionCustomTooltip from '../../../components/Tooltip/iosVersionTooltip';
import { commonFunctions } from '../../../utils/commonFunctions';
import RightMenu from '../../../components/RighMenuBase/rightMenuBase';
import '../../../commonStyles/filtersListing.scss';

interface IGlobalRulesListingProps {
  editId?: number;
  searchName?: string;
  fetchRules: any;
  fetchClassifications: any;
  fetchRulesLevels: any;
  fetchSingleRule: any;
  rules: any;
  isFetching: any;
  rule: IRule;
  classifications: any;
  levels: any;
  isFetchingList: any;
  saveRule: any;
  editRule: any;
  customers: ICustomerShort[];
  createClassifications: any;
  deleteRule: any;
  types: any;
  fetchTypes: any;
  globalVariables: any;
  fetchGlobalVariables: any;
  fetchManufacturers: TFetchManufacturers;
  manufacturers: any[];
  fetchRuleFunctions: any;
  fetchAllCustomerDevices: any;
  rerunConfigCompliance: any;
  customerId: any;
  fetchTaskStatus: any;
  addErrorMessage: any;
  addSuccessMessage: any;
  isPostingBatch: boolean;
}

interface IGlobalRulesListingState {
  rule: IRule;
  openViewModalRule: boolean;
  open: boolean;
  error: {
    detail: IFieldValidation;
    level: IFieldValidation;
    classification: IFieldValidation;
    customers_assigned: IFieldValidation;
    applies_to: IFieldValidation;
    applies_to_manufacturers: IFieldValidation;
    name: IFieldValidation;
    description: IFieldValidation;
    function: IFieldValidation;
    to_ignore_devices: IFieldValidation;
    apply_not: IFieldValidation;
    ios_version: IFieldValidation;
    exceptions: IFieldValidation;
  };
  isopenConfirm: boolean;
  rules: IRule[];
  rows: IRule[];
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  filters: IRulesFilter;
  id?: any;
  isFilterModalOpen: boolean;
  viewhistory: boolean;
  viewCDhistory: boolean;
  functionList: any[];
  devicesList: any[];
  selectedRows: any[];
  showComplianceRerunBtn: boolean;
  complianceRunTaskId: any;
  isSuccess: boolean;
}

class GlobalRulesList extends React.Component<
  IGlobalRulesListingProps,
  IGlobalRulesListingState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  private debouncedFetch;

  constructor(props: IGlobalRulesListingProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 500);
  }

  getEmptyState = () => ({
    rules: [],
    rows: [],
    openViewModalRule: false,
    open: false,
    rule: {
      detail: '',
      level: '',
      classification: 0,
      description: '',
      applies_to: [],
      applies_to_manufacturers: [],
      name: '',
      function: '',
      is_enabled: true,
      apply_not: false,
      to_ignore_devices: [],
      exceptions: ''
    },
    error: {
      detail: { ...GlobalRulesList.emptyErrorState },
      level: { ...GlobalRulesList.emptyErrorState },
      classification: { ...GlobalRulesList.emptyErrorState },
      customers_assigned: { ...GlobalRulesList.emptyErrorState },
      applies_to: { ...GlobalRulesList.emptyErrorState },
      applies_to_manufacturers: { ...GlobalRulesList.emptyErrorState },
      name: { ...GlobalRulesList.emptyErrorState },
      description: { ...GlobalRulesList.emptyErrorState },
      function: { ...GlobalRulesList.emptyErrorState },
      to_ignore_devices: { ...GlobalRulesList.emptyErrorState },
      apply_not: { ...GlobalRulesList.emptyErrorState },
      ios_version: { ...GlobalRulesList.emptyErrorState },
      exceptions: { ...GlobalRulesList.emptyErrorState },
    },
    isopenConfirm: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    reset: false,
    isFilterModalOpen: false,
    filters: {
      level: [],
      classification: [],
      is_enabled: [],
    },
    viewhistory: false,
    viewCDhistory: false,
    functionList: [],
    devicesList: [],
    selectedRows: [],
    showComplianceRerunBtn: false,
    complianceRunTaskId: null,
    isSuccess: false
  });
  toggleViewModalRule = () => {
    this.setState({
      rule: this.getEmptyState().rule,
      openViewModalRule: !this.state.openViewModalRule,
    });
  };

  componentDidMount() {
    if(this.props.searchName) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search: this.props.searchName,
          },
        },
      }))
    }
    if (this.props.editId) {
      this.props
        .fetchSingleRule(this.props.editId)
        .then((action) => {
          if (action.type === FETCH_RULE_SUCCESS) {
            const response: IRule = action.response;
            const rule = {
              ...response,
              exceptions: response.exceptions.join('\n'),
              applies_to_ios_version: response.applies_to_ios_version.join(', ')
            };
            this.setState({
              open: true,
              rule: rule,
            });
          }
        });
    }
    if (!this.props.manufacturers) {
      this.props.fetchManufacturers();
    }
    if (this.props.levels.length === 0) {
      this.props.fetchRulesLevels();
    }
    if (this.props.classifications.length === 0) {
      this.props.fetchClassifications(_.get(this.props, 'user.type'));
    }
    if (this.props.types === null) {
      this.props.fetchTypes();
    }
    this.props.fetchGlobalVariables({ pagination: false });
    this.fetchRuleFunction();
  }

  fetchRuleFunction = () => {
    this.props.fetchRuleFunctions()
      .then(action => {
        if (action.type === FETCH_RULE_FUNCTION_SUCCESS && action.response) {
          const functionList = action.response.map(c => ({
            value: c.key,
            label: c.key,
            disabled: false,
          }));
          this.setState({ functionList })
        }
      })
  }

  componentDidUpdate(prevProps: IGlobalRulesListingProps) {
    if (this.props.rules && this.props.rules !== prevProps.rules) {
      this.setRows(this.props);
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchRules(newParams);
  };
  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: IGlobalRulesListingProps) => {
    const customersResponse = nextProps.rules;
    const rules: any[] = customersResponse.results;
    const rows: any[] = rules.map((rule, index) => ({
      classification: rule.classification,
      classification_name: rule.classification_name,
      customers_assigned: rule.customers_assigned,
      detail: rule.detail,
      level: rule.level,
      updated_on: rule.updated_on,
      created_on: rule.created_on,
      name: rule.name,
      description: rule.description,
      applies_to: rule.applies_to,
      applies_to_manufacturers: rule.applies_to_manufacturers,
      is_assigned: rule.is_assigned,
      is_enabled: rule.is_enabled,
      is_global: rule.is_global,
      id: rule.id,
      function: rule.function,
      to_ignore_devices: rule.to_ignore_devices,
      apply_not: rule.apply_not,
      exceptions: rule.exceptions.join('\n'),
      applies_to_ios_version: rule.applies_to_ios_version.join(', '),
      index,
    }));

    this.setState(prevState => ({
      reset: false,
      rows,
      rules,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onEditRowClick = (rules: any, event: any) => {
    event.stopPropagation();
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.rule as IRule) = rules;
    this.setState(newState);
  };

  onCloneClick = (rules: any, event: any) => {
    event.stopPropagation();
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.rule as IRule) = rules;
    (newState.rule.id as number) = 0;
    (newState.rule.name as string) = newState.rule.name + ' (cloned)';
    this.setState(newState);
  };

  onClickViewviewCDHistory = () => {
    this.setState({
      viewCDhistory: true,
    });
  }
  toggleviewCDHistoryPopup = (e, reverted) => {
    if (reverted) {
      this.fetchData(this.state.pagination.params)
    }
    this.setState({
      viewCDhistory: false,
    });
  };

  onRowClick = rowInfo => {
    event.stopPropagation();
    // tslint:disable-next-line: max-line-length
    this.setState({ openViewModalRule: true, rule: this.state.rows[rowInfo.original.index] });
  };

  onClickReRunCompliance = (event: any, index?: any) => {
    const { selectedRows } = this.state;

    const data = {
      customer_id: this.props.customerId,
      rule_ids: selectedRows
    };

    this.props
      .rerunConfigCompliance(data)
      .then(a => {
        if (a.type === RERUN_COMPLIANCE_SUCCESS) {
          if (a.response && a.response.task_id) {
            this.setState({
              complianceRunTaskId: a.response.task_id,
            });
            this.fetchTaskStatus(a.response.task_id, undefined, 'Re-run compliance ', 'ComplianceRuntaskId');
          }
        }
      });
  };

  fetchTaskStatus = (taskId, list, message: string, idInState) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then(a => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatus(taskId, list, message, idInState);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  renderTopBar = () => {
    return (
      <div className="service-catalog-listing__actions ">
        <div className="header-panel">
          <Input
            field={{
              value: this.state.pagination.params.search,
              label: '',
              type: InputFieldType.SEARCH,
            }}
            width={11}
            name="searchString"
            onChange={this.onSearchStringChange}
            placeholder="Search"
            className="search"
          />
          <div className='header-actions'>
            { this.state.showComplianceRerunBtn && (
                <SquareButton
                  onClick={this.onClickReRunCompliance}
                  content={
                    <span>
                      <img alt="" src="/assets/icons/reload.png" />
                        &nbsp; Re-Run Compliance
                    </span>
                  }
                  bsStyle={ButtonStyle.PRIMARY}
                  disabled={this.props.isPostingBatch}
                  className={`btn-img ${this.props.isPostingBatch ? 'rotate-img-running' : ''}`}
                />
            )}
            <SquareButton
              onClick={this.onClickViewviewCDHistory}
              content={
                <span>
                  <img alt="" src="/assets/icons/version.svg" />
                  History
                </span>
              }
              bsStyle={ButtonStyle.PRIMARY}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={this.props.rules && this.props.rules.length === 0}
              bsStyle={ButtonStyle.PRIMARY}
            />
            <SquareButton
              content={`+ Add Rule`}
              bsStyle={ButtonStyle.PRIMARY}
              onClick={() => this.clearPopUp(true)}
              className="save"
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };
  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      prevState => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.setState(prevState => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              level: prevState.filters.level.join(),
              classification: prevState.filters.classification.join(),
              is_enabled: prevState.filters.is_enabled.join(),
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      (filters.level && filters.level.length > 0) ||
      (filters.classification && filters.classification.length > 0) ||
      (filters.is_enabled && filters.is_enabled.length > 0);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.level.length > 0 && (
          <div className="section-show-filters">
            <label>Level: </label>
            <SelectInput
              name="level"
              value={filters.level}
              onChange={this.onFilterChange}
              options={this.levelList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.classification.length > 0 && (
          <div className="section-show-filters">
            <label>Classification: </label>
            <SelectInput
              name="classification"
              value={filters.classification}
              onChange={this.onFilterChange}
              options={this.classificationList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.is_enabled.length > 0 && (
          <div className="section-show-filters">
            <label>Status: </label>
            <SelectInput
              name="is_enabled"
              value={filters.is_enabled}
              onChange={this.onFilterChange}
              options={[
                { label: 'Enabled', value: 'true' },
                { label: 'Disabled', value: 'false' },
              ]}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };
  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteRule(this.state.id).then(action => {
      if (action.type === FETCH_RULE_SUCCESS) {
        this.debouncedFetch();
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: '',
    });
  };


  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: '',
    });
  };

  onClickViewHistory(id: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      id,
    });
  }

  toggleFilterModal = () => {
    this.setState(prevState => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IRulesFilter) => {
    if (filters) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            level: filters.level.join(),
            is_enabled: filters.is_enabled.join(),
            classification: filters.classification.join(),
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal();
    }
  };

  handleChange = e => {
    const newState = cloneDeep(this.state);
    (newState.rule[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  handleChangeCheckBox = (e: any) => {
    const newState = cloneDeep(this.state);
    (newState.rule[e.target.name] as any) = e.target.checked;
    this.setState(newState);
  };

  handleChangeDetails = e => {
    const newState = cloneDeep(this.state);
    (newState.rule.detail as any) = e.target.value;
    this.setState(newState);
  };

  handleChangeType = (event: any) => {
    const newState = cloneDeep(this.state);
    const targetValue = event.target.value;
    (newState.rule.is_enabled as boolean) = JSON.parse(targetValue);
    this.setState(newState);
  };
  handleChangeClassification = e => {
    const newState = cloneDeep(this.state);
    (newState.rule.classification as any) = e.value;
    this.setState(newState);
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.rule as IRule) = this.getEmptyState().rule;
    (newState.error as any) = this.getEmptyState().error;
    (newState.isopenConfirm as any) = false;
    this.setState(newState);
  };

  validateForm() {
    const newState: IGlobalRulesListingState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.rule.name || this.state.rule.name.trim().length === 0) {
      newState.error.name.errorState = IValidationState.ERROR;
      newState.error.name.errorMessage = 'Name cannot be empty';
      isValid = false;
    }
    if (!this.state.rule.detail || this.state.rule.detail.trim().length === 0) {
      newState.error.detail.errorState = IValidationState.ERROR;
      newState.error.detail.errorMessage = 'Text cannot be empty';
      isValid = false;
    }
    if (!this.state.rule.level) {
      newState.error.level.errorState = IValidationState.ERROR;
      newState.error.level.errorMessage = 'Please select level';
      isValid = false;
    }
    if (!this.state.rule.classification) {
      newState.error.classification.errorState = IValidationState.ERROR;
      newState.error.classification.errorMessage =
        'Please select classification';
      isValid = false;
    }

    if (
      !this.state.rule.applies_to ||
      (this.state.rule.applies_to && this.state.rule.applies_to.length === 0)
    ) {
      newState.error.applies_to.errorState = IValidationState.ERROR;
      newState.error.applies_to.errorMessage = 'Please select applies to';
      isValid = false;
    }
    if (
      !this.state.rule.applies_to_manufacturers ||
      (this.state.rule.applies_to_manufacturers && this.state.rule.applies_to_manufacturers.length === 0)
    ) {
      newState.error.applies_to_manufacturers.errorState = IValidationState.ERROR;
      newState.error.applies_to_manufacturers.errorMessage = 'Please select applies to manufacturers';
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }
  onSaveClick = (
    rule: IRule,
    reload: boolean = false,
    checkValid: boolean = false
  ) => {
    if (this.validateForm() || checkValid) {
      const regex = /#RULE#/gi;

      rule.detail = rule.detail.replace(regex, '');
      rule.exceptions = rule.exceptions.split('\n');

      rule.applies_to_ios_version = rule.applies_to_ios_version === "" ?
        [] : rule.applies_to_ios_version && rule.applies_to_ios_version.split(',');

      if (rule.id) {
        this.props.editRule(rule).then(action => {
          if (action.type === 'FETCH_RULE_SUCCESS') {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true })
              this.debouncedFetch({ page: 1 });
            }
          }
          if (action.type === 'FETCH_RULE_FAILURE') {
            this.setValidationErrors(action.errorList.data);
          }
        });
      } else {
        this.props.saveRule(rule).then(action => {
          if (action.type === 'FETCH_RULE_SUCCESS') {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true })
              this.debouncedFetch({ page: 1 });
            }
          }
          if (action.type === 'FETCH_RULE_FAILURE') {
            this.setValidationErrors(action.errorList.data);
          }
        });
      }
    }
  };

  setValidationErrors = errorList => {
    const newState: IGlobalRulesListingState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };

  onNewOptionClick = option => {
    const { className, ...newOption } = option;
    const name = newOption.label;
    this.props.createClassifications(name).then(action => {
      if (action.type === FETCH_COMP_CLASS_SUCCESS) {
        this.props.fetchClassifications(_.get(this.props, 'user.type'));
      }
    });
  };
  levelList = () => {
    const levelList = this.props.levels
      ? this.props.levels.map(c => ({
        value: c,
        label: c,
        disabled: false,
      }))
      : [];

    return levelList;
  };

  classificationList = () => {
    const classification =
      this.props.classifications.length > 0
        ? this.props.classifications.map(c => ({
          value: c.id,
          label: c.name,
          disabled: false,
        }))
        : [];

    return classification;
  };

  typeList = () => {
    const types = this.props.types
      ? this.props.types.filter(x => x !== '')
      : [];
    const typess =
      types && types.length > 0
        ? types.map(c => ({
          value: c,
          label: c,
          disabled: false,
        }))
        : [];

    return typess;
  };
  manufacturers = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(c => ({
        value: c.label,
        label: c.label,
      }))
      : [];

    return manufacturers;
  };
  globalVariableList = () => {
    const globalVariables =
      this.props.globalVariables && this.props.globalVariables.length > 0
        ? this.props.globalVariables.map(c => ({
          id: c.id,
          display: c.variable_code,
        }))
        : [];

    return globalVariables;
  };


  rangeVariables = () => {
    const variablesList = ['#NUM_RANGE(1-10)', '#VAR_OPTION_OR(value1,value2,value3)', '#VAR_OPTION_AND(value1, value2,value3) ']
    const globalVariables = variablesList.map(c => ({
      id: c,
      display: c,
    }))

    return globalVariables;
  };

  onRowsToggle = selectedRows => {
    this.setState({
      selectedRows,
      showComplianceRerunBtn: selectedRows.length > 0
    });
  };

  render() {
    const columns: ITableColumn[] = [
      {
        accessor: 'level',
        Header: 'Level',
        id: 'level_int',
        width: 80,
        sortable: true,
        Cell: cell => (
          <div className="icons-template">
            {cell.original.level && (
              <img
                src={`/assets/icons/l-${cell.original.level}.svg`}
                alt=""
                title={cell.original.level}
                className="status-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'name',
        Header: 'Name',
        id: 'name',
        width: 200,
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'applies_to',
        Header: 'Applies To',
        sortable: false,
        id: 'applies_to',
        Cell: c => <div>{c.value && c.value.join()}</div>,
      },
      {
        accessor: 'detail',
        Header: 'Detail',
        width: 80,
        sortable: false,
        id: 'detail',
        Cell: cell => (
          <div className="icons-template-1">
            {cell.value && cell.value.length > 0 && (
              <img
                src={`/assets/new-icons/info.svg`}
                alt=""
                title={`${cell.value} `}
                className="info-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'description',
        Header: 'Notes',
        width: 80,
        sortable: false,
        id: 'description',
        Cell: cell => (
          <div className="icons-template-1">
            {cell.value && cell.value.length > 0 && (
              <img
                src={`/assets/icons/notepad.png`}
                alt=""
                title={`${cell.value} `}
                className="info-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'is_enabled',
        id: 'is_enabled',
        sortable: false,
        Header: 'Status',
        Cell: status => (
          <div
            className={`status status--${status.value === true ? 'Enabled' : 'Disabled'
              }`}
          >
            {status.value === true ? 'Enabled' : 'Disabled'}
          </div>
        ),
        width: 130,
      },
      {
        accessor: 'id',
        Header: 'History',
        width: 55,
        sortable: false,
        Cell: cell => (
          <IconButton
            icon="version.svg"
            onClick={e => {
              this.onClickViewHistory(cell.original.id, e);
            }}
            title={'Show Rule History'}
          />
        ),
      },
      {
        accessor: 'id',
        Header: 'Actions',
        width: 140,
        sortable: false,
        Cell: cell => (
          <div>
            <EditButton onClick={e => this.onEditRowClick(cell.original, e)} />
            <DeleteButton
              type="cloned"
              title="Clone Rule"
              onClick={e => this.onCloneClick(cell.original, e)}
            />
            <DeleteButton onClick={e => this.onDeleteRowClick(cell, e)} />
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.debouncedFetch,
      reset: this.state.reset,
    };
    return (
      <div className="rules-listing-parent">
        <div className="loader">
          <Spinner show={this.props.isFetching || this.props.isFetchingList} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${this.props.isFetching ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
        />
        <ViewRuleHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
        // ruleHistory={this.props.ruleHistory}
        />
        <RulesFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          levels={this.props.levels}
          classifications={this.props.classifications}
          types={this.props.types}
          prevFilters={this.state.filters}
        />
        <RuleDetails
          show={this.state.openViewModalRule}
          onClose={this.toggleViewModalRule}
          rule={this.state.rule}
          devices={this.state.devicesList}
        />
        <RightMenu
          show={this.state.open}
          onClose={() => this.clearPopUp(!this.state.open)}
          titleElement={ <div className='right-popup-header'> {this.state.rule.id ? 'Update' : 'Add'} Rule  </div>}
          bodyElement={
            <div className="col-md-12 body rule-right-menu">
              <div className="loader">
                <Spinner show={this.props.isFetching} />
              </div>
              <Input
                field={{
                  label: 'Name',
                  type: InputFieldType.TEXT,
                  value: this.state.rule.name,
                  isRequired: true,
                }}
                width={6}
                name="name"
                onChange={e => this.handleChange(e)}
                error={this.state.error.name}
                placeholder={`Enter Name`}
              />
              <Input
                field={{
                  label: 'Level',
                  type: InputFieldType.PICKLIST,
                  value: this.state.rule.level,
                  options: this.levelList(),
                  isRequired: true,
                }}
                width={6}
                name="level"
                onChange={e => this.handleChange(e)}
                error={this.state.error.level}
                placeholder={`Select Level`}
              />
              <Input
                field={{
                  label: 'Applies To Manufacturer',
                  type: InputFieldType.PICKLIST,
                  value: this.state.rule.applies_to_manufacturers,
                  options: this.manufacturers(),
                  isRequired: true,
                }}
                width={6}
                multi={true}
                name="applies_to_manufacturers"
                onChange={e => this.handleChange(e)}
                error={this.state.error.applies_to_manufacturers}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: 'Applies To',
                  type: InputFieldType.PICKLIST,
                  value: this.state.rule.applies_to,
                  options: this.typeList(),
                  isRequired: true,
                }}
                width={6}
                multi={true}
                name="applies_to"
                onChange={e => this.handleChange(e)}
                error={this.state.error.applies_to}
                placeholder={`Select`}
              />
              <div
                className={`field-section
                 field-section--required   col-md-6 col-xs-6 ${this.state.error.classification.errorMessage
                    ? `field-section--error`
                    : ''
                  } `}
              >
                <div className="field__label row">
                  <label className="field__label">Classification</label>
                  <span className="field__label-required" />
                </div>
                <div>
                  {' '}
                  <Select.Creatable
                    name="classification"
                    options={this.classificationList()}
                    onChange={e => this.handleChangeClassification(e)}
                    value={this.state.rule.classification}
                    onNewOptionClick={this.onNewOptionClick}
                    placeholder="Select or Type to Create New"
                    loading={this.props.isFetching}
                    clearable={false}
                    promptTextCreator={name => `Create Type "${name}"`}
                  />
                </div>
                {this.state.error.classification.errorMessage && (
                  <div className="field__error">
                    {this.state.error.classification.errorMessage}
                  </div>
                )}
              </div>
              <Input
                field={{
                  label: 'Function ',
                  type: InputFieldType.PICKLIST,
                  value: this.state.rule.function,
                  options: this.state.functionList,
                  isRequired: false,
                }}
                width={6}
                multi={false}
                name="function"
                onChange={e => this.handleChange(e)}
                error={this.state.error.function}
                placeholder={`Select`}
              />
               <Input
                  className='rule-iosversion1'
                  field={{
                    label: 'IOS versions',
                    type: InputFieldType.TEXT,
                    value: this.state.rule.applies_to_ios_version,
                    isRequired: false,
                  }}
                  width={12}
                  labelIcon="info"
                  labelTitle={<IOSVersionCustomTooltip />}
                  name="applies_to_ios_version"
                  onChange={e => this.handleChange(e)}
                  error={this.state.error.ios_version}
                  placeholder={`Enter IOS versions`}
                />
              <div className="field-section field-section--required  col-md-12 col-xs-12">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Detail
                  </label>
                  <span className="field__label-required" />
                  <TooltipCustom/>
                  <div className="not-checkbox">
                    <Checkbox
                      isChecked={this.state.rule.apply_not}
                      name="apply_not"
                      onChange={e => this.handleChangeCheckBox(e)}
                    >
                      Fail if match found
                    </Checkbox>
                  </div>
                </div>
                <div
                  className={`${this.state.error.detail.errorMessage ? `error-input` : ''
                    }`}
                >
                  <MentionsInput
                    markup="[__display__]"
                    value={this.state.rule.detail}
                    onChange={this.handleChangeDetails}
                    className={'outer'}
                  >
                    <Mention
                      trigger="@"
                      data={this.globalVariableList()}
                      className={'inner-drop'}
                      markup="#RULE#__display__"
                    />
                    <Mention
                      trigger="#"
                      data={this.rangeVariables()}
                      className={'inner-drop'}
                      markup="#RULE#__display__"
                    />
                  </MentionsInput>
                </div>
                {this.state.error.detail.errorMessage && (
                  <div className="field__error">
                    {this.state.error.detail.errorMessage}
                  </div>
                )}
              </div>
              <Input
                  className="rule-exceptions1"
                  field={{
                    label: 'Exceptions',
                    type: InputFieldType.TEXTAREA,
                    value: this.state.rule.exceptions,
                    isRequired: false,
                  }}
                  width={12}
                  name="exceptions"
                  onChange={e => this.handleChange(e)}
                  error={this.state.error.exceptions}
                  placeholder={`Enter Rule Exceptions`}
                  labelIcon="info"
                  labelTitle={<RuleExceptionCustomTooltip />}
                />
              <Input
                field={{
                  label: 'Description',
                  type: InputFieldType.TEXTAREA,
                  value: this.state.rule.description,
                  isRequired: false,
                }}
                width={12}
                name="description"
                onChange={e => this.handleChange(e)}
                error={this.state.error.description}
                placeholder={`Enter Description`}
              />
              {this.state.rule.id !== 0 && (
                <Input
                  field={{
                    label: 'Status',
                    type: InputFieldType.RADIO,
                    value: this.state.rule.is_enabled,
                    isRequired: true,
                    options: [
                      { value: true, label: 'Enabled' },
                      { value: false, label: 'Disabled' },
                    ],
                  }}
                  width={6}
                  name="is_enabled"
                  onChange={e => this.handleChangeType(e)}
                  placeholder={''}
                />
              )}
            </div>
          }
          footerElement={
            <div className='right-menu-footer'>
              <SquareButton
                content={`Save`}
                bsStyle={ButtonStyle.PRIMARY}
                onClick={e => this.onSaveClick(this.state.rule, true, false)}
                className="save"
              />
              <SquareButton
                content={`Cancel`}
                bsStyle={ButtonStyle.DEFAULT}
                onClick={e => this.clearPopUp()}
                className="save"
              />
            </div>
          }
          className="add-edit-rule"
        />
        {this.state.viewCDhistory &&
          <ViewRuleCDHistory
            show={this.state.viewCDhistory}
            onClose={this.toggleviewCDHistoryPopup}
          />
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  rules: state.configuration.rules,
  isFetching: state.configuration.isFetching,
  rule: state.configuration.rule,
  classifications: state.configuration.classifications,
  levels: state.configuration.levels,
  isFetchingList: state.configuration.isFetchingList,
  customers: state.customer.customersShort,
  customerId: state.customer.customerId,
  types: state.inventory.types,
  manufacturers: state.inventory.manufacturers,
  globalVariables: state.configuration.globalVariables,
  isPostingBatch: state.inventory.isPostingBatch,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchRules: (params?: ISPRulesFilterParams) => dispatch(fetchRules(params)),
  fetchClassifications: () => dispatch(fetchClassifications()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchRulesLevels: () => dispatch(fetchRulesLevels()),
  fetchSingleRule: (id: number) => dispatch(fetchSingleRule(id)),
  saveRule: (rule: any) => dispatch(saveRule(rule)),
  editRule: (rule: any) => dispatch(editRule(rule)),
  createClassifications: (text: string) =>
    dispatch(createClassifications(text)),
  deleteRule: (id: number) => dispatch(deleteRule(id)),
  fetchTypes: () => dispatch(fetchTypes()),
  fetchGlobalVariables: (params?: IServerPaginationParams) =>
    dispatch(fetchGlobalVariables(params)),
  fetchRuleFunctions: () => dispatch(fetchRuleFunctions()),
  fetchAllCustomerDevices: () => dispatch(fetchAllCustomerDevices()),
  rerunConfigCompliance: (data: any) => dispatch(rerunConfigCompliance(data)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GlobalRulesList);
