import React from 'react';
import { connect } from 'react-redux';
import _, { cloneDeep, debounce } from 'lodash';

import {
  fetchCollectorToken,
  fetchCustomerCollectors,
  fetchCustomerCollectorsPU,
  GET_CLT_TOKEN_SUCCESS,
  saveCustomerCollector,
  saveCustomerCollectorPU,
  fetchCollectorServiceTypes,
  saveCollectorsMapping,
  fetchSingleCollectorCU,
  fetchSingleCollectorPU,
  getDownloadURL,
  deleteCollector,
  DELETE_COLLECTOR_SUCCESS,
} from '../../actions/collector';
import SquareButton from '../../components/Button/button';
import IconButton from '../../components/Button/iconButton';
import ConfirmBox from '../../components/ConfirmBox/ConfirmBox';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { fromISOStringToDateTimeString } from '../../utils/CalendarUtil';
import CollectorDetails from './details';
import { addSuccessMessage } from '../../actions/appState';
import DeleteButton from '../../components/Button/deleteButton';
import { fetchManufacturers } from '../../actions/inventory';
import Checkbox from '../../components/Checkbox/checkbox';
import { commonFunctions } from '../../utils/commonFunctions';
import './style.scss';

interface ICollectorProps extends ICommonProps {
  fetchCustomerCollectors: any;
  fetchCollectorToken: any;
  fetchSingleCollectorCU: any;
  fetchSingleCollectorPU: any;
  saveCustomerCollector: any;
  collectors: ICollector[];
  customerCollectors: any;
  customerCollector: any;
  isFetchingCollectors: boolean;
  isFetchingCustomerCollectors: boolean;
  isFetchingCustomerCollector: boolean;
  isFetching: boolean;
  saveCustomerCollectorPU: any;
  fetchCustomerCollectorsPU: any;
  customerId: any;
  loggenInUser: any;
  addSuccessMessage: any;
  fetchManufacturers: TFetchManufacturers;
  manufacturers: any[];
  fetchCollectorServiceTypes: any;
  serviceTypes: any;
  isFetchingServiceTypes: boolean;
  saveCollectorsMapping: any;
  user: any;
  downloadURL: string,
  isFetchingURL: boolean;
  getDownloadURL: any;
  deleteCollector: any;
}

interface ICollectorState {
  customerCollector: ICustomerCollector;
  open: boolean;
  copied: boolean;
  error: {
    collector: IFieldValidation;
    customer: IFieldValidation;
    token: IFieldValidation;
    name: IFieldValidation;
  };
  isopenConfirm: boolean;
  customerCollectors?: ICustomerCollector[];
  rows: any[];
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  id?: any;
  isOpenDetails: boolean;
  download: boolean;
}

class CollectorList extends React.Component<ICollectorProps, ICollectorState> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  private debouncedFetch;

  constructor(props: ICollectorProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  getEmptyState = () => ({
    customerCollectors: [],
    rows: [],
    open: false,
    download: true,
    copied: false,
    customerCollector: {
      collector: '',
      customer: '',
      token: '',
      version: '',
      name: '',
    },
    error: {
      collector: { ...CollectorList.emptyErrorState },
      customer: { ...CollectorList.emptyErrorState },
      token: { ...CollectorList.emptyErrorState },
      name: { ...CollectorList.emptyErrorState },
    },
    isopenConfirm: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    reset: false,
    isFilterModalOpen: false,
    filters: {
      type: [],
      option: [],
    },
    isOpenDetails: false,
  });
  public columns: ITableColumn[] = [
    {
      accessor: 'id',
      Header: 'ID',
      width: 80,
      id: 'id',
      sortable: false,
      Cell: c => <div>{c.value}</div>,
    },
    {
      accessor: 'status',
      Header: 'Config Status',
      id: 'status',
      width: 95,
      sortable: false,
      Cell: c => <div className="text-capitalize">
        {c.value === 'validated' && (
          <img
            src={`/assets/icons/active-right.svg`}
            className="status-svg-images"
            alt=""
            title={'Validated'}
          />
        )}
        {c.value === 'pending' && (
          <img
            src={`/assets/icons/clock.svg`}
            alt=""
            className="status-svg-images"
            title={`Pending`}
          />
        )}
        {c.value === 'inactive' && (
          <img
            src={`/assets/icons/tick-gray.svg`}
            alt=""
            className="status-svg-images"
            title={`In Active`}
          />
        )}
        {c.value === 'active' && (
          <img
            src={`/assets/icons/tick.svg`}
            alt=""
            className="status-svg-images"
            title={'Active'}
          />
        )}
      </div>,
    },
    {
      accessor: 'name',
      Header: 'Name',
      id: 'name',
      sortable: false,
      Cell: c => <div>{c.value ? c.value : '-'}</div>,
    },
    {
      accessor: 'host_metadata',
      Header: 'Host Name',
      id: 'host_metadata',
      sortable: false,
      Cell: c => <a
        title={`OS type: ${
          c.value.os_type
            ? c.value.os_type
            : ' N.A.'
          },
Uptime: ${
          c.value.uptime
            ? c.value.uptime
            : ' N.A.'
          },`}>{c.value ? c.value.hostname : '-'}</a>,
    },
    {
      accessor: 'current_version',
      Header: 'Version',
      width: 120,
      id: 'current_version',
      sortable: false,
      Cell: c => <div>{c.value ? c.value : '-'}</div>,
    },
    {
      accessor: 'last_heart_beat',
      Header: 'Last Heart Beat',
      id: 'last_heart_beat',
      sortable: false,
      Cell: c => (
        <div>{c.value ? fromISOStringToDateTimeString(c.value) : '-'}</div>
      ),
    },
    {
      accessor: 'heartbeat_status',
      Header: 'Status',
      id: 'heartbeat_status',
      width: 70,
      sortable: false,
      Cell: c => <div className="text-capitalize">
        {c.value === 'Inactive' && (
          <div className="circle-animate-inactive" title="Inactive" />
        )}
        {c.value === 'Active' && (
          <div className="circle-animate-active" title="Active" />
        )}
        {c.value === 'Not Responding' && (
          <div className="circle-animate-not-responding" title="Not Responding" />
        )}
        {c.value === 'Not Available' && (
          <img
            src={`/assets/icons/cancel.svg`}
            alt=""
            className="status-heartbit-images"
            title={`Not Available`}
          />
        )}
      </div>,
    },
    {
      accessor: 'Query',
      Header: 'Query',
      width: 100,
      sortable: false,
      Cell: action => (
        <div>
          <SquareButton
            content="Queries"
            onClick={e => this.gotToQueries(action, e)}
            bsStyle={ButtonStyle.PRIMARY}
            className="queries-btn"
          />
        </div>
      ),
    },
    {
      accessor: 'id',
      Header: 'Actions',
      width: 120,
      sortable: false,
      Cell: cell => (
        <div>
          <IconButton
            className="action-button-clt"
            icon="download-button.png"
            onClick={e => this.downloadCollectorWithEvent(this.props.downloadURL, e)}
            title={'Download Collector'}
          />
          <CopyToClipboard text={cell.original.token}
            onCopy={() => this.props.addSuccessMessage('Token Copied')}>
            <DeleteButton
              type="cloned"
              title="Copy Token"
              onClick={e => e.stopPropagation()}
            />
          </CopyToClipboard>
          {(cell.original.heartbeat_status === 'Inactive' || cell.original.heartbeat_status === 'Not Available') && (
            <DeleteButton onClick={e => this.onDeleteRowClick(cell, e)} />
          )}
        </div>
      ),
    },
  ];
  componentDidMount() {
    if (!this.props.manufacturers) {
      this.props.fetchManufacturers();
    }
    if (this.props.serviceTypes.length === 0) {
      this.props.fetchCollectorServiceTypes();
    }
    if (!this.props.downloadURL) {
      this.props.getDownloadURL();
    }
  }

  componentDidUpdate(prevProps: ICollectorProps) {
    if (
      this.props.customerCollectors &&
      this.props.customerCollectors !== prevProps.customerCollectors
    ) {
      this.setRows(this.props);
    }
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchCustomerCollectorsPU(this.props.customerId);
    }
    if (
      this.props.loggenInUser &&
      this.props.loggenInUser !== prevProps.loggenInUser
    ) {
      if (_.get(this.props.loggenInUser, "type") === "customer") {
        this.props.fetchCustomerCollectors();
      }
      if (
        _.get(this.props.loggenInUser, "type") === "provider" &&
        prevProps.customerId
      ) {
        this.props.fetchCustomerCollectorsPU(prevProps.customerId);
      }
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params?: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));
    if (_.get(this.props.loggenInUser, 'type') === 'customer') {
      this.props.fetchCustomerCollectors(newParams);
    }
    if (_.get(this.props.loggenInUser, 'type') === 'provider' && this.props.customerId) {
      this.props.fetchCustomerCollectorsPU(this.props.customerId, newParams);
    }
  };
  onSearchListChange = (e?: any) => {
    const search = e &&  e.target.value ?  e.target.value : this.state.pagination.params.search;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: ICollectorProps) => {
    const customersResponse = nextProps.customerCollectors;
    const customerCollectors: any[] = customersResponse.results;
    const rows: any[] =
      customerCollectors &&
      customerCollectors.map((customerCollector, index) => ({
        collector: customerCollector.collector,
        current_version: customerCollector.current_version,
        token: customerCollector.token,
        name: customerCollector.name,
        last_heart_beat: customerCollector.last_heart_beat,
        status: customerCollector.status,
        heartbeat_status: customerCollector.heartbeat_status,
        customer: customerCollector.customer,
        created_on: customerCollector.created_on,
        download_link:
          customerCollector.collector &&
          customerCollector.collector.download_link,
        id: customerCollector.id,
        integrations: customerCollector.integrations,
        host_metadata: customerCollector.host_metadata,
        index,
      }));

    this.setState(prevState => ({
      reset: false,
      rows,
      customerCollectors,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onRowClick = rowInfo => {
    event.stopPropagation();
    // tslint:disable-next-line: max-line-length
    this.setState({ isOpenDetails: true, id: rowInfo.original.id });
    if (_.get(this.props.loggenInUser, 'type') === 'customer') {
      this.props.fetchSingleCollectorCU(rowInfo.original.id);
    } else {
      this.props.fetchSingleCollectorPU(this.props.customerId, rowInfo.original.id);
    }
  };

  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteCollector(this.state.id, this.props.customerId).then(action => {
      if (action.type === DELETE_COLLECTOR_SUCCESS) {
        this.setState({
          reset: true,
        });
        this.debouncedFetch({page: 1});
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: '',
    });
  };

  handleChange = e => {
    const newState = cloneDeep(this.state);
    (newState.customerCollector[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.customerCollector as ICustomerCollector) = this.getEmptyState().customerCollector;
    (newState.error as any) = this.getEmptyState().error;
    (newState.isopenConfirm as any) = false;
    (newState.copied as any) = false;
    this.setState(newState);
  };

  validateForm() {
    const error: any = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.customerCollector.name || this.state.customerCollector.name.trim().length === 0) {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage = 'Please enter name';
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }
  onSaveClick = () => {
    if (this.validateForm()) {
      const customerCollector = this.state.customerCollector;
      if (_.get(this.props.loggenInUser, 'type') === 'customer') {
        this.props.saveCustomerCollector(customerCollector).then(action => {
          if (action.type === 'UPDATE_CUS_COLLTS_SUCCESS') {
            if (this.state.download) {
              const url = this.props.downloadURL
              this.downloadCollector(url);
            }
            this.props.fetchCustomerCollectors();
            this.clearPopUp();
          }
          if (action.type === 'UPDATE_CUS_COLLTS_FAILURE') {
            this.setValidationErrors(action.errorList.data);
          }
        });
      } else {
        this.props
          .saveCustomerCollectorPU(this.props.customerId, customerCollector)
          .then(action => {
            if (action.type === 'UPDATE_CUS_COLLTS_SUCCESS') {
              if (this.state.download) {
                const url = this.props.downloadURL;
                this.downloadCollector(url);
              }
              this.props.fetchCustomerCollectorsPU(this.props.customerId);
              this.clearPopUp();
            }
            if (action.type === 'UPDATE_CUS_COLLTS_FAILURE') {
              this.setValidationErrors(action.errorList.data);
            }
          });
      }
    }
  };

  setValidationErrors = errorList => {
    const newState: ICollectorState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };


  onAddClick = () => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    this.setState(newState);

    this.props.fetchCollectorToken().then(action => {
      if (action.type === GET_CLT_TOKEN_SUCCESS) {
        const nw = cloneDeep(this.state);
        (nw.customerCollector.token as any) =
          action.response && action.response.token;
        this.setState(nw);
      }
    });
  };

  downloadCollectorWithEvent = (urlObj, e) => {
    const url = urlObj && urlObj.download_url;
    e.stopPropagation();
    const link = document.createElement('a');
    link.href = url;
    link.target = '_blank';
    link.setAttribute('download', url.substring(url.lastIndexOf('/') + 1));
    document.body.appendChild(link);
    link.click();
  };
  downloadCollector = urlObj => {
    const url = urlObj && urlObj.download_url;
    const link = document.createElement('a');
    link.href = url;
    link.target = '_blank';
    link.setAttribute('download', url.substring(url.lastIndexOf('/') + 1));
    document.body.appendChild(link);
    link.click();
  };
  renderTopBar = () => {
    return (
      <div className={'configuration-management__table-top '}>
        <div className="collector__table-actions header-panel">
          <SquareButton
            onClick={e => this.onSearchListChange()}
            content={'Reload'}
            bsStyle={ButtonStyle.PRIMARY}
            className="add-collector"
            disabled={
              _.get(this.props.loggenInUser, 'type') === 'provider' &&
                !this.props.customerId
                ? true
                : false
            }
          />
          <SquareButton
            onClick={this.onAddClick}
            content={'+ Add Collector'}
            bsStyle={ButtonStyle.PRIMARY}
            className="add-collector"
            disabled={
              _.get(this.props.loggenInUser, 'type') === 'provider' &&
                !this.props.customerId
                ? true
                : false
            }
          />
        </div>
      </div>
    );
  };
  toggleViewViolation = () => {
    this.setState(prevState => ({
      isOpenDetails: !prevState.isOpenDetails,
    }));
  };

  onSaveCollectorMappingSolarwinds = (collector, integrations) => {
    const data = {
      "meta_config": integrations[0].meta_config
    }
    this.props.saveCollectorsMapping(
      this.props.customerId,
      collector.id,
      collector.integrations[0].id,
      data
    ).then(action => {
      if (action.type === 'SAVE_COLL_MAPPING_SUCCESS') {
        this.fetchData();
        this.setState(prevState => ({
          isOpenDetails: !prevState.isOpenDetails,
        }));
      }
    });

  };
  gotToQueries = (data: any, event: any) => {
    event.stopPropagation();
    const url = `/collector/${data.row.id}/queries`;
    this.props.history.push(url)
  };

  handleChangeCheckBox = (event: any) => {
    const targetValue = event.target.checked;
    this.setState(prevState => ({ download: targetValue }));
  };
  render() {

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };

    return (
      <div className="customer-collector-listing-parent">
        <h3>Collectors</h3>

        <div className="loader">
          <Spinner show={this.props.isFetchingCustomerCollectors} />
        </div>
        <Table
          columns={this.columns}
          rows={this.state.rows}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetching ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingCustomerCollectors}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetchingURL}
        />
        <ModalBase
          show={this.state.open}
          onClose={() => this.clearPopUp(!this.state.open)}
          titleElement={` ${
            this.state.customerCollector.id ? 'Update' : 'Add'
            } Customer Collector`}
          bodyElement={
            <div className="col-md-12 body">
              <div className="loader">
                <Spinner
                  show={
                    this.props.isFetching ||
                    this.props.isFetchingCollectors
                  }
                />
              </div>
              <Input
                field={{
                  label: 'Name',
                  type: InputFieldType.TEXT,
                  value: this.state.customerCollector.name,
                  isRequired: true,
                }}
                width={12}
                name="name"
                onChange={e => this.handleChange(e)}
                error={this.state.error.name}
                placeholder={`Enter name`}
              />
              <Input
                field={{
                  label: 'Token',
                  type: InputFieldType.TEXT,
                  value: this.state.customerCollector.token,
                  isRequired: true,
                }}
                width={9}
                name="token"
                onChange={e => null}
                error={this.state.error.customer}
                placeholder={`Token`}
              />
              <CopyToClipboard
                className="copy-token-btn" text={this.state.customerCollector.token}
                onCopy={() => this.setState({ copied: true })}>
                <SquareButton
                  content={this.state.copied ? <span style={{ color: 'green' }}>Copied.</span> : 'Copy Token'}
                  bsStyle={ButtonStyle.DEFAULT}
                  className=""
                />
              </CopyToClipboard>
              <div className="col-md-12" style={{ margin: '5px' }}>

                <Checkbox
                  isChecked={this.state.download}
                  name="loa"
                  onChange={e => this.handleChangeCheckBox(e)}
                >
                  Download Collector
            </Checkbox>
              </div>
              <div className="col-md-12">
                {' '}
                <SquareButton
                  content={'Save'}
                  bsStyle={ButtonStyle.PRIMARY}
                  onClick={e => this.onSaveClick()}
                  className="download-exe-btn"
                />


              </div>
            </div>
          }
          footerElement={null}
          className="add-edit-customerCollector"
        />
        <CollectorDetails
          {...this.props}
          show={this.state.isOpenDetails}
          onClose={this.toggleViewViolation}
          onSave={this.onSaveCollectorMappingSolarwinds}
          collector={this.props.customerCollector}
          isFetching={this.props.isFetchingCustomerCollector}
          manufacturers={this.props.manufacturers}
          serviceTypes={this.props.serviceTypes}
          user={this.props.user}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  collectors: state.collector.collectors,
  customerCollectors: state.collector.customerCollectors,
  customerCollector: state.collector.customerCollector,
  isFetchingCollectors: state.collector.isFetchingCollectors,
  isFetchingCustomerCollectors: state.collector.isFetchingCustomerCollectors,
  isFetchingCustomerCollector: state.collector.isFetchingCustomerCollector,
  isFetching: state.collector.isFetching,
  downloadURL: state.collector.downloadURL,
  isFetchingURL: state.collector.isFetchingURL,
  customerId: state.customer.customerId,
  loggenInUser: state.profile.user,
  manufacturers: state.inventory.manufacturers,
  serviceTypes: state.collector.serviceTypes,
  isFetchingServiceTypes: state.collector.isFetchingServiceTypes,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchCustomerCollectors: (params?: IServerPaginationParams) =>
    dispatch(fetchCustomerCollectors(params)),
  getDownloadURL: () => dispatch(getDownloadURL()),
  fetchCollectorToken: () => dispatch(fetchCollectorToken()),
  saveCustomerCollector: (collector: any) =>
    dispatch(saveCustomerCollector(collector)),
  deleteCollector: (collectorId: number, customerId?: number) => dispatch(deleteCollector(collectorId, customerId)),
  saveCustomerCollectorPU: (id: any, collector: any) =>
    dispatch(saveCustomerCollectorPU(id, collector)),
  fetchCustomerCollectorsPU: (id: any, params?: IServerPaginationParams) =>
    dispatch(fetchCustomerCollectorsPU(id, params)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchCollectorServiceTypes: () => dispatch(fetchCollectorServiceTypes()),
  saveCollectorsMapping: (custId: any,
    collectortId: any,
    intId: any,
    data: any) => dispatch(saveCollectorsMapping(custId, collectortId, intId, data)),
  fetchSingleCollectorPU: (id: string, collectortId: number) => dispatch(fetchSingleCollectorPU(id, collectortId)),
  fetchSingleCollectorCU: (collectortId: number) => dispatch(fetchSingleCollectorCU(collectortId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CollectorList);
