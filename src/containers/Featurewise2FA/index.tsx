import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { userTwoFAStatus, PHONE_VALIDATE_SUCCESS } from "../../actions/auth";
import TwoFAModal from "./twoFAModal";
import PhoneVerifyModal from "./phoneVerifyModal";
import "./style.scss";

interface Featurewise2FAProps {
  user: ISuperUser;
  onClose: (verificationCode?: string) => void;
  userTwoFAStatus: (userId: string) => Promise<any>;
}

const Featurewise2FA: React.FC<Featurewise2FAProps> = (props) => {
  const [redirectKey, setRedirectKey] = useState<number>(Math.random());
  const [isTwoFAEnabled, setIsTwoFAEnabled] = useState<boolean>(true);

  useEffect(() => {
    if (props.user) checkForRegisteredPhoneNo();
  }, [redirectKey, props.user]);

  const redirectToTwoFA = () => {
    setRedirectKey(Math.random());
  };

  const checkForRegisteredPhoneNo = () => {
    props.userTwoFAStatus(props.user.id).then((action) => {
      if (action.type === PHONE_VALIDATE_SUCCESS) {
        setIsTwoFAEnabled(action.response.two_fa_configured);
        sessionStorage.setItem("eyJ0eX1QiLCJ2fa", action.response.token);
      }
    });
  };

  return isTwoFAEnabled ? (
    <TwoFAModal onClose={props.onClose} />
  ) : (
    <PhoneVerifyModal onClose={props.onClose} redirect={redirectToTwoFA} />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  userTwoFAStatus: (userId: string) => dispatch(userTwoFAStatus(userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Featurewise2FA);
