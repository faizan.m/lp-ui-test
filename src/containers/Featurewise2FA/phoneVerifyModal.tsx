import React, { useState } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import {
  validatePhone,
  requestPhoneVerification,
  PHONE_VALIDATE_SUCCESS,
  PHONE_VALIDATE_FAILURE,
  REQUEST_PHONE_VALID_SUCCESS,
  REQUEST_PHONE_VALID_FAILURE,
} from "../../actions/auth";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import Validator from "../../utils/validator";
import { countryCodes } from "../../utils/countryCodes";

interface PhoneVerificationModalProps {
  onClose: () => void;
  redirect: () => void;
  validatePhone: (data: {
    code: string;
    phone: string;
    country_code: string;
  }) => Promise<any>;
  requestPhoneVerification: (data: {
    country_code: string;
    phone: string;
  }) => Promise<any>;
}

interface ErrorInterface {
  otp: IFieldValidation;
  phone: IFieldValidation;
  countryCode: IFieldValidation;
}

const getErrorState = (msg?: string) => {
  return {
    errorState: msg ? IValidationState.ERROR : IValidationState.SUCCESS,
    errorMessage: msg ? msg : "",
  };
};

const CountryCodes: IPickListOptions[] = countryCodes.map((country) => ({
  value: country.code,
  label: `${country.code} (${country.name})`,
}));

const validator = new Validator();

const PhoneVerificationModal: React.FC<PhoneVerificationModalProps> = (
  props
) => {
  const [otp, setOTP] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [editable, setEditable] = useState<boolean>(true);
  const [fetching, setFetching] = useState<boolean>(false);
  const [countryCode, setCountryCode] = useState<string>("");
  const [resendButtonEnable, setResendButtonEnable] = useState<boolean>(true);
  const [requestComplete, setRequestComplete] = useState<boolean>(false);
  const [error, setError] = useState<ErrorInterface>({
    otp: getErrorState(),
    phone: getErrorState(),
    countryCode: getErrorState(),
  });

  const handleChangeOTP = (e: React.ChangeEvent<HTMLInputElement>) => {
    setOTP(e.target.value.trim());
  };

  const handleChangePhone = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(e.target.value.trim());
  };

  const handleChangeCountryCode = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCountryCode(e.target.value);
  };

  const editPhone = () => {
    setOTP("");
    setPhone("");
    setEditable(true);
    setRequestComplete(false);
  };

  const validatePhoneForm = (): boolean => {
    let isValid = true;
    const errors = cloneDeep(error);
    cloneDeep(error);

    if (!phone) {
      errors.phone = getErrorState("Enter phone number");
      isValid = false;
    }
    if (!countryCode) {
      errors.countryCode = getErrorState("Enter country code");
      isValid = false;
    }
    if (editable && phone && !validator.isValidPhoneNumber(phone)) {
      errors.phone = getErrorState("Enter valid phone number");
      isValid = false;
    }
    if (!editable && requestComplete && !otp) {
      errors.otp = getErrorState("Enter OTP");
      isValid = false;
    }

    setError(errors);
    return isValid;
  };

  const requestPhone = () => {
    if (validatePhoneForm()) {
      setFetching(true);
      setResendButtonEnable(false);
      props
        .requestPhoneVerification({
          phone: phone,
          country_code: countryCode,
        })
        .then((action) => {
          if (action.type === REQUEST_PHONE_VALID_SUCCESS) {
            setRequestComplete(true);
            setError({
              otp: getErrorState(),
              phone: getErrorState(),
              countryCode: getErrorState(),
            });
            setTimeout(() => setResendButtonEnable(true), 10000);
          } else if (action.type === REQUEST_PHONE_VALID_FAILURE) {
            setError((prevState) => ({
              ...prevState,
              phone: getErrorState("Enter valid phone"),
            }));
          }
        })
        .finally(() => setFetching(false));
    }
  };

  const validatePhone = () => {
    if (countryCode && phone) {
      if (validatePhoneForm()) {
        setFetching(true);
        props
          .validatePhone({
            code: otp,
            phone: phone,
            country_code: countryCode,
          })
          .then((action) => {
            if (action.type === PHONE_VALIDATE_SUCCESS) {
              if (action.response.success === true) {
                sessionStorage.removeItem("eyJ0eX1QiLCJ2fa");
                props.redirect();
              } else {
                setError((prevState) => ({
                  ...prevState,
                  otp: getErrorState("Invalid OTP"),
                }));
              }
            } else if (action.type === PHONE_VALIDATE_FAILURE) {
              setError((prevState) => ({
                ...prevState,
                otp: getErrorState("Invalid OTP"),
              }));
            }
          })
          .finally(() => setFetching(false));
      }
    }
  };

  const getBody = () => {
    return (
      <div className="validate-phone-no-form">
        <Spinner show={fetching} />
        <h3>
          Please verify your phone number to start with Two-factor
          authentication.
        </h3>
        <div className="phone-section">
          <Input
            field={{
              label: "Country code",
              isRequired: false,
              type: InputFieldType.PICKLIST,
              options: CountryCodes,
              value: countryCode,
            }}
            error={error.countryCode}
            width={5}
            placeholder="Enter country code"
            name="countryCode"
            onChange={handleChangeCountryCode}
            className="country-code"
            disabled={requestComplete || !editable}
          />
          <Input
            field={{
              label: "Phone number",
              type: InputFieldType.TEXT,
              isRequired: false,
              value: phone,
            }}
            width={7}
            error={error.phone}
            className="phone-number"
            placeholder="Enter phone number"
            name="phone"
            onChange={handleChangePhone}
            disabled={requestComplete || !editable}
          />
        </div>
        {requestComplete && (
          <div className="otp-section">
            <Input
              field={{
                label: "OTP",
                type: InputFieldType.TEXT,
                isRequired: false,
                value: otp,
              }}
              error={error.otp}
              width={7}
              placeholder="Enter OTP"
              name="otp"
              className="otp"
              onChange={handleChangeOTP}
            />
            <SquareButton
              bsStyle={
                requestComplete ? ButtonStyle.OUTLINE : ButtonStyle.PRIMARY
              }
              content={"Resend OTP"}
              onClick={requestPhone}
              className="resend-otp"
              disabled={!resendButtonEnable}
            />
          </div>
        )}
      </div>
    );
  };

  const getFooter = () => {
    return (
      <>
        <SquareButton
          onClick={() => props.onClose()}
          content={"Close"}
          bsStyle={ButtonStyle.DEFAULT}
        />
        {requestComplete ? (
          <SquareButton
            onClick={validatePhone}
            content={"Verify"}
            bsStyle={ButtonStyle.PRIMARY}
            disabled={!otp}
          />
        ) : (
          <SquareButton
            bsStyle={ButtonStyle.PRIMARY}
            content={"Get OTP"}
            onClick={requestPhone}
          />
        )}
        {(requestComplete || !editable) && (
          <SquareButton
            bsStyle={ButtonStyle.OUTLINE}
            content="Edit"
            onClick={editPhone}
          />
        )}
      </>
    );
  };

  return (
    <ModalBase
      show={true}
      hideCloseButton={true}
      onClose={() => props.onClose()}
      titleElement={"Phone Number Verification"}
      bodyElement={getBody()}
      footerElement={getFooter()}
      className="phone-verification-modal"
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  requestPhoneVerification: (data: { country_code: string; phone: string }) =>
    dispatch(requestPhoneVerification(data)),
  validatePhone: (data: {
    code: string;
    phone: string;
    country_code: string;
  }) => dispatch(validatePhone(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhoneVerificationModal);
