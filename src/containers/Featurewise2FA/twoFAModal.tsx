import React, { useState } from "react";
import { connect } from "react-redux";
import isNumeric from "validator/lib/isNumeric";
import { verifyAuthyToken, TWOFA_REQUEST_SUCCESS } from "../../actions/auth";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import "./style.scss";

interface TwoFAModalProps {
  onClose: (verificationCode?: string) => void;
  verifyAuthyToken: (otp: string) => Promise<any>;
}

const getErrorState = (msg?: string) => {
  return {
    errorState: msg ? IValidationState.ERROR : IValidationState.SUCCESS,
    errorMessage: msg ? msg : "",
  };
};

const TwoFAModal: React.FC<TwoFAModalProps> = (props) => {
  const [code, setCode] = useState<string>("");
  const [sending, setSending] = useState<boolean>(false);
  const [error, setError] = useState<IFieldValidation>({
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  });

  const validate = (): boolean => {
    let isValid = true;
    if (!isNumeric(code) || code.length !== 7) {
      isValid = false;
      setError(getErrorState("Verification code should be a 7-digit number"));
    } else {
      setError(getErrorState());
    }
    return isValid;
  };

  const handleChangeCode = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCode(e.target.value.trim());
  };

  const onSubmit = () => {
    if (validate()) {
      setSending(true);
      props
        .verifyAuthyToken(code)
        .then((action) => {
          if (action.type === TWOFA_REQUEST_SUCCESS) {
            if (action.response.is_valid) {
              props.onClose(action.response.token);
            } else {
              setError(getErrorState("Invalid Token"));
            }
          }
        })
        .finally(() => setSending(false));
    }
  };

  const getBody = () => {
    return (
      <div className="featurewise-2fa-container">
        <Spinner show={sending} />
        <h3>
          Please verify your Two-Factor Authentication credentials using the
          token from Authy App
        </h3>
        <Input
          field={{
            value: code,
            label: null,
            type: InputFieldType.TEXT,
            isRequired: false,
          }}
          width={6}
          name={"verification_code"}
          placeholder="Enter 7-digit token"
          onChange={handleChangeCode}
          className="two-fa-code-input"
          error={error}
          disabled={sending}
        />
      </div>
    );
  };

  const getFooter = () => {
    return (
      <>
        <SquareButton
          onClick={() => props.onClose()}
          content={"Close"}
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={onSubmit}
          content={"Verify"}
          bsStyle={ButtonStyle.PRIMARY}
        />
      </>
    );
  };

  return (
    <ModalBase
      hideCloseButton={true}
      show={true}
      onClose={() => props.onClose()}
      titleElement={"Two-factor authentication (2FA)"}
      bodyElement={getBody()}
      footerElement={getFooter()}
      className="two-fa-verification-modal"
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  verifyAuthyToken: (otp: string) => dispatch(verifyAuthyToken(otp)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TwoFAModal);
