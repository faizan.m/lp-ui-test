import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  loginSuccess,
  PHONE_VALIDATE_FAILURE,
  PHONE_VALIDATE_SUCCESS,
  REQUEST_PHONE_VALID_FAILURE,
  REQUEST_PHONE_VALID_SUCCESS,
  requestPhoneVerification,
  TWOFA_FAILURE,
  TWOFA_REQUEST_FAILURE,
  TWOFA_REQUEST_SUCCESS,
  TWOFA_SUCCESS,
  twoFARequestAuth,
  twoFAValidateAuth,
  validatePhone,
} from '../../actions/auth';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import { isValidVal } from '../../utils/CommonUtils';

import { fetchLogoColor } from '../../actions/provider';
import Spinner from '../../components/Spinner';
import {
  getUserProfile,
  isLoggedIn,
  storeUserProfile,
} from '../../utils/AuthUtil';
import { countryCodes } from '../../utils/countryCodes';
import Validator from '../../utils/validator';
import './style.scss';
import { phoneNumberInFormat } from '../../utils/CalendarUtil';

interface ITWOFAProps extends ICommonProps {
  twoFAValidateAuth: any; // TODO:
  logo_url: string;
  themeColor: string;
  fetchLogoColor: any;
  updated_on: string;
  loginSuccess: any;
  twoFARequestAuth: any;
  requestPhoneVerification: any;
  validatePhone: any;
}

interface ITWOFAState {
  type: string;
  twoFACode: string;
  isFormValid: boolean;
  requestComplete: boolean;
  editable: boolean;
  resendButtonEnable: boolean;
  enablebutton: boolean;
  fetching: boolean;
  error: {
    twoFACode: IFieldValidation;
    countryCode: IFieldValidation;
    phone: IFieldValidation;
    otp: IFieldValidation;
  };
  logo_url: string;
  themeColor: string;
  otp: string;
  phone: string;
  countryCode: string;
}

class TWOFA extends React.Component<ITWOFAProps, ITWOFAState> {
  static validator = new Validator();

  constructor(props: ITWOFAProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    type: 'AUTHY_APP',
    twoFACode: '',
    isFormValid: true,
    requestComplete: false,
    editable: true,
    enablebutton: false,
    resendButtonEnable: true,
    fetching: false,
    logo_url: '',
    themeColor: '',
    countryCode: '1',
    phone: '',
    otp: '',
    error: {
      twoFACode: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      countryCode: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      phone: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      otp: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
    },
  });

  componentDidMount() {
    this.props.fetchLogoColor();
    const tempToken = sessionStorage.getItem('eyJ0eX1QiLCJ2fa');
    const countryCode = sessionStorage.getItem('countryCode');
    const phone = sessionStorage.getItem('phone');
    if (phone && phone.length > 9) {
      this.setState({
        phone,
        countryCode: countryCode ? countryCode : '1',
        editable: false,
      });
    }
    if (tempToken && tempToken.length > 200) {
      console.info('');
    } else {
      this.props.history.push('/login');
    }
    if (isLoggedIn()) {
      this.redirectUser();
    }
  }

  componentDidUpdate(prevProps: ITWOFAProps) {
    if (this.props.updated_on !== prevProps.updated_on) {
      this.setState({
        themeColor: this.props.themeColor,
        logo_url: `${this.props.logo_url}`,
      });
      if (isValidVal(this.props.themeColor)) {
        document.documentElement.style.setProperty(
          '--primary',
          this.props.themeColor
        );
      }
    }
  }  

  clearValidationStatus() {
    const cleanState = {
      twoFACode: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      countryCode: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      phone: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      otp: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
    };

    const newState: ITWOFAState = cloneDeep(this.state);
    newState.error = cleanState;

    this.setState(newState);
  }

  validateForm() {
    const newState: ITWOFAState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.twoFACode) {
      newState.error.twoFACode.errorState = IValidationState.ERROR;
      newState.error.twoFACode.errorMessage = 'Enter Token';

      isValid = false;
    }

    if (this.state.twoFACode && this.state.twoFACode.length !== 7) {
      newState.error.twoFACode.errorState = IValidationState.ERROR;
      newState.error.twoFACode.errorMessage = 'Invalid Token';

      isValid = false;
    }
    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;
    (newState.requestComplete as boolean) = false;
    (newState.twoFACode as any) = '';
    (newState.error as any) = this.getEmptyState().error;
    this.setState(newState);
  };

  handleChangeCode = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;
    this.setState(newState);
  };

  editPhone = (event: any) => {
    const newState = cloneDeep(this.state);
    (newState.requestComplete as boolean) = false;
    (newState.enablebutton as boolean) = false;
    (newState.editable as boolean) = true;
    (newState.phone as any) = '';
    (newState.otp as any) = '';
    this.setState(newState);
  };

  validate = e => {
    if (this.validateForm()) {
      this.setState({ fetching: true });

      this.props
        .twoFAValidateAuth({
          code: this.state.twoFACode,
          type: this.state.type,
        })
        .then(action => {
          if (action.type === TWOFA_SUCCESS) {
            storeUserProfile(action.response);
            this.setState({ fetching: false });
            this.redirectUser();
            sessionStorage.removeItem('eyJ0eX1QiLCJ2fa');
            sessionStorage.removeItem('is_two_fa_enabled');
            sessionStorage.removeItem('phone');
          } else if (action.type === TWOFA_FAILURE) {
            this.clearValidationStatus();
            const newState: ITWOFAState = this.state;
            newState.error.twoFACode.errorState = IValidationState.ERROR;
            newState.error.twoFACode.errorMessage = 'Invalid Token';
            newState.fetching = false;
            this.setState(newState);
          }
        });
    }
  };

  request = e => {
    this.setState({ fetching: true, resendButtonEnable: false });

    this.props
      .twoFARequestAuth({
        code: this.state.twoFACode,
        type: this.state.type,
      })
      .then(action => {
        if (action.type === TWOFA_REQUEST_SUCCESS) {
          if (action.response && action.response.approval_request) {
            this.props
              .twoFAValidateAuth({
                code: action.response.approval_request.uuid,
                type: this.state.type,
              })
              .then(a => {
                if (a.type === TWOFA_SUCCESS) {
                  storeUserProfile(a.response);
                  this.setState({ fetching: false });
                  this.redirectUser();
                  sessionStorage.removeItem('eyJ0eX1QiLCJ2fa');
                  sessionStorage.removeItem('is_two_fa_enabled');
                  sessionStorage.removeItem('phone');
                  sessionStorage.removeItem('countryCode');
                } else if (a.type === TWOFA_FAILURE) {
                  const newState: ITWOFAState = this.state;
                  newState.error.twoFACode.errorState = IValidationState.ERROR;
                  newState.error.twoFACode.errorMessage =
                    'Something went wrong';
                  newState.fetching = false;
                  this.setState(newState);
                }
              });
          } else {
            this.setState({ requestComplete: true, fetching: false });
            setTimeout(
              () => this.setState({ resendButtonEnable: true }),
              10000
            );
          }
        } else if (action.type === TWOFA_REQUEST_FAILURE) {
          this.setState({ requestComplete: false, fetching: false });
        }
      });
  };
  validatePhoneForm() {
    const newState: ITWOFAState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.phone) {
      newState.error.phone.errorState = IValidationState.ERROR;
      newState.error.phone.errorMessage = 'Enter phone number';

      isValid = false;
    }
    if (
      this.state.editable &&
      this.state.phone &&
      !TWOFA.validator.isValidPhoneNumber(this.state.phone)
    ) {
      newState.error.phone.errorState = IValidationState.ERROR;
      newState.error.phone.errorMessage = 'Enter valid phone number';

      isValid = false;
    }

    if (!this.state.editable && this.state.requestComplete && !this.state.otp) {
      newState.error.otp.errorState = IValidationState.ERROR;
      newState.error.otp.errorMessage = 'Enter OTP';

      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  requestPhone = e => {
    this.clearValidationStatus();
    if (this.validatePhoneForm()) {
      this.setState({ fetching: true, resendButtonEnable: false });
      this.props
        .requestPhoneVerification({
          phone: this.state.phone,
          country_code: this.state.countryCode,
        })
        .then(action => {
          if (action.type === REQUEST_PHONE_VALID_SUCCESS) {
            this.setState({
              error: this.getEmptyState().error,
              requestComplete: true,
              fetching: false,
            });
            setTimeout(
              () => this.setState({ resendButtonEnable: true }),
              10000
            );
          } else if (action.type === REQUEST_PHONE_VALID_FAILURE) {
            const newState: ITWOFAState = this.state;
            newState.error.phone.errorState = IValidationState.ERROR;
            newState.error.phone.errorMessage = 'Enter valid phone';
            newState.fetching = false;
            this.setState(newState);
          }
        });
    }
  };

  validatePhone = e => {
    if (this.state.countryCode && this.state.phone) {
      if (this.validatePhoneForm()) {
        this.setState({ fetching: true });
        this.props
          .validatePhone({
            code: this.state.otp,
            phone: this.state.phone,
            country_code: this.state.countryCode,
          })
          .then(action => {
            if (action.type === PHONE_VALIDATE_SUCCESS) {
              if (action.response.success === true) {
                sessionStorage.setItem('is_two_fa_enabled', 'true');
                this.clearValidationStatus();
              } else {
                this.clearValidationStatus();
                const newState: ITWOFAState = this.state;
                newState.error.otp.errorState = IValidationState.ERROR;
                newState.error.otp.errorMessage = 'Invalid OTP';
                newState.fetching = false;
                this.setState(newState);
              }
              this.setState({ fetching: false });
            } else if (action.type === PHONE_VALIDATE_FAILURE) {
              this.clearValidationStatus();
              const newState: ITWOFAState = this.state;
              newState.error.otp.errorState = IValidationState.ERROR;
              newState.error.otp.errorMessage = 'Invalid OTP';
              newState.fetching = false;
              this.setState(newState);
            }
          });
      }
    }
  };

  onKeyDown = e => {
    if (e.key === 'Enter') {
      this.validate(e);
    }
  };
  redirectUser = () => {
    const userProfile: IUserProfile = getUserProfile();
    this.props.loginSuccess(userProfile);
    this.props.history.push(userProfile.default_landing_page);
  };

  getCountryCodes = () => {
    if (countryCodes && countryCodes.length > 0) {
      return countryCodes.map(country => ({
        value: country.code,
        label: `${country.code} (${country.name})`,
      }));
    } else {
      return [];
    }
  };
  render() {
    const isTwoFAEnabled = sessionStorage.getItem('is_two_fa_enabled');

    return (
      <div className="login-container login-container--forgot-password">
        <div className={this.state.fetching ? 'loader' : ''}>
          <Spinner show={this.state.fetching} />
        </div>
        <div className="login-form two-fa" onKeyDown={this.onKeyDown}>
          <div
            className="login-form__logo"
            style={{
              backgroundImage: `url(${
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              })`,
            }}
          >
            {/* <img
              src={
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}?${new Date().getTime()}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              }
            /> */}
          </div>
          {isTwoFAEnabled === 'true' && (
            <div className="login-form__fields">
              <h3 className="heading-one">Two-factor authentication (2FA)</h3>
              <h5 className="heading-two"> Keep your account safe</h5>
              <p className="heading-three">
                Please verify your Two-Factor Authentication credentials using
                the Authy App or a verification code.
              </p>
              <div>
                <label className="radio-row" htmlFor="type">
                  <input
                    type="radio"
                    name="type"
                    value="AUTHY_NOTIFY"
                    checked={this.state.type === 'AUTHY_NOTIFY'}
                    onChange={this.handleChange}
                  />{' '}
                  Verify using OneTouch Verification (Authy app)
                </label>
                <label className="radio-row" htmlFor="type">
                  <input
                    type="radio"
                    name="type"
                    value="AUTHY_APP"
                    checked={this.state.type === 'AUTHY_APP'}
                    onChange={this.handleChange}
                  />{' '}
                  Verify using Authy Token (Authy app)
                </label>
                {/* {this.state.type === 'AUTHY_APP' && (
                  <Input
                    field={{
                      label: 'Code',
                      type: InputFieldType.TEXT,
                      isRequired: false,
                      value: this.state.twoFACode,
                    }}
                    error={this.state.error.twoFACode}
                    width={12}
                    placeholder="Enter your 7-digit Authy Token"
                    name="twoFACode"
                    onChange={this.handleChangeCode}
                  />
                )} */}

                <label className="radio-row" htmlFor="type">
                  <input
                    type="radio"
                    name="type"
                    value="CALL"
                    checked={this.state.type === 'CALL'}
                    onChange={this.handleChange}
                  />{' '}
                  Get a code via phone call to: {phoneNumberInFormat(this.state.countryCode,this.state.phone)}{' '}
                </label>
                {/* {this.state.type === 'CALL' &&
                  this.state.requestComplete && (
                    <Input
                      field={{
                        label: 'Code',
                        type: InputFieldType.TEXT,
                        isRequired: false,
                        value: this.state.twoFACode,
                      }}
                      error={this.state.error.twoFACode}
                      width={12}
                      placeholder="Enter your 7-digit Token"
                      name="twoFACode"
                      onChange={this.handleChangeCode}
                    />
                  )} */}
                <label className="radio-row" htmlFor="type">
                  <input
                    type="radio"
                    name="type"
                    value="SMS"
                    checked={this.state.type === 'SMS'}
                    onChange={this.handleChange}
                  />{' '}
                  Get a code texted to:{phoneNumberInFormat(this.state.countryCode,this.state.phone)}{' '}
                </label>
                {this.state.type !== 'AUTHY_NOTIFY' && (
                    <Input
                      field={{
                        label: 'Code',
                        type: InputFieldType.TEXT,
                        isRequired: false,
                        value: this.state.twoFACode,
                      }}
                      error={this.state.error.twoFACode}
                      width={12}
                      placeholder="Enter your 7-digit Token"
                      name="twoFACode"
                      onChange={this.handleChangeCode}
                    />
                  )}
              </div>
            </div>
          )}
          {isTwoFAEnabled === 'true' && (
            <div className="login-form__actions">
              {this.state.type === 'AUTHY_APP' && (
                <SquareButton
                  bsStyle={ButtonStyle.PRIMARY}
                  content="Verify"
                  onClick={this.validate}
                />
              )}

              {this.state.type === 'AUTHY_NOTIFY' && (
                <SquareButton
                  bsStyle={ButtonStyle.PRIMARY}
                  content="Send"
                  onClick={this.request}
                  disabled={this.state.fetching}
                />
              )}
              {this.state.type === 'SMS' &&(
                  <SquareButton
                    bsStyle={ButtonStyle.PRIMARY}
                    content="Verify"
                    onClick={this.validate}
                  />
                )}
              {this.state.type === 'CALL'  && (
                  <SquareButton
                    bsStyle={ButtonStyle.PRIMARY}
                    content="Verify"
                    onClick={this.validate}
                  />
                )}
              {this.state.type === 'SMS' && (
                <SquareButton
                  bsStyle={
                    this.state.requestComplete
                      ? ButtonStyle.DEFAULT
                      : ButtonStyle.PRIMARY
                  }
                  content={
                    this.state.requestComplete ? 'Resend OTP' : 'Get OTP'
                  }
                  onClick={this.request}
                  disabled={
                    this.state.requestComplete && !this.state.resendButtonEnable
                  }
                />
              )}
              {this.state.type === 'CALL' && (
                <SquareButton
                  bsStyle={
                    this.state.requestComplete
                      ? ButtonStyle.DEFAULT
                      : ButtonStyle.PRIMARY
                  }
                  content={
                    this.state.requestComplete ? 'call retry' : 'Get call'
                  }
                  disabled={
                    this.state.requestComplete && !this.state.resendButtonEnable
                  }
                  onClick={this.request}
                />
              )}
              <Link to="/login">Back to Login Page</Link>
            </div>
          )}
          {isTwoFAEnabled === 'false' && (
            <div className="login-form__fields">
              <h3 className="heading-one">Phone verification</h3>
              <h5 className="heading-two"> Keep your account safe</h5>
              <p className="heading-three">
                Please verify your phone number to start with Two-factor
                authentication.
              </p>
              <div>
                <div className="phone-section">
                  <Input
                    field={{
                      label: 'Country code',
                      isRequired: false,
                      type: InputFieldType.PICKLIST,
                      options: this.getCountryCodes(),
                      value: this.state.countryCode,
                    }}
                    error={this.state.error.countryCode}
                    width={12}
                    placeholder="code"
                    name="countryCode"
                    onChange={this.handleChangeCode}
                    className="country-code"
                    disabled={
                      this.state.requestComplete || !this.state.editable
                    }
                  />
                  <Input
                    field={{
                      label: 'Phone number',
                      type: InputFieldType.TEXT,
                      isRequired: false,
                      value: this.state.phone,
                    }}
                    error={this.state.error.phone}
                    width={12}
                    placeholder="Enter phone number"
                    name="phone"
                    onChange={this.handleChangeCode}
                    disabled={
                      this.state.requestComplete || !this.state.editable
                    }
                    className="phone-number"
                  />
                </div>
                {this.state.requestComplete && (
                  <div>
                    <Input
                      field={{
                        label: 'OTP',
                        type: InputFieldType.TEXT,
                        isRequired: false,
                        value: this.state.otp,
                      }}
                      error={this.state.error.otp}
                      width={6}
                      placeholder="Enter OTP"
                      name="otp"
                      className="otp"
                      onChange={this.handleChangeCode}
                    />
                    <SquareButton
                      bsStyle={
                        this.state.requestComplete
                          ? ButtonStyle.DEFAULT
                          : ButtonStyle.PRIMARY
                      }
                      content={'Resend OTP'}
                      onClick={this.requestPhone}
                      className="resend-otp"
                      disabled={!this.state.resendButtonEnable}
                    />
                  </div>
                )}
              </div>
            </div>
          )}

          {isTwoFAEnabled === 'false' && (
            <div className="two-fa-phone-form__actions ">
              {this.state.requestComplete && (
                <SquareButton
                  bsStyle={ButtonStyle.PRIMARY}
                  content="Verify"
                  onClick={this.validatePhone}
                  disabled={!this.state.otp}
                />
              )}
              {!this.state.requestComplete && (
                <SquareButton
                  bsStyle={
                    this.state.requestComplete
                      ? ButtonStyle.DEFAULT
                      : ButtonStyle.PRIMARY
                  }
                  content={
                    this.state.requestComplete ? 'Resend OTP' : 'Get OTP'
                  }
                  onClick={this.requestPhone}
                />
              )}
              {(this.state.requestComplete || !this.state.editable) && (
                <SquareButton
                  bsStyle={ButtonStyle.DEFAULT}
                  content="Edit"
                  onClick={this.editPhone}
                />
              )}
              <Link to="/login">Back to Login Page</Link>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  logo_url: state.provider.logo_url,
  themeColor: state.provider.themeColor,
  updated_on: state.provider.updated_on,
});

const mapDispatchToProps = (dispatch: any) => ({
  twoFAValidateAuth: (data: any) => dispatch(twoFAValidateAuth(data)),
  twoFARequestAuth: (data: any) => dispatch(twoFARequestAuth(data)),
  requestPhoneVerification: (data: any) =>
    dispatch(requestPhoneVerification(data)),
  validatePhone: (data: any) => dispatch(validatePhone(data)),
  fetchLogoColor: () => dispatch(fetchLogoColor()),
  loginSuccess: (userProfile: IUserProfile) =>
    dispatch(loginSuccess(userProfile)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TWOFA);
