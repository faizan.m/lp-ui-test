import { DebouncedFunc, debounce } from "lodash";
import React from "react";
import { connect } from "react-redux";
import { fetchUserTypes } from "../../actions/userType";
import { getActivityStatusList } from "../../actions/pmo";
import { fetchProviderProfile } from "../../actions/provider";
import { fetchTerritoryMembers } from "../../actions/provider/integration";
import { fetchUserForCustomer, fetchUserProfile } from "../../actions/profile";
import {
  setUserAccess,
  FETCH_USER_ACCESS_SUCCESS,
} from "../../actions/providerAdminUser";
import {
  sendActivationEmail,
  ACTIVATE_USER_EMAIL_FAILURE,
  ACTIVATE_USER_EMAIL_SUCCESS,
} from "../../actions/email";
import {
  editProviderUser,
  fetchProviderUsers,
  createProviderUser,
  deleteProviderUser,
  PROVIDER_USER_CREATE_FAILURE,
  PROVIDER_USER_CREATE_SUCCESS,
  PROVIDER_USER_DELETE_SUCCESS,
  PROVIDER_USER_EDIT_FAILURE,
  PROVIDER_USER_EDIT_SUCCESS,
} from "../../actions/provider/user";
import UserForm from "../User/userForm";
import UserDetails from "../User/userDetails";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import Table from "../../components/Table/table";
import SquareButton from "../../components/Button/button";
import IconButton from "../../components/Button/iconButton";
import EditButton from "../../components/Button/editButton";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";
import DeleteButton from "../../components/Button/deleteButton";
import { allowPermission } from "../../utils/permissions";
import UserAccessForm from "./UserAccessForm";
import "./style.scss";

interface IProviderUserTableRow {
  id: string;
  name: string;
  email: string;
  role: string;
  status: string;
  can_edit: boolean;
  can_delete: boolean;
}

interface IProviderUserProps extends ICommonProps {
  formError: string;
  isAction: boolean;
  terretoryMembers: any;
  isFetchingUsers: boolean;
  isUsersFetching: boolean;
  loggenInUser: ISuperUser;
  userTypes: IUserTypeRole[];
  customers: ICustomerShort[];
  customerUsers: ISuperUser[];
  customer_instance_id: string;
  providerUsers: ISuperUserPaginated;
  fetchUserTypes: TFetchUserTypes;
  editProviderUser: TEditProviderUser;
  fetchUserProfile: () => Promise<any>;
  fetchProviderUsers: TFetchProviderUsers;
  createProviderUser: TCreateProviderUser;
  sendActivationEmail: TPostEmailActivation;
  fetchProviderProfile: () => Promise<any>;
  fetchTerritoryMembers: () => Promise<any>;
  getRoleRateMappingList: () => Promise<any>;
  setUserAccess: (list: any) => Promise<any>;
  deleteProviderUser: (userId: string) => Promise<any>;
  fetchUserForCustomer: (id: string) => Promise<any>;
}

interface IProviderUserState {
  isUserModalOpen: boolean;
  isDetailsModalOpen: boolean;
  rows: IProviderUserTableRow[];
  selectedRows: number[];
  user: ISuperUser;
  errorList?: any;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  providerUsers: ISuperUser[];
  isPostingUser: boolean;
  emailActivatingIndex: number;
  isopenConfirm: boolean;
  id: string;
  reset: boolean;
  statusList?: any[];
  loading?: boolean;
  isAccessModalOpen: boolean;
  updatingAccess: boolean;
  accessList: any[];
}

class ProviderUserListing extends React.Component<
  IProviderUserProps,
  IProviderUserState
> {
  private debouncedFetch: DebouncedFunc<
    (params?: IServerPaginationParams) => void
  >;
  constructor(props: IProviderUserProps) {
    super(props);

    this.state = {
      isUserModalOpen: false,
      isDetailsModalOpen: false,
      isAccessModalOpen: false,
      updatingAccess: false,
      accessList: [],
      rows: [],
      selectedRows: [],
      user: null,
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      providerUsers: [],
      isPostingUser: false,
      emailActivatingIndex: null,
      isopenConfirm: false,
      id: "",
      reset: false,
      statusList: [],
      loading: false,
    };
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  componentDidMount() {
    this.props.fetchProviderUsers();
    this.props.fetchUserTypes();
    this.props.fetchProviderProfile();
    if (this.props.customer_instance_id) {
      this.props.fetchUserForCustomer(this.props.customer_instance_id);
    }
    if (this.props.getRoleRateMappingList) {
      this.getRoleRateMapping();
    }
  }

  componentDidUpdate(prevProps: IProviderUserProps) {
    if (this.props.providerUsers !== prevProps.providerUsers) {
      this.setRows(this.props);
    }

    if (
      this.props.customer_instance_id &&
      this.props.customer_instance_id !== prevProps.customer_instance_id
    ) {
      this.props.fetchUserForCustomer(this.props.customer_instance_id);
    }
  }

  setRows = (nextProps: IProviderUserProps) => {
    const providerUserResponse = nextProps.providerUsers;
    const providerUsers: ISuperUser[] = providerUserResponse.results;
    const rows: IProviderUserTableRow[] = providerUsers.map(
      (providerUser, index) => ({
        ...providerUser,
        id: providerUser.id,
        name: `${providerUser.first_name} ${providerUser.last_name}`,
        email: providerUser.email,
        role: providerUser.role_display_name,
        project_rate_role_name:
          providerUser.project_rate_role &&
          this.geProjectRoleNameById(providerUser.project_rate_role),
        status: providerUser.is_active ? "Enabled" : "Disabled",
        index,
        action: this.getActivationButton(providerUser),
        can_edit: providerUser.can_edit,
        can_delete: providerUser.can_delete,
      })
    );

    this.setState((prevState) => ({
      reset: false,
      rows,
      providerUsers,
      pagination: {
        ...prevState.pagination,
        totalRows: providerUserResponse.count,
        currentPage: providerUserResponse.links.page_number - 1,
        totalPages: Math.ceil(
          providerUserResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  getRoleRateMapping = () => {
    this.setState({ loading: true });
    this.props.getRoleRateMappingList().then((action) => {
      this.setState({ loading: false, statusList: action.response });
    });
  };

  geProjectRoleNameById = (id: number) => {
    let name;
    name =
      (this.state.statusList &&
        this.state.statusList.filter((x) => x.id === id)[0] &&
        this.state.statusList.filter((x) => x.id === id)[0].role_name) ||
      "";
    return name;
  };

  getProviderUserRoles = () => {
    const userTypes = this.props.userTypes;
    if (userTypes) {
      const providerType = userTypes.find(
        (userType) => userType.user_type === "provider"
      );

      return providerType.roles.map((role) => ({
        value: role.id,
        label: role.display_name,
      }));
    }

    return [];
  };

  onEditAccessRowClick = (user: any, event: any) => {
    event.stopPropagation();
    this.setState({
      isAccessModalOpen: true,
      updatingAccess: false,
      accessList:
        user.original.profile.feature.length > 0
          ? user.original.profile.feature
          : [
              {
                feature: "FinanceReport",
                enabled: false,
                user: user.original.id,
              },
            ],
    });
  };
  
  onEditRowClick = (providerUserIndex: number, event: any) => {
    event.stopPropagation();

    this.setState({
      isUserModalOpen: true,
      user: this.state.rows[providerUserIndex] as any,
    });
  };

  onRowClick = (rowInfo) => {
    const providerUserIndex = rowInfo.original.index;

    this.setState({
      isDetailsModalOpen: true,
      user: this.state.rows[providerUserIndex] as any,
    });
  };

  openCreateProviderUserModal = () => {
    this.setState({
      isUserModalOpen: true,
      user: null,
    });
  };

  onCreateProviderUser = (providerUser: ISuperUser) => {
    this.setState({ isPostingUser: true });
    this.props.createProviderUser(providerUser).then((action) => {
      if (action.type === PROVIDER_USER_CREATE_SUCCESS) {
        this.debouncedFetch({ page: 1 });
        this.setState({
          isUserModalOpen: false,
          isPostingUser: false,
          reset: true,
        });
      }
      if (action.type === PROVIDER_USER_CREATE_FAILURE) {
        this.setState({
          isUserModalOpen: true,
          errorList: action.errorList.data,
          isPostingUser: false,
        });
      }
    });
  };

  setUserAccess = (list) => {
    this.setState({ updatingAccess: true });
    this.props
      .setUserAccess(list)
      .then((action) => {
        if (action.type === FETCH_USER_ACCESS_SUCCESS) {
          this.debouncedFetch();
          this.props.fetchUserProfile();
        }
      })
      .finally(() =>
        this.setState({
          isAccessModalOpen: false,
          updatingAccess: false,
        })
      );
  };

  onEditProviderUserSubmit = (providerUser: ISuperUser) => {
    this.setState({ isPostingUser: true });
    this.props
      .editProviderUser(providerUser.id, providerUser)
      .then((action) => {
        if (action.type === PROVIDER_USER_EDIT_SUCCESS) {
          this.debouncedFetch();
          this.setState({
            isUserModalOpen: false,
            isPostingUser: false,
          });
        }
        if (action.type === PROVIDER_USER_EDIT_FAILURE) {
          this.setState({
            isUserModalOpen: true,
            errorList: action.errorList.data,
            isPostingUser: false,
          });
        }
      });
  };

  toggleCreateEditModal = () => {
    this.setState((prevState) => ({
      isUserModalOpen: !prevState.isUserModalOpen,
    }));
  };

  toggleDetailsModal = () => {
    this.setState((prevState) => ({
      isDetailsModalOpen: !prevState.isDetailsModalOpen,
    }));
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchProviderUsers(newParams);
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  fetchUsers = () => {
    const search = this.state.pagination.params.search;
    const page = 0;
    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
            page,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  renderTopBar = () => {
    return (
      <div className="provider-users-listing__actions header-panel">
        {allowPermission("search_provider_user") && (
          <Input
            field={{
              value: this.state.pagination.params.search,
              label: "",
              type: InputFieldType.SEARCH,
            }}
            width={4}
            name="searchString"
            onChange={this.onSearchStringChange}
            placeholder="Search"
            className="search"
          />
        )}
        {allowPermission("add_user") && (
          <SquareButton
            onClick={this.openCreateProviderUserModal}
            content="+ Add user"
            bsStyle={ButtonStyle.PRIMARY}
            className="provider-users-listing__actions-create"
          />
        )}
      </div>
    );
  };

  getActivationButton = (user) => {
    let actionType = "showActive";
    if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 0 &&
      user.activation_mails_sent_count < 6
    ) {
      actionType = "showResend";
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 5
    ) {
      actionType = "showResendStop";
    }

    return actionType;
  };

  onclickSendMail = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState({ emailActivatingIndex: userIndex });
    this.props
      .sendActivationEmail(this.state.providerUsers[userIndex].id)
      .then((action) => {
        if (action.type === ACTIVATE_USER_EMAIL_SUCCESS) {
          this.setState({ emailActivatingIndex: null });
        }
        if (action.type === ACTIVATE_USER_EMAIL_FAILURE) {
          this.setState({ emailActivatingIndex: null });
        }
        this.fetchUsers();
      });
  };
  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteProviderUser(this.state.id).then((action) => {
      if (action.type === PROVIDER_USER_DELETE_SUCCESS) {
        this.debouncedFetch();
        this.toggleConfirmOpen();
      }
    });
  };

  onSubmitAccessConfirm = (list) => {
    this.setUserAccess(list);
  };
  toggleModal = () => {
    this.setState((prevState) => ({
      isAccessModalOpen: !prevState.isAccessModalOpen,
    }));
  };
  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };
  render() {
    const columns: ITableColumn[] = [
      {
        accessor: "name",
        Header: "Name",
        id: "first_name",
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      { accessor: "email", Header: "Email", id: "email" },
      { accessor: "role", Header: "Role", id: "role" },
      {
        accessor: "status",
        Header: "Status",
        id: "is_active",
        Cell: (status) => (
          <div className={`status status--${status.value}`}>{status.value}</div>
        ),
      },
      {
        accessor: "index",
        Header: "Actions",
        sortable: false,
        Cell: (cell) => (
          <div>
            {allowPermission("finance_report") && (
              <IconButton
                className="action-button-fr"
                icon="data-collection.svg"
                onClick={(e) => this.onEditAccessRowClick(cell, e)}
                title={"Access"}
              />
            )}
            {cell.original.can_edit && (
              <EditButton onClick={(e) => this.onEditRowClick(cell.value, e)} />
            )}
            {cell.original.can_delete && (
              <DeleteButton onClick={(e) => this.onDeleteRowClick(cell, e)} />
            )}
          </div>
        ),
      },
      {
        accessor: "action",
        Header: "User activation",
        sortable: false,
        show: allowPermission("activate_user"),
        Cell: (action) => (
          <div>
            {action.value === "showResend" &&
              action.index !== this.state.emailActivatingIndex && (
                <SquareButton
                  content="Resend e-Mail"
                  onClick={(e) => this.onclickSendMail(action.index, e)}
                  bsStyle={ButtonStyle.PRIMARY}
                  className="activate-btn"
                />
              )}
            {action.value === "showResend" &&
              action.index === this.state.emailActivatingIndex && (
                <Spinner show={true} className="email__spinner" />
              )}
            {action.value === "showResendStop" && (
              <div>No more Emails can sent</div>
            )}
            {action.value === "showActive" && "Activated"}
          </div>
        ),
      },
    ];
    const isEdit = this.state.user ? true : false;
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };

    return (
      <div className="provider-users-listing">
        <h3>Users and Access</h3>
        <div className="loader">
          <Spinner show={this.props.isFetchingUsers} />
        </div>
        <Table
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          columns={columns}
          rows={this.state.rows}
          customTopBar={this.renderTopBar()}
          className={`provider-users-listing__table ${
            this.props.isFetchingUsers ? `loading` : ``
          }`}
          loading={this.props.isFetchingUsers}
        />
        {this.state.isUserModalOpen && (
          <UserForm
            show={this.state.isUserModalOpen}
            onClose={this.toggleCreateEditModal}
            onSubmit={
              isEdit ? this.onEditProviderUserSubmit : this.onCreateProviderUser
            }
            user={this.state.user}
            userRoles={this.getProviderUserRoles()}
            userType=""
            errorList={this.state.errorList}
            isLoading={this.state.isPostingUser || this.props.isUsersFetching}
            customers={this.props.customers}
            customerUsers={this.props.customerUsers}
            loggenInUser={this.props.loggenInUser}
            fetchUserForCustomer={this.props.fetchUserForCustomer}
            customer_instance_id={this.props.customer_instance_id}
            fetchTerritoryMembers={this.props.fetchTerritoryMembers}
            terretoryMembers={this.props.terretoryMembers}
            getRoleRateMappingList={this.props.getRoleRateMappingList}
          />
        )}
        <UserAccessForm
          accessList={this.state.accessList}
          onClose={this.toggleModal}
          show={this.state.isAccessModalOpen}
          onSubmit={this.onSubmitAccessConfirm}
          isLoading={this.state.updatingAccess}
        />
        <UserDetails
          show={this.state.isDetailsModalOpen}
          user={this.state.user}
          onClose={this.toggleDetailsModal}
          customerUsers={this.props.customerUsers}
          customers={this.props.customers}
          fetchUserForCustomer={this.props.fetchUserForCustomer}
          fetchTerritoryMembers={this.props.fetchTerritoryMembers}
          terretoryMembers={this.props.terretoryMembers}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isAction}
          message="This process cannot be undone."
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  providerUsers: state.providerUser.providerUsers,
  userTypes: state.userType.userTypes,
  isFetchingUsers: state.providerUser.isFetchingUsers,
  customers: state.customer.customersShort,
  customerUsers: state.profile.customerUsers,
  isUsersFetching: state.profile.isUsersFetching,
  loggenInUser: state.profile.user,
  isAction: state.providerUser.isAction,
  customer_instance_id: state.provider.customer_instance_id,
  terretoryMembers: state.integration.terretoryMembers,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchUserProfile: () => dispatch(fetchUserProfile()),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
  createProviderUser: (providerUser: ISuperUser) =>
    dispatch(createProviderUser(providerUser)),
  editProviderUser: (providerUserId: string, providerUser: ISuperUser) =>
    dispatch(editProviderUser(providerUserId, providerUser)),
  sendActivationEmail: (userId: string) =>
    dispatch(sendActivationEmail(userId)),
  fetchUserForCustomer: (id: string) =>
    dispatch(fetchUserForCustomer(Number(id))),
  setUserAccess: (id: any) => dispatch(setUserAccess(id)),
  fetchTerritoryMembers: () => dispatch(fetchTerritoryMembers()),
  deleteProviderUser: (userId: string) => dispatch(deleteProviderUser(userId)),
  fetchProviderProfile: () => dispatch(fetchProviderProfile()),
  getRoleRateMappingList: () =>
    dispatch(getActivityStatusList("pmo/settings/project-rate-mappings")),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProviderUserListing);
