import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import TagsInput from "react-tagsinput";
import {
  getLineItemsIngramData,
  getAllLineItemsOperations,
  getSinglePurchaseOrder,
  GET_SINGLE_PO_SUCCESS,
  INGRAM_DATA_SUCCESS,
  LINE_ITEMS_SUCCESS,
} from "../../actions/inventory";
import { getSingleQuote } from "../../actions/sow";
import { getDataCommon, GET_DATA_SUCCESS } from "../../actions/orderTracking";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import Checkbox from "../../components/Checkbox/checkbox";
import TooltipCustom from "../../components/Tooltip/tooltip";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import "./style.scss";

interface IOrderTrackingState {
  ticket_last_note: string;
  purchaseOrder: any;
  loading: boolean;
  lineItems: any[];
  loadingLineItems: boolean;
  ingramData: any[];
  loadingIngramData: boolean;
  opportunity: any;
  toExcludeServices: boolean;
  inputValue: string;
  showSearch: boolean;
  showReceivedOnly: boolean;
}

interface IOrderTrackingProps {
  quote: IQuote;
  order: IPurchaseHistory;
  isFetchingShipments: boolean;
  shipments: IPickListOptions[];
  purchaseOrderSetting: IPurchaseOrderSetting;
  getDataCommon: (url: string) => Promise<any>;
  getSingleQuote: (id: number) => Promise<any>;
  getSinglePurchaseOrder: (id: number) => Promise<any>;
  getLineItemsOperations: (purchase_order_ids: number[]) => Promise<any>;
  getLineItemsIngramData: (
    purchase_orders: string[],
    line_item_identifiers: string[]
  ) => Promise<any>;
}

class OrderTracking extends Component<
  IOrderTrackingProps,
  IOrderTrackingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: IOrderTrackingProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    loading: false,
    ticket_last_note: "-",
    purchaseOrder: {},
    lineItems: [],
    loadingLineItems: false,
    ingramData: [],
    loadingIngramData: false,
    showReceivedOnly: false,
    opportunity: {},
    toExcludeServices: false,
    inputValue: "",
    showSearch: false,
    categoryIds: [],
  });

  componentDidMount() {
    this.setState({ loading: true });
    this.props
      .getSinglePurchaseOrder(this.props.order.purchaseOrderId)
      .then((action) => {
        if (action.type === GET_SINGLE_PO_SUCCESS) {
          this.setState(
            {
              purchaseOrder: action.response,
            },
            () => {
              this.getLineItemsOperations(this.props.order.purchaseOrderId);
              this.getLastNote(this.props.order.ticket_crm_id);
            }
          );
        }
      })
      .finally(() => this.setState({ loading: false }));
    this.props.getSingleQuote(this.props.order.opportunity_crm_id);
  }

  handleExcludeServicesChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newstate = this.state;
    newstate[e.target.name] = e.target.checked;
    this.setState(newstate);
  };

  renderLabelWithDetails = (width: number, label: string, detail: string) => {
    return (
      <div className={`field-section col-xs-${width}`}>
        <div className="field__label row">
          <div className="field__label-label">{label}</div>
        </div>
        <div className="detail">{detail}</div>
      </div>
    );
  };

  renderHeader = () => {
    return (
      <div className="po-ot-header">
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            6,
            "Opportunity",
            this.props.order.opportunity_name
          )}
          {this.renderLabelWithDetails(
            6,
            "Won Date",
            this.props.quote && this.props.quote.closed_date
              ? moment(this.props.quote.closed_date).format("MM/DD/YYYY")
              : "-"
          )}
        </div>
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            6,
            "Purchase Order",
            this.props.order.purchaseOrderNumber +
              (this.state.purchaseOrder.customerCompany &&
              this.state.purchaseOrder.customerCompany.name
                ? ` (${this.state.purchaseOrder.customerCompany.name})`
                : "")
          )}
          {this.renderLabelWithDetails(
            3,
            "Customer PO",
            this.props.order.customer_po_number
              ? this.props.order.customer_po_number
              : "-"
          )}
          {this.renderLabelWithDetails(
            3,
            "Order Status",
            this.state.purchaseOrder
              ? this.state.purchaseOrder.closedFlag
                ? "Closed"
                : "Open"
              : "N/A"
          )}
        </div>
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            6,
            "Order Ticket",
            this.props.order.ticket_crm_id +
              "-" +
              this.props.order.ticket_summary
          )}
          {this.renderLabelWithDetails(
            3,
            "LookingPoint PO",
            this.props.order.purchaseOrderNumber
          )}
        </div>
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            12,
            "Last Note",
            this.state.ticket_last_note
          )}
        </div>
      </div>
    );
  };

  getLastNote = (ticket_crm_id: number) => {
    this.props
      .getDataCommon(`providers/purchase-order-tickets/${ticket_crm_id}/notes`)
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          this.setState({ ticket_last_note: action.response.text });
        }
      });
  };

  getLineItemsOperations = (value: number) => {
    this.setState({ loadingLineItems: true });
    this.props.getLineItemsOperations([value]).then((action) => {
      if (action.type === LINE_ITEMS_SUCCESS) {
        const lineItems = action.response;

        lineItems.map((item) => {
          item.shipping_method_id = null;
          item.serial_numbers = [];
          item.ship_date = "";
          item.tracking_number = [];
        });

        this.setState({ lineItems, loadingLineItems: false }, () => {
          if (
            this.state.lineItems.length &&
            !this.state.purchaseOrder.closedFlag
          ) {
            this.getLineItemsIngramData();
          }
        });
      }
      this.setState({ loadingLineItems: false });
    });
  };

  getLineItemsIngramData = () => {
    this.setState({ loadingIngramData: true });
    this.props
      .getLineItemsIngramData(
        [this.state.purchaseOrder.poNumber],
        this.state.lineItems.map((item) => item.product.identifier, [
          this.props.order.purchaseOrderId,
        ])
      )
      .then((action) => {
        if (action.type === INGRAM_DATA_SUCCESS) {
          const ingramData = action.response;
          const newstate = this.state;
          (newstate.ingramData as any) = ingramData;
          this.state.lineItems.map((item, index) => {
            if (
              item.product.identifier &&
              ingramData[item.product.identifier]
            ) {
              newstate.lineItems[index].shipping_method_id =
                this.props.purchaseOrderSetting &&
                this.props.purchaseOrderSetting.extra_config &&
                this.props.purchaseOrderSetting.extra_config
                  .carrier_tracking_url &&
                this.props.purchaseOrderSetting.extra_config
                  .carrier_tracking_url[
                  ingramData[item.product.identifier].carrier
                ] &&
                this.props.purchaseOrderSetting.extra_config
                  .carrier_tracking_url[
                  ingramData[item.product.identifier].carrier
                ].connectwise_shipper;
              newstate.lineItems[index].serial_numbers =
                ingramData[item.product.identifier].serial_numbers || [];
              newstate.lineItems[index].ship_date =
                ingramData[item.product.identifier].ship_date;
              newstate.lineItems[index].estimated_delivery_date =
                ingramData[item.product.identifier].estimated_delivery_date;
              newstate.lineItems[index].tracking_number =
                ingramData[item.product.identifier].tracking_number || [];
              newstate.lineItems[index].shipped_quantity =
                ingramData[item.product.identifier].shipped_quantity;
              newstate.lineItems[index].carrier =
                ingramData[item.product.identifier].carrier;
              newstate.lineItems[index].isIngram = true;
            }
          });
          (newstate.loadingIngramData as boolean) = false;
          this.setState(newstate);
        }

        this.setState({ loadingIngramData: false });
      });
  };

  sortBy = (itemA, itemB) => {
    var valueA = itemA.shipped_quantity;
    var valueB = itemB.shipped_quantity;

    var a = valueA || 0;
    var b = valueB || 0;
    var r = b - a;
    if (r === 0) {
      // Next line makes the magic :)
      r =
        typeof itemA.id !== "undefined" && typeof itemB.id !== "undefined"
          ? itemA.id - itemB.id
          : 0;
    }
    return r;
  };

  renderPurchaseOrders = () => {
    const {
      lineItems,
      toExcludeServices,
      inputValue,
      showReceivedOnly,
    } = this.state;
    let filteredLineItems = lineItems;

    if (toExcludeServices) {
      filteredLineItems = filteredLineItems.filter(
        (item) => !item.product.identifier.startsWith("CON")
      );
    }

    if (inputValue !== "") {
      filteredLineItems = filteredLineItems.filter(
        (item) =>
          item.product.identifier
            .toLowerCase()
            .includes(inputValue.toLowerCase()) ||
          item.description.toLowerCase().includes(inputValue.toLowerCase())
      );
    }

    const IngramTooltip = (
      <TooltipCustom icon_url="i.png" className="ingram-tooltip">
        Ingram Data
      </TooltipCustom>
    );

    if (showReceivedOnly) {
      filteredLineItems = filteredLineItems.filter(
        (a) => a.partially_received_quantity > 0
      );
    }

    // If Order Status is Closed, filter out ingram data
    if (this.state.purchaseOrder.closedFlag) {
      filteredLineItems = filteredLineItems.filter((a) => !a.isIngram);
    }

    filteredLineItems = filteredLineItems.sort((a, b) => this.sortBy(a, b));

    return filteredLineItems.length > 0 ? (
      filteredLineItems.map((data) => (
        <div
          key={data.id}
          className={`product-details-container ${
            data.selected ? "selected" : ""
          } ${data.isIngram ? "shipped-quantity" : ""}`}
        >
          <div className="product-detail top-secton">
            <div className="product-id">
              <div className="detail-title" title={data.id}>
                Product ID
              </div>
              <div className="product-data">{data.product.identifier}</div>
            </div>
            <div>
              <div className="detail-title">Description</div>
              <div className="product-data po-description">
                {data.description}
              </div>
            </div>
            <div>
              <div className="detail-title">
                {data.partially_received_quantity > 0
                  ? "Remaining"
                  : "Purchased Qty"}
              </div>
              <div className="quantity-pending product-data purchased-qty">
                {Math.round(
                  data.total_quantity - data.partially_received_quantity
                )}
              </div>
            </div>
            <div>
              <div className="detail-title">
                Shipped Qty {data.isIngram && IngramTooltip}
              </div>
              <div className="quantity-p quantity-shipped product-data shipped-qty">
                {data.shipped_quantity ? data.shipped_quantity : "-"}
              </div>
            </div>
            <div>
              <div className="detail-title">Received Qty</div>
              <div className="quantity-p quantity-received product-data shipped-qty">
                {data.partially_received_quantity
                  ? data.partially_received_quantity
                  : "-"}
              </div>
            </div>
            <div className="recieved-qty-actions">
              <div className="detail-title">Receiving</div>
              <div className="product-data qty-action-tabs">
                <div
                  className={`or-action-item product-data ${[
                    "none",
                    undefined,
                  ].includes(data.recievedQtyOption) && "active"}`}
                >
                  None
                </div>
                <div
                  className={`or-action-item product-data ${data.recievedQtyOption ===
                    "all" && "active"}`}
                >
                  All
                </div>
                {data.recievedQtyOption !== "custom" ? (
                  <div
                    className={`or-action-item product-data ${data.recievedQtyOption ===
                      "custom" && "active"}`}
                  >
                    Custom
                  </div>
                ) : (
                  <Input
                    field={{
                      label: "",
                      type: InputFieldType.NUMBER,
                      isRequired: false,
                      value: data.received_qty || 0,
                    }}
                    disabled={false}
                    width={12}
                    name="received_qty"
                    onChange={(e) => null}
                    className="custom-recieved-qty"
                  />
                )}
              </div>
            </div>
          </div>
          <div className="product-detail middle-section">
            <div className="tag-input">
              <div className="detail-title">
                Tracking Number {data.isIngram && IngramTooltip}
              </div>
              <TagsInput
                value={
                  (data.tracking_number &&
                    data.tracking_number.filter(
                      (x) =>
                        data.received_tracking_numbers &&
                        !data.received_tracking_numbers.includes(x)
                    )) ||
                  []
                }
                onChange={() => null}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Tracking Numbers",
                }}
                disabled={true}
              />
            </div>
            <div className="tag-input">
              <div className="detail-title">
                Serial Number {data.isIngram && IngramTooltip}
              </div>
              <TagsInput
                value={
                  (data.serial_numbers &&
                    data.serial_numbers.filter(
                      (x) =>
                        data.received_serial_numbers &&
                        !data.received_serial_numbers.includes(x)
                    )) ||
                  []
                }
                onChange={() => null}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Serial Numbers",
                }}
                disabled={true}
              />
            </div>
            <div className="mapped-carrier-container">
              <div className="detail-title">
                Mapped Carrier {data.isIngram && IngramTooltip}
              </div>
              <Input
                field={{
                  label: "",
                  type: InputFieldType.PICKLIST,
                  isRequired: false,
                  value: data.shipping_method_id,
                  options: this.props.shipments,
                }}
                disabled={true}
                width={12}
                name="shipping_method_id"
                onChange={(e) => null}
                loading={this.props.isFetchingShipments}
                className="product-data"
              />
            </div>
            <div>
              <div className="detail-title">
                Ship Date {data.isIngram && IngramTooltip}
              </div>
              <Input
                field={{
                  label: "",
                  type: InputFieldType.DATE,
                  isRequired: false,
                  value: data.ship_date,
                }}
                disabled={true}
                showTime={false}
                width={12}
                name="ship_date"
                onChange={(e) => null}
                className="product-data"
              />
            </div>
            <div>
              <div className="detail-title">
                Estimated Delivery Date {data.isIngram && IngramTooltip}
              </div>
              <Input
                field={{
                  label: "",
                  type: InputFieldType.TEXT,
                  isRequired: false,
                  value:
                    fromISOStringToFormattedDate(
                      data.estimated_delivery_date,
                      "MM-DD-YYYY"
                    ) || "-",
                }}
                disabled={true}
                width={12}
                name="internalNotes"
                onChange={(e) => {}}
                className="product-data"
              />
            </div>
          </div>
          <div className="product-detail bottom-section"></div>
        </div>
      ))
    ) : (
      <div className="no-data" style={{ margin: "0 10px 30px" }}>
        No data found
      </div>
    );
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ inputValue: e.target.value });
  };

  render() {
    return (
      <div className="po-order-tracking">
        <div className="loader">
          <Spinner
            show={
              this.state.loading ||
              this.state.loadingLineItems ||
              this.state.loadingIngramData
            }
          />
        </div>
        {this.renderHeader()}
        <div className="product-data-section">
          {this.state.lineItems.length > 0 ? (
            <>
              <div className="select-all-rows">
                <div className="section-title">PRODUCT DETAILS</div>
                <Checkbox
                  isChecked={this.state.toExcludeServices}
                  name="toExcludeServices"
                  onChange={(e) => this.handleExcludeServicesChange(e)}
                  className="exclude-services-checkbox"
                >
                  Exclude Services
                </Checkbox>
                <Checkbox
                  isChecked={this.state.showReceivedOnly}
                  name="showReceivedOnly"
                  onChange={(e) => this.handleExcludeServicesChange(e)}
                  className="exclude-services-checkbox"
                >
                  Show Received Only
                </Checkbox>
                <Input
                  field={{
                    value: this.state.inputValue,
                    label: "",
                    type: InputFieldType.SEARCH,
                  }}
                  width={10}
                  name="searchString"
                  onChange={this.onSearchStringChange}
                  onBlur={() => {
                    if (this.state.inputValue.trim() === "") {
                      this.setState({ showSearch: false });
                    }
                  }}
                  placeholder="Search"
                  className="search-order-tracking search-order-tracking-outside"
                />
                <div className="legents-box">
                  {!this.state.purchaseOrder.closedFlag && (
                    <div className="legent-data ">
                      <div className="square-border border-yello" />
                      Data from Connectwise and Ingram
                    </div>
                  )}
                  <div className="legent-data ">
                    <div className="square-border border-white" />
                    Data from Connectwise only
                  </div>
                </div>
              </div>
              {this.renderPurchaseOrders()}
            </>
          ) : (
            !(
              this.state.loadingIngramData ||
              this.state.loading ||
              this.state.loadingLineItems
            ) && <div className="no-data">No data available</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  quote: state.sow.quote,
  shipments: state.setting.shipments,
  isFetchingShipments: state.setting.isFetchingShipments,
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getLineItemsOperations: (purchase_order_ids: number[]) =>
    dispatch(getAllLineItemsOperations(purchase_order_ids)),
  getLineItemsIngramData: (
    purchase_orders: string[],
    line_item_identifiers: string[]
  ) => dispatch(getLineItemsIngramData(purchase_orders, line_item_identifiers)),
  getSingleQuote: (id: number) => dispatch(getSingleQuote(id)),
  getSinglePurchaseOrder: (id: number) => dispatch(getSinglePurchaseOrder(id)),
  getDataCommon: (url: string) => dispatch(getDataCommon(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTracking);
