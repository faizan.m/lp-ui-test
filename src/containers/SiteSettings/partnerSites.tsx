import React, { useCallback, useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  fireReportPartnerSitesCRUD,
  FIRE_REPORT_SITES_SUCCESS,
} from "../../actions/setting";
import {
  fetchTaskStatusConfig,
  CONFIG_TASK_STATUS_SUCCESS,
} from "../../actions/configuration";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import { searchInFields } from "../../utils/searchListUtils";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import CreateSite from "../CreateNew/createSite";
import EditButton from "../../components/Button/editButton";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";

interface ConfiguredSitesProps {
  isTaskPolling: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchTaskStatus: (id: number) => Promise<any>;
  partnerSitesCRUD: (method: HTTPMethods, site?: ISite) => Promise<any>;
}

const ConfiguredSites: React.FC<ConfiguredSitesProps> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [orderBy, setOrderBy] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [sites, setSites] = useState<ISite[]>([]);
  const [editSite, setEditSite] = useState<ISite>();
  const [showModal, setShowModal] = useState<boolean>(false);

  const filteredSites: ISite[] = useMemo(() => {
    let newSites: ISite[] = [...sites];
    if (search) {
      newSites = newSites.filter((row) =>
        searchInFields(row, search, ["name", "address_line_1", "city", "state"])
      );
    }
    if (orderBy) {
      let orderField = orderBy;
      if (orderField.charAt(0) !== "-") {
        newSites = newSites.sort((a, b) => {
          let s1: string = a[orderField] ? a[orderField] : "";
          let s2: string = b[orderField] ? b[orderField] : "";
          return s2.localeCompare(s1);
        });
      } else {
        orderField = orderField.substring(1);
        newSites = newSites.sort((a, b) => {
          let s1: string = a[orderField] ? a[orderField] : "";
          let s2: string = b[orderField] ? b[orderField] : "";
          return s1.localeCompare(s2);
        });
      }
    }
    return newSites;
  }, [sites, search, orderBy]);

  useEffect(() => {
    fetchConfiguredSites();
  }, []);

  const fetchConfiguredSites = () => {
    setLoading(true);
    props
      .partnerSitesCRUD(HTTPMethods.GET)
      .then((action) => {
        if (action.type === FIRE_REPORT_SITES_SUCCESS) {
          setSites(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value.toLowerCase());
  };

  const getOrderClass = (field: string) => {
    let currentClassName = "";

    if (orderBy === `-${field}`) currentClassName = "desc-order";
    if (orderBy === field) currentClassName = "asc-order";

    return currentClassName;
  };

  const fetchByOrder = (field: string) => {
    let newOrderBy = "";
    switch (orderBy) {
      case `-${field}`:
        newOrderBy = field;
        break;
      case field:
        newOrderBy = `-${field}`;
        break;
      case "":
        newOrderBy = field;
        break;

      default:
        newOrderBy = field;
        break;
    }
    setOrderBy(newOrderBy);
  };

  const fetchTaskStatus = (taskId: number, type: string) => {
    if (taskId) {
      props.fetchTaskStatus(taskId).then((action) => {
        if (action.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (action.response.status === "SUCCESS") {
            props.addSuccessMessage(`Partner Sites' ${type} task completed!`);
          }
          if (action.response.status === "PENDING") {
            setTimeout(() => fetchTaskStatus(taskId, type), 3000);
          }
          if (action.response.status === "FAILURE") {
            props.addErrorMessage(`Partner Sites' ${type} task failed!`);
          }
        }
      });
    }
  };

  const handleCUDPartnerSite = (site: ISite, deleteMode?: boolean) => {
    const editMode: boolean = Boolean(editSite);
    const CUDType: string = deleteMode
      ? "deletion"
      : editMode
      ? "updation"
      : "creation";
    const method: HTTPMethods = deleteMode
      ? HTTPMethods.DELETE
      : editMode
      ? HTTPMethods.PUT
      : HTTPMethods.POST;

    props.partnerSitesCRUD(method, site).then((action) => {
      if (action.type === FIRE_REPORT_SITES_SUCCESS) {
        const taskId = action.response.task_id;
        fetchConfiguredSites();
        fetchTaskStatus(taskId, CUDType);
        if (!deleteMode) {
          closeModal();
        }
        props.addSuccessMessage(`Partner Sites' ${CUDType} task started!`);
      }
    });
  };

  const closeModal = () => {
    setShowModal(false);
    setEditSite(undefined);
  };

  const onClickEdit = (site: ISite) => {
    const sitePayload: ISite = {
      ...site,
      country_id: site.country_crm_id,
      state_id: site.state_crm_id,
    };
    delete sitePayload.country_crm_id;
    delete sitePayload.state_crm_id;
    setEditSite(sitePayload);
    setShowModal(true);
  };

  const sitesListing = useCallback(() => {
    return filteredSites.map((site, index) => (
      <div
        className={`row-panel column-5 ${index % 2 ? "even" : "odd"}`}
        key={index}
      >
        <div className="hide-overflow-width">
          <div className="type ellipsis-text" title={site.name}>
            {site.name}
          </div>
        </div>
        <div className="hide-overflow-width">
          <div
            className="type ellipsis-text"
            title={site.address_line_1 ? site.address_line_1 : "-"}
          >
            {site.address_line_1 ? site.address_line_1 : "-"}
          </div>
        </div>
        <div className="hide-overflow-width">
          <div
            className="type ellipsis-text"
            title={site.city ? site.city : "-"}
          >
            {site.city ? site.city : "-"}
          </div>
        </div>
        <div className="hide-overflow-width">
          <div
            className="type ellipsis-text"
            title={site.state ? site.state : "-"}
          >
            {site.state ? site.state : "-"}
          </div>
        </div>
        {!props.isTaskPolling && (
          <div className="configured-sites-actions">
            <EditButton
              title="Edit Partner Site"
              onClick={() => onClickEdit(site)}
            />
            <SmallConfirmationBox
              className="remove"
              showButton={true}
              onClickOk={() => handleCUDPartnerSite(site, true)}
              text={"Partner Site"}
              title="Remove Partner Site"
            />
          </div>
        )}
      </div>
    ));
  }, [filteredSites, props.isTaskPolling]);

  return (
    <div className="sites-listing-container">
      {props.isTaskPolling && (
        <div className="task-status">Partner Site task is in progress</div>
      )}
      <div className="action-top-bar">
        <Input
          field={{
            label: "",
            type: InputFieldType.SEARCH,
            value: search,
            isRequired: false,
          }}
          width={3}
          placeholder="Search"
          name="searchString"
          onChange={handleSearchChange}
          className="site-settings__search"
        />
        <div className="button-section">
          <SquareButton
            onClick={() => setShowModal(true)}
            content={"Add New Site"}
            bsStyle={ButtonStyle.PRIMARY}
            className="site-setting-add-site"
            disabled={props.isTaskPolling}
          />
        </div>
      </div>
      <div className="sites-list">
        <div className="header column-5">
          <div
            className={`sites-header-title ${getOrderClass("name")}`}
            onClick={() => fetchByOrder("name")}
          >
            Site
          </div>
          <div
            className={`sites-header-title ${getOrderClass("address_line_1")}`}
            onClick={() => fetchByOrder("address_line_1")}
          >
            Address
          </div>
          <div
            className={`sites-header-title ${getOrderClass("city")}`}
            onClick={() => fetchByOrder("city")}
          >
            City
          </div>
          <div
            className={`sites-header-title ${getOrderClass("state")}`}
            onClick={() => fetchByOrder("state")}
          >
            State
          </div>
          <div />
        </div>
        <div
          id="siteSettingsList"
          style={{ position: "relative", maxHeight: "80vh", overflow: "auto" }}
        >
          {loading ? (
            <div className="no-data">Loading...</div>
          ) : filteredSites.length > 0 ? (
            sitesListing()
          ) : (
            <div className="no-data">No Data</div>
          )}
        </div>
      </div>
      {showModal && (
        <CreateSite
          site={editSite}
          close={closeModal}
          isVisible={showModal}
          handleEditPartnerSite={handleCUDPartnerSite}
        />
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  isTaskPolling: state.configuration.isTaskPolling,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  partnerSitesCRUD: (method: HTTPMethods, site?: ISite) =>
    dispatch(fireReportPartnerSitesCRUD(method, site)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfiguredSites);
