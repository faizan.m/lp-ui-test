import React, { useCallback, useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  addSite,
  editSite,
  deleteCustomerSite,
  fetchSitesProvidersCustomersUser,
  ADD_SITE_SUCCESS,
  FETCH_SITES_SUCCESS,
  DELETE_SITE_SUCCESS,
} from "../../actions/inventory";
import { addSuccessMessage } from "../../actions/appState";
import { searchInFields } from "../../utils/searchListUtils";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import CreateSite from "../CreateNew/createSite";
import EditButton from "../../components/Button/editButton";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import {
  FIRE_REPORT_SITES_SUCCESS,
  fireReportPartnerSitesCRUD,
} from "../../actions/setting";

interface CustomerSitesProps {
  customers: ICustomerShort[];
  isFetchingCustomers: boolean;
  fetchPartnerSites: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  fetchCustomerSites: (customerId: number) => Promise<any>;
  addCustomerSite: (customerId: number, site: ISite) => Promise<any>;
  editCustomerSite: (customerId: number, site: ISite) => Promise<any>;
  deleteCustomerSite: (customerId: number, site: ISite) => Promise<any>;
}

const CustomerSites: React.FC<CustomerSitesProps> = (props) => {
  const [search, setSearch] = useState<string>("");
  const [orderBy, setOrderBy] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [sites, setSites] = useState<ISite[]>([]);
  const [editSite, setEditSite] = useState<ISite>();
  const [customer, setCustomer] = useState<number>(null);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [partnerSites, setPartnerSites] = useState<ISite[]>([]);

  useEffect(() => {
    fetchConfiguredPartnerSites();
  }, []);

  const customerOptions: IPickListOptions[] = useMemo(
    () =>
      props.customers
        ? props.customers.map((customer) => ({
            value: customer.id,
            label: customer.name,
          }))
        : [],
    []
  );

  const filteredSites: ISite[] = useMemo(() => {
    let partnerSiteNames = new Set(partnerSites.map((el) => el.name));
    let newSites: ISite[] = [
      ...sites.filter((el) => partnerSiteNames.has(el.name)),
    ];
    if (search) {
      newSites = newSites.filter((row) =>
        searchInFields(row, search, ["name", "address_line_1", "city", "state"])
      );
    }
    if (orderBy) {
      let orderField = orderBy;
      if (orderField.charAt(0) !== "-") {
        newSites = newSites.sort((a, b) => {
          let s1: string = a[orderField] ? a[orderField] : "";
          let s2: string = b[orderField] ? b[orderField] : "";
          return s2.localeCompare(s1);
        });
      } else {
        orderField = orderField.substring(1);
        newSites = newSites.sort((a, b) => {
          let s1: string = a[orderField] ? a[orderField] : "";
          let s2: string = b[orderField] ? b[orderField] : "";
          return s1.localeCompare(s2);
        });
      }
    }
    return newSites;
  }, [partnerSites, sites, search, orderBy]);

  const fetchConfiguredSites = (customerId?: number) => {
    let customerID = customerId ? customerId : customer;
    setLoading(true);
    if (customerID)
      props
        .fetchCustomerSites(customerID)
        .then((action) => {
          if (action.type === FETCH_SITES_SUCCESS) {
            setSites(action.response);
          }
        })
        .finally(() => setLoading(false));
  };

  const fetchConfiguredPartnerSites = () => {
    props.fetchPartnerSites().then((action) => {
      if (action.type === FIRE_REPORT_SITES_SUCCESS) {
        setPartnerSites(action.response);
      }
    });
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value.toLowerCase());
  };

  const getOrderClass = (field: string) => {
    let currentClassName = "";

    if (orderBy === `-${field}`) currentClassName = "desc-order";
    if (orderBy === field) currentClassName = "asc-order";

    return currentClassName;
  };

  const fetchByOrder = (field: string) => {
    let newOrderBy = "";
    switch (orderBy) {
      case `-${field}`:
        newOrderBy = field;
        break;
      case field:
        newOrderBy = `-${field}`;
        break;
      case "":
        newOrderBy = field;
        break;

      default:
        newOrderBy = field;
        break;
    }
    setOrderBy(newOrderBy);
  };

  const handleAddSite = (saved: boolean = false) => {
    if (saved === true) {
      fetchConfiguredSites();
    }
    setShowModal(false);
    setEditSite(undefined);
  };

  const handleCustomerSiteUpdate = (site: ISite) => {
    props.editCustomerSite(customer, site).then((action) => {
      if (action.type === ADD_SITE_SUCCESS) {
        fetchConfiguredSites();
        setShowModal(false);
        setEditSite(undefined);
        props.addSuccessMessage("Site edited successfully!");
      }
    });
  };

  const onClickEdit = (site: ISite) => {
    setEditSite(site);
    setShowModal(true);
  };

  const onClickDelete = (site: ISite) => {
    props.deleteCustomerSite(customer, site).then((action) => {
      if (action.type === DELETE_SITE_SUCCESS) {
        fetchConfiguredSites();
        props.addSuccessMessage("Site deleted Successfully!");
      }
    });
  };

  const handleChangeCustomer = (event: { target: { value: number } }) => {
    const customerId: number = Number(event.target.value);
    setCustomer(customerId);
    fetchConfiguredSites(customerId);
  };

  const sitesListing = useCallback(() => {
    return filteredSites.map((site, index) => (
      <div
        className={`row-panel column-5 ${index % 2 ? "even" : "odd"}`}
        key={index}
      >
        <div className="hide-overflow-width">
          <div className="type ellipsis-text" title={site.name}>
            {site.name}
          </div>
        </div>
        <div className="hide-overflow-width">
          <div
            className="type ellipsis-text"
            title={site.address_line_1 ? site.address_line_1 : "-"}
          >
            {site.address_line_1 ? site.address_line_1 : "-"}
          </div>
        </div>
        <div className="hide-overflow-width">
          <div
            className="type ellipsis-text"
            title={site.city ? site.city : "-"}
          >
            {site.city ? site.city : "-"}
          </div>
        </div>
        <div className="hide-overflow-width">
          <div
            className="type ellipsis-text"
            title={site.state ? site.state : "-"}
          >
            {site.state ? site.state : "-"}
          </div>
        </div>
        <div className="configured-sites-actions">
          <EditButton
            title="Edit Partner Site"
            onClick={() => onClickEdit(site)}
          />
          <SmallConfirmationBox
            className="remove"
            showButton={true}
            onClickOk={() => onClickDelete(site)}
            text={"Partner Site"}
            title="Remove Partner Site"
          />
        </div>
      </div>
    ));
  }, [filteredSites]);

  const renderSelectCustomer = useCallback(() => {
    return (
      <div className="add-customer-block">
        <Input
          field={{
            label: "Customer",
            type: InputFieldType.PICKLIST,
            value: customer,
            options: customerOptions,
            isRequired: false,
          }}
          width={6}
          multi={false}
          name="customer"
          labelIcon="info"
          labelTitle={"Please select a customer to modify its partner sites"}
          onChange={(e) => handleChangeCustomer(e)}
          placeholder={`Select Customer`}
        />
      </div>
    );
  }, [customer, customerOptions]);

  return (
    <>
      {renderSelectCustomer()}
      <div className="sites-listing-container">
        <div className="action-top-bar">
          <Input
            field={{
              label: "",
              type: InputFieldType.SEARCH,
              value: search,
              isRequired: false,
            }}
            width={3}
            placeholder="Search"
            name="searchString"
            onChange={handleSearchChange}
            className="site-settings__search"
          />
          <div className="button-section">
            <SquareButton
              onClick={() => setShowModal(true)}
              title={
                Boolean(customer)
                  ? "Create new site for the selected customer"
                  : "Please select a customer to create site"
              }
              content={"Add New Site"}
              bsStyle={ButtonStyle.PRIMARY}
              className="site-setting-add-site"
              disabled={!Boolean(customer)}
            />
          </div>
        </div>
        <div className="sites-list">
          <div className="header column-5">
            <div
              className={`sites-header-title ${getOrderClass("name")}`}
              onClick={() => fetchByOrder("name")}
            >
              Site
            </div>
            <div
              className={`sites-header-title ${getOrderClass(
                "address_line_1"
              )}`}
              onClick={() => fetchByOrder("address_line_1")}
            >
              Address
            </div>
            <div
              className={`sites-header-title ${getOrderClass("city")}`}
              onClick={() => fetchByOrder("city")}
            >
              City
            </div>
            <div
              className={`sites-header-title ${getOrderClass("state")}`}
              onClick={() => fetchByOrder("state")}
            >
              State
            </div>
            <div />
          </div>
          <div
            id="siteSettingsList"
            style={{
              position: "relative",
              maxHeight: "80vh",
              overflow: "auto",
            }}
          >
            {!customer ? (
              <div className="no-data">Please select a customer</div>
            ) : loading ? (
              <div className="no-data">Loading...</div>
            ) : filteredSites.length > 0 ? (
              sitesListing()
            ) : (
              <div className="no-data">No Data</div>
            )}
          </div>
        </div>
      </div>
      {showModal && (
        <CreateSite
          site={editSite}
          customerId={customer}
          isVisible={showModal}
          close={handleAddSite}
          onUpdate={handleCustomerSiteUpdate}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
  isFetchingCustomers: state.customer.isFetchingCustomers,
});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  fetchPartnerSites: () =>
    dispatch(fireReportPartnerSitesCRUD(HTTPMethods.GET)),
  fetchCustomerSites: (customerId: number) =>
    dispatch(fetchSitesProvidersCustomersUser(customerId)),
  addCustomerSite: (customerId: number, site: ISite) =>
    dispatch(addSite(customerId, site)),
  editCustomerSite: (customerId: number, site: ISite) =>
    dispatch(editSite(customerId, site)),
  deleteCustomerSite: (customerId: number, site: ISite) =>
    dispatch(deleteCustomerSite(customerId, site)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerSites);
