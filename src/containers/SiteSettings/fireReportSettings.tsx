import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep, isNil } from "lodash";
import { connect } from "react-redux";
import {
  fireReportSettingsCRU,
  FIRE_REPORT_SETTING_SUCCESS,
  FIRE_REPORT_SETTING_FAILURE,
} from "../../actions/setting";
import { fetchAllProviderUsers } from "../../actions/provider/user";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";

interface FireSettingsProps {
  isFetchingUsers: boolean;
  providerUsers: ISuperUser[];
  fetchAllProviderUsers: () => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fireReportSettingCRU: (
    method: HTTPMethods,
    data?: IFIREReportSettings
  ) => Promise<any>;
}

interface ErrorInterface {
  subject: IFieldValidation;
  frequency: IFieldValidation;
  past_days: IFieldValidation;
  receiver_email_ids: IFieldValidation;
}

const SuccessMessage: IFieldValidation = {
  errorState: IValidationState.SUCCESS,
  errorMessage: "",
};

const EmptyErrorState = {
  subject: { ...SuccessMessage },
  frequency: { ...SuccessMessage },
  past_days: { ...SuccessMessage },
  receiver_email_ids: { ...SuccessMessage },
};

const FrequencyOptions: string[] = [
  "DAILY",
  "WEEKLY",
  "MONTHLY",
  "YEARLY",
  "NEVER",
];

const FireSettings: React.FC<FireSettingsProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [settings, setSettings] = useState<IFIREReportSettings>({
    subject: "",
    frequency: "",
    past_days: 0,
    receiver_email_ids: [],
  });
  const [error, setError] = useState<ErrorInterface>({ ...EmptyErrorState });
  const providerUsersEmails: IPickListOptions[] = useMemo(
    () =>
      props.providerUsers
        ? props.providerUsers.map((author: ISuperUser) => ({
            value: author.email,
            label: `${author.first_name} ${author.last_name}`,
          }))
        : [],
    [props.providerUsers]
  );

  useEffect(() => {
    getFireReportSettings();
    props.fetchAllProviderUsers();
  }, []);

  const getFireReportSettings = () => {
    setLoading(true);
    props
      .fireReportSettingCRU(HTTPMethods.GET)
      .then((action) => {
        if (action.type === FIRE_REPORT_SETTING_SUCCESS) {
          setSettings(action.response);
        } else if (action.type === FIRE_REPORT_SETTING_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const validateForm = (): boolean => {
    let isValid = true;
    const err = cloneDeep(EmptyErrorState);
    if (settings.receiver_email_ids.length === 0) {
      err.receiver_email_ids = {
        errorState: IValidationState.ERROR,
        errorMessage: "Please select at least one email address",
      };
      isValid = false;
    }
    if (!settings.subject.trim()) {
      err.subject = {
        errorState: IValidationState.ERROR,
        errorMessage: "Please enter a subject",
      };
      isValid = false;
    }
    if (!settings.frequency) {
      err.frequency = {
        errorState: IValidationState.ERROR,
        errorMessage: "Please enter a frequency",
      };
      isValid = false;
    }
    if (isNil(settings.past_days)) {
      err.past_days = {
        errorState: IValidationState.ERROR,
        errorMessage: "Please enter number of days",
      };
      isValid = false;
    }
    if (!isValid) setError(err);
    return isValid;
  };

  const saveSettings = () => {
    if (validateForm()) {
      props
        .fireReportSettingCRU(
          firstSave ? HTTPMethods.POST : HTTPMethods.PUT,
          settings
        )
        .then((action) => {
          if (action.type === FIRE_REPORT_SETTING_SUCCESS) {
            props.addSuccessMessage(
              `Settings ${firstSave ? "saved" : "updated"} successfully!`
            );
            setFirstSave(false);
          } else if (action.type === FIRE_REPORT_SETTING_FAILURE) {
            props.addErrorMessage("Error saving settings!");
          }
        });
    }
  };

  const handleChange = (event: {
    target: { name: string; value: any; type: string };
  }) => {
    setSettings((prevState) => ({
      ...prevState,
      [event.target.name]:
        event.target.type === "number"
          ? Number(event.target.value)
          : event.target.value,
    }));
  };

  return (
    <div className="fire-report-settings">
      <Spinner className="fire-settings-loader" show={loading} />
      <div className="fire-setting-row">
        <Input
          field={{
            label: "Send to Users",
            type: InputFieldType.PICKLIST,
            value: settings.receiver_email_ids,
            options: providerUsersEmails,
            isRequired: true,
          }}
          width={6}
          multi={true}
          name="receiver_email_ids"
          onChange={handleChange}
          loading={props.isFetchingUsers}
          error={
            settings.receiver_email_ids.length === 0
              ? error.receiver_email_ids
              : { ...SuccessMessage }
          }
          placeholder={`Select Email Addresses`}
        />
        <Input
          field={{
            label: "Subject",
            type: InputFieldType.TEXT,
            value: settings.subject,
            isRequired: true,
          }}
          width={6}
          name="subject"
          onChange={handleChange}
          labelIcon="info"
          labelTitle={
            "Subject line that will appear in the email sent to users"
          }
          error={
            !settings.subject.trim() ? error.subject : { ...SuccessMessage }
          }
          placeholder={`Enter subject for email`}
        />
      </div>
      <div className="fire-setting-row">
        <Input
          field={{
            label: "Frequency",
            type: InputFieldType.PICKLIST,
            value: settings.frequency,
            options: FrequencyOptions,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="frequency"
          labelIcon="info"
          labelTitle={
            "FIRE Report will be generated as per this frequency and send to users"
          }
          onChange={handleChange}
          error={!settings.frequency ? error.frequency : { ...SuccessMessage }}
          placeholder={`Select Frequency`}
        />
        <Input
          field={{
            label: "Duration",
            type: InputFieldType.NUMBER,
            value: settings.past_days,
            isRequired: true,
          }}
          width={6}
          multi={false}
          minimumValue="0"
          name="past_days"
          onChange={handleChange}
          labelIcon="info"
          labelTitle={
            "FIRE Report will be generated for the Sales Orders that were updated within these days in the past"
          }
          error={!settings.past_days ? error.past_days : { ...SuccessMessage }}
          placeholder={`Select Duration`}
        />
      </div>
      <SquareButton
        onClick={saveSettings}
        content={firstSave ? "Save" : "Update"}
        bsStyle={ButtonStyle.PRIMARY}
        className="fire-setting-save"
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  isFetchingUsers: state.providerUser.isFetchingUsers,
  providerUsers: state.providerUser.providerUsersAll,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  fetchAllProviderUsers: () => dispatch(fetchAllProviderUsers()),
  fireReportSettingCRU: (method: HTTPMethods, data?: IFIREReportSettings) =>
    dispatch(fireReportSettingsCRU(method, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FireSettings);
