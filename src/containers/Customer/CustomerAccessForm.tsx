import React from 'react';
import { cloneDeep } from 'lodash';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';

import './style.scss';

interface ICustomerUserFormState {
  user: Icustomer;
}

interface ICustomerUserFormProps {
  onClose: (e: any) => void;
  user?: Icustomer;
  onSubmit: (user: Icustomer) => void;
  isLoading: boolean;
}

export default class CustomerAccessForm extends React.Component<
  ICustomerUserFormProps,
  ICustomerUserFormState
  > {
  static emptyState: ICustomerUserFormState = {
    user: null,
  };

  constructor(props: ICustomerUserFormProps) {
    super(props);

    this.state = {
      ...CustomerAccessForm.emptyState,
    };
  }

  componentDidMount() {
    if(this.props.user) {
      this.setState({ user: this.props.user })
    }
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    const targetValue = event.target.value === 'true' ? true : false;

    newState.user[event.target.name] = targetValue;

    this.setState(newState);
  };

  handleIsActiveChange = (event: any) => {
    const targetValue = event.target.value === 'Enable' ? true : false;
    const newState = cloneDeep(this.state);
    newState.user[event.target.name] = targetValue;

    this.setState(newState);
  };

  handleComplianceChange = (event: any) => {
    const targetValue = event.target.value;
    const newState = cloneDeep(this.state);
    newState.user.config_compliance_service_allowed = targetValue;
    newState.user.config_compliance_service_write_allowed = targetValue;

    if (targetValue === 'Enable') {
      newState.user.config_compliance_service_allowed = true;
      newState.user.config_compliance_service_write_allowed = true;
    }

    if (targetValue === 'Disable') {
      newState.user.config_compliance_service_allowed = false;
      newState.user.config_compliance_service_write_allowed = false;
    }

    if (targetValue === 'Read') {
      newState.user.config_compliance_service_allowed = true;
      newState.user.config_compliance_service_write_allowed = false;
    }

    this.setState(newState);
  };

  getComplianceValue = (obj: Icustomer = this.state.user) => {
    let enable = 'Disable';
    if (
      obj.config_compliance_service_write_allowed === true &&
      obj.config_compliance_service_allowed === true) {
      enable = 'Enable';
    }
    if (
      obj.config_compliance_service_write_allowed === false &&
      obj.config_compliance_service_allowed === true
    ) {
      enable = 'Read';
    }
    if (obj.config_compliance_service_write_allowed === false &&
      obj.config_compliance_service_allowed === false
    ) {
      enable = 'Disable';
    }
    return enable
  };


  onClose = e => {
    this.setState({
      ...CustomerAccessForm.emptyState,
    });

    this.props.onClose(e);
  };

  onSubmit = () => {
    this.props.onSubmit(this.state.user);
  };

  getBody = () => {
    return this.state.user ? (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`user-modal__body ${
            this.props.isLoading ? `loading` : ''
            }`}
        >
          <Input
            field={{
              value: this.state.user.is_active ? 'Enable' : 'Disable',
              label: 'Status',
              type: InputFieldType.RADIO,
              isRequired: false,
              options: [
                { value: 'Enable', label: 'Enable' },
                { value: 'Disable', label: 'Disable' },
              ],
            }}
            width={6}
            name="is_active"
            onChange={this.handleIsActiveChange}
          />
          <Input
            field={{
              value: this.state.user.is_ms_customer,
              label: 'Is Managed Service Customer',
              type: InputFieldType.RADIO,
              isRequired: false,
              options: [
                { value: true, label: 'Yes' },
                { value: false, label: 'No' },
              ],
            }}
            width={6}
            name="is_ms_customer"
            onChange={this.handleChange}
          />
          <Input
                field={{
                  value: this.state.user.renewal_service_allowed ? 'Enable' : 'Disable',
                  label: 'Renewal',
                  type: InputFieldType.RADIO,
                  isRequired: false,
                  options: [
                    { value: 'Enable', label: 'Enable' },
                    { value: 'Disable', label: 'Disable' },
                  ],
                }}
                width={6}
                name="renewal_service_allowed"
                onChange={this.handleIsActiveChange}
              />
              <Input
                field={{
                  value: this.state.user.collector_service_allowed ? 'Enable' : 'Disable',
                  label: 'Collector',
                  type: InputFieldType.RADIO,
                  isRequired: false,
                  options: [
                    { value: 'Enable', label: 'Enable' },
                    { value: 'Disable', label: 'Disable' },
                  ],
                }}
                width={6}
                name="collector_service_allowed"
                onChange={this.handleIsActiveChange}
              />
              <Input
                field={{
                  value: this.getComplianceValue(),
                  label: 'Compliance',
                  type: InputFieldType.RADIO,
                  isRequired: false,
                  options: [
                    { value: 'Read', label: 'Read only' },
                    { value: 'Enable', label: 'Read/write' },
                    { value: 'Disable', label: 'Disable' },
                  ],
                }}
                width={12}
                name="config_compliance_service_allowed"
                onChange={this.handleComplianceChange}
              />
           <Input
            field={{
              value: this.state.user.customer_order_visibility_allowed,
              label: 'Customer Order Tracking',
              type: InputFieldType.RADIO,
              isRequired: false,
              options: [
                { value: true, label: 'Enable' },
                { value: false, label: 'Disable' },
              ],
            }}
            width={6}
            name="customer_order_visibility_allowed"
            onChange={this.handleChange}
          />
        </div>
      </div>
    ) : null;
  };

  getFooter = () => {
    return (
      <div
        className={`user-modal__footer
      ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Save Changes"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    const title = 'Edit customer access';

    return (
      <ModalBase
        show={true}
        onClose={this.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="user-modal__customer-access-form"
      />
    );
  }
}
