import React from 'react';
import { cloneDeep } from 'lodash';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import './style.scss';

interface IActivationUserFormState {
  user: ISuperUser;
  error: {
    role: IFieldValidation;
  };
  isFormValid: boolean;
}

interface IActivationUserFormProps {
  show: boolean;
  onClose: (e: any) => void;
  user?: ISuperUser;
  onSubmit: (user: ISuperUser) => void;
  customerUserRoles: Array<{ value: number; label: string }>;
  isLoading?: boolean;
}

export default class UserActivationForm extends React.Component<
  IActivationUserFormProps,
  IActivationUserFormState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  static emptyState: IActivationUserFormState = {
    user: null,
    error: {
      role: {
        ...UserActivationForm.emptyErrorState,
      },
    },
    isFormValid: false,
  };

  constructor(props: IActivationUserFormProps) {
    super(props);

    this.state = {
      ...UserActivationForm.emptyState,
    };
  }

  componentDidUpdate(prevProps: IActivationUserFormProps) {
    if (prevProps.user !== this.props.user) {
      this.setState({
        user: this.props.user,
      });
    }
  }

  handleChange = (event: any) => {
    const newState = this.state;
    newState.user[event.target.name] = event.target.value;

    this.setState(newState);
  };

  onClose = e => {
    this.setState({
      ...UserActivationForm.emptyState,
    });

    this.props.onClose(e);
  };

  onSubmit = () => {
    if (this.validateForm()) {
      this.props.onSubmit(this.state.user);
      this.setState({
        ...UserActivationForm.emptyState,
      });
    }
  };

  validateForm() {
    const newState: IActivationUserFormState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.user.user_role) {
      newState.error.role.errorState = IValidationState.ERROR;
      newState.error.role.errorMessage = 'Please select user role';
      isValid = false;
    }

    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  }

  getBody = () => {
    return this.state.user ? (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`user-modal__body ${
            this.props.isLoading ? `loading` : ''
            }`}
        >
          <Input
            field={{
              value: this.state.user.user_role,
              label: 'Role',
              type: InputFieldType.PICKLIST,
              isRequired: true,
              options: this.props.customerUserRoles,
            }}
            width={6}
            name="user_role"
            onChange={this.handleChange}
            placeholder="Select Role"
            error={this.state.error.role}
          />
        </div>
      </div>
    ) : null;
  };

  getFooter = () => {
    return (
      <div
        className={`user-modal__footer
      ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Save Changes"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    const title = 'Set User Role';

    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="user-modal__customer-access-form"
      />
    );
  }
}
