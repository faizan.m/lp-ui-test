import { cloneDeep } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addSuccessMessage } from '../../actions/appState';
import {
  logOutRequest, resetPasswordRequest, RESET_PASSWORD_FAILURE,
  RESET_PASSWORD_SUCCESS
} from '../../actions/auth';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';

import AppValidators from '../../utils/validator';
import './style.scss';

interface IResetPassState {
  currentPass: string;
  newPass: string;
  confirmPass: string;
  isFormValid: boolean;
  error: {
    currentPass: IFieldValidation;
    newPass: IFieldValidation;
    confirmPass: IFieldValidation;
  };
  errorList?: any;
}

interface IResetPassProps extends ICommonProps {
  logOutRequest: TLogout;
  resetPasswordRequest: TResetPasswordRequest;
  addSuccessMessage: TShowSuccessMessage;
}

class ResetPassword extends Component<IResetPassProps, IResetPassState> {
  constructor(props: any) {
    super(props);
    this.state = {
      currentPass: '',
      newPass: '',
      confirmPass: '',
      isFormValid: false,
      error: {
        currentPass: {
          errorState: IValidationState.SUCCESS,
          errorMessage: '',
        },
        newPass: {
          errorState: IValidationState.SUCCESS,
          errorMessage: '',
        },
        confirmPass: {
          errorState: IValidationState.SUCCESS,
          errorMessage: '',
        },
      },
    };
  }

  handleChange = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  validateForm = () => {
    this.clearValidationStatus();
    const newPassState: IResetPassState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.newPass) {
      newPassState.error.newPass.errorState = IValidationState.ERROR;
      newPassState.error.newPass.errorMessage = 'Password cannot be empty';

      isValid = false;
    }
    if (
      this.state.newPass &&
      !AppValidators.isValidPassword(this.state.newPass)
    ) {
      newPassState.error.newPass.errorState = IValidationState.ERROR;
      // tslint:disable-next-line:max-line-length
      newPassState.error.newPass.errorMessage = `Password should contain one or more lowercase, uppercase,
      numeric and special character. Length should be minimum 8 character.`;
      newPassState.error.currentPass.errorState = IValidationState.SUCCESS;
      newPassState.error.confirmPass.errorMessage = '';

      isValid = false;
    }
    if (
      AppValidators.isValidPassword(this.state.newPass) &&
      this.state.newPass !== this.state.confirmPass
    ) {
      newPassState.error.confirmPass.errorState = IValidationState.ERROR;
      newPassState.error.confirmPass.errorMessage = 'Passwords do not match';
      newPassState.error.newPass.errorState = IValidationState.SUCCESS;
      newPassState.error.newPass.errorMessage = '';
      isValid = false;
    }
    if (!this.state.currentPass) {
      newPassState.error.currentPass.errorState = IValidationState.ERROR;
      newPassState.error.currentPass.errorMessage =
        'Current Password cannot be empty';

      isValid = false;
    }

    if (this.state.newPass === this.state.currentPass) {
      newPassState.error.newPass.errorState = IValidationState.ERROR;
      newPassState.error.newPass.errorMessage =
        'New Password and Current Password cannot be same.';

      isValid = false;
    }

    newPassState.isFormValid = isValid;
    this.setState(newPassState);

    return isValid;
  };

  submit = () => {
    if (this.validateForm()) {
      const request = {
        current_password: this.state.currentPass,
        new_password: this.state.newPass,
      };
      this.props.resetPasswordRequest(request).then(action => {
        if (action.type === RESET_PASSWORD_SUCCESS) {
          this.clearValidationStatus();
          this.props.addSuccessMessage('Password Reset Successfully !!');
          setTimeout(() => {
            this.props.logOutRequest();
            this.props.history.push('/login');
            // tslint:disable-next-line:align
          }, 2000);
        }
        if (action.type === RESET_PASSWORD_FAILURE) {
          this.clearValidationStatus();
          const newState = cloneDeep(this.state);
          newState.error.currentPass.errorMessage =
            action.errorList.data.current_password[0];
          newState.error.currentPass.errorState = IValidationState.ERROR;
          this.setState(newState);
        }
      });
    }
  };

  clear = () => {
    this.clearValidationStatus();
    this.setState({ currentPass: '', newPass: '', confirmPass: '' });
  };

  clearValidationStatus = () => {
    const error = {
      currentPass: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      newPass: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      confirmPass: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
    };

    const newPassState: IResetPassState = cloneDeep(this.state);
    newPassState.error = error;
    this.setState(newPassState);
  };

  render() {
    return (
      <div>
        <h3>Reset Password</h3>
        <div className="reset-container">
          <Input
            field={{
              label: 'Current Password',
              type: InputFieldType.PASSWORD,
              isRequired: true,
              value: this.state.currentPass,
            }}
            error={this.state.error.currentPass}
            width={6}
            placeholder="Enter Current Password"
            name="currentPass"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: 'New Password',
              type: InputFieldType.PASSWORD,
              isRequired: true,
              value: this.state.newPass,
            }}
            error={this.state.error.newPass}
            width={6}
            placeholder="Enter New Password"
            name="newPass"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: 'Confirm Password',
              type: InputFieldType.PASSWORD,
              isRequired: true,
              value: this.state.confirmPass,
            }}
            error={this.state.error.confirmPass}
            width={6}
            placeholder="Re-enter Password"
            name="confirmPass"
            onChange={this.handleChange}
          />
          <div className="reset_actions">
            <SquareButton
              bsStyle={ButtonStyle.PRIMARY}
              content="Reset Password"
              onClick={this.submit}
            />
            <SquareButton
              bsStyle={ButtonStyle.DEFAULT}
              content="Reset"
              onClick={this.clear}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  resetPasswordRequest: (req: IResetPasswordRequest) =>
    dispatch(resetPasswordRequest(req)),
  logOutRequest: () => dispatch(logOutRequest()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(
  null,
  mapDispatchToProps
)(ResetPassword);
