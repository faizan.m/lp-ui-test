import React, { Component } from "react";

import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";

interface INewUploadProps {
  close?: (e: any) => void;
  isVisible: boolean;
  onSubmit: (data: any) => void;
  loading?: boolean;
  titleField?: string;
  fileField?: string;
}

interface INewUploadState {
  dataFile: any;
  error: string;
}

class NewUpload extends Component<INewUploadProps, INewUploadState> {
  constructor(props: INewUploadProps) {
    super(props);
    this.state = {
      dataFile: "",
      error: "",
    };
  }

  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile: any = files[0];
    var FileSize = dataFile.size / 1024 / 1024; // in MB
    if (FileSize > 15) {
      this.setState({ error: " 15 MB File size limit exceeded" });
    } else {
      this.setState({ dataFile, error: "" });
    }
  };

  onSubmit = (e) => {
    if (this.state.dataFile) {
      const data = new FormData();

      const name = this.props.titleField ? this.props.titleField : "title";
      const fileField = this.props.fileField ? this.props.fileField : "file";
      data.append(name, this.state.dataFile.name);
      data.append(fileField, this.state.dataFile);
      data.append("is_active", "true");
      this.props.onSubmit(data);
      this.setState({ dataFile: null });
    }
  };

  onClose = (e) => {
    this.setState({ dataFile: null, error: "" });
    this.props.close(e);
  };

  renderBody = () => {
    return (
      <div>
        {this.props.loading && (
          <div className="loader modal-loader">
            <Spinner show={true} />
          </div>
        )}
        <label
          className="btn btn-primary square-btn file-button"
          style={{
            display: "flex",
            alignItems: "center",
            gap: "8px",
            width: "135px",
            marginBottom: "10px",
          }}
        >
          <img
            className="attachments__icon__upload"
            src="/assets/icons/cloud.png"
          />
          Upload File
          <input type="file" onChange={this.handleFile} />
        </label>
        {this.state.dataFile && <p>{this.state.dataFile.name}</p>}
        {this.state.error && <p style={{ color: "red" }}>{this.state.error}</p>}
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div>
        <SquareButton
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
          onClick={this.onClose}
          disabled={this.props.loading}
        />
        <SquareButton
          content="Submit"
          bsStyle={ButtonStyle.PRIMARY}
          onClick={this.onSubmit}
          disabled={this.props.loading}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Upload Attachment"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="service-upload-container"
      />
    );
  }
}

export default NewUpload;
