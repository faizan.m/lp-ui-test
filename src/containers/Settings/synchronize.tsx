import React from "react";
import SquareButton from "../../components/Button/button";

interface ISynchronizeProps extends ICommonProps {
  syncNowConnectwise: () => void;
  syncNowExternal: () => void;
  periodicSync: boolean;
  configStatus: IConfigStatus;
}

interface ISynchronizeState {
  periodicSync: boolean;
}

export default class Synchronize extends React.Component<
  ISynchronizeProps,
  ISynchronizeState
> {
  constructor(props: ISynchronizeProps) {
    super(props);
    this.state = {
      periodicSync: this.props.periodicSync,
    };
  }

  clickManufacturer = () => {
    this.props.history.push(`/setting/api-credentials`);
  };

  render() {
    return (
      <div>
        <div className="synchronize">
          <div onClick={this.props.syncNowConnectwise} className="sync-now">
            <img
              className="sync-img"
              src="/assets/new-icons/sync.svg"
              alt="sync data"
            />
            <div className="sync-text">Sync ConnectWise Data</div>
          </div>
          <div onClick={this.props.syncNowExternal} className="sync-now">
            <img
              className="sync-img"
              src="/assets/new-icons/sync.svg"
              alt="sync data"
            />
            <div className="sync-text">
              {"Sync External Services (Cisco, Logicmonitor) Data"}
            </div>
          </div>
        </div>

        {!this.props.configStatus.device_manufacturer_api_configured && (
          <div className="go-to-manufacturer">
            <h4>
              Device Manufacturer Account Settings has not been configured.
              Please contact the site administrator.
            </h4>
            <SquareButton
              content="Go to Manufacturer Setting >> Click here"
              onClick={this.clickManufacturer}
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        )}
      </div>
    );
  }
}
