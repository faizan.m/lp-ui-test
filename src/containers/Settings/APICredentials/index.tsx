import React from "react";
import SettingDeviceManufacturer from "./SettingDeviceManufacturer";
import SettingIngramAPI from "./SettingIngramAPI";
import SettingPAX8 from "./SettingPAX8";

const APICredentials: React.FC = () => {
  return (
    <>
      <SettingDeviceManufacturer />
      <SettingIngramAPI />
      <SettingPAX8 />
    </>
  );
};

export default APICredentials;
