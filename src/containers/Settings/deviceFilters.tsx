import React from "react";

import SquareButton from "../../components/Button/button";
import Spinner from "../../components/Spinner";
import TagsInput from "react-tagsinput";
import { cloneDeep } from "lodash";

interface IDeviceFilters {
  name_filters: any[];
  serial_filters: any[];
}
interface IDeviceFiltersProps {
  deviceFilters: IDeviceFilters;
  onSubmit: (data: any) => void;
}

interface IDeviceFiltersState {
  deviceFilters: IDeviceFilters;
  error: {
    name_filters: IFieldValidation;
    serial_filters: IFieldValidation;
  };
}

export default class DeviceFilters extends React.Component<
  IDeviceFiltersProps,
  IDeviceFiltersState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  constructor(props: IDeviceFiltersProps) {
    super(props);

    let deviceFilters = {
      name_filters: [],
      serial_filters: [],
    };
    if (this.props.deviceFilters) {
      deviceFilters = this.props.deviceFilters;
    }
    this.state = {
      deviceFilters,
      error: {
        name_filters: { ...DeviceFilters.emptyErrorState },
        serial_filters: { ...DeviceFilters.emptyErrorState },
      },
    };
  }

  componentDidUpdate(prevProps: IDeviceFiltersProps) {
    if (
      this.props.deviceFilters &&
      prevProps.deviceFilters !== this.props.deviceFilters
    ) {
      this.setState({
        deviceFilters: this.props.deviceFilters,
      });
    }
  }

  onSubmit = () => {
    this.props.onSubmit(this.state.deviceFilters);
  };

  handleChangeValueNameFilter = (e) => {
    const newState = cloneDeep(this.state);
    (newState.deviceFilters.name_filters as any) = e;
    this.setState(newState);
  };
  handleChangeValueSerialFilter = (e) => {
    const newState = cloneDeep(this.state);
    (newState.deviceFilters.serial_filters as any) = e;
    this.setState(newState);
  };

  renderDeviceFilters = () => {
    return (
      <div className="device-mapping__options">
        <div className="col-md-12 mapping-row">
          <div
            className="email-tag  field-section
                field-section--error field-section--required
                 col-md-10 col-xs-10"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Name Filters
              </label>
              <span className="field__label-required" />
            </div>
            <div>
              <TagsInput
                value={
                  this.state.deviceFilters.name_filters
                    ? this.state.deviceFilters.name_filters
                    : []
                }
                onChange={(e) => this.handleChangeValueNameFilter(e)}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Value",
                }}
                addOnBlur={true}
              />
            </div>
            {this.state.error.name_filters.errorMessage && (
              <div className="field__error">
                {this.state.error.name_filters.errorMessage}
              </div>
            )}
          </div>
          <div
            className="email-tag  field-section
                field-section--error field-section--required
                 col-md-10 col-xs-10"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Serial Filters
              </label>
              <span className="field__label-required" />
            </div>
            <div>
              <TagsInput
                value={
                  this.state.deviceFilters.serial_filters
                    ? this.state.deviceFilters.serial_filters
                    : []
                }
                onChange={(e) => this.handleChangeValueSerialFilter(e)}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Value",
                }}
                addOnBlur={true}
              />
            </div>
            {this.state.error.serial_filters.errorMessage && (
              <div className="field__error">
                {this.state.error.serial_filters.errorMessage}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="category-mapping-container">
        <div className="loader">
          <Spinner show={false} />
        </div>
        {this.renderDeviceFilters()}
        <div className="action-btns">
          <SquareButton
            content="Save"
            onClick={this.onSubmit}
            className="save-mapping"
            bsStyle={ButtonStyle.PRIMARY}
            disabled={
              !this.state.deviceFilters.name_filters ||
              !this.state.deviceFilters.serial_filters
                ? true
                : false
            }
          />
        </div>
      </div>
    );
  }
}
