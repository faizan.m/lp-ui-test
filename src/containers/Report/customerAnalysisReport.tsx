import React from "react";
import DateRangePicker from "react-daterange-picker";
import { connect } from "react-redux";

import SquareButton from "../../components/Button/button";
import Select from "../../components/Input/Select/select";
import {
  CUSTOMER_ANALYSIS_REPORT_SUCCESS,
  getCustomerAnalysisReportTypes,
  reportCustomerAnalysis,
} from "../../actions/report";
import {
  fetchProjectsForCustomer,
  FETCH_PROJECTS_LIST_SUCCESS,
  reportProjectAudit,
} from "../../actions/pmo";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import { exportTextToCSVFile } from "../../utils/download";
import Checkbox from "../../components/Checkbox/checkbox";
import "react-daterange-picker/dist/css/react-calendar.css";
import "./style.scss";

enum PageType {
  customerAnalysisReport,
  projectAudit,
}

interface IProvideReportProps extends ICommonProps {
  user: ISuperUser;
  customers: ICustomerShort[];
  customerAnalysisReportTypes: any[];
  getCustomerAnalysisReportTypes: any;
  reportCustomerAnalysis: any;
  fetchProjectsForCustomer: any;
  reportProjectAudit: any;
}

interface IProvideReportState {
  currentPage: {
    pageType: PageType;
  };
  reportError: string;
  customerId: any;
  report_type: string;
  report_sub_types: any[];
  report_sub_type: string;
  value: any;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
  isDatePickerEnabled: boolean;
  isFetching: boolean;
  loadingProject: boolean;
  project: any;
  projects: any;
  selectAllCustomer: boolean;
  selectAllProject: boolean;
  file_name: string;
  error: {
    customer: IFieldValidation;
    project: IFieldValidation;
  };
}

class ProvideReport extends React.Component<
  IProvideReportProps,
  IProvideReportState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  constructor(props: IProvideReportProps) {
    super(props);
    this.state = {
      currentPage: {
        pageType: PageType.customerAnalysisReport,
      },
      reportError: "",
      customerId: "",
      report_type: "",
      report_sub_type: "",
      value: null,
      isDatePickerSelected: false,
      isDateValueSelected: false,
      report_sub_types: [],
      isDatePickerEnabled: false,
      isFetching: false,
      project: [],
      projects: [],
      loadingProject: false,
      selectAllCustomer: false,
      selectAllProject: false,
      file_name: "Project Audit",
      error: {
        customer: { ...ProvideReport.emptyErrorState },
        project: { ...ProvideReport.emptyErrorState },
      },
    };
  }

  componentDidMount() {
    this.props.getCustomerAnalysisReportTypes();
    this.getProjectWithFilter();
  }

  getProjectWithFilter = () => {
    this.setState({
      loadingProject: true,
    });
    this.props
      .fetchProjectsForCustomer(this.state.customerId)
      .then((action) => {
        if (action.type === FETCH_PROJECTS_LIST_SUCCESS) {
          this.setState({
            projects: action.response,
            loadingProject: false,
          });
        } else {
          this.setState({
            projects: [],
            loadingProject: false,
          });
        }
      });
  };
  changePage = (pageType: PageType) => {
    this.setState({
      reportError: "",
      customerId: "",
      report_type: "",
      report_sub_type: "",
      value: null,
      isDatePickerSelected: false,
      isDateValueSelected: false,
      report_sub_types: [],
      isDatePickerEnabled: false,
      isFetching: false,
      project: [],
      projects: [],
      loadingProject: false,
      selectAllCustomer: false,
      selectAllProject: false,
      file_name: "Project Audit",
      error: {
        customer: { ...ProvideReport.emptyErrorState },
        project: { ...ProvideReport.emptyErrorState },
      },
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="report__header">
        {
          <div
            className={`report__header-link ${
              currentPage.pageType === PageType.customerAnalysisReport
                ? "report__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.customerAnalysisReport)}
          >
            Customer Analysis Report
          </div>
        }
        {
          <div
            className={`report__header-link ${
              currentPage.pageType === PageType.projectAudit
                ? "report__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.projectAudit)}
          >
            Project Audit
          </div>
        }
      </div>
    );
  };

  validateAuditReportSelection = (data: any) => {
    let valid = true;
    if (data.length > 20) valid = false;
    return valid;
  };

  handleCustomerChange = (event: any, fetchProject) => {
    if (!this.validateAuditReportSelection(event.target.value)) {
      const error: any = this.state.error;
      error.customer.errorState = IValidationState.ERROR;
      error.customer.errorMessage = "Upto 20 selections allowed";

      this.setState({ error });
      return;
    }

    this.setState(
      {
        customerId: event.target.value,
        project: [],
        projects: [],
        selectAllCustomer: false,
        selectAllProject: false,
        value: null,
        isDatePickerSelected: false,
        isDateValueSelected: false,
        error: {
          customer: { ...ProvideReport.emptyErrorState },
          project: { ...ProvideReport.emptyErrorState },
        },
      },
      () => {
        fetchProject && this.getProjectWithFilter();
      }
    );
  };

  handleChangeSelectAllCustomer = (e: any) => {
    const checked = e.target.checked;
    this.setState(
      {
        selectAllCustomer: checked,
        selectAllProject: false,
        customerId: [],
        project: [],
        projects: [],
        error: {
          customer: { ...ProvideReport.emptyErrorState },
          project: { ...ProvideReport.emptyErrorState },
        },
      },
      () => {
        checked && this.getProjectWithFilter();
      }
    );
  };

  handleChangeSelectAlllProject = (e: any) => {
    const checked = e.target.checked;
    this.setState({
      selectAllProject: checked,
      project: checked ? this.state.projects.map((x) => x.id) : [],
      error: {
        customer: { ...ProvideReport.emptyErrorState },
        project: { ...ProvideReport.emptyErrorState },
      },
    });
  };
  handleProjectChange = (event: any) => {
    if (!this.validateAuditReportSelection(event.target.value)) {
      const error: any = this.state.error;
      error.project.errorState = IValidationState.ERROR;
      error.project.errorMessage = "Upto 20 selections allowed";

      this.setState({ error });
      return;
    }

    this.setState({
      project: event.target.value,
      selectAllProject: false,
      error: {
        customer: { ...ProvideReport.emptyErrorState },
        project: { ...ProvideReport.emptyErrorState },
      },
    });
  };

  handleReportChange = (event: any) => {
    // tslint:disable-next-line: variable-name
    let report_sub_types_list = [];
    // tslint:disable-next-line: variable-name
    const selected_report = event.target.value;
    this.props.customerAnalysisReportTypes.forEach((report, index) => {
      if (selected_report === report.type) {
        report_sub_types_list = report.sub_types;
        if (report_sub_types_list && report_sub_types_list[0].label !== "All") {
          report_sub_types_list.unshift({
            type: "All",
            label: "All",
            display_datepicker: true,
          });
        }
      }
    });

    this.setState({
      report_type: selected_report,
      report_sub_type: "",
      isDatePickerEnabled: false,
      report_sub_types: report_sub_types_list,
    });
  };

  handleReportTypeChange = (event: any) => {
    // tslint:disable-next-line: variable-name
    let is_date_picker_enabled = false;
    // tslint:disable-next-line: variable-name
    const selected_report_sub_type = event.target.value;
    this.state.report_sub_types.forEach((reportSubType, index) => {
      if (selected_report_sub_type === reportSubType.type) {
        is_date_picker_enabled = reportSubType.display_datepicker;
      }
    });
    this.setState({
      report_sub_type: event.target.value,
      isDatePickerEnabled: is_date_picker_enabled,
      isDatePickerSelected: false,
    });
  };

  downloadReport = () => {
    let filters = {};
    if (this.state.report_sub_type === "All") {
      filters = {
        report_type: this.state.report_type,
        start_date: this.state.value ? this.state.value.start : undefined,
        end_date: this.state.value ? this.state.value.end : undefined,
      };
    } else {
      filters = {
        report_type: this.state.report_type,
        report_sub_type: this.state.report_sub_type,
        start_date: this.state.value ? this.state.value.start : undefined,
        end_date: this.state.value ? this.state.value.end : undefined,
      };
    }
    this.setState({ reportError: "", isFetching: true });
    this.props
      .reportCustomerAnalysis(filters, this.state.customerId)
      .then((action) => {
        if (action.type === CUSTOMER_ANALYSIS_REPORT_SUCCESS) {
          if (action.response && action.response !== "") {
            if (action.response.message) {
              this.setState({
                reportError: action.response.message,
              });
            }
            if (action.response.file_path) {
              const url = action.response.file_path;
              const link = document.createElement("a");
              link.href = url;
              link.target = "_blank";
              link.setAttribute("download", action.response.file_name);
              document.body.appendChild(link);
              link.click();
            }
          } else {
            this.setState({
              reportError: "No report available.",
            });
          }
        } else {
          this.setState({
            reportError: "Error occured while downloading report.",
          });
        }
        this.setState({ isFetching: false });
      });
  };

  downloadProjectAuditReport = () => {
    let filters = {};
    filters = {
      project_ids: this.state.project,
      start_date: this.state.value ? this.state.value.start : undefined,
      end_date: this.state.value ? this.state.value.end : undefined,
  };
    this.setState({ reportError: "", isFetching: true });
    this.props.reportProjectAudit(filters).then((action) => {
      if (action.type === "PROJECT_AUDIT_SUCCESS") {
        if (action.response && action.response !== "") {
          if (action.response.message) {
            this.setState({
              reportError: action.response.message,
            });
          }
          exportTextToCSVFile(action.response, this.state.file_name);
        } else {
          this.setState({
            reportError: "No report available.",
          });
        }
      } else {
        this.setState({
          reportError: "Error occured while downloading report.",
        });
      }
      this.setState({ isFetching: false });
    });
  };

  onSelect = (value, states) => {
    this.setState({
      value,
      isDatePickerSelected: false,
      isDateValueSelected: true,
    });
  };

  showDateRangePicker = () => {
    this.setState({ isDatePickerSelected: !this.state.isDatePickerSelected });
    this.setState({ isDateValueSelected: false });
  };

  isAlphaNumeric = (str: string): boolean => {
    return str.trim().length == 0 || /^[a-zA-Z0-9][\w- ]*$/.test(str.trim());
  }

  onDateClear = () => {
    this.setState({ value: null, isDateValueSelected: false})
  }

  renderCustomerAnalysisReport = () => {
    const customers = this.props.customers
      ? this.props.customers.map((cust) => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];

    const types = this.props.customerAnalysisReportTypes
      ? Object.keys(this.props.customerAnalysisReportTypes).map(
          (reportType, index) => ({
            label: this.props.customerAnalysisReportTypes[index].label,
            value: this.props.customerAnalysisReportTypes[index].type,
          })
        )
      : [];

    // tslint:disable-next-line: variable-name
    const sub_types = this.state.report_sub_types
      ? Object.keys(this.state.report_sub_types).map((reportType, index) => ({
          label: this.state.report_sub_types[index].label,
          value: this.state.report_sub_types[index].type,
        }))
      : [];

    return (
      <div className="customer-analysis-report">
        <div className="field__label customers customer-report">
          <label className="field__label-label">Customer</label>
          <span className="field__label-required" />
          <div className="field__input">
            <Select
              name="customerId"
              value={this.state.customerId}
              onChange={(e) => this.handleCustomerChange(e, false)}
              options={customers}
              searchable={true}
              multi={false}
              placeholder="Select Customer"
            />
          </div>
        </div>
        <div className="field__label types customer-report">
          <label className="field__label-label">Report Type</label>
          <span className="field__label-required" />
          <div className="field__input">
            <Select
              name="report_type"
              value={this.state.report_type}
              onChange={this.handleReportChange}
              options={types}
              searchable={true}
              multi={false}
              placeholder="Select type"
            />
          </div>
        </div>
        <div className="field__label types customer-report">
          <label className="field__label-label">Report Sub Type</label>
          <span className="field__label-required" />
          <div className="field__input">
            <Select
              name="report_sub_type"
              value={this.state.report_sub_type}
              onChange={this.handleReportTypeChange}
              options={sub_types}
              searchable={true}
              multi={false}
              placeholder="Select Sub type"
              disabled={types.length === 0 ? true : false}
            />
          </div>
        </div>
        <div className="field__label types customer-report">
          {this.state.isDatePickerEnabled && (
            <label className="field__label-label">Date Range</label>
          )}
          <div className="date-range-box">
            {this.state.isDatePickerEnabled && (
              <SquareButton
                onClick={this.showDateRangePicker}
                content=""
                bsStyle={ButtonStyle.DEFAULT}
                disabled={!this.state.customerId || !this.state.report_type}
                className="date-button"
              />
            )}

            {this.state.isDatePickerEnabled &&
              this.state.isDatePickerSelected &&
              !this.state.isDateValueSelected && (
                <DateRangePicker
                  value={this.state.value}
                  onSelect={this.onSelect}
                  singleDateRange={true}
                  minimumDate={new Date("1990/01/01")}
                  maximumDate={new Date()}
                  numberOfCalendars={1}
                  showLegend={true}
                />
              )}
            {this.state.isDatePickerEnabled &&
              !this.state.isDatePickerSelected &&
              this.state.isDateValueSelected && (
                <>
                  <div className="date-value">
                    {this.state.value.start.format("MM/DD/YYYY")}
                    {" - "}
                    {this.state.value.end.format("MM/DD/YYYY")}
                  </div>
                  <div className="date-clear" onClick={this.onDateClear}>&#x00d7;</div>
              </>
              )}
          </div>
        </div>
        <SquareButton
          onClick={() => this.downloadReport()}
          content="Download"
          bsStyle={ButtonStyle.PRIMARY}
          disabled={this.idDownloadDisable()}
          className="download-report customer-report"
        />
      </div>
    );
  };

  idDownloadDisable = () => {
    let isDisable = false;
    isDisable =
      !this.state.customerId ||
      !this.state.report_type ||
      !this.state.report_sub_type;

    return isDisable;
  };

  idDownloadDisableProjectAudit = () => {
    let isDisable = false;

    if (this.state.project.length === 0 || !this.isAlphaNumeric(this.state.file_name)) {
      isDisable = true;
    }

    return isDisable;
  };

  renderProjectAudit = () => {
    const customers = this.props.customers
      ? this.props.customers.map((cust) => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];

    const projects = this.state.projects
      ? this.state.projects.map((project) => ({
          value: project.id,
          label: project.title,
        }))
      : [];

    return (
      <div className="customer-analysis-report">
        <Input
          field={{
            value: this.state.customerId,
            label: (
              <div className="override ">
                Customer
                <Checkbox
                  isChecked={this.state.selectAllCustomer}
                  name="total_sites_override"
                  className="hours-override"
                  onChange={(e) => this.handleChangeSelectAllCustomer(e)}
                >
                  All customers
                </Checkbox>
              </div>
            ),
            type: InputFieldType.PICKLIST,
            isRequired: false,
            options: customers,
          }}
          width={12}
          name="customerId"
          onChange={(e) => this.handleCustomerChange(e, true)}
          placeholder="Select Customer"
          className={`customer-report ${
            this.state.customerId.length > 10 ? "all-customer" : ""
          }`}
          multi={true}
          clearable={true}
          disabled={this.state.selectAllCustomer}
          error={this.state.error.customer}
        />
        <Input
          field={{
            value: this.state.project,
            label: (
              <div className="override ">
                Project
                <Checkbox
                  isChecked={this.state.selectAllProject}
                  name="total_sites_override"
                  className="hours-override"
                  onChange={(e) => this.handleChangeSelectAlllProject(e)}
                  disabled={this.state.loadingProject}
                >
                  All Projects
                </Checkbox>
              </div>
            ),
            type: InputFieldType.PICKLIST,
            isRequired: false,
            options: projects,
          }}
          width={12}
          name="project"
          onChange={(e) => this.handleProjectChange(e)}
          placeholder="Select Project"
          className={`customer-report ${
            this.state.project.length > 10 ? "all-customer" : ""
          }`}
          multi={true}
          loading={this.state.loadingProject}
          clearable={true}
          error={this.state.error.project}
        />
        <Input
          field={{
            label: "File Name",
            type: InputFieldType.TEXT,
            value: this.state.file_name,
            isRequired: false,
          }}
          width={12}
          className={`customer-report`}
          multi={false}
          name="name"
          onChange={(e) => {
            this.setState({ file_name: e.target.value });
          }}
          error={{
            errorState: this.isAlphaNumeric(this.state.file_name) ? IValidationState.SUCCESS : IValidationState.ERROR,
            errorMessage: "Filename should only contain hyphen(-), underscore(_) whitespace and alphanumeric characters."
          }}
          placeholder={`Enter file name`}
        />
        <div className="field__label types customer-report">
          {this.state.customerId && this.state.project && (
            <label className="field__label-label">Date Range</label>
          )}
          <div className="date-range-box">
            {this.state.customerId && this.state.project && (
              <SquareButton
                onClick={this.showDateRangePicker}
                content=""
                bsStyle={ButtonStyle.DEFAULT}
                disabled={!this.state.customerId || !this.state.project}
                className="date-button"
              />
            )}
            {this.state.customerId &&
              this.state.project &&
              this.state.isDatePickerSelected &&
              !this.state.isDateValueSelected && (
                <DateRangePicker
                  value={this.state.value}
                  onSelect={this.onSelect}
                  singleDateRange={true}
                  minimumDate={new Date("1990/01/01")}
                  maximumDate={new Date()}
                  numberOfCalendars={1}
                  showLegend={true}
                />
              )}
            {this.state.customerId &&
              this.state.project &&
              !this.state.isDatePickerSelected &&
              this.state.isDateValueSelected && (
                <>
                  <div className="date-value">
                    {this.state.value.start.format("MM/DD/YYYY")}
                    {" - "}
                    {this.state.value.end.format("MM/DD/YYYY")}
                  </div>
                  <div className="date-clear" onClick={this.onDateClear}>&#x00d7;</div>
                </>
              )}
          </div>
        </div>

        <SquareButton
          onClick={() => this.downloadProjectAuditReport()}
          content="Download"
          bsStyle={ButtonStyle.PRIMARY}
          disabled={this.idDownloadDisableProjectAudit()}
          className="download-report customer-report"
        />
      </div>
    );
  };

  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="provider-report">
        <div className="loader">
          <Spinner show={this.state.isFetching} />
        </div>
        <div className="report">
          {this.renderTopBar()}
          {this.state.reportError && (
            <div className="no-device-error">{this.state.reportError}</div>
          )}
          {currentPage.pageType === PageType.customerAnalysisReport && (
            <div>{this.renderCustomerAnalysisReport()}</div>
          )}
          {currentPage.pageType === PageType.projectAudit && (
            <div>{this.renderProjectAudit()}</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customers: state.customer.customersShort,
  customerAnalysisReportTypes: state.report.customerAnalysisReportTypes,
});

const mapDispatchToProps = (dispatch: any) => ({
  getCustomerAnalysisReportTypes: () =>
    dispatch(getCustomerAnalysisReportTypes()),
  reportCustomerAnalysis: (filter: any, customerId: string) =>
    dispatch(reportCustomerAnalysis(filter, customerId)),
  reportProjectAudit: (filter: any) => dispatch(reportProjectAudit(filter)),
  fetchProjectsForCustomer: (customerIds: number[]) =>
    dispatch(fetchProjectsForCustomer(customerIds)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProvideReport);
