import React, { Component } from "react";
import { connect } from "react-redux";
import DateRangePicker from "react-daterange-picker";
import {
  downloadFireReport,
  FETCH_FIRE_REPORT_SUCCESS,
  FETCH_FIRE_REPORT_FAILURE,
} from "../../actions/report";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Checkbox from "../../components/Checkbox/checkbox";
import {
  fireReportPartnerSitesCRUD,
  FIRE_REPORT_SITES_SUCCESS,
} from "../../actions/setting";
import {
  FETCH_SITES_FAILURE,
  FETCH_SITES_SUCCESS,
  fetchSitesProvidersCustomersUser,
} from "../../actions/inventory";

interface IFireReportFilters {
  start_date: string;
  end_date: string;
  site_crm_ids: number[];
}

interface IFireReportProps {
  customers: ICustomerShort[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchSites: () => Promise<any>;
  fetchCustomerSites: (customerId: number) => Promise<any>;
  downloadFireReport: (data: IFireReportFilters) => Promise<any>;
}

interface IFireReportState {
  customer: number;
  selectedAllCustomers: boolean;
  sites: number[];
  selectedAllSites: boolean;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
  filterDateValue: any;
  filterDateFrom: string;
  filterDateTo: string;
  downloadingReport: boolean;
  siteSettings: ISite[];
  errorMsg: string;
  unconfiguredSettings: boolean;
  fetchingSites: boolean;
  siteOptions: IPickListOptions[];
}

class FireReport extends Component<IFireReportProps, IFireReportState> {
  constructor(props: IFireReportProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    filterDateTo: "",
    siteOptions: [],
    siteSettings: [],
    sites: [],
    filterDateFrom: "",
    errorMsg: "",
    fetchingSites: false,
    customer: null,
    selectedAllSites: false,
    downloadingReport: false,
    isDateValueSelected: false,
    filterDateValue: undefined,
    selectedAllCustomers: false,
    unconfiguredSettings: false,
    isDatePickerSelected: false,
  });

  componentDidMount() {
    this.fetchDefaultSites();
  }

  fetchDefaultSites = () => {
    this.props.fetchSites().then((action) => {
      if (action.type === FIRE_REPORT_SITES_SUCCESS) {
        if (action.response.length === 0) {
          this.setState({ unconfiguredSettings: true });
        } else {
          this.setState({ siteSettings: action.response });
        }
      }
    });
  };

  handleChangeCustomer = (event: React.ChangeEvent<HTMLInputElement>) => {
    const customerId = Number(event.target.value);
    if (customerId) {
      this.setState({ fetchingSites: true });
      this.props
        .fetchCustomerSites(customerId)
        .then((action) => {
          if (action.type === FETCH_SITES_SUCCESS) {
            const configuredSiteNames: Set<string> = new Set(
              this.state.siteSettings.map((el) => el.name)
            );
            const customerSites: ISite[] = action.response;
            const siteOptions: IPickListOptions[] = customerSites
              .filter((el) => configuredSiteNames.has(el.name))
              .map((el) => ({
                value: el.site_id,
                label: el.name,
              }));
            this.setState({
              siteOptions,
              sites: [],
            });
          } else if (action.type === FETCH_SITES_FAILURE) {
            this.props.addErrorMessage(
              "Error fetching sites for the selected customer"
            );
          }
        })
        .finally(() => {
          this.setState({ fetchingSites: false });
        });
    }
    this.setState({ customer: customerId });
  };

  handleChangeSites = (event: { target: { value: number[] } }) => {
    this.setState({ sites: event.target.value });
  };

  onDateRangeSelect = (value, states) => {
    this.setState({
      filterDateFrom: fromISOStringToFormattedDate(
        value.start._d,
        "YYYY-MM-DD"
      ),
      filterDateTo: fromISOStringToFormattedDate(value.end._d, "YYYY-MM-DD"),
      filterDateValue: value,
      isDatePickerSelected: false,
      isDateValueSelected: true,
    });
  };

  resetDate = () => {
    this.setState({
      filterDateFrom: "",
      filterDateTo: "",
      filterDateValue: undefined,
      isDatePickerSelected: false,
      isDateValueSelected: false,
    });
  };

  getCustomerOptions = (): IPickListOptions[] => {
    return this.props.customers
      ? this.props.customers.map((cust) => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];
  };

  selectAllCustomers = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      selectedAllCustomers: e.target.checked,
      customer: null,
      sites: [],
      selectedAllSites: false,
    });
  };

  selectAllSites = (e: React.ChangeEvent<HTMLInputElement>) => {
    const allSitesChecked: boolean = e.target.checked;
    const sites: number[] =
      this.state.selectedAllCustomers || allSitesChecked
        ? []
        : this.state.siteOptions.map((el) => el.value);
    this.setState({
      sites,
      selectedAllSites: allSitesChecked,
    });
  };

  generatePayload = (): IFireReportFilters => {
    const start_date: string = this.state.filterDateFrom
      ? this.state.filterDateFrom
      : null;
    const end_date: string = this.state.filterDateTo
      ? this.state.filterDateTo
      : null;
    let site_crm_ids: number[] = null;
    if (this.state.customer) {
      if (this.state.selectedAllSites)
        site_crm_ids = this.state.siteOptions.map((el) => el.value);
      else site_crm_ids = this.state.sites;
    }
    return { start_date, end_date, site_crm_ids };
  };

  onDownloadFireReport = () => {
    // Validation
    if (!this.state.selectedAllCustomers && !this.state.customer) {
      this.setState({ errorMsg: "Please select a customer option" });
      return;
    } else if (
      this.state.customer &&
      this.state.sites.length === 0 &&
      !this.state.selectedAllSites
    ) {
      this.setState({
        errorMsg: "Please select site option for the selected customer",
      });
      return;
    }

    this.setState({
      errorMsg: "",
      downloadingReport: true,
    });

    const payload: IFireReportFilters = this.generatePayload();
    this.props
      .downloadFireReport(payload)
      .then((action) => {
        if (action.type === FETCH_FIRE_REPORT_SUCCESS) {
          const text = action.response;
          var filename = `fire_report_${new Date().toISOString()}.csv`;

          var blob = new Blob([text], { type: "text/csv;charset=utf-8;" });
          var link = document.createElement("a");
          if (link.download !== undefined) {
            // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = "hidden";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            this.props.addSuccessMessage("Report downloaded successfully!");
          }
        } else if (action.type === FETCH_FIRE_REPORT_FAILURE) {
          if (action.errorList.data.detail) {
            this.props.addErrorMessage(action.errorList.data.detail);
          } else {
            this.props.addErrorMessage("Error downloading finance report!");
          }
        }
      })
      .finally(() => this.setState({ downloadingReport: false }));
  };

  render() {
    return this.state.unconfiguredSettings ? (
      <div className="fire-no-settings">
        Please configure the sites in Site Settings
      </div>
    ) : (
      <div className="fire-report-container">
        <Spinner
          show={this.state.downloadingReport}
          className="fire-report-loader"
        />
        <div className="fire-report-filters">
          <div className="fire-report-row">
            <Input
              field={{
                label: "Customer",
                type: InputFieldType.PICKLIST,
                value: this.state.customer,
                options: this.getCustomerOptions(),
                isRequired: false,
              }}
              width={6}
              multi={false}
              name="customer"
              onChange={(e) => this.handleChangeCustomer(e)}
              placeholder={`Select Customer`}
              disabled={this.state.selectedAllCustomers}
            />
            <Checkbox
              isChecked={this.state.selectedAllCustomers}
              name="selectAllCustomers"
              onChange={(e) => this.selectAllCustomers(e)}
            >
              All Customers
            </Checkbox>
          </div>
          <div className="fire-report-row">
            <Input
              field={{
                label: "Sites",
                type: InputFieldType.PICKLIST,
                value: this.state.sites,
                options: this.state.siteOptions,
                isRequired: false,
              }}
              width={6}
              multi={true}
              name="sites"
              onChange={(e) => this.handleChangeSites(e)}
              loading={this.state.fetchingSites}
              placeholder={`Select Sites`}
              disabled={
                (!this.state.selectedAllCustomers && !this.state.customer) ||
                this.state.selectedAllCustomers ||
                this.state.selectedAllSites
              }
            />
            <Checkbox
              isChecked={this.state.selectedAllSites}
              name="selectAllSites"
              onChange={(e) => this.selectAllSites(e)}
              disabled={
                (!this.state.selectedAllCustomers && !this.state.customer) ||
                this.state.selectedAllCustomers
              }
            >
              All Sites
            </Checkbox>
          </div>
          <div className="fire-report-row">
            <div className="date-range-box">
              <SquareButton
                onClick={() =>
                  this.setState({
                    isDatePickerSelected: !this.state.isDatePickerSelected,
                    isDateValueSelected: false,
                  })
                }
                content=""
                bsStyle={ButtonStyle.DEFAULT}
                className="date-button"
              />
              {this.state.isDatePickerSelected &&
                !this.state.isDateValueSelected && (
                  <DateRangePicker
                    value={this.state.filterDateValue}
                    onSelect={this.onDateRangeSelect}
                    singleDateRange={true}
                    minimumDate={new Date("1990/01/01")}
                    maximumDate={new Date()}
                    numberOfCalendars={1}
                    showLegend={true}
                    clearAriaLabel={"reset"}
                    ClearIcon={"reset"}
                  />
                )}
              {!this.state.isDatePickerSelected &&
                this.state.isDateValueSelected && (
                  <div className="date-value" title={"MM-DD-YYYY"}>
                    {fromISOStringToFormattedDate(
                      this.state.filterDateFrom,
                      "MM-DD-YYYY"
                    )}
                    {" \u2013 "}
                    {fromISOStringToFormattedDate(
                      this.state.filterDateTo,
                      "MM-DD-YYYY"
                    )}
                  </div>
                )}
              {!this.state.isDatePickerSelected &&
                this.state.isDateValueSelected && (
                  <div
                    onClick={(e) => this.resetDate()}
                    className="reset-button"
                  >
                    x
                  </div>
                )}
              {!this.state.isDatePickerSelected &&
                !this.state.isDateValueSelected && (
                  <div className="date-value">
                    Filter by Sales Order Creation Date
                  </div>
                )}
            </div>
          </div>
        </div>
        <div className="fire-report-error">{this.state.errorMsg}</div>
        <SquareButton
          onClick={this.onDownloadFireReport}
          content="Download"
          bsStyle={ButtonStyle.PRIMARY}
          className="download-fire-report"
          disabled={this.state.downloadingReport}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  downloadFireReport: (data: IFireReportFilters) =>
    dispatch(downloadFireReport(data)),
  fetchCustomerSites: (customerId: number) =>
    dispatch(fetchSitesProvidersCustomersUser(customerId)),
  fetchSites: () => dispatch(fireReportPartnerSitesCRUD(HTTPMethods.GET)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FireReport);
