import React from "react";
import { connect } from "react-redux";
import { cloneDeep, isEmpty } from "lodash";
import {
  fetchContractStatuses,
  fetchSitesProvidersCustomersUser,
  fetchTypes,
} from "../../actions/inventory";
import {
  BATCH_REPORT_SUCCESS_PROVIDER,
  dataAnomalyReport,
  getCustomerReportTypes,
  POST_ANOMALY_REPORT_FAILURE,
  POST_ANOMALY_REPORT_SUCCESS,
  reportProviderCustomerUser,
  editScheduledReport,
  POST_SH_REPORT_FAILURE,
  POST_SH_REPORT_SUCCESS,
  postScheduledReport,
  getScheduledReportByCat,
  GetScheduledReport,
  fetchFrequencySHReport,
} from "../../actions/report";
import {
  getFeatureAccessUsers,
 ACCESS_FEATURE_USERS_SUCCESS,
} from "../../actions/providerAdminUser";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import { allowFeaturePermission } from "../../utils/permissions";
import SquareButton from "../../components/Button/button";
import Select from "../../components/Input/Select/select";
import Spinner from "../../components/Spinner";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import Checkbox from "../../components/Checkbox/checkbox";
import FinanceReport from "./financeReport";
import FireExportReport from "./fireExportReport";
import DeviceReportBundle from "./deviceReportBundle";
import "react-daterange-picker/dist/css/react-calendar.css";
import "./style.scss";

export interface IFiltersReport {
  site_id?: number[];
  device_type?: any[];
  contract_status_id?: number[];
  customer_id?: number[];
  report_type?: {
    name: string;
    category: string;
    type: string;
  };
}
interface IProvideReportProps extends ICommonProps {
  fetchDevicesCustomerUser: TFetchDevicesCustomerUser;
  fetchSitesProvidersCustomersUser: any;
  fetchContractStatuses: TFetchContractStatuses;
  reportProviderCustomerUser: any;
  fetchTypes: any;
  sites: any[];
  types: any[];
  contractStatuses: IContractStatus[];
  user: ISuperUser;
  reportError: string;
  isFetching: boolean;
  customers: ICustomerShort[];
  dataAnomalyReport: any;
  reportTypes: any[];
  getCustomerReportTypes: any;
  reportFrequencies: any;
  isFetchingSH: boolean;
  scheduledReportByCat: any;
  editScheduledReport: any;
  addErrorMessage: any;
  addSuccessMessage: any;
  postScheduledReport: any;
  getScheduledReportByCat: any;
  GetScheduledReport: any;
  fetchFrequencySHReport: any;
  scheduledReportList: any;
  getFeatureAccessUsers: (feature: string) => Promise<any>;
}

interface IProvideReportState {
  error: {
    [fieldName: string]: IFieldValidation;
  };
  filters: IFiltersReport;
  financeAccessList: IFeatureAccess[];
  fireAccessList: IFeatureAccess[];
  report: any;
  reportError: string;
  reportTypeList: any[];
  reportType: string;
  anomalyReportError: boolean;
  anomalyReportSuccess: boolean;
  customerId: any;
  selectedAllCustomers: boolean;
  scheduledReportByCat: any;
}

class ProvideReport extends React.Component<
  IProvideReportProps,
  IProvideReportState
> {
  constructor(props: IProvideReportProps) {
    super(props);

    this.state = {
      customerId: "",
      error: {},
      filters: {
        site_id: [],
        device_type: [],
        contract_status_id: [],
        customer_id: [],
        report_type: {
          name: "",
          category: "",
          type: "",
        },
      },
      report: "",
      reportType: "",
      reportError: "",
      financeAccessList: [],
      fireAccessList: [],
      reportTypeList: [],
      anomalyReportError: false,
      anomalyReportSuccess: false,
      selectedAllCustomers: false,
      scheduledReportByCat: [],
    };
  }

  componentDidMount() {
    this.fetchFeatureAccessList();
    this.props.fetchContractStatuses();
    this.props.fetchTypes();
    this.props.getCustomerReportTypes();
    this.props.getScheduledReportByCat("DEVICES");
    this.props.GetScheduledReport();
    this.props.fetchFrequencySHReport();
    this.setState({ scheduledReportByCat: this.props.scheduledReportByCat });
  }

  componentDidUpdate(prevProps: IProvideReportProps) {
    if (
      this.props.scheduledReportByCat &&
      this.props.scheduledReportByCat !== prevProps.scheduledReportByCat
    ) {
      this.setState({ scheduledReportByCat: this.props.scheduledReportByCat });
    }
    if (
      this.props.scheduledReportList &&
      prevProps.scheduledReportList !== this.props.scheduledReportList
    ) {
      const scheduledReportByCat = cloneDeep(this.state.scheduledReportByCat);
      scheduledReportByCat &&
        scheduledReportByCat.map((type, index) => {
          scheduledReportByCat[index].frequency = "NEVER";
          scheduledReportByCat[index].report_id = "";
        });
      scheduledReportByCat &&
        scheduledReportByCat.map((type, index) => {
          this.props.scheduledReportList.map((report, i) => {
            if (type.type === report.scheduled_report_type.type) {
              scheduledReportByCat[index].frequency = report.frequency;
              scheduledReportByCat[index].report_id = report.id;
            }
          });
        });
      this.setState({ scheduledReportByCat });
    }
  }

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
      reportError: "",
    }));

    if (
      targetName === "customer_id" &&
      (targetValue.length === 0 || targetValue.length > 1)
    ) {
      this.setState((prevState) => ({
        filters: {
          ...prevState.filters,
          site: null,
        },
        reportError: "",
      }));
    }

    if (targetName === "customer_id" && targetValue.length === 1) {
      this.props.fetchSitesProvidersCustomersUser(targetValue[0]);
    }
  };

  fetchFeatureAccessList = () => {
    this.props.getFeatureAccessUsers("FinanceReport").then((action) => {
      if (action.type ===ACCESS_FEATURE_USERS_SUCCESS) {
        this.setState({
          financeAccessList: action.response,
        });
      }
    });
    this.props.getFeatureAccessUsers("Fire Report").then((action) => {
      if (action.type ===ACCESS_FEATURE_USERS_SUCCESS) {
        this.setState({
          fireAccessList: action.response,
        });
      }
    });
  };
  
  onReportChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const reportTypeList = this.props.reportTypes[e.target.value];
    this.setState({
      report: e.target.value,
      reportTypeList,
      reportType: "",
      reportError: "",
    });
  };

  onReportTypeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    const reportTypeList = this.state.reportTypeList[targetValue];

    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: reportTypeList,
      },
      reportType: targetValue,
      reportError: "",
    }));
  };

  onResetClick = () => {
    this.setState((prevState) => ({
      filters: {
        site_id: [],
        device_type: [],
        contract_status_id: [],
        customer_id: [],
        report_type: {
          name: "",
          category: "",
          type: "",
        },
      },
      selectedAllCustomers: false,
      report: "",
      reportType: "",
      reportError: "",
    }));
  };

  onRunClick = () => {
    if (this.state.filters) {
      const filters: any = {};
      if (
        this.state.filters.customer_id !== null &&
        this.state.filters.customer_id.length !== 0
      ) {
        filters.customer_id = this.state.filters.customer_id;
      }
      if (
        this.state.filters.report_type !== null &&
        this.state.filters.report_type
      ) {
        filters.report_type = this.state.filters.report_type;
      }
      if (
        this.state.filters.site_id !== null &&
        this.state.filters.site_id !== undefined &&
        this.state.filters.site_id.length !== 0
      ) {
        filters.site_id = this.state.filters.site_id;
      }
      if (
        this.state.filters.device_type !== null &&
        this.state.filters.device_type.length !== 0
      ) {
        filters.device_type = this.state.filters.device_type;
      }
      if (
        this.state.filters.contract_status_id !== null &&
        this.state.filters.contract_status_id &&
        this.state.filters.contract_status_id.length !== 0
      ) {
        filters.contract_status_id = this.state.filters.contract_status_id;
      }
      this.props.reportProviderCustomerUser(filters).then((action) => {
        if (action.type === BATCH_REPORT_SUCCESS_PROVIDER) {
          if (action.response && action.response !== "") {
            const url = action.response.file_path;
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", action.response.file_name);
            document.body.appendChild(link);
            link.click();
          } else {
            this.setState({
              reportError: "No device Found for selected filter.",
            });
          }
        }
      });
    }
  };

  handleChange = (event: any) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  selectAllCustomers = (event: any) => {
    let val = this.state.selectedAllCustomers;

    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        customer_id: [],
        device_type: [],
      },
      selectedAllCustomers: !val,
    }));
  };

  disableRunBtn = () => {
    if (this.state.selectedAllCustomers === true) {
      return this.state.reportType === "" || this.state.report === "";
    } else {
      return (
        this.state.reportType === "" ||
        this.state.report === "" ||
        isEmpty(this.state.filters.customer_id) ||
        this.state.filters.customer_id.length === 0
      );
    }
  };

  renderInventory = () => {
    const filters = this.state.filters;
    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];
    const types = this.props.types
      ? this.props.types.map((type, index) => ({
          value: type,
          label: type ? type : "N.A.",
        }))
      : [];

    const customers = this.props.customers
      ? this.props.customers.map((cust, index) => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];

    const contractStatuses = this.props.contractStatuses
      ? this.props.contractStatuses.map((contractStatus) => ({
          value: contractStatus.contract_status_id,
          label: contractStatus.contract_status,
        }))
      : [];

    const reportList = this.props.reportTypes
      ? Object.keys(this.props.reportTypes).map((reportType, index) => ({
          value: reportType,
          label: reportType,
        }))
      : [];
    const reportTypeList = this.state.reportTypeList
      ? Object.keys(this.state.reportTypeList).map((reportType, index) => ({
          value: this.state.reportTypeList[reportType].type,
          label: this.state.reportTypeList[reportType].name,
        }))
      : [];

    return (
      <div className="inventory">
        <h5 className="filter__header">FILTER</h5>
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div
          className={`filter-provider-report__body ${
            this.props.isFetching ? `loading` : ``
          }`}
        >
          <div className="field-section report-mandatory">
            <div className="field__label">
              <div className="label-customers">
                <label className="field__label-label">Customers</label>
                <span className="field__label-required" />
              </div>
              <div className="field__input">
                <Select
                  name="customer_id"
                  value={filters.customer_id}
                  onChange={this.onFilterChange}
                  options={customers}
                  searchable={true}
                  multi={true}
                  placeholder="Select Customers"
                  disabled={this.state.selectedAllCustomers}
                />
              </div>
            </div>
          </div>
          <div className="field-section report-mandatory">
            <div className="field__label">
              <div className="label-customers-checkbox">
                <div className="all-customers-checkbox">
                  <Checkbox
                    isChecked={this.state.selectedAllCustomers}
                    name="selectAllCustomers"
                    onChange={(e) => this.selectAllCustomers(e)}
                  >
                    All Customers
                  </Checkbox>
                </div>
              </div>
            </div>
          </div>
          <div className="field-section report-mandatory">
            <div className="field__label">
              <label className="field__label-label">Report Type</label>
              <span className="field__label-required" />
              <div className="field__input">
                <Select
                  name="report"
                  value={this.state.report}
                  onChange={this.onReportChange}
                  options={reportList}
                  multi={false}
                  placeholder="Select Report"
                />
              </div>
            </div>
          </div>
          <div className="field-section report-mandatory">
            <div className="field__label">
              <label className="field__label-label">Report Sub-type</label>
              <span className="field__label-required" />
              <div className="field__input">
                <Select
                  name="report_type"
                  value={this.state.reportType}
                  onChange={this.onReportTypeChange}
                  options={reportTypeList}
                  multi={false}
                  placeholder={
                    reportTypeList.length === 0
                      ? "Select Report"
                      : "Select Report Type"
                  }
                  disabled={reportTypeList.length === 0 ? true : false}
                />
              </div>
            </div>
          </div>
          {this.state.filters.customer_id &&
            this.state.filters.customer_id.length === 1 && (
              <div className="field-section">
                <div className="field__label">
                  <label className="field__label-label">Site</label>
                  <div className="field__input">
                    <Select
                      name="site_id"
                      value={filters.site_id}
                      onChange={this.onFilterChange}
                      options={sites}
                      searchable={true}
                      multi={true}
                      placeholder="Select Site"
                    />
                  </div>
                </div>
              </div>
            )}
          <div className="field-section">
            <div className="field__label">
              <label className="field__label-label">Type</label>
              <div className="field__input">
                <Select
                  name="device_type"
                  value={filters.device_type}
                  onChange={this.onFilterChange}
                  options={types}
                  multi={true}
                  searchable={true}
                  placeholder="Select Type"
                />
              </div>
            </div>
          </div>
          <div className="field-section">
            <div className="field__label">
              <label className="field__label-label">Status</label>
              <div className="field__input">
                <Select
                  name="contract_status_id"
                  value={filters.contract_status_id}
                  onChange={this.onFilterChange}
                  options={contractStatuses}
                  searchable={true}
                  multi={true}
                  placeholder="Select Status"
                />
              </div>
            </div>
          </div>
          <div className="field-section-button">
            <SquareButton
              onClick={(e) => this.onRunClick()}
              content="Run"
              bsStyle={ButtonStyle.PRIMARY}
              disabled={this.disableRunBtn()}
            />
            <SquareButton
              onClick={(e) => this.onResetClick()}
              content="Reset"
              bsStyle={ButtonStyle.DEFAULT}
            />
          </div>
        </div>
      </div>
    );
  };

  downloadAnomalyReport = () => {
    this.props.dataAnomalyReport().then((action) => {
      if (action.type === POST_ANOMALY_REPORT_SUCCESS) {
        this.setState((prevState) => ({
          anomalyReportSuccess: true,
          anomalyReportError: false,
        }));
      }
      if (action.type === POST_ANOMALY_REPORT_FAILURE) {
        this.setState((prevState) => ({
          anomalyReportError: true,
          anomalyReportSuccess: false,
        }));
      }
    });
  };

  renderDataAnomaly = () => {
    return (
      <div className="data-anomaly">
        <h5 className="data-anomaly__header">Data Anomaly Report : </h5>
        <SquareButton
          onClick={(e) => this.downloadAnomalyReport()}
          content="Request"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  renderPreBuildReport = () => {
    const customers = this.props.customers
      ? this.props.customers.map((cust) => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];

    return (
      <div className="pre-build-report">
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className="row col-md-12">
          <div
            className="col-lg-4 col-md-6 col-xs-10
          field-section report-mandatory"
          >
            <div className="field__label">
              <label className="field__label-label">Customer</label>
              <span className="field__label-required" />
              <div className={`${!this.state.customerId ? `error-input` : ""}`}>
                <Select
                  name="customerId"
                  value={this.state.customerId}
                  onChange={this.handleChange}
                  options={customers}
                  searchable={true}
                  multi={false}
                  placeholder="Select Customers"
                />
              </div>
              {!this.state.customerId && (
                <div className="select-manufacturer-error">
                  Please select customer
                </div>
              )}
            </div>
          </div>
        </div>
        <div
          className={`filter__body ${this.props.isFetching ? `loading` : ``}`}
        >
          {this.props.reportTypes &&
            Object.keys(this.props.reportTypes).map((row, index) => (
              <div key={index} className="field-section box">
                <label className="box-label">{row}</label>
                <div className="field__input">
                  {Object.keys(this.props.reportTypes[row]).map((data, i) => (
                    <div
                      key={i}
                      className="field-section report-row"
                      onClick={(e) =>
                        this.onReportDownloadClick(
                          this.props.reportTypes[row][data]
                        )
                      }
                    >
                      <div className="name">
                        {this.props.reportTypes[row][data].name}
                      </div>
                      {this.state.customerId && <div className="icon" />}
                    </div>
                  ))}
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  };

  onReportDownloadClick = (data) => {
    this.setState({
      reportError: "",
    });
    if (this.state.customerId) {
      this.props
        .reportProviderCustomerUser({
          report_type: data,
          report_format: "pdf",
          customer_id: [this.state.customerId],
        })
        .then((action) => {
          if (action.type === BATCH_REPORT_SUCCESS_PROVIDER) {
            if (action.response && action.response !== "") {
              const url = action.response.file_path;
              const link = document.createElement("a");
              link.href = url;
              link.target = "_blank";
              link.setAttribute("download", action.response.file_name);
              document.body.appendChild(link);
              link.click();
            } else {
              this.setState({
                reportError: "No device Found for selected filter.",
              });
            }
          }
        });
    }
  };

  renderScheduledReport = () => {
    const reportFrequencies = this.props.reportFrequencies
      ? Object.keys(this.props.reportFrequencies).map((reportType) => ({
          value: reportType,
          label: reportType,
        }))
      : [];

    return (
      <div className="inventory">
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetchingSH} />
        </div>
        <div className="scheduled-report-list ">
          {this.state.scheduledReportByCat &&
            this.state.scheduledReportByCat.map((row, index) => (
              <div key={index} className="scheduled-report row">
                <div className="name col-md-8">{row.display_name}</div>
                <div className="field-section  col-md-4">
                  <div className="field__label">
                    <label className="field__label-label">Frequency</label>
                    <span className="field__label-required" />
                    <div className="field__input">
                      <Select
                        name="frequency"
                        value={this.state.scheduledReportByCat[index].frequency}
                        onChange={(e) => this.onRunClickScheduled(e, index)}
                        options={reportFrequencies}
                        multi={false}
                        placeholder="Select Frequency"
                      />
                    </div>{" "}
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  };

  onRunClickScheduled = (e, index) => {
    const targetValue = e.target.value;
    const newState = cloneDeep(this.state);
    newState.scheduledReportByCat[index].frequency = targetValue;

    this.setState(newState);

    if (
      this.state.scheduledReportByCat &&
      this.state.scheduledReportByCat[index].report_id
    ) {
      this.props
        .editScheduledReport(
          this.state.scheduledReportByCat[index].report_id,
          targetValue,
          this.state.scheduledReportByCat[index].id
        )
        .then((action) => {
          if (action.type === POST_SH_REPORT_SUCCESS) {
            if (action.response && action.response !== "") {
              this.props.addSuccessMessage("Scheduled report updated.");
            }
          }
          if (action.type === POST_SH_REPORT_FAILURE) {
            this.props.addErrorMessage(
              action.errorList &&
                action.errorList.data &&
                action.errorList.data.detail
            );
          }
        });
    } else {
      this.props
        .postScheduledReport(
          targetValue,
          this.state.scheduledReportByCat[index].id
        )
        .then((action) => {
          if (action.type === POST_SH_REPORT_SUCCESS) {
            if (action.response && action.response !== "") {
              this.props.addSuccessMessage("Sheduled report subscribed.");
              const scheduledReportByCat = cloneDeep(
                this.state.scheduledReportByCat
              );
              scheduledReportByCat.map((type, i) => {
                if (type.id === action.response.scheduled_report_type) {
                  scheduledReportByCat[i].frequency = action.response.frequency;
                  scheduledReportByCat[i].report_id = action.response.id;
                }
              });
              this.setState({ scheduledReportByCat });
            }
          }
          if (action.type === POST_SH_REPORT_FAILURE) {
            this.props.addErrorMessage(
              action.errorList &&
                action.errorList.data &&
                action.errorList.data.detail
            );
          }
        });
    }
  };

  render() {
    return (
      <div className="provider-report">
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === "provider" && (
            <div className="report">
              <HorizontalTabSlider>
                <Tab title="Inventory">{this.renderInventory()}</Tab>
                <Tab title="Data Anomaly">
                  <div>
                    {this.renderDataAnomaly()}
                    {this.state.anomalyReportError && (
                      <div className="anomaly-report-message-error">
                        Please try after some time
                      </div>
                    )}
                    {this.state.anomalyReportSuccess && (
                      <div className="anomaly-report-message-success">
                        Please check your e-mail
                      </div>
                    )}
                  </div>
                </Tab>
                <Tab title="Customer Standard Report">
                  {this.renderPreBuildReport()}
                </Tab>
                <Tab title="Scheduled Reports">
                  {this.renderScheduledReport()}
                </Tab>
                <Tab
                  title="Finance"
                  hide={
                    !allowFeaturePermission(
                      this.props.user.id,
                      "FinanceReport",
                      this.state.financeAccessList
                    )
                  }
                >
                  <FinanceReport />
                </Tab>
                <Tab
                  title="FIRE Export"
                  hide={
                    !allowFeaturePermission(
                      this.props.user.id,
                      "Fire Report",
                      this.state.fireAccessList
                    )
                  }
                >
                  <FireExportReport />
                </Tab>
                <Tab title="Device Report Bundle">
                  <DeviceReportBundle />
                </Tab>
              </HorizontalTabSlider>
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  sites: state.inventory.sites,
  types: state.inventory.types,
  contractStatuses: state.inventory.contractStatuses,
  user: state.profile.user,
  reportError: state.report.reportError,
  isFetching: state.report.isFetching,
  customers: state.customer.customersShort,
  reportTypes: state.report.reportTypes,
  reportFrequencies: state.report.reportFrequencies,
  isFetchingSH: state.report.isFetchingSH,
  scheduledReportByCat: state.report.scheduledReportByCat,
  scheduledReportList: state.report.scheduledReportList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTypes: () => dispatch(fetchTypes()),
  getFeatureAccessUsers: (feature: string) =>
    dispatch(getFeatureAccessUsers(feature)),
  dataAnomalyReport: () => dispatch(dataAnomalyReport()),
  fetchSitesProvidersCustomersUser: (id: number) =>
    dispatch(fetchSitesProvidersCustomersUser(id)),
  fetchContractStatuses: () => dispatch(fetchContractStatuses()),
  reportProviderCustomerUser: (filter: any) =>
    dispatch(reportProviderCustomerUser(filter)),
  getCustomerReportTypes: () => dispatch(getCustomerReportTypes()),
  editScheduledReport: (id: string, freq: string, type: string) =>
    dispatch(editScheduledReport(id, freq, type)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  postScheduledReport: (freq: string, type: string) =>
    dispatch(postScheduledReport(freq, type)),
  getScheduledReportByCat: (cat: string) =>
    dispatch(getScheduledReportByCat(cat)),
  GetScheduledReport: () => dispatch(GetScheduledReport()),
  fetchFrequencySHReport: () => dispatch(fetchFrequencySHReport()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProvideReport);
