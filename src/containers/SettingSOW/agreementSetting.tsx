import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  agreementMarginSettingsCRUD,
  FETCH_AGREEMENT_MARGIN_SUCCESS,
} from "../../actions/setting";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";

interface AgreementSettingProps {
  agreementMargin: { id: number; margin_limit: number }[];
  agreementMarginSettingsCRUD: (
    data?: { id?: number; margin_limit: number },
    method?: string
  ) => Promise<any>;
}

const AgreementSetting: React.FC<AgreementSettingProps> = (props) => {
  const [agreementMarginLimit, setMarginLimit] = useState<number>(0);

  useEffect(() => {
    props.agreementMarginSettingsCRUD();
  }, []);

  useEffect(() => {
    if (props.agreementMargin)
      setMarginLimit(
        props.agreementMargin.length ? props.agreementMargin[0].margin_limit : 0
      );
  }, [props.agreementMargin]);

  const handleChangeMarginLimit = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMarginLimit(Number(e.target.value));
  };

  const saveAgreementSettings = () => {
    if (props.agreementMargin.length) {
      props
        .agreementMarginSettingsCRUD(
          {
            margin_limit: agreementMarginLimit,
            id: props.agreementMargin[0].id,
          },
          "put"
        )
        .then((action) => {
          if (action.type === FETCH_AGREEMENT_MARGIN_SUCCESS) {
            props.agreementMarginSettingsCRUD();
          }
        });
    } else {
      props
        .agreementMarginSettingsCRUD(
          { margin_limit: agreementMarginLimit },
          "post"
        )
        .then((action) => {
          if (action.type === FETCH_AGREEMENT_MARGIN_SUCCESS) {
            props.agreementMarginSettingsCRUD();
          }
        });
    }
  };

  return (
    <div className="agreement-settings-main">
      <div className="input-wrapper">
        <Input
          field={{
            value: agreementMarginLimit,
            label: "Margin Limit",
            type: InputFieldType.NUMBER,
            isRequired: false,
          }}
          width={4}
          name={"agreement_margin_limit"}
          placeholder="Enter Margin Limit"
          onChange={handleChangeMarginLimit}
          className="agreement_margin_limit"
        />
      </div>
      <SquareButton
        onClick={saveAgreementSettings}
        content={"Save Settings"}
        bsStyle={ButtonStyle.PRIMARY}
        className="save-agr-settings-btn"
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  agreementMargin: state.setting.agreementMargin,
});

const mapDispatchToProps = (dispatch: any) => ({
  agreementMarginSettingsCRUD: (
    data?: { id?: number; margin_limit: number },
    method?: string
  ) => dispatch(agreementMarginSettingsCRUD(data, method)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AgreementSetting);
