import React, { Component } from "react";
import { connect } from "react-redux";
import { cloneDeep, get } from "lodash";
import ReactQuill from "react-quill";
import { Mention, MentionsInput } from "react-mentions";
import {
  editSOWDOCSetting,
  fetchSOWDOCSetting,
  EDIT_DOC_SETTING_SUCCESS,
} from "../../actions/setting";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import { commonFunctions } from "../../utils/commonFunctions";

interface ISowDocSettingProps {
  docSetting: IDOCSetting;
  isFetchingDocSet: boolean;
  fetchSOWDOCSetting: () => Promise<any>;
  editSOWDOCSetting: (data: IDOCSetting) => Promise<any>;
}
interface ISowDocSettingState {
  docSetting: IDOCSetting;
  error: {
    fixed_fee_variable: IFieldValidation;
    engineering_hourly_cost: IFieldValidation;
    pm_hourly_cost: IFieldValidation;
    engineering_hourly_rate: IFieldValidation;
    after_hours_rate: IFieldValidation;
    after_hours_cost: IFieldValidation;
    integration_technician_hourly_rate: IFieldValidation;
    integration_technician_hourly_cost: IFieldValidation;
    project_management_hourly_rate: IFieldValidation;
    terms_t_and_m: IFieldValidation;
    project_management_t_and_m: IFieldValidation;
    terms_fixed_fee: IFieldValidation;
    project_management_fixed_fee: IFieldValidation;
    total_no_of_cutovers_statement: IFieldValidation;
    total_no_of_sites_statement: IFieldValidation;
    engineering_hours_description: IFieldValidation;
    after_hours_description: IFieldValidation;
    integration_technician_description: IFieldValidation;
    project_management_hours_description: IFieldValidation;
  };
}

class SowDocSetting extends Component<
  ISowDocSettingProps,
  ISowDocSettingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  quillRef: React.RefObject<ReactQuill>;
  constructor(props: ISowDocSettingProps) {
    super(props);
    this.state = this.getEmptyState();
    this.quillRef = React.createRef<ReactQuill>();
  }

  getEmptyState = () => ({
    isOpen: true,
    docSetting: {
      fixed_fee_variable: null,
      engineering_hourly_cost: null,
      pm_hourly_cost: null,
      engineering_hourly_rate: null,
      after_hours_rate: null,
      after_hours_cost: null,
      integration_technician_hourly_rate: null,
      integration_technician_hourly_cost: null,
      project_management_hourly_rate: null,
      terms_t_and_m: "",
      project_management_t_and_m: "",
      terms_fixed_fee: "",
      project_management_fixed_fee: "",
      total_no_of_sites_statement: "",
      total_no_of_cutovers_statement: "",
      engineering_hours_description: "",
      after_hours_description: "",
      integration_technician_description: "",
      project_management_hours_description: "",
    },
    error: {
      fixed_fee_variable: { ...SowDocSetting.emptyErrorState },
      engineering_hourly_cost: { ...SowDocSetting.emptyErrorState },
      pm_hourly_cost: { ...SowDocSetting.emptyErrorState },
      engineering_hourly_rate: { ...SowDocSetting.emptyErrorState },
      after_hours_rate: { ...SowDocSetting.emptyErrorState },
      after_hours_cost: { ...SowDocSetting.emptyErrorState },
      integration_technician_hourly_rate: { ...SowDocSetting.emptyErrorState },
      integration_technician_hourly_cost: { ...SowDocSetting.emptyErrorState },
      project_management_hourly_rate: { ...SowDocSetting.emptyErrorState },
      terms_t_and_m: { ...SowDocSetting.emptyErrorState },
      project_management_t_and_m: { ...SowDocSetting.emptyErrorState },
      terms_fixed_fee: { ...SowDocSetting.emptyErrorState },
      project_management_fixed_fee: { ...SowDocSetting.emptyErrorState },
      total_no_of_sites_statement: { ...SowDocSetting.emptyErrorState },
      total_no_of_cutovers_statement: { ...SowDocSetting.emptyErrorState },
      engineering_hours_description: { ...SowDocSetting.emptyErrorState },
      after_hours_description: { ...SowDocSetting.emptyErrorState },
      integration_technician_description: { ...SowDocSetting.emptyErrorState },
      project_management_hours_description: {
        ...SowDocSetting.emptyErrorState,
      },
    },
  });

  componentDidMount() {
    this.props.fetchSOWDOCSetting();
  }

  componentDidUpdate(prevProps: ISowDocSettingProps) {
    if (
      this.props.docSetting &&
      this.props.docSetting !== prevProps.docSetting
    ) {
      this.setState({
        docSetting: {
          ...this.props.docSetting,
        },
      });
    }
  }

  OnSaveDocSetting = () => {
    if (this.isValid()) {
      this.props.editSOWDOCSetting(this.state.docSetting).then((action) => {
        if (action.type === EDIT_DOC_SETTING_SUCCESS) {
          this.props.fetchSOWDOCSetting();
        }
      });
    }
  };

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["#"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = [
          { id: "project_name", value: "{Project Name}" },
          { id: "project_date", value: "{Project Date}" },
        ];
      }

      renderList(values, searchTerm);
    },
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangesTM = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.docSetting
      .change_request_t_and_m_terms as string) = this.getContentFromObject(
      object
    );
    (newState.docSetting
      .change_request_t_and_m_terms_markdown as string) = content;
    this.setState({ ...newState });
  };

  handleChangesFF = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.docSetting
      .change_request_fixed_fee_terms as string) = this.getContentFromObject(
      object
    );
    (newState.docSetting
      .change_request_fixed_fee_terms_markdown as string) = content;
    this.setState({ ...newState });
  };

  handleChangeSow = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.docSetting[event.target.name] = event.target.value;
    this.setState(newState);
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;
    let number_fields = [
      "after_hours_rate",
      "after_hours_cost",
      "integration_technician_hourly_rate",
      "integration_technician_hourly_cost",
      "engineering_hourly_cost",
      "engineering_hourly_rate",
      "fixed_fee_variable",
      "pm_hourly_cost",
      "project_management_hourly_rate",
    ];
    number_fields.forEach((field) => {
      if (Number(this.state.docSetting[field]) <= 0) {
        error[field].errorState = IValidationState.ERROR;
        error[field].errorMessage = "Positive Number Required";

        isValid = false;
      }
    });

    let text_fields = [
      "total_no_of_cutovers_statement",
      "total_no_of_sites_statement",
      "engineering_hours_description",
      "after_hours_description",
      "integration_technician_description",
      "project_management_hours_description",
    ];
    text_fields.forEach((field) => {
      if (
        !this.state.docSetting[field] ||
        this.state.docSetting[field].trim().length === 0
      ) {
        error[field].errorState = IValidationState.ERROR;
        error[field].errorMessage = "Required field";

        isValid = false;
      }
    });

    let sow_fields = [
      "project_management_t_and_m",
      "terms_t_and_m",
      "project_management_fixed_fee",
      "terms_fixed_fee",
    ];

    sow_fields.forEach((field) => {
      if (commonFunctions.isEditorEmpty(this.state.docSetting[field])) {
        error[field].errorState = IValidationState.ERROR;
        error[field].errorMessage = "Required field";

        isValid = false;
      }
    });

    this.setState({
      error,
    });

    return isValid;
  };

  handleChangeSowMarkDown = (html: string, name: string) => {
    const newState = cloneDeep(this.state);
    newState.docSetting[name] = html;
    this.setState(newState);
  };

  rangeVariables = () => {
    const variablesList = [`#{no_of_sites}`, `#{no_of_cutovers}`];
    const globalVariables = variablesList.map((c) => ({
      id: c,
      display: c,
    }));

    return globalVariables;
  };

  render() {
    return (
      <div className="doc-setting-container">
        <div className="header">
          <label className="field__label-label" title="">
            Document Setting
          </label>
        </div>
        <div className="service-catagories">
          <Input
            field={{
              label: "Engineering Hours Default Description",
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.docSetting.engineering_hours_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="engineering_hours_description"
            onChange={this.handleChangeSow}
            error={this.state.error.engineering_hours_description}
          />
          <Input
            field={{
              label: "Engineering Hourly Rate",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.engineering_hourly_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering hourly rate"
            name="engineering_hourly_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.engineering_hourly_rate}
          />
          <Input
            field={{
              label: "Engineering Hourly Cost",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.engineering_hourly_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering hourly cost"
            name="engineering_hourly_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.engineering_hourly_cost}
          />
          <Input
            field={{
              label: "Engineering After Hours Default Description",
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.docSetting.after_hours_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="after_hours_description"
            onChange={this.handleChangeSow}
            error={this.state.error.after_hours_description}
          />
          <Input
            field={{
              label: "Engineering After Hours Rate",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.after_hours_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering after hours rate"
            name="after_hours_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.after_hours_rate}
          />
          <Input
            field={{
              label: "Engineering After Hours Cost",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.after_hours_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering after hours cost"
            name="after_hours_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.after_hours_cost}
          />
          <Input
            field={{
              label: "Integration Technician Default Description",
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.docSetting.integration_technician_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="integration_technician_description"
            onChange={this.handleChangeSow}
            error={this.state.error.integration_technician_description}
          />
          <Input
            field={{
              label: "Integration Technician Hourly Rate",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.integration_technician_hourly_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter Hourly Rate"
            name="integration_technician_hourly_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.integration_technician_hourly_rate}
          />
          <Input
            field={{
              label: "Integration Technician Hourly Cost",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.integration_technician_hourly_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter Hourly Cost"
            name="integration_technician_hourly_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.integration_technician_hourly_cost}
          />
          <Input
            field={{
              label: "Project Management Hours Default Description",
              type: InputFieldType.TEXT,
              isRequired: true,
              value: this.state.docSetting.project_management_hours_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="project_management_hours_description"
            onChange={this.handleChangeSow}
            error={this.state.error.project_management_hours_description}
          />
          <Input
            field={{
              label: "Project Management Hourly Rate",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.project_management_hourly_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter project management hourly rate"
            name="project_management_hourly_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.project_management_hourly_rate}
          />
          <Input
            field={{
              label: "PM Hourly Cost",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.pm_hourly_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter PM hourly cost"
            name="pm_hourly_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.pm_hourly_cost}
          />
          <Input
            field={{
              label: "Fixed Fee Variable",
              type: InputFieldType.NUMBER,
              isRequired: true,
              value: this.state.docSetting.fixed_fee_variable,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter fixed fee variable"
            name="fixed_fee_variable"
            onChange={this.handleChangeSow}
            error={this.state.error.fixed_fee_variable}
          />
          <Input
            field={{
              label: "Total no. of cutovers statement",
              type: InputFieldType.CUSTOM,
              value: "",
              isRequired: true,
            }}
            width={4}
            labelIcon={""}
            name="total_no_of_cutovers_statement"
            onChange={(e) => null}
            customInput={
              <MentionsInput
                markup="[__display__]"
                value={this.state.docSetting.total_no_of_cutovers_statement}
                onChange={(event) => {
                  const newState = cloneDeep(this.state);
                  newState.docSetting.total_no_of_cutovers_statement =
                    event.target.value;
                  this.setState(newState);
                }}
                className={"outer"}
              >
                <Mention
                  trigger="#"
                  data={this.rangeVariables()}
                  className={"inner-drop"}
                  markup="#__display__"
                />
              </MentionsInput>
            }
            placeholder={`Status`}
            error={this.state.error.total_no_of_cutovers_statement}
            disabled={false}
          />

          <Input
            field={{
              label: "Total no. of sites statement",
              type: InputFieldType.CUSTOM,
              value: "",
              isRequired: true,
            }}
            width={4}
            labelIcon={""}
            name="to"
            onChange={(e) => null}
            customInput={
              <MentionsInput
                markup="[__display__]"
                value={this.state.docSetting.total_no_of_sites_statement}
                onChange={(event) => {
                  const newState = cloneDeep(this.state);
                  newState.docSetting.total_no_of_sites_statement =
                    event.target.value;
                  this.setState(newState);
                }}
                className={"outer"}
              >
                <Mention
                  trigger="#"
                  data={this.rangeVariables()}
                  className={"inner-drop"}
                  markup="#__display__"
                />
              </MentionsInput>
            }
            placeholder={`Status`}
            error={this.state.error.total_no_of_sites_statement}
            disabled={false}
          />
          <div className="col-md-12 sow-settings-editor">
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "project_management_t_and_m"
                  )
                }
                label={"Project Management T & M"}
                value={this.state.docSetting.project_management_t_and_m}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.project_management_t_and_m}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(value, "terms_t_and_m")
                }
                label={"Terms & Conditions T & M"}
                value={this.state.docSetting.terms_t_and_m}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.terms_t_and_m}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "project_management_fixed_fee"
                  )
                }
                label={"Project Management Fixed Fee"}
                value={this.state.docSetting.project_management_fixed_fee}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.project_management_fixed_fee}
              />
            </div>
            <div className="field-section  mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(value, "terms_fixed_fee")
                }
                label={"Terms & Conditions Fixed Fee"}
                value={this.state.docSetting.terms_fixed_fee}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.terms_fixed_fee}
              />
            </div>
            <div className="field-section  mde-editor">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Change request T & M terms
                </label>
              </div>
              <ReactQuill
                ref={this.quillRef}
                value={
                  this.state.docSetting.change_request_t_and_m_terms_markdown ||
                  ""
                }
                onChange={this.handleChangesTM}
                modules={{ mention: this.mentionModule }}
                className={"react-quill-main"}
              />
            </div>
            <div className="field-section  mde-editor">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Change request Fixed Fee terms
                </label>
              </div>
              <ReactQuill
                ref={this.quillRef}
                value={
                  this.state.docSetting
                    .change_request_fixed_fee_terms_markdown || ""
                }
                onChange={this.handleChangesFF}
                modules={{ mention: this.mentionModule }}
                className={"react-quill-main"}
              />
            </div>
          </div>
        </div>
        <SquareButton
          onClick={() => this.OnSaveDocSetting()}
          content="Save"
          bsStyle={ButtonStyle.PRIMARY}
          className="save-button-doc-setting"
          disabled={
            this.props.isFetchingDocSet ||
            this.state.docSetting === this.props.docSetting
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  docSetting: state.setting.docSetting,
  isFetchingDocSet: state.setting.isFetchingDocSet,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
  editSOWDOCSetting: (data: IDOCSetting) => dispatch(editSOWDOCSetting(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SowDocSetting);
