import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  vendorSettingsCRU,
  fetchCustomerTypes,
  fetchCustomerStatuses,
  vendorAttachmentsEdit,
  VENDOR_ATTACHMENTS_SUCCESS,
  GET_VENDOR_SETTINGS_SUCCESS,
  GET_VENDOR_SETTINGS_FAILURE,
} from "../../actions/setting";
import { fetchTerritories } from "../../actions/provider/integration";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import AppValidators from "../../utils/validator";
import { saveFileFromURL } from "../../utils/download";
import { commonFunctions } from "../../utils/commonFunctions";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import NewUploadFile from "../ServiceRequest/newUploadFile";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";

interface VendorSettingsProps {
  territories: any;
  vendorStatuses: IPickListOptions[];
  customerTypes: IPickListOptions[];
  isFetchingTerritories: boolean;
  isFetchingCustomerTypes: boolean;
  isFetchingVendorStatuses: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchTerritories: () => Promise<void>;
  fetchCustomerTypes: () => Promise<void>;
  fetchCustomerStatuses: () => Promise<void>;
  vendorAttachmentsEdit: (
    request: "get" | "post" | "delete",
    data?: FormData,
    id?: number
  ) => Promise<any>;
  vendorSettingsCRU: (
    request: "get" | "post" | "put",
    data?: IVendorSettings
  ) => Promise<any>;
}

interface VendorAttachment {
  id: number;
  name: string;
  attachment: string;
}

const VendorSettings: React.FC<VendorSettingsProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [uploading, setUploading] = useState<boolean>(false);
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [attachments, setAttachments] = useState<VendorAttachment[]>([]);
  const [showUploadBox, setShowUploadBox] = useState<boolean>(false);
  const [vendorSettings, setVendorSettings] = useState<IVendorSettingsState>({
    territories: [],
    customer_type: null,
    vendor_onboarding_email_alias: "",
    vendor_onboarding_email_subject: "",
    vendor_onboarding_email_body: "",
    vendor_list_statuses: [],
    vendor_create_status: null,
  });

  const territoryOptions: IPickListOptions[] = useMemo(
    () =>
      props.territories
        ? props.territories.map((territory) => ({
            value: territory.id,
            label: territory.name,
          }))
        : [],
    [props.territories]
  );

  const errorsFound: boolean = useMemo(
    () =>
      !Boolean(
        vendorSettings.vendor_onboarding_email_alias &&
          !commonFunctions.isEditorEmpty(
            vendorSettings.vendor_onboarding_email_body
          ) &&
          vendorSettings.vendor_onboarding_email_subject &&
          AppValidators.isValidEmail(
            vendorSettings.vendor_onboarding_email_alias
          ) &&
          vendorSettings.territories.length &&
          vendorSettings.vendor_list_statuses.length &&
          vendorSettings.vendor_create_status &&
          vendorSettings.customer_type
      ),
    [vendorSettings]
  );

  useEffect(() => {
    fetchVendorSettings();
    props.fetchTerritories();
    props.fetchCustomerTypes();
    props.fetchCustomerStatuses();
  }, []);

  const getVendorAttachments = () => {
    setLoading(true);
    props
      .vendorAttachmentsEdit("get")
      .then((action) => {
        if (action.type === VENDOR_ATTACHMENTS_SUCCESS) {
          let attachments: VendorAttachment[] = action.response.map((el) => ({
            id: el.id,
            name: el.name,
            attachment: el.attachment,
          }));
          setAttachments(attachments);
        }
      })
      .finally(() => setLoading(false));
  };

  const deleteAttachment = (id: number) => {
    props.vendorAttachmentsEdit("delete", undefined, id).then((action) => {
      if (action.type === VENDOR_ATTACHMENTS_SUCCESS) {
        props.addSuccessMessage("File deleted successfully!");
        getVendorAttachments();
      }
    });
  };

  const fetchVendorSettings = () => {
    props
      .vendorSettingsCRU("get")
      .then((action) => {
        if (action.type === GET_VENDOR_SETTINGS_SUCCESS) {
          let data: IVendorSettings = action.response;
          let territories = data.territories.map((el) => el.territory_crm_id);
          let customer_type = data.customer_type.customer_type_crm_id;
          let vendor_list_statuses = data.vendor_list_statuses.map(
            (el) => el.customer_status_crm_id
          );
          let vendor_create_status =
            data.vendor_create_status.customer_status_crm_id;
          setVendorSettings({
            ...data,
            territories,
            customer_type,
            vendor_list_statuses,
            vendor_create_status,
          });
          getVendorAttachments();
        } else if (action.type === GET_VENDOR_SETTINGS_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const onSubmit = () => {
    let territoryIds = new Set(vendorSettings.territories);
    let statusIds = new Set(vendorSettings.vendor_list_statuses);
    let territories: {
      territory_crm_id: number;
      territory_name: string;
    }[] = [];
    let vendor_list_statuses: {
      customer_status_crm_id: number;
      customer_status_name: string;
    }[] = [];
    let vendor_create_status: {
      customer_status_crm_id: number;
      customer_status_name: string;
    };
    territoryOptions.forEach((el) => {
      if (territoryIds.has(el.value)) {
        territories.push({
          territory_crm_id: el.value as number,
          territory_name: el.label as string,
        });
      }
    });
    props.vendorStatuses.forEach((el) => {
      if (statusIds.has(el.value)) {
        vendor_list_statuses.push({
          customer_status_crm_id: el.value as number,
          customer_status_name: el.label as string,
        });
      }
      if (el.value === vendorSettings.vendor_create_status) {
        vendor_create_status = {
          customer_status_crm_id: el.value as number,
          customer_status_name: el.label as string,
        };
      }
    });
    let customerTypeOption = props.customerTypes.find(
      (el) => el.value === vendorSettings.customer_type
    );
    let customer_type = {
      customer_type_crm_id: customerTypeOption.value as number,
      customer_type_name: customerTypeOption.label as string,
    };

    let payload: IVendorSettings = {
      vendor_onboarding_email_alias:
        vendorSettings.vendor_onboarding_email_alias,
      vendor_onboarding_email_subject:
        vendorSettings.vendor_onboarding_email_subject,
      vendor_onboarding_email_body: vendorSettings.vendor_onboarding_email_body,
      territories,
      customer_type,
      vendor_list_statuses,
      vendor_create_status,
    };
    setLoading(true);
    props
      .vendorSettingsCRU(firstSave ? "post" : "put", payload)
      .then((action) => {
        if (action.type === GET_VENDOR_SETTINGS_SUCCESS) {
          props.addSuccessMessage(
            "Vendor Settings " +
              (firstSave ? "Saved" : "Updated") +
              " Successfully!"
          );
          setFirstSave(false);
        } else if (action.type === GET_VENDOR_SETTINGS_FAILURE) {
          props.addErrorMessage("Error saving vendor settings!");
        }
      })
      .finally(() => setLoading(false));
  };

  const toggleUploadBox = (open: boolean) => {
    setShowUploadBox(open);
  };

  const handleFileData = (uploadFile: FormData) => {
    setUploading(true);
    props
      .vendorAttachmentsEdit("post", uploadFile)
      .then((action) => {
        if (action.type === VENDOR_ATTACHMENTS_SUCCESS) {
          getVendorAttachments();
          props.addSuccessMessage("File uploaded successfully!");
        }
      })
      .finally(() => {
        setUploading(false);
        setShowUploadBox(false);
      });
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setVendorSettings((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleChangeEmailBody = (html: string) => {
    setVendorSettings((prevState) => ({
      ...prevState,
      vendor_onboarding_email_body: html,
    }));
  };

  return (
    <div className="vendor-settings-container">
      <Spinner show={loading} className="vendor-settings-spinner" />
      <SquareButton
        content={
          <div onClick={() => toggleUploadBox(true)}>
            <img
              className="attachments__icon__upload"
              src="/assets/icons/cloud.png"
            />
            Upload Attachment
          </div>
        }
        bsStyle={ButtonStyle.PRIMARY}
        onClick={() => toggleUploadBox(true)}
        className="attachment-btn"
      />
      <div className="col-md-12 row">
        <Input
          field={{
            label: "Territory Setting",
            type: InputFieldType.PICKLIST,
            value: vendorSettings.territories,
            options: territoryOptions,
            isRequired: true,
          }}
          width={6}
          multi={true}
          labelIcon="info"
          labelTitle="The vendor will be created within one of the territories."
          name="territories"
          onChange={handleChange}
          loading={props.isFetchingTerritories}
          placeholder={`Select Territory`}
        />
        <Input
          field={{
            label: "Company Type",
            type: InputFieldType.PICKLIST,
            value: vendorSettings.customer_type,
            options: props.customerTypes,
            isRequired: true,
          }}
          width={6}
          labelIcon="info"
          labelTitle="The vendor created will belong to this type."
          name="customer_type"
          onChange={handleChange}
          placeholder={`Select Company Type`}
          loading={props.isFetchingCustomerTypes}
        />
      </div>
      <div className="col-md-12 row">
        <Input
          field={{
            label: "Vendor List Statuses",
            type: InputFieldType.PICKLIST,
            value: vendorSettings.vendor_list_statuses,
            options: props.vendorStatuses,
            isRequired: true,
          }}
          width={6}
          multi={true}
          labelIcon="info"
          labelTitle="These statuses will be used to filter vendors when listing."
          name="vendor_list_statuses"
          onChange={handleChange}
          loading={props.isFetchingVendorStatuses}
          placeholder={`Select Vendor Status(es)`}
        />
        <Input
          field={{
            label: "Vendor Create Status",
            type: InputFieldType.PICKLIST,
            value: vendorSettings.vendor_create_status,
            options: props.vendorStatuses,
            isRequired: true,
          }}
          width={6}
          labelIcon="info"
          labelTitle="The new vendor created will have this status."
          name="vendor_create_status"
          onChange={handleChange}
          placeholder={`Select Vendor Status`}
          loading={props.isFetchingVendorStatuses}
        />
      </div>
      {!firstSave && (
        <div className="col-md-12 attachment-section">
          <label htmlFor="Attachments" title="Attachment">
            Attachments
          </label>
          {Boolean(attachments && attachments.length) ? (
            <div className="view-attachments">
              {attachments.map((el, key) => (
                <div className="attachment-box" key={key}>
                  <span
                    className="attachment-link"
                    onClick={() => saveFileFromURL(el.attachment)}
                  >
                    {el.name}
                  </span>
                  <SmallConfirmationBox
                    text="attachment"
                    onClickOk={() => deleteAttachment(el.id)}
                  />
                </div>
              ))}
            </div>
          ) : (
            <label style={{ color: "#333" }}>No attachments</label>
          )}
        </div>
      )}
      <div className="col-md-12 row">
        <Input
          field={{
            label: "Vendor Onboarding Email Alias",
            type: InputFieldType.TEXT,
            value: vendorSettings.vendor_onboarding_email_alias,
            isRequired: true,
          }}
          width={6}
          labelIcon="info"
          labelTitle="The emails will be sent on behalf of this ID."
          name="vendor_onboarding_email_alias"
          onChange={handleChange}
          placeholder={`Enter Email Alias`}
        />
      </div>
      <div className="col-md-12 row">
        <Input
          field={{
            label: "Onboarding Email Subject",
            type: InputFieldType.TEXT,
            value: vendorSettings.vendor_onboarding_email_subject,
            isRequired: true,
          }}
          width={6}
          name="vendor_onboarding_email_subject"
          onChange={handleChange}
          placeholder={`Enter Email Subject`}
        />
      </div>
      <div className="col-md-10">
        <QuillEditorAcela
          onChange={handleChangeEmailBody}
          label={"Vendor Onboarding Email Body "}
          value={vendorSettings.vendor_onboarding_email_body}
          scrollingContainer=".app-body"
          wrapperClass={"vendor-settings-quill"}
          isRequired={true}
          hideTable={true}
          customToolbar={[
            ["bold", "italic", "underline", "strike"], // toggled buttons
            [{ color: [] }, { background: [] }],
            [{ list: "ordered" }, { list: "bullet" }],
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
          ]}
        />
      </div>
      <SquareButton
        content={firstSave ? "Save" : "Update"}
        bsStyle={ButtonStyle.PRIMARY}
        onClick={onSubmit}
        disabled={errorsFound}
        className="save-vendor-settings"
      />
      <NewUploadFile
        isVisible={showUploadBox}
        close={() => toggleUploadBox(false)}
        onSubmit={handleFileData}
        loading={uploading}
        titleField="name"
        fileField="attachment"
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  territories: state.integration.terretories,
  customerTypes: state.setting.customerTypes,
  vendorStatuses: state.setting.customerStatuses,
  isFetchingCustomerTypes: state.setting.isFetchingCustomerTypes,
  isFetchingVendorStatuses: state.setting.isFetchingCustomerStatuses,
  isFetchingTerritories: state.integration.isFetchingTerretories,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTerritories: () => dispatch(fetchTerritories()),
  fetchCustomerTypes: () => dispatch(fetchCustomerTypes()),
  fetchCustomerStatuses: () => dispatch(fetchCustomerStatuses()),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  vendorSettingsCRU: (
    request: "get" | "put" | "post",
    data?: IVendorSettings
  ) => dispatch(vendorSettingsCRU(request, data)),
  vendorAttachmentsEdit: (
    request: "get" | "post" | "delete",
    data?: FormData,
    id?: number
  ) => dispatch(vendorAttachmentsEdit(request, data, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VendorSettings);
