import React from 'react';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';

import './style.scss';

interface IAddNoteFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: any;
  note: string;
  handleChange: (e: any) => void;
  title: string;
  isLoading: boolean;
}

interface IAddNoteFormState {
  error: IFieldValidation;
}

export default class AddNoteForm extends React.Component<
  IAddNoteFormProps,
  IAddNoteFormState
> {
  constructor(props: IAddNoteFormProps) {
    super(props);

    this.state = {
      error: {
        errorMessage: '',
        errorState: null,
      },
    };
  }

  onClose = e => {
    this.props.onClose(e);
    this.setState({
      error: {
        errorMessage: '',
        errorState: null,
      },
    });
  };

  isValid = () => {
    const error = this.state.error;
    let isValid = true;
    if (!this.props.note) {
      error.errorState = IValidationState.ERROR;
      error.errorMessage = 'Note can not be empty';

      isValid = false;
    } else if (this.props.note && this.props.note.length > 100) {
      error.errorState = IValidationState.ERROR;
      error.errorMessage = 'Note should be less than 100 chars.';

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };
  onSubmit = e => {
    if (this.isValid()) {
      this.props.onSubmit();
    }
  };

  getBody = () => {
    return (
      <div className="note-modal">
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`add-note__body" ${this.props.isLoading ? `loading` : ''}`}
        >
          <Input
            field={{
              label: 'Note',
              type: InputFieldType.TEXTAREA,
              value: this.props.note,
              isRequired: true,
            }}
            width={12}
            placeholder="Enter Note"
            name="note"
            error={this.state.error}
            onChange={e => this.props.handleChange(e)}
          />
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-note__footer
        ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Add"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.props.title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-note"
      />
    );
  }
}
