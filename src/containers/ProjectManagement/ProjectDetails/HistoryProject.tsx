import { debounce, isEmpty } from 'lodash';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { connect } from 'react-redux';
import { pmoCommonAPI } from '../../../actions/pmo';
import SquareButton from '../../../components/Button/button';
import PMOCollapsible from '../../../components/PMOCollapsible';
import Spinner from '../../../components/Spinner';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';
import { showValue } from '../../ProjectSetting/HistorySetting';
import './historyStyle.scss';
import './style.scss';

interface IProjectProps extends ICommonProps {
  pmoCommonAPI: any;
  user: any;
}

interface IProjectState {
  list: any;
  projects: any[];
  pagination: IScrollPaginationFilters;
  inputValue: string;
  noData: boolean;
  groupBy: string;
  showGroupBy: boolean;
  loading: boolean;
  details: any;
}

class HistoryPMO extends React.Component<
  any,
  IProjectState
> {
  constructor(props: IProjectProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 2000);
  }

  getEmptyState = () => ({
    list: [],
    projects: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: '',
      search: '',
      type: '',
      customer: '',
      manager: '',
      closed_end_date: "",
      closed_start_date: "",
      action_end_date: '',
      action_start_date: '',
    },
    inputValue: '',
    noData: false,
    showSearch: false,
    showGroupBy: false,
    loading: true,
    groupBy: '',
    showFiltersPop: false,
    details: null,
  });

  componentDidMount() {
    this.fetchMoreData(true)
  }

  updateMessage = search => {
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        search
      },
    }));
    this.fetchMoreData(true)
  };

  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
        type: this.state.pagination.type,
      };
    }
    if (prevParams.nextPage !== null) {

      const id = this.props.match.params.id;

      this.props.pmoCommonAPI(
        `projects/${id}/history`,
        'get', '', prevParams)
        .then(action => {
          if (action.response) {
            let projects = [];
            if (clearData) {
              projects = [...action.response.results];
            } else {
              projects = [...this.state.projects, ...action.response.results];
            }
            const newPagination = {
              currentPage: action.response.links.page_number,
              nextPage: action.response.links.next_page_number,
              page_size: this.state.pagination.page_size,
              ordering: this.state.pagination.ordering,
              search: this.state.pagination.search,
              type: this.state.pagination.type,
            };

            this.setState({
              pagination: newPagination,
              projects,
              noData: projects.length === 0 ? true : false,
              loading: false,
            })
          } else {
            this.setState({
              noData: true,
              loading: false,
            });
          }
        })
    }
  };

  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };

  convertHtmlToText = (str: string) => {
    var tempDivElement = document.createElement("div");
    tempDivElement.innerHTML = str;
    return tempDivElement.textContent || tempDivElement.innerText || "";
  };
  
  listingRows = (project, index) => {
    return (
      <div
        className={`history-card`} key={index}>
        <div className="field updated-fields" >
          <div className="tags">
            <div className={`action-tag ${project.action}`}>{project.action}</div>
            <div className={`name-tag ${project.model_display_name}`}>{project.model_display_name}</div>
            <div className="actor" > {project.actor} </div>
            <div className="date" >{fromISOStringToFormattedDate(project.timestamp, 'MMM DD, YYYY hh:mm a')}</div>
          </div>


          {
            project && project.changes_display_dict &&
            Object.entries(project.changes_display_dict).map(([key, value]) => {
              if (key.includes('markdown')) {
                return '';
              }
              return (
                <div className="data-row" key={key}>
                  <div className="key"> {key} </div>
                  <div className="old">
                    {this.convertHtmlToText(showValue(value[0], key))}{" "}
                  </div>
                  <div className="new">
                    {this.convertHtmlToText(showValue(value[1], key))}{" "}
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    )
  }

  detailView =()=>{
    return (
      <div className="field updated-fields" >
      {
        this.state.details &&
        Object.entries( this.state.details).map(([key, value]) => {
          let value0 = value[0];
          let value1 = value[1];
          return (
            <div className=" data-row" key={key}>
              <div className="key">  {key} </div>
              <div className="old">  {value0} </div>
              <div className="new">  {value1} </div>
            </div>
          )
        })
      }
    </div>
    )
  }

  render() {
    const groupedByInvalid = this.groupBy(this.state.projects, this.state.groupBy);

    return (
      <div className="project-history">
        <div className="project-details meeting-details create-change-request">
          {this.state.loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          )}
          <div className="header">
            <div className="left">
              <div className="meeting-name">
               Project  History
              </div>
            </div>
            <div className="right">

              <SquareButton
                content="Close"
                bsStyle={ButtonStyle.DEFAULT}
                onClick={() => {
                  const query = new URLSearchParams(this.props.location.search);
                  const AllProject = query.get('AllProject') === 'true' ? true : false;
                  this.props.history.push(`/ProjectManagement/${this.props.match.params.id}?AllProject=${AllProject}`)
                }}
              /></div>
          </div>
        </div>
        <div className="list">
        <div className={`header columns`}>
            <div className={`field data-row  updated-fields`}>
              <div className="keys">Field Name</div>
              <div className="keys">Old Value</div>
              <div className="keys">New Value</div>
            </div>
          </div>
          <div id="scrollableDiv" style={{ height: '72vh', overflow: "auto" }}>

            <InfiniteScroll
              dataLength={this.state.projects.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
            >
              {!this.state.groupBy && this.state.projects.map((project, index) => (
                this.listingRows(project, index)
              ))}
              {this.state.groupBy &&
                groupedByInvalid &&
                !isEmpty(groupedByInvalid) &&
                Object.keys(groupedByInvalid).map((data, i) => (
                  <div key={i} className="voilation-statistics col-md-12">
                    <PMOCollapsible label={groupedByInvalid[data][0][this.state.groupBy] || 'New Projects'} isOpen={true}>
                      {groupedByInvalid[data].map((project, index) =>
                        this.listingRows(project, index)
                      )}
                    </PMOCollapsible>
                  </div>
                ))}
              {!this.state.loading && this.state.pagination.nextPage && this.state.projects.length > 0 && (
                <div className="load-more" onClick={() => {
                  this.fetchMoreData(false)
                }}>
                  load more data...
                </div>
              )}
            </InfiniteScroll>
            {
              this.state.noData && this.state.projects.length === 0 &&
              <div className="no-data">No History Available</div>
            }
            {
              this.state.loading &&
              <div className="loader">
                <Spinner show={true} />
              </div>
            }
          </div>
          {
            this.detailView()
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  pmoCommonAPI: (api: string, type: string, data: any, params?: IScrollPaginationFilters) => dispatch(pmoCommonAPI(api, type, data, params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HistoryPMO);

