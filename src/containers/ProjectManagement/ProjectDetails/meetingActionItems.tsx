import { cloneDeep } from 'lodash';
import _cloneDeep from 'lodash/cloneDeep';
import React, { Component } from 'react';
import Input from '../../../components/Input/input';
import SmallConfirmationBox from '../../../components/SmallConfirmationBox/confirmation';
import './style.scss';



interface IIMeetingActionItemState {
  validationTrigger: boolean;
  agenda_topics: string[];
}
interface IIProjectSettingProps {
  setAgenda: (agenda_topics: any) => void;
  initialData?: string[];
}

export default class MeetingActionItem extends Component<
IIProjectSettingProps,
  IIMeetingActionItemState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    agenda_topics: [],
    validationTrigger: false,
  });
  componentDidMount() {
    if (this.props.initialData) {
      const newState = _cloneDeep(this.state);
      (newState.agenda_topics as any) = this.props.initialData;
      this.setState(newState);
    }
  }
  handleChange = (event: any, topicIndex) => {
    const newState = _cloneDeep(this.state);
    newState.agenda_topics[topicIndex] = event.target.value;
    this.props.setAgenda(newState.agenda_topics);
    this.setState(newState);
  };

  deleteMeetingActionItem = (index) => {
    const newState = cloneDeep(this.state);
    newState.agenda_topics.splice(index, 1);
    this.props.setAgenda(newState.agenda_topics);
    this.setState(newState);
  };


  render() {

    return (
      <div className="agenda-topic-component">
        {this.state.agenda_topics.map((topic, topicIndex) => {
          return (
            <div className="col-md-12 status-row" key={topicIndex}>
              <Input
                field={{
                  label: '',
                  type: InputFieldType.TEXT,
                  value: topic,
                  isRequired: false,
                }}
                width={9}
                multi={true}
                error={!topic && this.state.validationTrigger && {
                  errorState: IValidationState.ERROR,
                  errorMessage: 'Required field.',
                }}
                name="kickoffMeetings"
                onChange={e => this.handleChange(e, topicIndex)}
                placeholder={`Enter Topic `}
              />
              <SmallConfirmationBox
                className='remove-action-item'
                onClickOk={() => this.deleteMeetingActionItem(topicIndex)}
                text={'Agenda topic'}
              />
            </div>
          )
        })}
        {
          this.state.agenda_topics.length === 0 &&
          <div className="col-md-12 status-row risk-edited" >

            <div
              className="no-data-action-item col-md-9"
            > No items available</div>
          </div>
        }
        {
          <div className="col-md-12 status-row risk-edited" >

            <div
              onClick={e => {
                const newState = cloneDeep(this.state);
                newState.agenda_topics.push('')
                this.setState(newState);
              }} className="add-new-action-item col-md-9"
            >Add  Action Items</div>
          </div>
        }
      </div>
    );
  }
}