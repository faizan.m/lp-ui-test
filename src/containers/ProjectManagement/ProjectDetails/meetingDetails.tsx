import { cloneDeep } from "lodash";
import moment, { Moment } from "moment";
import React from "react";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../../actions/documentation";
import {
  deleteExternalMeetingAttendeesByProjectID,
  deleteMeetingAttendeesByProjectID,
  EXTERNAL_MEETING_ATTENDEES_SUCCESS,
  FETCH_MEETING_PDF_PREVIEW_SUCCESS,
  getActionItemList,
  getActivityStatusList,
  getContactRoles,
  getExternalMeetingAttendeesByProjectID,
  getMeetingAttendeesByProjectID,
  getMeetingNotesByProjectID,
  getMeetingPDFPreview,
  getMeetingTemplates,
  getProjectAdditionalContactsByID,
  getProjectCustomerContactsByID,
  getProjectDetails,
  getProjectEngineersByID,
  getProjectPhasesByID,
  getProjectTickets,
  getProjectTypes,
  meetingComplete,
  MEETING_ATTENDEES_SUCCESS,
  MEETING_COMPLETE_SUCCESS,
  MEETING_SUCCESS,
  postStatusMeeting,
  PROJECT_ADDITIONAL_CONTACTS_FAILURE,
  PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
  PROJECT_DETAILS_SUCCESS,
  PROJECT_ENGINEERS_SUCCESS,
  PROJECT_MEETING_NOTES_SUCCESS,
  PROJECT_PHASES_SUCCESS,
  PROJECT_TYPES_SUCCESS,
  saveExternalMeetingAttendeesByProjectID,
  saveMeetingAttendeesByProjectID,
  saveMeetingNotesByProjectID,
  saveProjectAdditionalContactsByID,
  saveProjectCustomerContactsByID,
  TEMPLATES_SUCCESS,
  updateAdditionalContact,
  updateMeetingNotesByProjectID,
  updateProjectPhasesStatusByID,
} from "../../../actions/pmo";
import Accordian from "../../../components/Accordian";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import PMOCollapsible from "../../../components/PMOCollapsible";
import Spinner from "../../../components/Spinner";
import UsershortInfo from "../../../components/UserImage";
import { commonFunctions } from "../../../utils/commonFunctions";
import { getConvertedColorWithOpacity } from "../../../utils/CommonUtils";
import AppValidators from "../../../utils/validator";
import CustomerTouchMeeting from "../../ProjectSetting/CustomerTouchMeeting";
import ActionItems from "./actionItems";
import CustomerUserNew from "./addCustomerUser";
import AgendaTopic from "./agendaTopics";
import MeetingDocuments from "./meetingDocuments";
import MeetingNotes from "./meetingNotes";
import ProjectAcceptance from "./projectAcceptance";
import RiskItems from "./riskItems";
import "./style.scss";

interface IProjectMeetingProps extends ICommonProps {
  getProjectTypes: () => Promise<any>;
  getContactRoles: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getActivityStatusList: () => Promise<any>;
  getMeetingTemplates: (url: string) => Promise<any>;
  fetchAllCustomerUsers: (id: string) => Promise<any>;
  getProjectTickets: (projectId: number) => Promise<any>;
  getProjectDetails: (projectId: number) => Promise<any>;
  getProjectPhasesByID: (projectId: number) => Promise<any>;
  getProjectCustomerContactsByID: (id: number) => Promise<any>;
  getActionItemList: (projectId: number, api: string) => Promise<any>;
  getProjectAdditionalContactsByID: (projectId: number) => Promise<any>;
  getProjectEngineersByID: (projectID: number, type?: string) => Promise<any>;
  meetingComplete: (projectID: number, data: any, api: string) => Promise<any>;
  postStatusMeeting: (
    projectID: number,
    data: any,
    api: string
  ) => Promise<any>;
  getMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number
  ) => Promise<any>;
  saveMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    note: string
  ) => Promise<any>;
  updateMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    noteId: number,
    note: string
  ) => Promise<any>;
  getMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number
  ) => Promise<any>;
  deleteMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) => Promise<any>;
  saveMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) => Promise<any>;
  saveProjectCustomerContactsByID: (
    projectID: number,
    data: any
  ) => Promise<any>;
  getMeetingPDFPreview: (
    projectId: number,
    meetingId: number,
    completed: boolean,
    sent_to_cc: boolean
  ) => Promise<any>;
  updateProjectPhasesStatusByID: (
    data: any,
    projectId: number,
    phaseId: number
  ) => Promise<any>;
  getExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number
  ) => Promise<any>;
  saveExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) => Promise<any>;
  deleteExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) => Promise<any>;
  saveProjectAdditionalContactsByID: (
    projectId: number,
    data: any
  ) => Promise<any>;
  updateAdditionalContact: (
    projectId: number,
    contactId: number,
    data: any
  ) => Promise<any>;
}

interface TimeEntry {
  start_time: Moment;
  end_time: Moment;
  notes: string;
  user_id: string;
  ticket_id: string;
}
interface IProjectMeetingState {
  showAddContactForm: boolean;
  showContactAddErrorMsg: string;
  selectedCustomerName: string;
  selectedCustomerRole: string;
  fetchingCustomerUsers: boolean;
  showCreateUserModal: boolean;
  customerContactRolesList: any;
  project?: any;
  loading: boolean;
  isOpen: boolean;
  currentProjectTypeId: number;
  projectTypes: any;
  ticketList: any;
  type: string;
  view: boolean;
  template: any;
  projectMeeting: IProjectMeeting;
  closeMeeting: boolean;
  send_to_cc: boolean;
  closeTicket: {
    engineer_ids: any[];
    engineer_ticket_id: string;
    ticket_id: number;
    pm_time_entry: TimeEntry;
    engineer_time_entries: TimeEntry[];
  };
  engineeringTimeEntry: TimeEntry;
  ticketError: object[];
  meetingNote: { id?: number; note: string };
  noteData: string;
  projectCustomerContacts: any;
  customerUsers: any;
  meetingAttendees: any;
  selectedMeetingAttendees: any;
  default_project_acceptance_markdown: any;
  default_project_acceptance_text: any;
  projectEngineers: any;
  isFetchingMeetingDetails: boolean;
  accountManager: any;
  showEditDropdown: boolean;
  isFetchingCustomerContacts: boolean;
  openPreview: boolean;
  previewHTML: { url: string };
  preSalesEngineers: any;
  pdfLoading: boolean;
  phases: any[];
  phase_to_complete: number;
  statusList: any[];
  estimated_completion_date: Date;
  showAdditionalContacts: boolean;
  showAdditionalContactAddErrorMsg: string;
  showAdditionalContactActions: boolean;
  editAdditionalContactId: number;
  showAdditionalContactForm: boolean;
  name: string;
  email: string;
  role: string;
  phone: string;
  error: {
    name: IFieldValidation;
    email: IFieldValidation;
    role: IFieldValidation;
    phone: IFieldValidation;
  };
  externalMeetingAttendees: any[];
  projectAdditionalContacts: any[];
  showAdditionalContactId: boolean;
  saved: boolean;
  currentStatusList: any[];
  onCloseMeetRedirect: boolean;
  saveClicked: boolean;
}

const project = {
  name: "",
  pm: "",
  type: "",
  actual: 0,
  budget: 0,
  close_date: "",
  action_date: "",
  status: "",
};

class ProjectMeeting extends React.Component<
  IProjectMeetingProps,
  IProjectMeetingState
> {
  constructor(props: IProjectMeetingProps) {
    super(props);
    this.state = this.getEmptyState();
  }
  deleteConfirmBox = React.createRef<HTMLElement>();

  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  static validator = new AppValidators();
  getEmptyState = () => ({
    project,
    loading: false,
    isOpen: true,
    currentProjectTypeId: undefined,
    projectTypes: [],
    ticketList: [],
    type: "",
    template: "",
    projectMeeting: {
      name: "",
      schedule_start_datetime: moment(),
      schedule_end_datetime: moment().add(1, "hours"),
      status: "Pending",
      meeting_type: "",
      email_body: "",
      email_subject: "",
      email_body_text: "",
      email_body_replaced_text: "",
      project_acceptance_markdown: "",
      project_acceptance_text: "",
      project_acceptance_replaced_text: "",
    },
    closeTicket: {
      ticket_id: null,
      engineer_ids: [],
      engineer_ticket_id: "",
      pm_time_entry: {
        start_time: moment(),
        end_time: moment().add(1, "hours"),
        notes: "",
        ticket_id: "",
        user_id: "",
      },
      engineer_time_entries: [],
    },
    engineeringTimeEntry: {
      start_time: moment(),
      end_time: moment().add(1, "hours"),
      notes: "",
      ticket_id: "",
      user_id: "",
    },
    ticketError: [],
    currentStatusList: [],
    closeMeeting: false,
    view: false,
    meetingNote: {
      note: "",
    },
    noteData: "",
    projectCustomerContacts: [],
    customerUsers: [],
    meetingAttendees: [],
    selectedMeetingAttendees: [],
    default_project_acceptance_markdown: "",
    default_project_acceptance_text: "",
    projectEngineers: [],
    isFetchingMeetingDetails: false,
    accountManager: {
      id: "",
      member_id: 0,
      first_name: "",
      last_name: "",
      profile_url: "",
    },
    showEditDropdown: false,
    fetchingCustomerUsers: false,
    isFetchingCustomerContacts: false,
    showCreateUserModal: false,
    showAddContactForm: false,
    showContactAddErrorMsg: "",
    selectedCustomerRole: undefined,
    selectedCustomerName: undefined,
    customerContactRolesList: [],
    openPreview: false,
    previewHTML: null,
    preSalesEngineers: [],
    pdfLoading: false,
    phases: [],
    phase_to_complete: null,
    statusList: [],
    estimated_completion_date: new Date(),
    name: undefined,
    email: undefined,
    role: undefined,
    phone: undefined,
    error: {
      name: { ...ProjectMeeting.emptyErrorState },
      email: { ...ProjectMeeting.emptyErrorState },
      role: { ...ProjectMeeting.emptyErrorState },
      phone: { ...ProjectMeeting.emptyErrorState },
    },
    showAdditionalContactForm: false,
    externalMeetingAttendees: [],
    projectAdditionalContacts: [],
    showAdditionalContacts: false,
    showAdditionalContactAddErrorMsg: "",
    showAdditionalContactActions: false,
    editAdditionalContactId: undefined,
    showAdditionalContactId: undefined,
    saved: false,
    onCloseMeetRedirect: false,
    saveClicked: false,
    send_to_cc: false,
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState((prevState) => ({
      project: {
        ...prevState.project,
        id,
      },
    }));
    this.props.getActivityStatusList().then((action) => {
      this.setState({ loading: false, statusList: action.response });
    });
    this.getProjectDetails(id);
    this.getProjectTypes();
    this.getProjectCustomerContactsByID(id);
    this.getProjectEngineersByID(id);
    this.gePreSalesProjectEngineersByID(id);
    this.getProjectAdditionalContactsByID(id);
    this.getAccountManager();
    this.getContactRoles();
    const query = new URLSearchParams(this.props.location.search);
    const type = query.get("type");
    const view = query.get("view") === "true" ? true : false;
    const completed = query.get("completed") === "true" ? true : false;
    const template = query.get("template");
    const meetingID = this.props.match.params.meetingID;
    const newState = cloneDeep(this.state);
    newState.projectMeeting.name = type;
    (newState.type as string) = type;
    (newState.view as boolean) = view;
    (newState.template as string) = template;
    this.setState(newState);
    if (view) {
      if (completed) {
        let meeting = JSON.parse(localStorage.getItem("meeting")) as any;
        if (type === "Customer Touch" || type === "Close Out") {
          meeting = meeting.meeting;
          (newState.projectMeeting as any) = meeting || {};
        } else {
          (newState.projectMeeting as any) = meeting.meeting || {};
        }
        (newState.projectMeeting.critical_path_items as any) =
          (meeting && meeting.critical_path_items) || [];
        (newState.projectMeeting.risk_items as any) = meeting.risk_items;
        (newState.meetingNote as any) =
          meeting.meeting_notes && meeting.meeting_notes[0];
        (newState.projectMeeting.action_items as any) = meeting.action_items;
        newState.projectMeeting.email_body = meeting.email_body || "";
        newState.projectMeeting.email_subject = meeting.email_subject || "";
        (newState.meetingAttendees as any) = meeting.meeting_attendee;
        this.setState(newState);
      } else {
        this.getCompletedMeeting();
      }
    } else {
      if (meetingID && meetingID !== "0") {
        this.getMeeting(meetingID, type);
        this.getMeetingNotesByProjectID(id, meetingID);
        this.getMeetingAttendeesByProjectID(id, meetingID);
        this.getExternalMeetingAttendeesByProjectID(id, meetingID);
        this.getProjectPhasesByID(id);
      } else if (["Kickoff", "Internal Kickoff", "Status"].includes(type)) {
        this.getMeetingTemplate(template);
      }
    }
  }

  getAccountManager = () => {
    this.props
      .getActionItemList(this.props.match.params.id, "account-manager")
      .then((action) => {
        this.setState({ loading: false, accountManager: action.response });
      });
  };

  getProjectAdditionalContactsByID = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectAdditionalContactsByID(id).then((action) => {
      if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
        this.setState({
          projectAdditionalContacts: action.response,
          showAddContactForm: false,
          showAdditionalContactForm: false,
          email: undefined,
          name: undefined,
          phone: undefined,
          role: undefined,
        });
      }
      this.setState({ loading: false });
    });
  };

  getProjectEngineersByID = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectEngineersByID(id).then((action) => {
      if (action.type === PROJECT_ENGINEERS_SUCCESS) {
        this.setState({
          projectEngineers: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  gePreSalesProjectEngineersByID = (id: number) => {
    this.setState({ loading: true });
    this.props
      .getProjectEngineersByID(id, "pre-sales-engineers")
      .then((action) => {
        if (action.type === PROJECT_ENGINEERS_SUCCESS) {
          this.setState({
            preSalesEngineers: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  getAllCustomerUsers = (customerID: string) => {
    this.setState({ fetchingCustomerUsers: true });
    this.props.fetchAllCustomerUsers(customerID).then((action) => {
      if (action.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        this.setState({
          customerUsers: action.response,
        });
      }
      this.setState({ fetchingCustomerUsers: false });
    });
  };

  getProjectCustomerContactsByID = (id: number) => {
    this.setState({ loading: true, isFetchingCustomerContacts: true });
    this.props.getProjectCustomerContactsByID(id).then((action) => {
      if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
        this.setState({
          projectCustomerContacts: action.response,
        });
      }
      this.setState({ loading: false, isFetchingCustomerContacts: false });
    });
  };

  getProjectDetails = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectDetails(id).then((action) => {
      if (action.type === PROJECT_DETAILS_SUCCESS) {
        this.setState({
          project: action.response,
          currentProjectTypeId: action.response.type_id,
        });
        this.getAllCustomerUsers(action.response.customer_id);
        this.getProjectTickts(action.response.crm_id);
      }
      this.setState({ loading: false });
    });
  };

  getProjectTypes = () => {
    this.setState({ loading: true });
    this.props.getProjectTypes().then((action) => {
      if (action.type === PROJECT_TYPES_SUCCESS) {
        const projectTypes = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));
        this.setState({ projectTypes });
      }
      this.setState({ loading: false });
    });
  };

  getMeetingNotesByProjectID = (projectId: number, meetingId: number) => {
    this.props
      .getMeetingNotesByProjectID(projectId, meetingId)
      .then((action) => {
        if (action.type === PROJECT_MEETING_NOTES_SUCCESS) {
          if (action.response.length > 0) {
            this.setState({
              meetingNote: action.response[0],
              noteData: action.response[0].note,
            });
          }
        }
        this.setState({ loading: false });
      });
  };

  getMeetingAttendeesByProjectID = (projectId: number, meetingId: number) => {
    this.props
      .getMeetingAttendeesByProjectID(projectId, meetingId)
      .then((action) => {
        if (action.type === MEETING_ATTENDEES_SUCCESS) {
          this.setState({
            meetingAttendees: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  getExternalMeetingAttendeesByProjectID = (
    projectId: number,
    meetingId: number
  ) => {
    this.props
      .getExternalMeetingAttendeesByProjectID(projectId, meetingId)
      .then((action) => {
        if (action.type === EXTERNAL_MEETING_ATTENDEES_SUCCESS) {
          this.setState({
            externalMeetingAttendees: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  saveMeetingAttendeesByProjectID = (userId: number) => {
    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;

    const data = {
      attendee: userId,
    };

    this.props
      .saveMeetingAttendeesByProjectID(projectID, meetingID, data)
      .then((action) => {
        if (action.type === MEETING_ATTENDEES_SUCCESS) {
          this.getMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  saveExternalMeetingAttendeesByProjectID = (userId: number) => {
    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;

    const data = {
      external_contact: userId,
    };

    this.props
      .saveExternalMeetingAttendeesByProjectID(projectID, meetingID, data)
      .then((action) => {
        if (action.type === EXTERNAL_MEETING_ATTENDEES_SUCCESS) {
          this.getExternalMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  getProjectPhasesByID = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectPhasesByID(id).then((action) => {
      if (action.type === PROJECT_PHASES_SUCCESS) {
        const phasesList = action.response;
        this.setState({ phases: phasesList });
      }
      this.setState({ loading: false });
    });
  };
  updatePhaseComplete = () => {
    const id = this.props.match.params.id;
    this.setState({ loading: true });
    const newState = cloneDeep(this.state);
    this.props
      .updateProjectPhasesStatusByID(
        {
          status: this.state.statusList.find((x) => x.title === "Completed").id,
          estimated_completion_date: this.state.estimated_completion_date,
        },
        id,
        this.state.phase_to_complete
      )
      .then((action) => {
        this.setState({
          loading: false,
          onCloseMeetRedirect: true,
          view: true,
        });
      });

    this.setState(newState);
  };

  deleteMeetingAttendeesByProjectID = (meetingAttendee: any) => {
    this.setState({ loading: true });

    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;
    const meetingAttendeeId = meetingAttendee.id;

    this.props
      .deleteMeetingAttendeesByProjectID(
        projectID,
        meetingID,
        meetingAttendeeId
      )
      .then((action) => {
        if (action.type === MEETING_ATTENDEES_SUCCESS) {
          this.getMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  deleteExternalMeetingAttendeesByProjectID = (meetingAttendee: any) => {
    this.setState({ loading: true });

    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;
    const meetingAttendeeId = meetingAttendee.id;

    this.props
      .deleteExternalMeetingAttendeesByProjectID(
        projectID,
        meetingID,
        meetingAttendeeId
      )
      .then((action) => {
        if (action.type === EXTERNAL_MEETING_ATTENDEES_SUCCESS) {
          this.getExternalMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    const targetName = e.target.name;
    const targetValue = e.target.value;
    if (e.target.name === "estimated_completion_date")
      newState[targetName] = targetValue;
    else newState.projectMeeting[targetName] = targetValue;
    this.setState(newState);
  };

  handleChangeStart = (e: { target: { name: string; value: Moment } }) => {
    const newState = cloneDeep(this.state);
    const targetValue = e.target.value;
    newState.projectMeeting.schedule_start_datetime = targetValue;
    const end = moment(targetValue).add(1, "hours");
    (newState.projectMeeting.schedule_end_datetime as any) = end;
    this.setState(newState);
  };

  handleChangeStartClose = (e: { target: { name: string; value: Moment } }) => {
    const newState = cloneDeep(this.state);
    const targetValue = e.target.value;
    if (e.target.name === "start_time") {
      newState.closeTicket.pm_time_entry.start_time = targetValue;
      const end = moment(targetValue).add(1, "hours");
      newState.closeTicket.pm_time_entry.end_time = end;
    } else {
      newState.closeTicket.pm_time_entry.end_time = targetValue;
    }
    this.setState(newState);
  };

  handleChangeEngineerStartClose = (e: {
    target: { name: string; value: Moment };
  }) => {
    const newState = cloneDeep(this.state);
    const targetValue = e.target.value;
    if (e.target.name === "start_time") {
      newState.engineeringTimeEntry.start_time = targetValue;
      const end = moment(targetValue).add(1, "hours");
      newState.engineeringTimeEntry.end_time = end;
    } else {
      newState.engineeringTimeEntry.end_time = targetValue;
    }
    this.setState(newState);
  };

  handleChangeNotes = (
    e: React.ChangeEvent<HTMLInputElement>,
    engineeringNotes: boolean
  ) => {
    const newState = cloneDeep(this.state);
    const targetName = e.target.name;
    const targetValue = e.target.value;
    if (engineeringNotes)
      newState.engineeringTimeEntry[targetName] = targetValue;
    else newState.closeTicket.pm_time_entry[targetName] = targetValue;
    this.setState(newState);
  };

  getProjectType = () => {
    const { projectTypes, currentProjectTypeId } = this.state;
    if (projectTypes.length > 0 && currentProjectTypeId) {
      const type = projectTypes.find(
        (obj) => obj.value == currentProjectTypeId
      );
      if (type) {
        return type.value;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  getMeetingCloseText = (type: string = this.state.type) => {
    let text = "";
    switch (type) {
      case "Status":
        text = "Send to contacts";
        break;
      case "Customer Touch":
        text =
          "Send to Primary Project contact and account manager, mark the project ticket Acela project phase completed.";
        break;
      case "Internal Kickoff":
        text = "Send to contacts and mark kickoff complete";
        break;
      case "Kickoff":
        text = "Send to contacts and mark kickoff complete";
        break;
      case "Closeout":
        text =
          "Send to Primary Project contact and account manager, mark the project ticket Acela project phase completed.";
        break;
      case "Close Out":
        text =
          "Send to Primary contact and account manager and mark the project phase as complete";
        break;
      default:
        break;
    }
    return text;
  };

  onSaveClick = (showAlert: boolean = true) => {
    const projectID = this.props.match.params.id;
    let projectMeeting = this.state.projectMeeting;
    if (
      ["Kickoff", "Status", "Internal Kickoff"].includes(this.state.type) &&
      projectMeeting.agenda_topics.length > 0 &&
      projectMeeting.agenda_topics.findIndex((agenda_topic) =>
        commonFunctions.isEditorEmpty(agenda_topic.topic)
      ) !== -1
    ) {
      this.setState({ saveClicked: true });
      return;
    }
    this.setState({ loading: true, currentStatusList: [] });
    if (
      !projectMeeting.agenda_topics ||
      (projectMeeting.agenda_topics &&
        projectMeeting.agenda_topics.length === 0)
    ) {
      projectMeeting.agenda_topics = null;
    }
    if (this.state.type === "Internal Kickoff") {
      projectMeeting.is_internal = true;
    }
    let url = `${getURLBYMeetingType(this.state.type)}`;
    const meetingID = this.props.match.params.meetingID;

    if (meetingID && meetingID !== "0") {
      url = `${getURLBYMeetingType(this.state.type)}/${
        this.props.match.params.meetingID
      }`;
    }
    this.props
      .postStatusMeeting(projectID, projectMeeting, url)
      .then((action) => {
        if (action.type === MEETING_SUCCESS) {
          this.state.noteData && this.meetingNotesAction(action.response.id);
          showAlert && this.props.addSuccessMessage(`Meeting Saved !!!`);
          this.setState({ loading: false, saveClicked: false });
          if (this.state.closeMeeting) {
            this.saveMeetingAction(action.response.id);
          }
        } else {
          this.setState({ loading: false, saveClicked: false });
        }
      });
  };

  meetingNotesAction = (
    meetingID: string = this.props.match.params.meetingID
  ) => {
    const projectID = this.props.match.params.id;

    if (this.state.meetingNote.id) {
      // edit note
      const noteId = this.state.meetingNote.id;
      this.props
        .updateMeetingNotesByProjectID(
          projectID,
          Number(meetingID),
          noteId,
          this.state.noteData
        )
        .then((action) => {
          if (action.type === PROJECT_MEETING_NOTES_SUCCESS) {
            this.setState({
              meetingNote: action.response,
              noteData: action.response.note,
              saved: true,
            });
            setTimeout(() => this.setState({ saved: false }), 5000);
          }
        });
    } else {
      // save note
      this.props
        .saveMeetingNotesByProjectID(
          projectID,
          Number(meetingID),
          this.state.noteData
        )
        .then((action) => {
          if (action.type === PROJECT_MEETING_NOTES_SUCCESS) {
            this.setState({
              meetingNote: action.response,
              noteData: action.response.note,
              saved: true,
            });
            setTimeout(() => this.setState({ saved: false }), 5000);
          }
          this.setState({ loading: false });
        });
    }
  };

  saveMeetingAction = (
    meetingID: string = this.props.match.params.meetingID
  ) => {
    this.setState({ loading: true });
    const projectID = this.props.match.params.id;
    const newState = cloneDeep(this.state);
    const engineeringTimeEntry = newState.engineeringTimeEntry;
    const sourceDate = newState.closeTicket.pm_time_entry.start_time;
    engineeringTimeEntry.start_time.set({
      year: sourceDate.year(),
      month: sourceDate.month(),
      date: sourceDate.date(),
    });
    engineeringTimeEntry.end_time.set({
      year: sourceDate.year(),
      month: sourceDate.month(),
      date: sourceDate.date(),
    });
    this.state.closeTicket.engineer_ids.map((x) => {
      newState.closeTicket.engineer_time_entries.push({
        ...engineeringTimeEntry,
        ticket_id: newState.closeTicket.engineer_ticket_id,
        user_id: x,
      });
    });
    this.setState(newState);

    const url: string =
      getURLBYMeetingType(this.state.type) +
      `/${meetingID}/actions/complete?add_customer_contacts=${!this.state
        .send_to_cc}`;

    this.props
      .postStatusMeeting(projectID, newState.closeTicket, url)
      .then((action) => {
        if (action.type === MEETING_SUCCESS) {
          this.makeMeetingComplete(meetingID);
          this.setState({ currentStatusList: action.response });
        } else {
          this.setState({ loading: false, ticketError: action.errorList.data });
        }
      });
  };
  makeMeetingComplete = (
    meetingID: string = this.props.match.params.meetingID
  ) => {
    const projectID = this.props.match.params.id;
    const url = `meetings/${meetingID}/archive`;
    this.props.meetingComplete(projectID, "post", url).then((action) => {
      if (action.type === MEETING_COMPLETE_SUCCESS) {
        if (this.state.phase_to_complete) {
          this.updatePhaseComplete();
        } else {
          this.setState({ onCloseMeetRedirect: true, view: true });
        }
      }
      this.setState({ loading: false });
    });
  };
  getCompletedMeeting = () => {
    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;
    const url = `meetings/${meetingID}/archive`;
    this.setState({ loading: true });
    this.props.meetingComplete(projectID, "get", url).then((action) => {
      if (action.type === MEETING_COMPLETE_SUCCESS) {
        const newState = cloneDeep(this.state);
        (newState.projectMeeting as any) = action.response.meeting;
        (newState.projectMeeting
          .agenda_topics as any) = action.response.meeting.agenda_topics;
        (newState.projectMeeting
          .critical_path_items as any) = action.response.critical_path_items;
        (newState.projectMeeting
          .risk_items as any) = action.response.risk_items;
        (newState.meetingNote as any) =
          action.response.meeting_notes && action.response.meeting_notes[0];
        (newState.projectMeeting
          .action_items as any) = action.response.action_items;
        newState.projectMeeting.email_body =
          action.response.meeting.email_body || "";
        newState.projectMeeting.email_subject =
          action.response.meeting.email_subject || "";
        (newState.meetingAttendees as any) = action.response.meeting_attendee;
        this.setState(newState);
      }
    });
  };
  callbackfn = (agenda_topics) => {
    const newState = cloneDeep(this.state);
    newState.projectMeeting.agenda_topics = agenda_topics;
    this.setState(newState, () => {
      this.onSaveClick(false);
    });
  };
  callbackCustomerTouchMeeting = (d) => {
    const newState = cloneDeep(this.state);

    newState.projectMeeting.email_body = d.customerTouchMeetingMailBodyMarkDown;
    newState.projectMeeting.email_subject = d.customerTouchMeetingMailSubject;

    const meetingBodyTxtArr = d.customerTouchMeetingMailBodyText.split(" ");

    const meetingBodyTxtArrFinalArr = [];
    meetingBodyTxtArr.map((word) => {
      if (word.startsWith("{")) {
        word = "#" + word;
      }
      meetingBodyTxtArrFinalArr.push(word);
    });

    newState.projectMeeting.email_body_text = meetingBodyTxtArrFinalArr.join(
      " "
    );
    newState.projectMeeting.email_body_replaced_text =
      d.customerTouchMeetingMailBodyMarkDown;
    this.setState(newState);
  };
  callbackCloseOutMeeting = (d) => {
    const newState = cloneDeep(this.state);
    newState.projectMeeting.email_subject =
      d.closeOutMeetingEmailTemplateMailSubject;
    newState.projectMeeting.project_acceptance_replaced_text =
      d.projectAcceptanceMailBodyMarkDown;
    newState.projectMeeting.project_acceptance_markdown =
      d.projectAcceptanceMailBodyMarkDown;

    const meetingBodyTxtArr = d.projectAcceptanceMailBodyText.split(" ");

    const meetingBodyTxtArrFinalArr = [];
    meetingBodyTxtArr.map((word) => {
      if (word.startsWith("{")) {
        word = "#" + word;
      }
      meetingBodyTxtArrFinalArr.push(word);
    });

    newState.projectMeeting.project_acceptance_text = meetingBodyTxtArrFinalArr.join(
      " "
    );
    this.setState(newState);
  };

  callbackMeetingNote = (note: string, autoSave: boolean) => {
    const newState = cloneDeep(this.state);
    (newState.meetingNote.note as any) = note;
    (newState.noteData as any) = note;
    (newState.saved as boolean) = false;
    this.setState(newState, () => {
      if (autoSave) {
        const meetingID = this.props.match.params.meetingID;
        this.meetingNotesAction(meetingID);
      }
    });
  };

  getMeetingTemplate = (template) => {
    this.setState({ loading: true });
    const url = `settings/meeting-templates/${template}`;

    this.props.getMeetingTemplates(url).then((action) => {
      const newState = cloneDeep(this.state);
      newState.projectMeeting.agenda_topics = action.response.agenda_topics;
      newState.projectMeeting.action_items = action.response.action_items;
      this.setState(newState);
    });
  };
  getMeeting = (id, type) => {
    this.setState({ loading: true, isFetchingMeetingDetails: true });
    const url = `projects/${this.props.match.params.id}/${getURLBYMeetingType(
      type
    )}/${id}`;
    this.props.getMeetingTemplates(url).then((action) => {
      if (action.type === TEMPLATES_SUCCESS) {
        const newState = cloneDeep(this.state);
        (newState.projectMeeting as any) = action.response;
        (newState.isFetchingMeetingDetails as any) = false;
        this.setState(newState);
      }
    });
  };

  getContactRoles = async () => {
    const mapping: any = await this.props.getContactRoles();
    this.setState({ customerContactRolesList: mapping.response });
  };

  getCustomerUserOptions = () => {
    const users = this.state.customerUsers
      ? this.state.customerUsers.map((t) => ({
          value: t.id,
          label: `${t.first_name} ${t.last_name}`,
          disabled: false,
        }))
      : [];

    return users;
  };

  getCustomerRoleOptions = () => {
    const roleOptions = this.state.customerContactRolesList
      ? this.state.customerContactRolesList.map((t) => ({
          value: t.id,
          label: `${t.role}`,
          disabled: false,
        }))
      : [];

    return roleOptions;
  };

  handleChangeCheckBox = (e: any) => {
    const newState = cloneDeep(this.state);
    (newState[e.target.name] as any) = e.target.checked;
    this.setState(newState);
  };

  handleChangeAttendeeCheck = (e: any, userId: any) => {
    if (e.target.checked) {
      // call save
      this.saveMeetingAttendeesByProjectID(userId);
    } else {
      // call remove
      const attendeeArr =
        this.state.meetingAttendees &&
        this.state.meetingAttendees.filter((e) => e.attendee === userId);
      if (attendeeArr.length > 0) {
        this.deleteMeetingAttendeesByProjectID(attendeeArr[0]);
      }
    }
  };

  handleChangeExternalAttendeeCheck = (e: any, userId: any) => {
    if (e.target.checked) {
      // call save
      this.saveExternalMeetingAttendeesByProjectID(userId);
    } else {
      // call remove
      const attendeeArr =
        this.state.externalMeetingAttendees &&
        this.state.externalMeetingAttendees.filter(
          (e) => e.external_contact === userId
        );
      if (attendeeArr.length > 0) {
        this.deleteExternalMeetingAttendeesByProjectID(attendeeArr[0]);
      }
    }
  };

  getProjectTickts = (id) => {
    this.setState({ loading: true });
    this.props.getProjectTickets(id).then((action) => {
      if (action.type === TEMPLATES_SUCCESS) {
        this.setState({
          loading: false,
          ticketList: action.response,
        });
      }
    });
  };

  disableSaveMeeting = () => {
    if (this.state.isFetchingMeetingDetails) return true;
    let disabled = this.state.loading;
    if (this.state.closeMeeting) {
      if (!this.state.closeTicket.ticket_id) {
        disabled = true;
      } else {
        disabled = false;
      }
    }
    if (
      (this.state.closeTicket.engineer_ticket_id &&
        this.state.closeTicket.engineer_ids.length === 0) ||
      (!this.state.closeTicket.engineer_ticket_id &&
        this.state.closeTicket.engineer_ids.length > 0)
    ) {
      disabled = true;
    }
    return disabled;
  };

  handleContactChange = (e: any, type: string) => {
    if (type == "name") {
      this.setState({
        selectedCustomerName: e.target.value,
      });
    }

    if (type == "role") {
      this.setState({
        selectedCustomerRole: e.target.value,
      });
    }
  };

  handleAdditionalContactChange = (e: any) => {
    const newState = cloneDeep(this.state);
    (newState[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  addCustomerContact = () => {
    this.setState({ loading: false });
    const customerContact = this.state.customerUsers.filter(
      (customer) => customer.id === this.state.selectedCustomerName
    );

    if (
      this.state.selectedCustomerName &&
      this.state.selectedCustomerName &&
      customerContact.length > 0
    ) {
      const data = {
        role: this.state.selectedCustomerRole,
        user: this.state.selectedCustomerName,
        contact_crm_id: customerContact[0].crm_id,
      };

      this.props
        .saveProjectCustomerContactsByID(this.state.project.id, data)
        .then((action) => {
          if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
            this.getProjectCustomerContactsByID(this.state.project.id);
            this.setState((prevState) => ({
              showAddContactForm: false,
              selectedCustomerRole: undefined,
              selectedCustomerName: undefined,
              showContactAddErrorMsg: "",
            }));
          } else {
            let errorMsg = "";

            if (Array.isArray(action.errorList.data)) {
              errorMsg = action.errorList.data[0]["contact/id"];
            } else {
              errorMsg = action.errorList.data.contact_crm_id;
            }
            this.setState((prevState) => ({
              showContactAddErrorMsg: errorMsg,
            }));
          }
        });
    }
  };

  closeUserModal = () => {
    this.setState((prevState) => ({
      showCreateUserModal: !prevState.showCreateUserModal,
    }));
    this.getAllCustomerUsers(this.state.project.customer_id);
  };

  onPDFPreviewClick = () => {
    this.setState({ pdfLoading: true });
    this.onSaveClick(false);

    const meetingID = this.props.match.params.meetingID;
    const query = new URLSearchParams(this.props.location.search);
    const completed = query.get("completed") === "true" ? true : false;

    this.props
      .getMeetingPDFPreview(
        this.state.project.id,
        meetingID,
        completed,
        !this.state.send_to_cc
      )
      .then((a) => {
        if (a.type === FETCH_MEETING_PDF_PREVIEW_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            pdfLoading: false,
          });
        } else {
          this.setState({
            pdfLoading: false,
          });
        }
      });
  }

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  addAdditionalContact = () => {
    const isValid = this.validateForm();
    if (isValid) {
      const {
        editAdditionalContactId,
        email,
        name,
        role,
        phone,
        project,
      } = this.state;

      let data: any = {
        name: name,
        phone: phone ? phone : null,
        role: role,
      };
      if (editAdditionalContactId) {
        // put
        const contact: any = this.state.projectAdditionalContacts.filter(
          (val) => val.id === editAdditionalContactId
        );
        if (contact.length > 0 && email !== contact[0].email) {
          data = { ...data, email: email };
        }
        this.props
          .updateAdditionalContact(project.id, editAdditionalContactId, data)
          .then((action) => {
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
              this.getProjectAdditionalContactsByID(project.id);
              this.setState({
                showAdditionalContacts: false,
                showAddContactForm: false,
              });
            }
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
              this.setValidationErrors(action.errorList.data);
            }
          });
      } else {
        data = { ...data, email: email };
        this.props
          .saveProjectAdditionalContactsByID(project.id, data)
          .then((action) => {
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
              this.getProjectAdditionalContactsByID(project.id);
              this.setState({
                showAdditionalContacts: false,
                showAddContactForm: false,
              });
            }
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
              this.setValidationErrors(action.errorList.data);
            }
          });
      }
    }
  };

  setValidationErrors = (errorList: object) => {
    const newState: IProjectMeetingState = cloneDeep(this.state);
    const errorT = commonFunctions.errorStateHandle(errorList, newState);
    this.setState(errorT);
  };

  validateForm() {
    const error: any = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.name || this.state.name.trim() === "") {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage = "Please enter name";
      isValid = false;
    }
    if (
      !this.state.email ||
      this.state.email.trim() === "" ||
      !AppValidators.isValidEmail(this.state.email)
    ) {
      error.email.errorState = IValidationState.ERROR;
      error.email.errorMessage = "Please enter valid email";
      isValid = false;
    }
    if (
      this.state.phone &&
      this.state.phone.trim() !== "" &&
      !ProjectMeeting.validator.isValidPhoneNumber(this.state.phone)
    ) {
      error.phone.errorState = IValidationState.ERROR;
      error.phone.errorMessage = "Please enter valid phone number";
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }

  renderAdditionalContacts = () => {
    return (
      <div style={{ paddingTop: "15px" }}>
        <div className="label-details">
          {this.state.projectAdditionalContacts.length > 0 ? "Resources" : ""}
        </div>
        {this.state.projectAdditionalContacts.map((contact, idx) => (
          <div
            className="contact-container"
            onMouseEnter={() =>
              this.setState({
                showAdditionalContactActions: true,
                showAdditionalContactId: contact.id,
              })
            }
            onMouseLeave={() => {
              if (!this.deleteConfirmBox) {
                this.setState({
                  showAdditionalContactActions: false,
                  showAdditionalContactId: undefined,
                });
              }
            }}
            key={contact.id}
          >
            <div className={`label-content ${contact.id}`}>
              <UsershortInfo name={contact.name} url={contact.profile_url} />
              {["Kickoff", "Status"].includes(this.state.type) && (
                <Checkbox
                  isChecked={
                    this.state.externalMeetingAttendees &&
                    this.state.externalMeetingAttendees.filter(
                      (e) => e.external_contact === contact.id
                    ).length > 0
                  }
                  name="externalMeetingAttendees"
                  onChange={(e) =>
                    this.handleChangeExternalAttendeeCheck(e, contact.id)
                  }
                  disabled={this.state.view}
                />
              )}
            </div>
            {<div className="label-sub-content">{contact.role}</div>}
          </div>
        ))}
        {(this.state.showAddContactForm ||
          this.state.showAdditionalContactForm) && (
          <div className="open-won-switch customer-resource">
            Customer{" "}
            <input
              className="switch"
              type="checkbox"
              onChange={(e) => {
                const value = e.target.checked === true ? true : false;
                this.setState({
                  showAdditionalContactForm: value,
                  selectedCustomerRole: undefined,
                  editAdditionalContactId: undefined,
                  showAdditionalContactId: undefined,
                  showAddContactForm: !value,
                });
              }}
            />{" "}
            Resourse
          </div>
        )}
        {!this.state.showAdditionalContactForm ? (
          ""
        ) : (
          <div className="meeting-detail-contact">
            <div className="box">
              <Input
                field={{
                  label: "Name",
                  type: InputFieldType.TEXT,
                  value: this.state.name,
                  isRequired: true,
                }}
                width={12}
                multi={false}
                name="name"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter name`}
                error={this.state.error.name}
              />
              <Input
                field={{
                  label: "Email",
                  type: InputFieldType.TEXT,
                  value: this.state.email,
                  isRequired: true,
                }}
                width={12}
                multi={false}
                name="email"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter email`}
                error={this.state.error.email}
              />
              <Input
                field={{
                  label: "Role",
                  type: InputFieldType.TEXT,
                  value: this.state.role,
                  isRequired: false,
                }}
                width={12}
                multi={false}
                name="role"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter role`}
                error={this.state.error.role}
              />
              <Input
                field={{
                  label: "Phone",
                  type: InputFieldType.TEXT,
                  value: this.state.phone,
                  isRequired: false,
                }}
                width={12}
                multi={false}
                name="phone"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter phone`}
                error={this.state.error.phone}
              />
              <div className="right">
                <SquareButton
                  content="Cancel"
                  bsStyle={ButtonStyle.DEFAULT}
                  className="contact-cancel"
                  onClick={() =>
                    this.setState({
                      showAdditionalContactAddErrorMsg: "",
                      showAdditionalContactForm: false,
                      name: undefined,
                      role: undefined,
                      email: undefined,
                      phone: undefined,
                      editAdditionalContactId: undefined,
                      showAdditionalContactId: undefined,
                    })
                  }
                />
                <SquareButton
                  content="Apply"
                  bsStyle={ButtonStyle.PRIMARY}
                  className="contact-save"
                  onClick={() => this.addAdditionalContact()}
                />
              </div>
            </div>
          </div>
        )}
        {this.state.showAdditionalContactAddErrorMsg != "" && (
          <div className="label-details-error-msg">
            {this.state.showAdditionalContactAddErrorMsg}
          </div>
        )}
      </div>
    );
  };

  render() {
    const project = this.state.project;
    const projectEngineers = this.state.projectEngineers || [];
    return (
      <div className="project-details meeting-details">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {["Status", "Kickoff", "Internal Kickoff"].includes(
          this.state.type
        ) && (
          <PDFViewer
            show={this.state.openPreview}
            onClose={this.toggleOpenPreview}
            titleElement={`Meeting PDF Preview`}
            previewHTML={{
              file_path: this.state.previewHTML && this.state.previewHTML.url,
            }}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={ButtonStyle.DEFAULT}
                onClick={this.toggleOpenPreview}
              />
            }
            className=""
          />
        )}
        <div className="header">
          <div className="left">
            <div className="project-name">
              {project.customer} - {project.title}
            </div>
          </div>
          <div className="right">
            {!this.state.view && (
              <>
                <Checkbox
                  isChecked={this.state.closeMeeting}
                  name="closeMeeting"
                  onChange={(e) => this.handleChangeCheckBox(e)}
                >
                  {this.getMeetingCloseText()}
                </Checkbox>
                {!["Internal Kickoff"].includes(this.state.type) && (
                  <Checkbox
                    isChecked={this.state.send_to_cc}
                    name="send_to_cc"
                    onChange={(e) => this.handleChangeCheckBox(e)}
                    className="ml-5"
                  >
                    Mark as Internal
                  </Checkbox>
                )}
              </>
            )}
            {["Status", "Kickoff", "Internal Kickoff"].includes(
              this.state.type
            ) && (
              <SquareButton
                content={
                  this.state.pdfLoading ? (
                    <>
                      {" "}
                      Loading PDF{" "}
                      <img
                        className="icon__loading-white"
                        src="/assets/icons/loading.gif"
                        alt="Downloading File"
                      />
                    </>
                  ) : (
                    "Preview Meeting PDF"
                  )
                }
                bsStyle={ButtonStyle.PRIMARY}
                onClick={this.onPDFPreviewClick}
                disabled={this.state.pdfLoading}
              />
            )}
            {!this.state.view && (
              <SquareButton
                content="Save"
                bsStyle={ButtonStyle.PRIMARY}
                onClick={(e) => this.onSaveClick()}
                disabled={this.disableSaveMeeting()}
              />
            )}
            <SquareButton
              content="Close"
              bsStyle={ButtonStyle.DEFAULT}
              onClick={() => {
                const query = new URLSearchParams(this.props.location.search);
                const AllProject =
                  query.get("AllProject") === "true" ? true : false;
                this.props.history.push(
                  `/ProjectManagement/${this.props.match.params.id}?meeting=true&AllProject=${AllProject}`
                );
              }}
            />
          </div>
        </div>
        <div className="first-row">
          <div className="action-second status-actions-row">
            <div className="project-action-component collapsible-section-meeting">
              {["Status", "Kickoff", "Internal Kickoff"].includes(
                this.state.type
              ) && (
                <div className="name-date">
                  <Input
                    field={{
                      label: "Name",
                      type: InputFieldType.TEXT,
                      value: this.state.projectMeeting.name,
                      isRequired: false,
                    }}
                    width={10}
                    labelIcon={"info"}
                    name="name"
                    onChange={(e) => this.handleChange(e)}
                    placeholder={`Enter name`}
                    disabled={this.state.view}
                    className="meeeting-name-field"
                  />
                  <div className="date-time">
                    <Input
                      field={{
                        label: "Date",
                        type: InputFieldType.DATE,
                        value: this.state.projectMeeting
                          .schedule_start_datetime,
                        isRequired: false,
                      }}
                      width={4}
                      labelIcon={"info"}
                      name="schedule_start_datetime"
                      onChange={(e) => this.handleChangeStart(e)}
                      placeholder={`Select Date`}
                      showTime={false}
                      disablePrevioueDates={true}
                      disabled={this.state.view}
                    />
                    <div className="time-to-from">
                      <Input
                        field={{
                          label: "Time",
                          type: InputFieldType.DATE,
                          value: this.state.projectMeeting
                            .schedule_start_datetime,
                          isRequired: false,
                        }}
                        width={4}
                        labelIcon={"info"}
                        name="schedule_start_datetime"
                        onChange={(e) => this.handleChangeStart(e)}
                        showTime={true}
                        showDate={false}
                        hideIcon={true}
                        disablePrevioueDates={true}
                        disabled={this.state.view}
                      />
                      <div className="dash-separator">-</div>
                      <Input
                        field={{
                          label: " ",
                          type: InputFieldType.DATE,
                          value: this.state.projectMeeting
                            .schedule_end_datetime,
                          isRequired: false,
                        }}
                        width={4}
                        labelIcon={"info"}
                        name="schedule_end_datetime"
                        onChange={(e) => this.handleChange(e)}
                        showTime={true}
                        showDate={false}
                        hideIcon={true}
                        className="to-time"
                        disablePrevioueDates={true}
                        disabled={this.state.view}
                      />
                    </div>
                  </div>
                </div>
              )}
              {["Status"].includes(this.state.type) && (
                <>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Agenda Topics"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <AgendaTopic
                        saveClicked={this.state.saveClicked}
                        viewOnly={this.state.view}
                        initialData={this.state.projectMeeting.agenda_topics}
                        setAgenda={this.callbackfn}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Critical Path Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ActionItems
                        initialData={
                          this.state.projectMeeting.critical_path_items
                        }
                        viewOnly={this.state.view}
                        api={"critical-path-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#550aad"
                    label={"Action Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ActionItems
                        initialData={this.state.projectMeeting.action_items}
                        viewOnly={this.state.view}
                        api={"action-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#c007d0"
                    label={"Risks"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <RiskItems
                        initialData={this.state.projectMeeting.risk_items}
                        viewOnly={this.state.view}
                        api={"risk-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  {!this.state.loading &&
                    hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <MeetingNotes
                        note={
                          this.state.meetingNote && this.state.meetingNote.note
                        }
                        callbackMeetingNote={this.callbackMeetingNote}
                        saved={this.state.saved}
                      />
                    )}
                </>
              )}
              {["Kickoff"].includes(this.state.type) && (
                <>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Agenda Topics"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <AgendaTopic
                        saveClicked={this.state.saveClicked}
                        viewOnly={this.state.view}
                        initialData={this.state.projectMeeting.agenda_topics}
                        setAgenda={this.callbackfn}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Critical Path Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ActionItems
                        initialData={
                          this.state.projectMeeting.critical_path_items
                        }
                        viewOnly={this.state.view}
                        api={"critical-path-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>

                  <PMOCollapsible
                    background="#176f07"
                    label={"Action Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ActionItems
                        initialData={this.state.projectMeeting.action_items}
                        viewOnly={this.state.view}
                        api={"action-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>

                  <PMOCollapsible
                    background="#c007d0"
                    label={"Risks"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <RiskItems
                        initialData={this.state.projectMeeting.risk_items}
                        viewOnly={this.state.view}
                        api={"risk-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  {hasProjectMettingIDS(
                    this.state.project,
                    this.state.projectMeeting
                  ) && (
                    <MeetingNotes
                      note={
                        this.state.meetingNote && this.state.meetingNote.note
                      }
                      callbackMeetingNote={this.callbackMeetingNote}
                      saved={this.state.saved}
                    />
                  )}
                </>
              )}
              {["Internal Kickoff"].includes(this.state.type) && (
                <>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Agenda Topics"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <AgendaTopic
                        saveClicked={this.state.saveClicked}
                        viewOnly={this.state.view}
                        initialData={this.state.projectMeeting.agenda_topics}
                        setAgenda={this.callbackfn}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Critical Path Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ActionItems
                        initialData={
                          this.state.projectMeeting.critical_path_items
                        }
                        viewOnly={this.state.view}
                        api={"critical-path-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#176f07"
                    label={"Action Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ActionItems
                        initialData={this.state.projectMeeting.action_items}
                        viewOnly={this.state.view}
                        api={"action-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#c007d0"
                    label={"Risks"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <RiskItems
                        initialData={this.state.projectMeeting.risk_items}
                        viewOnly={this.state.view}
                        api={"risk-items"}
                        project={this.state.project}
                      />
                    )}
                  </PMOCollapsible>
                  {hasProjectMettingIDS(
                    this.state.project,
                    this.state.projectMeeting
                  ) && (
                    <MeetingNotes
                      note={
                        this.state.meetingNote && this.state.meetingNote.note
                      }
                      callbackMeetingNote={this.callbackMeetingNote}
                      saved={this.state.saved}
                    />
                  )}
                </>
              )}
              {["Closeout", "Close Out"].includes(this.state.type) && (
                <>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Project Acceptance"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ProjectAcceptance
                        defaultProjectAcceptance={{
                          defaultMarkdown: this.state
                            .default_project_acceptance_markdown,
                          defaultText: this.state
                            .default_project_acceptance_text,
                        }}
                        projectAcceptance={{
                          emailSubject: this.state.projectMeeting.email_subject,
                          projectAcceptanceMarkdown: this.state.projectMeeting
                            .project_acceptance_markdown,
                          projectAcceptanceText: this.state.projectMeeting
                            .project_acceptance_text,
                          replacedProjectAcceptanceText: this.state
                            .projectMeeting.project_acceptance_replaced_text,
                        }}
                        setData={this.callbackCloseOutMeeting}
                        projectID={this.state.project.id}
                        meetingID={this.props.match.params.meetingID}
                        viewOnly={this.state.view}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#c007d0"
                    label={"Meeting Documents"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMettingIDS(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <MeetingDocuments
                        meetingID={this.props.match.params.meetingID}
                        projectID={this.state.project.id}
                        viewOnly={this.state.view}
                      />
                    )}
                  </PMOCollapsible>
                </>
              )}
              {["Customer Touch"].includes(this.state.type) && (
                <div className="list-actions">
                  <CustomerTouchMeeting
                    initialData={this.state.projectMeeting}
                    viewOnly={this.state.view}
                    meeting={this.props.match.params.meetingID}
                    setData={this.callbackCustomerTouchMeeting}
                  />
                </div>
              )}
            </div>
          </div>
          <div className="status-overall-accordians">
            <Accordian label={"Project Details"} isOpen={true}>
              <div className="details-box">
                <div className="status-overall">
                  <div className="action">
                    <div className="status-text">Overall Status:</div>
                    <div className="field-section color-preview" title={""}>
                      <div
                        style={{
                          backgroundColor: project.overall_status_color,
                          borderColor: project.overall_status_color,
                        }}
                        className="left-column"
                      />
                      <div
                        style={{
                          background: `${getConvertedColorWithOpacity(
                            project.overall_status_color
                          )}`,
                        }}
                        className="text"
                      >
                        {project.overall_status}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Customer</div>
                <div className="label-content">{project.customer}</div>
              </div>
              <div className="details-box">
                <div className="label-details">Customer Contacts</div>
                {this.state.projectCustomerContacts.length > 0 &&
                  ["Kickoff", "Status", "Internal Kickoff"].includes(
                    this.state.type
                  ) && (
                    <div className="label-sub-content-attended-text">
                      Attended?
                    </div>
                  )}
                {this.state.projectCustomerContacts.map((contact, idx) => (
                  <div className="contact-container" key={idx}>
                    <div className={`label-content ${contact.contact_crm_id}`}>
                      <UsershortInfo
                        name={contact.name}
                        url={contact.profile_pic}
                      />
                      {["Kickoff", "Status"].includes(this.state.type) && (
                        <Checkbox
                          isChecked={
                            !this.state.send_to_cc &&
                            this.state.meetingAttendees &&
                            this.state.meetingAttendees.filter(
                              (e) => e.attendee === contact.user_id
                            ).length > 0
                          }
                          name="meetingAttendees"
                          onChange={(e) =>
                            this.handleChangeAttendeeCheck(e, contact.user_id)
                          }
                          disabled={
                            this.state.view ||
                            this.state.send_to_cc ||
                            contact.user_id === undefined ||
                            contact.user_id === null
                          }
                        />
                      )}
                    </div>
                    {
                      <div className="label-sub-content">
                        {contact.project_role}
                      </div>
                    }
                  </div>
                ))}
                <div className="contact-container">
                  {this.renderAdditionalContacts()}
                </div>
                {this.state.showAddContactForm && (
                  <div className="meeting-detail-contact">
                    <div className="box">
                      <div className="select-type select-customer-contact add-new-option-section field-section col-xs-6 col-md-6">
                        <div className="add-new-option-box">
                          <div className="field__label row">
                            <label className="field__label-label" title="">
                              Select User
                            </label>
                            <span className="field__label-required" />
                          </div>
                          <div>
                            <SelectInput
                              name="user_id"
                              value={this.state.selectedCustomerName}
                              onChange={(e) =>
                                this.handleContactChange(e, "name")
                              }
                              options={this.getCustomerUserOptions()}
                              multi={false}
                              searchable={true}
                              placeholder="Select User"
                              disabled={false}
                              loading={this.state.fetchingCustomerUsers}
                            />
                          </div>
                        </div>

                        <SquareButton
                          content="+"
                          onClick={(e) =>
                            this.setState({
                              showCreateUserModal: !this.state
                                .showCreateUserModal,
                            })
                          }
                          className="add-new-option-customer"
                          bsStyle={ButtonStyle.PRIMARY}
                          title="Add New User"
                          disabled={false}
                        />
                      </div>
                      <Input
                        field={{
                          label: "Project Role",
                          type: InputFieldType.PICKLIST,
                          value: this.state.selectedCustomerRole,
                          options: this.getCustomerRoleOptions(),
                          isRequired: false,
                        }}
                        width={12}
                        multi={false}
                        name="status"
                        onChange={(e) => this.handleContactChange(e, "role")}
                        placeholder={`Select`}
                      />
                      <div className="right">
                        <SquareButton
                          content="Cancel"
                          bsStyle={ButtonStyle.DEFAULT}
                          className="contact-cancel"
                          onClick={() =>
                            this.setState({
                              showContactAddErrorMsg: "",
                              showAddContactForm: false,
                              selectedCustomerName: undefined,
                              selectedCustomerRole: undefined,
                            })
                          }
                        />
                        <SquareButton
                          content="Apply"
                          bsStyle={ButtonStyle.PRIMARY}
                          className="contact-save"
                          onClick={() => this.addCustomerContact()}
                        />
                      </div>
                      {this.state.showCreateUserModal && (
                        <CustomerUserNew
                          isVisible={this.state.showCreateUserModal}
                          close={this.closeUserModal}
                          customerId={this.state.project.customer_id}
                        />
                      )}
                    </div>
                  </div>
                )}
                {this.state.showContactAddErrorMsg != "" && (
                  <div className="label-details-error-msg">
                    {this.state.showContactAddErrorMsg}
                  </div>
                )}
              </div>

              <div className="details-box">
                <div className="label-details">Project Manager</div>
                <div className="label-content">
                  <UsershortInfo
                    name={project.project_manager}
                    url={project.project_manager_profile_pic}
                  />
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Engineer Name</div>
                {projectEngineers.length === 0 && (
                  <div className="label-content">-</div>
                )}
                {projectEngineers.map((engineer, idx) => (
                  <div className="label-content" key={idx}>
                    <UsershortInfo
                      name={`${engineer.first_name} ${engineer.last_name}`}
                      url={engineer.profile_url}
                    />
                    {["Kickoff", "Status", "Internal Kickoff"].includes(
                      this.state.type
                    ) && (
                      <Checkbox
                        isChecked={
                          this.state.meetingAttendees &&
                          this.state.meetingAttendees.filter(
                            (e) => e.attendee === engineer.id
                          ).length > 0
                        }
                        name="meetingAttendees"
                        onChange={(e) =>
                          this.handleChangeAttendeeCheck(e, engineer.id)
                        }
                        disabled={
                          this.state.view ||
                          engineer.id === undefined ||
                          engineer.id === null
                        }
                      />
                    )}
                  </div>
                ))}
              </div>
              <div className="details-box">
                <div className="label-details">Pre Sales Engineer</div>
                {this.state.preSalesEngineers &&
                  this.state.preSalesEngineers.length === 0 && (
                    <div className="label-content">-</div>
                  )}
                {this.state.preSalesEngineers &&
                  this.state.preSalesEngineers.map((engineer, idx) => (
                    <div className="label-content" key={idx}>
                      <UsershortInfo
                        name={`${engineer.first_name} ${engineer.last_name}`}
                        url={engineer.profile_url}
                      />
                      {["Kickoff", "Status", "Internal Kickoff"].includes(
                        this.state.type
                      ) && (
                        <Checkbox
                          isChecked={
                            this.state.meetingAttendees &&
                            this.state.meetingAttendees.filter(
                              (e) => e.attendee === engineer.id
                            ).length > 0
                          }
                          name="meetingAttendees"
                          onChange={(e) =>
                            this.handleChangeAttendeeCheck(e, engineer.id)
                          }
                          disabled={
                            this.state.view ||
                            engineer.id === undefined ||
                            engineer.id === null
                          }
                        />
                      )}
                    </div>
                  ))}
              </div>
              <div className="details-box">
                <div className="label-details">Account Manager</div>
                <div className="label-content">
                  {(this.state.accountManager &&
                    this.state.accountManager.id && (
                      <>
                        <UsershortInfo
                          name={`${this.state.accountManager.first_name} ${this.state.accountManager.last_name}`}
                          url={this.state.accountManager.profile_url}
                        />
                        {["Kickoff", "Status", "Internal Kickoff"].includes(
                          this.state.type
                        ) && (
                          <Checkbox
                            isChecked={
                              this.state.meetingAttendees &&
                              this.state.meetingAttendees.filter(
                                (e) =>
                                  e.attendee === this.state.accountManager.id
                              ).length > 0
                            }
                            name="meetingAttendees"
                            onChange={(e) =>
                              this.handleChangeAttendeeCheck(
                                e,
                                this.state.accountManager.id
                              )
                            }
                            disabled={
                              this.state.view ||
                              project.project_manager_id === undefined ||
                              project.project_manager_id === null
                            }
                          />
                        )}
                      </>
                    )) ||
                    "-"}
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Project Type</div>
                <div className="project-type">
                  <div className="label-content">{project.type}</div>
                </div>
              </div>
            </Accordian>
          </div>
        </div>
        <ModalBase
          show={this.state.closeMeeting}
          onClose={() =>
            this.setState({
              closeMeeting: false,
              ticketError: [],
              currentStatusList: [],
            })
          }
          titleElement={`Close Meeting`}
          bodyElement={
            <div className="col-md-12 body close-meeting-po-up">
              <div className="loader">
                <Spinner show={this.state.loading} />
              </div>
              <div className="time-entry-pm">
                <div className="box">
                  <Input
                    field={{
                      label: "Ticket",
                      type: InputFieldType.PICKLIST,
                      value: this.state.closeTicket.ticket_id,
                      options:
                        this.state.ticketList &&
                        this.state.ticketList.map((s) => ({
                          value: s.id,
                          label: `${s.id} - ${s.summary}`,
                        })),
                      isRequired: true,
                    }}
                    width={12}
                    multi={false}
                    name="ticket_id"
                    onChange={(e) => {
                      this.setState((prevState) => ({
                        closeTicket: {
                          ...prevState.closeTicket,
                          ticket_id: e.target.value,
                        },
                      }));
                    }}
                    placeholder={`Select Ticket`}
                    className="ticket-selection"
                    error={
                      !this.state.closeTicket.ticket_id && {
                        errorState: IValidationState.ERROR,
                        errorMessage: "Please Select Ticket",
                      }
                    }
                  />
                  <div className="date-time">
                    <Input
                      field={{
                        label: "Date",
                        type: InputFieldType.DATE,
                        value: this.state.closeTicket.pm_time_entry.start_time,
                        isRequired: false,
                      }}
                      width={4}
                      labelIcon={"info"}
                      name="start_time"
                      onChange={(e) => this.handleChangeStartClose(e)}
                      placeholder={`Select Date`}
                      showTime={false}
                      disablePrevioueDates={false}
                      disabled={this.state.view}
                    />
                    <div className="time-to-from">
                      <Input
                        field={{
                          label: "Start Time",
                          type: InputFieldType.DATE,
                          value: this.state.closeTicket.pm_time_entry
                            .start_time,
                          isRequired: false,
                        }}
                        width={4}
                        labelIcon={"info"}
                        name="start_time"
                        onChange={(e) => this.handleChangeStartClose(e)}
                        showTime={true}
                        showDate={false}
                        hideIcon={true}
                        disablePrevioueDates={false}
                        disabled={this.state.view}
                      />
                      <Input
                        field={{
                          label: "End Time",
                          type: InputFieldType.DATE,
                          value: this.state.closeTicket.pm_time_entry.end_time,
                          isRequired: false,
                        }}
                        width={4}
                        labelIcon={"info"}
                        name="end_time"
                        onChange={(e) => this.handleChangeStartClose(e)}
                        showTime={true}
                        showDate={false}
                        hideIcon={true}
                        className="to-time"
                        disablePrevioueDates={false}
                        disabled={this.state.view}
                      />
                    </div>
                  </div>
                  <Input
                    field={{
                      value: this.state.closeTicket.pm_time_entry.notes,
                      label: "Notes",
                      type: InputFieldType.TEXTAREA,
                      isRequired: false,
                    }}
                    width={12}
                    name="notes"
                    onChange={(e) => this.handleChangeNotes(e, false)}
                    placeholder="Enter notes"
                    className="notes-height-sm"
                  />
                </div>
                <div className="engineer-list-heading">
                  <div className="heading">Engineer Time Entry</div>
                </div>
                <div className="enginner-time-entry">
                  <Input
                    field={{
                      label: "Engineers",
                      type: InputFieldType.PICKLIST,
                      value: this.state.closeTicket.engineer_ids,
                      options: projectEngineers.map((s) => ({
                        value: s.id,
                        label: `${s.first_name} - ${s.last_name}`,
                      })),
                      isRequired: false,
                    }}
                    width={6}
                    multi={true}
                    name="engineer_ids"
                    onChange={(e) =>
                      this.setState((prevState) => ({
                        closeTicket: {
                          ...prevState.closeTicket,
                          engineer_ids: e.target.value,
                        },
                      }))
                    }
                    placeholder={`Select`}
                    clearable={true}
                  />
                  <Input
                    field={{
                      label: "Engineer Ticket",
                      type: InputFieldType.PICKLIST,
                      value: this.state.closeTicket.engineer_ticket_id,
                      options:
                        this.state.ticketList &&
                        this.state.ticketList.map((s) => ({
                          value: s.id,
                          label: `${s.id} - ${s.summary}`,
                        })),
                      isRequired: false,
                    }}
                    width={6}
                    multi={false}
                    name="engineer_ticket_id"
                    onChange={(e) => {
                      this.setState((prevState) => ({
                        closeTicket: {
                          ...prevState.closeTicket,
                          engineer_ticket_id: e.target.value,
                        },
                      }));
                    }}
                    placeholder={`Select Ticket`}
                    clearable={true}
                    className="ticket-selection"
                  />
                  <Input
                    field={{
                      label: "Start Time",
                      type: InputFieldType.DATE,
                      value: this.state.engineeringTimeEntry.start_time,
                      isRequired: false,
                    }}
                    width={6}
                    labelIcon={"info"}
                    name="start_time"
                    onChange={(e) => this.handleChangeEngineerStartClose(e)}
                    showTime={true}
                    showDate={false}
                    hideIcon={true}
                    disablePrevioueDates={false}
                    disabled={this.state.view}
                  />
                  <Input
                    field={{
                      label: "End Time",
                      type: InputFieldType.DATE,
                      value: this.state.engineeringTimeEntry.end_time,
                      isRequired: false,
                    }}
                    width={6}
                    labelIcon={"info"}
                    name="end_time"
                    onChange={(e) => this.handleChangeEngineerStartClose(e)}
                    showTime={true}
                    showDate={false}
                    hideIcon={true}
                    className="to-time"
                    disablePrevioueDates={false}
                    disabled={this.state.view}
                  />
                  <Input
                    field={{
                      value: this.state.engineeringTimeEntry.notes,
                      label: "Notes",
                      type: InputFieldType.TEXTAREA,
                      isRequired: false,
                    }}
                    width={12}
                    name="notes"
                    onChange={(e) => this.handleChangeNotes(e, true)}
                    placeholder="Enter notes"
                    className="notes-height-sm"
                  />
                </div>
                <div className="engineer-list-heading">
                  <div className="heading">Phase Completion</div>
                </div>
                <div className="enginner-time-entry">
                  <Input
                    field={{
                      label: "Mark Phase Complete",
                      type: InputFieldType.PICKLIST,
                      value: this.state.phase_to_complete,
                      options:
                        this.state.phases &&
                        this.state.phases.map((s) => ({
                          value: s.id,
                          label: `${s.phase && s.phase.title}`,
                        })),
                      isRequired: false,
                    }}
                    width={6}
                    multi={false}
                    name="phase_to_complete"
                    onChange={(e) => {
                      this.setState({
                        phase_to_complete: e.target.value,
                      });
                    }}
                    placeholder={`Select Ticket`}
                    className="ticket-selection"
                    clearable={true}
                  />
                  {this.state.phase_to_complete && (
                    <Input
                      field={{
                        value: this.state.estimated_completion_date,
                        label: "Estimated date of completion",
                        type: InputFieldType.DATE,
                        isRequired: false,
                      }}
                      width={6}
                      name="estimated_completion_date"
                      onChange={(e) => this.handleChange(e)}
                      placeholder="Select Date"
                      showTime={false}
                      showDate={true}
                      disablePrevioueDates={false}
                    />
                  )}
                </div>
              </div>
              {this.state.currentStatusList &&
                this.state.currentStatusList.length > 0 &&
                showCurrentStates(this.state.currentStatusList)}
              {this.state.ticketError &&
                showErrorDetail(this.state.ticketError)}
            </div>
          }
          footerElement={
            !this.state.onCloseMeetRedirect ? (
              <SquareButton
                content={"Save  & Close Meeting"}
                bsStyle={ButtonStyle.PRIMARY}
                onClick={(e) => this.onSaveClick()}
                className="save-close-meeting"
                disabled={this.disableSaveMeeting()}
              />
            ) : (
              <SquareButton
                content={"Go to Meeting Detail"}
                bsStyle={ButtonStyle.PRIMARY}
                onClick={(e) => {
                  const projectID = this.props.match.params.id;
                  const query = new URLSearchParams(this.props.location.search);
                  const AllProject =
                    query.get("AllProject") === "true" ? true : false;
                  this.props.history.push(
                    `/ProjectManagement/${projectID}?meeting=true&AllProject=${AllProject}`
                  );
                }}
                className="save-close-meeting"
                disabled={this.disableSaveMeeting()}
              />
            )
          }
          className="close-ticket-modal"
        />
      </div>
    );
  }
}

const showErrorDetail = (ticketError: object[]) => {
  const checkIsList = Array.isArray(ticketError);

  if (checkIsList) {
    return (
      <div className="ticket-error">
        {ticketError.length > 0 &&
          ticketError.map((x, i) => {
            return (
              <div className="block" key={i}>
                {Object.values(x)}
              </div>
            );
          })}
      </div>
    );
  } else {
    const error = JSON.stringify(ticketError);
    return (
      <div className="ticket-error">
        <div className="block">
          {error && error.substring(11, error.length - 2)}
        </div>
      </div>
    );
  }
};

const showCurrentStates = (list) => {
  return (
    <div className="operations-status-list">
      <div className="heading">Meeting Background Operations</div>
      <div className="block-status">
        <div className="name">Name</div>
        <div className="error">Details</div>
        <div className="result">Result</div>
      </div>
      {list.length > 0 &&
        list.map((x, i) => {
          return (
            <div
              key={i}
              className={`block-status ${x.result ? "success" : "error"}`}
            >
              <div className="name">{x.label}</div>
              <div className="erros-l">
                {x.error.map((y, ind) => {
                  return showErrorDetail(y);
                })}
              </div>
              <div className={`result ${x.result ? "Success" : "Error"}`}>
                {x.result ? "Success" : "Error"}
              </div>
            </div>
          );
        })}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getActivityStatusList: () => dispatch(getActivityStatusList()),
  getProjectPhasesByID: (projectId: number) =>
    dispatch(getProjectPhasesByID(projectId)),
  updateProjectPhasesStatusByID: (
    data: any,
    projectId: number,
    phaseId: number
  ) => dispatch(updateProjectPhasesStatusByID(data, projectId, phaseId)),
  getProjectTickets: (projectID: number) =>
    dispatch(getProjectTickets(projectID)),
  getProjectDetails: (projectID: number) =>
    dispatch(getProjectDetails(projectID)),
  getProjectTypes: () => dispatch(getProjectTypes()),
  getMeetingTemplates: (url: string) => dispatch(getMeetingTemplates(url)),
  postStatusMeeting: (projectID: number, data: any, api: string) =>
    dispatch(postStatusMeeting(projectID, data, api)),
  meetingComplete: (projectID: number, data: any, api: string) =>
    dispatch(meetingComplete(projectID, data, api)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getMeetingNotesByProjectID: (projectId: number, meetingId: number) =>
    dispatch(getMeetingNotesByProjectID(projectId, meetingId)),
  saveMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    note: string
  ) => dispatch(saveMeetingNotesByProjectID(projectId, meetingId, note)),
  updateMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    noteId: number,
    note: string
  ) =>
    dispatch(updateMeetingNotesByProjectID(projectId, meetingId, noteId, note)),
  getProjectCustomerContactsByID: (projectID: number) =>
    dispatch(getProjectCustomerContactsByID(projectID)),
  fetchAllCustomerUsers: (id: string) => dispatch(fetchAllCustomerUsers(id)),
  saveMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) => dispatch(saveMeetingAttendeesByProjectID(projectId, meetingId, data)),
  getMeetingAttendeesByProjectID: (projectId: number, meetingId: number) =>
    dispatch(getMeetingAttendeesByProjectID(projectId, meetingId)),
  getMeetingPDFPreview: (
    projectId: number,
    meetingId: number,
    completed: boolean,
    sent_to_cc: boolean
  ) =>
    dispatch(getMeetingPDFPreview(projectId, meetingId, completed, sent_to_cc)),
  deleteMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) =>
    dispatch(
      deleteMeetingAttendeesByProjectID(projectId, meetingId, attendeeId)
    ),
  getProjectEngineersByID: (projectID: number, type?: string) =>
    dispatch(getProjectEngineersByID(projectID, type)),
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  getContactRoles: () => dispatch(getContactRoles()),
  saveProjectCustomerContactsByID: (projectID: number, data: any) =>
    dispatch(saveProjectCustomerContactsByID(projectID, data)),
  getProjectAdditionalContactsByID: (projectId: number) =>
    dispatch(getProjectAdditionalContactsByID(projectId)),
  saveProjectAdditionalContactsByID: (projectId: number, data: any) =>
    dispatch(saveProjectAdditionalContactsByID(projectId, data)),
  saveExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) =>
    dispatch(
      saveExternalMeetingAttendeesByProjectID(projectId, meetingId, data)
    ),
  getExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number
  ) => dispatch(getExternalMeetingAttendeesByProjectID(projectId, meetingId)),
  deleteExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) =>
    dispatch(
      deleteExternalMeetingAttendeesByProjectID(
        projectId,
        meetingId,
        attendeeId
      )
    ),
  updateAdditionalContact: (projectId: number, contactId: number, data: any) =>
    dispatch(updateAdditionalContact(projectId, contactId, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectMeeting);

export const hasProjectMettingIDS = (project, meeting) => {
  if (project && project.id && meeting && meeting.id) {
    return true;
  } else {
    return false;
  }
};

export const getURLBYMeetingType = (type) => {
  let url = "";
  switch (type) {
    case "Status":
      url = "status-meetings";
      break;
    case "Customer Touch":
      url = "customertouch-meetings";
      break;
    case "Internal Kickoff":
      url = "kickoff-meetings";
      break;
    case "Kickoff":
      url = "kickoff-meetings";
      break;
    case "Close Out":
      url = "closeout-meetings";
      break;
    case "Closeout":
      url = "closeout-meetings";
      break;
    default:
      break;
  }
  return url;
};
