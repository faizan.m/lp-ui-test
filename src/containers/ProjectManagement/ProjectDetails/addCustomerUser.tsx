import { cloneDeep } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  createUserCustomerPU,
  PU_CU_CREATE_FAILURE,
  PU_CU_CREATE_SUCCESS
} from '../../../actions/customerUser';
import { fetchUserTypes } from '../../../actions/userType';
import SquareButton from '../../../components/Button/button';
import Input from '../../../components/Input/input';
import ModalBase from '../../../components/ModalBase/modalBase';
import Spinner from '../../../components/Spinner';
import { commonFunctions } from '../../../utils/commonFunctions';
import AppValidators from '../../../utils/validator';

class CustomerUserNew extends Component<IUserNewProps, IUserNewState> {
  roles = [{ value: '', label: '' }];
  statuses = [
    { value: 'true', label: 'Enabled' },
    { value: 'false', label: 'Disabled' },
  ];
  constructor(props: IUserNewProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    firstName: '',
    lastName: '',
    email: '',
    isFormValid: false,
    department: '',
    title: '',
    office_phone: '',
    country_code: '1',
    cellPhone: '',
    twitter: '',
    linkedin: '',
    isEnabled: 'false',
    role: this.roles[0].value,
    error: {
      firstName: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      lastName: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      email: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      role: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
    },
  });
  componentDidMount() {
    this.props.fetchUserTypes();
  }

  handleChange = (e: any): void => {
    const newState = { ...this.state };
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  submitRequest = () => {
    const {
      cellPhone,
      department,
      office_phone,
      email,
      firstName,
      lastName,
      linkedin,
      title,
      twitter,
      role,
      isEnabled,
      country_code,
    } = this.state;

    if (this.validateForm()) {
      const newUser = {
        email,
        first_name: firstName,
        title,
        last_name: lastName,
        role,
        is_active: isEnabled,
        user_role: parseInt(role, 10),
        profile: {
          department,
          title,
          office_phone: office_phone,
          cell_phone_number: cellPhone,
          country_code,
          twitter_profile_url: twitter,
          linkedin_profile_url: linkedin,
        },
      };
      this.props
        .createUserCustomer(this.props.customerId, newUser)
        .then(action => {
          if (action.type === PU_CU_CREATE_SUCCESS) {
            this.props.close(null);
          }
          if (action.type === PU_CU_CREATE_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
        });
    }
  };

  validateForm = () => {
    const error = this.getEmptyState().error;

    let isValid = true;
    if (!this.state.firstName) {
      error.firstName.errorState = IValidationState.ERROR;
      error.firstName.errorMessage = 'First Name cannot be empty';
      isValid = false;
    } else if (this.state.firstName && this.state.firstName.length > 300) {
      error.firstName.errorState = IValidationState.ERROR;
      error.firstName.errorMessage =
        'First Name should be less than 300 chars.';

      isValid = false;
    }
    if (!this.state.lastName) {
      error.lastName.errorState = IValidationState.ERROR;
      error.lastName.errorMessage = 'Last Name cannot be empty';
      isValid = false;
    } else if (this.state.lastName && this.state.lastName.length > 300) {
      error.lastName.errorState = IValidationState.ERROR;
      error.lastName.errorMessage = 'Last Name should be less than 300 chars.';

      isValid = false;
    }
    if (!this.state.email) {
      error.email.errorState = IValidationState.ERROR;
      error.email.errorMessage = 'Email cannot be empty';
      isValid = false;
    }
    if (this.state.email && !AppValidators.isValidEmail(this.state.email)) {
      error.email.errorState = IValidationState.ERROR;
      error.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }
    if (!this.state.role) {
      error.role.errorState = IValidationState.ERROR;
      error.role.errorMessage = 'Role cannot be empty.';
      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  transformUserTypes = () => {

    const userTypes = this.props.userTypes;
    if (userTypes) {
      const providerType = userTypes.find(
        userType => userType.user_type === 'provider'
      );

      return providerType.roles.map(role => ({
        value: role.id,
        label: role.display_name,
      }));
    }

    return [];
  };

  setValidationErrors = errorList => {
    const newState: IUserNewState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };
  renderBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isPostingUser} />
        </div>
        <div
          className={`new__body ${this.props.isPostingUser ? `loading` : ''}`}
        >
          <div className="row">
            <Input
              field={{
                label: 'First Name ',
                type: InputFieldType.TEXT,
                isRequired: true,
                value: `${this.state.firstName}`,
              }}
              error={this.state.error.firstName}
              width={6}
              name="firstName"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Last Name ',
                type: InputFieldType.TEXT,
                isRequired: true,
                value: `${this.state.lastName}`,
              }}
              error={this.state.error.lastName}
              width={6}
              name="lastName"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Email Address ',
                type: InputFieldType.TEXT,
                isRequired: true,
                value: `${this.state.email}`,
              }}
              error={this.state.error.email}
              width={6}
              name="email"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Role ',
                type: InputFieldType.PICKLIST,
                value: `${this.state.role}`,
                isRequired: true,
                options: this.transformUserTypes(),
              }}
              error={this.state.error.role}
              width={6}
              name="role"
              onChange={this.handleChange}
            />
          </div>
          <div />
        </div>
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isPostingUser ? `loading` : ''}`}>
        <SquareButton
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
          onClick={this.props.close}
        />
        <SquareButton
          content="Add"
          bsStyle={ButtonStyle.PRIMARY}
          onClick={this.submitRequest}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Add New User"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-new-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  createUserCustomer: (id: any, user: ICustomerUser) =>
    dispatch(createUserCustomerPU(id, user)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
});

const mapStateToProps = (state: IReduxStore) => ({
  userTypes: state.userType.userTypes,
  isPostingUser: state.customerUser.isPostingUser,
});
//TODO: Remove any
export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(CustomerUserNew);
