import React from "react";
import UsershortInfo from "../../../components/UserImage";

interface ISelectOptionsProps {
  children: any;
  className: string;
  isDisabled: boolean;
  isFocused: boolean;
  isSelected: boolean;
  onFocus: any;
  onSelect: any;
  option: any;
}
interface ISelectValueProps {
  children: any;
  placeholder: any;
  value: any;
}
export class SelectOptions extends React.Component<ISelectOptionsProps, {}> {
  handleMouseDown(event, _props) {
    event.preventDefault();
    event.stopPropagation();
    _props.onSelect(_props.option, event);
  }

  handleMouseEnter(event, _props) {
    _props.onFocus(_props.option, event);
  }

  handleMouseMove(event, _props) {
    if (_props.isFocused) return;
    _props.onFocus(_props.option, event);
  }

  render() {
    return (
      <div
        className={this.props.className}
        onMouseDown={(e) => this.handleMouseDown(e, this.props)}
        onMouseEnter={(e) => this.handleMouseEnter(e, this.props)}
        onMouseMove={(e) => this.handleMouseMove(e, this.props)}
        title={this.props.option.label}
      >
        <UsershortInfo
          name={this.props.children}
          url={this.props.option.profile_url}
          className={"inside-box"}
        />
      </div>
    );
  }
}

export class SelectValue extends React.Component<ISelectValueProps, {}> {
  render() {
    return (
      <div className="Select-value" title={this.props.value.title}>
        <span className="Select-value-label">
          <UsershortInfo
            name={this.props.value.label}
            url={this.props.value.profile_url}
            className="inside-box"
          />
        </span>
      </div>
    );
  }
}
