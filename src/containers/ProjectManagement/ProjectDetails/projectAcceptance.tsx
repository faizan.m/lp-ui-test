import _, { cloneDeep } from 'lodash';
import 'quill-mention';
import React, { Component } from "react";
import { Mention, MentionsInput } from 'react-mentions';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import showdown from 'showdown';
import { commonFunctions } from '../../../utils/commonFunctions';
import "./style.scss";

interface IProjectAcceptanceState {
  projectAcceptanceMailSubject: any;
  projectAcceptanceMailBodyText: any;
  projectAcceptanceMailBodyMarkDown: any;
  closeOutMeetingEmailTemplateMailSubject: string;
  showActionButtons: boolean;
  savedSuccess: boolean;
}

interface IProjectAcceptanceProps {
  defaultProjectAcceptance?: any;
  projectAcceptance: any;
  projectID: any;
  meetingID: any;
  setData: any;
  viewOnly?: boolean;
}

export default class ProjectAcceptance extends Component<
  IProjectAcceptanceProps,
  IProjectAcceptanceState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  customerMailBodyRef: any;
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    projectAcceptanceMailSubject: "",
    projectAcceptanceMailBodyMarkDown: "",
    projectAcceptanceMailBodyText: "",
    closeOutMeetingEmailTemplateMailSubject: "",
    showActionButtons: false,
    savedSuccess: false
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);

    },
  };

  componentDidMount() {
    const {projectAcceptance} = this.props;
    const replacedMeetingText = projectAcceptance.replacedProjectAcceptanceText;
    let parsedMarkdown = "";

    if (replacedMeetingText) {
      parsedMarkdown = this.convertTextToHTML(replacedMeetingText);
    } else {
      parsedMarkdown = projectAcceptance.projectAcceptanceMarkdown ? projectAcceptance.projectAcceptanceMarkdown : "";
    }

    this.setState({
      closeOutMeetingEmailTemplateMailSubject: projectAcceptance.emailSubject,
      projectAcceptanceMailBodyMarkDown: parsedMarkdown,
      projectAcceptanceMailBodyText: replacedMeetingText
    })
  }

  componentDidUpdate() {
    if (this.state.projectAcceptanceMailBodyMarkDown === "") {
      const replacedMeetingText = this.props.projectAcceptance.replacedProjectAcceptanceText;
  
      let parsedMarkdown = "";
  
      if (replacedMeetingText) {
        parsedMarkdown = this.convertTextToHTML(replacedMeetingText)
      } else {
        parsedMarkdown = this.props.projectAcceptance.projectAcceptanceMarkdown ? this.props.projectAcceptance.projectAcceptanceMarkdown : "";
      }
  
      this.setState({
        closeOutMeetingEmailTemplateMailSubject: this.props.projectAcceptance.emailSubject,
        projectAcceptanceMailBodyMarkDown: parsedMarkdown,
        projectAcceptanceMailBodyText: replacedMeetingText
      });
    }
  }
  

  convertTextToHTML = (replacedText: string) => {

    let formattedReplaceText = replacedText.replace(/(\r\n|\r|\n)/g, '<br>');
    formattedReplaceText = formattedReplaceText.replace(/<br\/>([^<])/g, "<br\/>\n\n$1");

    const converter = new showdown.Converter();
    const parsedMarkdown = converter.makeHtml(
      formattedReplaceText
    );

    return parsedMarkdown;
  };

  
  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangeMailSubject = e => {
    const newState = cloneDeep(this.state);
    (newState.closeOutMeetingEmailTemplateMailSubject as any) = e.target.value;
    this.setState({ ...newState, showActionButtons: true });
  };

  handleChange = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.projectAcceptanceMailBodyText as any) = this.getContentFromObject(object);
    (newState.projectAcceptanceMailBodyMarkDown as any) = content;
    (newState.savedSuccess as boolean) = false;
    this.setState({ ...newState, showActionButtons: true });
    this.props.setData && this.props.setData(newState)
  };

  render() {
    return (
        <div className="customer-contact-role col-md-12">
          <div className="col-md-12 row customer-email">
            <div>
              <label className="col-md-3 field__label-label email-subject" >
                <span><b>Email Subject</b></span>
              </label>
            </div>
            <div className="mail-subject-mention-input">
              <MentionsInput
                markup="[__display__]"
                value={this.state.closeOutMeetingEmailTemplateMailSubject || ''}
                onChange={(e) => this.handleChangeMailSubject(e)}
                className={'outer'}
              >
                <Mention
                  trigger="#"
                  data={commonFunctions.rangeVariables()}
                  className={'inner-drop'}
                  markup="#RULE#__display__"
                />
              </MentionsInput>
            </div>
          </div>
          <div className="col-md-12 row customer-email">
            <div className="project-acceptance-md-editor">
                <ReactQuill
                  ref={this.customerMailBodyRef}
                  value={this.state.projectAcceptanceMailBodyMarkDown}
                  onChange={this.handleChange}
                  modules={{ mention: this.mentionModule }}
                  readOnly={this.props.viewOnly}
                />
            </div>
          </div>
        </div>
    );
  }
}
