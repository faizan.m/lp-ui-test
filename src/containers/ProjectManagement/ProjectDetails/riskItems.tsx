import React from "react";

import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import Input from "../../../components/Input/input";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../components/Spinner";
import { getConvertedColorWithOpacity } from "../../../utils/CommonUtils";
import {
  ACTION_ITEM_SUCCESS,
  ACTION_ITEM_FAILURE,
  getActionItemList,
  postActionItem,
  deleteActionItem,
  getProjectStatusByProjectID,
  editActionItem,
  getTeamMembers,
  getProjectCustomerContactsByID,
  getImpactList,
  getProjectAdditionalContactsByID,
} from "../../../actions/pmo";
import SquareButton from "../../../components/Button/button";
import { fetchProviderUsers } from "../../../actions/provider/user";
import {
  dateSetTimeZero,
  fromISOStringToFormattedDate,
} from "../../../utils/CalendarUtil";
import moment from "moment";
import { SelectOptions, SelectValue } from "./userSelectOptionValue";
import { commonFunctions } from "../../../utils/commonFunctions";
import ReactQuill from "react-quill";

interface IActionItemProps {
  getActionItemList: any;
  project?: any;
  postActionItem: any;
  deleteActionItem: any;
  getProjectStatusByProjectID: any;
  fetchProviderUsers: any;
  providerUsers: any;
  api: any;
  user: any;
  editActionItem: any;
  viewOnly?: boolean;
  initialData?: any;
  getTeamMembers: any;
  getProjectCustomerContactsByID: any;
  getImpactList: any;
  getProjectAdditionalContactsByID: any;
}

interface IRiskItemListtate {
  actionItemList: any[];
  open: boolean;
  loading: boolean;
  statusList: any[];
  usersList: any[];
  impactList: any[];
  notes: string;
  sorting: string;
  accountManager: any;
}

class RiskItemList extends React.Component<
  IActionItemProps,
  IRiskItemListtate
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: IActionItemProps) {
    super(props);

    this.state = {
      actionItemList: [],
      statusList: [],
      usersList: [],
      open: true,
      loading: false,
      impactList: [],
      notes: "",
      sorting: "",
      accountManager: null,
    };
  }
  componentDidMount() {
    this.setState({ loading: true });
    if (this.props.viewOnly) {
      this.setState({ actionItemList: this.props.initialData || [] });
    } else {
      this.getActionItemList();
    }
    this.getProjectStatusByProjectID();
    this.getAccountManager();
    this.getTeamMembers();
    this.getImpactList();
  }

  getAccountManager = () => {
    this.props
      .getActionItemList(this.props.project.id, "account-manager")
      .then((action) => {
        const usersList = [...this.state.usersList];
        usersList.push({
          user_id: action.response.id,
          name: action.response.name,
          profile_url: action.response.profile_url,
        });
        this.setState({
          loading: false,
          accountManager: action.response,
          usersList,
        });
      });
  };

  getImpactList = () => {
    this.props.getImpactList().then((action) => {
      this.setState({ loading: false, impactList: action.response });
    });
  };
  getTeamMembers = () => {
    Promise.all([
      this.props.getActionItemList(this.props.project.id, "team-members"),
      this.props.getProjectCustomerContactsByID(this.props.project.id, {
        pagination: false,
      }),
      this.props.getProjectAdditionalContactsByID(this.props.project.id),
    ]).then(([teamMembers, CustomerUsers, resources]) => {
      const usersList = [
        ...teamMembers.response.map((t) => {
          return { ...t, user_id: t.id };
        }),
        ...CustomerUsers.response,
        ...resources.response.map((t) => {
          return { ...t, user_id: t.id };
        }),
      ];

      this.props.project.project_manager &&
        usersList.push({
          user_id: this.props.project.project_manager_id,
          name: this.props.project.project_manager,
          profile_url: this.props.project.project_manager_profile_pic,
        });
      this.state.accountManager &&
        usersList.push({
          user_id: this.state.accountManager.id,
          name: this.state.accountManager.name,
          profile_url: this.state.accountManager.profile_url,
        });

      this.setState({
        usersList,
      });
    });
  };
  getActionItemList = () => {
    this.props
      .getActionItemList(this.props.project.id, this.props.api)
      .then((action) => {
        this.setState({ loading: false, actionItemList: action.response });
      });
  };
  getProjectStatusByProjectID = () => {
    this.props
      .getProjectStatusByProjectID(this.props.project.id)
      .then((action) => {
        this.setState({ loading: false, statusList: action.response });
      });
  };

  addNew = () => {
    const newState = cloneDeep(this.state);
    const status = this.state.impactList && this.state.impactList[1];
    newState.actionItemList.push({
      description: "",
      owner: this.props.user && this.props.user.id,
      impact: status && status.id,
      created_on: moment(),
      edited: true,
    });

    this.setState(newState);
  };

  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    this.props
      .deleteActionItem(this.props.project.id, rowData.id, this.props.api)
      .then((action) => {
        if (action.type === ACTION_ITEM_SUCCESS) {
          this.getActionItemList();
        }
        if (action.type === ACTION_ITEM_FAILURE) {
          newState.actionItemList[index].descriptionError = `Couldn't delete`;
        }
        this.setState(newState);
      });
  };

  onSaveRowClick = (e, index, rowData) => {
    if (rowData.owner > 0) {
      rowData.resource = rowData.owner;
      rowData.owner = null;
    }
    const newState = cloneDeep(this.state);
    if (rowData.id) {
      this.props
        .editActionItem(this.props.project.id, rowData, this.props.api)
        .then((action) => {
          if (action.type === ACTION_ITEM_SUCCESS) {
            newState.actionItemList[index].edited = false;
            newState.actionItemList[index].descriptionError = "";
            newState.actionItemList[index] = action.response;
            // this.getActionItemList();
          }
          if (action.type === ACTION_ITEM_FAILURE) {
            newState.actionItemList[index].edited = true;
            newState.actionItemList[
              index
            ].descriptionError = action.errorList.data.description.join(" ");
          }
          this.setState(newState);
        });
    } else {
      this.props
        .postActionItem(this.props.project.id, rowData, this.props.api)
        .then((action) => {
          if (action.type === ACTION_ITEM_SUCCESS) {
            newState.actionItemList[index].edited = false;
            newState.actionItemList[index].descriptionError = "";
            newState.actionItemList[index] = action.response;
            // this.getActionItemList();
          }
          if (action.type === ACTION_ITEM_FAILURE) {
            newState.actionItemList[index].edited = true;
            newState.actionItemList[
              index
            ].descriptionError = action.errorList.data.description.join(" ");
          }
          this.setState(newState);
        });
    }
  };
  onEditRowClick = (e, index, rowData) => {
    const newState = cloneDeep(this.state);
    newState.actionItemList[index].edited = true;
    this.setState(newState);
  };
  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.actionItemList[index].edited = true;
    let value = e.target.value;
    newState.actionItemList[index][e.target.name] = value;
    this.setState(newState);
  };

  handleChangeMakrkdown = (content, index) => {
    const newState = cloneDeep(this.state);
    newState.actionItemList[index].edited = true;
    newState.actionItemList[index]["description"] = content;
    this.setState(newState);
  };

  onclickCompleted = (e, index) => {
    const inputValue = e.target.value || 0;
    const status = this.state.impactList.find((x) => x.id === inputValue);
    if (status.name === "Mitigated") {
      const newState = cloneDeep(this.state);
      newState.actionItemList[index].edited = true;
      newState.actionItemList[index][e.target.name] = inputValue;
      newState.actionItemList[index].showCompleted = true;
      newState.actionItemList[index].completed_on = dateSetTimeZero(new Date());
      this.setState(newState);
    } else {
      const newState = cloneDeep(this.state);
      newState.actionItemList[index].edited = true;
      newState.actionItemList[index][e.target.name] = inputValue;
      this.setState(newState);
    }
  };
  enableEdit = (e, index, row) => {
    if (!this.props.viewOnly && row.status_title !== "Completed") {
      const newState = cloneDeep(this.state);
      newState.actionItemList[index].edited = true;
      this.setState(newState);
    }
  };

  isValid = () => {
    const error = {
      subscription_category_id: { ...RiskItemList.emptyErrorState },
      device_category_row: { ...RiskItemList.emptyErrorState },
    };
    let isValid = true;

    if (this.state.actionItemList && this.state.actionItemList.length > 0) {
      this.state.actionItemList.map((e) => {
        if (!e.connectwise_shipper || !e.name) {
          error.device_category_row.errorState = IValidationState.ERROR;
          // tslint:disable-next-line:max-line-length
          error.device_category_row.errorMessage = `Please enter required fields.`;
          isValid = false;
        }
      });
    }

    return isValid;
  };

  handleChangeColor = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.actionItemList[index].owner = e.owner;
    this.setState(newState);
  };
  getActionItemData = () => {
    if (!this.state.actionItemList) {
      return null;
    }
  };

  handleNoteChange = (e, index) => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  setSort = (name) => {
    this.setState({
      sorting: this.state.sorting.includes("-") ? name : `-${name}`,
    });
  };
  getSortClass = (name) => {
    return (
      (this.state.sorting &&
        this.state.sorting.includes(name) &&
        (this.state.sorting.includes("-") ? "desc-order" : "asc-order")) ||
      ""
    );
  };

  render() {
    const actionItemList = this.state.actionItemList;

    return (
      <div className="project-details-action-items">
        {actionItemList && actionItemList.length > 0 && !this.state.loading && (
          <div className="col-md-12 status-row">
            <label
              className={`col-md-4 field__label-label ${this.getSortClass(
                "description"
              )}`}
              onClick={(e) => this.setSort("description")}
            >
              Item Name
            </label>
            <label
              className={`col-md-3 field__label-label ${this.getSortClass(
                "owner_name"
              )}`}
              onClick={(e) => this.setSort("owner_name")}
            >
              Owner
            </label>
            <label
              className={`col-md-2 field__label-label_date ${this.getSortClass(
                "created_on"
              )}`}
              onClick={(e) => this.setSort("created_on")}
            >
              Entered On
            </label>
            <label
              className={`col-md-2 field__label-label ${this.getSortClass(
                "impact"
              )}`}
              onClick={(e) => this.setSort("impact")}
            >
              Impact
            </label>
          </div>
        )}
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {actionItemList &&
          !this.state.loading &&
          actionItemList
            .sort((a, b) =>
              commonFunctions.sortList(a, b, "id", this.state.sorting)
            )
            .map((row, index) => {
              row.owner = row.owner || row.resource;
              return (
                <div
                  className={`col-md-12 status-row ${
                    row.edited ? "edited" : ""
                  }`}
                  key={index}
                >
                  {!row.edited && (
                    <div
                      className="description col-md-4 user ql-editor"
                      onClick={(e) => {
                        this.enableEdit(e, index, row);
                      }}
                      dangerouslySetInnerHTML={{ __html: row.description }}
                    >
                      {/* <SMPopUp
                    key={index}
                    className="item-notes"
                    onClickOk={e => {
                      const newState = cloneDeep(this.state);
                      newState.actionItemList[index]["notes"] = this.state.notes;
                      this.setState(newState, () => this.onSaveRowClick(e, index, newState.actionItemList[index]))
                    }}
                    isValidForm={true}
                    clickableSection={
                        <div >
                          <img
                            width="40px"
                            height="40px"
                            className={'d-pointer icon-remove edit-png'}
                            alt=""
                            src={`/assets/icons/comment-bubble${ row.notes ? '-highlight': ''}.svg`}
                            onClick={(e) => this.setState({ notes: row.notes })}
                            title={row.notes}
                          />
                        </div>
                    }
                  >
                    <div className="box">
                      <Input
                        field={{
                            label: 'Note',
                            type: InputFieldType.TEXTAREA,
                            value: this.state.notes,
                            isRequired: false,
                        }}
                        width={12}
                        multi={false}
                        name="notes"
                        onChange={e => { e.stopPropagation(); this.handleNoteChange(e, index)}}
                        placeholder={`Enter a note`}
                      />
                    </div>
                  </SMPopUp> */}
                    </div>
                  )}
                  {row.edited && (
                    <Input
                      field={{
                        label: "",
                        type: InputFieldType.CUSTOM,
                        value: "",
                        isRequired: false,
                      }}
                      width={4}
                      labelIcon={""}
                      name=""
                      customInput={
                        <ReactQuill
                          value={row.description || ""}
                          onChange={(e) => this.handleChangeMakrkdown(e, index)}
                          modules={{
                            toolbar: [
                              [{ list: "ordered" }, { list: "bullet" }],
                              // ["clean"], // remove formatting button
                            ],
                          }}
                          theme="snow"
                          className={`action-items-quill ${
                            row.descriptionError ? "ql-error" : ""
                          }`}
                        />
                      }
                      onChange={(e) => null}
                      placeholder={""}
                      error={
                        row.descriptionError && {
                          errorState: IValidationState.ERROR,
                          errorMessage: row.descriptionError,
                        }
                      }
                      disabled={this.props.viewOnly}
                    />
                  )}

                  <Input
                    field={{
                      label: "",
                      type: InputFieldType.PICKLIST,
                      value: row.owner ? row.owner : row.resource,
                      isRequired: false,
                      options:
                        this.state.usersList &&
                        this.state.usersList.map((user) => ({
                          value: user.user_id,
                          label: user.name,
                          profile_url: user.profile_url,
                        })),
                    }}
                    width={3}
                    labelIcon={"info"}
                    name="owner"
                    onChange={(e) => this.handleChangeList(e, index)}
                    placeholder={row.edited ? "Selet User" : ""}
                    className={`meeting-status action-item-user-dropdon ${
                      row.status_title === "Completed"
                        ? "pointer-events-none"
                        : ""
                    } `}
                    disabled={
                      row.is_archived ||
                      this.props.viewOnly ||
                      row.impact_name === "Mitigated"
                    }
                    optionComponent={SelectOptions}
                    valueComponent={SelectValue}
                  />
                  {row.edited && (
                    <Input
                      field={{
                        label: "",
                        type: InputFieldType.DATE,
                        isRequired: false,
                        value: row.created_on,
                      }}
                      width={2}
                      name="created_on"
                      onChange={(e) => this.handleChangeList(e, index)}
                      placeholder="Date"
                      showTime={false}
                      showDate={true}
                      disabled={false}
                      disablePrevioueDates={false}
                      className=""
                    />
                  )}
                  {!row.edited && (
                    <div
                      className="row-date col-md-2"
                      onClick={(e) => {
                        this.enableEdit(e, index, row);
                      }}
                    >
                      {row.created_on
                        ? fromISOStringToFormattedDate(
                            row.created_on,
                            "MMM DD, YYYY"
                          )
                        : "N.A."}
                    </div>
                  )}
                  <Input
                    field={{
                      label: "",
                      type: InputFieldType.PICKLIST,
                      value: row.impact,
                      isRequired: false,
                      options: this.state.impactList.map((s) => ({
                        value: s.id,
                        label: (
                          <div
                            className="field-section color-preview-option"
                            title={""}
                          >
                            <div
                              style={{
                                backgroundColor: s.color,
                                borderColor: s.color,
                              }}
                              className="left-column"
                            />
                            <div
                              style={{
                                background: `${getConvertedColorWithOpacity(
                                  s.color
                                )}`,
                              }}
                              className="text"
                            >
                              {s.name}
                            </div>
                          </div>
                        ),
                      })),
                    }}
                    width={2}
                    labelIcon={"info"}
                    name="impact"
                    onChange={(e) => this.onclickCompleted(e, index)}
                    placeholder={`Select impact`}
                    className="meeting-status action-item-status "
                    disabled={
                      row.is_archived ||
                      this.props.viewOnly ||
                      row.impact_name === "Mitigated"
                    }
                  />
                  {row.showCompleted && (
                    <Input
                      field={{
                        label: "Completed On",
                        type: InputFieldType.DATE,
                        isRequired: false,
                        value: row.completed_on,
                      }}
                      width={2}
                      name="completed_on"
                      onChange={(e) => this.handleChangeList(e, index)}
                      placeholder="Completed On"
                      showTime={false}
                      showDate={true}
                      disabled={false}
                      disablePrevioueDates={false}
                    />
                  )}
                  {!this.props.viewOnly && row.id && (
                    <SmallConfirmationBox
                      className="remove col-md-1"
                      onClickOk={() => this.onDeleteRowClick(index, row)}
                      text={"Item"}
                    />
                  )}
                  {(row.edited || !row.id) && (
                    <div className="row-action-btn">
                      <SquareButton
                        onClick={() => {
                          const newState = cloneDeep(this.state);
                          if (!row.id) {
                            newState.actionItemList.splice(index, 1);
                          } else {
                            this.getActionItemList();
                          }
                          this.setState(newState);
                        }}
                        content={"Cancel"}
                        bsStyle={ButtonStyle.DEFAULT}
                      />
                      <SquareButton
                        onClick={(e) => this.onSaveRowClick(e, index, row)}
                        content={"Apply"}
                        bsStyle={ButtonStyle.PRIMARY}
                        disabled={!row.owner || row.description.length < 12}
                      />
                    </div>
                  )}
                </div>
              );
            })}
        {actionItemList && actionItemList.length === 0 && !this.state.loading && (
          <div className="col-md-12 status-row risk-edited">
            <div className="no-data-action-item col-md-11">
              {" "}
              No items available
            </div>
          </div>
        )}
        {actionItemList &&
          this.state.actionItemList.filter((x) => !x.id).length === 0 &&
          !this.props.viewOnly && (
            <div className="col-md-12 status-row risk-edited">
              <div
                onClick={this.addNew}
                className="add-new-action-item col-md-11"
              >
                Add{" "}
                {this.props.api === "critical-path-items"
                  ? " Critical Path Item"
                  : " Risk"}
              </div>
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  providerUsers: state.providerUser.providerUsers,
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  editActionItem: (projectId: number, data: any, api: string) =>
    dispatch(editActionItem(projectId, data, api)),
  postActionItem: (projectId: number, data: any, api: string) =>
    dispatch(postActionItem(projectId, data, api)),
  deleteActionItem: (projectId: number, ItemId: any, api: string) =>
    dispatch(deleteActionItem(projectId, ItemId, api)),
  getProjectStatusByProjectID: (projectId: number) =>
    dispatch(getProjectStatusByProjectID(projectId)),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  getTeamMembers: (id: string) => dispatch(getTeamMembers(id)),
  getProjectCustomerContactsByID: (projectId: number) =>
    dispatch(getProjectCustomerContactsByID(projectId)),
  getImpactList: () => dispatch(getImpactList()),
  getProjectAdditionalContactsByID: (projectId: number) =>
    dispatch(getProjectAdditionalContactsByID(projectId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RiskItemList);
