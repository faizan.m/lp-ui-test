import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import SquareButton from '../../../components/Button/button';
import Checkbox from '../../../components/Checkbox/checkbox';
import SelectInput from '../../../components/Input/Select/select';

import './style.scss';

interface IProjectFiltersProps {
    show: boolean;
    setPopUpState: any;
    projectTypeOptions?: any;
    customerOptions?: any;
    projectManagerOptions?: any;
    activeProjectTypeFilter?: any;
    activeCustomerFilter?: any;
    activeProjectManagerFilter?: any;
    applyFilters: any;
    showAllClosedProjects: boolean;
}

interface IProjectFiltersState {
    list: any;
    showAllClosedProjects: boolean;
    activeFilterOption: string;
    fetchingCustomers: boolean;
    fetchingProjectTypes: boolean;
    fetchingProjectManagers: boolean;
    selectedProjectType: any;
    selectedProjectCustomer: any;
    selectedProjectManager: any;
    selectedProjectTypArr: any;
    selectedProjectCustomerArr: any;
    selectedProjectManagerArr: any;
}

class ProjectFilters extends React.Component<
    IProjectFiltersProps,
    IProjectFiltersState
> {
    constructor(props: IProjectFiltersProps) {
        super(props);
        this.state = this.getEmptyState();
    }

    getEmptyState = () => ({
        list: [],
        showAllClosedProjects: false,
        activeFilterOption: "projectType",
        fetchingCustomers: false,
        fetchingProjectTypes: false,
        fetchingProjectManagers: false,
        customers: [],
        projectTypes: [],
        projectManagers: [],
        selectedProjectType: undefined,
        selectedProjectCustomer: undefined,
        selectedProjectManager: undefined,
        selectedProjectTypArr: [],
        selectedProjectCustomerArr: [],
        selectedProjectManagerArr: [],
    });

    componentDidMount() {
        this.setState({
            showAllClosedProjects: this.props.showAllClosedProjects,
            selectedProjectCustomerArr: [...this.props.activeCustomerFilter],
            selectedProjectTypArr: [...this.props.activeProjectTypeFilter],
            selectedProjectManagerArr: [...this.props.activeProjectManagerFilter]
        });
    }

    onApplyClick = (e: any) => {
        e.preventDefault();
        this.props.setPopUpState(false);
        
        const typeStr = [];
        this.state.selectedProjectTypArr.forEach((value) => {
            this.props.projectTypeOptions.forEach((obj) => {
                if (obj.value === value) {
                    typeStr.push(obj.label)
                }
            });
        });

        const customerStr = [];
        this.state.selectedProjectCustomerArr.forEach((value) => {
            this.props.customerOptions.forEach((obj) => {
                if (obj.value === value) {
                    customerStr.push(obj.label)
                }
            });
        });
        
        const projectManagerStr = [];
        this.state.selectedProjectManagerArr.forEach((value) => {
            this.props.projectManagerOptions.forEach((obj) => {
                if (obj.value === value) {
                    projectManagerStr.push(obj.label)
                }
            });
        });

        this.props.applyFilters({
            showAllClosedProjects: this.state.showAllClosedProjects,
            projectType: this.state.selectedProjectTypArr,
            customer: this.state.selectedProjectCustomerArr,
            projectManager: this.state.selectedProjectManagerArr,
            typeStr: typeStr.join(),
            customerStr: customerStr.join(),
            projectManagerStr: projectManagerStr.join()
        });
    };

    onCancelClick = (e: any) => {
        e.preventDefault();
        this.props.setPopUpState(false);
    }

    handleChangeCheckBox = (e: any) => {
        e.stopPropagation();
        e.preventDefault();

        const newState = cloneDeep(this.state);

        (newState[e.target.name] as any) = e.target.checked;
        this.setState(newState);
    };

    handleProjectTypeChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectType: e.target.value,
            selectedProjectTypArr: [ ...prevState.selectedProjectTypArr, e.target.value ],
        }));
    }

    handleProjectCustomerChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectCustomer: e.target.value,
            selectedProjectCustomerArr: [ ...prevState.selectedProjectCustomerArr, e.target.value ],
        }));
    }

    handleProjectManagerChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectManager: e.target.value,
            selectedProjectManagerArr: [ ...prevState.selectedProjectManagerArr, e.target.value ],
        }));
    }

    removeFilter = (filter: any, type: string) => {
        if (type === "projectType") {
            this.setState({
                selectedProjectTypArr: this.state.selectedProjectTypArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "customer") {
            this.setState({
                selectedProjectCustomerArr: this.state.selectedProjectCustomerArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "projectManager") {
            this.setState({
                selectedProjectCustomerArr: this.state.selectedProjectCustomerArr.filter((val) => { 
                    return val !== filter
            })});
        }
    }

    getProjectTypeVal = (projectType) => {
        const type = this.props.projectTypeOptions.find(obj => obj.value == projectType)
        if (type) {
            return type.label
        }
        return ""
    };

    getCustomerVal = (customer) => {
        const type = this.props.customerOptions.find(obj => obj.value == customer)
        if (type) {
            return type.label
        }
        return ""
    }

    getProjectManagerVal = (manager) => {
        const type = this.props.projectManagerOptions.find(obj => obj.value == manager)
        if (type) {
            return type.label
        }
        return ""
    }

    render() {

        return (
            <div className="project-filters-container">
                <div className="header">
                    <div className="filter-heading">Filter By</div>
                    <Checkbox
                        isChecked={this.state.showAllClosedProjects}
                        name="showAllClosedProjects"
                        onChange={e => this.handleChangeCheckBox(e)}
                    >
                        Show all Closed Projects
                    </Checkbox>
                </div>
                <div className="body-container">
                    <div className="filter-left">
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "projectType" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'projectType'})}
                        >
                            <span className="filter-title">Project Type</span>
                                {
                                    this.state.selectedProjectTypArr.length > 0 &&
                                        <div className="selection-count">
                                            {`${this.state.selectedProjectTypArr.length} Selected`}
                                        </div>
                                }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "customer" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'customer'})}
                        >
                            <span className="filter-title">Customer</span>
                            {
                                this.state.selectedProjectCustomerArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedProjectCustomerArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "projectManager" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'projectManager'})}
                        >
                            <span className="filter-title">Project Manager</span>
                            {
                                this.state.selectedProjectManagerArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedProjectManagerArr.length} Selected`}
                                    </div>
                            }
                        </div>
                    </div>
                    <div className="filter-right">
                        {
                            this.state.activeFilterOption === "projectManager" ? 
                                this.renderProjectManagerFilter()
                            :
                            this.state.activeFilterOption === "customer" ? 
                                this.renderProjectCustomerFilter()
                            :
                                this.renderProjectTypeFilter()
                        }
                    </div>
                </div>
                <div className="action-footer">
                    <SquareButton
                        content="Cancel"
                        bsStyle={ButtonStyle.DEFAULT}
                        className="contact-cancel"
                        onClick={(e) => this.onCancelClick(e)}
                    />
                    <SquareButton
                        content="Apply"
                        bsStyle={ButtonStyle.PRIMARY}
                        className="contact-save"
                        onClick={(e) => this.onApplyClick(e)}
                    />
                </div>
            </div>
        );
    }

    renderProjectTypeFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectType"
                    value={this.state.selectedProjectType}
                    onChange={e => this.handleProjectTypeChange(e, "projectType")}
                    options={this.props.projectTypeOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Project Type"
                    disabled={false}
                    loading={this.state.fetchingProjectTypes}
                />
                {
                    this.state.selectedProjectTypArr.map((projectType) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getProjectTypeVal(projectType)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(projectType, "projectType")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderProjectCustomerFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectCustomer"
                    value={this.state.selectedProjectCustomer}
                    onChange={e => this.handleProjectCustomerChange(e, "customer")}
                    options={this.props.customerOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Customer"
                    disabled={false}
                    loading={this.state.fetchingCustomers}
                />
                {
                    this.state.selectedProjectCustomerArr.map((customer) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getCustomerVal(customer)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(customer, "customer")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderProjectManagerFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectType"
                    value={this.state.selectedProjectManager}
                    onChange={e => this.handleProjectManagerChange(e, "projectManager")}
                    options={this.props.projectManagerOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Project Manager"
                    disabled={false}
                    loading={this.state.fetchingProjectManagers}
                />
                {
                    this.state.selectedProjectManagerArr.map((projectManager) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getProjectManagerVal(projectManager)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(projectManager, "projectManager")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }
}

const mapStateToProps = (state: IReduxStore) => ({
});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectFilters);
