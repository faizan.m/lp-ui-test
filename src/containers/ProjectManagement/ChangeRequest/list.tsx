import { cloneDeep, debounce } from 'lodash';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { connect } from 'react-redux';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import {
  createChangeRequests, fetchChangeRequests, getActionItemList, GetAdditionalSetting, getProjectCustomerContactsByID, getProjectDetails, PROJECT_CUSTOMER_CONTACTS_SUCCESS, PROJECT_DETAILS_SUCCESS, UPDATE_CHANGE_REQ_SUCCESS, uploadProcessCR
} from '../../../actions/pmo';
import { downloadSOWDocumnet, DOWNLOAD_SOW_SUCCESS } from '../../../actions/sow';
import SquareButton from '../../../components/Button/button';
import DeleteButton from '../../../components/Button/deleteButton';
import Input from '../../../components/Input/input';
import ModalBase from '../../../components/ModalBase/modalBase';
import PDFViewer from '../../../components/PDFViewer/PDFViewer';
import Spinner from '../../../components/Spinner';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';
import Filters from './filters';
import './style.scss';

interface IChangeRequestProps extends ICommonProps {
  fetchChangeRequests: any;
  fetching: boolean;
  rules: any;
  additionalSetting: IAdditionSettingPMO;
  GetAdditionalSetting: any;
  getProjectDetails: any;
  getActionItemList: any;
  createChangeRequests: any;
  getProjectCustomerContactsByID: any;
  downloadSOWDocumnet: any;
  uploadProcessCR: any;
  addErrorMessage: any;
  addSuccessMessage: any;
}

interface IChangeRequestState {
  project?: any;
  list: any;
  currentPage: {
    pageType: PageType;
  };
  pagination: IScrollPaginationFilters;
  inputValue: string;
  noData: boolean;
  showAllChangeRequest?: boolean;
  groupBy: string;
  showSearch: boolean;
  showGroupBy: boolean;
  loading: boolean;
  showFiltersPop: boolean;
  activeChangeRequestTypeFilter: any;
  activeCustomerFilter: any;
  activeChangeRequestManagerFilter: any;
  projectTypeOptions?: any;
  customerOptions?: any;
  projectManagerOptions?: any;
  fetchingCustomers: boolean;
  fetchingChangeRequestTypes: boolean;
  fetchingChangeRequestManagers: boolean;
  showAllClosedChangeRequests: boolean;
  usersList: any;
  projectCustomerContacts: any;
  openPreview: boolean;
  previewPDFIds: number[];
  previewHTML: any;
  attachment: any;
  show: boolean;
  uploading: boolean;
  changeRequestId: number;
  downloadingIds: any;
}
enum PageType {
  ChangeRequests,
  AllChangeRequests,
}
const project = { name: '', pm: '', type: '', actual: 0, budget: 0, close_date: '', action_date: '', status: '' }

class PMOChangeRequests extends React.Component<
  any,
  IChangeRequestState
> {
  constructor(props: IChangeRequestProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 2000);
  }

  getEmptyState = () => ({
    list: [],
    currentPage: {
      pageType: PageType.ChangeRequests,
    },
    project,
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: '',
      search: '',
      type: '',
      customer: '',
      manager: '',
    },
    inputValue: '',
    noData: false,
    showSearch: false,
    showGroupBy: false,
    showAllChangeRequest: false,
    loading: true,
    groupBy: '',
    showFiltersPop: false,
    activeChangeRequestTypeFilter: [],
    activeCustomerFilter: [],
    activeChangeRequestManagerFilter: [],
    fetchingCustomers: false,
    fetchingChangeRequestTypes: false,
    fetchingChangeRequestManagers: false,
    projectTypeOptions: [],
    customerOptions: [],
    projectManagerOptions: [],
    showAllClosedChangeRequests: false,
    usersList: [],
    projectCustomerContacts: [],
    openPreview: false,
    previewPDFIds: [],
    previewHTML: null,
    attachment: null,
    uploading: false,
    show: false,
    changeRequestId: 0,
    downloadingIds: [],
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState(prevState => ({
      project: {
        ...prevState.project,
        id
      },
    }));
    this.getProjectDetails(id);
    const query = new URLSearchParams(this.props.location.search);
    const AllChangeRequest = query.get('AllChangeRequest') === 'true' ? true : false;
    this.setState({ showAllChangeRequest: AllChangeRequest })
    this.fetchMoreData(true)
    this.getTeamMembers();
    this.getProjectCustomerContactsByID(id);
  }
  getTeamMembers = () => {

    this.props.getActionItemList(this.props.match.params.id, 'team-members').then(teamMembers => {
      const usersList = teamMembers.response
      this.setState({ usersList })
    });
  }

  getProjectCustomerContactsByID = (id) => {
    this.setState({ loading: true })
    this.props.getProjectCustomerContactsByID(id).then(
      action => {
        if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
          this.setState({
            projectCustomerContacts: action.response,
          })
        }
      }
    );
  }
  getProjectDetails = (id) => {
    this.setState({ loading: true })
    this.props.getProjectDetails(id).then(
      action => {
        if (action.type === PROJECT_DETAILS_SUCCESS) {
          this.setState({
            project: action.response,
          })
        }
        this.setState({ loading: false })
      }
    );
  }

  onSearchStringChange = (e) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
    this.updateMessage(search);
  }


  updateMessage = search => {
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        search
      },
    }));
    this.fetchMoreData(true)
  };
  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
        type: this.state.pagination.type,
        customer: this.state.pagination.customer,
        manager: this.state.pagination.manager
      };
    }
    if (prevParams.nextPage !== null) {

      prevParams = {
        ...prevParams,
        customer: this.state.activeCustomerFilter.join(),
        type: this.state.activeChangeRequestTypeFilter.join(),
        status: this.state.showAllClosedChangeRequests ? 'closed' : 'all'
      }

      const projectID = this.props.match.params.id;
      this.props.fetchChangeRequests(projectID, prevParams)
        .then(action => {
          if (action.response) {
            let list = [];
            if (clearData) {
              list = [...action.response.results];
            } else {
              list = [...this.state.list, ...action.response.results];
            }
            const newPagination = {
              currentPage: action.response.links.page_number,
              nextPage: action.response.links.next_page_number,
              page_size: this.state.pagination.page_size,
              ordering: this.state.pagination.ordering,
              search: this.state.pagination.search,
              type: this.state.pagination.type,
              customer: this.state.pagination.customer,
              manager: this.state.pagination.manager,
            };

            this.setState({
              pagination: newPagination,
              list,
              noData: list.length === 0 ? true : false,
              loading: false,
            });
          } else {
            this.setState({
              noData: true,
              loading: false,
            });
          }
        })
    }
  };

  fetchByOrder = (field) => {
    let orderBy = '';

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field
        break;
      case field:
        orderBy = `-${field}`
        break;
      case '':
        orderBy = field
        break;

      default:
        orderBy = field
        break;
    }
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        ordering: orderBy
      },
    }), () => {
      this.fetchMoreData(true)
    });
  }
  getOrderClass = (field) => {
    let currentClassName = '';
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = 'asc-order'
        break;
      case field:
        currentClassName = 'desc-order'
        break;

      default:
        currentClassName = ''
        break;
    }
    return currentClassName;
  }

  goToDetails = (changeRequest) => {
    const query = new URLSearchParams(this.props.location.search);
    const AllProject = query.get('AllProject') === 'true' ? true : false;
    this.props.history.push(`/Projects/${this.props.match.params.id}/change-requests/${changeRequest.id}?&AllProject=${AllProject}`)
  }
  renderTableHeaderActions = () => {
    return (<div className="header-actions">
      <div className="left" >Change Request</div>
      <div className="right">
        {(this.state.activeChangeRequestTypeFilter.length > 0 ||
          this.state.activeCustomerFilter.length > 0 ||
          this.state.activeChangeRequestManagerFilter.length > 0
        ) &&
          <div className="top-bar-active-filter">
            {
              (this.state.activeChangeRequestTypeFilter.length > 0 ||
                this.state.activeCustomerFilter.length > 0 ||
                this.state.activeChangeRequestManagerFilter.length > 0
              ) &&
              <div
                className="clear-action"
                onClick={() => this.clearFilters()}
              >
                Clear All
              </div>
            }
            {
              this.state.pagination.customer !== "" &&
              <div className="applied-filter">Customer:
                <span className="filters">{this.state.pagination.customer}</span>
                <img
                  className="top-filter-delete"
                  alt=""
                  src={'/assets/icons/close.png'}
                  onClick={() => this.removeFilter("customer")}
                />
              </div>
            }
            {
              this.state.pagination.type !== "" &&
              <div className="applied-filter">ChangeRequest Type:
                <span className="filters">{this.state.pagination.type}</span>
                <img
                  className="top-filter-delete"
                  alt=""
                  src={'/assets/icons/close.png'}
                  onClick={() => this.removeFilter("type")}
                />
              </div>
            }
            {
              this.state.activeChangeRequestManagerFilter.length > 0 &&
              <div className="applied-filter">ChangeRequest Manager:
                <span className="filters">{this.state.pagination.manager}</span>
                <img
                  className="top-filter-delete"
                  alt=""
                  src={'/assets/icons/close.png'}
                  onClick={() => this.removeFilter("projectManager")}
                />
              </div>
            }
          </div>}
        {
          this.state.showFiltersPop &&
          <Filters
            show={this.state.showFiltersPop}
            showAllClosedProjects={this.state.showAllClosedChangeRequests}
            projectTypeOptions={this.state.projectTypeOptions}
            customerOptions={this.state.customerOptions}
            projectManagerOptions={this.state.projectManagerOptions}
            setPopUpState={(show) => this.setState({ showFiltersPop: show })}
            activeProjectTypeFilter={this.state.activeChangeRequestTypeFilter}
            activeCustomerFilter={this.state.activeCustomerFilter}
            activeProjectManagerFilter={this.state.activeChangeRequestManagerFilter}
            applyFilters={this.applyFilters}
          />
        }

        <Input
          field={{
            value: this.state.inputValue,
            label: '',
            type: InputFieldType.SEARCH,
          }}
          width={10}
          name="searchString"
          onChange={this.onSearchStringChange}
          onBlur={() => {
            if (this.state.inputValue.trim() === '') {
              this.setState({ showSearch: false })
            }
          }}
          placeholder="Search"
          className="search  search-change"
        />
        <span className="search-add-separator">|</span>
        <SquareButton
          content="Add"
          bsStyle={ButtonStyle.PRIMARY}
          onClick={() => {
            const projectID = this.props.match.params.id;
            const query = new URLSearchParams(this.props.location.search);
            const AllProject = query.get('AllProject') === 'true' ? true : false;
            const ChangeRequestID = 0;
            this.props.history.push(`/Projects/${projectID}/change-requests/${ChangeRequestID}?AllProject=${AllProject}`);
          }}
          className="header-action-btn"
        />
        {
          this.state.showGroupBy && (
            <Input
              field={{
                label: "",
                type: InputFieldType.PICKLIST,
                value: this.state.groupBy,
                isRequired: false,
                options: [
                  { 'label': 'ChangeRequest Type', value: 'type' },
                  { 'label': 'ChangeRequest Manager', value: 'project_manager' },
                  { 'label': 'ChangeRequest Status', value: 'overall_status' },
                  { 'label': 'Company', value: 'customer' }
                ]
              }}
              width={10}
              labelIcon={'info'}
              name="status"
              onChange={(e) => this.setState({ groupBy: e.target.value }, () => {
                this.fetchByOrder(e.target.value)
              })}
              placeholder={`Group By`}
              className="group-by"
              clearable={true}
              onBlur={() => {
                if (!this.state.groupBy) {
                  this.setState({ showGroupBy: false })
                }
              }}
            />)
        }
      </div>
    </div>)
  };
  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };

  applyFilters = (filters) => {
    this.setState({
      showAllClosedChangeRequests: filters.showAllClosedChangeRequests,
      activeChangeRequestTypeFilter: filters.projectType,
      activeCustomerFilter: filters.customer,
      activeChangeRequestManagerFilter: filters.projectManager,
      pagination: {
        ...this.state.pagination,
        type: filters.typeStr,
        customer: filters.customerStr,
        manager: filters.projectManagerStr
      }
    }, () => { this.fetchMoreData(true) });
  }

  removeFilter = (filterType: string) => {
    if (filterType === "customer") {
      this.setState({
        activeCustomerFilter: [],
        pagination: {
          ...this.state.pagination,
          customer: "",
        }
      }, () => { this.fetchMoreData(true) });
    }

    if (filterType === "type") {
      this.setState({
        activeChangeRequestTypeFilter: [],
        pagination: {
          ...this.state.pagination,
          type: "",
        }
      }, () => { this.fetchMoreData(true) });
    }

    if (filterType === "projectManager") {
      this.setState({
        activeChangeRequestManagerFilter: [],
        pagination: {
          ...this.state.pagination,
          manager: "",
        }
      }, () => { this.fetchMoreData(true) });
    }
  }

  clearFilters = () => {
    this.setState({
      activeChangeRequestTypeFilter: [],
      activeCustomerFilter: [],
      activeChangeRequestManagerFilter: [],
      showFiltersPop: false,
      pagination: {
        ...this.state.pagination,
        type: '',
        customer: '',
        manager: ''
      }
    }, () => { this.fetchMoreData(true) });
  }
  onSaveClick = (e: any, index) => {
    const newState = cloneDeep(this.state);
    (newState.list[index].status as any) = e.target.value;
    this.setState(newState, () => {
      this.props.createChangeRequests(this.props.match.params.id, this.state.list[index]).then(
        action => {
          if (action.type === UPDATE_CHANGE_REQ_SUCCESS) {
          }
        }
      );
    });
  };

  getStream = (doc: any) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, doc],
    });
    this.props.downloadSOWDocumnet(doc, 'pdf').then(action => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        const url = action.response.file_path;
        const link = document.createElement('a');
        link.href = url;
        link.target = '_blank';
        link.setAttribute('download', action.response.file_name);
        document.body.appendChild(link);
        link.click();
        this.setState({
          downloadingIds: this.state.downloadingIds.filter(id => doc !== id),
        });
      }
    });
  };
  changeRequestListingRows = (changeRequest, index) => {
    const getPDFPrevieMarkUp = sow_document => {
      if (this.state.previewPDFIds.includes(changeRequest.id)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_preview"
            title="Preview"
            onClick={e => this.onPDFPreviewClick(changeRequest, e)}
          />
        );
      }
    };
    
    const getDOCXDownloadMarkUp = cell => {
      if (this.state.downloadingIds.includes(cell)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_download"
            title="PDF Download"
            onClick={e => this.getStream(cell)}
          />
        );
      }
    };
   
    const requested = this.state.projectCustomerContacts && this.state.projectCustomerContacts.find(u => u.user_id === changeRequest.requested_by)
    const assigned_to = this.state.usersList && this.state.usersList.find(u => u.id === changeRequest.assigned_to)
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    return (
      <div
        className={`change-request-row`} key={index}>
        <div className="left-align first-column ellipsis-no-width" onClick={() => this.goToDetails(changeRequest)} title={changeRequest.name}> {changeRequest.change_request_type === 'Fixed Fee' && (
              <img
              className="icon-list-fixed-fee"
              title="Fixed Fee"
                alt=""
                src="/assets/icons/coin.svg"
              />
            )}{' '}
            {changeRequest.change_request_type === 'T & M' && (
              <img
                className="icon-list-tm"
                title={"T & M"}
                alt=""
                src="/assets/icons/timer.svg"
              />
            )}{changeRequest.name}</div>

        <div className="user-dropdown-list">
          {
            requested &&
            <>
              <div
                style={{
                  backgroundImage: `url(${requested && requested.profile_url
                    ? `${requested.profile_url}`
                    : '/assets/icons/user-filled-shape.svg'
                    })`,
                }}
                className="pm-image-circle"
              />
              <div className="type" title={`${requested.name}`}>{`${requested.name}`}
              </div>
            </>
          }
        </div>


        <div className="user-dropdown-list">
          {
            assigned_to &&
            <>
              <div
                style={{
                  backgroundImage: `url(${assigned_to && assigned_to.profile_url
                    ? `${assigned_to.profile_url}`
                    : '/assets/icons/user-filled-shape.svg'
                    })`,
                }}
                className="pm-image-circle"
              />
              <div className="type" title={`${assigned_to.first_name} ${assigned_to.last_name}`}>{`${assigned_to.first_name} ${assigned_to.last_name}`}
              </div>
            </>
          }
        </div>

        <div className={`left-align`}>{fromISOStringToFormattedDate(changeRequest.created_on, 'MMM DD, YYYY') || '-'}</div>
        <div className={`left-align`}>{fromISOStringToFormattedDate(changeRequest.updated_on, 'MMM DD, YYYY') || '-'}</div>
        <div className={`center-align`}>
          {formatter.format(
            (changeRequest &&
              changeRequest.json_config &&
              changeRequest.json_config.service_cost &&
              changeRequest.json_config.service_cost.customer_cost) ||
              0
          )}
        </div>
        <div className={`center-align`}>{changeRequest.change_number || 0}</div>
        <div className={`left-align `} onClick={() => {
          this.setState({ show: true, changeRequestId: changeRequest.id });
        }}>
          {
            changeRequest.status === 'In Progress' &&
            <SquareButton
              content={'Process CR'}
              bsStyle={ButtonStyle.PRIMARY}
              onClick={e => null}
              className="process-cr"
            />}</div>
        <Input
          field={{
            label: "",
            type: InputFieldType.PICKLIST,
            value: changeRequest.status,
            isRequired: false,
            options: ['In Progress', 'Cancelled/Rejected', 'Approved'].map(x => ({
              value: x,
              label: <div className={`label-user-dropdown ${x}`}>{x}</div>,
              disabled: x === 'Approved'
            })),
          }}
          width={12}
          name="status"
          onChange={(e) => this.onSaveClick(e, index)}
          placeholder={''}
          disabled={changeRequest.status === "Approved"}
          className={`list-selection`}
        />
        <div className="right-align" >
          {
            changeRequest.status === 'In Progress'  ?
              <img
                onClick={() => this.goToDetails(changeRequest)}
                className={'d-pointer edit-png'}
                alt=""
                src={'/assets/icons/edit.png'}
                title={'Edit in details'}
              />
              : <div className="blank-img" />
          }
          {getPDFPrevieMarkUp(changeRequest)}
          {getDOCXDownloadMarkUp(changeRequest.sow_document)}
        </div>

      </div>
    )
  }

  onPDFPreviewClick(changeRequest: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, changeRequest.id],
    });
    this.props.downloadSOWDocumnet(changeRequest.sow_document, 'pdf', '').then(a => {
      if (a.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          previewPDFIds: this.state.previewPDFIds.filter(
            id => changeRequest.id !== id
          ),
        });

      }
    });
  }
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };
  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.show as boolean) = e;
    this.setState(newState);
  };
  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    this.setState({ attachment })
  };
  onUploadClick = (e: any) => {
    const attachment: any = this.state.attachment;
    this.setState({ uploading: true })
    if (
      attachment
    ) {
      const data = new FormData();
      data.append('files', attachment);
      this.props.uploadProcessCR(this.props.match.params.id, this.state.changeRequestId, data).then(action => {
        if (action.type === 'PROCES_CR_UPLOAD_SUCCESS') {
          this.fetchMoreData(true)
          this.props.addSuccessMessage('Upload Successful!');
        }
        if (action.type === 'PROCES_CR_UPLOAD_FAILURE') {
          if (action.errorList.data && action.errorList.data.detail) {
            this.props.addErrorMessage(action.errorList.data.detail);
          } else this.props.addErrorMessage("Upload Failed!");
        }
        this.setState({ uploading: false, changeRequestId: 0, show: false })
      });
    } else {
      this.props.addErrorMessage('Invalid format.');
    }
  };
  render() {
    const project = this.state.project;
    return (
      <div className="change-request">
        <div className="header-change-request">
          <div className="left">
            <div className="name">{project.title} - {project.customer}</div>
          </div>
          <div className="right">
            <SquareButton
              content="Close"
              bsStyle={ButtonStyle.DEFAULT}
              onClick={() => {
                const query = new URLSearchParams(this.props.location.search);
                const AllProject = query.get('AllProject') === 'true' ? true : false;
                this.props.history.push(`/ProjectManagement/${this.props.match.params.id}?meeting=true&AllProject=${AllProject}`)
              }}
            />
          </div>
        </div>
        {this.renderTableHeaderActions()}
        <div className="change-request-list">
          <div className={`header`}>
            <div 
              className={`left-align first-column ${this.getOrderClass('name')}`}
              onClick={() => this.fetchByOrder('name')}
            >
              Type &amp; Name
            </div>
            <div className={`left-align`}> Requested By</div>
            <div className={`left-align`}> Assigned to</div>
            <div className={`left-align`}> Created</div>
            <div className={`left-align`}> Updated</div>
            <div className={`center-align`}>Amount</div>
            <div 
              className={`left-align ${this.getOrderClass('change_number')}`} 
              onClick={() => this.fetchByOrder('change_number')}
            >
              Number
            </div>
            <div className={`left-align `}> Process CR</div>
            <div className={`left-align`}>Status</div>
            <div />
          </div>
          <div id="scrollableDiv" style={{ height: '72vh', overflow: "auto", minHeight: '200px' }}>

            <InfiniteScroll
              dataLength={this.state.list.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
              style={{ minHeight: this.state.list.length > 0 ? '200px' : 'unset' }}
            >
              {!this.state.groupBy && this.state.list.map((project, index) => (
                this.changeRequestListingRows(project, index)
              ))}
              {!this.state.loading && this.state.pagination.nextPage && this.state.list.length > 0 && (
                <div className="load-more" onClick={() => {
                  this.fetchMoreData(false)
                }}>
                  load more data...
                </div>
              )}
            </InfiniteScroll>
            {
              this.state.noData && this.state.list.length === 0 &&
              <div className="no-data">No Change Requests Available</div>
            }
            {
              this.state.loading &&
              <div className="loader">
                <Spinner show={true} />
              </div>
            }
          </div>
          <PDFViewer
            show={this.state.openPreview}
            onClose={this.toggleOpenPreview}
            titleElement={`View SOW Preview`}
            previewHTML={this.state.previewHTML}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={ButtonStyle.DEFAULT}
                onClick={this.toggleOpenPreview}
              />
            }
            className=""
          />
          <ModalBase
            show={this.state.show}
            onClose={e => this.clearPopUp()}
            titleElement={'Change Request Processing'}
            bodyElement={
              <>
                <div className="loader">
                  <Spinner show={this.state.uploading} />
                </div>
                <Input
                  field={{
                    label: '',
                    type: InputFieldType.FILE,
                    value: `${this.state.attachment ? this.state.attachment : ''
                      }`,
                    id: 'attachment',
                  }}
                  width={9}
                  name=""
                  accept="*"
                  onChange={this.handleFile}
                  className="custom-file-input upload-btn"
                />
              </>
            }
            footerElement={
              <div className="col-md-12">
                {
                  <SquareButton
                    content={'Save'}
                    bsStyle={ButtonStyle.PRIMARY}
                    onClick={e => this.onUploadClick(e)}
                    className=""
                    disabled={!this.state.attachment}
                  />
                }
                <SquareButton
                  content={'Close'}
                  bsStyle={ButtonStyle.DEFAULT}
                  onClick={e => this.clearPopUp(!this.state.show)}
                  className=""
                />

              </div>
            } className={`confirm ${this.props.className}`}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  rules: state.configuration.rules,
  isFetching: state.configuration.isFetching,
  additionalSetting: state.pmo.additionalSetting,
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchChangeRequests: (id, params?: IScrollPaginationFilters) => dispatch(fetchChangeRequests(id, params)),
  GetAdditionalSetting: () => dispatch(GetAdditionalSetting()),
  getProjectDetails: (projectID: number) => dispatch(getProjectDetails(projectID)),
  getActionItemList: (projectId: number, api: string) => dispatch(getActionItemList(projectId, api)),
  createChangeRequests: (projectID: number, data: any) =>
    dispatch(createChangeRequests(projectID, data)),
  getProjectCustomerContactsByID: (projectID: number) => dispatch(getProjectCustomerContactsByID(projectID)),
  downloadSOWDocumnet: (id: any, type: string, name: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
  uploadProcessCR: (id: any, CRID: string, data: any) =>
    dispatch(uploadProcessCR(id, CRID, data)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PMOChangeRequests);
