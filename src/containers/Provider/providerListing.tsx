import { debounce } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import Provider from './provider';

import { fetchProviders, fetchTimezones } from '../../actions/provider';
import { allowPermission } from '../../utils/permissions';

import './style.scss';

interface IProviderTableRow {
  id: number;
  name: string;
  url: string;
  address: string;
  status: string;
  is_two_fa_enabled: boolean;
}

interface IProviderListingState {
  isCreateEditModalOpen: boolean;
  rows: IProviderTableRow[];
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
}

interface IProviderListingProps extends ICommonProps {
  providers: IProviderPaginated;
  fetchProviders: TFetchProviderList;
  isProvidersFetching: boolean;
  fetchTimezones: any;
  timezones: any[];
}

class ProviderListing extends React.Component<
  IProviderListingProps,
  IProviderListingState
> {
  static columns: any = [
    {
      accessor: 'name',
      Header: 'Provider Name',
      id: 'name',
      Cell: name => <div className="pl-15">{name.value}</div>,
    },
    { accessor: 'url', Header: 'URL', id: 'url' },
    { accessor: 'address', Header: 'Address', id: 'address__address_1' },
    {
      accessor: 'status',
      Header: 'Status',
      id: 'is_active',
      Cell: status => (
        <div className={`status status--${status.value}`}>{status.value}</div>
      ),
    },
    {
      accessor: 'is_two_fa_enabled',
      Header: 'Two-Factor-Authentication',
      sortable: false,
      id: 'data',
      Cell: status => (
        <div
          className={`status status--${status.value ? 'Enabled' : 'Disabled'}`}
        >
          {status.value === true ? 'Enabled' : 'Disabled'}
        </div>
      ),
    },
  ];
  private debouncedFetch;

  constructor(props: IProviderListingProps) {
    super(props);

    this.state = {
      isCreateEditModalOpen: false,
      rows: [],
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
    };
    this.debouncedFetch = debounce(this.fetchData, 500);
  }
  componentDidMount() {
    if(!this.props.timezones || (this.props.timezones && this.props.timezones.length === 0)){
      this.props.fetchTimezones();
    }
  }

  componentDidUpdate(prevProps: IProviderListingProps) {
    if (this.props.providers && prevProps.providers !== this.props.providers) {
      this.setRows(this.props);
    }
  }

  setRows = (nextProps: IProviderListingProps) => {
    const providersResponse = nextProps.providers;
    const providers: IProvider[] = providersResponse.results;
    const rows: IProviderTableRow[] = providers.map((provider, index) => ({
      id: provider.id,
      name: provider.name,
      url: provider.url,
      address: provider.address.address_1,
      status: provider.is_active ? 'Enabled' : 'Disabled',
      is_two_fa_enabled: provider.is_two_fa_enabled,
    }));

    this.setState(prevState => ({
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: providersResponse.count,
        currentPage: providersResponse.links.page_number - 1,
        totalPages: Math.ceil(
          providersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  toggleCreateEditModal = () => {
    this.setState(prevState => ({
      isCreateEditModalOpen: !prevState.isCreateEditModalOpen,
    }));
  };

  showProviderDetails = rowInfo => {
    if (allowPermission('view_provider')) {
      this.props.history.push(`/providers/details/${rowInfo.original.id}`);
    }
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchProviders(newParams);
  };

  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({ page: 1 });
  };

  renderTopBar = () => {
    return (
      <div className="superusers-listing__actions">
        <Input
          field={{
            value: this.state.pagination.params.search,
            label: '',
            type: InputFieldType.SEARCH,
          }}
          width={6}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search"
          className="superusers-listing__actions-search"
        />
        {allowPermission('create_provider') && (
          <SquareButton
            onClick={this.toggleCreateEditModal}
            content="&#43; Create a Provider"
            bsStyle={ButtonStyle.PRIMARY}
          />
        )}
      </div>
    );
  };

  render() {
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };

    return (
      <div className="providers-listing superusers-listing">
        <h3>Providers Listing</h3>
        <div className="loader">
          <Spinner show={this.props.isProvidersFetching} />
        </div>
        <Table
          columns={ProviderListing.columns}
          rows={this.state.rows}
          customTopBar={this.renderTopBar()}
          className={`superusers-listing__table ${
            this.props.isProvidersFetching ? 'loading' : ''
          }`}
          onRowClick={this.showProviderDetails}
          manualProps={manualProps}
          loading={this.props.isProvidersFetching}
        />
        <Provider
          show={this.state.isCreateEditModalOpen}
          onClose={this.toggleCreateEditModal}
          fetchData={this.fetchData}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  providers: state.provider ? state.provider.providers : [],
  isProvidersFetching: state.provider.isProvidersFetching,
  timezones: state.provider.timezones,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProviders: (params?: IServerPaginationParams) =>
    dispatch(fetchProviders(params)),
    fetchTimezones: () => dispatch(fetchTimezones()),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProviderListing)
);
