import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  getAccessFeaturesList,
  getFeatureAccessUsers,
  setFeatureAccessUsers,
  ACCESS_FEATURE_USERS_SUCCESS,
} from "../../actions/providerAdminUser";
import { addSuccessMessage } from "../../actions/appState";
import { fetchProviderUsersForAdmin } from "../../actions/provider/user";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import "./style.scss";

interface FeatureAccessProps {
  providerId: number;
  features: string[];
  providerUsers: ISuperUser[];
  getFeatureList: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  getFeatureAccessUsers: () => Promise<any>;
  fetchProviderUsers: (id: number) => Promise<any>;
  setFeatureAccessUsers: (payload: IFeatureAccess[]) => Promise<any>;
}

interface FeatureUserObj {
  [featureName: string]: string[];
}

const FeatureAccess: React.FC<FeatureAccessProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [usersForCurrentProvider, setUsersForCurrentProvider] = useState<
    FeatureUserObj
  >({});
  const [usersForOtherProviders, setUsersForOtherProviders] = useState<
    FeatureUserObj
  >({});

  const providerUserOptions: IPickListOptions[] = useMemo(
    () =>
      props.providerUsers
        ? props.providerUsers.map((author: ISuperUser) => ({
            value: author.id,
            label: `${author.first_name} ${author.last_name}`,
          }))
        : [],
    [props.providerUsers]
  );

  useEffect(() => {
    props.getFeatureList();
    props.fetchProviderUsers(props.providerId);
  }, [props.providerId]);

  useEffect(() => {
    if (props.features && providerUserOptions.length) {
      fetchUsersWhoHaveAccess(
        props.features,
        providerUserOptions.map((el) => el.value)
      );
    }
  }, [props.features, providerUserOptions]);

  const fetchUsersWhoHaveAccess = (
    features: string[],
    providerUsers: string[]
  ) => {
    setLoading(true);
    props
      .getFeatureAccessUsers()
      .then((action) => {
        if (action.type === ACCESS_FEATURE_USERS_SUCCESS) {
          let otherProviderUsers: FeatureUserObj = {};
          let currentProviderUsers: FeatureUserObj = {};
          const currentUsers: Set<string> = new Set(providerUsers);
          features.forEach((feature) => {
            currentProviderUsers[feature] = [];
            otherProviderUsers[feature] = [];
          });
          action.response.forEach((el: IFeatureAccess) => {
            if (currentUsers.has(el.user))
              currentProviderUsers[el.feature].push(el.user);
            else otherProviderUsers[el.feature].push(el.user);
          });
          setUsersForCurrentProvider(currentProviderUsers);
          setUsersForOtherProviders(otherProviderUsers);
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChange = (
    e: { target: { name: string; value: string[] } },
    featureName: string
  ) => {
    setUsersForCurrentProvider((prevState) => ({
      ...prevState,
      [featureName]: e.target.value as string[],
    }));
  };

  const generatePayload = (): IFeatureAccess[] => {
    // This function merges the users of current provider which may be changed
    // and users of other providers which were kept in state
    return props.features.map((featureName) => ({
      feature: featureName,
      enabled: true,
      users: [
        ...usersForCurrentProvider[featureName],
        ...usersForOtherProviders[featureName],
      ],
    }));
  };

  const onSubmit = () => {
    const payload = generatePayload();
    props.setFeatureAccessUsers(payload).then((action) => {
      if (action.type === ACCESS_FEATURE_USERS_SUCCESS) {
        props.addSuccessMessage("Changes saved successfully!");
      }
    });
  };

  return (
    <div className="feature-access-container">
      <h5>Feature Access</h5>
      <div className="feature-access-row">
        {props.features &&
          props.features.map((featureName, idx) => (
            <Input
              field={{
                label: featureName,
                type: InputFieldType.PICKLIST,
                value: usersForCurrentProvider[featureName],
                options: providerUserOptions,
                isRequired: false,
              }}
              key={idx}
              width={10}
              multi={true}
              loading={loading}
              name={featureName}
              placeholder="Select User"
              onChange={(e) => handleChange(e, featureName)}
            />
          ))}
      </div>
      <SquareButton
        onClick={onSubmit}
        content={"Save"}
        className="save-feat-acc-btn"
        bsStyle={ButtonStyle.PRIMARY}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  features: state.providerAdminUser.accessFeatures,
  providerUsers: state.providerUser.providerUsersAll,
});

const mapDispatchToProps = (dispatch: any) => ({
  getFeatureList: () => dispatch(getAccessFeaturesList()),
  getFeatureAccessUsers: () => dispatch(getFeatureAccessUsers()),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  fetchProviderUsers: (id: number) => dispatch(fetchProviderUsersForAdmin(id)),
  setFeatureAccessUsers: (payload: IFeatureAccess[]) =>
    dispatch(setFeatureAccessUsers(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FeatureAccess);
