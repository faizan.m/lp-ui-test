import React from "react";
import moment from "moment";
import InfiniteListing from "../../../components/InfiniteList/infiniteList";

interface WonOpportunityProps {
  customerCrmId: number;
}

const WonOpportunity: React.FC<WonOpportunityProps> = (props) => {
  const columns: IColumnInfinite<{
    id: number;
    name: string;
    closed_date: string;
    customer_crm_id: number;
    customer_name: string;
  }>[] = [
    {
      name: "Opportunity",
      id: "name",
      ordering: "name",
      className: "opp-name-trunc",
    },
    {
      name: "Date",
      id: "closed_date",
      ordering: "closed_date",
      className: "width-25",
      Cell: (c) => moment(c.closed_date).format("MM/DD/YY"),
    },
  ];

  return (
    <>
      <div className="client-left-table client-table">
        <div className="client-left-table-header">
          <h3>Opportunities Won Last 90 Days</h3>
        </div>
        {props.customerCrmId ? (
          <InfiniteListing
            url={`providers/client360/won-opp-chart?customer_crm_id=${props.customerCrmId}`}
            key={props.customerCrmId}
            columns={columns}
            showSearch={false}
            id="WonOpportunity"
            height={"calc(100% - 23px)"}
            className="won-opp-listing"
          />
        ) : (
          <div className="loading-data">Loading...</div>
        )}
      </div>
    </>
  );
};

export default WonOpportunity;
