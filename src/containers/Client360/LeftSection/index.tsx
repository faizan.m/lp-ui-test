import React, { useMemo } from "react";
import { connect } from "react-redux";
import OpenActivities from "./openActivities";
import WonOpp from "./wonOpp";
import OpenOppChart from "./openOppChart";

interface OppActivityProps {
  customerId: number;
  customers: ICustomerShort[];
}

const OppActivity: React.FC<OppActivityProps> = (props) => {
  const customerCrmId: number = useMemo(
    () =>
      props.customers && props.customers.length
        ? props.customers.find((el) => el.id === props.customerId).crm_id
        : null,
    [props.customers, props.customerId]
  );
  return (
    <div className="client-opp-activity-container">
      <OpenOppChart customerCrmId={customerCrmId}/>
      <WonOpp customerCrmId={customerCrmId}/>
      <OpenActivities />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customerId: Number(state.customer.customerId),
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(OppActivity);
