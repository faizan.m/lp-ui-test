import React from "react";
import InfiniteListing from "../../../components/InfiniteList/infiniteList";

interface ManagedTicketsProps {
  customerCrmId: number;
}

const ManagedTickets: React.FC<ManagedTicketsProps> = (props) => {
  const columns: IColumnInfinite<{
    id: number;
    summary: string;
    status_name: string;
    status_crm_id: number;
    ticket_crm_id: number;
    customer_name: string;
    customer_crm_id: number;
  }>[] = [
    {
      name: "Ticket #",
      id: "ticket_crm_id",
      ordering: "ticket_crm_id",
      className: "ticket-no-width",
    },
    {
      name: "Status",
      id: "status_name",
      ordering: "status_name",
      className: "width-20",
    },
    {
      name: "Description",
      id: "summary",
    },
  ];

  return (
    <div className="managed-tickets-outer-div">
      <h3 className="client-bottom-subheading">Managed Tickets</h3>
      <div className="client-bottom-table client-table">
        <InfiniteListing
          url={`providers/client360/open-service-tickets?customer_crm_id=${props.customerCrmId}`}
          key={props.customerCrmId}
          columns={columns}
          showSearch={false}
          id="ManagedTickets"
          height={"calc(100% - 23px)"}
          className="managed-tickets-listing"
        />
      </div>
    </div>
  );
};

export default ManagedTickets;
