import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  client360SettingCRU,
  CLIENT_360_SETTINGS_CRU_SUCCESS,
} from "../../actions/setting";
import Spinner from "../../components/Spinner";
import SquareButton from "../../components/Button/button";
import OppActivity from "./LeftSection";
import MainSection from "./MainSection";
import ClientServices from "./BottomSection";
import "./style.scss";

interface Client360Props extends ICommonProps {
  customerId: number;
  fetchSettings: () => Promise<any>;
}

const Client360: React.FC<Client360Props> = (props) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [configuredSettings, setConfigured] = useState<boolean>(false);

  useEffect(() => {
    props
      .fetchSettings()
      .then((action) => {
        if (action.type === CLIENT_360_SETTINGS_CRU_SUCCESS) {
          setConfigured(true);
        }
      })
      .finally(() => setLoading(false));
  }, []);

  const commonProps: ICommonProps = useMemo(
    () => ({
      match: props.match,
      history: props.history,
      location: props.location,
    }),
    [props.match, props.history, props.location]
  );

  return configuredSettings ? (
    props.customerId && !loading ? (
      <article className="client-360-container">
        <section className="client-top-section">
          <OppActivity />
          <MainSection {...commonProps} />
        </section>
        <section className="client-bottom-section">
          <h2 id="client-bottom-heading">Services</h2>
          <ClientServices {...commonProps} />
        </section>
      </article>
    ) : null
  ) : (
    <>
      <Spinner show={loading} className="client-360-loader" />
      {!loading && (
        <div className="error-boundary-container configure-client-setting">
          <h2>Please configure the Client 360 Setting</h2>
          <div className="eb-button-container">
            <SquareButton
              onClick={() => props.history.push("/setting/customer?tab=Client360")}
              content="Go to Settings"
              bsStyle={ButtonStyle.PRIMARY}
              className="reload-btn"
            />
          </div>
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customerId: Number(state.customer.customerId),
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSettings: () => dispatch(client360SettingCRU(HTTPMethods.GET)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Client360);
