import React from "react";
import { connect } from "react-redux";
import {
  addSite,
  ADD_SITE_SUCCESS,
  fetchCountriesAndStates,
} from "../../actions/inventory";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import AppValidators from "../../utils/validator";
interface ICreateSiteState {
  site: ISite;
  error: {
    name: IFieldValidation;
    address_line_1: IFieldValidation;
    address_line_2: IFieldValidation;
    city: IFieldValidation;
    state_id: IFieldValidation;
    country_id: IFieldValidation;
    zip: IFieldValidation;
    phone_number: IFieldValidation;
  };
}

interface ICreateSiteProps {
  countries: any;
  site?: ISite;
  customerId?: number;
  isVisible?: boolean;
  isFetching: boolean;
  close?: (e: any) => void;
  isFetchingCountries: boolean;
  onUpdate?: (site: ISite) => void;
  fetchCountriesAndStates: () => Promise<any>;
  handleEditPartnerSite?: (site: ISite) => void;
  addSite: (customerId: number, newSite: ISite) => Promise<any>;
}

class CreateSite extends React.Component<ICreateSiteProps, ICreateSiteState> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: ICreateSiteProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    site: {
      name: "",
      address_line_1: "",
      address_line_2: "",
      city: "",
      zip: "",
    },
    error: {
      name: { ...CreateSite.emptyErrorState },
      address_line_1: { ...CreateSite.emptyErrorState },
      address_line_2: { ...CreateSite.emptyErrorState },
      city: { ...CreateSite.emptyErrorState },
      state_id: { ...CreateSite.emptyErrorState },
      country_id: { ...CreateSite.emptyErrorState },
      zip: { ...CreateSite.emptyErrorState },
      phone_number: { ...CreateSite.emptyErrorState },
    },
  });

  componentDidMount() {
    this.props.fetchCountriesAndStates();
    if (this.props.site) {
      this.setState({ site: this.props.site });
    }
  }

  getCountries = (): IPickListOptions[] => {
    if (this.props.countries && this.props.countries.length > 0) {
      return this.props.countries.map((country) => ({
        value: country.country_id,
        label: country.country,
      }));
    } else {
      return [];
    }
  };

  getStates = (): IPickListOptions[] => {
    const countries = this.props.countries;
    if (countries && this.state.site.country_id) {
      const country = countries.find(
        (c) => c.country_id === this.state.site.country_id
      );

      return (
        country &&
        country.states &&
        country.states.map((state) => ({
          value: state.state_id,
          label: state.state,
        }))
      );
    }

    return [];
  };

  renderAddSite = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div
          className={`add-site__body ${this.props.isFetching ? `loading` : ""}`}
        >
          <Input
            field={{
              label: "Name",
              type: InputFieldType.TEXT,
              value: this.state.site.name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Site Name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Address Line 1",
              type: InputFieldType.TEXT,
              value: this.state.site.address_line_1,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Address Line 1"
            error={this.state.error.address_line_1}
            name="address_line_1"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Address Line 2",
              type: InputFieldType.TEXT,
              value: this.state.site.address_line_2,
            }}
            width={6}
            placeholder="Enter Address Line 2"
            error={this.state.error.address_line_2}
            name="address_line_2"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Phone Number",
              type: InputFieldType.TEXT,
              value: this.state.site.phone_number,
            }}
            error={this.state.error.phone_number}
            width={6}
            placeholder="Enter Phone Number"
            name="phone_number"
            onChange={this.handleChange}
          />

          <Input
            field={{
              value: this.state.site.country_id,
              label: "Country",
              type: InputFieldType.PICKLIST,
              isRequired: true,
              options: this.getCountries(),
            }}
            width={6}
            name="country_id"
            onChange={this.handleChangeCountry}
            placeholder="Select country"
            loading={this.props.isFetchingCountries}
            error={this.state.error.country_id}
          />
          <Input
            field={{
              value: this.state.site.state_id,
              label: "State",
              type: InputFieldType.PICKLIST,
              isRequired: false,
              options: this.getStates(),
            }}
            width={6}
            name="state_id"
            onChange={this.handleChangeState}
            placeholder="Select State"
            error={this.state.error.state_id}
            loading={this.props.isFetchingCountries}
            disabled={!this.state.site.country_id}
          />
          <Input
            field={{
              label: "City",
              type: InputFieldType.TEXT,
              value: this.state.site.city,
            }}
            width={6}
            placeholder="Enter City"
            error={this.state.error.city}
            name="city"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Zip Code",
              type: InputFieldType.TEXT,
              value: this.state.site.zip,
            }}
            width={6}
            placeholder="Enter Zip Code"
            error={this.state.error.zip}
            name="zip"
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  };

  handleChangeCountry = (event: React.ChangeEvent<HTMLInputElement>) => {
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      site: {
        ...prevState.site,
        country_id: Number(targetValue),
        state_id: null,
        state: "",
      },
    }));
  };

  handleChangeState = (event: any) => {
    this.setState((prevState) => ({
      site: {
        ...prevState.site,
        state_id: event.target.value,
        state: event.target.label,
      },
    }));
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    this.setState((prevState) => ({
      site: {
        ...prevState.site,
        [targetName]: targetValue,
      },
    }));
  };

  onSiteAdd = () => {
    const customerId = this.props.customerId;
    if (this.isValid() && customerId) {
      const site = { ...this.state.site };
      if (!site.state_id) {
        delete site.state_id;
      }
      if (this.props.site) {
        this.props.onUpdate(site);
      } else
        this.props.addSite(customerId, site).then((action) => {
          if (action.type === ADD_SITE_SUCCESS) {
            this.props.close(true);
          }
        });
    }
    if (this.isValid() && !customerId) {
      const sitePayload: ISite = {
        ...this.state.site,
      };
      sitePayload.state_crm_id = sitePayload.state_id
        ? Number(sitePayload.state_id)
        : null;
      sitePayload.country_crm_id = Number(sitePayload.country_id);
      sitePayload.country = this.getCountries().find(
        (el) => el.value == sitePayload.country_id
      ).label as string;
      sitePayload.address_line_2 = sitePayload.address_line_2
        ? sitePayload.address_line_2
        : null;
      sitePayload.city = sitePayload.city ? sitePayload.city : null;
      sitePayload.state = sitePayload.state ? sitePayload.state : null;
      sitePayload.zip = sitePayload.zip ? sitePayload.zip : null;
      sitePayload.phone_number = sitePayload.phone_number
        ? sitePayload.phone_number
        : null;
      delete sitePayload.state_id;
      delete sitePayload.country_id;
      this.props.handleEditPartnerSite(sitePayload);
    }
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.site.name || this.state.site.name.length === 0) {
      error.name.errorState = IValidationState.ERROR;
      error.name.errorMessage = "Enter a valid site name";

      isValid = false;
    }

    if (!this.state.site.country_id) {
      error.country_id.errorState = IValidationState.ERROR;
      error.country_id.errorMessage = "Select country ";

      isValid = false;
    }

    if (this.getStates().length !== 0 && !this.state.site.state_id) {
      error.state_id.errorState = IValidationState.ERROR;
      error.state_id.errorMessage = "Select State";

      isValid = false;
    }
    if (
      this.state.site.phone_number &&
      !AppValidators.isPhoneNumber(this.state.site.phone_number)
    ) {
      error.phone_number.errorState = IValidationState.ERROR;
      error.phone_number.errorMessage = "Enter a valid phone number";

      isValid = false;
    }
    if (!this.state.site.address_line_1.trim()) {
      error.address_line_1.errorState = IValidationState.ERROR;
      error.address_line_1.errorMessage = "This field is required";

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isFetching ? `loading` : ""}`}>
        <SquareButton
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
          onClick={this.props.close}
          disabled={this.props.isFetching}
        />
        <SquareButton
          content={this.props.site ? "Update" : "Add"}
          bsStyle={ButtonStyle.PRIMARY}
          onClick={this.onSiteAdd}
          disabled={this.props.isFetching}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement={this.props.site ? "Update Site" : "Add New Site"}
        bodyElement={this.renderAddSite()}
        footerElement={this.renderFooter()}
        className={"create-site-modal"}
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  addSite: (customerId: number, newSite: ISite) =>
    dispatch(addSite(customerId, newSite)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
});

const mapStateToProps = (state: IReduxStore) => ({
  countries: state.inventory.countries,
  isFetching: state.inventory.isFetching,
  isFetchingCountries: state.inventory.isCountriesFetching,
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateSite);
