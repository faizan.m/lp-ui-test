import cloneDeep from 'lodash/cloneDeep';
import React from 'react';
import { connect } from 'react-redux';
import TagsInput from 'react-tagsinput';
import { editServiceTypeSTT, EDIT_SERVICE_TYPE_SUCCESS, fetchServiceTypeSTT } from '../../actions/setting';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import './styles.scss';
interface ICreateTechnologyState {
  serviceType: any;
  category: any;
  types: any;
  error: {
    category: IFieldValidation;
    types: IFieldValidation;
  };
}

interface ICreateTechnologyProps {
  isFetchingType?: boolean;
  serviceType?: any;
  editServiceTypeSTT?: any;
  fetchServiceTypeSTT?: any;
  close?: (e: any) => void;
  isVisible?: boolean;
  customerId?: any;
}
class CreateTechnology extends React.Component<ICreateTechnologyProps, ICreateTechnologyState> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };
  constructor(props: ICreateTechnologyProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    serviceType: [],
    category: null,
    types: [],
    error: {
      category: CreateTechnology.emptyErrorState,
      types: CreateTechnology.emptyErrorState,
    },
  });

  componentDidMount() {
    this.props.fetchServiceTypeSTT();
  }

  componentDidUpdate(prevProps: ICreateTechnologyProps) {
    if (this.props.serviceType && prevProps.serviceType !== this.props.serviceType) {
      const serviceType = this.props.serviceType;
      serviceType.forEach((type, index) => {
        if (!Array.isArray(serviceType[index].technology_types_list)) {
          serviceType[index].technology_types_list = [];
        }
        type.technology_types.forEach((techType, i) => {
          if (techType.is_disabled !== true) {
            serviceType[index].technology_types_list.push(techType.name);
          }
        });
      });
      this.setState({
        serviceType,
      });
    }
  }

  renderBody = () => {
    return (
      <div className="create-technoloy">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingType} />
        </div>
        <div
          className={`box ${this.props.isFetchingType ? `loading` : ''}`}
        >
          <Input
            field={{
              label: 'Category',
              type: InputFieldType.PICKLIST,
              value: this.state.category,
              isRequired: true,
              options: this.props.serviceType.map(cat => {
                return ({
                  label: cat.name,
                  value: cat.id
                })
              }),
            }}
            error={this.state.error.category}
            width={8}
            placeholder="Select Category"
            name="category"
            onChange={e => this.handleChange(e)}
          />
          <div className="category col-md-12">
            <div className="category-inner">
              <div className="heading"> Types</div>
              <TagsInput
                value={this.state.types}
                onChange={e => this.handleChangeTypes(e)}
                inputProps={{
                  className: 'react-tagsinput-input',
                  placeholder: 'Enter Type',
                }}
                addOnBlur={true}
              />
            </div>
          </div>
          <div />
        </div>
      </div>
    );
  };
  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    (newState.category as any) = event.target.value;
    (newState.types as any) = this.props.serviceType.
      find(ele => ele.id === event.target.value).technology_types.
      filter(t => t.is_disabled !== true)
      .map(t => t.name);
    this.setState(newState);
  };
  handleChangeTypes = (event: any) => {
    const newState = cloneDeep(this.state);
    const list = event.filter(
      (value, index, self) => self.indexOf(value) === index
    );
    (newState.types as any) = list;
    this.setState(newState);
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isFetchingType ? `loading` : ''}`}>
        <SquareButton
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
          onClick={this.props.close}
        />
        <SquareButton
          content="Add"
          bsStyle={ButtonStyle.PRIMARY}
          onClick={this.onSaveServiceType}
        />
      </div>
    );
  };

  onSaveServiceType = () => {
    const technology_types = [];
    this.state.types.map((techType, i) =>
      technology_types.push({ name: techType })
    );
    const serviceType = {
      technology_types,
      id: this.state.category,
    };
    this.props.editServiceTypeSTT(serviceType).then(action => {
      if (action.type === EDIT_SERVICE_TYPE_SUCCESS) {
        this.props.close(true);
      }
    });
  };


  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Add New Technology"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-new-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  fetchServiceTypeSTT: () => dispatch(fetchServiceTypeSTT()),
  editServiceTypeSTT: (data: any) => dispatch(editServiceTypeSTT(data)),
});

const mapStateToProps = (state: IReduxStore) => ({
  isFetchingType: state.setting.isFetchingType,
  serviceType: state.setting.serviceType,
});
export default connect<any, any>
  (mapStateToProps, mapDispatchToProps)(CreateTechnology);
