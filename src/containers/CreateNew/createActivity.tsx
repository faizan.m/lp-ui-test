import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import { cloneDeep } from "lodash";
import {
  FETCH_ALL_CUST_USERS_SUCCESS,
  fetchAllCustomerUsers,
} from "../../actions/documentation";
import {
  SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
  getSalesActivityStatusSetting,
  SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
  getSalesActivityPrioritySetting,
  postActivityStatus,
  CREATE_SALES_ACTIVITY_SUCCESS,
  CREATE_SALES_ACTIVITY_FAILURE,
} from "../../actions/sales";
import {
  CREATE_QUOTES_SUCCESS,
  createQuote,
  getQuoteTypeList,
} from "../../actions/sow";
import { fetchQuoteDashboardListingPU } from "../../actions/dashboard";
import { fetchProviderUsers } from "../../actions/provider/user";
import {
  fetchCustomersShort,
  FETCH_CUSTOMERS_SHORT_SUCCESS,
} from "../../actions/customer";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import Checkbox from "../../components/Checkbox/checkbox";
import Spinner from "../../components/Spinner";
import CustomerUserNew from "../SOW/Sow/addUser";
import AddQuote from "../SOW/Sow/addQuote";

import "./styles.scss";

import { addSuccessMessage } from "../../actions/appState";
import { commonFunctions } from "../../utils/commonFunctions";
import {
  getQuoteAllStages,
  GET_QUOTE_STAGES_SUCCESS,
} from "../../actions/setting";
import ModalBase from "../../components/ModalBase/modalBase";

interface IActivityDetailProps {
  close: (e: any) => void;
  isVisible: boolean;
  customer: any;
  showPM?: boolean;
  postStatusMeeting?: any;
  addSuccessMessage?: any;
  addErrorMessage?: any;
  fetchAllCustomerUsers?: any;
  fetchCustomersShort?: any;
  providerUsers?: any;
  user?: any;
  fetchProviderUsers?: any;
  getSalesActivityStatusSetting?: any;
  getSalesActivityPrioritySetting?: any;
  postActivityStatus?: any;
  updateSalesActivity?: any;
  isFetchingUsers?: boolean;
  qTypeList?: any[];
  isFetchingQStageList?: boolean;
  isFetchingQTypeList?: boolean;
  createQuote?: any;
  quoteList?: any;
  quoteFetching?: any;
  fetchQuoteDashboardListing?: any;
  getQuoteTypeList?: any;
  getQuoteStages?: any;
  description: string;
}

interface IActivityDetailState {
  list: any;
  loading: boolean;
  customerUsers: any;
  customerOptions: any;
  fetchProviderUsers: any;
  providerUsers: any[];
  data: any;
  fetchingCustomers: boolean;
  isFetchingCustomerContacts: boolean;
  fetchingStatusSettings: boolean;
  fetchingPrioritySettings: boolean;
  statusSettingOptions: any;
  prioritySettingOptions: any;
  fetchingCustomersUsers: boolean;
  error: {
    description: IFieldValidation;
    priority: IFieldValidation;
    customer: IFieldValidation;
    customer_contact: IFieldValidation;
    notes: IFieldValidation;
    status: IFieldValidation;
    due_date: IFieldValidation;
    assigned_to: IFieldValidation;
    activity_type: IFieldValidation;
  };
  disableSaveClick: boolean;
  isCreateUserModal: boolean;
  isCreateOpportunityModal: boolean;
  quote_id: any;
  quote: any;
  errorList?: any;
  account_manager_name: string;
  stages: any;
}
class ActivityDetail extends React.Component<
  IActivityDetailProps,
  IActivityDetailState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: IActivityDetailProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    list: [],
    loading: false,
    data: {
      description: "",
      priority: null,
      customer: "",
      customer_contact: "",
      notes: "",
      status: "",
      due_date: "",
      assigned_to: "",
      activity_type: [],
      opportunity_id: null,
    },
    error: {
      description: { ...ActivityDetail.emptyErrorState },
      priority: { ...ActivityDetail.emptyErrorState },
      customer: { ...ActivityDetail.emptyErrorState },
      customer_contact: { ...ActivityDetail.emptyErrorState },
      notes: { ...ActivityDetail.emptyErrorState },
      status: { ...ActivityDetail.emptyErrorState },
      due_date: { ...ActivityDetail.emptyErrorState },
      assigned_to: { ...ActivityDetail.emptyErrorState },
      activity_type: { ...ActivityDetail.emptyErrorState },
      opportunity_id: { ...ActivityDetail.emptyErrorState },
    },
    customerUsers: [],
    customerOptions: [],
    providerUsers: [],
    statusSettingOptions: [],
    prioritySettingOptions: [],
    fetchProviderUsers: false,
    isFetchingCustomerContacts: false,
    fetchingCustomers: false,
    fetchingStatusSettings: false,
    fetchingPrioritySettings: false,
    fetchingCustomersUsers: false,
    disableSaveClick: false,
    isCreateUserModal: false,
    isCreateOpportunityModal: false,
    quote: {},
    quote_id: undefined,
    account_manager_name: "-",
    stages: [],
  });

  componentDidMount() {
    this.setState({
      data: {
        ...this.state.data,
        assigned_to: this.props.user ? this.props.user.id : "",
        due_date: moment().add(7, "days"),
        description: this.props.description,
        customer: this.props.customer,
      },
    });

    this.getAllCustomerUsers(this.props.customer);
    this.props.fetchQuoteDashboardListing(this.props.customer, true);
    this.props.fetchProviderUsers({ pagination: false });
    this.getAllCustomers();
    this.getStatusSettings();
    this.getPrioritySettings();
    this.props.getQuoteTypeList();
    this.props.getQuoteStages().then((action) => {
      if (action.type === GET_QUOTE_STAGES_SUCCESS) {
        this.setState({
          stages: action.response.map((stage) => ({
            value: stage.id,
            label: stage.label,
          })),
        });
      }
    });
  }

  getStatusSettings = () => {
    this.setState({ fetchingStatusSettings: true });
    this.props.getSalesActivityStatusSetting().then((action) => {
      if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
        const statusSettingOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));

        this.setState({ statusSettingOptions });
        const defaultStatusId = action.response
          .filter((val) => {
            return val.is_default;
          })
          .map((obj) => {
            return obj.id;
          });

        this.setState({
          statusSettingOptions,
          data: {
            ...this.state.data,
            status: defaultStatusId.length > 0 ? defaultStatusId[0] : "",
          },
        });
      }
      this.setState({ fetchingStatusSettings: false });
    });
  };

  getPrioritySettings = () => {
    this.setState({ fetchingPrioritySettings: true });
    this.props.getSalesActivityPrioritySetting().then((action) => {
      if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS) {
        const prioritySettingOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));

        this.setState({ prioritySettingOptions });

        const defaultPriorityId = action.response
          .filter((val) => {
            return val.is_default;
          })
          .map((obj) => {
            return obj.id;
          });

        this.setState({
          data: {
            ...this.state.data,
            priority: defaultPriorityId.length > 0 ? defaultPriorityId[0] : "",
          },
        });
      }
      this.setState({ fetchingPrioritySettings: false });
    });
  };

  getAllCustomers = () => {
    this.setState({ fetchingCustomers: true });
    this.props.fetchCustomersShort().then((action) => {
      if (action.type === FETCH_CUSTOMERS_SHORT_SUCCESS) {
        const customerOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.name}`,
          crm_id: data.crm_id,
          account_manager_name: data.account_manager_name,
        }));
        this.setState({ customerOptions });
      }
      this.setState({ fetchingCustomers: false });
    });
  };

  getAllCustomerUsers = (customerID) => {
    this.setState({ fetchingCustomersUsers: true });
    this.props.fetchAllCustomerUsers(customerID).then((action) => {
      if (action.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const customerUsers = action.response.map((data) => ({
          value: data.id,
          label: `${data.first_name} ${data.last_name}`,
        }));
        this.setState({ customerUsers });
      }
      this.setState({ fetchingCustomersUsers: false });
    });
  };

  handleDataChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.data[event.target.name] = event.target.value;
    newState.error[event.target.name].errorState = IValidationState.SUCCESS;
    newState.error[event.target.name].errorMessage = "";
    this.setState(newState);
  };

  handleCustomerChange = (event: any) => {
    const newState = cloneDeep(this.state);
    const customerObj = this.state.customerOptions.filter(
      (customer) => event.target.value === customer.value
    );

    newState.data[event.target.name] = event.target.value;
    (newState as any).fetchingCustomersUsers = true;
    newState.error[event.target.name].errorState = IValidationState.SUCCESS;
    newState.error[event.target.name].errorMessage = "";

    (newState as any).account_manager_name =
      customerObj.length > 0 && customerObj[0].account_manager_name;
    this.getAllCustomerUsers(event.target.value);
    this.props.fetchQuoteDashboardListing(event.target.value, true);
    this.setState(newState);
  };

  handleActivityTypeChange = (e, type) => {
    const isChecked = e.target.checked;

    let updatedActivityType = [...this.state.data.activity_type];

    if (isChecked) {
      updatedActivityType = [...updatedActivityType, type];
    } else {
      updatedActivityType = updatedActivityType.filter((value) => {
        return value !== type;
      });
    }

    this.setState({
      data: {
        ...this.state.data,
        activity_type: updatedActivityType,
      },
    });
  };

  setValidationErrors = (errorList) => {
    const newState: IActivityDetailState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  validateForm() {
    const newState: IActivityDetailState = cloneDeep(this.state);
    let isValid = true;

    if (
      !this.state.data.description ||
      this.state.data.description.trim().length === 0
    ) {
      newState.error.description.errorState = IValidationState.ERROR;
      newState.error.description.errorMessage = "Description cannot be empty";
      isValid = false;
    }

    if (!this.state.data.priority) {
      newState.error.priority.errorState = IValidationState.ERROR;
      newState.error.priority.errorMessage = "Please select a priority";
      isValid = false;
    }

    if (!this.state.data.status) {
      newState.error.status.errorState = IValidationState.ERROR;
      newState.error.status.errorMessage = "Please select a status";
      isValid = false;
    }

    if (!this.state.data.customer) {
      newState.error.customer.errorState = IValidationState.ERROR;
      newState.error.customer.errorMessage = "Please select a customer";
      isValid = false;
    }

    if (!this.state.data.assigned_to) {
      newState.error.assigned_to.errorState = IValidationState.ERROR;
      newState.error.assigned_to.errorMessage = "Please select an Assignee";
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  onSaveClick = (e) => {
    this.setState({ loading: true, disableSaveClick: true });

    const { data } = this.state;
    data.customer = this.props.customer;

    if (!this.validateForm()) return;

    data.due_date = data.due_date.format("YYYY-MM-DD");
    this.props.postActivityStatus(data).then((action) => {
      if (action.type === CREATE_SALES_ACTIVITY_SUCCESS) {
        this.props.addSuccessMessage(`Activity created successfully.`);
        this.props.close(e);
      }
      if (action.type === CREATE_SALES_ACTIVITY_FAILURE) {
        this.setValidationErrors(action.errorList.data);
      }
      this.setState({ loading: false, disableSaveClick: false });
    });
  };

  toggleCreateUserModal = () => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
  };

  closeUserModal = (action: any) => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
    if (action === null) this.getAllCustomerUsers(this.state.data.customer);
  };

  closeCreateOpportunityModal = (action: any) => {
    this.setState((prevState) => ({
      isCreateOpportunityModal: !prevState.isCreateOpportunityModal,
    }));
    if (action === null)
      this.props.fetchQuoteDashboardListing(this.props.customer, true);
  };

  toggleCreateOpportunityModal = () => {
    this.setState((prevState) => ({
      isCreateOpportunityModal: !prevState.isCreateOpportunityModal,
    }));
  };

  handleChangeQuotes = (e) => {
    if (e.target.value === 0) {
      this.toggleCreateOpportunityModal();
    } else {
      const newState = cloneDeep(this.state);
      newState[e.target.name] = e.target.value;
      newState.data.opportunity_id = e.target.value;
      this.setState(newState);
    }
  };

  createQuote = (data: any) => {
    this.setState({ loading: true });
    const customer = this.props.customer;

    const customerObj = this.state.customerOptions.filter(
      (c) => customer === c.value
    );

    if (customer && customerObj.length > 0) {
      (data.customer_id as any) = customer;
      (data.user_id as any) = this.state.data.customer_contact;
      this.props
        .createQuote(customerObj[0].crm_id, data)
        .then((action) => {
          if (action.type === CREATE_QUOTES_SUCCESS) {
            this.setState({
              isCreateOpportunityModal: false,
            });
            this.props.fetchQuoteDashboardListing(customer, true);
          } else {
            this.setState({
              isCreateOpportunityModal: true,
              errorList: action.errorList.data,
              quote: data,
            });
          }
          this.setState({ loading: false });
        })
        .catch(() => {
          this.setState({ loading: false });
        });
    } else {
      this.setState({
        loading: false,
      });
    }
  };

  getOpportunityOptions = () => {
    const quoteList = this.props.quoteList
      ? this.props.quoteList.map((t) => ({
          value: t.id,
          label: `${t.name} (${t.stage_name})`,
          disabled: false,
        }))
      : [];

    return quoteList;
  };

  renderBody = () => {
    return (
      <div className="sales-activity-notes">
        {this.state.isCreateUserModal && (
          <CustomerUserNew
            isVisible={this.state.isCreateUserModal}
            close={this.closeUserModal}
            customerId={this.state.data.customer}
          />
        )}
        <AddQuote
          show={this.state.isCreateOpportunityModal}
          onClose={this.closeCreateOpportunityModal}
          onSubmit={this.createQuote}
          types={this.props.qTypeList}
          stages={this.state.stages}
          isLoading={this.state.loading}
          errorList={this.state.errorList}
          quote={this.state.quote}
        />
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div className="first-row">
          <div className="action-second status-actions-row">
            <div className="sales-activity-action-component">
              <Input
                field={{
                  label: "Description",
                  type: InputFieldType.TEXT,
                  value: this.state.data.description,
                  isRequired: true,
                }}
                width={6}
                labelIcon={"info"}
                name="description"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Enter description`}
                error={this.state.error.description}
              />
              <Input
                field={{
                  label: "Priority",
                  type: InputFieldType.PICKLIST,
                  value: this.state.data.priority,
                  options: this.state.prioritySettingOptions,
                  isRequired: true,
                }}
                className="priority-input"
                width={6}
                multi={false}
                name="priority"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select`}
                loading={this.state.fetchingPrioritySettings}
                error={this.state.error.priority}
              />

              <div className="customer-contact-notes-activity col-md-6 row">
                <Input
                  field={{
                    label: "Customer Contact",
                    type: InputFieldType.PICKLIST,
                    value: this.state.data.customer_contact,
                    options: this.state.customerUsers,
                    isRequired: false,
                  }}
                  className="customer-contact-input"
                  width={12}
                  multi={false}
                  name="customer_contact"
                  onChange={(e) => this.handleDataChange(e)}
                  placeholder={`Select Customer Contact`}
                  loading={this.state.fetchingCustomersUsers}
                  error={this.state.error.customer_contact}
                />
                <SquareButton
                  content="+"
                  onClick={(e) => this.toggleCreateUserModal()}
                  className="add-new-option-notes-activity "
                  bsStyle={ButtonStyle.PRIMARY}
                  title="Add New User"
                  disabled={!this.state.data.customer}
                />
              </div>
              <div className="customer-contact-notes-activity-opp col-md-6 row">
                <Input
                  field={{
                    label: "Select Opportunity",
                    type: InputFieldType.PICKLIST,
                    value: this.state.quote_id,
                    options: this.getOpportunityOptions(),
                    isRequired: false,
                  }}
                  loading={this.props.quoteFetching}
                  width={12}
                  className="customer-contact-input"
                  name="quote_id"
                  onChange={(e) => this.handleChangeQuotes(e)}
                  multi={false}
                  placeholder="Select Opportunity"
                  disabled={false}
                  error={{
                    errorState: IValidationState.SUCCESS,
                    errorMessage: "",
                  }}
                />
                <SquareButton
                  content="+"
                  onClick={(e) => this.toggleCreateOpportunityModal()}
                  className="add-new-option-notes-activity "
                  bsStyle={ButtonStyle.PRIMARY}
                  title="Add New Opportunity"
                  disabled={
                    !this.state.data.customer ||
                    !this.state.data.customer_contact
                  }
                />
              </div>
            </div>
          </div>
          <div className="status-overall-accordians">
            <div className="details-box">
              <Input
                field={{
                  label: "Due Date",
                  type: InputFieldType.DATE,
                  value: this.state.data.due_date,
                  isRequired: true,
                }}
                className="due-date-input"
                width={6}
                labelIcon={"info"}
                name="due_date"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select Date`}
                showTime={false}
                disablePrevioueDates={false}
                disabled={false}
                error={this.state.error.due_date}
                disableTimezone={true}
              />
            </div>
            <div className="details-box">
              <Input
                field={{
                  label: "Assigned To",
                  type: InputFieldType.PICKLIST,
                  value:
                    this.state.data.assigned_to === ""
                      ? this.props.user && this.props.user.id
                      : this.state.data.assigned_to,
                  options:
                    this.props.providerUsers &&
                    this.props.providerUsers.map((data) => ({
                      value: data.id,
                      label: `${data.first_name} ${data.last_name}`,
                    })),
                  isRequired: true,
                }}
                width={6}
                multi={false}
                name="assigned_to"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select`}
                loading={this.props.isFetchingUsers}
                error={this.state.error.assigned_to}
              />
            </div>
            <div className="details-box col-md-8">
              <div className="header-label">Types of Activity</div>
              <Checkbox
                className="activity-type-option"
                isChecked={this.state.data.activity_type.includes(
                  "Statement of work"
                )}
                name="activityTypeCheck"
                onChange={(e) =>
                  this.handleActivityTypeChange(e, "Statement of work")
                }
              >
                Statement of Work
              </Checkbox>
              <Checkbox
                className="activity-type-option"
                isChecked={this.state.data.activity_type.includes(
                  "Hardware BoM"
                )}
                name="activityTypeCheck"
                onChange={(e) =>
                  this.handleActivityTypeChange(e, "Hardware BoM")
                }
              >
                Hardware BoM
              </Checkbox>
              <Checkbox
                className="activity-type-option"
                isChecked={this.state.data.activity_type.includes(
                  "Research and Respond"
                )}
                name="activityTypeCheck"
                onChange={(e) =>
                  this.handleActivityTypeChange(e, "Research and Respond")
                }
              >
                Research and Respond
              </Checkbox>
            </div>
          </div>
        </div>
      </div>
    );
  };
  renderFooter = () => {
    return (
      <div className={`${this.state.loading ? `loading` : ""}`}>
        <SquareButton
          content="Save"
          bsStyle={ButtonStyle.PRIMARY}
          onClick={(e) => this.onSaveClick(e)}
          disabled={this.state.disableSaveClick}
        />
        <SquareButton
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
          onClick={(e) => {
            this.props.close(e);
          }}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Add New Activity"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-new-container"
      />
    );
  }
}

const mapStateToProps = (state: any) => ({
  isFetchingUsers: state.providerUser.isFetchingUsers,
  providerUsers: state.providerUser.providerUsers,
  user: state.profile.user,
  qTypeList: state.sow.qTypeList,
  isFetchingQStageList: state.sow.isFetchingQStageList,
  isFetchingQTypeList: state.sow.isFetchingQTypeList,
  quoteList: state.dashboard.quoteList,
  quoteFetching: state.dashboard.quoteFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchCustomersShort: () => dispatch(fetchCustomersShort()),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  getSalesActivityPrioritySetting: () =>
    dispatch(getSalesActivityPrioritySetting()),
  getSalesActivityStatusSetting: () =>
    dispatch(getSalesActivityStatusSetting()),
  postActivityStatus: (data: any) => dispatch(postActivityStatus(data)),
  createQuote: (id: number, q: any) => dispatch(createQuote(id, q)),
  fetchAllCustomerUsers: (id: string, params?: IServerPaginationParams) =>
    dispatch(fetchAllCustomerUsers(id)),
  fetchQuoteDashboardListing: (id: any, openOnly?: boolean) =>
    dispatch(fetchQuoteDashboardListingPU(id, openOnly)),
  getQuoteStages: () => dispatch(getQuoteAllStages()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(ActivityDetail);
