import _ from "lodash";
import React from "react";
import { connect } from "react-redux";
import * as XLSX from "xlsx";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import {
  addDevice,
  addDeviceCustomerUser,
  ADD_DEVICE_FAILURE,
  ADD_DEVICE_SUCCESS,
  batchCreateExportData,
  createCircuitDeviceAssociation,
  createCircuitDeviceAssociationCU,
  DEVICE_UPDATE_SUCCESS,
  editSingleDevice,
  editSingleDevicePU,
  EDIT_SINGLE_DEVICE_FAILURE,
  EDIT_SINGLE_DEVICE_SUCCESS,
  fetchAvailableCircuitDeviceAssociation,
  fetchAvailableCircuitDeviceAssociationCU,
  fetchContractStatuses,
  fetchDeviceCategories,
  fetchDevices,
  fetchDevicesCustomerUser,
  fetchExistingDeviceAssociation,
  fetchExistingDeviceAssociationCU,
  fetchManufacturers,
  fetchProviderIntegrationsForFilter,
  fetchSingleDevice,
  fetchSingleDevicePU,
  fetchSites,
  fetchSitesCustomersUser,
  fetchStatuses,
  fetchTaskStatus,
  fetchTypes,
  FETCH_SINGLE_DEVICE_FAILURE,
  FETCH_SINGLE_DEVICE_SUCCESS,
  FETCH_XLSX_REQUEST,
  FETCH_XLSX__SUCCESS,
  resetManufacturerApiRetryCount,
  setXlsxData,
  TASK_STATUS_FAILURE,
  TASK_STATUS_SUCCESS,
  updateContract,
  updateDeviceCategoryCU,
  updateDeviceCategoryPU,
  updateManufaturerCU,
  updateManufaturerPU,
  updateSitesCU,
  updateSitesPU,
  updateStatus,
  updateStatusCustomer,
  UPDATE_STATUS_SUCCESS,
  LAST_VISITED_ASSET_SITE,
} from "../../actions/inventory";
import {
  fetchAllProviderIntegrations,
  fetchProviderConfigStatus,
  FETCH_PROVIDER_CONFIG_STATUS_SUCCESS,
} from "../../actions/provider/integration";
import { LAST_VISITED_CLIENT_360_TAB } from "../../actions/customer";
import SquareButton from "../../components/Button/button";
import EditButton from "../../components/Button/editButton";
import Checkbox from "../../components/Checkbox/checkbox";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import Table from "../../components/Table/table";
import store from "../../store";
import { toFormattedDate } from "../../utils/CalendarUtil";
import { allowPermission } from "../../utils/permissions";
import { searchInFields } from "../../utils/searchListUtils";
import AddDevice from "./addDevice";
import BulkUpdate from "./bulkUpdate";
import EditDevice from "./editDevice";
import FilterModal from "./filterModal";
import "./style.scss";
import ViewDevice from "./viewDevice";

interface IInventoryProps extends ICommonProps {
  fetchDevices: TFetchDevices;
  fetchDevicesCustomerUser: TFetchDevicesCustomerUser;
  fetchSingleDevice: TFetchSingleDevice;
  fetchManufacturers: TFetchManufacturers;
  fetchContractStatuses: TFetchContractStatuses;
  fetchSites: TFetchSites;
  fetchSitesCustomersUser: TFetchSitesCustomerUser;
  fetchTypes: TFetchTypes;
  updateContract: TUpdateContract;
  addDevice: TAddDevice;
  addDeviceCustomerUser: TAddDeviceCustomerUser;
  editSingleDevice: TEditDevice;
  editSingleDevicePU: any;
  devices: IInventory[];
  manufacturers: any[];
  sites: any[];
  types: any[];
  customerId: number;
  deviceDetails: IDevice;
  contractStatuses: IContractStatus[];
  setXlsxData: any;
  batchCreateExportData: any;
  user: ISuperUser;
  isLoading: boolean;
  isDeviceFetching: boolean;
  addErrorMessage: TShowErrorMessage;
  fetchingXlsx: boolean;
  resetManufacturerApiRetryCount: any;
  updateStatus: any;
  updateStatusCustomer: any;
  fetchProviderConfigStatus: any;
  fetchAvailableCircuitDeviceAssociation: any;
  createCircuitDeviceAssociation: any;
  fetchExistingDeviceAssociation: any;
  availableAssociationList: any;
  fetchAvailableCircuitDeviceAssociationCU: any;
  fetchExistingDeviceAssociationCU: any;
  createCircuitDeviceAssociationCU: any;
  existingAssociationList: any;
  isFetchingDevice: boolean;
  fetchSingleDevicePU: any;
  fetchDeviceCategories: any;
  deviceCategoryList: any;
  updateDeviceCategoryPU: any;
  updateDeviceCategoryCU: any;
  fetchProviderIntegrationsForFilter: any;
  providerIntegration: any;
  allProviderIntegrations: IProviderIntegration[];
  fetchAllProviderIntegrations: any;
  updateManufaturerCU: any;
  updateManufaturerPU: any;
  updateSitesCU: any;
  updateSitesPU: any;
  fetchTaskStatus: any;
  addSuccessMessage: any;
  fetchStatuses: any;
  statusList: any[];
}

interface IInventoryState {
  rows: IInventory[];
  selectedRows: number[];
  errorList?: any;
  isAddDeviceModalOpen: boolean;
  isEditDeviceModalOpen: boolean;
  isViewDeviceModalOpen: boolean;
  isFilterModalOpen: boolean;
  customerId?: number;
  deviceDetails?: IDevice;
  searchString: string;
  showInactiveDevices: boolean;
  filters: IInventoryFilters;
  siteLabelIds: { [id: number]: string };
  typesLabelIds: { [id: string]: string };
  statusLabelIds: { [id: number]: string };
  contractStatusLabelIds: { [id: number]: string };
  managedLabelIds: { [id: string]: string };
  createdBYLabelIds: { [id: string]: string };
  manufacturerLabelIds: { [id: string]: string };
  replacementLabelIds: { [id: string]: string };
  nonCisco: { [id: string]: string };
  xlsxfile?: any;
  isDevicePosting: boolean;
  isPostingBulkUpdate: boolean;
  deviceCategories: any;
  isBulkUpdateModalOpen: boolean;
  clearAdd: boolean;
  taskId: any;
  fetching: boolean;
  downloadingIds: number[];
}

class InventoryManagement extends React.Component<
  IInventoryProps,
  IInventoryState
> {
  unlisten: any;
  constructor(props: IInventoryProps) {
    super(props);

    this.state = {
      rows: [],
      selectedRows: [],
      isAddDeviceModalOpen: false,
      isEditDeviceModalOpen: false,
      isViewDeviceModalOpen: false,
      isBulkUpdateModalOpen: false,
      isFilterModalOpen: false,
      searchString: "",
      showInactiveDevices: false,
      deviceCategories: null,
      filters: {
        site: [],
        status: [],
        type: [],
        manufacturer: [],
        contractStatus: [],
      },
      siteLabelIds: {},
      typesLabelIds: {},
      statusLabelIds: {},
      contractStatusLabelIds: {},
      managedLabelIds: {
        Managed: "Managed",
        "Not Managed": "Not Managed",
      },
      createdBYLabelIds: {
        "Created By LogicMonitor": "Created By LogicMonitor",
        "Created By Solarwinds": "Created By Solarwinds",
      },
      replacementLabelIds: {
        "Replacement: Available": "Replacement: Available",
        "Replacement: Not Available": "Replacement: Not Available",
      },
      nonCisco: {
        "Cisco Devices": "Cisco Devices",
        "Non-Cisco Devices": "Non-Cisco Devices",
      },
      manufacturerLabelIds: {},
      isDevicePosting: false,
      isPostingBulkUpdate: false,
      clearAdd: false,
      taskId: null,
      fetching: false,
      downloadingIds: [],
    };
  }

  componentDidMount() {
    if (this.props.location.pathname === "/client-360") {
      store.dispatch({
        type: LAST_VISITED_CLIENT_360_TAB,
        response: "Assets",
      });
    }
    this.props.fetchProviderConfigStatus().then((action) => {
      if (
        action.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS &&
        action.response &&
        action.response.crm_authentication_configured
      ) {
        this.props.fetchManufacturers();
        this.props.fetchDeviceCategories();
        this.props.fetchContractStatuses();
      }
    });
    if (this.props.statusList.length === 0) {
      this.props.fetchStatuses();
    }
    this.props.fetchTypes();
    if (this.props.customerId) {
      this.props.fetchDevices(
        this.props.customerId,
        this.state.showInactiveDevices
      );
      this.props.fetchSites(this.props.customerId);
      this.props.fetchAvailableCircuitDeviceAssociation(this.props.customerId);
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.fetchDevicesCustomerUser(this.state.showInactiveDevices);
      this.props.fetchSitesCustomersUser();
      this.props.fetchAvailableCircuitDeviceAssociationCU();
    }
    if (!this.props.allProviderIntegrations) {
      this.props.fetchAllProviderIntegrations();
    }
    if (this.props.providerIntegration) {
      this.setLabelsManufacturer(this.props);
    }
    if (this.props.statusList) {
      this.setLabelsStatus(this.props);
    }
    this.unlisten = this.props.history.listen(() => {
      const lastVisitedSite: string = this.props.location.pathname;
      store.dispatch({
        type: LAST_VISITED_ASSET_SITE,
        response: lastVisitedSite,
      });
    });
  }

  componentWillUnmount(): void {
    this.unlisten();
  }

  componentDidUpdate(prevProps: IInventoryProps) {
    if (this.props.devices !== prevProps.devices) {
      const rows = this.getRows(this.props, "", this.state.filters);
      this.setState({
        rows,
      });
    }
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchDevices(
        this.props.customerId,
        this.state.showInactiveDevices
      );
      this.props.fetchSites(this.props.customerId);
      this.setState({
        filters: {
          site: [],
          status: [],
          type: [],
          manufacturer: [],
          contractStatus: [],
        },
      });
    }

    if (this.props.deviceDetails !== prevProps.deviceDetails) {
      this.setState({ deviceDetails: this.props.deviceDetails });
    }
    if (this.props.sites !== prevProps.sites) {
      const siteLabelIds = this.props.sites.reduce((labelIds, site) => {
        labelIds[site.site_id] = `Site: ${site.name}`;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        siteLabelIds,
      });
    }
    if (this.props.providerIntegration !== prevProps.providerIntegration) {
      this.setLabelsManufacturer(this.props);
    }
    if (this.props.types !== prevProps.types) {
      const typesLabelIds = this.props.types.reduce((labelIds, type) => {
        labelIds[type] = `Type: ${type}`;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        typesLabelIds,
      });
    }
    if (this.props.contractStatuses !== prevProps.contractStatuses) {
      const contractStatusLabelIds = this.props.contractStatuses.reduce(
        (statusIds, contractStatus) => {
          statusIds[contractStatus.contract_status_id] =
            contractStatus.contract_status === "N.A."
              ? "Contract Status : Not Covered"
              : `Contract Status : ${contractStatus.contract_status}`;

          return statusIds;
        },
        {}
      ); // tslint:disable-line

      this.setState({
        contractStatusLabelIds,
      });
    }
    if (this.props.statusList !== prevProps.statusList) {
      this.setLabelsStatus(this.props);
    }
    if (this.props.user && this.props.user !== prevProps.user) {
      if (this.props.user.type && this.props.user.type === "customer") {
        this.props.fetchDevicesCustomerUser(this.state.showInactiveDevices);
        this.props.fetchSitesCustomersUser();
      }
    }
    if (
      prevProps.allProviderIntegrations !==
        this.props.allProviderIntegrations &&
      this.props.allProviderIntegrations
    ) {
      // This will be called on reload of page
      // and when the crm_authentication_configured
      // is true.
      // TODO: this is not dynamic
      const provider = this.props.allProviderIntegrations.filter(
        (int) => int.type === ISetingsType.CONNECTWISE
      );

      if (provider.length > 0) {
        this.props.fetchProviderIntegrationsForFilter(provider[0].id);
      }
    }
  }

  setLabelsStatus = (props) => {
    const statusLabelIds = props.statusList.reduce(
      (statusIds, contractStatus) => {
        statusIds[contractStatus.id] = `Status: ${contractStatus.description}`;

        return statusIds;
      },
      {}
    ); // tslint:disable-line

    this.setState({
      statusLabelIds,
    });
  };

  setLabelsManufacturer = (props) => {
    const mapping = props.providerIntegration.other_config.manufacturer_mapping;
    const manufacturerLabelIds =
      props.providerIntegration &&
      Object.keys(mapping).reduce((labelIds, s) => {
        if (s !== "default" && s !== "software_suggestion") {
          labelIds[
            mapping[s].device_category_id
          ] = `MF: ${mapping[s].cw_mnf_name}`;
        }

        return labelIds;
      }, {}); // tslint:disable-line

    this.setState({
      manufacturerLabelIds,
    });
  };
  getCreatedSource = (device, filterCreatedBy) => {
    let show = false;
    const createdBy = _.get(device, "monitoring_data.created_by");

    if (
      filterCreatedBy.includes("Created By Solarwinds") &&
      createdBy === "solarwinds"
    ) {
      show = true;
    }
    if (
      filterCreatedBy.includes("Created By LogicMonitor") &&
      createdBy === "logicmonitor"
    ) {
      show = true;
    }

    return show;
  };

  getRows = (
    nextProps: IInventoryProps,
    searchString?: string,
    filters?: IInventoryFilters,
    showInactiveDevices?: boolean
  ) => {
    let devices = nextProps.devices;
    const search = searchString ? searchString : this.state.searchString;

    if (filters) {
      // Add filter for type once included.
      const filterSites = filters.site;
      const filterStatus = filters.status;
      const filterContractStatus = filters.contractStatus;
      const filterManufacturer = filters.manufacturer;
      const filterTypes = filters.type.map((item) => {
        return item === "N.A." ? "" : item;
      });
      const filterManaged = filters.is_managed;
      const filterCreatedBy = filters.created_by;
      const filterReplacement = filters.replacement;
      const filterNonCisco = filters.non_cisco;

      devices =
        devices &&
        devices.filter((device) => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
          const hasFilteredSite =
            filterSites.length > 0
              ? filterSites.includes(device.site_id)
              : true;
          const hasFilteredStatus =
            filterStatus.length > 0
              ? filterStatus.includes(device.status_id)
              : true;
          const hasFilteredContractStatus =
            filterContractStatus.length > 0
              ? filterContractStatus.includes(
                  device.contract_status && device.contract_status_id
                )
              : true;
          const hasFilteredType =
            filterTypes.length > 0
              ? filterTypes.includes(device.device_type)
              : true;
          const hasFilteredManaged =
            filterManaged && filterManaged.length > 0
              ? filterManaged.includes(
                  device &&
                    device.monitoring_data &&
                    device.monitoring_data.is_managed
                    ? "Managed"
                    : "Not Managed"
                )
              : true;
          const hasFilteredManagedOrCreatedBy =
            filterCreatedBy && filterCreatedBy.length > 0
              ? this.getCreatedSource(device, filterCreatedBy)
              : true;
          const hasFilteredReplacement =
            filterReplacement && filterReplacement.length > 0
              ? filterReplacement.includes(
                  device && device.migration_info
                    ? "Replacement: Available"
                    : "Replacement: Not Available"
                )
              : true;
          const hasFilteredManufacturer =
            filterManufacturer.length > 0
              ? filterManufacturer.includes(device.category_id)
              : true;
          const hasFilteredfilterNonCisco =
            filterNonCisco && filterNonCisco.length > 0
              ? filterNonCisco.includes(
                  device && device.non_cisco
                    ? "Non-Cisco Devices"
                    : "Cisco Devices"
                )
              : true;
          return (
            hasFilteredSite &&
            hasFilteredStatus &&
            hasFilteredType &&
            hasFilteredManaged &&
            hasFilteredManagedOrCreatedBy &&
            hasFilteredManufacturer &&
            hasFilteredReplacement &&
            hasFilteredContractStatus &&
            hasFilteredfilterNonCisco
          );
        });
    }
    if (search && search.length > 0) {
      devices =
        devices &&
        devices.filter((row) =>
          searchInFields(row, search, [
            "name",
            "device_name",
            "device_type",
            "status",
            "serial_number",
            "category_name",
            "manufacturer_name",
            "contract_status",
            "model_number",
            "service_contract_number",
            "monitoring_data.logicmonitor.sys_name",
            "monitoring_data.logicmonitor.host_name",
            "monitoring_data.solarwinds.sys_name",
            "monitoring_data.solarwinds.host_name",
            "monitoring_data.logicmonitor.model",
            "asset_tag",
          ])
        );
    }

    const rows: any =
      devices &&
      devices.map((device, index) => ({
        ...device,
        site: device.name,
        model: device.model_number,
        endOfSupport: device.LDOS_date,
        expiration_date: device.expiration_date,
        host_name: _.get(
          device,
          "monitoring_data.logicmonitor.host_name",
          undefined
        ),
        is_managed: _.get(device, "monitoring_data.is_managed", false),
        index,
      }));

    return rows;
  };

  onRowClick = (rowInfo) => {
    if (allowPermission("view_device")) {
      this.setState({
        downloadingIds: [...this.state.downloadingIds, rowInfo.original.id],
      });
      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "customer"
      ) {
        this.props.fetchSingleDevice(rowInfo.original.id).then((action) => {
          if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
            this.setState(() => ({
              isViewDeviceModalOpen: true,
            }));

            this.props.fetchAvailableCircuitDeviceAssociationCU(
              rowInfo.original.id
            );
            this.props.fetchExistingDeviceAssociationCU(rowInfo.original.id);
            this.setState({
              downloadingIds: this.state.downloadingIds.filter(
                (id) => rowInfo.original.id !== id
              ),
            });
          } else if (action.type === FETCH_SINGLE_DEVICE_FAILURE) {
            this.props.addErrorMessage(
              action.errorList && action.errorList.data.message
            );
            this.setState({
              downloadingIds: this.state.downloadingIds.filter(
                (id) => rowInfo.original.id !== id
              ),
            });
          }
        });
      } else {
        this.props
          .fetchSingleDevicePU(this.props.customerId, rowInfo.original.id)
          .then((action) => {
            if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
              this.setState(() => ({
                isViewDeviceModalOpen: true,
              }));
              if (this.props.customerId) {
                this.props.fetchAvailableCircuitDeviceAssociation(
                  this.props.customerId,
                  rowInfo.original.id
                );
                this.props.fetchExistingDeviceAssociation(
                  this.props.customerId,
                  rowInfo.original.id
                );
              }
              this.setState({
                downloadingIds: this.state.downloadingIds.filter(
                  (id) => rowInfo.original.id !== id
                ),
              });
            } else if (action.type === FETCH_SINGLE_DEVICE_FAILURE) {
              this.props.addErrorMessage(
                action.errorList && action.errorList.data.message
              );
              this.setState({
                downloadingIds: this.state.downloadingIds.filter(
                  (id) => rowInfo.original.id !== id
                ),
              });
            }
          });
      }
    }
  };

  onEditRowClick = (index: number, event: any) => {
    event.stopPropagation();

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.fetchSingleDevice(index).then((action) => {
        if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
          if (
            this.props.user &&
            this.props.user.type &&
            this.props.user.type === "customer"
          ) {
            this.props.fetchAvailableCircuitDeviceAssociationCU(index);
            this.props.fetchExistingDeviceAssociationCU(index);
          }
        }
      });
    } else {
      this.props
        .fetchSingleDevicePU(this.props.customerId, index)
        .then((action) => {
          if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
            if (this.props.customerId) {
              this.props.fetchAvailableCircuitDeviceAssociation(
                this.props.customerId,
                index
              );
              this.props.fetchExistingDeviceAssociation(
                this.props.customerId,
                index
              );
            }
          }
        });
    }
    this.setState(() => ({
      isEditDeviceModalOpen: true,
    }));
  };

  openURL = (event: any, url) => {
    event.stopPropagation();
    if (url) {
      window.open(url);
    }
  };

  toggleAddNewDeviceModal = () => {
    this.setState((prevState) => ({
      isAddDeviceModalOpen: !prevState.isAddDeviceModalOpen,
      deviceDetails: {
        device_name: "",
        category_id: null,
        serial_number: null,
        site_id: null,
        manufacturer_id: null,
        notes: "",
        instance_id: null,
        asset_tag: "",
        customer_notes: "",
      },
    }));
  };

  downloadSampleFile = () => {
    const url =
      "https://acella-images.s3.us-west-2.amazonaws.com/media/samples-files/CRR+Template.xlsx";
    const link = document.createElement("a");
    link.href = url;
    link.target = "_blank";
    link.setAttribute("download", "CRR_Template.xlsx");
    document.body.appendChild(link);
    link.click();
  };
  toggleEditDeviceModal = () => {
    this.setState((prevState) => ({
      isEditDeviceModalOpen: !prevState.isEditDeviceModalOpen,
    }));
  };

  toggleViewDeviceModal = () => {
    this.setState((prevState) => ({
      isViewDeviceModalOpen: !prevState.isViewDeviceModalOpen,
    }));
  };

  toggleUpdateContract = () => {
    this.setState((prevState) => ({
      isBulkUpdateModalOpen: !prevState.isBulkUpdateModalOpen,
    }));
  };

  toggleUpdateCategory = () => {
    this.setState((prevState) => ({
      isBulkUpdateModalOpen: !prevState.isBulkUpdateModalOpen,
    }));
  };

  toggleUpdateStatus = () => {
    this.setState((prevState) => ({
      isBulkUpdateModalOpen: !prevState.isBulkUpdateModalOpen,
    }));
  };
  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  toggleBulkUpdate = () => {
    this.setState((prevState) => ({
      isBulkUpdateModalOpen: !prevState.isBulkUpdateModalOpen,
    }));
  };

  onRowsToggle = (selectedRows) => {
    this.setState({
      selectedRows,
    });
  };

  onDeviceAdd = (newDevice: IDevice) => {
    this.setState({ isDevicePosting: true });

    if (!newDevice.instance_id) {
      newDevice.instance_id = "-";
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props
        .addDeviceCustomerUser(newDevice)
        .then((action) => {
          if (action.type === ADD_DEVICE_SUCCESS) {
            this.setState({
              isAddDeviceModalOpen: false,
              isDevicePosting: false,
              errorList: [],
              deviceDetails: {
                device_name: "",
                category_id: null,
                serial_number: null,
                site_id: null,
                manufacturer_id: null,
                notes: "",
                instance_id: null,
                asset_tag: "",
                customer_notes: "",
              },
            });
            this.props.fetchDevicesCustomerUser(this.state.showInactiveDevices);
          } else if (action.type === ADD_DEVICE_FAILURE) {
            this.setState({
              isAddDeviceModalOpen: true,
              errorList: action.errorList.data,
              clearAdd: false,
              deviceDetails: newDevice,
            });
            this.setState({ isDevicePosting: false });
          }
        })
        .catch(() => {
          this.setState({ isDevicePosting: false });
        });
    } else {
      const customerId = this.props.customerId;
      if (customerId) {
        this.props.addDevice(customerId, newDevice).then((action) => {
          if (action.type === ADD_DEVICE_SUCCESS) {
            this.setState({
              isAddDeviceModalOpen: false,
              isDevicePosting: false,
              clearAdd: true,
              deviceDetails: {
                device_name: "",
                category_id: null,
                serial_number: null,
                site_id: null,
                manufacturer_id: null,
                notes: "",
                instance_id: null,
                asset_tag: "",
                customer_notes: "",
              },
            });

            this.props.fetchDevices(customerId, this.state.showInactiveDevices);
          } else if (action.type === ADD_DEVICE_FAILURE) {
            this.setState({
              isAddDeviceModalOpen: true,
              errorList: action.errorList.data,
              clearAdd: false,
              deviceDetails: newDevice,
            });
            this.setState({ isDevicePosting: false });
          }
          this.setState({ isDevicePosting: false });
        });
      }
    }
  };

  onDeviceEditSave = (device: IDevice) => {
    this.setState({ isDevicePosting: true });
    const customerId = this.props.customerId;
    if (!device.instance_id) {
      device.instance_id = "-";
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.editSingleDevice(device.id, device).then((action) => {
        if (action.type === EDIT_SINGLE_DEVICE_SUCCESS) {
          this.setState({
            isEditDeviceModalOpen: false,
            isDevicePosting: false,
          });
          const data = {
            circuit_info_ids: device.circuit_info_ids,
            device_crm_id: device.id,
          };

          if (
            this.props.user &&
            this.props.user.type &&
            this.props.user.type === "customer"
          ) {
            this.props.fetchDevicesCustomerUser(this.state.showInactiveDevices);
            this.props.createCircuitDeviceAssociationCU(data);
          }
        } else if (action.type === EDIT_SINGLE_DEVICE_FAILURE) {
          this.setState({
            isEditDeviceModalOpen: true,
            errorList: action.errorList.data,
            isDevicePosting: false,
          });
        }
      });
    } else {
      this.props
        .editSingleDevicePU(customerId, device.id, device)
        .then((action) => {
          if (action.type === EDIT_SINGLE_DEVICE_SUCCESS) {
            this.setState({
              isEditDeviceModalOpen: false,
              isDevicePosting: false,
            });
            const data = {
              circuit_info_ids: device.circuit_info_ids,
              device_crm_id: device.id,
            };

            this.props.fetchDevices(customerId, this.state.showInactiveDevices);
            this.props.createCircuitDeviceAssociation(data, customerId);
          } else if (action.type === EDIT_SINGLE_DEVICE_FAILURE) {
            this.setState({
              isEditDeviceModalOpen: true,
              errorList: action.errorList.data,
              isDevicePosting: false,
            });
          }
        });
    }
  };

  onContractUpdate = (contractNo: string) => {
    this.setState({ isPostingBulkUpdate: true });
    if (this.props.customerId) {
      this.props
        .updateContract(
          this.props.customerId,
          contractNo,
          this.state.selectedRows
        )
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    }
  };

  onUpdateCategory = (category: string) => {
    this.setState({
      isPostingBulkUpdate: true,
    });

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "provider" &&
      this.props.customerId
    ) {
      this.props
        .updateDeviceCategoryPU(
          this.props.customerId,
          category,
          this.state.selectedRows
        )
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    } else {
      this.props
        .updateDeviceCategoryCU(category, this.state.selectedRows)
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    }
  };

  onStatusUpdate = (status: number) => {
    this.setState({ isPostingBulkUpdate: true });
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "provider" &&
      this.props.customerId
    ) {
      this.props
        .updateStatus(this.props.customerId, status, this.state.selectedRows)
        .then((action) => {
          if (action.type === UPDATE_STATUS_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    } else {
      this.props
        .updateStatusCustomer(status, this.state.selectedRows)
        .then((action) => {
          if (action.type === UPDATE_STATUS_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    }
  };

  fetchTaskStatus = () => {
    if (this.state.taskId) {
      this.props.fetchTaskStatus(this.state.taskId).then((a) => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === "SUCCESS"
          ) {
            if (
              this.props.user &&
              this.props.user.type &&
              this.props.user.type === "provider" &&
              this.props.customerId
            ) {
              this.props.addSuccessMessage("Bulk update completed.");
              this.setState({
                isBulkUpdateModalOpen: false,
                isPostingBulkUpdate: false,
                selectedRows: [],
              });
              this.setState({ taskId: "" });
              this.props.fetchDevices(
                this.props.customerId,
                this.state.showInactiveDevices
              );
            } else {
              this.props.addSuccessMessage("Bulk update completed.");
              this.setState({
                isBulkUpdateModalOpen: false,
                isPostingBulkUpdate: false,
                selectedRows: [],
              });
              this.props.fetchDevicesCustomerUser(
                this.state.showInactiveDevices
              );
            }
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "FAILURE"
          ) {
            this.setState({ taskId: "" });
            this.props.addErrorMessage("Bulk update failed.");
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "PENDING"
          ) {
            setTimeout(() => {
              this.fetchTaskStatus();
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.fetchDevices(this.props.customerId, false);
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  onUpdateSites = (site: string) => {
    this.setState({
      isPostingBulkUpdate: true,
    });

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "provider" &&
      this.props.customerId
    ) {
      this.props
        .updateSitesPU(this.props.customerId, site, this.state.selectedRows)
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    } else {
      this.props
        .updateSitesCU(site, this.state.selectedRows)
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    }
  };
  onUpdateManufaturer = (manufacturer: string) => {
    this.setState({
      isPostingBulkUpdate: true,
    });

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "provider" &&
      this.props.customerId
    ) {
      this.props
        .updateManufaturerPU(
          this.props.customerId,
          manufacturer,
          this.state.selectedRows
        )
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    } else {
      this.props
        .updateManufaturerCU(manufacturer, this.state.selectedRows)
        .then((action) => {
          if (action.type === DEVICE_UPDATE_SUCCESS) {
            if (action.response && action.response.task_id) {
              this.setState({ taskId: action.response.task_id });
              this.fetchTaskStatus();
            }
          }
        })
        .catch(() => {
          this.setState({ isPostingBulkUpdate: false });
        });
    }
  };

  onFiltersUpdate = (filters: IInventoryFilters) => {
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState({
      rows,
      filters,
      isFilterModalOpen: false,
    });
  };

  handleChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };

  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters
    );

    this.setState({ rows });
  };

  onChecboxChanged = (event: any) => {
    const showInactiveDevices = event.target.checked;
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters,
      showInactiveDevices
    );

    this.setState({
      showInactiveDevices,
      rows,
    });
    if (this.props.user.type && this.props.user.type === "customer") {
      this.props.fetchDevicesCustomerUser(showInactiveDevices);
    } else {
      this.props.fetchDevices(this.props.customerId, showInactiveDevices);
    }
  };

  deleteFilter = (filterName: string, attributeIndex: number) => {
    const prevFilters = this.state.filters;
    const filters = {
      ...prevFilters,
      [filterName]: [
        ...prevFilters[filterName].slice(0, attributeIndex),
        ...prevFilters[filterName].slice(attributeIndex + 1),
      ],
    };
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState(() => ({
      filters,
      rows,
    }));
  };

  onClickExport = () => {
    if (this.props.customerId) {
      this.setState({ fetching: true });
      const deviceIds = this.state.rows.map((item) => {
        return item.id;
      });
      this.props
        .batchCreateExportData(
          this.props.customerId,
          deviceIds,
          this.state.showInactiveDevices
        )
        .then((action) => {
          this.setState({ fetching: false });
        });
    }
  };

  // Render methods

  renderFilters = () => {
    const {
      filters,
      siteLabelIds,
      typesLabelIds,
      managedLabelIds,
      createdBYLabelIds,
      statusLabelIds,
      contractStatusLabelIds,
      manufacturerLabelIds,
      replacementLabelIds,
      nonCisco,
    } = this.state;
    const shouldRenderFilters = filters
      ? (filters.site && filters.site.length > 0) ||
        (filters.type && filters.type.length > 0) ||
        (filters.is_managed && filters.is_managed.length > 0) ||
        (filters.created_by && filters.created_by.length > 0) ||
        (filters.status && filters.status.length > 0) ||
        (filters.contractStatus && filters.contractStatus.length > 0) ||
        (filters.manufacturer && filters.manufacturer.length > 0) ||
        (filters.non_cisco && filters.non_cisco.length > 0) ||
        (filters.replacement && filters.replacement.length > 0)
      : false;

    return shouldRenderFilters ? (
      <div className="inventory-management__table-filters">
        <label>Applied Filters: </label>
        {Object.keys(filters).map((filterName, filterIndex) => {
          const filterValues = filters[filterName];
          let labelIds = {};
          if (filterName === "site") {
            labelIds = siteLabelIds;
          } else if (filterName === "status") {
            labelIds = statusLabelIds;
          } else if (filterName === "contractStatus") {
            labelIds = contractStatusLabelIds;
          } else if (filterName === "type") {
            labelIds = typesLabelIds;
          } else if (filterName === "is_managed") {
            labelIds = managedLabelIds;
          } else if (filterName === "created_by") {
            labelIds = createdBYLabelIds;
          } else if (filterName === "manufacturer") {
            labelIds = manufacturerLabelIds;
          } else if (filterName === "replacement") {
            labelIds = replacementLabelIds;
          } else if (filterName === "non_cisco") {
            labelIds = nonCisco;
          }
          if (filterValues.length > 0) {
            return filterValues.map((id, valueIndex) => (
              <div
                key={`${filterIndex}.${valueIndex}`}
                className="inventory-management__filter"
              >
                <label>{labelIds[id] ? labelIds[id] : "N.A."}</label>
                <span
                  onClick={() => this.deleteFilter(filterName, valueIndex)}
                />
              </div>
            ));
          }
        })}
      </div>
    ) : null;
  };

  handleFileSelect = (event: any) => {
    const file = event.target.files[0];
    if (file.name.includes("xls")) {
      store.dispatch({ type: FETCH_XLSX_REQUEST });
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const data = e.target.result;
        const workbook = XLSX.read(data, {
          type: "binary",
        });
        // Here is your object
        workbook.SheetNames.forEach((sheetName) => {
          const xlRowObject = XLSX.utils.sheet_to_json(
            workbook.Sheets[sheetName],
            {
              defval: "",
              blankrows: false,
            }
          );
          store.dispatch({ type: FETCH_XLSX__SUCCESS });
          if (xlRowObject.length > 0) {
            this.props.history.push("/inventory-management/field-mapping");
            this.props.setXlsxData(xlRowObject, file);
          } else {
            this.props.addErrorMessage("Import file cannot be empty");
          }
        });
      };

      reader.onerror = (ex) => {
        console.info(ex);
      };

      reader.readAsBinaryString(file);
    } else {
      this.props.addErrorMessage("Incorrect file type");
    }
  };

  getLoadingMarkUp = (cell) => {
    if (this.state.downloadingIds.includes(cell.original.id)) {
      return (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Downloading File"
        />
      );
    } else {
      return false;
    }
  };

  renderTopBar = () => {
    return (
      <div
        className={
          this.props.customerId ||
          (this.props.user &&
            this.props.user.type &&
            this.props.user.type === "customer")
            ? "inventory-management__table-top "
            : "inventory-management__table-top disable-div"
        }
      >
        <div className="inventory-management__table-actions">
          <div className="inventory-management__table-header-search">
            {allowPermission("search_device") && (
              <Input
                field={{
                  label: "",
                  type: InputFieldType.SEARCH,
                  value: this.state.searchString,
                  isRequired: false,
                }}
                width={3}
                placeholder="Search"
                name="searchString"
                onChange={this.handleChange}
                className="inventory-management__search"
              />
            )}

            <Checkbox
              isChecked={this.state.showInactiveDevices}
              name="option"
              onChange={(e) => this.onChecboxChanged(e)}
            >
              Show Decommissioned Devices
            </Checkbox>
          </div>
          <div className="inventory-management__table-header-btn-group">
            {allowPermission("filters") && (
              <SquareButton
                onClick={this.toggleFilterModal}
                content={
                  <span>
                    <img alt="" src="/assets/icons/filter.png" />
                    Filters
                  </span>
                }
                disabled={this.props.devices && this.props.devices.length === 0}
                bsStyle={ButtonStyle.PRIMARY}
              />
            )}
            <SquareButton
              onClick={this.toggleBulkUpdate}
              content="Bulk Update"
              bsStyle={ButtonStyle.PRIMARY}
              disabled={this.state.selectedRows.length > 0 ? false : true}
            />
            {allowPermission("add_device") && (
              <SquareButton
                onClick={this.toggleAddNewDeviceModal}
                content="+ Add Device"
                bsStyle={ButtonStyle.PRIMARY}
              />
            )}
            {allowPermission("import") &&
              this.props.user &&
              this.props.user.type &&
              this.props.user.type !== "customer" && (
                <label
                  className="btn square-btn btn-primary
                file-button import-button"
                >
                  <img src="/assets/icons/import.png" />
                  Import
                  <input
                    type="file"
                    name="xlsxFile"
                    accept=".xls,.xlsx"
                    onChange={this.handleFileSelect}
                    onClick={(event: any) => {
                      event.target.value = null;
                    }}
                  />
                </label>
              )}
            {allowPermission("export") &&
              this.props.user &&
              this.props.user.type &&
              this.props.user.type !== "customer" && (
                <SquareButton
                  onClick={this.onClickExport}
                  content={
                    <span>
                      <img
                        className={this.state.fetching ? "icon__loading" : ""}
                        src={
                          this.state.fetching
                            ? "/assets/icons/loading.gif"
                            : "/assets/icons/export.png"
                        }
                        alt="Downloading File"
                      />
                      Export
                    </span>
                  }
                  bsStyle={ButtonStyle.PRIMARY}
                  disabled={
                    this.state.rows && this.state.rows.length > 0 ? false : true
                  }
                />
              )}
            <SquareButton
              onClick={this.downloadSampleFile}
              content={"Download CRR"}
              bsStyle={ButtonStyle.PRIMARY}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  render() {
    const columns: any = [
      {
        accessor: "status",
        Header: "Device Status",
        sortable: false,
        width: 120,
        Cell: (status) => (
          <div className={`device-status-icons`}>
            {status.original.contract_status === "Active" && (
              <img
                src={`/assets/icons/verified.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.contract_status === "Expiring" && (
              <img
                src={`/assets/icons/caution.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.contract_status === "N.A." && (
              <img
                src={`/assets/icons/cancel.svg`}
                className="status-svg-images"
                alt=""
                title={"Not covered"}
              />
            )}
            {status.original.contract_status === "Expired" && (
              <img
                src={`/assets/icons/HIGH.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {((status.original.is_managed &&
              status.original.status !== "Active") ||
              (!status.original.is_managed &&
                status.original.status === "Active")) && (
              <img
                src={`/assets/icons/monitor-grey.png`}
                alt=""
                title={`${
                  status.value === "Active" ? "In Service " : "Decommissioned"
                } & ${
                  status.original.is_managed === true
                    ? "Monitoring"
                    : "Not Monitoring"
                }`}
              />
            )}
            {status.original.is_managed &&
              status.original.status === "Active" && (
                <img
                  src={`/assets/icons/monitor-green.png`}
                  alt=""
                  title={"In Service & Monitored"}
                />
              )}
            {!status.original.is_managed &&
              status.original.status !== "Active" && (
                <img
                  src={`/assets/icons/monitor-red.png`}
                  alt=""
                  title={"Decommissioned"}
                />
              )}
            {(status.original.migration_info === undefined ||
              status.original.migration_info === null) && (
              <img
                src={`/assets/icons/notification-gray.svg`}
                className="status-svg-images"
                alt=""
                title={`Replacement not available`}
              />
            )}
            {status.original.migration_info !== undefined &&
              status.original.migration_info !== null && (
                <img
                  src={`/assets/icons/notification.svg`}
                  className="status-svg-images"
                  alt=""
                  title={`Replacement available`}
                />
              )}
            {status.original.host_name &&
              status.original.host_name !== undefined &&
              status.original.host_name !== null && (
                <img
                  src={`/assets/icons/plug-green.svg`}
                  className="status-svg-images"
                  alt=""
                  title={`IP available`}
                />
              )}
            {(!status.original.host_name ||
              status.original.host_name === undefined ||
              status.original.host_name === null) && (
              <img
                src={`/assets/icons/plug.svg`}
                className="status-svg-images"
                alt=""
                title={`No IP available`}
              />
            )}
          </div>
        ),
      },
      {
        accessor: "device_name",
        Header: "Device Name",
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "serial_number",
        Header: "Serial",
        Cell: (s) => (
          <>
            {this.getLoadingMarkUp(s)}
            <a
              title={`Contract End: ${
                s.original.expiration_date
                  ? toFormattedDate(s.original.expiration_date)
                  : " N.A."
              },
End of Support: ${
                s.original.endOfSupport
                  ? toFormattedDate(s.original.endOfSupport)
                  : " N.A."
              },
End of Life: ${
                s.original.EOL_date
                  ? toFormattedDate(s.original.EOL_date)
                  : " N.A."
              },
Manufacturer: ${
                s.original.manufacturer_name
                  ? s.original.manufacturer_name
                  : " N.A."
              },
${s.original.host_name ? `IP : ${s.original.host_name}` : ""}`}
            >
              {s.original.serial_number ? s.original.serial_number : " N.A."}
            </a>
          </>
        ),
      },

      {
        accessor: "model_number",
        Header: "Model",
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "device_type",
        Header: "Type",
        width: 100,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "site",
        Header: "Site",
        Cell: (cell) => (
          <div> {`${cell.value ? cell.value : "Not Selected"}`}</div>
        ),
      },
      {
        accessor: "service_contract_number",
        Header: "Contract",
        width: 100,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value
                ? cell.value
                : cell.original.expiration_date
                ? "Unknown"
                : "Not Covered"
            }`}
          </div>
        ),
      },
      {
        accessor: "expiration_date",
        Header: "End date",
        width: 100,
        Cell: (s) => (
          <div>
            {" "}
            {s.original.expiration_date
              ? toFormattedDate(s.original.expiration_date)
              : " N.A."}
          </div>
        ),
      },
      {
        accessor: "index",
        Header: "Edit",
        width: 60,
        sortable: false,
        show: allowPermission("edit_device"),
        Cell: (cell) => (
          <EditButton
            onClick={(e) => this.onEditRowClick(cell.original.id, e)}
          />
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: "id",
      onRowsToggle: this.onRowsToggle,
    };

    return (
      <div className="inventory-management">
        <div className="loader">
          <Spinner show={this.props.fetchingXlsx} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`device-listing__table ${
            this.props.isFetchingDevice ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          loading={this.props.isFetchingDevice}
        />

        <AddDevice
          show={this.state.isAddDeviceModalOpen}
          onClose={this.toggleAddNewDeviceModal}
          onSubmit={this.onDeviceAdd}
          manufacturers={this.props.manufacturers}
          sites={this.props.sites}
          fetchSites={this.props.fetchSites}
          isLoading={this.state.isDevicePosting}
          errorList={this.state.errorList}
          userType={this.props.user && this.props.user.type}
          deviceDetails={this.state.deviceDetails}
        />
        <EditDevice
          show={this.state.isEditDeviceModalOpen}
          onClose={this.toggleEditDeviceModal}
          onSubmit={this.onDeviceEditSave}
          manufacturers={this.props.manufacturers}
          sites={this.props.sites}
          fetchSites={this.props.fetchSites}
          deviceDetails={this.state.deviceDetails}
          openURL={this.openURL}
          isFetching={this.props.isDeviceFetching || this.state.isDevicePosting}
          availableAssociationList={this.props.availableAssociationList}
          existingAssociationList={this.props.existingAssociationList}
          userType={this.props.user && this.props.user.type}
        />
        <ViewDevice
          {...this.props}
          show={this.state.isViewDeviceModalOpen}
          onClose={this.toggleViewDeviceModal}
          manufacturers={this.props.manufacturers}
          sites={this.props.sites}
          fetchSites={this.props.fetchSites}
          deviceDetails={this.state.deviceDetails}
          openURL={this.openURL}
          isFetching={this.props.isDeviceFetching}
          resetRetry={this.props.resetManufacturerApiRetryCount}
          userType={this.props.user && this.props.user.type}
          existingAssociationList={this.props.existingAssociationList}
        />

        <BulkUpdate
          show={this.state.isBulkUpdateModalOpen}
          onClose={this.toggleBulkUpdate}
          onUpdateCategory={this.onUpdateCategory}
          onContractUpdate={this.onContractUpdate}
          onStatusUpdate={this.onStatusUpdate}
          onUpdateSites={this.onUpdateSites}
          onUpdateManufaturer={this.onUpdateManufaturer}
          totalSelected={this.state.selectedRows.length}
          isLoading={this.state.isPostingBulkUpdate}
          deviceCategoryList={this.props.deviceCategoryList}
          sites={this.props.sites}
          manufacturers={this.props.manufacturers}
          userType={this.props.user && this.props.user.type}
        />
        <FilterModal
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          sites={this.props.sites}
          types={this.props.types}
          contractStatuses={this.props.contractStatuses}
          statusList={this.props.statusList}
          prevFilters={this.state.filters}
          manufacturers={
            this.props.providerIntegration &&
            this.props.providerIntegration.other_config.manufacturer_mapping
          }
          showReplacement={true}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  devices: state.inventory.devices,
  manufacturers: state.inventory.manufacturers,
  sites: state.inventory.sites,
  types: state.inventory.types,
  contractStatuses: state.inventory.contractStatuses,
  customerId: state.customer.customerId,
  deviceDetails: state.inventory.device,
  user: state.profile.user,
  isLoading: state.inventory.isFetching,
  isFetchingDevice: state.inventory.isFetchingDevice,
  isDeviceFetching: state.inventory.isDeviceFetching,
  fetchingXlsx: state.inventory.fetchingXlsx,
  availableAssociationList: state.inventory.availableAssociationList,
  existingAssociationList: state.inventory.existingAssociationList,
  deviceCategoryList: state.inventory.deviceCategoryList,
  providerIntegration: state.inventory.providerIntegration,
  allProviderIntegrations: state.providerIntegration.allProviderIntegrations,
  statusList: state.inventory.statusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchDeviceCategories: () => dispatch(fetchDeviceCategories()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchContractStatuses: () => dispatch(fetchContractStatuses()),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  fetchSitesCustomersUser: () => dispatch(fetchSitesCustomersUser()),
  fetchTypes: () => dispatch(fetchTypes()),
  addDevice: (customerId: number, newDevice: IDevice) =>
    dispatch(addDevice(customerId, newDevice)),
  addDeviceCustomerUser: (newDevice: IDevice) =>
    dispatch(addDeviceCustomerUser(newDevice)),
  editSingleDevice: (deviceId: number, device: IDevice) =>
    dispatch(editSingleDevice(deviceId, device)),
  editSingleDevicePU: (customerId: number, deviceId: number, device: IDevice) =>
    dispatch(editSingleDevicePU(customerId, deviceId, device)),
  updateContract: (customerId: number, contractNo: any, deviceIds: number[]) =>
    dispatch(updateContract(customerId, contractNo, deviceIds)),
  fetchDevicesCustomerUser: (show: boolean) =>
    dispatch(fetchDevicesCustomerUser(show)),
  fetchDevices: (id: number, show: boolean) => dispatch(fetchDevices(id, show)),
  fetchSingleDevice: (id: number) => dispatch(fetchSingleDevice(id)),
  fetchSingleDevicePU: (customerId: number, id: number) =>
    dispatch(fetchSingleDevicePU(customerId, id)),
  setXlsxData: (data: any, file: any) => dispatch(setXlsxData(data, file)),
  batchCreateExportData: (
    customerId: number,
    deviceIds: any,
    showInactiveDevices: boolean
  ) =>
    dispatch(batchCreateExportData(customerId, deviceIds, showInactiveDevices)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  resetManufacturerApiRetryCount: (num: string) =>
    dispatch(resetManufacturerApiRetryCount(num)),
  updateStatus: (customerId: number, contractNo: any, deviceIds: number[]) =>
    dispatch(updateStatus(customerId, contractNo, deviceIds)),
  updateStatusCustomer: (contractNo: any, deviceIds: number[]) =>
    dispatch(updateStatusCustomer(contractNo, deviceIds)),
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
  fetchAvailableCircuitDeviceAssociation: (customerId: number) =>
    dispatch(fetchAvailableCircuitDeviceAssociation(customerId)),
  createCircuitDeviceAssociation: (data: any, customerId: number) =>
    dispatch(createCircuitDeviceAssociation(data, customerId)),
  fetchExistingDeviceAssociation: (contractNo: any, deviceId: number) =>
    dispatch(fetchExistingDeviceAssociation(contractNo, deviceId)),
  fetchAvailableCircuitDeviceAssociationCU: () =>
    dispatch(fetchAvailableCircuitDeviceAssociationCU()),
  createCircuitDeviceAssociationCU: (data: any) =>
    dispatch(createCircuitDeviceAssociationCU(data)),
  fetchExistingDeviceAssociationCU: (deviceId: number) =>
    dispatch(fetchExistingDeviceAssociationCU(deviceId)),
  updateDeviceCategoryCU: (Category: any, deviceIds: number[]) =>
    dispatch(updateDeviceCategoryCU(Category, deviceIds)),
  updateDeviceCategoryPU: (
    customerId: number,
    Category: any,
    deviceIds: number[]
  ) => dispatch(updateDeviceCategoryPU(customerId, Category, deviceIds)),
  fetchProviderIntegrationsForFilter: (id: number) =>
    dispatch(fetchProviderIntegrationsForFilter(id)),
  fetchAllProviderIntegrations: () => dispatch(fetchAllProviderIntegrations()),
  updateSitesCU: (site: any, deviceIds: number[]) =>
    dispatch(updateSitesCU(site, deviceIds)),
  updateSitesPU: (customerId: number, site: any, deviceIds: number[]) =>
    dispatch(updateSitesPU(customerId, site, deviceIds)),
  updateManufaturerCU: (manufacturer: any, deviceIds: number[]) =>
    dispatch(updateManufaturerCU(manufacturer, deviceIds)),
  updateManufaturerPU: (
    customerId: number,
    manufacturer: any,
    deviceIds: number[]
  ) => dispatch(updateManufaturerPU(customerId, manufacturer, deviceIds)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchStatuses: () => dispatch(fetchStatuses()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InventoryManagement);
