import React from 'react';

import SquareButton from '../../components/Button/button';
import Select from '../../components/Input/Select/select';
import ModalBase from '../../components/ModalBase/modalBase';

import './style.scss';

interface IAddDeviceFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: IInventoryFilters) => void;
  prevFilters: IInventoryFilters;
  sites: any[];
  types: any[];
  contractStatuses: IContractStatus[];
  manufacturers?: any;
  showReplacement?: boolean;
  statusList?: any[];
}

interface IAddDeviceFormState {
  filters: IInventoryFilters;
}

export default class AddDeviceForm extends React.Component<
  IAddDeviceFormProps,
  IAddDeviceFormState
> {
  constructor(props: IAddDeviceFormProps) {
    super(props);

    this.state = {
      filters: {
        site: [],
        type: [],
        status: [],
        is_managed: null,
        created_by : [],
        manufacturer: [],
        replacement: null,
        contractStatus: [],
        non_cisco: []
      },
    };
  }

  componentDidUpdate(prevProps: IAddDeviceFormProps) {
    const { show, prevFilters } = this.props;
    if (show && show !== prevProps.show) {
      this.setState({ filters: prevFilters || {} });
    }
  }

  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };

  getBody = () => {
    const manufacturers = this.props.manufacturers
      ? Object.keys(this.props.manufacturers).map(m => ({
          value: this.props.manufacturers[m].device_category_id,
          label: this.props.manufacturers[m].cw_mnf_name,
          disabled: this.props.manufacturers[m].cw_mnf_name ? false : true,
        }))
      : [];
    const sites = this.props.sites
      ? this.props.sites.map(site => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];
    const types = this.props.types
      ? this.props.types.map((type, index) => ({
          value: type,
          label: type ? type : 'N.A.',
        }))
      : [];
    const contractStatuses = this.props.contractStatuses
      ? this.props.contractStatuses.map(contractStatus => ({
          value: contractStatus.contract_status_id,
          label:
            contractStatus.contract_status === 'N.A.'
              ? 'Not Covered'
              : contractStatus.contract_status,
        }))
      : [];

    const statusList = this.props.statusList
      ? this.props.statusList.map(s => ({
          value: s.id,
          label: s.description,
        }))
      : [];
    const filters = this.state.filters;

    return (
      <div
        className="filters-modal__body col-md-12"
      >
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Device Monitoring</label>
            <div className="field__input">
              <Select
                name="is_managed"
                value={filters.is_managed}
                onChange={this.onFilterChange}
                options={[
                  { value: 'Managed', label: 'Managed' },
                  { value: 'Not Managed', label: 'Not Managed' },
                ]}
                multi={true}
                clearable={false}
                placeholder="Select Status"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Monitoring Source</label>
            <div className="field__input">
              <Select
                name="created_by"
                value={filters.created_by}
                onChange={this.onFilterChange}
                options={[
                  { value: 'Created By LogicMonitor', label: 'Created by LogicMonitor' },
                  { value: 'Created By Solarwinds', label: 'Created by Solarwinds' },
                ]}
                multi={true}
                clearable={false}
                placeholder="Select"
              />
            </div>
          </div>
        </div>

        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Site</label>
            <div className="field__input">
              <Select
                name="site"
                value={filters.site}
                onChange={this.onFilterChange}
                options={sites}
                multi={true}
                placeholder="Select Site"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Contract Status</label>
            <div className="field__input">
              <Select
                name="contractStatus"
                value={filters.contractStatus}
                onChange={this.onFilterChange}
                options={contractStatuses}
                multi={true}
                placeholder="Select Contract Status"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Status</label>
            <div className="field__input">
              <Select
                name="status"
                value={filters.status}
                onChange={this.onFilterChange}
                options={statusList}
                multi={true}
                placeholder="Select Status"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Type</label>
            <div className="field__input">
              <Select
                name="type"
                value={filters.type}
                onChange={this.onFilterChange}
                options={types}
                multi={true}
                placeholder="Select Type"
              />
            </div>
          </div>
        </div>
        {this.props.manufacturers &&
          this.props.manufacturers.length !== 0 && (
            <div className="field-section col-md-4">
              <div className="field__label">
                <label className="field__label-label">Manufacturer</label>
                <div className="field__input">
                  <Select
                    name="manufacturer"
                    value={filters.manufacturer}
                    onChange={this.onFilterChange}
                    options={manufacturers}
                    searchable={true}
                    placeholder="Select Manufacturer"
                    clearable={false}
                    multi={true}
                  />
                </div>
              </div>
            </div>
          )}
        {this.props.showReplacement &&
          this.props.showReplacement === true && (
            <div className="field-section col-md-4">
              <div className="field__label">
                <label className="field__label-label">Replacement</label>
                <div className="field__input">
                  <Select
                    name="replacement"
                    value={filters.replacement}
                    onChange={this.onFilterChange}
                    options={[
                      { value: 'Replacement: Available', label: 'Replacement: Available' },
                      { value: 'Replacement: Not Available', label: 'Replacement: Not Available' },
                    ]}
                    multi={true}
                    clearable={false}
                    placeholder="Select Status"
                  />
                </div>
              </div>
            </div>
          )}
          <div className="field-section col-md-4">
              <div className="field__label">
                <label className="field__label-label">Cisco/Non-cisco Devices</label>
                <div className="field__input">
                  <Select
                    name="non_cisco"
                    value={filters.non_cisco}
                    onChange={this.onFilterChange}
                    options={[
                      { value: 'Cisco Devices', label: 'Cisco Devices' },
                      { value: 'Non-Cisco Devices', label: 'Non-Cisco Devices' },
                    ]}
                    multi={true}
                    clearable={false}
                    placeholder="Select Status"
                  />
                </div>
              </div>
            </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
