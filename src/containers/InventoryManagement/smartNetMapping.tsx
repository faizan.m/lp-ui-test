import _, { cloneDeep, isArray } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import TagsInput from 'react-tagsinput';
import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import { fetchCustomerUsers } from '../../actions/customer';
import {
  addSite, ADD_SITE_SUCCESS, batchCreateImportData, batchCreateImportDataSmartNet, BATCH_CREATE_FAILURE, BATCH_CREATE_SUCCESS, fetchCountriesAndStates,
  fetchDateFormat,
  fetchDevices,
  fetchSites,
  fetchTaskStatus, FETCH_POTICKETS_SUCCESS, FETCH_RECIPIENTS_SUCCESS, getListPOTickts, getListPurchaseOrders, getRecipientLists, smartnetImportFileValidation,
  SMARTNET_VALIDATE_FAILURE, SMARTNET_VALIDATE_SUCCESS, TASK_STATUS_FAILURE,
  TASK_STATUS_SUCCESS, uploadDeviceFile, UPLOAD_DEVICE_FILE_SUCCESS
} from '../../actions/inventory';
import { fetchTerritoryMembers } from '../../actions/provider/integration';
import { fetchProviderUsers, FETCH_PROVIDER_USERS_SUCCESS } from '../../actions/provider/user';
import { getMSAgentAssociation, getSmartNetSettings, GET_MS_AGENT_ACC_SUCCESS } from '../../actions/setting';
import { createQuoteSmartnet, CREATE_QUOTES_SUCCESS, getQuoteBusinessList, getQuoteStageList, getQuoteStatusesList, getQuoteTypeList, GET_BUSINESS_SUCCESS, GET_STATUSES_SUCCESS } from '../../actions/sow';
import BackButton from '../../components/BackLink';
import SquareButton from '../../components/Button/button';
import Checkbox from '../../components/Checkbox/checkbox';
import Collapsible from '../../components/Collapsible/Collapsible';
import ConfirmBox from '../../components/ConfirmBox/ConfirmBox';
import Input from '../../components/Input/input';
import SelectInput from '../../components/Input/Select/select';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { fromISOStringToFormattedDate } from '../../utils/CalendarUtil';
import { commonFunctions } from '../../utils/commonFunctions';
import { exportCSVFile } from '../../utils/download';
import { searchInFields } from '../../utils/searchListUtils';
import AppValidators from '../../utils/validator';
import CreateSite from './addSite';
import ConfirmDevices from './confirmDevices';
import AddOpportunity from './createOpportunity';
import SiteMapping from './siteMapping';
import './style.scss';

const fields: any[] = [
  {
    label: 'Serial Number',
    value: 'serial_number',
    isRequired: true,
    isDate: false,
    isCheckBox: false,
    id: 1,
  },
  {
    label: 'Instance Number',
    value: 'instance_number',
    isRequired: true,
    isDate: false,
    isCheckBox: false,
    id: 2,
  },
  {
    label: 'Device Name',
    value: 'device_name',
    isRequired: true,
    isDate: false,
    isCheckBox: false,
    id: 3,
  },
  {
    label: 'Contract Number',
    value: 'service_contract_number',
    isDate: false,
    isRequired: true,
    isCheckBox: false,
    id: 4,
  },
  {
    label: 'Contract Type',
    value: 'contract_type',
    message: '',
    isDate: false,
    isCheckBox: false,
    id: 5,
  },
  {
    label: 'Contract Start Date',
    value: 'installation_date',
    message: '',
    isDate: true,
    isCheckBox: false,
    id: 6,
  },
  {
    label: 'Contract End Date',
    value: 'expiration_date',
    message: '',
    isRequired: true,
    isDate: true,
    isCheckBox: false,
    id: 7,
  },
  { label: 'Address', value: 'address', isRequired: true, id: 8, isDate: false },
  { label: 'Quantity', receiving_smartnet: true, value: 'quantity', id: 9, isRequired: true, isDate: false },
  { label: 'SNT SKU', receiving_smartnet: true, value: 'snt_sku', id: 10, isRequired: true, isDate: false },
  {
    header: 'Ignore Zero $ items',
    label: 'Price ',
    value: 'ignoreZero',
    isRequired: false,
    isDate: false,
    id: 11,
    isCheckBox: true,
  },
  {
    header: 'Filter by Product Type ',
    label: 'Product types ',
    value: 'product_types',
    isRequired: false,
    isDate: false,
    id: 12,
    isCheckBox: true,
  },
];


interface ISmartnetMappingProps extends ICommonProps {
  xlsxData: any;
  sites: ISite[];
  lastVisitedSite: string;
  fetchSites: (customerId: number) => any;
  customerId: number;
  addSite: (customerId: number, newSite: any) => any;
  batchCreateImportData: (customerId: number, deviceInfo: any) => any;
  smartnetImportFileValidation: (customerId: number, deviceInfo: any) => any;
  batchCreateImportDataSmartNet: (customerId: number, deviceInfo: any) => any;
  deviceSuccess: IDeviceCreateRes;
  customersShort: ICustomerShort[];
  countries: any;
  fetchCountriesAndStates: any;
  isPostingBatch: boolean;
  manufacturers: any[];
  fetchDevices: TFetchDevices;
  fetchDateFormat: any;
  dateFormats: any;
  xlsxFile: any;
  uploadDeviceFile: (fileReq: any, name: string) => Promise<any>;
  addErrorMessage: any;
  fetchTaskStatus: any;
  addSuccessMessage: any;
  getListPurchaseOrders: any;
  purchaseOrders: any[];
  purchaseOrderCorrection: any;
  fetchCustomerUsers: TFetchSingleCustomer;
  customerUsers: any;
  isFetchingUsers: boolean;
  getRecipientLists: any;
  getListPOTickts: any;
  createQuoteSmartnet: any;
  errorList: any;
  qStageList: any[];
  qTypeList: any[];
  getQuoteTypeList: any;
  getQuoteStageList: any;
  getQuoteStatusesList: any;
  getQuoteBusinessList: any;
  terretoryMembers: any;
  fetchTerritoryMembers: any;
  loggenInUser: any;
  smartNetSetting: ISmartNetSetting;
  getSmartNetSettings: any;
  getMSAgentAssociation: any;
  fetchProviderUsers: any;
}
interface ISmartnetMappingState {
  openSections: any;
  // selectedRows: number[];
  selectedRowsPO: number[];
  searchQueryField: any;
  searchQueryAddress: any;
  selectedPOTicketsList: any;
  checkboxList: any;
  rows: any[];
  poErrorRows: any[];
  deviceErrorRows: any[];
  missingDataRows: any[];
  xlsxData: any;
  columns: any[];
  deviceList: any;
  addressList: any[];
  deviceTypes: any[];
  sites: any[];
  customerId?: number;
  isCreateSiteModal: boolean;
  deviceSuccess: IDeviceCreateRes;
  isPostingSite: boolean;
  isopenConfirm: boolean;
  isopenConfirmDialog: boolean;
  errors: string[];
  manufacturer_id: number;
  receiving_smartnet: boolean;
  error: {
    manufacturer_id: IFieldValidation;
    contactEmails: IFieldValidation;
    contactEmailsError: IFieldValidation;
  };
  dateFormat: any;
  isopenFilterPrototype: boolean;
  taskId: any;
  loading: boolean;
  removeCustomer: boolean;
  emailNotification: boolean;
  includeMSAgent: boolean;
  customerUsers: any[];
  contactEmails: any[];
  validationData: any;
  filePath: string;
  poTickets: any[];
  emailRecipientList: any[];
  poTicketsRaw: any[];
  isCreateOpportunityModal: boolean;
  errorList: any;
  isPosting: boolean;
  opportunity: any;
  businessList: any[];
  statusesList: any[];
  providersContact: any;
  providerUsers: any;
  isopenSiteMapping: boolean;
  clearSite: boolean;
  searchString: string;
}

const sections = {
  'fieldMapping': true,
  'addressMapping': false,
  'purchaseOrder': false,
  'poTicketMapping': false,
  'correction': false,
  'emailSetting': false,
  'result': false
};

class SmartnetMapping extends React.Component<
  ISmartnetMappingProps,
  ISmartnetMappingState
> {

  constructor(props: ISmartnetMappingProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    // selectedRows: [],
    selectedRowsPO: [],
    openSections: sections,
    rows: [],
    poErrorRows: [],
    missingDataRows: [],
    deviceErrorRows: [],
    searchQueryField: [
      "Serial Number",
      "Instance ID",
      "Product Number",
      "Contract Number",
      "Service Level",
      "Start Date",
      "End Date",
      "End Customer Address 1",
      "Quantity",
      "Service SKU",
      null
  ],
    searchQueryAddress: [],
    selectedPOTicketsList: [],
    checkboxList: [null, null, null, null, null, null, null, null, null, null, null],
    dateFormat: [null,
    null,
    null,
    null,
    null,
    "%d/%m/%Y",
    "%d/%m/%Y"]
    ,
    // isAddressMappingOpen: false,
    isCreateSiteModal: false,
    xlsxData: null,
    columns: [],
    deviceList: [],
    addressList: [],
    deviceTypes: [],
    sites: [],
    deviceSuccess: {
      total_request_items_count: 0,
      successfully_updated_items_count: 0,
      successfully_created_items_count: 0,
      failed_updated_items_count: 0,
      failed_created_items_count: 0,
      import_warnings_file_info: {
        file_name: '',
        file_path: '',
      },
    },
    isPostingSite: false,
    isopenConfirm: false,
    isopenConfirmDialog: false,
    isopenFilterPrototype: false,
    manufacturer_id: null,
    receiving_smartnet: true,
    errors: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
    error: {
      manufacturer_id: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      contactEmails: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
      contactEmailsError: {
        errorState: IValidationState.SUCCESS,
        errorMessage: '',
      },
    },
    taskId: null,
    loading: false,
    emailNotification: false,
    includeMSAgent: false,
    removeCustomer: false,
    customerUsers: [],
    providersContact: [],
    contactEmails: [],
    validationData: {
      "purchase_order_receiving_result": {
        "result": false,
        "errors": {}
      },
      "device_import_result": {
        "result": false,
        "meta_data": {
          "successfully_updated_items_count": 0,
          "successfully_created_items_count": 0,
          "failed_updated_items_count": 0,
          "failed_created_items_count": 0
        }
      },
      "ticket_update_result": true,
      "email_notification_result": {
        "result": false,
        "recipient_list": []
      },
      "event_logs": []
    },
    filePath: '',
    poTickets: [],
    emailRecipientList: [],
    poTicketsRaw: [],
    isCreateOpportunityModal: false,
    errorList: [],
    isPosting: false,
    opportunity: {},
    businessList: [],
    statusesList: [],
    providerUsers: [],
    isopenSiteMapping: false,
    clearSite: false,
    searchString:'',
  });

  componentDidMount() {
    this.props.fetchCountriesAndStates();
    this.props.getQuoteStageList();
    this.props.getQuoteTypeList();
    this.getQuoteStatusesList();
    this.getQuoteBusinessList();
    this.props.fetchTerritoryMembers();
    this.props.getSmartNetSettings();
    if (!this.props.dateFormats) {
      this.props.fetchDateFormat();
    }
    if (!this.props.customerId) {
      this.props.history.push(this.props.lastVisitedSite);
    }
    if (this.props.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
    if (this.props.customerId !== null) {
      this.props.getListPurchaseOrders(this.props.customerId)
    }
    if (this.props.purchaseOrders) {
      const rows = this.getRows(this.props);
      this.setState({
        rows,
      });
    }

    if (this.props.purchaseOrderCorrection) {
      const poErrorRows = this.getCorrectionRows(this.props);
      const missingDataRows = this.getMissingDataRows(this.props);
      const deviceErrorRows = this.getDeviceErrorRows(this.props);
      this.setState({
        deviceErrorRows,
        poErrorRows,
        missingDataRows,
      });
    }
    if (this.props.customerId) {
      this.props.fetchCustomerUsers(this.props.customerId, {
        pagination: false,
      })
      this.getRecipientLists();
      this.getListPOTickts();
    }
  }

  componentDidUpdate(prevProps: ISmartnetMappingProps) {
    if (this.props.xlsxData !== prevProps.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
    if (this.props.customerId && this.props.customerId !== prevProps.customerId) {
      this.props.fetchSites(this.props.customerId);
    }
    if (this.props.deviceSuccess !== prevProps.deviceSuccess) {
      this.setState({ validationData: this.props.deviceSuccess });
    }
    if (this.props.purchaseOrders !== prevProps.purchaseOrders) {
      const rows = this.getRows(this.props);
      this.setState({ rows });
    }
    if (this.props.purchaseOrderCorrection !== prevProps.purchaseOrderCorrection) {
      const poErrorRows = this.getCorrectionRows(this.props);
      const missingDataRows = this.getMissingDataRows(this.props);
      const deviceErrorRows = this.getDeviceErrorRows(this.props);
      this.setState({
        deviceErrorRows,
        poErrorRows,
        missingDataRows
      });
    }
    if (this.props.customerUsers !== prevProps.customerUsers) {
      this.setUsers(this.props);
    }
  }  

  getRows = (
    nextProps: ISmartnetMappingProps,
  ) => {
    let pos = nextProps.purchaseOrders;

    const rows: any =
      pos &&
      pos.map((po, index) => ({
        poNumber: po.poNumber,
        poDate: po.poDate,
        endOfSupport: po.LDOS_date,
        customerCompany: _.get(po, 'vendorCompany.name', '-'),
        vendorContact: _.get(po, 'vendorContact.name', '-'),
        dateEntered: _.get(po, '_info.dateEntered', '-'),
        enteredBy: _.get(po, '_info.enteredBy', '-'),
        updatedBy: _.get(po, '_info.updatedBy', '-'),
        lastUpdated: _.get(po, '_info.lastUpdated', '-'),
        total: po.total,
        searched: false,
        id: po.id,
        index,
      }));

    return rows;
  };
  getCorrectionRows = (
    nextProps: ISmartnetMappingProps,
  ) => {
    let pos = _.get(nextProps, 'purchaseOrderCorrection.purchase_order_validation_result.validation_data', []);

    const rows: any =
      pos &&
      Object.keys(pos).map((po, index) => ({
        input_quantity: pos[po].input_quantity,
        cw_quantity: pos[po].cw_quantity || 'N.A.',
        ignored: pos[po].input_quantity === pos[po].cw_quantity ? true : false,
        is_valid: pos[po].is_valid,
        name: po,
        index,
      }));

    return rows;
  };
  getMissingDataRows = (
    nextProps: ISmartnetMappingProps,
  ) => {
    let pos = _.get(nextProps, 'purchaseOrderCorrection.purchase_order_validation_result.unmatched_line_items', []);

    const rows: any =
      pos &&
      pos.map((po, index) => ({
        product: po.product && po.product.identifier,
        description: po.description,
        quantity: po.quantity,
        vendorOrderNumber: po.vendorOrderNumber,
        receivedStatus: po.receivedStatus,
        index,
      }));

    return rows;
  };
  getMSAgentAssociation = () => {
    this.props.getMSAgentAssociation().then(action => {
      if (action.type === GET_MS_AGENT_ACC_SUCCESS) {
        this.setState({ providersContact: action.response });
        this.fetchProviderUsers();
      }
    });
  };
  getDeviceErrorRows = (
    nextProps: ISmartnetMappingProps,
  ) => {
    let pos = _.get(nextProps, 'purchaseOrderCorrection.device_import_validation_result.validated_data', []);

    const rows: any =
      pos &&
      pos.map((po, index) => ({
        row: po['Row #'],
        field: po['Field'],
        error_messages: po['Error Messages'],
        warnings: po['Warnings'],
        index,
      }));

    return rows;
  };
  setColumns = (nextProps: ISmartnetMappingProps) => {
    const xlsxDataResponce = nextProps.xlsxData;
    const columns = [];
    let index = 0;
    let tempKey = '';
    for (const key in xlsxDataResponce[0]) {
      if (xlsxDataResponce[0].hasOwnProperty(key)) {
        if (key.includes('EMPTY')) {
          tempKey = `column_${index + 1}`;
        }
        columns.push({ key: tempKey !== '' ? tempKey : key, value: key });
        index++;
        tempKey = '';
      }
    }
    this.setState({ columns });
  };

  setAddressList = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const addressList = [];
    xlsxDataResponce.map(data => {
      addressList.push(
        data[this.state.searchQueryField[7]] &&
          data[this.state.searchQueryField[7]] !== '-'
          ? data[this.state.searchQueryField[7]]
          : 'N.A.'
      );
    });
    const onlyUniqueAddress = addressList.filter(this.onlyUnique);
    const searchQueryAddress = onlyUniqueAddress.map(x=>{
      return this.getSiteIdByName(x)
    });
    this.setState({ addressList: onlyUniqueAddress,searchQueryAddress });
  };
  getSiteIdByName =(name)=>{
    const site = this.props.sites.filter(s=>s.name ===name)[0];
    return site && site.site_id || '';
  }
  getListPOTickts = () => {
    this.props.getListPOTickts(this.props.customerId).then(action => {
      if (action.type === FETCH_POTICKETS_SUCCESS) {
        const poTicketsRaw = action.response;
        const poTickets = poTicketsRaw
          ? poTicketsRaw.map(c => ({
            value: c.id,
            label: ` ${c.id}-${c.summary}`,
          }))
          : [];
        this.setState({ poTickets, poTicketsRaw })
      }
    });
  }

  getQuoteBusinessList = () => {
    this.props.getQuoteBusinessList().then(action => {
      if (action.type === GET_BUSINESS_SUCCESS) {
        const poTicketsRaw = action.response;
        const businessList = poTicketsRaw
          ? poTicketsRaw.map(c => ({
            value: c.id,
            label: ` ${c.label}`,
          }))
          : [];
        this.setState({ businessList })
      }
    });
  }

  getQuoteStatusesList = () => {
    this.props.getQuoteStatusesList().then(action => {
      if (action.type === GET_STATUSES_SUCCESS) {
        const poTicketsRaw = action.response;
        const statusesList = poTicketsRaw
          ? poTicketsRaw.map(c => ({
            value: c.id,
            label: `${c.label}`,
          }))
          : [];
        this.setState({ statusesList })
      }
    });
  }

  getRecipientLists = () => {
    this.props.getRecipientLists(this.props.customerId).then(action => {
      if (action.type === FETCH_RECIPIENTS_SUCCESS) {
        const emailRecipientList = action.response;
        this.setState({ emailRecipientList });
      }
    });
  }

  onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  toggleMappingOpen = field => {
    const openSections = {};
    Object.keys(this.state.openSections).map((f, index) => {
      openSections[f] = false;
    });
    openSections[field] = true;
    this.setState({ openSections });

  };

  toggleCreateSiteModal = () => {
    this.setState(prevState => ({
      isCreateSiteModal: !prevState.isCreateSiteModal,
      clearSite: false,
    }));
  };
  getFieldsOptions = () => {
    if (this.state.columns.length > 0) {
      return this.state.columns.map(role => ({
        value: role.value,
        label: role.key,
      }));
    } else {
      return [];
    }
  };

  getDateFormatOptions = () => {
    const formats = [];
    if (this.props.dateFormats) {
      Object.keys(this.props.dateFormats).map(key => {
        formats.push({
          value: this.props.dateFormats[key],
          label: key,
        });
      });

      return formats;
    } else {
      return [];
    }
  };

  getSitesOptions = () => {
    if (this.props.sites && this.props.sites.length > 0) {
      return this.props.sites.map(site => ({
        label: commonFunctions.getAddress(site),
        value: site.site_id,
      }));
    } else {
      return [];
    }
  };

  onSiteAdd = (newSite: any) => {
    this.setState({ isPostingSite: true, clearSite: false });
    const customerId = this.props.customerId;
    if (customerId) {
      this.props
        .addSite(customerId, newSite)
        .then(action => {
          if (action.type === ADD_SITE_SUCCESS) {
            this.setState({
              isCreateSiteModal: false,
              clearSite: true,
            });

            this.props.fetchSites(customerId);
          } else if(action.errorList.status === 400 ){
            const msg = commonFunctions.getErrorMessage(action.errorList.data);
            this.props.addErrorMessage(msg)
          }
          this.setState({ isPostingSite: false });
        })
        .catch(() => {
          this.setState({ isPostingSite: false });
        });
    }
  };

  onClickCancel = () => {
    this.props.history.push(this.props.lastVisitedSite);
  };

  handleChangeField = (event: any, index?: any) => {
    const searchQueryField = this.state.searchQueryField;
    searchQueryField[index] = event.target.value;

    this.setState({
      searchQueryField,
    });

    if (index === 11) {
      this.setDeviceTypes();
    }
    if (index === 7) {
      this.setState({ searchQueryAddress: [] })
    }
  };

  handleChangeDateFormat = (event: any, index?: any) => {
    const dateFormat = this.state.dateFormat;
    dateFormat[index] = event.target.value;

    this.setState({
      dateFormat,
    });
  };

  handleSearchChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
       this.filterOrders()
    });
  };


  filterOrders = () => {
    let filteredOrders = this.state.rows;

    if (this.state.searchString.length > 0) {
      filteredOrders = filteredOrders.map( row =>
         {
          row.searched = searchInFields(row, this.state.searchString, [
            "poNumber",
          ])
          // row.selected = this.state.selectedRows.includes(row.id)
          return row;
         }
      )
      filteredOrders.sort
    }
     filteredOrders.sort((a, b) => Number(b.searched) - Number(a.searched));
     this.setState({rows: filteredOrders})
  };

  handleChangeAddress = (event: any, index?: any) => {
    if (event.target.value === '0') {
      this.toggleCreateSiteModal();
    } else {
      const searchQueryAddress = this.state.searchQueryAddress;
      searchQueryAddress[index] = event.target.value;

      this.setState({
        searchQueryAddress,
      });
    }
  };

  onClickImport = (event: any, index?: any) => {
    if (this.validateForm()) {
      this.setAddressList();
      this.toggleMappingOpen('addressMapping');
    }
  };

  setDeviceTypes = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const deviceTypes = [];
    const uniqueTypes = [];
    let isBlank = false;
    xlsxDataResponce.map(data => {
      deviceTypes.push(data[this.state.searchQueryField[11]]);
    });
    const onlyUniqueTypes = deviceTypes.filter(this.onlyUnique);
    onlyUniqueTypes.map(type => {
      if (type !== '' && type !== '-') {
        uniqueTypes.push({
          product_type: type,
          include: true,
          ignore_zero_dollar_items: this.state.checkboxList[10]
            ? this.state.checkboxList[10]
            : false,
          include_non_serials: false,
        });
      } else {
        isBlank = true;
      }
    });
    if (isBlank) {
      uniqueTypes.push({
        product_type: 'N.A.',
        include: true,
        ignore_zero_dollar_items: this.state.checkboxList[10]
          ? this.state.checkboxList[10]
          : false,
        include_non_serials: false,
      });
    }
    this.setState({ deviceTypes: uniqueTypes });
  };

  validateForm = () => {
    let isValid = true;
    const newState = cloneDeep(this.state);
    fields.map((field, index) => {
      newState.errors[index] = '';
    });
    newState.error.manufacturer_id.errorState = IValidationState.SUCCESS;
    newState.error.manufacturer_id.errorMessage = '';


    newState.error.manufacturer_id.errorState = IValidationState.SUCCESS;
    newState.error.manufacturer_id.errorMessage = '';
    if (!this.state.manufacturer_id) {
      newState.error.manufacturer_id.errorState = IValidationState.ERROR;
      newState.error.manufacturer_id.errorMessage =
        'Please select manufacturer ';
      isValid = false;
    }

    fields.map((field, index) => {
      const inValidField = !this.state.columns.map(f => f.value).includes(this.state.searchQueryField[index]);
      if (field.isRequired && (!this.state.searchQueryField[index] || inValidField)) {
        newState.errors[index] = `${field.label} is required`;
        isValid = false;
      }
      if (
        this.state.searchQueryField[index] &&
        field.isDate &&
        !this.state.dateFormat[index]
      ) {
        newState.errors[index] = `Please select Date format`;
        isValid = false;
      }
    });

    if (this.state.checkboxList[11] && !this.state.searchQueryField[11]) {
      newState.errors[11] = 'Please select field';
      isValid = false;
    }
    this.setState(newState);

    return isValid;
  };

  isValidMapping = () => {
    let isValid = false;
    if (this.state.searchQueryAddress.filter(x =>x).length === this.state.addressList.length) {
      isValid = true;
    }

    return isValid;
  };

  checkValidationNext = (event: any, section: any, isValid: any) => {
    if (isValid) {
      this.toggleMappingOpen(section);
    }
  };
  clearPOSelection = (event: any, section) => {
    // this.setState({ selectedRows: [] });
    this.toggleMappingOpen(section);
  };

  createFinalDeviceObject = () => {
    const addressSiteObject = [];
    this.state.searchQueryAddress.map((data, i) => {
      addressSiteObject.push({
        site_id: data,
        address: this.state.addressList[i],
      });
    });
    const poTicketsObject = {};

    this.state.rows.filter(x=>x.selected).map((data, i) => {
      poTicketsObject[data.id] = data.poTicket
    });
    const deviceInfo = {
      file_name: this.props.xlsxFile && this.props.xlsxFile.name,
      field_mappings: {
        serial_number: this.state.searchQueryField[0]
          ? this.state.searchQueryField[0]
          : null,
        device_name: this.state.searchQueryField[2]
          ? this.state.searchQueryField[2]
          : null,
        service_contract_number: this.state.searchQueryField[3]
          ? this.state.searchQueryField[3]
          : null,
        contract_type: this.state.searchQueryField[4],
        purchase_date: null,
        purchase_date_format: null,
        installation_date: this.state.searchQueryField[5]
          ? this.state.searchQueryField[5]
          : null,
        installation_date_format: this.state.dateFormat[5]
          ? this.state.dateFormat[5]
          : null,
        expiration_date: this.state.searchQueryField[6]
          ? this.state.searchQueryField[6]
          : null,
        expiration_date_format: this.state.dateFormat[6]
          ? this.state.dateFormat[6]
          : null,
        address: this.state.searchQueryField[7]
          ? this.state.searchQueryField[7]
          : null,
        quantity: this.state.searchQueryField[8]
          ? this.state.searchQueryField[8]
          : null,
        snt_sku: this.state.searchQueryField[9]
          ? this.state.searchQueryField[9]
          : null,
        device_price: this.state.searchQueryField[10]
          ? this.state.searchQueryField[10]
          : null,
        product_type: this.state.searchQueryField[11]
          ? this.state.searchQueryField[11]
          : null,
        instance_number: this.state.searchQueryField[1]
          ? this.state.searchQueryField[1]
          : null,
      },
      manufacturer_id: this.state.manufacturer_id,
      ignore_zero_dollar_items: this.state.checkboxList[10]
        ? this.state.checkboxList[10]
        : false,
      filter_by_product_type: this.state.checkboxList[11]
        ? this.state.checkboxList[11]
        : false,
      device_type_filter_meta: this.state.deviceTypes
        ? this.state.deviceTypes
        : null,
      address_field_meta: addressSiteObject ? addressSiteObject : null,
      selected_purchase_orders: this.state.rows.filter(x=>x.selected).map(x=>x.id),
      email_notification_meta: {
        send_to_customer: false,
        extra_recipient_email: [],
        ms_recipients_cco_ids: []
      },
      purchase_order_ticket_mapping: poTicketsObject
    };
    if (this.state.searchQueryField[10] === '' || this.state.searchQueryField[10] === null) {
      delete deviceInfo.field_mappings.device_price;
    }
    if (!this.state.searchQueryField[4]) {
      delete deviceInfo.field_mappings.contract_type;
    }
    return deviceInfo;
  }

  onClickImportSave = (event: any, index?: any) => {
    if (this.isValidMappingPOTickets()) {
      this.setState({ loading: true })
      const deviceInfo = this.createFinalDeviceObject();

      if (this.props.xlsxFile) {
        const data = new FormData();
        data.append('filename', this.props.xlsxFile);
        this.props
          .uploadDeviceFile(
            data,
            this.props.xlsxFile && this.props.xlsxFile.name
          )
          .then(action => {
            if (action.type === UPLOAD_DEVICE_FILE_SUCCESS) {
              deviceInfo.file_name = action.response;
              this.setState({ filePath: action.response })
              if (this.props.customerId) {
                this.props
                  .smartnetImportFileValidation(this.props.customerId, deviceInfo)
                  .then(a => {
                    if (a.type === SMARTNET_VALIDATE_SUCCESS) {

                     this.toggleMappingOpen('correction');
                     
                    }
                    if (a.type === SMARTNET_VALIDATE_FAILURE) {
                      this.toggleMappingOpen('correction');
                      this.props.addErrorMessage(a.errorList.data.import_file || JSON.stringify(a.errorList.data));
                    }
                  });
              }
            }
            this.setState({ loading: false })

          });
      }
      this.getMSAgentAssociation();
    }
  };

  fetchProviderUsers = () => {
    this.props.fetchProviderUsers({ pagination: false }).then(action => {
      if (action.type === FETCH_PROVIDER_USERS_SUCCESS) {
        const newState = cloneDeep(this.state);
        const filtered = action.response.filter(x => this.state.providersContact.user.includes(x.id)).map(x=> {
          x.checked = true
          return x;
        });
        (newState.providerUsers as any)= filtered;
        this.setState(newState);
      }
    });
  };

  finalSubmit = () => {
    if (this.isValidMapping() && this.validateEmailPopup()) {
      let deviceInfo = this.createFinalDeviceObject();
      this.setState({ isopenConfirmDialog: false });
      if (!this.state.removeCustomer) {
        const emailsCustomers = this.state.customerUsers.filter(c => c.checked).map(c => c.email);
        const users = this.state.providerUsers.filter(c => c.checked).map(c => c.profile.cco_id);
        const allEmail = emailsCustomers.concat(this.state.contactEmails)
        deviceInfo.email_notification_meta = {
          extra_recipient_email: allEmail,
          send_to_customer: true,
          ms_recipients_cco_ids: users,
        }
      } else {
        delete deviceInfo.email_notification_meta;
      }

      if (this.props.customerId) {
        deviceInfo.file_name = this.state.filePath;

        this.props
          .batchCreateImportDataSmartNet(this.props.customerId, deviceInfo)
          .then(a => {
            if (a.type === BATCH_CREATE_SUCCESS) {
              if (a.response && a.response.task_id) {
                this.setState({ taskId: a.response.task_id });
                this.fetchTaskStatus();
              }
            }
            if (a.type === BATCH_CREATE_FAILURE) {
              this.toggleMappingOpen('correction');
              this.props.addErrorMessage(a.errorList.data.import_file);
            }
          });
      }
    }
  };

  fetchTaskStatus = () => {
    if (this.state.taskId) {
      this.props.fetchTaskStatus(this.state.taskId).then(a => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage('Bulk import completed.');
            this.props.fetchDevices(this.props.customerId, false);
            this.toggleMappingOpen('result')

          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage('Bulk import failed.');
            this.toggleMappingOpen('result')
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatus();
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.fetchDevices(this.props.customerId, false);
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState(prevState => ({
      [targetName]: targetValue,
    }));
  };

  onChecboxChangedType = (event: any, index: any, column: string) => {
    const deviceTypes = this.state.deviceTypes;
    deviceTypes[index][column] = event.target.checked;
    if (column === 'include' && event.target.checked === false) {
      deviceTypes[index].ignore_zero_dollar_items = false;
      deviceTypes[index].include_non_serials = false;
    }
    this.setState({
      deviceTypes,
    });
  };

  onChecboxChangedReceiving = (event: any) => {
    const active = event.target.checked;
    if (active === false) {
      this.props.history.push('/inventory-management/field-mapping')
    }
  };
  toggleCreateOpportunityModal = () => {
    this.setState(prevState => ({
      isCreateOpportunityModal: !prevState.isCreateOpportunityModal,
    }));
  };

  createQuote = (data: any) => {
    this.setState({ isPosting: true });

    const customerId = this.props.customerId;
    if (customerId) {
      (data.customer_id as any) = customerId;
      (data.close_date as any) = _.get(this.state.validationData, 'expiration_date', '');
      (data.contact_id as any) = _.get(this.state.validationData, 'contact_id', '');
      delete data.user_id;
      this.props
        .createQuoteSmartnet(customerId, data)
        .then(action => {
          if (action.type === CREATE_QUOTES_SUCCESS) {
            this.setState({
              isCreateOpportunityModal: false,
            });
            this.props.addSuccessMessage('Opportunity created.')

          } else {
            this.setState({
              isCreateOpportunityModal: true,
              errorList: action.errorList.data,
              opportunity: data
            });
            console.info('opportunity', action.errorList.data);
            this.props.addErrorMessage('something went wrong!!!')
          }
          this.setState({ isPosting: false });
        })
        .catch(() => {
          this.setState({ isPosting: false });
        });
    }
  };
  getSmartnetMapping = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(manufacturer => ({
        value: manufacturer.id,
        label: manufacturer.label,
      }))
      : [];

    return (
      <div className="field-mapping">
        <h3>Field Mapping</h3>
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-row">
              <td className="first-manufacturer">

                <Checkbox
                  isChecked={this.state.receiving_smartnet}
                  name="option"
                  onChange={e => this.onChecboxChangedReceiving(e)}
                >
                  Receiving Smartnet
                </Checkbox>
              </td>
              <td className="middle-manufacturer">
              </td>
              <td className="sheet-column" />
              <td className="last" />
              <th className="" />
            </tr>
            <tr className="field-mapping__table-row">
              <td className="first-manufacturer">
                Select Manufacturer
                <span className="field-mapping__label-required" />
                {this.state.error.manufacturer_id.errorMessage ? (
                  <span className="field-mapping__label-error">
                    {this.state.error.manufacturer_id.errorMessage}
                  </span>
                ) : null}
              </td>
              <td className="middle-manufacturer">
                <img src="/assets/new-icons/oppositearrow.svg" />
              </td>
              <td className="sheet-column">
                {
                  <SelectInput
                    name="manufacturer_id"
                    value={this.state.manufacturer_id}
                    onChange={this.handleChange}
                    options={manufacturers}
                    searchable={true}
                    placeholder="Manufacturer"
                    clearable={false}
                  />
                }
              </td>
              <td className="last" />
              <th className="" />
            </tr>

          </tbody>
        </table>
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-header">
              <th className="first">Map Field</th>
              <th className="middle" />
              <th className="last"> Sheet Columns </th>
              <th className="" />
            </tr>

            {fields.map((field, index) => {

              if (field.receiving_smartnet && !this.state.receiving_smartnet) {
                return false;
              }
              return (
                <tr key={index} className={`field-mapping__table-row ${field.receiving_smartnet && !this.state.receiving_smartnet ? 'disabled' : ''}`}>
                  {field.isCheckBox && (
                    <td className="first">
                      <Checkbox
                        isChecked={this.state.checkboxList[index]}
                        name="option"
                        onChange={e => this.onChecboxChanged(e, index)}
                      >
                        {field.header}
                      </Checkbox>
                    </td>
                  )}

                  <td className="first">
                    {((field.isCheckBox && this.state.checkboxList[index]) ||
                      !field.isCheckBox) &&
                      field.label}

                    {field.message ? (
                      <div className="field-mapping__label-message">
                        {field.message}
                      </div>
                    ) : null}
                    {field.isRequired && (
                      <span className="field-mapping__label-required" />
                    )}
                    {field.isCheckBox && this.state.checkboxList[index] && (
                      <img
                        className="opposite-arrow"
                        src="/assets/new-icons/oppositearrow.svg"
                      />
                    )}
                    {field.message ? (
                      <div className="field-mapping__label-message">
                        {field.message}
                      </div>
                    ) : null}
                    {this.state.errors[index] ? (
                      <span className="field-mapping__label-error">
                        {this.state.errors[index]}
                      </span>
                    ) : null}
                  </td>
                  {!field.isCheckBox && (
                    <td className="middle">
                      <img src="/assets/new-icons/oppositearrow.svg" />
                    </td>
                  )}
                  {((field.isCheckBox && this.state.checkboxList[index]) ||
                    !field.isCheckBox) && (
                      <td className="sheet-column">
                        <SelectInput
                          name="select"
                          value={this.state.searchQueryField[index]}
                          onChange={event => this.handleChangeField(event, index)}
                          options={this.getFieldsOptions()}
                          searchable={true}
                          placeholder="Select field"
                          clearable={true}
                        />
                      </td>
                    )}
                  {field.isDate && (
                    <td className="last">
                      <SelectInput
                        name="select"
                        value={this.state.dateFormat[index]}
                        onChange={event =>
                          this.handleChangeDateFormat(event, index)
                        }
                        options={this.getDateFormatOptions()}
                        searchable={true}
                        placeholder="Select Date Format"
                        clearable={false}
                        disabled={!this.state.searchQueryField[index]}
                      />
                    </td>
                  )}

                  <th className="" />
                  <th className="" />
                  <th className="" />
                </tr>
              )

            })}
          </tbody>
        </table>
        {this.state.deviceTypes && this.state.deviceTypes.length > 0 && (
          <table className="field-mapping__table">
            <tbody>
              <tr className="field-mapping__table-header">
                <th className="first">Type</th>
                <th className="first">Import</th>
                {this.state.checkboxList[10] && (
                  <th className="first">Ignore zero $</th>
                )}
                {
                  !this.state.receiving_smartnet &&
                  <th className="first">Include Non-serial</th>

                }
                <th className="" />
              </tr>

              {this.state.deviceTypes.map((type, index) => (
                <tr key={index} className="field-mapping__table-row">
                  <td className="device-type">{type.product_type}</td>
                  <td className="first">
                    <Checkbox
                      isChecked={this.state.deviceTypes[index].include}
                      name="option"
                      onChange={e =>
                        this.onChecboxChangedType(e, index, 'include')
                      }
                    />
                  </td>
                  {this.state.checkboxList[10] && (
                    <td className="first">
                      <Checkbox
                        isChecked={
                          this.state.deviceTypes[index].ignore_zero_dollar_items
                        }
                        name="option"
                        onChange={e =>
                          this.onChecboxChangedType(
                            e,
                            index,
                            'ignore_zero_dollar_items'
                          )
                        }
                        disabled={!type.include}
                      />
                    </td>
                  )}
                  {
                    !this.state.receiving_smartnet &&
                    <td className="first">
                      <Checkbox
                        isChecked={
                          this.state.deviceTypes[index].include_non_serials
                        }
                        name="option"
                        onChange={e =>
                          this.onChecboxChangedType(
                            e,
                            index,
                            'include_non_serials'
                          )
                        }
                        disabled={!type.include}
                      />
                    </td>
                  }

                  <td className="" />
                </tr>
              ))}
            </tbody>
          </table>
        )}

        <div className="field-mapping__footer">
          <SquareButton
            onClick={this.onClickCancel}
            content="Cancel"
            bsStyle={ButtonStyle.DEFAULT}
            className="import-button-1"
          />
          <SquareButton
            onClick={this.onClickImport}
            content="Next"
            bsStyle={ButtonStyle.PRIMARY}
            className="import-button-1"
          />
        </div>
      </div>
    );
  };

  onChecboxChanged = (event: any, index?: any) => {
    const checkboxList = this.state.checkboxList;
    const searchQueryField = this.state.searchQueryField;
    let deviceTypes = this.state.deviceTypes;
    if (index === 11) {
      deviceTypes = [];
    }
    if (index === 10) {
      deviceTypes = deviceTypes.map((item, i) => {
        if (item.include) {
          item.ignore_zero_dollar_items = event.target.checked;
        }

        return item;
      });
    }
    checkboxList[index] = event.target.checked;

    searchQueryField[index] = '';
    const errors = this.state.errors;
    errors[10] = '';
    errors[11] = '';
    this.setState({
      checkboxList,
      searchQueryField,
      errors,
      deviceTypes,
    });
  };

  toggleConfirmOpen = () => {
    this.setState(prevState => ({
      isopenConfirm: !prevState.isopenConfirm,
    }));
  };



  toggleOpenConfirmDialog = () => {

    if (this.state.deviceErrorRows.filter(data => data.error_messages).length === 0 &&
      this.getCorrectionRows(this.props).filter(row => !row.ignored).length === 0) {
      this.toggleMappingOpen('emailSetting');
    } else {
      this.setState(prevState => ({
        isopenConfirmDialog: !prevState.isopenConfirmDialog,
      }));
    }

  };

  onClickConfirm = () => {
    this.toggleMappingOpen(1);
    this.toggleConfirmOpen();
  };
  renderErrorAddressMapping = (errorText: string = 'Please map all site name with site.') => {
    return (
      <div className={'purchase-order-top'}>
        {this.state.searchQueryAddress.filter(x=>x).length !== this.state.addressList.length && errorText}
      </div>
    );
  };
  getAddressMapping = () => {
    return (
      <div className="field-mapping address-mapping">
        <h3>Site Mapping</h3>

        <div>
          <table className="field-mapping__table">
            <tbody>
              <tr className="field-mapping__table-row" />
              <tr className="field-mapping__table-header">
                <th className="first">Site</th>
                <th className="middle" />
                <th className="last">Saved Site</th>
                <th className="" >
                    {
                      this.addressValidSelection() &&
                      <SquareButton
                      onClick={this.toggleSiteMapping}
                      content="Auto Site Creation"
                      bsStyle={ButtonStyle.PRIMARY}
                      className="auto-site-creation"
                   />
                    }
                 
           </th>
              </tr>

              {this.state.addressList &&
                this.state.addressList.map((field, index) => (
                  <tr key={index} className="field-mapping__table-row">
                    <td className="first">{field}</td>
                    <td className="middle">
                      <img src="/assets/new-icons/oppositearrow.svg" />
                    </td>
                    <td className="last site-address-select">
                      {
                        <SelectInput
                          name="select"
                          value={this.state.searchQueryAddress[index]}
                          onChange={event =>
                            this.handleChangeAddress(event, index)
                          }
                          options={this.getSitesOptions().concat([
                            {
                              value: '0',
                              label: 'Create new site',
                            },
                          ])}
                          searchable={true}
                          placeholder="Select field"
                          clearable={true}
                        />
                      }
                    </td>
                    <th className="" />
                  </tr>
                ))}
            </tbody>
          </table>
          {this.renderErrorAddressMapping()}
          <div className="field-mapping__footer">
            <SquareButton
              onClick={e => this.toggleMappingOpen('fieldMapping')}
              content="Back"
              bsStyle={ButtonStyle.DEFAULT}
              className="import-button-1"
            />
            <SquareButton
              onClick={e => this.checkValidationNext(e, 'purchaseOrder', this.isValidMapping())}
              content="Next"
              bsStyle={ButtonStyle.PRIMARY}
              className="import-button-1"
              disabled={this.state.searchQueryAddress.filter(x =>x).length !== this.state.addressList.length}
            />
          </div>
        </div>
      </div>
    );
  };



  onRowsToggle = selectedRows => {
    const newState = this.state;

    // this.state.rows.map((row, i) => {
    //   newState.rows[i].selected = this.state.selectedRows.includes(row.id) ? true : false;
    // });

    selectedRows.map((id, index) => {
      this.state.rows.map((row, i) => {
        if (row.id === id) {
          newState.rows[i].selected = true;
        }
      })
    });



    this.setState({
      rows: newState.rows,
    }, () => {
      // this.setState({
      //   selectedRows,
      // });
    });
  };


  renderError = (errorText: string = 'Please select Purchase order and select ticket') => {
    return (
      <div className={'purchase-order-top'}>
        {errorText}
      </div>
    );
  };
  onRowsTogglePO = selectedRowsPO => {
    this.setState({
      selectedRowsPO,
    });
  };

  getPurchaseOrder = () => {

    return (
      <div className="field-mapping po-mapping">
        <div className="smartnet-mapping-header">
          <h3>Purchase orders</h3>
          <Input
            field={{
              label: '',
              type: InputFieldType.SEARCH,
              value: this.state.searchString,
              isRequired: false,
            }}
            width={3}
            placeholder="Search"
            name="searchString"
            onChange={this.handleSearchChange}
            className="po-smartnet_search"
          />
        </div>
        <div>
          <div className="po-table">
           <div className="po-line-header">
                    <div className={`po_number`}>PO Number</div>
                    <div className='selection'>Ticket</div>
                  <div className="column">Customer Company</div>
                  <div className="column">Vendor Contact</div>
                  <div className="column">Updated By</div>
                  <div className="column">Last Updated</div>
                  <div className="column">PO Date</div>
                  <div className="column">Total</div>
              </div>
          {
            this.state.rows.map((po,index)=>{
              return(
                 <div className={`po-line ${ index %2 ? 'odd': 'even'}`} key={index}>
                    <div className={`po_number ${po.searched ? 'searched': 'not-searched'}`}>
                    <Checkbox
                    isChecked={po.selected}
                    name="selected"
                    onChange={e => this.handleChangePOSelection(e, index)}
                    >{po.poNumber}</Checkbox>
                  </div>
                    <div className='column'>{
                        po.selected &&
                            <div
                              className={`select-inner-po
                              ${po.errorService ? 'required-error' : ' '}
                            `}
                            >
                              <SelectInput
                                name={po.id.toString()}
                                value={po.poTicket}
                                onChange={e => this.handleChangePOTickets(e, index)}
                                options={this.state.poTickets}
                                multi={false}
                                placeholder="Select Ticket"
                              />
                            </div>
                      }
                  </div>
                  <div className="column">{po.customerCompany}</div>
                  <div className="column">{po.vendorContact}</div>
                  <div className="column">{po.updatedBy}</div>
                  <div className="column">{fromISOStringToFormattedDate(po.lastUpdated)}</div>
                  <div className="column">{fromISOStringToFormattedDate(po.poDate)}</div>
                  <div className="column">{po.total}</div>
              </div>
              )
            })
          }
          </div>
          {this.state.rows.map(x=>x.selected).length === 0 && this.renderError() ||
            (this.state.rows.filter(row => row.errorService !== '')
              && this.state.rows.filter(row => row.errorService !== '').length > 0 &&
              this.renderError(this.state.rows.filter(row => row.errorService !== '')[0].errorService))}
          <div className="field-mapping__footer">
            <SquareButton
              onClick={e => this.clearPOSelection(e, 'addressMapping')}
              content="Back"
              disabled={this.props.isPostingBatch || this.state.loading}
              bsStyle={ButtonStyle.DEFAULT}
              className="import-button-1"
            />
            <SquareButton
              onClick={this.onClickImportSave}
              content="Submit"
              bsStyle={ButtonStyle.PRIMARY}
              className="import-button-1"
              disabled={this.props.isPostingBatch || this.state.loading || this.state.rows.filter(x=>x.selected).length === 0 || this.props.isPostingBatch}
            />
          </div>
        </div>
      </div>
    );
  };

  isValidMappingPOTickets = () => {
    let isValid = true;
    const newState = this.state;

    this.state.rows.map((row, i) => {
      newState.rows[i].errorService = '';
    });

    this.state.rows.map((row, i) => {
      if (row.selected === true && !row.poTicket) {
        newState.rows[i].errorService = 'Please Select Ticket';
        isValid = false;
      }
    })
    this.setState(newState);

    return isValid;
  };


  handleChangePOTickets = (event: any, index?: any) => {
    const rows = this.state.rows;
    rows[index].poTicket = event.target.value;

    this.setState({
      rows,
    });
  };

  handleChangePOSelection = (event: any, index?: any) => {
    const targetValue = event.target.checked;
    const rows = this.state.rows;
    rows[index].selected = targetValue;
    this.setState({
      rows,
    });
  };

  downloadReport = response => {
    if (response && response !== '') {
      const url = response;
      const link = document.createElement('a');
      link.href = url;
      link.target = '_blank';
      link.setAttribute('download', `error-report-${new Date().toUTCString()}`);
      document.body.appendChild(link);
      link.click();
    }
  };

  downloadJSONFile = response => {
    if (response && response !== '') {
      var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(response, null, 4));
      const link = document.createElement('a');
      link.href = dataStr;
      link.target = '_blank';
      link.setAttribute('download', `error-report-${new Date().toUTCString()}.json`);
      document.body.appendChild(link);
      link.click();
    }
  };

  setPOIgnore = (event: any, data?: any) => {
    const poErrorRows = this.state.poErrorRows;
    poErrorRows[data.index].ignored = true;

    this.setState({
      poErrorRows,
    });
  };


  setPOIgnoreAll = (event: any) => {
    const poErrorRows = this.state.poErrorRows;
    poErrorRows.map((row, index) => {
      poErrorRows[index].ignored = true;
    })

    this.setState({
      poErrorRows,
    });
  };
  getDiscrepancyIcons = data => {
    return (
      data.original.input_quantity !== data.original.cw_quantity && !data.original.ignored && (
        <>
          <img
            src={`/assets/icons/cancel.svg`}
            alt=""
            className="status-heartbit-images"
            title={`Please Ignore Purchase Order`}
          />
          <SquareButton
            onClick={e => this.setPOIgnore(e, data.original)}
            content={`Ignore`}
            bsStyle={ButtonStyle.PRIMARY}
            className="ignore-button"
            title={`Please click here to Ignore Purchase Order`}
          />
        </>
      ) ||

      data.original.input_quantity !== data.original.cw_quantity && (
        <img
          src={`/assets/icons/caution.svg`}
          alt=""
          className="status-heartbit-images"
          title={` Purchase Order Ignored`}
        />
      )
    )
  }

  onClickConfirmDialog = e => {
    if (this.state.deviceErrorRows.filter(data => data.error_messages).length === 0 && this.state.poErrorRows.filter(row => !row.ignored).length === 0) {
      this.toggleMappingOpen('emailSetting');
      this.toggleOpenConfirmDialog();
      this
    } else {
      this.props.history.push(this.props.lastVisitedSite);
    }
  };

  customTopBar = () => {
    return (
      <div className="heading-po">
        <span>Purchase Order Validation </span>
        <div className="actions">
          <SquareButton
            onClick={e => this.download()}
            content={`Download`}
            bsStyle={ButtonStyle.DEFAULT}
            className="ignore-button"
            title={`Click here to downlod Purchase Order Validation data`}
            disabled={this.state.poErrorRows.length === 0}
          />
          <SquareButton
            onClick={e => this.setPOIgnoreAll(e)}
            content={`Ignore All`}
            bsStyle={ButtonStyle.PRIMARY}
            className="ignore-button"
            title={`Ignore All Invalid Purchase Order`}
            disabled={this.state.poErrorRows.length === 0}
          />
        </div>
      </div>)
  }


  customTopBarDevice = () => {
    return (
      <div className="device-errors-table">
        <span> Device Errors
          <div> {
            this.state.deviceErrorRows.filter(data => data.error_messages).length === 0 ? '' : <div className="device-errors">Please fix all device error from sheet to process this import</div>
          }</div>
          <div> {
            this.state.deviceErrorRows.filter(data => data.error_messages).length === 0 &&
              this.state.deviceErrorRows.filter(data => data.warnings).length ? <div className="device-warnings">Warnings from sheet</div> : ''
          }</div>
        </span>
        <div className="actions">
          <SquareButton
            onClick={e => this.downloadDevice()}
            content={`Download`}
            bsStyle={ButtonStyle.DEFAULT}
            className="ignore-button"
            title={`Click here to downlod Device Errors data`}
            disabled={this.state.deviceErrorRows.length === 0}
          />
        </div>
      </div>)
  }
  downloadDevice = () => {
    let headers = {
      row: 'Row #',
      Field: 'Field',
      error_messages: 'Error Messages',
      warnings: 'Warnings',
    };
    let itemsFormatted = [];
    let itemsNotFormatted = this.state.deviceErrorRows;
    itemsNotFormatted.forEach((item) => {
      itemsFormatted.push({
        row: item.row || 'N.A.',
        field: item.field || 'N.A.',
        error_messages: item.error_messages || '',
        warnings: item.warnings || '',
      });
    });

    var fileTitle = 'device-errors-data';
    exportCSVFile(headers, itemsFormatted, fileTitle);
  }
  renderCorrection = () => {
    const columnPO: any = [
      {
        accessor: 'name', Header: 'SNT/SKU',
        Cell: data => <div className="pl-15" title={`Name : ${data.original.name}`}>{data.value}</div>,
      },
      {
        accessor: 'input_quantity', Header: 'Input Quantity',
        Cell: data => <div className="pl-15" title={`Name : ${data.original.name}`}>{data.value}</div>,
      },
      {
        accessor: 'cw_quantity', Header: 'CW Quantity',
        Cell: data => <div className={`pl-15 ${data.value > 0 ? '' : 'device-errors'}`} title={`${data.value > 0 ? '' : 'No matching Product found in connectwise for the selected purchase orders'}`}>{data.value}</div>,
      },
      {
        accessor: 'is_valid', Header: 'Is Valid',
        Cell: data => <div className="pl-15">{data.value === true ? 'Valid' : 'Invalid'}
          {this.getDiscrepancyIcons(data)}
        </div>,
      },
    ];
    const columnDeviceFailure: any = [
      {
        accessor: 'row', Header: 'Row #',
        Cell: data => <div className="pl-td">{data.value}</div>,
      },
      { accessor: 'field', Header: 'Field' },
      { accessor: 'error_messages', Header: 'Error Messages' },
      { accessor: 'warnings', Header: 'Warnings' },
    ];
    const columnMissingData: any = [
      {
        accessor: 'product', Header: 'Product #', width: 250,
        Cell: data => <div className="">{data.value}</div>,
      },
      { accessor: 'vendorOrderNumber', width: 150, Header: 'Vendor Order #' },
      { accessor: 'quantity', width: 100, Header: 'Quantity' },
      { accessor: 'receivedStatus', width: 100, Header: 'Status' },
      {
        accessor: 'description', Header: 'Description',
        Cell: data => <div title={data.value} className="">{data.value}</div>,
      },
    ];
    const rowSelectionPOProps = {
      showCheckbox: false,
      selectIndex: 'name',
      onRowsToggle: this.onRowsTogglePO,
    };
    return (
      <div className="field-mapping correction-response">
        <h3 className="">Data Validation:</h3>
        <div>
          <Table
            columns={columnDeviceFailure}
            rows={this.state.deviceErrorRows}
            customTopBar={this.customTopBarDevice()}
            className={`device-error`}
            loading={this.props.isPostingBatch}
          />
          <Table
            columns={columnMissingData}
            rows={this.state.missingDataRows}
            className={`device-error`}
            customTopBar={<div className="heading-po">
              <span>Missing Data From File</span>
            </div>}
            loading={this.props.isPostingBatch}
          />
          <Table
            columns={columnPO}
            rows={this.state.poErrorRows}
            rowSelection={rowSelectionPOProps}
            className={`purchase-order`}
            customTopBar={this.customTopBar()}
            loading={this.props.isPostingBatch}
            pageSize={this.state.poErrorRows.length}
          />
          <div className="field-mapping__footer">
            <SquareButton
              onClick={e => this.clearPOSelection(e, 'purchaseOrder')}
              content="Back"
              bsStyle={ButtonStyle.DEFAULT}
              className="import-button-1"
            />
            <SquareButton
              onClick={e => this.toggleOpenConfirmDialog()}
              content={
                this.state.deviceErrorRows.filter(data => data.error_messages).length === 0 &&
                  this.state.poErrorRows.filter(row => !row.ignored).length === 0
                  ? 'Process' : 'Cancel Import'}
              bsStyle={ButtonStyle.PRIMARY}
              className="import-button-1"
              disabled={this.props.isPostingBatch}
            />
          </div>
        </div>
      </div>
    )
  }


  renderResult =  () => {
    return (
      <div className="smart-net-final-output">
        <h3>Import Status</h3>
        <div className="result-section">
          {
            _.get(this.state.validationData, 'event_logs', []).length !== 0 &&
            <Collapsible label={'Events Log'} isOpen={true}
              parentClass={'col-md-5 remove-border'}
              statusClass={'label-padding'} >
              <ul className="namespace-list">
                {
                  this.state.validationData.event_logs
                    .map((data, index) => {
                      return (<div key={index}>
                        {data}
                      </div>)
                    })
                }
              </ul>
              <div className="row error warning-list">
                Download :
                <SquareButton
                  onClick={e =>
                    this.downloadReport(
                      this.state.validationData.log_file
                    )
                  }
                  content="Report"
                  bsStyle={ButtonStyle.PRIMARY}
                  title={'Download in PDF Format'}
                  disabled={this.props.isPostingBatch}
                  className="import-button-1"
                />

                <SquareButton
                  onClick={e =>
                    this.downloadJSONFile(
                      this.state.validationData
                    )
                  }
                  content={'JSON'}
                  title={'Download in JSON Format'}
                  bsStyle={ButtonStyle.DEFAULT}
                  className="import-button-1"
                  disabled={this.props.isPostingBatch}
                />

              </div>


            </Collapsible>
          }
          <Collapsible label={'Receiving Products'} isOpen={true}
            parentClass={'col-md-5 remove-border'}
            statusClass={_.get(this.state.validationData, 'purchase_order_receiving_result.result') ? 'success' : 'error'} >
            <ul className="namespace-list">
              {
                Object.keys(_.get(this.state.validationData, 'purchase_order_receiving_result.errors'))
                  .map((data, index) => {
                    return (<div key={index}>
                      {data}: {_.get(this.state.validationData, 'purchase_order_receiving_result.errors')[data].map((x,ind)=>{
                       return  <div key={ind} className="rcv">{Object.values(x)}</div>
                      })}
                    </div>)
                  })
              }
              {
                Object.keys(_.get(this.state.validationData, 'purchase_order_receiving_result.errors')).length === 0 && (_.get(this.state.validationData, 'purchase_order_receiving_result.result') ? 'Success' : 'Error')
              }
              {
                !_.get(this.state.validationData, 'purchase_order_receiving_result.result') && Object.keys(_.get(this.state.validationData, 'purchase_order_receiving_result.errors')).length === 0 &&
                <div className="nodata">Success</div>
              }
            </ul>
          </Collapsible>
          <Collapsible label={'Create Configurations in Connectwise'} isOpen={true}
            parentClass={'col-md-5 remove-border'}
            statusClass={_.get(this.state.validationData, 'device_import_result.result') ? 'success' : 'error'} >

            <ul className="namespace-list">
              {
                Object.keys(_.get(this.state.validationData, 'device_import_result.meta_data'))
                  .map((data, index) => {
                    const result = this.state.validationData.device_import_result.meta_data[data];

                    return (<div className="capitalize" key={index}>
                      <div className="left"> {data.split('_') && data.split('_').join(' ')} </div>:
                      <div className={`${isArray(result) ? 'right-serial-numbers' : 'right'}`}>
                        {result && isArray(result) ? result.join(', ') || 0 : result || 0}</div>
                    </div>)
                  })
              }
              {
                Object.keys(_.get(this.state.validationData, 'device_import_result.meta_data')).length === 0 &&
                <div className="nodata">No data</div>
              }
            </ul>
          </Collapsible>

          <Collapsible label={'Send Email Notification'} isOpen={true}
            parentClass={'col-md-5 remove-border'}
            statusClass={_.get(this.state.validationData, 'email_notification_result.result') ? 'success' : 'error'} >
            <ul className="namespace-list">
              {
                _.get(this.state.validationData, 'email_notification_result.recipient_list')
                  .map((data, index) => {
                    return (<div key={index}>
                      {data}
                    </div>)
                  })
              }
              {
                Object.keys(_.get(this.state.validationData, 'email_notification_result.recipient_list')).length === 0 &&
                <div className="nodata">No data</div>
              }
            </ul>
          </Collapsible>
          <Collapsible label={'Update Service Ticket'} isOpen={true}
            parentClass={'col-md-5 remove-border'}
            statusClass={_.get(this.state.validationData, 'ticket_update_result.result') ? 'success' : 'error'} >
            <ul className="namespace-list">

              {
                _.get(this.state.validationData, 'ticket_update_result.result') ? 'Service Ticket Updated.' : 'Service Ticket Update Failed.'
              }
            </ul>
          </Collapsible>
          <Collapsible label={'TAC Email'} isOpen={true}
            parentClass={'col-md-5 remove-border'}
            statusClass={_.get(this.state.validationData, 'tac_email_result.result') ? 'success' : 'error'} >
            <ul className="namespace-list">

              {_.get(this.state.validationData, 'tac_email_result.to_emails') &&
                <div className="tac-email-data">
                  <div className="heading"> To Email's</div>
                  <div className="email">
                    {
                      _.get(this.state.validationData, 'tac_email_result.to_emails', [])
                        .join(',\n')
                    }
                  </div>
                </div>
              }
              {_.get(this.state.validationData, 'tac_email_result.cc_emails') &&
                <div className="tac-email-data">
                  <div className="heading"> CC Email's</div>
                  <div className="email">
                    {
                      _.get(this.state.validationData, 'tac_email_result.cc_emails', [])
                        .join(',\n')
                    }
                  </div>
                </div>
              }
              {_.get(this.state.validationData, 'tac_email_result.contract_numbers') &&
                <div className="tac-email-data">
                  <div className="heading">Contract Number's</div>
                  <div className="email">
                    {
                      _.get(this.state.validationData, 'tac_email_result.contract_numbers', [])
                        .join(',\n')
                    }
                  </div>
                </div>
              }
              {_.get(this.state.validationData, 'tac_email_result.cco_ids') &&
                <div className="tac-email-data">
                  <div className="heading"> CCO ID's</div>
                  <div className="email">
                    {
                      _.get(this.state.validationData, 'tac_email_result.cco_ids', [])
                        .join(',\n')
                    }
                  </div>
                </div>
              }
              {
                _.get(this.state.validationData, 'tac_email_result.result') ? '' : 'TAC Email failed... please check events logs for more details.'
              }


            </ul>
          </Collapsible>

        </div>
        {
          _.get(this.state.validationData, 'device_import_result.result') &&
          <div className="create-opportunity">
            <span> Would you like to create a future renewal opportunity?</span>
            <SquareButton
              content="Create Opportunity"
              onClick={e => this.toggleCreateOpportunityModal()}
              className="add-new-option-sow"
              bsStyle={ButtonStyle.PRIMARY}
              title="Add New Opportunity"
            />
          </div>
        }
      </div >
    )
  }

  validateEmailPopup = () => {
    const error = this.getEmptyState().error;

    let isValid = true;

    Object.keys(this.state.contactEmails).map(key => {
      this.state.contactEmails.forEach(val => {
        if (!AppValidators.isValidEmail(val)) {
          error.contactEmails.errorMessage = 'Please enter valid email';
          error.contactEmails.errorState = IValidationState.ERROR;
          isValid = false;
        }
      });
    });

    if (!this.state.removeCustomer && this.state.emailNotification && this.state.contactEmails.length === 0 && this.state.customerUsers.filter(user => user.checked).length === 0) {
      error.contactEmailsError.errorMessage = 'No user selected and no email added';
      error.contactEmailsError.errorState = IValidationState.ERROR;
      isValid = false;
    }
    this.setState(prevState => ({
      error,
    }));

    return isValid;
  };

  handleChangeEmailNotify = (e: any) => {
    const targetName = e.target.name;
    const targetValue = e.target.value === 'true' ? true : false;
    this.setState({ [targetName]: targetValue });
  };


  setUsers = (nextProps: ISmartnetMappingProps) => {
    const users: any[] = nextProps.customerUsers;
    const rows: any[] = users.map((user, index) => ({
      name: `${user.first_name} ${user.last_name}`,
      email: user.email,
      phone_number: user.phone_number,
      role: user.role_display_name,
      checked: false,
      index,
    }));

    this.setState(prevState => ({
      customerUsers: rows,
    }));
  };

  renderEmailUsersSection = () => {

    return (
      <div className="field-mapping correction-response">
        <h3 className="">Email Settings:</h3>
        <div>
          {this.emailUsersSection()}
          <div className="field-mapping__footer">
            <SquareButton
              onClick={e => this.clearPOSelection(e, 'correction')}
              content="Back"
              bsStyle={ButtonStyle.DEFAULT}
              disabled={this.props.isPostingBatch}
              className="import-button-1"
            />

            <SquareButton
              onClick={e => this.finalSubmit()}
              content={this.state.poErrorRows.filter(row => !row.ignored).length === 0 ? 'Process' : 'Cancel Import'}
              bsStyle={ButtonStyle.PRIMARY}
              className="import-button-1"
              disabled={this.props.isPostingBatch}
            />
          </div>
        </div>
      </div>
    )
  }
  getUserDetailsByTicketID = (userId, name) => {
    const user = this.state.poTicketsRaw.filter(ticket => ticket.id === userId);
    return user && user[0] && user[0][name] || '';
  }

  getUniqueUsers = () => {

    const uniqueRows = this.state.rows;
    let dict = {};

    uniqueRows.map((row, index) => {
      if (this.state.rows.filter(x=>x.selected)) {
        dict[this.getUserDetailsByTicketID(row.poTicket, 'contact_email')] = this.getUserDetailsByTicketID(row.poTicket, 'resource');
      }
    });
    return (
      Object.keys(dict).map((row, index) => {
        return (
          <div className="user-box" key={index}>
            <div className="name">Name : {dict[row]}</div>
            <div className="email">{row}</div>
          </div>
        )
      })
    )
  }

  emailUsersSection = () => {
    return (
      <div className="confirm-with-users-email row">
        <div className="remove-customer col-md-4">
          <div className="heading">Notification deliver to customer</div>
          <Checkbox
            isChecked={this.state.removeCustomer}
            name="option"
            onChange={e => this.setState({ removeCustomer: e.target.checked, emailNotification: false, includeMSAgent: false })}
          >
            {`Remove Customer from Notification process`}
          </Checkbox>
        </div>
        <div className="email-sections col-md-8">
          {
            !this.state.removeCustomer && this.state.rows.length > 0 && (
              <div className="default-customer-users">
                <div className="heading">Default Customer Users : </div>
                <div className="user-list">
                  {this.getUniqueUsers()}
                </div>
              </div>
            )
          }
          {
            this.state.emailRecipientList && (
              <div className="recipient-list-box">
                <div className="heading">Default Email Recipients : </div>
                <div className="recipient-list">
                  {this.state.emailRecipientList && this.state.emailRecipientList.join(',\n') || 'N.A.'}
                </div>
              </div>
            )
          }
        </div>

        {
          !this.state.removeCustomer && (


            <div className="select-user-email">
              <Input
                field={{
                  value: this.state.emailNotification,
                  label: 'Would you like to send the notification additional contacts?',
                  type: InputFieldType.RADIO,
                  isRequired: false,
                  options: [
                    { value: true, label: 'Yes' },
                    { value: false, label: 'No' },
                  ],
                }}
                width={12}
                name="emailNotification"
                onChange={this.handleChangeEmailNotify}
                className="yes-no-radio"
              />
              {
                this.state.emailNotification && (
                  <>
                    <div className="heading">Customer Contacts</div>
                    <div className="contact-list">
                      {this.state.customerUsers.map((field, index) => (
                        <Checkbox
                          isChecked={this.state.customerUsers[index].checked}
                          name="customerUsers"
                          onChange={e => this.onChecboxChangedEmail(e, index)}
                          key={index}
                        >
                          {<div className="name"> {field.name} <span className="email"> {field.first_name}{field.last_name} ({field.email}) </span> </div>}
                        </Checkbox>
                      ))}
                    </div>
                    <div
                      className="email-tag field-section
             col-md-12 col-xs-12"
                    >
                      <div className="field__label row">
                        <label className="field__label-label" title="">
                          Enter Emails (Max. 4)
                        </label>
                        <span className="field__label-required" />
                      </div>
                      <div
                        className={`${this.state.error.contactEmails
                          .errorMessage
                          ? `error-input`
                          : ''
                          }`}
                      >
                        <TagsInput
                          value={this.state.contactEmails}
                          onChange={e =>
                            this.handleChangeContactEmail(
                              e,
                              'contactEmails'
                            )
                          }
                          inputProps={{
                            className: 'react-tagsinput-input',
                            placeholder: 'Enter Email',
                          }}
                          addOnBlur={true}
                        />
                      </div>
                      {this.state.emailNotification && this.state.error.contactEmails
                        .errorMessage && (
                          <div className="multiple-email-eror">
                            {
                              this.state.error.contactEmails
                                .errorMessage
                            }
                          </div>
                        )}
                    </div>

                  </>
                )
              }
              {this.state.error.contactEmailsError
                .errorMessage && (
                  <div className="multiple-email-eror">
                    {
                      this.state.error.contactEmailsError
                        .errorMessage
                    }
                  </div>
                )}
            </div>
          )}

        {
          <div className="select-user-email">
            <Input
              field={{
                value: this.state.includeMSAgent,
                label: 'Would you like to include MS Agents?',
                type: InputFieldType.RADIO,
                isRequired: false,
                options: [
                  { value: true, label: 'Yes' },
                  { value: false, label: 'No' },
                ],
              }}
              width={12}
              name="includeMSAgent"
              onChange={this.handleChangeEmailNotify}
              className="yes-no-radio"
            />
            {
              this.state.includeMSAgent && (
                <>
                  <div className="heading">MS Agent Team</div>
                  <div className="contact-list">
                    {this.state.providerUsers.map((field, index) => (
                      <Checkbox
                        isChecked={this.state.providerUsers[index].checked}
                        name="providerUsers"
                        onChange={e => this.onChecboxChangedMSAgent(e, index)}
                        key={index}
                      >
                          {<div className="name"> {field.name} <span className="email"> {field.first_name}{field.last_name} ({field.email}) </span> </div>}
                      </Checkbox>
                    ))}
                  </div>
                </>
              )
            }
            {this.state.error.contactEmailsError
              .errorMessage && (
                <div className="multiple-email-eror">
                  {
                    this.state.error.contactEmailsError
                      .errorMessage
                  }
                </div>
              )}
          </div>
        }
      </div>
    )
  }
  handleChangeContactEmail = (event: any, name: string) => {
    const error = this.getEmptyState().error;
    let list = event;

    list.forEach(val => {
      if (!AppValidators.isValidEmail(val)) {
        error[name].errorMessage = 'Please enter valid email';
        error[name].errorState = IValidationState.ERROR;
      }
    });
    list = list.filter(
      (value, index, self) => self.indexOf(value) === index
    );
    if (list.length < 5) {
      this.setState(prevState => ({
        contactEmails: list,
        error
      }));
    }

  };
  onChecboxChangedEmail = (e: any, index?: any) => {
    const targetValue = e.target.checked;
    const newState = cloneDeep(this.state);
    newState.customerUsers[index].checked = targetValue;
    this.setState({ customerUsers: newState.customerUsers });
  };
  onChecboxChangedMSAgent = (e: any, index?: any) => {
    const targetValue = e.target.checked;
    const newState = cloneDeep(this.state);
    newState.providerUsers[index].checked = targetValue;
    this.setState({ providerUsers: newState.providerUsers });
  };

  confirmBoxBody = () => {
    return (
      <div>
        {
          this.state.deviceErrorRows.filter(data => data.error_messages).length === 0
            && this.state.poErrorRows.filter(row => !row.ignored).length === 0 ?
            <div className="import-error-msg">You are importing non-matching Product Quantities.
              <p> Click OK to import, Click cancel to return to the import screen.</p>
            </div> :
            <div className="import-error-msg">You are canceling this import.
              <p> Click OK to cancel the import, Click cancel to return to the import screen. </p>
            </div>
        }
      </div>
    )
  }

  download = () => {
    let headers = {
      name: 'Product Number',
      input_quantity: 'Input Quanity',
      cw_quantity: 'CW Quantity',
      is_valid: 'Is Valid',
    };
    let itemsFormatted = [];
    let itemsNotFormatted = this.state.poErrorRows;
    itemsNotFormatted.forEach((item) => {
      itemsFormatted.push({
        name: item.name || 'N.A.',
        input_quantity: item.input_quantity || 'N.A.',
        cw_quantity: item.cw_quantity || 'N.A.',
        is_valid: item.is_valid ? 'Yes' : 'No',
      });
    });

    var fileTitle = 'purchase-order-validation-data'; // or 'my-unique-title'
    exportCSVFile(headers, itemsFormatted, fileTitle);
  }
  toggleSiteMapping = () => {
    this.setState(prevState => ({
      isopenSiteMapping: !prevState.isopenSiteMapping,
    }));
  };
  addressValidSelection =()=>{
    return this.state.searchQueryAddress.filter(x =>x).length !== this.state.addressList.length
  }
  render() {
    return (
      <div className="mapping">
        {this.state.loading || this.props.isPostingBatch ?
          <div className="loader"><Spinner show={true} /></div> : null}
        {this.state.openSections['fieldMapping'] === true && this.getSmartnetMapping()}
        {this.state.openSections['addressMapping'] === true &&
          this.getAddressMapping()}
        {this.state.openSections['purchaseOrder'] === true &&
          this.getPurchaseOrder()}
        {
          this.state.openSections['correction'] && this.renderCorrection()
        }
        {
          this.state.openSections['emailSetting'] && this.renderEmailUsersSection()
        }
        {
          this.state.openSections['result'] && <BackButton
            path="/inventory-management"
            name="Back to Device List"
          />
        }
        {
          this.state.openSections['result'] && this.renderResult()
        }
        <CreateSite
          show={this.state.isCreateSiteModal}
          onClose={this.toggleCreateSiteModal}
          onSubmit={this.onSiteAdd}
          countries={this.props.countries}
          isPosting={this.state.isPostingSite}
          clear={this.state.clearSite}
        />
        <ConfirmDevices
          deviceList={this.state.deviceList ? this.state.deviceList.length : 0}
          xlsxData={this.state.xlsxData ? this.state.xlsxData.length : 0}
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
        />
        {
          this.state.isopenConfirmDialog &&
          <ConfirmBox
            show={this.state.isopenConfirmDialog}
            onClose={this.toggleOpenConfirmDialog}
            onSubmit={this.onClickConfirmDialog}
            isLoading={this.props.isFetchingUsers}
            title={'Confirm'}
            okText={'OK'}
            cancelText={'Cancel'}
            message={this.confirmBoxBody()}
            className="confirm-user-email"
          />
        }
        {
          this.state.isCreateOpportunityModal &&
          <AddOpportunity
            show={this.state.isCreateOpportunityModal}
            onClose={this.toggleCreateOpportunityModal}
            onSubmit={this.createQuote}
            types={this.props.qTypeList}
            stages={this.props.qStageList}
            isLoading={this.state.isPosting}
            errorList={this.state.errorList}
            opportunity={this.state.opportunity}
            statusesList={this.state.statusesList}
            businessList={this.state.businessList}
            terretoryMembers={this.props.terretoryMembers}
            expirationYear={_.get(this.state.validationData, 'expiration_date', '').substring(0, 4)}
            currentUserEmail={this.props.loggenInUser.email}
            smartNetSetting={this.props.smartNetSetting}
          />
        }

{
          this.state.isopenSiteMapping &&
          <SiteMapping
          xlsxData={this.state.xlsxData}
          searchQueryAddress={this.state.searchQueryAddress}
          addressFieldSelected={this.state.searchQueryField[7]}
          addressList={this.state.addressList}
          show={this.state.isopenSiteMapping}
          onClose={this.toggleSiteMapping}
          onSubmit={this.onClickConfirm}
          directImport={false}
        />
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  xlsxData: state.inventory.xlsxData,
  sites: state.inventory.sites,
  customerId: state.customer.customerId,
  deviceSuccess: state.inventory.deviceSuccess,
  customersShort: state.customer.customersShort,
  countries: state.inventory.countries,
  isPostingBatch: state.inventory.isPostingBatch,
  manufacturers: state.inventory.manufacturers,
  dateFormats: state.inventory.dateFormats,
  xlsxFile: state.inventory.xlsxFile,
  purchaseOrders: state.inventory.purchaseOrders,
  lastVisitedSite: state.inventory.lastVisitedSite,
  purchaseOrderCorrection: state.inventory.purchaseOrderCorrection,
  customerUsers: state.customer.users,
  isFetchingUsers: state.customer.isFetchingUsers,
  qStageList: state.sow.qStageList,
  qTypeList: state.sow.qTypeList,
  terretoryMembers: state.integration.terretoryMembers,
  smartNetSetting: state.setting.smartNetSetting,
  loggenInUser: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStatusesList: () => dispatch(getQuoteStatusesList()),
  getQuoteBusinessList: () => dispatch(getQuoteBusinessList()),
  getQuoteStageList: () => dispatch(getQuoteStageList()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  addSite: (customerId: number, newSite: any) =>
    dispatch(addSite(customerId, newSite)),
  batchCreateImportData: (customerId: number, devices: IDevice[]) =>
    dispatch(batchCreateImportData(customerId, devices)),
  batchCreateImportDataSmartNet: (customerId: number, devices: IDevice[]) =>
    dispatch(batchCreateImportDataSmartNet(customerId, devices)),
  smartnetImportFileValidation: (customerId: number, devices: IDevice[]) =>
    dispatch(smartnetImportFileValidation(customerId, devices)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
  fetchDevices: (id: number, show: boolean) => dispatch(fetchDevices(id)),
  fetchDateFormat: () => dispatch(fetchDateFormat()),
  createQuoteSmartnet: (id: number, q: any) => dispatch(createQuoteSmartnet(id, q)),
  uploadDeviceFile: (fileReq: any, name: string) =>
    dispatch(uploadDeviceFile(fileReq, name)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  getListPurchaseOrders: (customerId: number) => dispatch(getListPurchaseOrders(customerId)),
  getRecipientLists: (customerId: number) => dispatch(getRecipientLists(customerId)),
  getListPOTickts: (customerId: number) => dispatch(getListPOTickts(customerId)),
  fetchCustomerUsers: (id: number, params?: IServerPaginationParams) =>
    dispatch(fetchCustomerUsers(id, params)),
  fetchTerritoryMembers: () => dispatch(fetchTerritoryMembers()),
  getSmartNetSettings: () => dispatch(getSmartNetSettings()),
  getMSAgentAssociation: () => dispatch(getMSAgentAssociation()),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SmartnetMapping);
