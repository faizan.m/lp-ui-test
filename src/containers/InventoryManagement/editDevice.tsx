import _, { cloneDeep } from 'lodash';
import React from 'react';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import SelectInput from '../../components/Input/Select/select';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { toFormattedDate } from '../../utils/CalendarUtil';

interface IEditDeviceFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newDevice: IDevice) => void;
  manufacturers: any[];
  sites: any[];
  fetchSites: (customerId: number) => any;
  deviceDetails: any;
  openURL: (e: any, url: string) => any;
  isFetching: boolean;
  availableAssociationList: any;
  existingAssociationList: any;
  userType?: string;
}

interface IEditDeviceFormState {
  sites: any[];
  device: IDevice;
  error: {
    device_name: IFieldValidation;
    serial_number: IFieldValidation;
    site_id: IFieldValidation;
    instance_id: IFieldValidation;
    manufacturer_id: IFieldValidation;
    notes: IFieldValidation;
    asset_tag: IFieldValidation;
    service_contract_number: IFieldValidation;
    customer_notes: IFieldValidation;
  };
}

export default class EditDeviceForm extends React.Component<
  IEditDeviceFormProps,
  IEditDeviceFormState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: '',
  };

  constructor(props: IEditDeviceFormProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    device: {
      device_name: '',
      category_id: null,
      serial_number: null,
      site_id: null,
      manufacturer_id: null,
      notes: '',
      instance_id: null,
      circuit_info_ids: null,
      asset_tag: '',
      customer_notes: '',
    },
    error: {
      device_name: { ...EditDeviceForm.emptyErrorState },
      serial_number: { ...EditDeviceForm.emptyErrorState },
      site_id: { ...EditDeviceForm.emptyErrorState },
      instance_id: { ...EditDeviceForm.emptyErrorState },
      manufacturer_id: { ...EditDeviceForm.emptyErrorState },
      notes: { ...EditDeviceForm.emptyErrorState },
      asset_tag: { ...EditDeviceForm.emptyErrorState },
      service_contract_number: { ...EditDeviceForm.emptyErrorState },
      customer_notes: { ...EditDeviceForm.emptyErrorState },
    },
    sites: [],
  });

  componentDidUpdate(prevProps: IEditDeviceFormProps) {
    const { deviceDetails, existingAssociationList } = this.props;
    if (deviceDetails !== prevProps.deviceDetails && deviceDetails) {
      this.setState({
        ...cloneDeep(this.getEmptyState()),
        device: deviceDetails,
      });
    }
    if (existingAssociationList !== prevProps.existingAssociationList) {
      const ids = existingAssociationList.map((a) => a.id);
      this.setState((prevState) => ({
        device: {
          ...prevState.device,
          ["circuit_info_ids"]: ids,
        },
      }));
    }
  }

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState(prevState => ({
      device: {
        ...prevState.device,
        [targetName]: targetValue,
      },
    }));
  };

  handleIsActiveChange = (event: any) => {
    const targetValue = event.target.value;

    const newState = cloneDeep(this.state);
    newState.device.status_id = parseInt(targetValue, 10);

    this.setState(newState);
  };

  onClose = e => {
    this.props.onClose(e);
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.device.device_name ||
      this.state.device.device_name.trim().length === 0
    ) {
      error.device_name.errorState = IValidationState.ERROR;
      error.device_name.errorMessage = 'Enter a valid device name';

      isValid = false;
    } else if (this.state.device.device_name.length > 300) {
      error.device_name.errorState = IValidationState.ERROR;
      error.device_name.errorMessage =
        'Device name should be greater than 300 chars.';

      isValid = false;
    }
    if (
      this.state.device.serial_number &&
      this.state.device.serial_number.length > 300
    ) {
      error.serial_number.errorState = IValidationState.ERROR;
      error.serial_number.errorMessage =
        'Serial No. should be greater than 300 chars.';

      isValid = false;
    }
    if (this.state.device.site_id && this.state.device.site_id < 0) {
      error.site_id.errorState = IValidationState.ERROR;
      error.site_id.errorMessage = 'Enter a valid Site';

      isValid = false;
    }
    if (!this.state.device.manufacturer_id) {
      error.manufacturer_id.errorState = IValidationState.ERROR;
      error.manufacturer_id.errorMessage = 'Please select Manufacturer';

      isValid = false;
    }
    if (
      this.state.device.manufacturer_id &&
      this.state.device.manufacturer_id < 0
    ) {
      error.manufacturer_id.errorState = IValidationState.ERROR;
      error.manufacturer_id.errorMessage = 'Enter a valid Manufacturer';

      isValid = false;
    }
    if (this.state.device.notes && this.state.device.notes.length > 1000) {
      error.notes.errorState = IValidationState.ERROR;
      error.notes.errorMessage = 'Notes should not be greater than 1000 chars';

      isValid = false;
    }
    if (
      this.state.device.instance_id &&
      this.state.device.instance_id.length > 300
    ) {
      error.instance_id.errorState = IValidationState.ERROR;
      error.instance_id.errorMessage =
        'Instance Id should be less than 300 chars.';

      isValid = false;
    }
    if (
      this.state.device.asset_tag &&
      this.state.device.asset_tag.length > 25
    ) {
      error.asset_tag.errorState = IValidationState.ERROR;
      error.asset_tag.errorMessage = 'Asset Tag should be max. 25 chars.';

      isValid = false;
    }
    if (
      this.state.device.customer_notes &&
      this.state.device.customer_notes.length > 1000
    ) {
      error.customer_notes.errorState = IValidationState.ERROR;
      error.customer_notes.errorMessage =
        'Customer_Notes should not be greater than 1000 chars';

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = e => {
    if (this.isValid()) {
      this.props.onSubmit(this.state.device);
    }
  };
  renderDeviceDetails = () => {
    const deviceDetails = this.state.device;

    return (
      <div className="device-details-modal__body">
        <div className="device-details-modal__body-field">
          <label>Serial #</label>
          <label>
            {deviceDetails.serial_number
              ? deviceDetails.serial_number
              : 'unknown'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Model</label>
          <label>{deviceDetails.model_number}</label>
        </div>
        <div className="device-details-modal__body-field ">
          <label>Contract Status</label>
          <label>
            {deviceDetails.contract_status !== 'N.A.' ? (
              deviceDetails.contract_status
            ) : deviceDetails.expiration_date ? (
              deviceDetails.expiration_date
            ) : (
                  <span className="contract-status-no-covered">Not Covered</span>
                )}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <Input
            field={{
              value: this.state.device.status_id,
              label: 'Production Status',
              type: InputFieldType.RADIO,
              isRequired: false,
              options: [
                { value: 1, label: 'In Service' },
                { value: 2, label: 'Decommissioned' },
              ],
            }}
            width={12}
            name="status_id"
            className="support-status"
            onChange={this.handleIsActiveChange}
          />
        </div>
        <div
          className="device-details-modal__body-field
        device-details-modal__body-field details-notes"
        >
          <label>Description</label>
          <label className="note-description">
            {this.state.device.item_description
              ? this.state.device.item_description
              : 'unknown'}
          </label>
        </div>
      </div>
    );
  };

  renderDeviceDateDetails = () => {
    const deviceDetails = this.state.device;

    return (
      <div className="device-details-modal__body">
        <div className="device-details-modal__body-field">
          <label>Is Managed</label>
          <label>
            {deviceDetails &&
              deviceDetails.monitoring_data &&
              deviceDetails.monitoring_data.is_managed
              ? 'Yes'
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>OS version</label>
          <label className="note-description">
            {_.get(deviceDetails, 'monitoring_data.is_managed')
              ? _.get(deviceDetails, `monitoring_data.${_.get(deviceDetails, `monitoring_data.last_updated_by`)}.os_version`)
              : this.state.device.updated_on ? 'Not Announced' : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Purchased</label>
          <label>
            {deviceDetails.purchase_date
              ? toFormattedDate(deviceDetails.purchase_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>End of Service attach</label>
          <label>
            {deviceDetails.EOS_date
              ? toFormattedDate(deviceDetails.EOSA_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>last day of support</label>
          <label>
            {deviceDetails.LDOS_date
              ? toFormattedDate(deviceDetails.LDOS_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>End Of Life</label>
          <label>
            {deviceDetails.EOL_date
              ? toFormattedDate(deviceDetails.EOL_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>end of sale </label>
          <label>
            {deviceDetails.EOS_date
              ? toFormattedDate(deviceDetails.EOS_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>last day of sw support</label>
          <label>
            {deviceDetails.EOSWS_date
              ? toFormattedDate(deviceDetails.EOSWS_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>end of security update</label>
          <label>
            {deviceDetails.EOSU_date
              ? toFormattedDate(deviceDetails.EOSU_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Expiration</label>
          <label className="label-expiration-date">
            {deviceDetails.expiration_date
              ? toFormattedDate(deviceDetails.expiration_date)
              : this.state.device.updated_on
                ? 'Not Announced'
                : 'Not Available'}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Contract Start Date</label>
          <label>
            {deviceDetails.installation_date
              ? toFormattedDate(deviceDetails.installation_date)
              : 'Not Available'}
          </label>
        </div>
      </div>
    );
  };

  getTitle = () => {
    return 'Edit Device Details';
  };

  getBody = () => {
    const sites = this.props.sites
      ? this.props.sites.map(site => ({
        value: site.site_id,
        label: site.name,
      }))
      : [];
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(manufacturer => ({
        value: manufacturer.id,
        label: manufacturer.label,
      }))
      : [];
    const availableAssociationList = this.props.availableAssociationList
      ? this.props.availableAssociationList.map(data => ({
        value: data.id,
        label: `${data.circuit_type} (${data.circuit_id})`,
      }))
      : [];

    const existingAssociationList = this.props.existingAssociationList
      ? this.props.existingAssociationList.map(data => ({
        value: data.id,
        label: `${data.circuit_type} (${data.circuit_id})`,
      }))
      : [];

    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div
          className={`edit-device ${this.props.isFetching ? `loading` : ''}`}
        >
          {this.renderDeviceDetails()}
          <div className="editable-fields">
            <Input
              field={{
                label: 'Device Name',
                type: InputFieldType.TEXT,
                value: this.state.device.device_name,
                isRequired: true,
              }}
              width={6}
              placeholder="Enter Device Name"
              name="device_name"
              error={this.state.error.device_name}
              onChange={this.handleChange}
            />
            {this.props.userType === 'provider' && (
              <Input
                field={{
                  label: 'Instance ID',
                  type: InputFieldType.TEXT,
                  value: this.state.device.instance_id,
                  isRequired: false,
                }}
                width={6}
                placeholder="Enter Instance ID"
                name="instance_id"
                error={this.state.error.instance_id}
                onChange={this.handleChange}
              />
            )}
            <Input
              field={{
                label: 'Contract Number',
                type: InputFieldType.TEXT,
                value: this.state.device.service_contract_number,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter Contract Number"
              name="service_contract_number"
              error={this.state.error.service_contract_number}
              onChange={this.handleChange}
            />
            <div
              className="select-manufacturer field-section
             col-md-6 col-xs-6"
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Select Manufacturer
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.manufacturer_id.errorMessage
                    ? `error-input`
                    : ''
                  }`}
              >
                <SelectInput
                  name="manufacturer_id"
                  value={this.state.device.manufacturer_id}
                  onChange={this.handleChange}
                  options={manufacturers}
                  searchable={true}
                  placeholder="Select Manufacturer"
                  clearable={false}
                />
              </div>
              {this.state.error.manufacturer_id.errorMessage && (
                <div className="select-manufacturer-error">
                  {this.state.error.manufacturer_id.errorMessage}
                </div>
              )}
            </div>
            <div className="field-section  col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="Serial No">
                  Location / Site
                </label>
              </div>
              <div>
                <SelectInput
                  name="site_id"
                  value={this.state.device.site_id}
                  onChange={this.handleChange}
                  options={sites}
                  searchable={true}
                  placeholder="Select Location / Site"
                  clearable={false}
                />
              </div>
            </div>

            <div className="ckt-association field-section  col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label">
                  Circuit association
                </label>
              </div>

              <div className="field__input row">
                <SelectInput
                  name="circuit_info_ids"
                  value={this.state.device.circuit_info_ids}
                  onChange={this.handleChange}
                  options={availableAssociationList.concat(
                    existingAssociationList
                  )}
                  multi={true}
                  placeholder="Select Circuits"
                />
              </div>
            </div>
            <Input
              field={{
                label: 'Asset Tag',
                type: InputFieldType.TEXT,
                value: this.state.device.asset_tag,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter Asset Tag"
              name="asset_tag"
              error={this.state.error.asset_tag}
              onChange={this.handleChange}
            />
            {this.props.userType === 'provider' && (
              <Input
                field={{
                  label: 'Internal Notes',
                  type: InputFieldType.TEXTAREA,
                  value: this.state.device.notes,
                }}
                width={6}
                name="notes"
                onChange={this.handleChange}
                error={this.state.error.notes}
                placeholder="Enter Internal Note"
              />
            )}
            <Input
              field={{
                label: `${
                  this.props.userType === 'provider'
                    ? 'Customer Notes'
                    : 'Notes'
                  }`,
                type: InputFieldType.TEXTAREA,
                value: this.state.device.customer_notes,
              }}
              width={6}
              name="customer_notes"
              onChange={this.handleChange}
              error={this.state.error.customer_notes}
              placeholder={`${
                this.props.userType === 'provider'
                  ? 'Enter Customer Notes'
                  : 'Enter Notes'
                }`}
            />
          </div>
          {
            !this.state.device.non_cisco &&
            <>
              <div className="device-detail__header">
                <h5>Device Details</h5>
              </div>

              {this.renderDeviceDateDetails()}
            </>
          }
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-device__footer
      ${this.props.isFetching ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Update"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-device"
      />
    );
  }
}
