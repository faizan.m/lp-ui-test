import { cloneDeep } from "lodash";
import React from "react";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import SelectInput from "../../components/Input/Select/select";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import { commonFunctions } from "../../utils/commonFunctions";
interface IAddDeviceFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newDevice: IDevice) => void;
  manufacturers: any[];
  sites: any[];
  fetchSites: (customerId: number) => any;
  isLoading: boolean;
  errorList: any;
  userType: string;
  deviceDetails: IDevice;
}

interface IAddDeviceFormState {
  sites: any[];
  device: IDevice;
  error: {
    device_name: IFieldValidation;
    serial_number: IFieldValidation;
    site_id: IFieldValidation;
    instance_id: IFieldValidation;
    manufacturer_id: IFieldValidation;
    notes: IFieldValidation;
    asset_tag: IFieldValidation;
    customer_notes: IFieldValidation;
    installation_date: IFieldValidation;
  };
}

export default class AddDeviceForm extends React.Component<
  IAddDeviceFormProps,
  IAddDeviceFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };

  constructor(props: IAddDeviceFormProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    device: {
      device_name: "",
      category_id: null,
      serial_number: null,
      site_id: null,
      manufacturer_id: null,
      notes: "",
      instance_id: null,
      asset_tag: "",
      customer_notes: "",
    },
    error: {
      device_name: { ...AddDeviceForm.emptyErrorState },
      serial_number: { ...AddDeviceForm.emptyErrorState },
      site_id: { ...AddDeviceForm.emptyErrorState },
      instance_id: { ...AddDeviceForm.emptyErrorState },
      manufacturer_id: { ...AddDeviceForm.emptyErrorState },
      notes: { ...AddDeviceForm.emptyErrorState },
      asset_tag: { ...AddDeviceForm.emptyErrorState },
      customer_notes: { ...AddDeviceForm.emptyErrorState },
      installation_date: { ...AddDeviceForm.emptyErrorState },
    },
    sites: [],
  });

  componentDidUpdate(prevProps: IAddDeviceFormProps) {
    const { errorList, deviceDetails } = this.props;

    if (errorList !== prevProps.errorList) {
      this.setValidationErrors(errorList);
    }

    if (deviceDetails !== prevProps.deviceDetails && deviceDetails) {
      let device = this.getEmptyState().device;
      (device as any) = deviceDetails;
      this.setState({ device });
    }
  }

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      device: {
        ...prevState.device,
        [targetName]: targetValue,
      },
    }));
  };

  onClose = (e) => {
    this.props.onClose(e);
    const newState = this.getEmptyState();
    this.setState(newState);
  };

  setValidationErrors = (errorList) => {
    const newState: IAddDeviceFormState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.device.device_name ||
      this.state.device.device_name.trim().length === 0
    ) {
      error.device_name.errorState = IValidationState.ERROR;
      error.device_name.errorMessage = "Enter a valid device name";

      isValid = false;
    } else if (this.state.device.device_name.length > 300) {
      error.device_name.errorState = IValidationState.ERROR;
      error.device_name.errorMessage =
        "Device name should be less than 300 chars.";

      isValid = false;
    }
    if (
      !this.state.device.serial_number ||
      this.state.device.serial_number.trim().length === 0
    ) {
      error.serial_number.errorState = IValidationState.ERROR;
      error.serial_number.errorMessage = "Enter a valid serial number";

      isValid = false;
    } else if (
      this.state.device.serial_number &&
      this.state.device.serial_number.length > 300
    ) {
      error.serial_number.errorState = IValidationState.ERROR;
      error.serial_number.errorMessage =
        "Serial No. should be less than 300 chars.";

      isValid = false;
    }

    if (
      this.state.device.asset_tag &&
      this.state.device.asset_tag.length > 25
    ) {
      error.asset_tag.errorState = IValidationState.ERROR;
      error.asset_tag.errorMessage = "Asset Tag should be max. 25 chars.";

      isValid = false;
    }

    if (this.state.device.site_id && this.state.device.site_id < 0) {
      error.site_id.errorState = IValidationState.ERROR;
      error.site_id.errorMessage = "Enter a valid Site";

      isValid = false;
    }

    // if (
    //   !this.state.device.instance_id ||
    //   this.state.device.instance_id.trim().length === 0
    // ) {
    //   error.instance_id.errorState = IValidationState.ERROR;
    //   error.instance_id.errorMessage = 'Enter a valid Instance Id';

    //   isValid = false;
    // } else
    if (
      this.state.device.instance_id &&
      this.state.device.instance_id.length > 300
    ) {
      error.instance_id.errorState = IValidationState.ERROR;
      error.instance_id.errorMessage =
        "Instance Id should be less than 300 chars.";

      isValid = false;
    }

    if (!this.state.device.manufacturer_id) {
      error.manufacturer_id.errorState = IValidationState.ERROR;
      error.manufacturer_id.errorMessage = "Please select Manufacturer";

      isValid = false;
    }

    if (
      this.state.device.manufacturer_id &&
      this.state.device.manufacturer_id < 0
    ) {
      error.manufacturer_id.errorState = IValidationState.ERROR;
      error.manufacturer_id.errorMessage = "Enter a valid Manufacturer";

      isValid = false;
    }

    if (this.state.device.notes && this.state.device.notes.length > 1000) {
      error.notes.errorState = IValidationState.ERROR;
      error.notes.errorMessage = "Notes should not be greater than 1000 chars";

      isValid = false;
    }

    if (
      this.state.device.customer_notes &&
      this.state.device.customer_notes.length > 1000
    ) {
      error.customer_notes.errorState = IValidationState.ERROR;
      error.customer_notes.errorMessage =
        "Customer_Notes should not be greater than 1000 chars";

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = (e) => {
    if (this.isValid()) {
      this.props.onSubmit(this.state.device);
    }
  };

  getTitle = () => {
    return "Add New Device";
  };

  getBody = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((manufacturer) => ({
          value: manufacturer.id,
          label: manufacturer.label,
        }))
      : [];
    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];

    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`add-device__body ${
            this.props.isLoading ? `loading` : ""
          }`}
        >
          <Input
            field={{
              label: "Serial No",
              type: InputFieldType.TEXT,
              value: this.state.device.serial_number,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Serial Number"
            error={this.state.error.serial_number}
            name="serial_number"
            onChange={this.handleChange}
            labelIcon="info"
          />
          {this.props.userType === "provider" && (
            <Input
              field={{
                label: "Instance ID",
                type: InputFieldType.TEXT,
                value: this.state.device.instance_id,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter Instance ID"
              error={this.state.error.instance_id}
              name="instance_id"
              onChange={this.handleChange}
              labelIcon="info"
            />
          )}
          <div className="select-manufacturer field-section  col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Manufacturer
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.manufacturer_id.errorMessage
                  ? `error-input`
                  : ""
              }`}
            >
              <SelectInput
                name="manufacturer_id"
                value={this.state.device.manufacturer_id}
                onChange={this.handleChange}
                options={manufacturers}
                searchable={true}
                placeholder="Select Manufacturer"
                clearable={false}
              />
            </div>
            {this.state.error.manufacturer_id.errorMessage && (
              <div className="select-manufacturer-error">
                {this.state.error.manufacturer_id.errorMessage}
              </div>
            )}
          </div>
          <div className="field-section  col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="Serial No">
                Location / Site
              </label>
            </div>
            <div>
              <SelectInput
                name="site_id"
                value={this.state.device.site_id}
                onChange={this.handleChange}
                options={sites}
                searchable={true}
                placeholder="Select Location / Site"
                clearable={false}
              />
            </div>
          </div>
          <Input
            field={{
              label: "Device Name",
              type: InputFieldType.TEXT,
              value: this.state.device.device_name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Device Name"
            name="device_name"
            error={this.state.error.device_name}
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Asset Tag",
              type: InputFieldType.TEXT,
              value: this.state.device.asset_tag,
              isRequired: false,
            }}
            width={6}
            placeholder="Enter Asset Tag"
            name="asset_tag"
            error={this.state.error.asset_tag}
            onChange={this.handleChange}
          />
          <Input
            field={{
              value: this.state.device.installation_date,
              label: "Contract Start Date",
              type: InputFieldType.DATE,
              isRequired: false,
            }}
            width={6}
            name="installation_date"
            onChange={this.handleChange}
            placeholder="Select Date"
            showTime={false}
            showDate={true}
            error={this.state.error.installation_date}
          />
          <Input
            field={{
              label: `${
                this.props.userType === "provider" ? "Customer Notes" : "Notes"
              }`,
              type: InputFieldType.TEXTAREA,
              value: this.state.device.customer_notes,
            }}
            width={6}
            name="customer_notes"
            onChange={this.handleChange}
            error={this.state.error.customer_notes}
            placeholder={`${
              this.props.userType === "provider"
                ? "Enter Customer Notes"
                : "Enter Notes"
            }`}
          />
          {this.props.userType === "provider" && (
            <Input
              field={{
                label: "Internal Notes",
                type: InputFieldType.TEXTAREA,
                value: this.state.device.notes,
              }}
              width={6}
              name="notes"
              onChange={this.handleChange}
              error={this.state.error.notes}
              placeholder="Enter Internal Notes"
            />
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-device__footer
      ${this.props.isLoading ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Add"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-device"
      />
    );
  }
}
