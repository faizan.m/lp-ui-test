import React from "react";

import SquareButton from "../../components/Button/button";
import Select from "../../components/Input/Select/select";
import ModalBase from "../../components/ModalBase/modalBase";

interface IFilterSiteProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: ISiteFilters) => void;
  prevFilters: ISiteFilters;
  sites: any[];
}

interface IFilterSiteState {
  filters: ISiteFilters;
}

export default class FilterSite extends React.Component<
  IFilterSiteProps,
  IFilterSiteState
> {
  constructor(props: IFilterSiteProps) {
    super(props);

    this.state = {
      filters: {
        site: [],
      },
    };
  }

  componentDidUpdate(prevProps: IFilterSiteProps) {
    const { show, prevFilters } = this.props;
    if (show !== prevProps.show && show) {
      this.setState({
        filters: prevFilters ? prevFilters : {},
      });
    }
  }

  onClose = (e) => {
    this.props.onClose(e);
  };

  onSubmit = (e) => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  getBody = () => {
    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];

    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        {" "}
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Site</label>
            <div className="field__input">
              <Select
                name="site"
                value={filters.site}
                onChange={this.onFilterChange}
                options={sites}
                multi={true}
                placeholder="Select Site"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={ButtonStyle.DEFAULT}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={ButtonStyle.PRIMARY}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
