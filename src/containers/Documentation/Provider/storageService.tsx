import React from "react";
import cloneDeep from "lodash/cloneDeep";
import { connect } from "react-redux";

import Spinner from "../../../components/Spinner";
import Validator from "../../../utils/validator";
import {
  DOWNLOAD_FILE_SUCCESS,
  downloadFilefromDropBox,
  FETCH_PATH_LISTING_SUCCESS,
  fetchPathListing,
  uploadFiletoDropBox,
} from "../../../actions/documentation";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  TASK_STATUS_SUCCESS,
  TASK_STATUS_FAILURE,
  fetchTaskStatus,
} from "../../../actions/inventory";
import "./style.scss";

interface IStorageServiceProps {
  isFetching: boolean;
  fetchPathListing: any;
  downloadFilefromDropBox: any;
  documentsList: any[];
  user: ISuperUser;
  customerId: number;
  uploadFiletoDropBox: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  fetchTaskStatus: any;
}

interface IStorageServiceState {
  data: any;
  lastPath: string;
  fullPath: string;
  showBackArrow: boolean;
  currentUploadedDocument: any;
  error: string;
}

//TODO: Remove update state durung render error
class StorageService extends React.Component<
  IStorageServiceProps,
  IStorageServiceState
> {
  static validator = new Validator();
  static emptyErrorState: IFieldValidation = {
    errorState: IValidationState.SUCCESS,
    errorMessage: "",
  };
  uploadInput = React.createRef<HTMLInputElement>();

  static emptyState: IStorageServiceState = {
    lastPath: "",
    data: null,
    showBackArrow: false,
    fullPath: "",
    currentUploadedDocument: undefined,
    error: "",
  };

  constructor(props: IStorageServiceProps) {
    super(props);

    this.state = cloneDeep(StorageService.emptyState);
  }

  componentDidMount() {
    if (this.props.customerId) {
      this.props.fetchPathListing(this.props.customerId);
      this.setState({
        showBackArrow: false,
        fullPath: "",
      });
    }
  }

  componentDidUpdate(prevProps: IStorageServiceProps) {
    if (this.props.customerId !== prevProps.customerId) {
      this.props.fetchPathListing(this.props.customerId);
      this.setState({
        showBackArrow: false,
        fullPath: "",
      });
    }
  }

  getBody = () => {
    const documentsList = Object.keys(this.props.documentsList || {});
    const data =
      documentsList &&
      documentsList.length > 0 &&
      this.props.documentsList[documentsList[0]];

    const currentPath =
      data && data.value.substr(0, data.value.lastIndexOf("/"));

    const backPath =
      currentPath && currentPath.substr(0, currentPath.lastIndexOf("/"));

    return (
      <div className="profile__details">
        <div className="profile__details-header">
          <h5>Storage Service </h5>
          <div className="docuentaion-documents">
            <div className="docuentaion-breadcrumbs">
              {this.state.fullPath && (
                <div
                  className="root"
                  onClick={() => {
                    this.openFolder("");
                  }}
                >
                  <img
                    alt=""
                    src="/assets/icons/root-directory.png"
                    title="root folder"
                  />{" "}
                  /
                </div>
              )}

              {this.state.fullPath &&
                this.state.fullPath.split("/").map((key, fieldIndex) => {
                  return (
                    <div className="document" key={fieldIndex}>
                      <div
                        className="folder"
                        onClick={() => {
                          this.openFolder(
                            this.state.fullPath
                              .split("/")
                              .slice(0, fieldIndex + 1)
                              .join("/")
                          );
                        }}
                      >
                        {key} /
                      </div>
                    </div>
                  );
                })}
            </div>

            {this.state.showBackArrow && (
              <div
                className="back"
                title="go back"
                onClick={() => {
                  this.openFolder(backPath || this.state.lastPath);
                }}
              >
                <img alt="" src="/assets/icons/back.png" />
              </div>
            )}

            {this.props.documentsList &&
              Object.keys(this.props.documentsList).map((key, fieldIndex) => {
                const attributes = this.props.documentsList[key];

                return (
                  <div className="document" key={fieldIndex}>
                    {attributes.type === "Folder" ? (
                      <div
                        className="folder"
                        onClick={() => {
                          this.openFolder(attributes.value);
                        }}
                      >
                        <img alt="" src="/assets/icons/folder.png" />

                        {attributes.value.substr(
                          attributes.value.lastIndexOf("/") + 1
                        )}
                      </div>
                    ) : (
                      <div
                        className="file"
                        onClick={() => {
                          this.downloadFile(attributes.value);
                        }}
                      >
                        <img alt="" src="/assets/icons/file.png" />
                        {attributes.value.substr(
                          attributes.value.lastIndexOf("/") + 1
                        )}
                      </div>
                    )}
                  </div>
                );
              })}

            {this.state.fullPath !== "" && (
              <input
                type="file"
                style={{ display: "none" }}
                ref={this.uploadInput}
                onChange={this.handleFileUpload}
              />
            )}

            {this.state.currentUploadedDocument && (
              <div className="drop-box-upload">
                <div className="heading">File Upload :</div>
                <div className="upload-box">
                  <span className="upload-filename">
                    {this.state.currentUploadedDocument.name}
                  </span>
                </div>
                <div className="file-upload-actions">
                  <img
                    className="file-action-icons file-upload-save"
                    alt=""
                    src={"/assets/icons/validated.svg"}
                    onClick={() => this.uploadFile()}
                  />
                  <img
                    className="file-action-icons file-upload-cancel"
                    alt=""
                    src={"/assets/icons/close.png"}
                    onClick={() =>
                      this.setState({
                        currentUploadedDocument: undefined,
                      })
                    }
                  />
                </div>
              </div>
            )}
            {!this.state.currentUploadedDocument && this.state.fullPath !== "" && (
              <div
                className="document-label-details action-item"
                onClick={() => (this.uploadInput as any).current.click()}
              >
                Upload File
              </div>
            )}
            {this.state.error && (
              <p style={{ color: "red" }}>{this.state.error}</p>
            )}
          </div>
        </div>
      </div>
    );
  };

  openFolder = (value) => {
    this.props.fetchPathListing(this.props.customerId, value).then((action) => {
      if (action.type === FETCH_PATH_LISTING_SUCCESS) {
        this.setState({
          showBackArrow: true,
          lastPath: value.substr(0, value.lastIndexOf("/")),
        });
        if (value === "") {
          this.setState({
            showBackArrow: false,
          });
        }
      }
    });
    this.setState({
      fullPath: value,
      lastPath: value.substr(0, value.lastIndexOf("/")),
    });
  };

  downloadFile = (value) => {
    this.props
      .downloadFilefromDropBox(this.props.customerId, value)
      .then((action) => {
        if (action.type === DOWNLOAD_FILE_SUCCESS) {
          if (action.response && action.response !== "") {
            const url = action.response.file_path;
            const link = document.createElement("a");
            link.href = url;
            link.target = "_blank";
            link.setAttribute("download", action.response.file_name);
            document.body.appendChild(link);
            link.click();
          }
        }
      });
  };

  fetchTaskStatus = (taskId, list, message: string, idInState) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then((a) => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === "SUCCESS"
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            list();
            this.setState({ [idInState]: "" });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "FAILURE"
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ [idInState]: "" });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "PENDING"
          ) {
            setTimeout(() => {
              this.fetchTaskStatus(taskId, list, message, idInState);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.fetchPathListing(
            this.props.customerId,
            this.state.fullPath
          );
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  handleFileUpload = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile: any = files[0];
    var FileSize = dataFile.size / 1024 / 1024; // in MB
    if (FileSize > 15) {
      this.setState({ error: " 15 MB File size limit exceeded" });
      setTimeout(() => {
        this.setState({
          error: "",
        }); // tslint:disable-next-line:align
      }, 10000);
    } else {
      this.setState({
        currentUploadedDocument: dataFile,
        error: "",
      });
    }
  };
  fetchPathListing = () => {
    this.props.fetchPathListing(this.props.customerId, this.state.fullPath);
  };
  uploadFile = () => {
    const data = new FormData();
    data.append("uploaded_file", this.state.currentUploadedDocument);
    this.props
      .uploadFiletoDropBox(this.props.customerId, this.state.fullPath, data)
      .then((action) => {
        if (action.type === DOWNLOAD_FILE_SUCCESS) {
          this.setState({
            currentUploadedDocument: undefined,
          });
          this.props.addSuccessMessage(`File uploaded`);
          this.fetchPathListing();
        } else {
          this.props.addErrorMessage(action.response.error.message);
        }
      });
  };
  render() {
    return (
      <div className="profile">
        <div className="profile__header">
          <div className="loader">
            <Spinner show={this.props.isFetching} />
          </div>
          <div className="profile__Body">{this.getBody()}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.documentation.isFetching,
  user: state.profile.user,
  customerId: state.customer.customerId,
  documentsList: state.documentation.documentsList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPathListing: (customerId: string, folderName?: string) =>
    dispatch(fetchPathListing(customerId, folderName)),
  downloadFilefromDropBox: (customerId: number, fileName: string) =>
    dispatch(downloadFilefromDropBox(customerId, fileName)),
  uploadFiletoDropBox: (customerId: number, fileName: string, data: any) =>
    dispatch(uploadFiletoDropBox(customerId, fileName, data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StorageService);
