import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "react-router-redux";

import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import PrivateRoute from "./components/PrivateRoute/privateRoute";
import Home from "./containers/Home/home";
import TWOFA from "./containers/Login/2fa";
import ForgotPassword from "./containers/Login/forgotPassword";
import Login from "./containers/Login/login";
import ResetPassword from "./containers/Login/resetPassword";
import { setAPIBaseUrl as initAPIMiddleware } from "./middleware/ApiMiddleware";
import { setAPIBaseUrl as initAuthMiddleware } from "./middleware/AuthMiddleware";
import store, { history } from "./store";

import "./main.scss";
Sentry.init({
  dsn: process.env.SENTRY_KEY,
  integrations: [new BrowserTracing()],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});
const initApp = () => {
  initAPIMiddleware(process.env.API_URL);
  initAuthMiddleware(process.env.API_URL);
};
initApp();
function FallbackComponent() {
  return <div>An error has occurred</div>;
}
const myFallback = <FallbackComponent />;

ReactDOM.render(
  <Sentry.ErrorBoundary fallback={myFallback} showDialog>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/eyJ0eXAiOiJKdV1QiLCJ2fa" component={TWOFA} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route
            exact
            path="/set-password/:uid/:token/"
            component={ResetPassword}
          />
          <PrivateRoute path="/" component={Home} />
        </Switch>
      </ConnectedRouter>
    </Provider>
  </Sentry.ErrorBoundary>,
  document.getElementById("app")
);
