import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_PROFILE_REQUEST = 'FETCH_PROFILE_REQUEST';
export const FETCH_PROFILE_SUCCESS = 'FETCH_PROFILE_SUCCESS';
export const FETCH_PROFILE_FAILURE = 'FETCH_PROFILE_FAILURE';

export const PROFILE_EDIT_REQUEST = 'PROFILE_EDIT_REQUEST';
export const PROFILE_EDIT_SUCCESS = 'PROFILE_EDIT_SUCCESS';
export const PROFILE_EDIT_FAILURE = 'PROFILE_EDIT_FAILURE';

export const PROFILE_UPLOAD_REQUEST = 'PROFILE_UPLOAD_REQUEST';
export const PROFILE_UPLOAD_SUCCESS = 'PROFILE_UPLOAD_SUCCESS';
export const PROFILE_UPLOAD_FAILURE = 'PROFILE_UPLOAD_FAILURE';

export const FETCH_CUST_USERS_REQUEST = 'FETCH_CUST_USERS_REQUEST';
export const FETCH_CUST_USERS_SUCCESS = 'FETCH_CUST_USERS_SUCCESS';
export const FETCH_CUST_USERS_FAILURE = 'FETCH_CUST_USERS_FAILURE';

export const fetchUserProfile = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/profile',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROFILE_REQUEST,
      FETCH_PROFILE_SUCCESS,
      FETCH_PROFILE_FAILURE,
    ],
  },
});

export const editUserProfile = (user: ISuperUser) => ({
  [CALL_API]: {
    endpoint: '/api/v1/profile',
    method: 'put',
    authenticated: true,
    body: user,
    types: [PROFILE_EDIT_REQUEST, PROFILE_EDIT_SUCCESS, PROFILE_EDIT_FAILURE],
  },
});

export const uploadProfile = (data: any) => ({
  [CALL_API]: {
    endpoint: '/api/v1/profile/update-picture',
    method: 'post',
    authenticated: true,
    body: data,
    types: [
      PROFILE_UPLOAD_REQUEST,
      PROFILE_UPLOAD_SUCCESS,
      PROFILE_UPLOAD_FAILURE,
    ],
  },
});

export const fetchUserForCustomer = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/users?pagination=false`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUST_USERS_REQUEST,
      FETCH_CUST_USERS_SUCCESS,
      FETCH_CUST_USERS_FAILURE,
    ],
  },
});
