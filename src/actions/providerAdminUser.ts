import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_PROVIDER_ADMIN_REQUEST = "FETCH_PROVIDER_ADMIN_REQUEST";
export const FETCH_PROVIDER_ADMIN_SUCCESS = "FETCH_PROVIDER_ADMIN_SUCCESS";
export const FETCH_PROVIDER_ADMIN_FAILURE = "FETCH_PROVIDER_ADMIN_FAILURE";

export const PROVIDER_ADMIN_EDIT_REQUEST = "PROVIDER_ADMIN_EDIT_REQUEST";
export const PROVIDER_ADMIN_EDIT_SUCCESS = "PROVIDER_ADMIN_EDIT_SUCCESS";
export const PROVIDER_ADMIN_EDIT_FAILURE = "PROVIDER_ADMIN_EDIT_FAILURE";

export const PROVIDER_ADMIN_ADD_REQUEST = "PROVIDER_ADMIN_ADD_REQUEST";
export const PROVIDER_ADMIN_ADD_SUCCESS = "PROVIDER_ADMIN_ADD_SUCCESS";
export const PROVIDER_ADMIN_ADD_FAILURE = "PROVIDER_ADMIN_ADD_FAILURE";

export const ACCESS_FEATURE_USERS_REQUEST = "ACESS_FEATURE_USERS_REQUEST";
export const ACCESS_FEATURE_USERS_SUCCESS = "ACESS_FEATURE_USERS_SUCCESS";
export const ACCESS_FEATURE_USERS_FAILURE = "ACESS_FEATURE_USERS_FAILURE";

export const GET_ACCESS_FEATURES_LIST_REQUEST =
  "GET_ACCESS_FEATURES_LIST_REQUEST";
export const GET_ACCESS_FEATURES_LIST_SUCCESS =
  "GET_ACCESS_FEATURES_LIST_SUCCESS";
export const GET_ACCESS_FEATURES_LIST_FAILURE =
  "GET_ACCESS_FEATURES_LIST_FAILURE";

export const PROVIDER_ADMIN_DELETE_REQUEST = "PROVIDER_ADMIN_DELETE_REQUEST";
export const PROVIDER_ADMIN_DELETE_SUCCESS = "PROVIDER_ADMIN_DELETE_SUCCESS";
export const PROVIDER_ADMIN_DELETE_FAILURE = "PROVIDER_ADMIN_DELETE_FAILURE";

export const FETCH_USER_ACCESS_REQUEST = "FETCH_USER_ACCESS_REQUEST";
export const FETCH_USER_ACCESS_SUCCESS = "FETCH_USER_ACCESS_SUCCESS";
export const FETCH_USER_ACCESS_FAILURE = "FETCH_USER_ACCESS_FAILURE";

export const fetchProviderAdmins = (
  providerId: number,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/providers/${providerId}/users`,
    method: "get",
    authenticated: true,
    params,
    types: [
      FETCH_PROVIDER_ADMIN_REQUEST,
      FETCH_PROVIDER_ADMIN_SUCCESS,
      FETCH_PROVIDER_ADMIN_FAILURE,
    ],
  },
});

export const addProviderAdminUser = (
  providerId: number,
  providerAdminUser: ISuperUser
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/providers/${providerId}/users`,
    method: "post",
    authenticated: true,
    body: providerAdminUser,
    types: [
      PROVIDER_ADMIN_ADD_REQUEST,
      PROVIDER_ADMIN_ADD_SUCCESS,
      PROVIDER_ADMIN_ADD_FAILURE,
    ],
  },
});

export const editProviderAdminUser = (
  providerId: number,
  providerAdminUser: ISuperUser
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/providers/${providerId}/users/${providerAdminUser.id}`,
    method: "put",
    authenticated: true,
    body: providerAdminUser,
    types: [
      PROVIDER_ADMIN_EDIT_REQUEST,
      PROVIDER_ADMIN_EDIT_SUCCESS,
      PROVIDER_ADMIN_EDIT_FAILURE,
    ],
  },
});

export const deleteProviderAdminUser = (
  providerId: number,
  userId: ISuperUser
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/providers/${providerId}/users/${userId}`,
    method: "delete",
    authenticated: true,
    types: [
      PROVIDER_ADMIN_DELETE_REQUEST,
      PROVIDER_ADMIN_DELETE_SUCCESS,
      PROVIDER_ADMIN_DELETE_FAILURE,
    ],
  },
});

export const setUserAccess = (list: any) => ({
  [CALL_API]: {
    endpoint: list[0].id
      ? `/api/v1/providers/user/feature/status/${list[0].id}`
      : `/api/v1/providers/user/feature/status`,
    method: list[0].id ? "put" : "post",
    body: list[0].id ? { ...list[0] } : list,
    authenticated: true,
    types: [
      FETCH_USER_ACCESS_REQUEST,
      FETCH_USER_ACCESS_SUCCESS,
      FETCH_USER_ACCESS_FAILURE,
    ],
  },
});

export const getAccessFeaturesList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/user/feature/list`,
    method: "get",
    authenticated: true,
    types: [
      GET_ACCESS_FEATURES_LIST_REQUEST,
      GET_ACCESS_FEATURES_LIST_SUCCESS,
      GET_ACCESS_FEATURES_LIST_FAILURE,
    ],
  },
});

export const getFeatureAccessUsers = (featureName?: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/user/feature/access${
      featureName ? `?feature=${featureName}` : ""
    }`,
    method: "get",
    authenticated: true,
    types: [
     ACCESS_FEATURE_USERS_REQUEST,
     ACCESS_FEATURE_USERS_SUCCESS,
     ACCESS_FEATURE_USERS_FAILURE,
    ],
  },
});

export const setFeatureAccessUsers = (payload: IFeatureAccess[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/user/feature/access`,
    method: "post",
    body: payload,
    authenticated: true,
    types: [
     ACCESS_FEATURE_USERS_REQUEST,
     ACCESS_FEATURE_USERS_SUCCESS,
     ACCESS_FEATURE_USERS_FAILURE,
    ],
  },
});
