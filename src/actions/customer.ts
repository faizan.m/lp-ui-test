import { trim } from "validator";
import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_CUSTOMERS_REQUEST = "FETCH_CUSTOMERS_REQUEST";
export const FETCH_CUSTOMERS_SUCCESS = "FETCH_CUSTOMERS_SUCCESS";
export const FETCH_CUSTOMERS_FAILURE = "FETCH_CUSTOMERS_FAILURE";
export const FETCH_CUSTOMERS_SHORT_REQUEST = "FETCH_CUSTOMERS_SHORT_REQUEST";
export const FETCH_CUSTOMERS_SHORT_SUCCESS = "FETCH_CUSTOMERS_SHORT_SUCCESS";
export const FETCH_CUSTOMERS_SHORT_FAILURE = "FETCH_CUSTOMERS_SHORT_FAILURE";
export const FETCH_CUSTOMER_USERS_REQUEST = "FETCH_CUSTOMER_USERS_REQUEST";
export const FETCH_CUSTOMER_USERS_SUCCESS = "FETCH_CUSTOMER_USERS_SUCCESS";
export const FETCH_CUSTOMER_USERS_FAILURE = "FETCH_CUSTOMER_USERS_FAILURE";
export const CUSTOMERS_CREATE_REQUEST = "CUSTOMERS_CREATE_REQUEST";
export const CUSTOMERS_CREATE_SUCCESS = "CUSTOMERS_CREATE_SUCCESS";
export const CUSTOMERS_CREATE_FAILURE = "CUSTOMERS_CREATE_FAILURE";
export const FETCH_SINGLE_CUSTOMER_REQUEST = "FETCH_SINGLE_CUSTOMER_REQUEST";
export const FETCH_SINGLE_CUSTOMER_SUCCESS = "FETCH_SINGLE_CUSTOMER_SUCCESS";
export const FETCH_SINGLE_CUSTOMER_FAILURE = "FETCH_SINGLE_CUSTOMER_FAILURE";
export const EDIT_CUSTOMER_USER_REQUEST = "EDIT_CUSTOMER_USER_REQUEST";
export const EDIT_CUSTOMER_USER_SUCCESS = "EDIT_CUSTOMER_USER_SUCCESS";
export const EDIT_CUSTOMER_USER_FAILURE = "EDIT_CUSTOMER_USER_FAILURE";
export const EDIT_CUSTOMER_REQUEST = "EDIT_CUSTOMER_REQUEST";
export const EDIT_CUSTOMER_SUCCESS = "EDIT_CUSTOMER_SUCCESS";
export const EDIT_CUSTOMER_FAILURE = "EDIT_CUSTOMER_FAILURE";
export const CUSTOMERS_CLEAR = "CUSTOMERS_CLEAR";
export const SET_CUSTOMER_ID = "SET_CUSTOMER_ID";
export const CREATE_CUSTOMERS_USER_REQUEST = "CREATE_CUSTOMERS_USER_REQUEST";
export const CREATE_CUSTOMERS_USER_SUCCESS = "CREATE_CUSTOMERS_USER_SUCCESS";
export const CREATE_CUSTOMERS_USER_FAILURE = "CREATE_CUSTOMERS_USER_FAILURE";
export const MERGE_CUSTOMER_USER_REQUEST = "MERGE_CUSTOMER_USER_REQUEST";
export const MERGE_CUSTOMER_USER_SUCCESS = "MERGE_CUSTOMER_USER_SUCCESS";
export const MERGE_CUSTOMER_USER_FAILURE = "MERGE_CUSTOMER_USER_FAILURE";
export const LAST_VISITED_CLIENT_360_TAB = "LAST_VISITED_CLIENT_360_TAB";

export const fetchCustomers = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/customers",
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_CUSTOMERS_REQUEST,
      FETCH_CUSTOMERS_SUCCESS,
      FETCH_CUSTOMERS_FAILURE,
    ],
  },
});

export const fetchCustomersShort = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/customers?full-info=false",
    method: "get",
    authenticated: true,
    types: [
      FETCH_CUSTOMERS_SHORT_REQUEST,
      FETCH_CUSTOMERS_SHORT_SUCCESS,
      FETCH_CUSTOMERS_SHORT_FAILURE,
    ],
  },
});

export const clearCustomers = () => ({
  type: CUSTOMERS_CLEAR,
});

export const fetchSingleCustomer = (id: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${parseInt(id, 10)}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_SINGLE_CUSTOMER_REQUEST,
      FETCH_SINGLE_CUSTOMER_SUCCESS,
      FETCH_SINGLE_CUSTOMER_FAILURE,
    ],
  },
});

export const editCustomer = (id: string, customer: Icustomer) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${parseInt(id, 10)}`,
    method: "put",
    authenticated: true,
    body: customer,
    types: [
      EDIT_CUSTOMER_REQUEST,
      EDIT_CUSTOMER_SUCCESS,
      EDIT_CUSTOMER_FAILURE,
    ],
  },
});

export const fetchCustomerUsers = (
  id: number,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/users`,
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_CUSTOMER_USERS_REQUEST,
      FETCH_CUSTOMER_USERS_SUCCESS,
      FETCH_CUSTOMER_USERS_FAILURE,
    ],
  },
});

export const editCustomerUser = (id: string, user: ISuperUser) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/customers` +
      `/${parseInt(id, 10)}/users/${trim(user.id)}`,
    method: "put",
    authenticated: true,
    body: user,
    types: [
      EDIT_CUSTOMER_USER_REQUEST,
      EDIT_CUSTOMER_USER_SUCCESS,
      EDIT_CUSTOMER_USER_FAILURE,
    ],
  },
});

export const createCustomer = (customer: Icustomer) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/customers",
    method: "post",
    authenticated: false,
    body: customer,
    types: [
      CUSTOMERS_CREATE_REQUEST,
      CUSTOMERS_CREATE_SUCCESS,
      CUSTOMERS_CREATE_FAILURE,
    ],
  },
});

export const activateCustomerUser = (customerId: number, user: ISuperUser) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/users/${user.id}/activate`,
    method: "put",
    authenticated: true,
    body: { user_role: user.user_role },
    types: [
      CREATE_CUSTOMERS_USER_REQUEST,
      CREATE_CUSTOMERS_USER_SUCCESS,
      CREATE_CUSTOMERS_USER_FAILURE,
    ],
  },
});

export const setCustomerId = (id: number) => ({
  type: SET_CUSTOMER_ID,
  id,
});
export const getCustomerUser = (id: string, custId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${custId}/users/${id}`,
    method: "GET",
    authenticated: true,
    types: [
      "GET_CUSTOMER_USER_REQUEST",
      "GET_CUSTOMER_USER_SUCCESS",
      "GET_CUSTOMER_USER_FAILURE",
    ],
  },
});

export const mergeCustomers = (
  sourceId: number,
  targetId: number,
  token: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/merge`,
    method: "post",
    body: {
      token,
      source_customer_id: sourceId,
      target_customer_id: targetId,
    },
    authenticated: true,
    types: [
      MERGE_CUSTOMER_USER_REQUEST,
      MERGE_CUSTOMER_USER_SUCCESS,
      MERGE_CUSTOMER_USER_FAILURE,
    ],
  },
});
