import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_DEVICES_REQUEST = "FETCH_DEVICES_REQUEST";
export const FETCH_DEVICES_SUCCESS = "FETCH_DEVICES_SUCCESS";
export const FETCH_DEVICES_FAILURE = "FETCH_DEVICES_FAILURE";
export const FETCH_COUNTRIES_REQUEST = "FETCH_COUNTRIES_REQUEST";
export const FETCH_COUNTRIES_SUCCESS = "FETCH_COUNTRIES_SUCCESS";
export const FETCH_COUNTRIES_FAILURE = "FETCH_CUSTOMERS_FAILURE";
export const ADD_DEVICE_REQUEST = "ADD_DEVICE_REQUEST";
export const ADD_DEVICE_SUCCESS = "ADD_DEVICE_SUCCESS";
export const ADD_DEVICE_FAILURE = "ADD_DEVICE_FAILURE";
export const ADD_SITE_REQUEST = "ADD_SITE_REQUEST";
export const ADD_SITE_SUCCESS = "ADD_SITE_SUCCESS";
export const ADD_SITE_FAILURE = "ADD_SITE_FAILURE";
export const DELETE_SITE_REQUEST = "DELETE_SITE_REQUEST";
export const DELETE_SITE_SUCCESS = "DELETE_SITE_SUCCESS";
export const DELETE_SITE_FAILURE = "DELETE_SITE_FAILURE";
export const FETCH_MANUFACTURERS_REQUEST = "FETCH_MANUFACTURERS_REQUEST";
export const FETCH_MANUFACTURERS_SUCCESS = "FETCH_MANUFACTURERS_SUCCESS";
export const FETCH_MANUFACTURERS_FAILURE = "FETCH_MANUFACTURERS_FAILURE";
export const FETCH_SITES_REQUEST = "FETCH_SITES_REQUEST";
export const FETCH_SITES_SUCCESS = "FETCH_SITES_SUCCESS";
export const FETCH_SITES_FAILURE = "FETCH_SITES_FAILURE";
export const FETCH_TYPES_REQUEST = "FETCH_TYPES_REQUEST";
export const FETCH_TYPES_SUCCESS = "FETCH_TYPES_SUCCESS";
export const FETCH_TYPES_FAILURE = "FETCH_TYPES_FAILURE";

export const FETCH_POTICKETS_REQUEST = "FETCH_POTICKETS_REQUEST";
export const FETCH_POTICKETS_SUCCESS = "FETCH_POTICKETS_SUCCESS";
export const FETCH_POTICKETS_FAILURE = "FETCH_POTICKETS_FAILURE";
export const FETCH_RECIPIENTS_REQUEST = "FETCH_RECIPIENTS_REQUEST";
export const FETCH_RECIPIENTS_SUCCESS = "FETCH_RECIPIENTS_SUCCESS";
export const FETCH_RECIPIENTS_FAILURE = "FETCH_RECIPIENTS_FAILURE";
export const FETCH_CONTRACT_STATUSES_REQUEST =
  "FETCH_CONTRACT_STATUSES_REQUEST";
export const FETCH_CONTRACT_STATUSES_SUCCESS =
  "FETCH_CONTRACT_STATUSES_SUCCESS";
export const FETCH_CONTRACT_STATUSES_FAILURE =
  "FETCH_CONTRACT_STATUSES_FAILURE";
export const FETCH_STATUSES_REQUEST = "FETCH_STATUSES_REQUEST";
export const FETCH_STATUSES_SUCCESS = "FETCH_STATUSES_SUCCESS";
export const FETCH_STATUSES_FAILURE = "FETCH_STATUSES_FAILURE";
export const DEVICE_UPDATE_REQUEST = "DEVICE_UPDATE_REQUEST";
export const DEVICE_UPDATE_SUCCESS = "DEVICE_UPDATE_SUCCESS";
export const DEVICE_UPDATE_FAILURE = "DEVICE_UPDATE_FAILURE";
export const UPDATE_STATUS_REQUEST = "UPDATE_STATUS_REQUEST";
export const UPDATE_STATUS_SUCCESS = "UPDATE_STATUS_SUCCESS";
export const UPDATE_STATUS_FAILURE = "UPDATE_STATUS_FAILURE";
export const FETCH_SINGLE_DEVICE_REQUEST = "FETCH_SINGLE_DEVICE_REQUEST";
export const FETCH_SINGLE_DEVICE_SUCCESS = "FETCH_SINGLE_DEVICE_SUCCESS";
export const FETCH_SINGLE_DEVICE_FAILURE = "FETCH_SINGLE_DEVICE_FAILURE";
export const EDIT_SINGLE_DEVICE_REQUEST = "EDIT_SINGLE_DEVICE_REQUEST";
export const EDIT_SINGLE_DEVICE_SUCCESS = "EDIT_SINGLE_DEVICE_SUCCESS";
export const EDIT_SINGLE_DEVICE_FAILURE = "EDIT_SINGLE_DEVICE_FAILURE";
export const TASK_STATUS_REQUEST = "TASK_STATUS_REQUEST";
export const TASK_STATUS_SUCCESS = "TASK_STATUS_SUCCESS";
export const TASK_STATUS_FAILURE = "TASK_STATUS_FAILURE";
export const BATCH_EXPORT_REQUEST = "BATCH_EXPORT_REQUEST";
export const BATCH_EXPORT_SUCCESS = "BATCH_EXPORT_SUCCESS";
export const BATCH_EXPORT_FAILURE = "BATCH_EXPORT_FAILURE";
export const SET_XLSX_DATA = "SET_XLSX_DATA";
export const FETCH_XLSX_REQUEST = "FETCH_XLSX_REQUEST";
export const FETCH_XLSX__SUCCESS = "FETCH_XLSX__SUCCESS";
export const FETCH_DATE_FORMAT_REQUEST = "FETCH_DATE_FORMAT_REQUEST";
export const FETCH_DATE_FORMAT_SUCCESS = "FETCH_DATE_FORMAT_SUCCESS";
export const FETCH_DATE_FORMAT_FAILURE = "FETCH_DATE_FORMAT_FAILURE";

export const UPLOAD_DEVICE_FILE_REQUEST = "UPLOAD_DEVICE_FILE_REQUEST";
export const UPLOAD_DEVICE_FILE_SUCCESS = "UPLOAD_DEVICE_FILE_SUCCESS";
export const UPLOAD_DEVICE_FILE_FAILURE = "UPLOAD_DEVICE_FILE_FAILURE";

export const RENEWAL_HISTORY_REQUEST = "RENEWAL_HISTORY_REQUEST";
export const RENEWAL_HISTORY_SUCCESS = "RENEWAL_HISTORY_SUCCESS";
export const RENEWAL_HISTORY_FAILURE = "RENEWAL_HISTORY_FAILURE";

export const GET_P_O_REQUEST = "GET_P_O_REQUEST";
export const GET_P_O_SUCCESS = "GET_P_O_SUCCESS";
export const GET_P_O_FAILURE = "GET_P_O_FAILURE";

export const GET_P_O_ALL_REQUEST = "GET_P_O_ALL_REQUEST";
export const GET_P_O_ALL_SUCCESS = "GET_P_O_ALL_SUCCESS";
export const GET_P_O_ALL_FAILURE = "GET_P_O_ALL_FAILURE";

export const GET_SINGLE_PO_REQUEST = "GET_SINGLE_PO_REQUEST";
export const GET_SINGLE_PO_SUCCESS = "GET_SINGLE_PO_SUCCESS";
export const GET_SINGLE_PO_FAILURE = "GET_SINGLE_PO_FAILURE";

export const GET_P_O_ALL_S_REQUEST = "GET_P_O_ALL_S_REQUEST";
export const GET_P_O_ALL_S_SUCCESS = "GET_P_O_ALL_S_SUCCESS";
export const GET_P_O_ALL_S_FAILURE = "GET_P_O_ALL_S_FAILURE";

export const UNBLOCK_RENEWAL_REQUEST = "UNBLOCK_RENEWAL_REQUEST";
export const UNBLOCK_RENEWAL_SUCCESS = "UNBLOCK_RENEWAL_SUCCESS";
export const UNBLOCK_RENEWAL_FAILURE = "UNBLOCK_RENEWAL_FAILURE";

export const FETCH_DEVICE_CATEGORIES_REQUEST =
  "FETCH_DEVICE_CATEGORIES_REQUEST";
export const FETCH_DEVICE_CATEGORIES_SUCCESS =
  "FETCH_DEVICE_CATEGORIES_SUCCESS";
export const FETCH_DEVICE_CATEGORIES_FAILURE =
  "FETCH_DEVICE_CATEGORIES_FAILURE";

export const CREATE_DEVICE_ASSOCIATION_REQUEST =
  "CREATE_DEVICE_ASSOCIATION_REQUEST";
export const CREATE_DEVICE_ASSOCIATION_SUCCESS =
  "CREATE_DEVICE_ASSOCIATION_SUCCESS";
export const CREATE_DEVICE_ASSOCIATION_FAILURE =
  "CREATE_DEVICE_ASSOCIATION_FAILURE";

export const FETCH_EXISTING_DEVICE_ASSOCIATION_REQUEST =
  "FETCH_EXISTING_DEVICE_ASSOCIATION_REQUEST";
export const FETCH_EXISTING_DEVICE_ASSOCIATION_SUCCESS =
  "FETCH_EXISTING_DEVICE_ASSOCIATION_SUCCESS";
export const FETCH_EXISTING_DEVICE_ASSOCIATION_FAILURE =
  "FETCH_EXISTING_DEVICE_ASSOCIATION_FAILURE";

export const FETCH_AVAILABLE_DEVICE_ASSOCIATION_REQUEST =
  "FETCH_AVAILABLE_DEVICE_ASSOCIATION_REQUEST";
export const FETCH_AVAILABLE_DEVICE_ASSOCIATION_SUCCESS =
  "FETCH_AVAILABLE_DEVICE_ASSOCIATION_SUCCESS";
export const FETCH_AVAILABLE_DEVICE_ASSOCIATION_FAILURE =
  "FETCH_AVAILABLE_DEVICE_ASSOCIATION_FAILURE";

export const FETCH_P_INTEGRATIONS_REQUEST = "FETCH_P_INTEGRATIONS_REQUEST";
export const FETCH_P_INTEGRATIONS_SUCCESS = "FETCH_P_INTEGRATIONS_SUCCESS";
export const FETCH_P_INTEGRATIONS_FAILURE = "FETCH_P_INTEGRATIONS_FAILURE";

export const BATCH_CREATE_REQUEST = "BATCH_CREATE_REQUEST";
export const BATCH_CREATE_SUCCESS = "BATCH_CREATE_SUCCESS";
export const BATCH_CREATE_FAILURE = "BATCH_CREATE_FAILURE";

export const SMARTNET_VALIDATE_REQUEST = "SMARTNET_VALIDATE_REQUEST";
export const SMARTNET_VALIDATE_SUCCESS = "SMARTNET_VALIDATE_SUCCESS";
export const SMARTNET_VALIDATE_FAILURE = "SMARTNET_VALIDATE_FAILURE";

export const FETCH_ALL_DEVICES_REQUEST = "FETCH_ALL_DEVICES_REQUEST";
export const FETCH_ALL_DEVICES_SUCCESS = "FETCH_ALL_DEVICES_SUCCESS";
export const FETCH_ALL_DEVICES_FAILURE = "FETCH_ALL_DEVICES_FAILURE";

export const POST_LINE_ITEMS_REQUEST = "POST_LINE_ITEMS_REQUEST";
export const POST_LINE_ITEMS_SUCCESS = "POST_LINE_ITEMS_SUCCESS";
export const POST_LINE_ITEMS_FAILURE = "POST_LINE_ITEMS_FAILURE";

export const INGRAM_DATA_REQUEST = "INGRAM_DATA_REQUEST";
export const INGRAM_DATA_SUCCESS = "INGRAM_DATA_SUCCESS";
export const INGRAM_DATA_FAILURE = "INGRAM_DATA_FAILURE";

export const LINE_ITEMS_REQUEST = "LINE_ITEMS_REQUEST";
export const LINE_ITEMS_SUCCESS = "LINE_ITEMS_SUCCESS";
export const LINE_ITEMS_FAILURE = "LINE_ITEMS_FAILURE";

export const BULK_SITE_REQUEST = "BULK_SITE_REQUEST";
export const BULK_SITE_SUCCESS = "BULK_SITE_SUCCESS";
export const BULK_SITE_FAILURE = "BULK_SITE_FAILURE";

export const LAST_VISITED_ASSET_SITE = "LAST_VISITED_ASSET_SITE";

export const fetchDevices = (id: number, no: boolean = false) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/devices?show-inactive=${no}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_DEVICES_REQUEST,
      FETCH_DEVICES_SUCCESS,
      FETCH_DEVICES_FAILURE,
    ],
  },
});

export const fetchComplianceDevices = (id: number, no: boolean = false) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/devices/compliance?show-inactive=${no}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_DEVICES_REQUEST,
      FETCH_DEVICES_SUCCESS,
      FETCH_DEVICES_FAILURE,
    ],
  },
});

export const fetchCountriesAndStates = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/countries-states`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_COUNTRIES_REQUEST,
      FETCH_COUNTRIES_SUCCESS,
      FETCH_COUNTRIES_FAILURE,
    ],
  },
});

export const fetchDevicesCustomerUser = (showInactive: boolean = false) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices?show-inactive=${showInactive}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_DEVICES_REQUEST,
      FETCH_DEVICES_SUCCESS,
      FETCH_DEVICES_FAILURE,
    ],
  },
});

export const fetchAllCustomerDevices = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/all-devices`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_ALL_DEVICES_REQUEST,
      FETCH_ALL_DEVICES_SUCCESS,
      FETCH_ALL_DEVICES_FAILURE,
    ],
  },
});

export const addDevice = (customerId: number, newDevice: IDevice) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/devices`,
    method: "post",
    body: newDevice,
    authenticated: true,
    types: [ADD_DEVICE_REQUEST, ADD_DEVICE_SUCCESS, ADD_DEVICE_FAILURE],
  },
});

export const addDeviceCustomerUser = (newDevice: IDevice) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices`,
    method: "post",
    body: newDevice,
    authenticated: true,
    types: [ADD_DEVICE_REQUEST, ADD_DEVICE_SUCCESS, ADD_DEVICE_FAILURE],
  },
});

export const fetchManufacturers = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/manufacturers",
    method: "get",
    authenticated: true,
    types: [
      FETCH_MANUFACTURERS_REQUEST,
      FETCH_MANUFACTURERS_SUCCESS,
      FETCH_MANUFACTURERS_FAILURE,
    ],
  },
});

export const fetchContractStatuses = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/contract-statuses",
    method: "get",
    authenticated: true,
    types: [
      FETCH_CONTRACT_STATUSES_REQUEST,
      FETCH_CONTRACT_STATUSES_SUCCESS,
      FETCH_CONTRACT_STATUSES_FAILURE,
    ],
  },
});

export const fetchStatuses = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/devices/statuses",
    method: "get",
    authenticated: true,
    types: [
      FETCH_STATUSES_REQUEST,
      FETCH_STATUSES_SUCCESS,
      FETCH_STATUSES_FAILURE,
    ],
  },
});

export const fetchSites = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/sites`,
    method: "get",
    authenticated: true,
    types: [FETCH_SITES_REQUEST, FETCH_SITES_SUCCESS, FETCH_SITES_FAILURE],
  },
});

export const fetchSitesCustomersUser = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/sites`,
    method: "get",
    authenticated: true,
    types: [FETCH_SITES_REQUEST, FETCH_SITES_SUCCESS, FETCH_SITES_FAILURE],
  },
});

export const fetchSitesProvidersCustomersUser = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/sites`,
    method: "get",
    authenticated: true,
    types: [FETCH_SITES_REQUEST, FETCH_SITES_SUCCESS, FETCH_SITES_FAILURE],
  },
});

export const fetchTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/device-types`,
    method: "get",
    authenticated: true,
    types: [FETCH_TYPES_REQUEST, FETCH_TYPES_SUCCESS, FETCH_TYPES_FAILURE],
  },
});

export const addSite = (customerId: number, newSite: ISite) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/sites`,
    method: "post",
    body: newSite,
    authenticated: true,
    types: [ADD_SITE_REQUEST, ADD_SITE_SUCCESS, ADD_SITE_FAILURE],
  },
});

export const editSite = (customerId: number, newSite: ISite) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/sites/${newSite.site_id}`,
    method: "put",
    body: newSite,
    authenticated: true,
    types: [ADD_SITE_REQUEST, ADD_SITE_SUCCESS, ADD_SITE_FAILURE],
  },
});

export const deleteCustomerSite = (customerId: number, newSite: ISite) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/sites/${newSite.site_id}`,
    method: "delete",
    authenticated: true,
    types: [DELETE_SITE_REQUEST, DELETE_SITE_SUCCESS, DELETE_SITE_FAILURE],
  },
});

export const addSiteCU = (newSite: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/sites`,
    method: "post",
    body: newSite,
    authenticated: true,
    types: [ADD_SITE_REQUEST, ADD_SITE_SUCCESS, ADD_SITE_FAILURE],
  },
});

export const editSiteCU = (newSite: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/sites/${newSite.site_id}`,
    method: "put",
    body: newSite,
    authenticated: true,
    types: [ADD_SITE_REQUEST, ADD_SITE_SUCCESS, ADD_SITE_FAILURE],
  },
});

export const updateContract = (
  customerId: number,
  contractNo: any,
  deviceIds: number[]
) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/customers/` + `${customerId}/devices/contract-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: contractNo,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});

export const updateDeviceCategoryPU = (
  customerId: number,
  category: any,
  deviceIds: number[]
) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/customers/` + `${customerId}/devices/category-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: category,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});

export const updateDeviceCategoryCU = (category: any, deviceIds: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/category-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: category,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});
export const updateStatus = (
  customerId: number,
  contractNo: any,
  deviceIds: number[]
) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/customers/` + `${customerId}/devices/status-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: contractNo,
    },
    authenticated: true,
    types: [
      UPDATE_STATUS_REQUEST,
      UPDATE_STATUS_SUCCESS,
      UPDATE_STATUS_FAILURE,
    ],
  },
});

export const updateStatusCustomer = (contractNo: any, deviceIds: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/status-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: contractNo,
    },
    authenticated: true,
    types: [
      UPDATE_STATUS_REQUEST,
      UPDATE_STATUS_SUCCESS,
      UPDATE_STATUS_FAILURE,
    ],
  },
});

export const resetManufacturerApiRetryCount = (serialNum: string) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/devices/${serialNum}/actions/reset-manufacturer-api-retry-count`,
    method: "post",
    authenticated: true,
    types: [
      FETCH_SINGLE_DEVICE_REQUEST,
      FETCH_SINGLE_DEVICE_SUCCESS,
      FETCH_SINGLE_DEVICE_FAILURE,
    ],
  },
});

export const fetchSingleDevicePU = (customerId, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/devices/${id}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_SINGLE_DEVICE_REQUEST,
      FETCH_SINGLE_DEVICE_SUCCESS,
      FETCH_SINGLE_DEVICE_FAILURE,
    ],
  },
});

export const fetchSingleDevice = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/${id}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_SINGLE_DEVICE_REQUEST,
      FETCH_SINGLE_DEVICE_SUCCESS,
      FETCH_SINGLE_DEVICE_FAILURE,
    ],
  },
});

export const fetchDeviceRenwalHistory = (
  customerId: number,
  deviceId: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/devices/${deviceId}/renewal-history`,
    method: "get",
    authenticated: true,
    types: [
      RENEWAL_HISTORY_REQUEST,
      RENEWAL_HISTORY_SUCCESS,
      RENEWAL_HISTORY_FAILURE,
    ],
  },
});

export const fetchDeviceRenwalHistoryCustomer = (deviceId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/${deviceId}/renewal-history`,
    method: "get",
    authenticated: true,
    types: [
      RENEWAL_HISTORY_REQUEST,
      RENEWAL_HISTORY_SUCCESS,
      RENEWAL_HISTORY_FAILURE,
    ],
  },
});

export const getListPurchaseOrders = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/purchase-orders`,
    method: "get",
    authenticated: true,
    types: [GET_P_O_REQUEST, GET_P_O_SUCCESS, GET_P_O_FAILURE],
  },
});
export const getListPurchaseOrdersAll = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/email-template/purchase_orders`,
    method: "get",
    authenticated: true,
    types: [GET_P_O_ALL_REQUEST, GET_P_O_ALL_SUCCESS, GET_P_O_ALL_FAILURE],
  },
});

export const getSinglePurchaseOrder = (purchase_order_crm_id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/receiving/purchase-order-details/${purchase_order_crm_id}`,
    method: "get",
    authenticated: true,
    types: [
      GET_SINGLE_PO_REQUEST,
      GET_SINGLE_PO_SUCCESS,
      GET_SINGLE_PO_FAILURE,
    ],
  },
});

export const getEmailTemplateByPOID = (
  poNumbers: number[],
  ids: number[],
  customerContact: any,
  customerId: any,
  toContactId: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/email-templates`,
    method: "post",
    body: {
      po_numbers: poNumbers,
      po_ids: ids,
      customer_contact: customerContact,
      customer_id: customerId,
      to_contact_id: toContactId,
    },
    authenticated: true,
    types: [
      FETCH_POTICKETS_REQUEST,
      FETCH_POTICKETS_SUCCESS,
      FETCH_POTICKETS_FAILURE,
    ],
  },
});
export const getListPOTickts = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/workflows/smartnet-receiving/purchase-order-tickets`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_POTICKETS_REQUEST,
      FETCH_POTICKETS_SUCCESS,
      FETCH_POTICKETS_FAILURE,
    ],
  },
});

export const getRecipientLists = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/workflows/smartnet-receiving/snt-email-recipient-list`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_RECIPIENTS_REQUEST,
      FETCH_RECIPIENTS_SUCCESS,
      FETCH_RECIPIENTS_FAILURE,
    ],
  },
});
export const unblockRenewalDevice = (deviceId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/${deviceId}/actions/continue-renewal`,
    method: "post",
    authenticated: true,
    types: [
      UNBLOCK_RENEWAL_REQUEST,
      UNBLOCK_RENEWAL_SUCCESS,
      UNBLOCK_RENEWAL_FAILURE,
    ],
  },
});

export const unblockRenewalDeviceProvider = (
  customerId: number,
  deviceId: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/devices/${deviceId}/actions/continue-renewal`,
    method: "post",
    authenticated: true,
    types: [
      UNBLOCK_RENEWAL_REQUEST,
      UNBLOCK_RENEWAL_SUCCESS,
      UNBLOCK_RENEWAL_FAILURE,
    ],
  },
});

export const editSingleDevicePU = (
  customerId: number,
  id: number,
  device: IDevice
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/devices/${id}`,
    method: "put",
    body: device,
    authenticated: true,
    types: [
      EDIT_SINGLE_DEVICE_REQUEST,
      EDIT_SINGLE_DEVICE_SUCCESS,
      EDIT_SINGLE_DEVICE_FAILURE,
    ],
  },
});

export const editSingleDevice = (id: number, device: IDevice) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/${id}`,
    method: "put",
    body: device,
    authenticated: true,
    types: [
      EDIT_SINGLE_DEVICE_REQUEST,
      EDIT_SINGLE_DEVICE_SUCCESS,
      EDIT_SINGLE_DEVICE_FAILURE,
    ],
  },
});

export const setXlsxData = (data: any, file: any) => ({
  type: SET_XLSX_DATA,
  data,
  file,
});

export const batchCreateImportData = (customerId: number, devices: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/devices/batch-create`,
    method: "post",
    body: devices,
    authenticated: true,
    types: [BATCH_CREATE_REQUEST, BATCH_CREATE_SUCCESS, BATCH_CREATE_FAILURE],
  },
});

export const batchCreateImportDataSmartNet = (
  customerId: number,
  devices: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/workflows/smartnet-receiving/import`,
    method: "post",
    body: devices,
    authenticated: true,
    types: [BATCH_CREATE_REQUEST, BATCH_CREATE_SUCCESS, BATCH_CREATE_FAILURE],
  },
});

export const smartnetImportFileValidation = (
  customerId: number,
  smartnetObject: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/workflows/smartnet-receiving/validation`,
    method: "post",
    body: smartnetObject,
    authenticated: true,
    types: [
      SMARTNET_VALIDATE_REQUEST,
      SMARTNET_VALIDATE_SUCCESS,
      SMARTNET_VALIDATE_FAILURE,
    ],
  },
});

export const batchCreateImportSubscription = (devices: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription/batch-create`,
    method: "post",
    body: devices,
    authenticated: true,
    types: [BATCH_CREATE_REQUEST, BATCH_CREATE_SUCCESS, BATCH_CREATE_FAILURE],
  },
});

export const fetchTaskStatus = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/task-status/${id}`,
    method: "get",
    authenticated: true,
    types: [TASK_STATUS_REQUEST, TASK_STATUS_SUCCESS, TASK_STATUS_FAILURE],
  },
});

export const importSmartNetEnable = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings/check`,
    method: "get",
    authenticated: true,
    types: [TASK_STATUS_REQUEST, TASK_STATUS_SUCCESS, TASK_STATUS_FAILURE],
  },
});

// tslint:disable-next-line:max-line-length
export const batchCreateExportData = (
  customerId: number,
  // tslint:disable-next-line:variable-name
  device_ids: any,
  showInactiveDevices: boolean
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/devices/export?show-inactive=${showInactiveDevices}`,
    method: "post",
    body: { device_ids },
    authenticated: true,
    responseType: "blob",
    types: [BATCH_EXPORT_REQUEST, BATCH_EXPORT_SUCCESS, BATCH_EXPORT_FAILURE],
  },
});

export const fetchDateFormat = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/date-formats`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_DATE_FORMAT_REQUEST,
      FETCH_DATE_FORMAT_SUCCESS,
      FETCH_DATE_FORMAT_FAILURE,
    ],
  },
});

export const uploadDeviceFile = (fileReq: any, name: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/devices-file-upload?filename=${name}`,
    method: "post",
    body: fileReq,
    authenticated: true,
    types: [
      UPLOAD_DEVICE_FILE_REQUEST,
      UPLOAD_DEVICE_FILE_SUCCESS,
      UPLOAD_DEVICE_FILE_FAILURE,
    ],
  },
});

export const fetchAvailableCircuitDeviceAssociation = (customerId: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-device-association/available`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_AVAILABLE_DEVICE_ASSOCIATION_REQUEST,
      FETCH_AVAILABLE_DEVICE_ASSOCIATION_SUCCESS,
      FETCH_AVAILABLE_DEVICE_ASSOCIATION_FAILURE,
    ],
  },
});

export const fetchExistingDeviceAssociation = (
  customerId: number,
  deviceId: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-device-association/device/${deviceId}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_EXISTING_DEVICE_ASSOCIATION_REQUEST,
      FETCH_EXISTING_DEVICE_ASSOCIATION_SUCCESS,
      FETCH_EXISTING_DEVICE_ASSOCIATION_FAILURE,
    ],
  },
});
export const createCircuitDeviceAssociation = (
  data: any,
  customerId: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-device-association/create`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_DEVICE_ASSOCIATION_REQUEST,
      CREATE_DEVICE_ASSOCIATION_SUCCESS,
      CREATE_DEVICE_ASSOCIATION_FAILURE,
    ],
  },
});

export const fetchAvailableCircuitDeviceAssociationCU = () => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/circuit-device-association/available`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_AVAILABLE_DEVICE_ASSOCIATION_REQUEST,
      FETCH_AVAILABLE_DEVICE_ASSOCIATION_SUCCESS,
      FETCH_AVAILABLE_DEVICE_ASSOCIATION_FAILURE,
    ],
  },
});

export const fetchExistingDeviceAssociationCU = (deviceId: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/circuit-device-association/device/${deviceId}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_EXISTING_DEVICE_ASSOCIATION_REQUEST,
      FETCH_EXISTING_DEVICE_ASSOCIATION_SUCCESS,
      FETCH_EXISTING_DEVICE_ASSOCIATION_FAILURE,
    ],
  },
});
export const createCircuitDeviceAssociationCU = (data: any) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/circuit-device-association/create`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_DEVICE_ASSOCIATION_REQUEST,
      CREATE_DEVICE_ASSOCIATION_SUCCESS,
      CREATE_DEVICE_ASSOCIATION_FAILURE,
    ],
  },
});

export const fetchDeviceCategories = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/device-categories",
    method: "get",
    authenticated: true,
    types: [
      FETCH_DEVICE_CATEGORIES_REQUEST,
      FETCH_DEVICE_CATEGORIES_SUCCESS,
      FETCH_DEVICE_CATEGORIES_FAILURE,
    ],
  },
});

export const fetchProviderIntegrationsForFilter = (providerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/${providerId}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_P_INTEGRATIONS_REQUEST,
      FETCH_P_INTEGRATIONS_SUCCESS,
      FETCH_P_INTEGRATIONS_FAILURE,
    ],
  },
});

export const updateSitesCU = (site: any, deviceIds: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/site-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: site,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});

export const updateSitesPU = (
  customerId: number,
  site: any,
  deviceIds: number[]
) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/customers/` + `${customerId}/devices/site-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: site,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});

export const updateManufaturerCU = (
  manufacturer: any,
  deviceIds: number[]
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices/manufacturer-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: manufacturer,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});

export const updateManufaturerPU = (
  customerId: number,
  category: any,
  deviceIds: number[]
) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/customers/` +
      `${customerId}/devices/manufacturer-update`,
    method: "post",
    body: {
      device_ids: deviceIds,
      value: category,
    },
    authenticated: true,
    types: [
      DEVICE_UPDATE_REQUEST,
      DEVICE_UPDATE_SUCCESS,
      DEVICE_UPDATE_FAILURE,
    ],
  },
});

export const getListPurchaseOrdersAllStatus = (custId: any) => ({
  [CALL_API]: {
    // endpoint: `/api/v1/providers/operations/email-template/purchase_orders`,
    endpoint: `/api/v1/providers/customers/${custId}/purchase-orders?show-all=true`,
    method: "get",
    authenticated: true,
    types: [
      GET_P_O_ALL_S_REQUEST,
      GET_P_O_ALL_S_SUCCESS,
      GET_P_O_ALL_S_FAILURE,
    ],
  },
});
export const getLineItemsOperations = (purchase_order_ids: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/receiving/line-items`,
    method: "post",
    body: {
      purchase_order_ids,
    },
    authenticated: true,
    types: [LINE_ITEMS_REQUEST, LINE_ITEMS_SUCCESS, LINE_ITEMS_FAILURE],
  },
});

export const getAllLineItemsOperations = (purchase_order_ids: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/receiving/all-line-items`,
    method: "post",
    body: {
      purchase_order_ids,
    },
    authenticated: true,
    types: [LINE_ITEMS_REQUEST, LINE_ITEMS_SUCCESS, LINE_ITEMS_FAILURE],
  },
});

export const getLineItemsIngramData = (
  purchase_orders: string[],
  line_item_identifiers: string[],
  purchase_order_id?: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/receiving/ingram-data`,
    method: "post",
    body: {
      purchase_orders,
      line_item_identifiers,
      purchase_order_id,
    },
    authenticated: true,
    types: [INGRAM_DATA_REQUEST, INGRAM_DATA_SUCCESS, INGRAM_DATA_FAILURE],
  },
});

export const postLineItemsData = (
  customer_id: any,
  purchase_order_id: number,
  ticket_id: number,
  ticket_note: string,
  line_items: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/receiving/update-po`,
    method: "post",
    body: {
      customer_id,
      purchase_order_id,
      ticket_id,
      ticket_note,
      line_items,
    },
    authenticated: true,
    types: [
      POST_LINE_ITEMS_REQUEST,
      POST_LINE_ITEMS_SUCCESS,
      POST_LINE_ITEMS_FAILURE,
    ],
  },
});

export const bulkSiteCreate = (id: number, sites: any[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/bulk-create-sites`,
    method: "post",
    body: sites,
    authenticated: true,
    types: [BULK_SITE_REQUEST, BULK_SITE_SUCCESS, BULK_SITE_FAILURE],
  },
});
