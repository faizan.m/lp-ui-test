import { CALL_API } from '../middleware/ApiMiddleware';

export const BATCH_REPORT_REQUEST = 'BATCH_REPORT_REQUEST';
export const BATCH_REPORT_SUCCESS_CUSTOMER = 'BATCH_REPORT_SUCCESS_CUSTOMER';
export const BATCH_REPORT_SUCCESS_PROVIDER = 'BATCH_REPORT_SUCCESS_PROVIDER';
export const BATCH_REPORT_SUCCESS = 'BATCH_REPORT_SUCCESS';
export const BATCH_REPORT_FAILURE = 'BATCH_REPORT_FAILURE';

export const POST_ANOMALY_REPORT_REQUEST = 'POST_ANOMALY_REPORT_REQUEST';
export const POST_ANOMALY_REPORT_SUCCESS = 'POST_ANOMALY_REPORT_SUCCESS';
export const POST_ANOMALY_REPORT_FAILURE = 'POST_ANOMALY_REPORT_FAILURE';

export const FETCH_REPORT_TYPE_REQUEST = 'FETCH_REPORT_TYPE_REQUEST';
export const FETCH_REPORT_TYPE_SUCCESS = 'FETCH_REPORT_TYPE_SUCCESS';
export const FETCH_REPORT_TYPE_FAILURE = 'FETCH_REPORT_TYPE_FAILURE';

export const FETCH_SHE_RE_TYPE_REQUEST = 'FETCH_SHE_RE_TYPE_REQUEST';
export const FETCH_SHE_RE_TYPE_SUCCESS = 'FETCH_SHE_RE_TYPE_SUCCESS';
export const FETCH_SHE_RE_TYPE_FAILURE = 'FETCH_SHE_RE_TYPE_FAILURE';

export const FETCH_SHE_RE_FREQUENCY_REQUEST = 'FETCH_SHE_RE_FREQUENCY_REQUEST';
export const FETCH_SHE_RE_FREQUENCY_SUCCESS = 'FETCH_SHE_RE_FREQUENCY_SUCCESS';
export const FETCH_SHE_RE_FREQUENCY_FAILURE = 'FETCH_SHE_RE_FREQUENCY_FAILURE';

export const POST_SH_REPORT_REQUEST = 'POST_SH_REPORT_REQUEST';
export const POST_SH_REPORT_SUCCESS = 'POST_SH_REPORT_SUCCESS';
export const POST_SH_REPORT_FAILURE = 'POST_SH_REPORT_FAILURE';

export const GET_SH_REPORT_REQUEST = 'GET_SH_REPORT_REQUEST';
export const GET_SH_REPORT_SUCCESS = 'GET_SH_REPORT_SUCCESS';
export const GET_SH_REPORT_FAILURE = 'GET_SH_REPORT_FAILURE';

export const FETCH_REP_CATEGORIES_REQUEST = 'FETCH_REP_CATEGORIES_REQUEST';
export const FETCH_REP_CATEGORIES_SUCCESS = 'FETCH_REP_CATEGORIES_SUCCESS';
export const FETCH_REP_CATEGORIES_FAILURE = 'FETCH_REP_CATEGORIES_FAILURE';

export const FETCH_CIRCUIT_INFO_RT_REQUEST = 'FETCH_CIRCUIT_INFO_RT_REQUEST';
export const FETCH_CIRCUIT_INFO_RT_SUCCESS = 'FETCH_CIRCUIT_INFO_RT_SUCCESS';
export const FETCH_CIRCUIT_INFO_RT_FAILURE = 'FETCH_CIRCUIT_INFO_RT_FAILURE';

export const CIRCUIT_REPORT_REQUEST = 'CIRCUIT_REPORT_REQUEST';
export const CIRCUIT_REPORT_SUCCESS = 'CIRCUIT_REPORT_SUCCESS';
export const CIRCUIT_REPORT_FAILURE = 'CIRCUIT_REPORT_FAILURE';

export const DEVICE_REPORT_BUNDLE_REQUEST = 'DEVICE_REPORT_BUNDLE_REQUEST';
export const DEVICE_REPORT_BUNDLE_SUCCESS = 'DEVICE_REPORT_BUNDLE_SUCCESS';
export const DEVICE_REPORT_BUNDLE_FAILURE = 'DEVICE_REPORT_BUNDLE_FAILURE';

export const CUSTOMER_ANALYSIS_RT_REQUEST = 'CUSTOMER_ANALYSIS_RT_REQUEST';
export const CUSTOMER_ANALYSIS_RT_SUCCESS = 'CUSTOMER_ANALYSIS_RT_SUCCESS';
export const CUSTOMER_ANALYSIS_RT_FAILURE = 'CUSTOMER_ANALYSIS_RT_FAILURE';

export const CUSTOMER_ANALYSIS_REPORT_REQUEST = 'CUSTOMER_ANALYSIS_REPORT_REQUEST'
export const CUSTOMER_ANALYSIS_REPORT_SUCCESS = 'CUSTOMER_ANALYSIS_REPORT_SUCCESS'
export const CUSTOMER_ANALYSIS_REPORT_FAILURE = 'CUSTOMER_ANALYSIS_REPORT_FAILURE'

export const FETCH_FINANCE_REPORT_REQUEST = 'FETCH_FINANCE_REPORT_REQUEST';
export const FETCH_FINANCE_REPORT_SUCCESS = 'FETCH_FINANCE_REPORT_SUCCESS';
export const FETCH_FINANCE_REPORT_FAILURE = 'FETCH_FINANCE_REPORT_FAILURE';

export const FETCH_FIRE_REPORT_REQUEST = 'FETCH_FINANCE_REPORT_REQUEST';
export const FETCH_FIRE_REPORT_SUCCESS = 'FETCH_FINANCE_REPORT_SUCCESS';
export const FETCH_FIRE_REPORT_FAILURE = 'FETCH_FINANCE_REPORT_FAILURE';

export const GET_FINANCE_STATUS_REQUEST = 'GET_FINANCE_STATUS_REQUEST';
export const GET_FINANCE_STATUS_SUCCESS = 'GET_FINANCE_STATUS_SUCCESS';
export const GET_FINANCE_STATUS_FAILURE = 'GET_FINANCE_STATUS_FAILURE';

export const GET_REPORT_D_STATS_REQUEST = 'GET_REPORT_D_STATS_REQUEST';
export const GET_REPORT_D_STATS_SUCCESS = 'GET_REPORT_D_STATS_SUCCESS';
export const GET_REPORT_D_STATS_FAILURE = 'GET_REPORT_D_STATS_FAILURE';


export const reportCustomerUser = filters => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/customer-device-management-report`,
    method: 'post',
    body: filters,
    authenticated: true,
    responseType: 'blob',
    types: [
      BATCH_REPORT_REQUEST,
      BATCH_REPORT_SUCCESS_CUSTOMER,
      BATCH_REPORT_FAILURE,
    ],
  },
});
export const reportProviderCustomerUser = filters => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-device-management-report`,
    method: 'post',
    body: filters,
    authenticated: true,
    responseType: 'blob',
    types: [
      BATCH_REPORT_REQUEST,
      BATCH_REPORT_SUCCESS_PROVIDER,
      BATCH_REPORT_FAILURE,
    ],
  },
});

export const dataAnomalyReport = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/devices/anomaly-report`,
    method: 'post',
    authenticated: true,
    types: [
      POST_ANOMALY_REPORT_REQUEST,
      POST_ANOMALY_REPORT_SUCCESS,
      POST_ANOMALY_REPORT_FAILURE,
    ],
  },
});

export const getCustomerReportTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customer-reporting-types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_REPORT_TYPE_REQUEST,
      FETCH_REPORT_TYPE_SUCCESS,
      FETCH_REPORT_TYPE_FAILURE,
    ],
  },
});

export const getCircuitInfoReportTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/circuit-info/report/types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CIRCUIT_INFO_RT_REQUEST,
      FETCH_CIRCUIT_INFO_RT_SUCCESS,
      FETCH_CIRCUIT_INFO_RT_FAILURE,
    ],
  },
});

export const getCustomerAnalysisReportTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-analysis-reports/meta`,
    method: 'get',
    authenticated: true,
    types: [
      CUSTOMER_ANALYSIS_RT_REQUEST,
      CUSTOMER_ANALYSIS_RT_SUCCESS,
      CUSTOMER_ANALYSIS_RT_FAILURE,
    ],
  },
});

export const downloadFinanceReport = (data: IFinanceReportFilters) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/finance/report`,
    params: data,
    method: "get",
    authenticated: true,
    types: [
      FETCH_FINANCE_REPORT_REQUEST,
      FETCH_FINANCE_REPORT_SUCCESS,
      FETCH_FINANCE_REPORT_FAILURE,
    ],
  },
});

export const downloadFireReport = (data: {
  site_crm_ids: number[];
  start_date: string;
  end_date: string;
}) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/fire-report`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      FETCH_FIRE_REPORT_REQUEST,
      FETCH_FIRE_REPORT_SUCCESS,
      FETCH_FIRE_REPORT_FAILURE,
    ],
  },
});

export const fetchFinanceReportStatuses = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/receiving/line-items/statuses`,
    method: "get",
    authenticated: true,
    types: [
      GET_FINANCE_STATUS_REQUEST,
      GET_FINANCE_STATUS_SUCCESS,
      GET_FINANCE_STATUS_FAILURE,
    ],
  },
});

export const reportCustomerAnalysis = (filters, customerId) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/customer-analysis-reports`,
    method: 'post',
    body: filters,
    authenticated: true,
    // responseType: 'blob',
    types: [
      CUSTOMER_ANALYSIS_REPORT_REQUEST,
      CUSTOMER_ANALYSIS_REPORT_SUCCESS,
      CUSTOMER_ANALYSIS_REPORT_FAILURE,
    ],
  },
});

export const downloadCircuitReportCU = (type: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/circuit-info/report`,
    method: 'post',
    body: { type },
    authenticated: true,
    types: [
      CIRCUIT_REPORT_REQUEST,
      CIRCUIT_REPORT_SUCCESS,
      CIRCUIT_REPORT_FAILURE,
    ],
  },
});

export const downloadCircuitReportPU = (customerId: number, type: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-info/report`,
    method: 'post',
    body: { type },
    authenticated: true,
    types: [
      CIRCUIT_REPORT_REQUEST,
      CIRCUIT_REPORT_SUCCESS,
      CIRCUIT_REPORT_FAILURE,
    ],
  },
});

export const deviceReportBundleRequest = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-device-management-reports-bundle`,
    method: 'post',
    body: { customer_id: [customerId] },
    authenticated: true,
    types: [
      DEVICE_REPORT_BUNDLE_REQUEST,
      DEVICE_REPORT_BUNDLE_SUCCESS,
      DEVICE_REPORT_BUNDLE_FAILURE,
    ],
  },
});

export const getReportCategories = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/scheduled-report/categories`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_REP_CATEGORIES_REQUEST,
      FETCH_REP_CATEGORIES_SUCCESS,
      FETCH_REP_CATEGORIES_FAILURE,
    ],
  },
});

export const getScheduledReportByCat = (category: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/scheduled-report/types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SHE_RE_TYPE_REQUEST,
      FETCH_SHE_RE_TYPE_SUCCESS,
      FETCH_SHE_RE_TYPE_FAILURE,
    ],
  },
});

export const fetchFrequencySHReport = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/scheduled-report/frequencies`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SHE_RE_FREQUENCY_REQUEST,
      FETCH_SHE_RE_FREQUENCY_SUCCESS,
      FETCH_SHE_RE_FREQUENCY_FAILURE,
    ],
  },
});

export const postScheduledReport = (frequency: string, type: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/users/scheduled-report`,
    method: 'post',
    authenticated: true,
    body: { frequency, scheduled_report_type: type },
    types: [
      POST_SH_REPORT_REQUEST,
      POST_SH_REPORT_SUCCESS,
      POST_SH_REPORT_FAILURE,
    ],
  },
});

export const editScheduledReport = (
  id: string,
  frequency: string,
  type: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/users/scheduled-report/${id}`,
    method: 'put',
    authenticated: true,
    body: { frequency, scheduled_report_type: type },
    types: [
      POST_SH_REPORT_REQUEST,
      POST_SH_REPORT_SUCCESS,
      POST_SH_REPORT_FAILURE,
    ],
  },
});

export const GetScheduledReport = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/users/scheduled-report`,
    method: 'get',
    authenticated: true,
    types: [
      GET_SH_REPORT_REQUEST,
      GET_SH_REPORT_SUCCESS,
      GET_SH_REPORT_FAILURE,
    ],
  },
});

export const getReportDownloadStats = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/report-download-stats`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      GET_REPORT_D_STATS_REQUEST,
      GET_REPORT_D_STATS_SUCCESS,
      GET_REPORT_D_STATS_FAILURE,
    ],
  },
});
